# coding: utf-8
import factory
from factory.django import DjangoModelFactory

from administration.models.people import Instructeur
from core.factories import UserFactory


class InstructeurFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)

    class Meta:
        model = Instructeur
