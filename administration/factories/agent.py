# coding: utf-8
import factory
from factory.django import DjangoModelFactory

from administration.factories.service import *
from administration.models import *
from administrative_division.factories import CommuneFactory
from core.factories import UserFactory
from sports.factories import FederationFactory


class ServiceAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    service = factory.SubFactory(ServiceFactory)

    class Meta:
        model = ServiceAgent


class MairieAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    commune = factory.SubFactory(CommuneFactory)

    class Meta:
        model = MairieAgent


class FederationAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    federation = factory.SubFactory(FederationFactory)

    class Meta:
        model = FederationAgent


class GGDAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    ggd = factory.SubFactory(GGDFactory)

    class Meta:
        model = GGDAgent


class EDSRAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    edsr = factory.SubFactory(EDSRFactory)

    class Meta:
        model = EDSRAgent


class EDSRAgentLocalFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    edsr = factory.SubFactory(EDSRFactory)

    class Meta:
        model = EDSRAgentLocal


class CGDAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    cgd = factory.SubFactory(CGDFactory)

    class Meta:
        model = CGDAgentLocal


class DDSPAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    ddsp = factory.SubFactory(CGDFactory)

    class Meta:
        model = DDSPAgent


class CommissariatAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    commissariat = factory.SubFactory(CommissariatFactory)

    class Meta:
        model = CommissariatAgentLocal


class CGAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    cg = factory.SubFactory(CGFactory)

    class Meta:
        model = CGAgent


class CGServiceAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    cg_service = factory.SubFactory(CGServiceFactory)

    class Meta:
        model = CGServiceAgentLocal


class CGSuperieurFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    cg = factory.SubFactory(CGFactory)

    class Meta:
        model = CGSuperieur


class SDISAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    sdis = factory.SubFactory(SDISFactory)

    class Meta:
        model = SDISAgent


class GroupementAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    compagnie = factory.SubFactory(CompagnieFactory)

    class Meta:
        model = CompagnieAgentLocal


class CODISAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    codis = factory.SubFactory(CODISFactory)

    class Meta:
        model = CODISAgent


class CISAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    cis = factory.SubFactory(CISFactory)

    class Meta:
        model = CISAgent


class BrigadeAgentFactory(DjangoModelFactory):
    """ Factory """

    user = factory.SubFactory(UserFactory)
    brigade = factory.SubFactory(BrigadeFactory)

    class Meta:
        model = BrigadeAgent
