# coding: utf-8
from django.urls import path

from administration.views.ajax import *
from administration.views.service import ServiceList


app_name = 'administration'
urlpatterns = [
    path('services/', ServiceList.as_view(), name='liste_services'),
    # Widget AJAX
    path('ajax/servicecomplexe/', ServiceComplexeAJAXView.as_view(), name='service_complexe'),
    path('ajax/service/', ServiceAJAXView.as_view(), name='service_widget'),
    path('ajax/prefecture/', PrefectureAJAXView.as_view(), name='prefecture_widget'),
    path('ajax/communeinstance/', CommuneAJAXView.as_view(), name='communeinstance_widget'),
    path('ajax/federation/', FederationAJAXView.as_view(), name='federationinstance_widget'),
    path('ajax/cgservice/', CGServiceAJAXView.as_view(), name='cgservice_widget'),
    path('ajax/cgd/', CGDAJAXView.as_view(), name='cgd_widget'),
    path('ajax/brigade/', BrigadeAJAXView.as_view(), name='brigade_widget'),
    path('ajax/commissariat/', CommissariatAJAXView.as_view(), name='commissariat_widget'),
    path('ajax/compagnie/', CompagnieAJAXView.as_view(), name='compagnie_widget'),
    path('ajax/cis/', CISAJAXView.as_view(), name='cis_widget'),
    path('ajax/autreservice/', AutreServiceAJAXView.as_view(), name='autreservice_widget'),
    path('ajax/secours/', SecoursAJAXView.as_view(), name='secours_widget'),

]
