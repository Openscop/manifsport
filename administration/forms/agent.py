# coding: utf-8
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField
from administration.models.agent import MairieAgent
from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelChoiceField
from core.forms.base import GenericForm


class MairieAgentForm(GenericForm):
    """ Formulaire des structures """

    # Champs
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'), Commune, label="Mairie")

    # Overrides
    def __init__(self, *args, **kwargs):
        super(MairieAgentForm, self).__init__(*args, **kwargs)
        if self.instance.pk and self.instance.commune:
            self.fields['departement'].initial = self.instance.commune.get_departement()
            self.fields['commune'].queryset = Commune.objects.by_departement(self.instance.commune.get_departement())

    # Meta
    class Meta:
        model = MairieAgent
        fields = ['user', 'departement', 'commune']
