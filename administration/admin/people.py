# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from administration.models.people import Instructeur, Secouriste
from administration.models.service import Prefecture
from core.util.admin import RelationOnlyFieldListFilter
from core.models import User


@admin.register(Instructeur)
class InstructeurAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'user', 'user__is_active', 'prefecture', 'prefecture__arrondissement__departement']
    list_filter = [('prefecture__arrondissement__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'prefecture__arrondissement__name']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(prefecture__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['prefecture'].queryset = Prefecture.objects.filter(arrondissement__departement=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form


@admin.register(Secouriste)
class SecouristeAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """
    list_display = ['pk', 'user', 'user__is_active', 'association']
    list_filter = [('association__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'association__name']
    list_per_page = 50

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(user__default_instance__departement=request.user.get_departement())
        return queryset
