# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin, getter_for_related_field

from administration.models.service import CGService, Commissariat, Compagnie, CGD, EDSR
from administration.models.agentlocal import CGServiceAgentLocal, CGDAgentLocal, CompagnieAgentLocal, CommissariatAgentLocal, \
    EDSRAgentLocal
from core.util.admin import RelationOnlyFieldListFilter
from core.models import User

class UserFullNameAndUsernameMixin():
    def agent_name(self, user_obj):
        return user_obj.user.get_full_name_and_username()
    agent_name.short_description = "Nom complet et pseudo"
    
@admin.register(CGServiceAgentLocal)
class CGServiceAgentLocalAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'cg_service', 'cg_service__cg__departement']
    list_filter = [('cg_service__cg__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'cg_service__name']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agentlocal__user__is_active', short_description='actif')

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cg_service__cg__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        if dept:
            if hasattr(dept, 'cg'):
                form.base_fields['cg_service'].queryset = CGService.objects.filter(cg=dept.cg)
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_cg:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(CompagnieAgentLocal)
class CompagnieAgentLocalAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'compagnie', 'compagnie__sdis__departement']
    list_filter = [('compagnie__sdis__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'compagnie__number']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agentlocal__user__is_active', short_description='actif')

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(compagnie__sdis__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        if dept:
            if hasattr(dept, 'sdis'):
                form.base_fields['compagnie'].queryset = Compagnie.objects.filter(sdis=dept.sdis)
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_sdis:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(CommissariatAgentLocal)
class CommissariatAgentLocalAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'commissariat', 'commissariat__commune__arrondissement__departement']
    list_filter = [('commissariat__commune__arrondissement__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'commissariat__commune__name']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agentlocal__user__is_active', short_description='actif')

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(commissariat__commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['commissariat'].queryset = Commissariat.objects.filter(commune__arrondissement__departement=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ddsp:
            return super().get_model_perms(request)
        else:
            return {}



@admin.register(CGDAgentLocal)
class CGDAgentLocalAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'cgd', 'cgd__commune__arrondissement__departement']
    list_filter = [('cgd__commune__arrondissement__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username', 'cgd__commune__arrondissement__name']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agentlocal__user__is_active', short_description='actif')

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(cgd__commune__arrondissement__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['cgd'].queryset = CGD.objects.filter(commune__arrondissement__departement=request.user.get_departement())
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}


@admin.register(EDSRAgentLocal)
class EDSRAgentLocalAdmin(ExportActionModelAdmin, RelatedFieldAdmin, UserFullNameAndUsernameMixin):
    """ Configuration admin """
    list_display = ['pk', 'agent_name', 'is_activ_bool', 'edsr', 'edsr__departement']
    list_filter = [('edsr__departement', RelationOnlyFieldListFilter), 'user__is_active']
    search_fields = ['user__username']
    list_per_page = 50

    is_activ_bool = getter_for_related_field('agentlocal__user__is_active', short_description='actif')

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not request.user.is_superuser:
            queryset = queryset.filter(edsr__departement=request.user.get_departement())
        return queryset

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        dept = request.user.get_departement()
        if dept:
            form.base_fields['edsr'].queryset = EDSR.objects.filter(departement=dept)
            if hasattr(dept, 'edsr'):
                form.base_fields['edsr'].initial = dept.edsr
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form

    def get_model_perms(self, request):
        if request.user.get_instance().active_ggd:
            return super().get_model_perms(request)
        else:
            return {}
