# coding: utf-8
from django.urls import reverse
from django.conf import settings
from django.db import models

from administrative_division.mixins.divisions import ManyPerCommuneMixin
from core.models.user import OneToUserQuerySetMixin, OneToUserModelMixin


class AgentQuerySet(models.QuerySet, OneToUserQuerySetMixin):
    """ Queryset des agents """

    # Getter
    def to_users(self):
        """ Renvoyer les utilisateurs pour ce queryset """
        return self.values_list('user', flat=True)


class Agent(models.Model, OneToUserModelMixin):
    """ Base agent départemental """

    # Champs
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="agent", verbose_name='utilisateur', on_delete=models.CASCADE)
    objects = AgentQuerySet.as_manager()

    # Getter
    @staticmethod
    def users_from_agents(agents):
        """ Renvoyer une liste d'utilisateurs depuis un itérable """
        return [agent.user for agent in agents]

    def save(self, *args, **kwargs):
        super(Agent, self).save(*args, **kwargs)
        modelname = self._meta.model_name
        if modelname != 'agent':
            user = self.user
            tableau = []
            if user.tableau_role:
                tableau = user.tableau_role.split(',')
            if modelname not in tableau:
                if modelname == 'cgsuperieur':
                    tableau.append('cgsuperieuragent')
                else:
                    tableau.append(modelname)
            user.tableau_role = ','.join(tableau)
            user.save()

    def delete(self, *args, **kwargs):
        modelname = self._meta.model_name
        if modelname != 'agent':
            user = self.user
            tableau = user.tableau_role.split(',')
            if modelname in tableau:
                if modelname == 'cgsuperieur':
                    tableau.remove('cgsuperieuragent')
                else:
                    tableau.remove(modelname)
            user.tableau_role = ','.join(tableau)
            user.save()
        super(Agent, self).delete(*args, **kwargs)

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        return reverse('admin:administration_' + type(self)._meta.object_name.lower() + '_change', args=(self.pk,))

    # Overrides
    def __str__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Agent"
        verbose_name_plural = "Agents"
        app_label = 'administration'
        default_related_name = 'agents'


class GGDAgent(Agent):
    """ Agent départemental GGD """

    # Champs
    ggd = models.ForeignKey('administration.ggd', verbose_name='GGD', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="ggdagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "GGD - Agent départemental"
        verbose_name_plural = "GGD - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'ggdagents'


class EDSRAgent(Agent):
    """ Agent départemental EDSR (assigné aux avis) """

    # Champs
    edsr = models.ForeignKey('administration.edsr', verbose_name='EDSR', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="edsragent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "EDSR - Agent départmental"
        verbose_name_plural = "EDSR - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'edsragents'


class BrigadeAgent(Agent):
    """ Agent de brigade de gendarmerie """

    # Champs
    brigade = models.ForeignKey('administration.brigade', verbose_name='brigade', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="brigadeagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "EDSR - Brigade - Agent départemental"
        verbose_name_plural = "EDSR - Brigade - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'brigadeagents'


class CODISAgent(Agent):
    """ Agent SDIS / CODIS """

    # Champs
    codis = models.ForeignKey('administration.codis', verbose_name='CODIS', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="codisagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "SDIS - CODIS - Agent départemental"
        verbose_name_plural = "SDIS - CODIS - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'codisagents'


class SDISAgent(Agent):
    """ Agent SDIS """

    # Champs
    sdis = models.ForeignKey('administration.sdis', verbose_name='SDIS', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="sdisagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "SDIS - Agent départemental"
        verbose_name_plural = "SDIS - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'sdisagents'


class CISAgent(Agent):
    """ Agent CIS (!) """

    # Champs
    cis = models.ForeignKey('administration.cis', verbose_name='CIS', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="cisagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "SDIS - CIS - Agent départemental"
        verbose_name_plural = "SDIS - CIS - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'cisagents'


class DDSPAgent(Agent):
    """ Agent DDSP départemental """

    # Champs
    ddsp = models.ForeignKey('administration.ddsp', verbose_name='DDSP', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="ddspagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "DDSP - Agent départemental"
        verbose_name_plural = "DDSP - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'ddspagents'


class FederationAgent(Agent):
    """ Agent de fédération sportive """

    # Champs
    federation = models.ForeignKey('sports.federation', verbose_name='fédération', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="federationagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Fédération - Agent départemental"
        verbose_name_plural = "Fédération - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'federationagents'


class MairieAgent(Agent, ManyPerCommuneMixin):
    """ Agenr de mairie (départemental ?) """

    # Champs
    agent = models.OneToOneField('administration.agent', related_name="mairieagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Agent municipal"
        verbose_name_plural = "Agents municipaux"
        app_label = 'administration'
        default_related_name = 'mairieagents'


class CGAgent(Agent):
    """ Agent du Conseil départemental """

    # Champs
    cg = models.ForeignKey('administration.cg', verbose_name='CD', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="cgagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "CD - Agent départemental"
        verbose_name_plural = 'CD - Agents départementaux'
        app_label = 'administration'
        default_related_name = 'cgagents'


class CGSuperieur(Agent):
    """ Agent supérieur du Conseil départemental """

    # Champs
    cg = models.ForeignKey('administration.cg', verbose_name='CD', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="cgsuperieuragent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "CD - Agent départemental n+1"
        verbose_name_plural = "CD - Agents départementaux n+1"
        app_label = 'administration'
        default_related_name = 'cgsuperieurs'


class ServiceAgent(Agent):
    """ Agent d'un service départemental """

    # Champs
    service = models.ForeignKey('administration.service', verbose_name='service', on_delete=models.CASCADE)
    agent = models.OneToOneField('administration.agent', related_name="serviceagent", parent_link=True, verbose_name="ID Agent", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Service 'simple' - Agent départemental"
        verbose_name_plural = "Services 'simples' - Agents départementaux"
        app_label = 'administration'
        default_related_name = 'serviceagents'
