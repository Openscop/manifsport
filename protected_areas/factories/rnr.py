# coding: utf-8
import factory

from administrative_division.factories import DepartementFactory
from ..models import AdministrateurRNR, RNR


class RNRFactory(factory.django.DjangoModelFactory):
    """ Factory réserve naturelle régionale """

    # Champs
    name = "Gorges de la Loire"
    departement = factory.SubFactory(DepartementFactory)

    # Méta
    class Meta:
        model = RNR


class AdministrateurRNRFactory(factory.django.DjangoModelFactory):
    """ Factory administrateur de RNR """

    # Champs
    short_name = 'FRAPNA'
    name = "Federation Rhone-Alpes de Protection de la Nature de la Loire"
    email = 'john.doe@frapna.org'
    rnr = factory.SubFactory(RNRFactory)
    departement = factory.SubFactory(DepartementFactory)

    # Meta
    class Meta:
        model = AdministrateurRNR
