# coding: utf-8
from django.apps import AppConfig


class ProtectedAreasConfig(AppConfig):
    """ Configuration de l'application """

    name = 'protected_areas'
    verbose_name = "Zones protégées"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        pass


default_app_config = 'protected_areas.ProtectedAreasConfig'
