from django.urls import path
from .views.message import *
from .views.filtre import FiltrePerso, FiltrePersoSupprime
from .views.cpt import CptView, EnvoiMessageRecap

app_name = 'messagerie'
urlpatterns = [

    path('listemessage/', ListeMessageView.as_view(), name="listemessage"),
    path('message/', MessageView.as_view(), name="message"),
    path('messagerie/', AffichageMessagerie.as_view(), name="messagerie"),
    path('lu/', LuMessage.as_view(), name="lu"),
    path('carnetinstructeur/', CarnetInstructeur.as_view(), name="carnetinstructeur"),
    path('listeservice/', ListeService.as_view(), name="listeservice"),
    path('envoimessage/', EnvoiMessage.as_view(), name='envoimessage'),
    path('envoimessageactualite/', EnvoiMessageActualite.as_view(), name='envoimessageactualite'),
    path('envoimessagereponse/', EnvoiMessageReponse.as_view(), name="envoimessagereponse"),
    path('envoimessagerecap/', EnvoiMessageRecap.as_view(), name="envoimessagerecap"),
    path('', PageMessagerieGlobale.as_view(), name="boite_principale"),

    path('filtre/del/', FiltrePersoSupprime.as_view(), name='filtre_perso_del'),
    path('filtre/listedocconcerne/', ListeDocConcerne.as_view(), name="listedocconcerne"),
    path('filtre/', FiltrePerso.as_view(), name="filtre_perso"),

    path('cpt/', CptView.as_view(), name='cpt'),


]