import json
from datetime import timedelta

from django.utils import timezone
from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse, HttpResponseForbidden
from django.db.models import Q
from django.db.models import Subquery
from django.utils.decorators import method_decorator

from administrative_division.models import Departement, Commune
from ..models import Enveloppe, CptMsg
from core.util.permissions import login_requis
from core.util.user import UserHelper
from messagerie.crea_carnet import get_carnet
from messagerie.forms.messages import MessageForm
from messagerie.tasks import envoi_msg_celery
from administration.models.service import *
from organisateurs.models import Organisateur
from instructions.models import DocumentOfficiel
from evenements.models import Manif, DocumentComplementaire
from administration.models.service import GGD, EDSR, CODIS, Brigade
from sports.models import Federation


@method_decorator(login_requis(), name='dispatch')
class ListeMessageView(View):
    """
    affichage des messages reçu par l'utilisateur
    Cette vue n'est appelée que apr ajax
    """

    def get(self, request):
        limit = request.GET['limit'] if request.GET.get('limit') else 10
        # dans le cas manif
        if request.GET.get('manif'):
            manif = Manif.objects.get(pk=request.GET['manif'])
            # solution abandonée
            # si intructeur ou mairie agent on prend tout les message de la manif sauf les conv privée
            # if request.user.has_role('instructeur') or request.user.has_role('mairieagent'):
            #
            #     enveloppe = Enveloppe.objects.filter(
            #         pk__in=Subquery(Enveloppe.objects.filter(
            #             manifestation=manif).distinct('corps').all().values('pk'))).order_by('-date')
            #     # Cette requete exclu toutes les conversations dont l'user n'est ni expediteur ni destinataire
            #     enveloppe = enveloppe.exclude(Q(type='conversation'))
            #     conv = Enveloppe.objects.filter(
            #         pk__in=Subquery(Enveloppe.objects.filter(
            #             Q(destinataire_id=request.user) | Q(expediteur_id=request.user)).filter(
            #             manifestation=manif).filter(type="conversation").distinct('corps').all().values('pk'))).order_by('-date')
            #     enveloppe = enveloppe | conv

            # un user n'aura le droit qu'à ses propres messages
            enveloppe = Enveloppe.objects.filter(
                pk__in=Subquery(Enveloppe.objects.filter(
                    Q(destinataire_id=request.user) | Q(expediteur_id=request.user)).filter(
                    manifestation=manif).distinct('corps').all().values('pk'))).order_by('-date')

        # cas dans la messagerie générale.
        else:
            enveloppe = Enveloppe.objects.filter(
                pk__in=Subquery(Enveloppe.objects.filter(
                    Q(destinataire_id=request.user) | Q(expediteur_id=request.user)).distinct('corps').all().values('pk'))).order_by('-date')
            manif = None

        # on fait ensuite le tri en fonction des filtres de l'utilisateur

        enveloppe = enveloppe.filter(~Q(type='conversation')) if not request.GET.get('conv') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='news')) if not request.GET.get('nouv') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='info_suivi')) if not request.GET.get('suivi') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='action')) if not request.GET.get('action') else enveloppe
        enveloppe = enveloppe.filter(~Q(type='tracabilite')) if not request.GET.get('trac') else enveloppe

        enveloppe = enveloppe.filter(~Q(doc_objet='dossier')) if not request.GET.get('dossier') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='piece_jointe')) if not request.GET.get('docjoin') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='avis')) if not request.GET.get('avis') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='preavis')) if not request.GET.get('preavis') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='arrete')) if not request.GET.get('arrete') else enveloppe
        enveloppe = enveloppe.filter(~Q(doc_objet='recepisse')) if not request.GET.get('recepisse') else enveloppe

        enveloppe = enveloppe.filter(~Q(destinataire_id=request.user)) if not request.GET.get('recu') else enveloppe
        enveloppe = enveloppe.filter(~Q(expediteur_id=request.user)) if not request.GET.get('envoi') else enveloppe

        enveloppe = enveloppe.filter(~Q(lu_requis=False)).filter(~Q(lu_datetime__isnull=False)).filter(~Q(expediteur_id=request.user)) if not \
            request.GET.get('nonlu') else enveloppe

        # on compte le nombre d'enveloppe puis on trie selon la limite d'enveloppe affichées
        max_enveloppe = len(enveloppe)
        enveloppe = enveloppe.all()[:int(limit)] if enveloppe else ''

        tab_env = []

        for envel in enveloppe:
            if not envel.type == "news" or not envel.expediteur_id == request.user:
                tab_env.append(envel)
            else:
                enve_desti = Enveloppe.objects.filter(corps=envel.corps).filter(~Q(destinataire_id=request.user))
                if not enve_desti:
                    tab_env.append(envel)
                else:
                    enve_desti = enve_desti.first()
                    tab_env.append(enve_desti)

        context = {
            'manif': manif,
            'messages': tab_env,
            'max': max_enveloppe,
        }
        return render(request, 'messagerie/liste_message.html', context)


@method_decorator(login_requis(), name='dispatch')
class MessageView(View):
    """
    on affiche un message
    """

    def get(self, request):
        enveloppe = Enveloppe.objects.get(pk=request.GET.get('pk', default=None))
        env_destinataires = Enveloppe.objects.filter(corps=enveloppe.corps).all()

        destinataires = []
        # ici on va modifié le nom des destinataires pour n'afficher que le service pour les organisateurs
        # pour le reste ou affiche service + nom, prenom s'il y a
        if not enveloppe.type == "news":
            for enve in env_destinataires:
                if request.user.has_role('organisateur'):
                    service = enve.destinataire_txt.split('→')[0]
                    lu = True
                    destinataires.append({"service": service, "lu": lu}) if len(list(filter(lambda sch: sch['service'] == service, destinataires)))==0 else ""
                else:
                    service_deja_ajoute = False
                    for user in destinataires:
                        if user.get("service", None) == enve.destinataire_txt.split('→')[0]:
                            service_deja_ajoute = True
                            nom = " " + enve.destinataire_txt.split('→')[1] if len(enve.destinataire_txt.split('→')) > 1 and (not enve.destinataire_txt.split('→')[1] == '  ' and not enve.destinataire_txt.split('→')[1] == ' ') else False
                            lu = False if not enve.lu_datetime and enve.lu_requis else True
                            user.get("utilisateur", None) .append({"nom": nom, "lu": lu})
                    if not service_deja_ajoute:
                        service = enve.destinataire_txt.split('→')[0]
                        nom = enve.destinataire_txt.split('→')[1] if len(enve.destinataire_txt.split('→')) > 1 and (not enve.destinataire_txt.split('→')[1] == '  ' and not enve.destinataire_txt.split('→')[1] == ' ') else False
                        lu = False if not enve.lu_datetime and enve.lu_requis else True
                        destinataires.append({"service": service, "utilisateur": [{"nom": nom, "lu": lu}]})

        # si on a une conversation on va chercher les message de cette conversation
        if enveloppe.reponse:
            old = Enveloppe.objects.filter(Q(reponse=enveloppe.reponse) | Q(pk=enveloppe.reponse)).filter(date__lt=enveloppe.date).order_by("-date")
            new = Enveloppe.objects.filter(reponse=enveloppe.reponse).filter(date__gt=enveloppe.date).order_by("-date")
        else:
            new = Enveloppe.objects.filter(reponse=enveloppe.pk).order_by("-date")
            old = []
        orga = True if request.user.has_role('organisateur') else False
        # le fen indique si le message à été ouvert dans la messagerie ou dans une nouvelle fenêtre
        if request.GET.get('fen'):
            context = {
                'enveloppe': enveloppe,
                'new': new,
                "old": old,
                'destinataires': destinataires,
                'fenetre': 1,
                'organisateur': orga
            }
        else:
            context = {
                'enveloppe': enveloppe,
                'new': new,
                "old": old,
                'destinataires': destinataires,
                'organisateur': orga
            }
        return render(request, 'messagerie/message.html', context)


@method_decorator(login_requis(), name='dispatch')
class LuMessage(View):
    """
    le message est lu
    """

    def post(self, request):
        enveloppe = Enveloppe.objects.get(pk=request.GET.get('pk', default=None))
        if enveloppe.destinataire_id == request.user:
            enveloppe.lu_datetime = timezone.now()
            enveloppe.save()
            cpt = CptMsg.objects.get(utilisateur=request.user)
            cpt.update_cpt()

        elif enveloppe.type == "news":
            enve = Enveloppe.objects.filter(corps=enveloppe.corps, destinataire_id=request.user)
            if enve:
                enve = enve.get()
                enve.lu_datetime = timezone.now()
                enve.save()
                cpt = CptMsg.objects.get(utilisateur=request.user)
                cpt.update_cpt()
        return HttpResponse(200)


@method_decorator(login_requis(), name='dispatch')
class ListeDocConcerne(View):
    """
    envoi de la liste des docs demandé ajax
    pk+ nom str (peut etre le service concerné ou le type)
    """

    def get(self, request):
        manif = Manif.objects.get(pk=request.GET['manif'])
        reponses = []

        if request.GET.get('avis'):
            if manif.get_instruction():
                if request.user.has_role('instructeur') or request.user.has_role('mairieagent'):
                    for avi in manif.instruction.avis.all():
                        reponse = {"nom": str(avi), "pk": avi.pk}
                        reponses.append(reponse)
                elif request.user.has_role('agent'):
                    avis = manif.instruction.get_avis_user(request.user)
                    if avis:
                        reponse = {"nom": str(avis), "pk": avis.pk}
                        reponses.append(reponse)
                else:
                    return HttpResponseForbidden(403)
                reponsesjson = json.dumps(reponses)
                return HttpResponse(reponsesjson)

        elif request.GET.get('preavis'):
            if manif.get_instruction():
                # ici on envoie rien pour eviter une erreur ajax on envera 403 au lieu d'une liste vide
                if request.user.has_role('instructeur') or request.user.has_role('mairieagent'):
                    return HttpResponseForbidden(403)
                elif request.user.has_role('agent'):
                    avis = manif.instruction.get_avis_user(request.user)
                    for preavi in avis.preavis.all():
                        reponse = {"nom": preavi.service_concerne, "pk": preavi.pk}
                        reponses.append(reponse)
                elif request.user.has_role('agentlocal'):
                    preavis = manif.instruction.get_preavis_user(request.user)
                    if preavis:
                        reponse = {"nom": str(preavis), "pk": preavis.pk}
                        reponses.append(reponse)
                reponsesjson = json.dumps(reponses)
                return HttpResponse(reponsesjson)
        elif request.GET.get('arrete'):
            docs = DocumentOfficiel.objects.filter(instruction__manif=manif)
            docs = docs.filter(~Q(nature=3))
            for doc in docs:
                reponse = {"nom": str(doc.get_nature_nom()), "pk": doc.pk}
                reponses.append(reponse)
            reponsesjson = json.dumps(reponses)
            return HttpResponse(reponsesjson)
        elif request.GET.get('recepisse'):
            docs = DocumentOfficiel.objects.filter(instruction__manif=manif, nature=3)
            for doc in docs:
                reponse = {"nom": str(doc.get_nature_nom()), "pk": doc.pk}
                reponses.append(reponse)
            reponsesjson = json.dumps(reponses)
            return HttpResponse(reponsesjson)
        elif request.GET.get('piece_jointe'):
            docs = DocumentComplementaire.objects.filter(manif=manif)
            for doc in docs:
                reponse = {"nom": doc.information_requise[:50], "pk": doc.pk}
                reponses.append(reponse)
            reponsesjson = json.dumps(reponses)
            return HttpResponse(reponsesjson)
        else:
            return HttpResponse([])


@method_decorator(login_requis(), name='dispatch')
class CarnetInstructeur(View):
    """
    création du carnet des instructeur selon le service qu'ils ont séléctionné
    """

    def get(self, request):

        def ajouter(carnet, agents):
            for agent in agents:
                user = agent.user
                if user.is_active:
                    carnet.append({"pk": user.pk,
                                   "service": str(user.get_service()),
                                   "first_name": user.first_name,
                                   "last_name": user.last_name})
            return carnet

        if request.GET.get('idservice'):
            carnet = []
            # si l'user ne veut pas un organisateur on va verifier le type de service et rentre les agents dans le carnet
            if not request.GET['idservice'].split("|")[0] == 'orga':
                serv = request.GET['idservice'].split('|')

                if serv[1] == 'Service':
                    service = Service.objects.get(pk=serv[0])
                    agents = service.serviceagents.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == 'Mairie':
                    mairie = Commune.objects.get(pk=serv[0])
                    agents = mairie.get_mairieagents()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == 'Federation':
                    federation = Federation.objects.get(pk=serv[0])
                    agents = federation.federationagents.all()
                    carnet = ajouter(carnet, agents)

                elif serv[1] == "Prefecture":
                    pref = Prefecture.objects.get(pk=serv[0])
                    intructeurs = pref.instructeurs.all()
                    carnet = ajouter(carnet, intructeurs)
                elif serv[1] == "SDIS":
                    sdis = SDIS.objects.get(pk=serv[0])
                    agents = sdis.sdisagents.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "GGD":
                    ggd = GGD.objects.get(pk=serv[0])
                    agents = ggd.ggdagents.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "EDSR":
                    edsr = EDSR.objects.get(pk=serv[0])
                    agents = edsr.edsragents.all()
                    agentslocal = edsr.edsragentslocaux.all()
                    carnet = ajouter(carnet, agents)
                    carnet = ajouter(carnet, agentslocal)
                elif serv[1] == "DDSP":
                    ddsp = DDSP.objects.get(pk=serv[0])
                    agents = ddsp.ddspagents.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "Compagnie":
                    comp = Compagnie.objects.get(pk=serv[0])
                    agents = comp.compagnieagentslocaux.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "Commissariat":
                    comm = Commissariat.objects.get(pk=serv[0])
                    agents = comm.commissariatagentslocaux.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "CODIS":
                    codis = CODIS.objects.get(pk=serv[0])
                    agents = codis.codisagents.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "CIS":
                    cis = CIS.objects.get(pk=serv[0])
                    agents = cis.cisagents.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "CGService":
                    cgs = CGService.objects.get(pk=serv[0])
                    agents = cgs.cgserviceagentslocaux.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "CGD":
                    cgd = CGD.objects.get(pk=serv[0])
                    agents = cgd.ggdagentslocaux.all()
                    carnet = ajouter(carnet, agents)
                elif serv[1] == "CG":
                    cg = CG.objects.get(pk=serv[0])
                    agents = cg.cgagents.all()
                    agentssup = cg.cgsuperieurs.all()
                    carnet = ajouter(carnet, agents)
                    carnet = ajouter(carnet, agentssup)
                elif serv[1] == "Brigade":
                    brigade = Brigade.objects.get(pk=serv[0])
                    agents = brigade.brigadeagents.all()
                    carnet = ajouter(carnet, agents)

                # cas où on a des roles
                elif serv[1] == "role":
                    pk_dep = request.GET.get('dep', '').isdigit()
                    if pk_dep:
                        dep = Departement.objects.filter(pk=request.GET.get('dep', None))
                    else:
                        dep = None
                    if dep:
                        dep = dep.get()
                    else:
                        dep = request.user.default_instance.departement
                    from core.models import User


                    users = User.objects.filter(default_instance__departement=dep)
                    for user in users:
                        if user.has_role(serv[0]):
                            carnet.append({"pk": user.pk,
                                       'service': str(user.get_service()),
                                       "first_name": user.first_name,
                                       "last_name": user.last_name})


            # ici on va prendre les organisateurs qui ont été connectés cette années
            elif request.GET['idservice'] == "orga|new":
                pk_dep = request.GET.get('dep', '').isdigit()
                if pk_dep:
                    dep = Departement.objects.filter(pk=request.GET.get('dep', None))
                else:
                    dep = None
                if dep:
                    dep = dep.get()
                else:
                    dep = request.user.default_instance.departement
                last_year = timezone.now() - timedelta(days=360)
                orgas = Organisateur.objects.filter(user__default_instance__departement=dep,
                                                    user__last_login__gte=last_year)
                for orga in orgas:
                    user = orga.user
                    if hasattr(orga, 'structure'):
                        carnet.append({"pk": user.pk,
                                       "service": str(orga.structure),
                                       "first_name": user.first_name,
                                       "last_name": user.last_name})
                    else:
                        carnet.append({"pk": user.pk,
                                       "service": "Aucun",
                                       "first_name": user.first_name,
                                       "last_name": user.last_name})
            # tout les organisateurs
            elif request.GET['idservice'] == "orga|old":
                pk_dep = request.GET.get('dep', '').isdigit()
                if pk_dep:
                    dep = Departement.objects.filter(pk=request.GET.get('dep', None))
                else:
                    dep = None
                if dep:
                    dep = dep.get()
                else:
                    dep = request.user.default_instance.departement
                orgas = Organisateur.objects.filter(user__default_instance__departement=dep)
                for orga in orgas:
                    user = orga.user
                    if hasattr(orga, 'structure'):
                        carnet.append({"pk": user.pk,
                                       "service": str(orga.structure),
                                       "first_name": user.first_name,
                                       "last_name": user.last_name})
                    else:
                        carnet.append({"pk": user.pk,
                                       "service": "Aucun",
                                       "first_name": user.first_name,
                                       "last_name": user.last_name})
            else:
                pk = request.GET['idservice'].split('|')[1]
                manif = Manif.objects.get(pk=pk)
                user = manif.structure.organisateur.user
                carnet.append({"pk": user.pk,
                               "service": str(manif.structure),
                               "first_name": user.first_name,
                               "last_name": user.last_name})
            data = json.dumps(carnet)
            return HttpResponse(data)
        else:
            return HttpResponseForbidden(403)


@method_decorator(login_requis(), name='dispatch')
class EnvoiMessageActualite(View):
    """
    Envoi d'une actualité
    """

    def post(self, request):
        ad_tech = request.user.groups.filter(name="Administrateurs techniques").exists()
        ad_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
        if ad_instance or ad_tech:
            # si admin terch on a le droit d'envoyer à tous
            if ad_tech:
                departement = request.POST.getlist('departement')
            else:
                # si admin instance on ne peut envoyer qu'à son departement
                departement = [request.user.get_departement().pk]
            role = request.POST.getlist('role')
            formulaire = MessageForm(request.POST)

            if formulaire.is_valid():

                msg_json = {
                    'expediteur': request.user.pk,
                    'msg': formulaire.cleaned_data['corps'],
                    'objet': request.POST['objet'],
                    'option': {
                        'doc_type': None,
                        'doc_pk': None,
                    },
                    'type': "news",
                    'manif': None,
                    'destinataire': {
                        'dep': departement,
                        'role': role,
                    },
                }
                envoi_msg_celery.delay(msg_json=msg_json, type_envoi='actualite')
                return HttpResponse(200)
            else:
                return HttpResponseForbidden(403)
        else:
            return HttpResponseForbidden(403)


def remplir_services(request):
    """
    fonction qui founit la liste des services du département
    """
    services = []
    admin_tech = request.user.groups.filter(name="Administrateurs techniques").exists()
    admin_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
    if request.user.has_role("instructeur") or request.user.has_role("mairieagent") or admin_instance or admin_tech:
        est_instructeur = True
    else:
        est_instructeur = False

    if request.GET.get('pk', None):
        dep = Departement.objects.filter(pk=request.GET['pk'])
        dep = dep.get() if dep else None
    else:
        dep = None
    dep = request.user.get_departement() if not dep else dep

    if est_instructeur:
        servi = Service.objects.filter(departements=dep)
        for service in servi:
            services.append(service)
    if request.user.has_role('serviceagent'):
        services.append(request.user.agent.serviceagent.service)
    # cas SDIS
    if est_instructeur or request.user.has_role('sdisagent') or request.user.has_role(
            'compagnieagentlocal') or request.user.has_role('codisagent'):
        sdis = SDIS.objects.filter(departement=dep)
        for serv in sdis:
            services.append(serv)
        codis = CODIS.objects.filter(departement=dep)
        for serv in codis:
            services.append(serv)
        cis = CIS.objects.filter(commune__arrondissement__departement=dep)
        for serv in cis:
            services.append(serv)
        compagnie = Compagnie.objects.filter(sdis__departement=dep)
        for serv in compagnie:
            services.append(serv)
    # cas EDSR
    if est_instructeur or request.user.has_role('ggdagent') or request.user.has_role(
            'edsragent') or request.user.has_role('edsragentlocal') or request.user.has_role(
        'cgdagentlocal') or request.user.has_role('brigadeagent'):
        ggd = GGD.objects.filter(departement=dep)
        for serv in ggd:
            services.append(serv)
        edsr = EDSR.objects.filter(departement=dep)
        for serv in edsr:
            services.append(serv)
        cgd = CGD.objects.filter(commune__arrondissement__departement=dep)
        for serv in cgd:
            services.append(serv)
        brigade = Brigade.objects.filter(commune__arrondissement__departement=dep)
        for serv in brigade:
            services.append(serv)
    # cas DDSP
    if est_instructeur or request.user.has_role('ddspagent') or request.user.has_role(
            'commissariatagentlocal'):
        ddsp = DDSP.objects.filter(departement=dep)
        for serv in ddsp:
            services.append(serv)
        commissariat = Commissariat.objects.filter(
            commune__arrondissement__departement=dep)
        for serv in commissariat:
            services.append(serv)
    # cg
    if est_instructeur or request.user.has_role('cgagent') or request.user.has_role(
            'cgsuperieuragent') or request.user.has_role('cgserviceagentlocal') or request.user.has_role(
        'cgdagentlocal') or request.user.has_role('brigadeagent'):
        cgservice = CGService.objects.filter(cg__departement=dep)
        for serv in cgservice:
            services.append(serv)
        cg = CG.objects.filter(departement=dep)
        for serv in cg:
            services.append(serv)
    # cas federation
    if request.user.has_role('federationagent'):
        services.append(request.user.agent.federationagent.federation)
    if est_instructeur:
        federations = Federation.objects.all()
        for serv in federations:
            services.append(serv)

    # tout le monde à le droit à la pref
    pref = Prefecture.objects.filter(arrondissement__departement=dep)
    for serv in pref:
        services.append(serv)
    return services


@method_decorator(login_requis(), name='dispatch')
class ListeService(View):
    """
    Class pour modifier la liste des services à la selection d'un nouveau département
    """
    def get(self, request):

        admin_tech = request.user.groups.filter(name="Administrateurs techniques").exists()
        admin_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
        if request.GET.get('pk', None):
            dep = Departement.objects.filter(pk=request.GET['pk'])
            print(dep)
            dep = dep.get() if dep else None
        else:
            dep = None
        dep = request.user.get_departement() if not dep else dep
        print(dep)

        if request.GET.get('type', None)=='service':
            services = remplir_services(request)
            carnet = []
            for service in services:
                carnet.append({
                    "pk": service.pk,
                    "classname": service.__class__.__name__,
                    "nom": str(service),
                })
            data = json.dumps(carnet)

        elif request.GET.get('type', None)=='mairie':
            mairies = Commune.objects.by_departement(dep)
            carnet = []
            for mairie in mairies:
                carnet.append({
                    "pk": mairie.pk,
                    "classname": "Mairie",
                    "nom": str(mairie),
                })
            data = json.dumps(carnet)

        elif request.GET.get('type', None)=="groupe":
            roles = UserHelper.ROLE_CHOICES
            carnet = []
            for role in roles:
                carnet.append({
                    'pk': role[0],
                    'classname': "role",
                    'nom': role[1]
                })
            data = json.dumps(carnet)

        else:
            data = ''

        return HttpResponse(data)




@method_decorator(login_requis(), name='dispatch')
class EnvoiMessage(View):
    """
    Envoi un message à l'utilisateur
    """
    # affichage de la boite d'envoi
    def get(self, request):
        formulaire = MessageForm()

        admin_tech = request.user.groups.filter(name="Administrateurs techniques").exists()
        admin_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
        if request.GET.get('manif'):
            manif = request.GET['manif']
            manif = Manif.objects.get(pk=manif)
        else:
            manif = None

        if request.user.has_role("instructeur") or request.user.has_role("mairieagent") or admin_instance or admin_tech:
            est_instructeur = True
        else:
            est_instructeur = False
        # si la personne est instructeur elle a accès à la recherche et l'envoi par service
        if not request.user.has_role("organisateur"):
            instru = 1
            services = remplir_services(request)
            carnet = []
        # sinon on va utiliser la fonction get carnet pour le générer
        else:
            instru = 0
            services = []
            carnet = get_carnet(request)
        dep = Departement.objects.all() if est_instructeur else None
        mydep = request.user.get_departement()
        role = UserHelper.ROLE_CHOICES
        ad_tech = request.user.groups.filter(name="Administrateurs techniques").exists()
        ad_instance = request.user.groups.filter(name="Administrateurs d'instance").exists()
        context = {
            "est_instructeur": est_instructeur,
            'form': formulaire,
            'manif': manif,
            'carnet': carnet,
            'instru': instru,
            'services': services,
            'deps': dep,
            "mydep": mydep,
            'roles': role,
            'ad_tech': ad_tech,
            'ad_instance': ad_instance,
        }
        return render(request, 'messagerie/envoi_message.html', context)

    # l'envoi de conversation
    def post(self, request):
        service = request.POST.get('service', None)
        destinataires = request.POST.getlist('destinataires')
        doc_objet = request.POST.getlist('doc_objet')
        doc_objet = doc_objet[0] if doc_objet else None
        doc_objet = None if doc_objet == "aucun" else doc_objet
        doc_associe = request.POST.getlist('doc_associe_link')
        doc_associe = doc_associe[0] if doc_associe else None
        departement_instru = request.POST.get('departement_intru', None)
        formulaire = MessageForm(request.POST)

        if doc_associe is None:
            action_pk = None
            action_type = None
        elif doc_objet == 'avis':
            action_pk = doc_associe
            action_type = 'avis'
        elif doc_objet == 'preavis':
            action_pk = doc_associe
            action_type = 'preavis'
        elif doc_objet == 'piece_jointe':
            action_pk = doc_associe
            action_type = 'doc'
        elif doc_objet == 'arrete':
            action_pk = doc_associe
            action_type = 'offi'
        elif doc_objet == 'recepisse':
            action_pk = doc_associe
            action_type = 'offi'
        else:
            action_pk = None
            action_type = None

        if formulaire.is_valid():
            msg_json = {
                'expediteur': request.user.pk,
                'msg': formulaire.cleaned_data['corps'],
                'objet': request.POST['objet'],
                'option': {
                    'doc_type': action_type,
                    'doc_pk': action_pk,
                    'doc_objet': doc_objet,
                },
                'type': "conversation",
                'manif': request.GET.get('manif', None),
            }
            if destinataires:
                # si des destinataires precis ont été selectionner
                msg_json['destinataire'] = destinataires
                envoi_msg_celery.delay(msg_json=msg_json, type_envoi='destinataire_user')
            else:
                # la on va envoyer à un service entier
                if service=="orga|new":
                    service = departement_instru+"|new"
                elif service=="orga|old":
                    service = departement_instru+"|old"
                # cas où c'est un role
                elif service.split("|")[1]=="role":
                    msg_json['destinataire'] = {'type': service.split("|")[0], 'pk': departement_instru}
                    envoi_msg_celery.delay(msg_json, 'destinataire_role')
                else:
                    ser = service.split('|')
                    msg_json['destinataire'] = {'type': ser[1], 'pk': ser[0]}
                    envoi_msg_celery.delay(msg_json, 'destinataire_service')
            return HttpResponse(200)
        else:
            return HttpResponseForbidden(403)


@method_decorator(login_requis(alert=True), name='dispatch')
class AffichageMessagerie(View):
    """
    affichage messagerie
    """

    def get(self, request):
        if request.GET.get('manif'):
            manif = request.GET['manif']
            manif = Manif.objects.get(pk=manif)
        else:
            manif = None

        if request.user.has_group('Administrateurs d\'instance') or request.user.is_superuser:
            actu = 1
        else:
            actu = 0
        context = {
            'manif': manif,
            "actu": actu,
        }
        return render(request, 'messagerie/messagerie.html', context)


@method_decorator(login_requis(), name='dispatch')
class EnvoiMessageReponse(View):
    """
    Envoi d'une réponse (différe du message normal)
    """
    def get(self, request):
        enve = Enveloppe.objects.filter(pk=request.GET.get('pk', None))
        if not enve:
            return HttpResponseForbidden(403)
        enve = enve.get()
        formulaire = MessageForm()
        context = {
            'enve': enve,
            'form': formulaire,
        }
        return render(request, 'messagerie/envoi_message_reponse.html', context)

    def post(self, request):
        enve = Enveloppe.objects.filter(pk=request.GET.get('pk', None))
        if not enve:
            return HttpResponseForbidden(403)
        enve = enve.get()
        formulaire = MessageForm(request.POST)
        if formulaire.is_valid():
            if request.user == enve.destinataire_id:
                destinataire = enve.expediteur_id
            else:
                destinataire = enve.destinataire_id
            # on va mettre le pk du premier msg de la conversation
            original = enve.reponse if enve.reponse else enve.pk
            manif = enve.manifestation.pk if enve.manifestation else None

            msg_json = {
                'expediteur': request.user.pk,
                'msg': formulaire.cleaned_data['corps'],
                'objet': request.POST['objet'],
                'original': original,
                'option': {
                    'doc_type': None,
                    'doc_pk': None,
                    'doc_objet': enve.doc_objet,
                },
                'type': "conversation",
                'manif': manif,
                'destinataire': [destinataire.pk],
            }

            task = envoi_msg_celery.delay(msg_json=msg_json, type_envoi='destinataire_user')
            return HttpResponse(200)
        else:
            return HttpResponseForbidden(403)


@method_decorator(login_requis(alert=True), name='dispatch')
class PageMessagerieGlobale(View):
    """
    Affichage du webmail n'étant pas exclusif à une manifestation
    """

    def get(self, request):
        return render(request, 'messagerie/messagerie_globale.html')
