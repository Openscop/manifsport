from django.views.generic import View
from django.http import HttpResponse, HttpResponseForbidden
from django.core import serializers

from messagerie.models.filtre import Filtre


class FiltrePerso(View):
    """
    affichage et enregistrement des filtres
    on rentre le lien ajax dans la base
    on va cherche l'ensemble des filtres de l'utilisateurs l'ajax s'occupera du reste
    """
    def get(self, request):
        if not request.user.is_authenticated:
            return HttpResponseForbidden(403)
        filtres = Filtre.objects.filter(utilisateur=request.user).all()
        data = serializers.serialize("json", filtres)
        return HttpResponse(data)

    def post(self, request):
        if not request.user.is_authenticated:
            return HttpResponseForbidden(403)
        count = Filtre.objects.filter(utilisateur=request.user).count()
        if count < 6:
            filtre = Filtre(utilisateur=request.user, titre=request.POST['titre'], option=request.POST['option'])
            filtre.save()
            return HttpResponse(200)
        else:
            return HttpResponse('Vous avez trop de filtres !')


class FiltrePersoSupprime(View):
    """
    suppression d'un filtre custom
    """
    def get(self, request):
        if not request.user.is_authenticated:
            return HttpResponseForbidden(403)
        pk = request.GET.get('pk')
        filtre = Filtre.objects.get(pk=pk)
        if filtre.utilisateur == request.user:
            filtre.delete()
            return HttpResponse(200)
        else:
            return HttpResponseForbidden(403)
