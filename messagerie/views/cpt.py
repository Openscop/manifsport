import json

from django.views.generic import View
from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseForbidden
from post_office import mail

from ..models import CptMsg
from configuration import settings


class CptView(View):
    """
    envoi des donnée cpt
    """
    def get(self, request):
        if request.user.is_authenticated:
            cpt = CptMsg.objects.get(utilisateur=request.user)
            data = {
                "conversation": cpt.conversation,
                "action": cpt.action,
                "info_suivi": cpt.info_suivi,
                "news": cpt.news
            }
            return render(request, 'messagerie/compteurnonlu.html', data)
        else:
            return HttpResponseForbidden(403)


class EnvoiMessageRecap(View):
    """
    Envoi d'un message de recap lors de la demande de l'utilisateur
    """
    def get(self, request):
        if not request.user.is_authenticated:
            return HttpResponseForbidden(403)
        user = request.user
        if user.recap:
            cpt = CptMsg.objects.get(utilisateur=user)
            if cpt.action or cpt.conversation or cpt.info_suivi or cpt.news:
                total = cpt.action + cpt.conversation + cpt.info_suivi + cpt.news
                action = cpt.action
                conv = cpt.conversation
                info = cpt.info_suivi
                nouv = cpt.news
                context = {
                    "total": total,
                    "action": action,
                    "conv": conv,
                    "info": info,
                    "nouv": nouv,
                }
                send = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                mail.send(user.email, send, template="msg_recap", context=context)
        return redirect('profile')
