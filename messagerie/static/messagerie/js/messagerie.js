$(function () {

    showcontexthelps();

    /*
    fonction de releve du courrier
     */
    function releve_du_courrier(limite, filtre){
        let url=url_prendremsg+"?limit="+limite+"&manif="+manif;
        if ($('#filter_suivi').is(":checked")){
            url+='&suivi=1';
        }
        if ($('#filter_action').is(':checked')){
            url+='&action=1'
        }
        if ($('#filter_conv').is(':checked')){
            url+='&conv=1'
        }
        if ($('#filter_tracabilite').is(':checked')){
            url+='&trac=1'
        }
        if ($('#filter_nouv').is(':checked')){
            url+='&nouv=1'
        }

        if ($('#filter_dossier').is(':checked')){
            url+='&dossier=1'
        }
        if ($('#filter_piece_jointe').is(':checked')){
            url+='&docjoin=1'
        }
        if ($('#filter_avis').is(':checked')){
            url+='&avis=1'
        }
        if ($('#filter_preavis').is(':checked')){
            url+='&preavis=1'
        }
        if ($('#filter_arrete').is(':checked')){
            url+='&arrete=1'
        }
        if ($('#filter_recepisse').is(':checked')){
            url+='&recepisse=1'
        }
        if ($('#filter_envoi').is(':checked')){
            url+='&envoi=1'
        }
        if ($('#filter_recu').is(':checked')){
            url+='&recu=1'
        }
        if (!$('#filter_nonlu').is(':checked')){
            url+='&nonlu=1'
        }
        if (filtre!==""){
            url=url_prendremsg+"?limit="+limite+"&manif="+manif+filtre
        }

        $.get(url, function(data){
            $('#messages').html(data)
        });
    }

    /*
    Fonction pour afficher un filtre à l'ouverture de la messagerie (uniquement en global)
     */

    let url_str = window.location.href;
    let url = new URL(url_str);
    let go_to = url.searchParams.get('cpt');
    let option='';
    $('#filter_nonlu').prop('checked', true)
    if (go_to){
        if (go_to==='conversation') {
            option+='&conv=1'
        }
        if (go_to==="action"){
            option+='&action=1'
        }
        if (go_to==="suivi"){
            option+="&suivi=1"
        }
        if (go_to==="nouveaute"){
            option+="&nouv=1"
        }
        filtre_custom=option+"&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1";
        let tab=filtre_custom.split('&');
        $('.filter_bar').prop('checked',false);
        $('#filter_nonlu').prop('checked', true)
        for (let i=0; i<tab.length;i++){
            if (tab[i]==='suivi=1'){
                $('#filter_suivi').prop('checked', true)
            }
            else if (tab[i]==='action=1'){
                $('#filter_action').prop('checked', true)
            }
            else if (tab[i]==='conv=1'){
                $('#filter_conv').prop('checked',true)
            }
            else if (tab[i]==='trac=1'){
                $("#filter_tracabilite").prop('checked', true)
            }
            else if (tab[i]==='nouv=1'){
                $('#filter_nouv').prop('checked', true)
            }
            else if (tab[i]==='dossier=1'){
                $('#filter_dossier').prop('checked', true)
            }
            else if (tab[i]==='docjoin=1'){
                $('#filter_piece_jointe').prop('checked', true)
            }
            else if (tab[i]==='avis=1'){
                $('#filter_avis').prop('checked', true)
            }
            else if (tab[i]==='preavis=1'){
                $('#filter_preavis').prop('checked', true)
            }
            else if (tab[i]==='arrete=1'){
                $('#filter_arrete').prop('checked', true)
            }
            else if (tab[i]==='recepisse=1'){
                $('#filter_recepisse').prop('checked', true)
            }
            else if (tab[i]==='envoi=1'){
                $('#filter_envoi').prop('checked', true)
            }
            else if (tab[i]==='recu=1'){
                $('#filter_recu').prop('checked', true)
            }
        }
    }

    /*
    fonction pour charger la boite d'envoi de la messsagerie
     */
    function affichage_envoi(){
        let url =url_envoiunique+"?manif="+manif;
        $.get(url, function (data) {
            $('#show_writediv').html(data)
        })
    }

    /*
    Trigger du bouton non lu
     */
    $('#nonlu_trigger').click(function () {
        $('#filter_nonlu').trigger('click')
    })


    /*
    fonction pour afficher la liste des filtres personnalisé
     */
    function affichage_filtre(){
        let url_filtre=url_filtreperso;
        $.get(url_filtre, function (data) {
            var filtres=JSON.parse(data);
            let display='<div class="barre_list_filtre"><div class="block_list_filtre">';
            for (let j=0; j<filtres.length; j++){
                display+='<div class="list_filtre"> <button class="filtre_perso_list" id="'+filtres[j].pk+'" value="'+filtres[j].fields.option+'">'+filtres[j].fields.titre+'</button>'+
                    '<button style="display:none;" class="btn btn-outline-danger filtre_perso_list_remove" id="supp_'+filtres[j].pk+'">Cliquez pour confirmer</button>'+
                    '<button style="display:none;" class="btn btn-outline-danger filtre_perso_list_confirm" id="supp_conf_'+filtres[j].pk+'">Cliquez pour supprimer '+filtres[j].fields.titre+' ?</button> </div>'

            }
            display+= '<div class="list_filtre"> <i class="supprimer btn_supp"></i>'+
                '<i style="display: none;" class="far fa-reply fa-fw anul_supp" title="Annuler la suppression" data-toggle="tooltip"></i></div></div></div>';
            $('#filtre_perso').html(display);
            if (filtres.length===0){
                $('#filtre_perso').hide();
            }
            else{
                $('#filtre_perso').show();
                AfficherIcones();
            }
        })
    }

    // on fait les fonctions au démarrage
    affichage_filtre();
    releve_du_courrier(limite, filtre_custom);


    // fonction de selection du filtre personnalisé,
    //on décoche tout les case puis on coche celle qui sont effectif dans ce filtre

    $('body').on('click', ".filtre_perso_list", function () {
        filtre_custom=$(this).attr('value');
        $('.filtre_perso_list').css('font-weight','normal');
        $(this).css('font-weight','bold');
        releve_du_courrier(limite, filtre_custom);
        let tab=filtre_custom.split('&');
        $('.filter_bar').prop('checked',false);
        $('#filter_nonlu').prop('checked', true)
        for (let i=0; i<tab.length;i++){
            if (tab[i]==='suivi=1'){
                $('#filter_suivi').prop('checked', true)
            }
            else if (tab[i]==='action=1'){
                $('#filter_action').prop('checked', true)
            }
            else if (tab[i]==='conv=1'){
                $('#filter_conv').prop('checked',true)
            }
            else if (tab[i]==='trac=1'){
                $("#filter_tracabilite").prop('checked', true)
            }
            else if (tab[i]==='nouv=1'){
                $('#filter_nouv').prop('checked', true)
            }
            else if (tab[i]==='dossier=1'){
                $('#filter_dossier').prop('checked', true)
            }
            else if (tab[i]==='docjoin=1'){
                $('#filter_piece_jointe').prop('checked', true)
            }
            else if (tab[i]==='avis=1'){
                $('#filter_avis').prop('checked', true)
            }
            else if (tab[i]==='preavis=1'){
                $('#filter_preavis').prop('checked', true)
            }
            else if (tab[i]==='arrete=1'){
                $('#filter_arrete').prop('checked', true)
            }
            else if (tab[i]==='recepisse=1'){
                $('#filter_recepisse').prop('checked', true)
            }
            else if (tab[i]==='envoi=1'){
                $('#filter_envoi').prop('checked', true)
            }
            else if (tab[i]==='recu=1'){
                $('#filter_recu').prop('checked', true)
            }
            else if (tab[i]==='nonlu=1'){
                $('#filter_nonlu').prop('checked', false)
            }
        }
    });

    // bouton only seulement le bouton cliquer sera selectionner

    $('.only_type').on('click', function () {
        let id = "#filter_" +  $(this).attr('id').split('_')[1]
        $('#filter_conv').prop('checked',false)
        $('#filter_action').prop('checked',false)
        $('#filter_suivi').prop('checked',false)
        $('#filter_nouv').prop('checked',false)
        $("#filter_tracabilite").prop('checked', false)
        $(id).prop('checked', true)
        releve_du_courrier(limite, filtre_custom)
    });

    $('.only_qui').on('click', function(){
        let id = "#filter_" +  $(this).attr('id').split('_')[1]
        $('#filter_envoi').prop('checked',false)
        $('#filter_recu').prop('checked',false)
        $(id).prop('checked', true)
        releve_du_courrier(limite, filtre_custom)

    })

    $('.only_objet').on('click', function () {
        let id = "#filter_" +  $(this).attr('id').split('|')[1]
        $('#filter_dossier').prop('checked',false)
        $('#filter_piece_jointe').prop('checked',false)
        $('#filter_avis').prop('checked',false)
        $('#filter_preavis').prop('checked',false)
        $('#filter_arrete').prop('checked',false)
        $('#filter_recepisse').prop('checked',false)
        $(id).prop('checked', true)
        releve_du_courrier(limite, filtre_custom)

    })

    //click suppression filtre

    $('body').on('click', '.btn_supp', function () {
        $('.filtre_perso_list_confirm').show();
        $('.filtre_perso_list').hide();
        $(this).hide();
        $('.anul_supp').show();
    });



    //annulation suppression filtre

    $('body').on('click', '.anul_supp', function () {
        $('.filtre_perso_list_confirm').hide();
        $('.filtre_perso_list_remove').hide();
        $('.filtre_perso_list').show();
        $(this).hide();
        $('.btn_supp').show();
    });


    //annulation lors de la confirmation

    $('body').on('click', '.filtre_perso_list_confirm', function () {
        let id = $(this).attr('id').split('_')[2];
        $(this).hide();
        $('#supp_'+id).show();
    });


    //suppresion du filtre

    $('body').on('click', '.filtre_perso_list_remove', function () {
        let id = $(this).attr('id').split('_')[1];
        let url = url_filtrepersodel+"?pk="+id;
        $.get(url, function (data) {
            if (data=='200'){
                affichage_filtre();
                $('.btn_supp').show();
            }
        })
    });



    //fonction pour afficher ou non les filtres

    var filtre_show = 0;
    $('#filtre_mail').click(function (e) {
        e.preventDefault();
        if (filtre_show===0){
            $('#collapsefiltre').fadeIn();
            filtre_show = 1;
            $(this).css('font-weight', 'bold').css('background-color', '#9aa400')
                .css('color', '#FFFFFF');
        }
        else {
            $('#collapsefiltre').fadeOut();
            filtre_show = 0;
            $(this).css('font-weight', 'normal').css('background-color', 'transparent')
                .css('color', '#212529');
        }
    });



    //fonction pour afficher le formulaire d'enregistrement d'un filtre perso

    $('#filtre_save').click(function () {
        $(this).fadeOut();
        $('#form_filtre').fadeIn()
    });


    //Fonction pour enregistrer le filtre personnel

    $('#form_filtre_submit').click(function () {
        let option='';
        if ($('#filter_suivi').is(":checked")){
            option+='&suivi=1';
        }
        if ($('#filter_action').is(':checked')){
            option+='&action=1'
        }
        if ($('#filter_conv').is(':checked')){
            option+='&conv=1';
        }
        if ($('#filter_tracabilite').is(':checked')){
            option+='&trac=1';
        }
        if ($('#filter_nouv').is(':checked')){
            option+='&nouv=1';
        }

        if ($('#filter_dossier').is(':checked')){
            option+='&dossier=1';
        }
        if ($('#filter_piece_jointe').is(':checked')){
            option+='&docjoin=1';
        }
        if ($('#filter_avis').is(':checked')){
            option+='&avis=1';
        }
        if ($('#filter_preavis').is(':checked')){
            option+='&preavis=1';
        }
        if ($('#filter_arrete').is(':checked')){
            option+='&arrete=1';
        }
        if ($('#filter_recepisse').is(':checked')){
            option+='&recepisse=1';
        }
        if ($('#filter_envoi').is(':checked')){
            option+='&envoi=1';
        }
        if ($('#filter_recu').is(':checked')){
            option+='&recu=1';
        }
        if (!$('#filter_nonlu').is(':checked')){
            option+='&nonlu=1';
        }
        let url_filtre=url_filtreperso;
        $.post(url_filtre, {titre:$('#nom_filtre').val(), option:option}, function (data) {
            if (data=='200'){
                affichage_filtre();
                $('#filtre_save').fadeIn();
                $('#form_filtre').fadeOut();
            }
            else{
                alert(data)
            }
        } )
    });


    // fonction du basculement d'affichage entre la lecture et l'écriture

    $('.navmail').click(function (e) {
        let id='#'+$(this).attr("id")+"div";
        e.preventDefault();
        if (id==='#show_writediv'){
            $('.div').hide();
            $(id).show();
            $('#li_non_lu').fadeOut();
            $('#filtre_perso').fadeOut();
            affichage_envoi();
            $('#show_mail').html('<a class="nav-link " href="#"><i class="retour mr-1"></i> Retour</a>');
            AfficherIcones();
            $('#show_write').fadeOut();
            $('#filter_bar').fadeOut();
            $('#filtre_mail').fadeOut();
            $('#collapsefiltre').fadeOut();
            $('#filtre_reset').fadeOut();
            filtre_show = 0
        }
        if (id==='#show_maildiv') {
            $('.div').hide();
            $('#show_write').fadeIn();
            $('#filtre_perso').fadeIn();
            $('#li_non_lu').fadeIn();
            $('#filter_bar').fadeIn();
            $('#filtre_mail').fadeIn();
            $('#filtre_reset').fadeIn();
            if ($('#show_mail').html()!=='<a class="nav-link " href="#"><i class="fal fa-redo-alt"></i>Actualiser</a>'){
                $('#filtre_mail').css('font-weight', 'normal');
            }
            $('#show_mail').html('<a class="nav-link " href="#"><i class="fal fa-redo-alt"></i>Actualiser</a>');
            $(id).fadeIn();
            $('#show_writediv').empty();
            $('#information').empty();
            releve_du_courrier(limite,filtre_custom);

        }


    });



    //fonction de selection manuelle des filtre, le filtre custum est effacé

    $('.filter_bar').click(function () {
        filtre_custom = "";
        $('.filtre_perso_list').css('font-weight','normal');
        releve_du_courrier(limite,filtre_custom)

    });


    //fonction pour reset les filtres

    $('#filtre_reset').click(function () {
        filtre_custom = "";
        $('.filter_bar').prop('checked', true);
        $('#filter_nonlu').prop('checked', false);
        $('.filtre_perso_list').css('font-weight','normal');
        releve_du_courrier(limite,filtre_custom);
    });


   // fonction pour afficher plus de mail +5 par palier

    $('#showMore').click(function () {
        limite +=20;
        releve_du_courrier(limite,filtre_custom);
    });

    // function pour augmenter la limite de message par 20 que le div charger est à l'ecran jusqu'à la limite donné par django

function createObserver(){
        var observer;

        var options = {
            root: null,
            rootMargin: "0px",
            threshold: 1.0
        };

        observer = new IntersectionObserver(handleIntersect, options);
        observer.observe(charger);
    }

    function handleIntersect(entries, observer){
        entries.forEach(entry=>{
            if (entry.isIntersecting){
                if (limite<max_env){
                    limite +=20;
                    releve_du_courrier(limite,filtre_custom);
                }

            }
        })
    }

    setTimeout(function () {
        charger = document.querySelector("#showMore");
            createObserver();
    }, 500);



})