$(function() { showcontexthelps(); });
// function qui va afficher la date.
// l'astuce est que l'on va comparer la date d'un message avec le precedant. s'il differe on créer une div et on y met la date dedans.
 function AfficherDate(){
        var clas= $('.title_msg');
        var ancien = '';
        for (var ligne in clas){

            if (clas[ligne].id){
                let myid= '#date_'+clas[ligne].id;
                if ($(myid).text() !== ancien){
                    $("<p class='date_msg'><span>" + $(myid).text() + '</span></p>').insertBefore("#"+clas[ligne].id)
                    ancien = $(myid).text()
                }
            }
        }
    }
    AfficherDate();

    AfficherIcones();


    // function faite quand l'utilisateur veut consulter un message.
    $('.title_msg').click(function (e) {
        if ($(this).hasClass('msg_header_open')){
            $('.message_body').empty().removeClass('message_body_open');
            $('.title_msg').removeClass('msg_header_open');
        }
        else {
            $('.message_body').empty().removeClass('message_body_open');
            $('.title_msg').removeClass('msg_header_open');
            $(this).addClass('msg_header_open');
            let id= '#body_'+$(this).attr('id');
            let pk=$(this).attr('id');
            let url_msg=url_afficherunmsg+"?pk="+$(this).attr('id');
            $(this).css('font-weight', 'normal').css('color', '#555555')
            $.get(url_msg,function (data) {
                $(id).html(data);
                $(id).addClass('message_body_open');
                $(id).append("<a href='"+url_msg+"&fen=1' target=\"_blank\" style='text-align:right; display:block; top:-0px; margin-left:auto; color:#97a200; margin-right:0.4em; width:12px; position:relative;'><i class=\"fenetre\"></i></a>")
                 AfficherIcones();
                // ici on envoi à django le fait que l'utilisateur à lu le message.
                $.post(url_lu+"?pk="+pk,function (data) {
                })
            })
        }

    })