$(function() { showcontexthelps(); });
// function pour afficher la liste des destinataire
$('#btn_destinataire').click(function (e) {
        e.preventDefault();
        $(this).hide();
        $('#collDestinataires').fadeIn();
});

// function pour afficher la div d'envoi de reponse
$('#repondre_affiche').click(function (e) {
        e.preventDefault();
        $.get(url_envoi_reponse+"?pk="+enve_pk, function (reponse) {
            $('#envoi_reponse_div').html(reponse).show();
            $('#repondre_affiche').hide();
        })
});
