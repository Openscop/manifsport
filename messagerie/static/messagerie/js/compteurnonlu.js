// function qui va créer le compteur et l'affiché.
function CPT() {
    if (is_log==="True"){
        $.ajax({
            url: url_cpt,
            type: "GET",
        }).done(function (data) {

            $('#cpt_msg_nav').html(data);
            // si django nous renvoi une erreur (comme un 403 ca rpàas co) on arrete le rechargement du compteur
        }).fail(function (data) {
            is_log="False"
        })

    }
}

// le compteur ce rechargera grace à ça/
$(function () {
    CPT();
    setInterval(function(){
        CPT()
    }, cpt_time_refresh*1000*60);
    AfficherIcones();

})
