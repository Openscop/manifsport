$(function() { showcontexthelps(); });
/*
    fonction pour charger la boite d'envoi de la messsagerie
     */
function affichage_envoi(){
    let url =url_envoiunique+"?manif="+manif;
    $.get(url, function (data) {
        $('#show_writediv').html(data)
    })
}


// ici on vas mettre en place chosen, on attente un peu sinon les script sont lancé avant que l'init de chosen soit finit

$(function () {
    $('.chosen-select').chosen();
    setTimeout(function () {

        $('#carnet_chosen').css('width', '100%');
        $('#doc_objet_chosen').css('width', '100%');
        // $('#service_chosen').css('width', '100%');
        $('#departement_actu_chosen').css('width', '100%');
        $('#departement_instru_chosen').css('width', '100%');
        $('#role_actu_chosen').css('width', '100%');
        $('#type_desti_chosen').css('width', '100%');
    },500)
    $('#attente_en_cours').fadeOut();
    $('#formmail_englobe').fadeIn();
})
AfficherIcones();

// fonction pour afficher la liste des services
function afficherService() {
    console.log("z")
    let dep = $('#departement_instru').val()
    if (typeof dep === "undefined"){
        dep = ""
    }
    let type = $('#type_desti').val()
    if (type){
        if (type.startsWith('orga')){


            let url = url_instrucarnet+"?idservice="+type+'&dep='+dep;
            var orga_manif = false
            if (manif && type.startsWith('orga|')){
                orga_manif = true
            }
            let spinner = '<i class="spinner"></i><span>Chargement en cours</span>';
            $('#intru_destinataire').empty().html(spinner).show();
            AfficherIcones();
            $.get(url, function (data) {
                let donne = JSON.parse(data);
                var display ="<select id=\"carnet\" class=\"chosen-select\" data-placeholder=\"Destinataires : laissez vide pour envoyer au service entier\" name=\"destinataires\" multiple=\"multiple\">\n";

                if (orga_manif){
                    for (let i=0; i<donne.length;i++){
                        display+='<option value="'+donne[i].pk+'" selected="selected">'+donne[i].service+' - '+donne[i].first_name+' '+donne[i].last_name+'</option>'
                    }
                }
                else {
                    for (let i=0; i<donne.length;i++){
                        display+='<option value="'+donne[i].pk+'">'+donne[i].service+' - '+donne[i].first_name+' '+donne[i].last_name+'</option>'
                    }
                }


                display+="</select>";

                $('#intru_destinataire').empty().html(display).show();
                $('#carnet').chosen({width: '100%'})
            })

        }
        else{
            $.get(`${url_listservice}?pk=${dep}&type=${type}`, function (response) {
                let donne = JSON.parse(response)
                let display= `<select id="service" class="chosen-select" name="service" data-placeholder="Sélectionnez le service destinataire" >
                <option disabled selected value="">Sélectionnez le service destinataire</option>`;
                for (let i=0; i<donne.length; i++){
                    display+=`<option  value="${donne[i].pk}|${donne[i].classname}">${donne[i].nom}</option>`;
                }
                display+=`</select>`;
                $('#intru_destinataire').empty().hide()
                $('#service_instru').empty().html(display).show()
                $("#service").chosen()
                $('#service_chosen').css('width', '100%');

            })
        }
    }

}


// function qui permet de changer le département lors d'envoi de conversation
$('#departement_instru').on("change", function (e) {
    e.preventDefault()
    afficherService()

})

// function pour choisir le type de destinataire
$('#type_desti').on('change', function (e) {
    e.preventDefault()
    afficherService()

})

// function qui va donner la liste des destinataire possible d'un service
$('body').on("change", "#service",function (e) {

    var val = $(this).val();
    var orga_manif = false
    if (manif && val.startsWith('orga|')){
        orga_manif = true
    }
    var dep = $('#departement_instru').val()
    let url = url_instrucarnet+"?idservice="+val+'&dep='+dep;

    let spinner = '<i class="spinner"></i><span>Chargement en cours</span>';
    $('#intru_destinataire').empty().html(spinner).show();
    AfficherIcones();
    $.get(url, function (data) {
        let donne = JSON.parse(data);
        var display ="<select id=\"carnet\" class=\"chosen-select\" data-placeholder=\"Destinataires : laissez vide pour envoyer au service entier\" name=\"destinataires\" multiple=\"multiple\">\n";

        if (orga_manif){
            for (let i=0; i<donne.length;i++){
                display+='<option value="'+donne[i].pk+'" selected="selected">'+donne[i].service+' - '+donne[i].first_name+' '+donne[i].last_name+'</option>'
            }
        }
        else {
            for (let i=0; i<donne.length;i++){
                display+='<option value="'+donne[i].pk+'">'+donne[i].service+' - '+donne[i].first_name+' '+donne[i].last_name+'</option>'
            }
        }

        display+="</select>";

        $('#intru_destinataire').empty().html(display).show();
        $('#carnet').chosen({width: '100%'})
    })
})

// function qui si on est dans une manif va permettre de selectionner le document associé à la manif
$('#doc_objet').change(function (e) {
    let val = $(this).val();
    if (manif){
        if (val==='avis' || val==='preavis' || val==='arrete' || val==='recepisse' || val==='piece_jointe'){
            let url = url_doc_associe+"?manif="+manif+"&"+val+"=1";
            $.get(url, function (data) {
                let datadecode= JSON.parse(data);
                if (datadecode.length===0){
                    $('#list_doc_associe').chosen("destroy");
                    $('#list_doc_objet').empty().hide();
                }
                else{
                    let display = "<select id='list_doc_associe' class='chosen-select' required name='doc_associe_link'>" +
                        "<option selected disabled value=''>Choissez le document concerné</option>";
                    for (let k=0; k<datadecode.length; k++){
                        display+= "<option value='"+datadecode[k].pk+"'>"+datadecode[k].nom+"</option>"
                    }
                    display+="</select>";
                    $('#list_doc_objet').html(display).show();
                    $('#list_doc_associe').chosen({width: "100%"})
                }


            })
        }
        else{
            $('#list_doc_associe').chosen("destroy");
            $('#list_doc_objet').empty().hide();
        }
    }


});

// on va verifier que tout est renseigner avant d'envoyer le mail
var tentative_envoi = 0;
$('#formmail').submit(function (e) {
    console.log(e)
    e.preventDefault();
    if (url_encour === url_envoiunique){
        if (is_instru===1){
            let is_orga = $('#type_desti').val().startsWith('orga|')
            if ((!$('#service').val() && !is_orga )|| !$('#doc_objet').val() || !$('#msg_objet').val()){
                if (!$('#service').val()){
                    $('#service_chosen').css('border', 'red 1px solid')
                }
                if (!$('#doc_objet').val()){
                    $('#doc_objet_chosen').css('border', 'red 1px solid')
                }
                if (!$('#msg_objet').val()){
                    $('#msg_objet').css('border', "red 1px solid")
                }
            }
            else {
                send_le_mail();
            }
        }
        else {
            if (!$('#carnet').val() || !$('#doc_objet').val() || !$('#msg_objet').val()){
                if (!$('#carnet').val()){
                    $('#carnet_chosen').css('border', 'red 1px solid')
                }
                if (!$('#doc_objet').val()){
                    $('#doc_objet_chosen').css('border', 'red 1px solid')
                }
                if (!$('#msg_objet').val()){
                    $('#msg_objet').css('border', "red 1px solid")
                }
            }
            else {
                send_le_mail();
            }
        }
    }
    else {
        if ( !$('#msg_objet').val()){
            if (!$('#msg_objet').val()){
                $('#msg_objet').css('border', "red 1px solid")
            }
        }
        else {
            send_le_mail();
        }
    }


})
// envoi du mail, le problème étant que ckeditor, et l'ajax c'est pas ça.
// le premier envoi donnera un formulaire incomplet. on le renvoi donc une seconde fois.
// si la seconde fois n'est pas passé on arrete l'erreur n'étant plus celà
function send_le_mail() {
    var data = $('#formmail').serialize();
    $.ajax({
        url: url_encour+"?manif="+manif,
        type: 'POST',
        data : data
    }).done(function (response) {
        $('#msg_envoi_alert').html("<div class='alert alert-success'>Message envoyé, redirection en cours</div>")
        setTimeout(function () {
            $('#show_mail').trigger("click")
        },1000)
    }).fail(function (response) {
        if (response.status===403 && tentative_envoi<2){
            tentative_envoi++;
            send_le_mail();
        }
        else{
            $('#msg_envoi_alert').html("<div class='alert alert-danger'>Erreur d'envoi du message</div>")
        }
    })
}

// ici c'est la fonction qui va switch entre l'envoi de conversation ou d'actualité
var url_encour = url_envoiunique;
$('#actualite_toogle').click(function (e) {
    if (!$(this).is(':checked')) {
        $('#actu_block').hide()
        $('.desti_block').show()
        $('#doc_objet_block').show()
        $('#list_doc_objet').show()
        // $('#formmailactu').attr('id', 'formmail');
        url_encour = url_envoiunique;
    } else {
        $('#actu_block').show()
        $('.desti_block').hide()
        $('#doc_objet_block').hide()
        $('#list_doc_objet').hide()
        // $('#formmail').attr('id', 'formmailactu')
        url_encour = url_envoi_actu
    }

})