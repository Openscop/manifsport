from __future__ import absolute_import, unicode_literals
from configuration.celery import app
from datetime import timedelta

from django.utils import timezone
from django.db.models import Q

from core.models import User
from administration.models.service import *
from evenements.models.manifestation import Manif
from messagerie.models import Message, Enveloppe
from organisateurs.models import Organisateur
from sports.models import Federation
from administrative_division.models import Commune

@app.task(bind=True)
def envoi_msg_celery(self, msg_json, type_envoi):
    """
    Fonction pour envoyer un message via les pages de messagerie
    :param msg_json: msg_json = {
                    'expediteur': expediteur,
                    'msg': corps du messages,
                    'objet': objet,
                    'option': {
                        'doc_pk': objet_lie_pk,
                        'doc_objet': objet_lie_nature,
                    },
                    'type': type_msg",
                    'manif': pk de la manif,
                    'destinataire': destinataires,
                    'reponse': pk de la reponse
                }
    Tout doit être sous format str ou serialisable
    :param type_envoi: type du message à envoyer (actualite, destinataire_service, destinataire_user)
    """


    if type_envoi == 'actualite':
        # création de la liste destinataire

        # ici si l'user a choisi un departement precis ou non
        from administrative_division.models import Departement
        tab_desti = []
        expediteur = User.objects.get(pk=msg_json['expediteur'])
        if msg_json['destinataire']['dep']:
            userindep = User.objects.none()
            dep = msg_json['destinataire']['dep']
            for departement in msg_json['destinataire']['dep']:
                if type(departement) == 'str':
                    departement = int(departement)
                obj_dep = Departement.objects.filter(pk=departement)
                useradd = User.objects.filter(Q(default_instance__departement__in=obj_dep))
                # On vas cela dans le cas où le departement reçoit une liste vide pour eviter les erreurs dans le second filtre
                userindep = userindep | useradd
        else:
            userindep = User.objects.all()

        # ici on va filtre ou non selon le role choisi
        if msg_json['destinataire']['role']:
            for user in userindep:
                for role in msg_json['destinataire']['role']:
                    if user.has_role(role):
                        tab_desti.append(user)
        else:
            for user in userindep:
                tab_desti.append(user)

        message = Message.objects.creer_et_envoyer(msg_json['type'], expediteur, tab_desti, msg_json['objet'],
                                         msg_json['msg'])


    # cas où on envoi le message à une liste d'utilisateur
    if type_envoi == 'destinataire_user':
        tab_desti = []
        for desti in msg_json['destinataire']:
            user = User.objects.get(pk=int(desti))
            tab_desti.append(user)
        expediteur = User.objects.get(pk=msg_json['expediteur'])
        manif = Manif.objects.get(pk=msg_json['manif']) if msg_json['manif'] else None
        reponse = msg_json.get('original', None)
        message = Message.objects.creer_et_envoyer(msg_json['type'], expediteur, tab_desti, msg_json['objet'],
                                         msg_json['msg'], manif, objet_lie_pk=msg_json['option']['doc_pk'],
                                         objet_lie_nature=msg_json['option']['doc_objet'], reponse_a_pk=reponse)

    if type_envoi == "destinataire_role":
        tab_desti=[]
        from administrative_division.models import Departement
        dep = Departement.objects.get(pk=msg_json['destinataire']['pk'])
        users = User.objects.filter(default_instance__departement=dep)
        for user in users:
            if user.has_role(msg_json['destinataire']['type']):
                tab_desti.append(user)
        expediteur = User.objects.get(pk=msg_json['expediteur'])
        manif = Manif.objects.get(pk=msg_json['manif']) if msg_json['manif'] else None
        message = Message.objects.creer_et_envoyer(msg_json['type'], expediteur, tab_desti, msg_json['objet'],
                                         msg_json['msg'], manifestation_liee=manif,
                                         objet_lie_pk=msg_json['option']['doc_pk'],
                                         objet_lie_nature=msg_json['option']['doc_objet'])
    # cas où on envoi à un service en entier
    if type_envoi == 'destinataire_service':

        tab_desti = []

        if msg_json['destinataire']['type'] =="new":
            from administrative_division.models import Departement
            dep = Departement.objects.filter(pk=msg_json['destinataire']['pk'])
            dep = dep.get() if dep else None
            last_year = timezone.now() - timedelta(days=360)
            orgas = Organisateur.objects.filter(user__default_instance__departement=dep,
                                                user__last_login__gte=last_year)
            for orga in orgas:
                tab_desti.append(orga.user)

        elif msg_json['destinataire']['type'] =="old":
            from administrative_division.models import Departement
            dep = Departement.objects.filter(pk=msg_json['destinataire']['pk'])
            dep = dep.get() if dep else None
            orgas = Organisateur.objects.filter(user__default_instance__departement=dep)
            for orga in orgas:
                tab_desti.append(orga.user)

        elif msg_json['destinataire']['type'] == 'Service':

            service = Service.objects.get(pk=msg_json['destinataire']['pk'])
            agents = service.serviceagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == 'Mairie':

                commune = Commune.objects.get(pk=msg_json['destinataire']['pk'])
                agents =  commune.get_mairieagents()
                for agent in agents:
                    user = agent.user
                    tab_desti.append(user)

        elif msg_json['destinataire']['type'] == 'Federation':

            federation = Federation.objects.get(pk=msg_json['destinataire']['pk'])
            agents = federation.federationagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "Prefecture":
            pref = Prefecture.objects.get(pk=msg_json['destinataire']['pk'])
            intructeur = pref.instructeurs.all()
            for agent in intructeur:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "SDIS":
            sdis = SDIS.objects.get(pk=msg_json['destinataire']['pk'])
            agents = sdis.sdisagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "GGD":
            ggd = GGD.objects.get(pk=msg_json['destinataire']['pk'])
            agents = ggd.ggdagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "EDSR":
            edsr = EDSR.objects.get(pk=msg_json['destinataire']['pk'])
            agents = edsr.edsragents.all()
            agentslocal = edsr.edsragentslocaux.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

            for agent in agentslocal:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "DDSP":
            ddsp = DDSP.objects.get(pk=msg_json['destinataire']['pk'])
            agents = ddsp.ddspagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "Compagnie":
            comp = Compagnie.objects.get(pk=msg_json['destinataire']['pk'])
            agents = comp.compagnieagentslocaux.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "Commissariat":
            comm = Commissariat.objects.get(pk=msg_json['destinataire']['pk'])
            agents = comm.commissariatagentslocaux.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "CODIS":
            codis = CODIS.objects.get(pk=msg_json['destinataire']['pk'])
            agents = codis.codisagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "CIS":
            cis = CIS.objects.get(pk=msg_json['destinataire']['pk'])
            agents = cis.cisagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "CGService":
            cgs = CGService.objects.get(pk=msg_json['destinataire']['pk'])
            agents = cgs.cgserviceagentslocaux.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "CGD":
            cgd = CGD.objects.get(pk=msg_json['destinataire']['pk'])
            agents = cgd.ggdagentslocaux.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "CG":
            cg = CG.objects.get(pk=msg_json['destinataire']['pk'])
            agents = cg.cgagents.all()
            agentssup = cg.cgsuperieurs.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)

            for agent in agentssup:
                user = agent.user
                tab_desti.append(user)

        elif msg_json['destinataire']['type'] == "Brigade":
            brigade = Brigade.objects.get(pk=msg_json['destinataire']['pk'])
            agents = brigade.brigadeagents.all()
            for agent in agents:
                user = agent.user
                tab_desti.append(user)
        expediteur = User.objects.get(pk=msg_json['expediteur'])
        manif = Manif.objects.get(pk=msg_json['manif']) if msg_json['manif'] else None
        message = Message.objects.creer_et_envoyer(msg_json['type'], expediteur, tab_desti, msg_json['objet'],
                                         msg_json['msg'], manifestation_liee=manif,
                                         objet_lie_pk=msg_json['option']['doc_pk'],
                                         objet_lie_nature=msg_json['option']['doc_objet'])

    # ici on va voir si l'utilisateur s'est envoyé lui même le message on le passera en lu
    if message and expediteur:
        env_auto_envoye = Enveloppe.objects.filter(expediteur_id=expediteur, destinataire_id=expediteur, corps=message)
        if env_auto_envoye:
            env_auto_envoye = env_auto_envoye.get()
            env_auto_envoye.lu_datetime = timezone.now()
            env_auto_envoye.save()