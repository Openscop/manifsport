from .models.message import Enveloppe
from evenements.models import Manif
from instructions.views.tableaudebord import get_instructions
from core.models.user import User
from core.util.user import UserHelper


def get_carnet(request):
    moi = request.user
    carnet = []

    # dans le cas d'un instructeur on va chercher toute les personnes de son instance.
    if hasattr(moi, 'instructeur') and moi.instructeur:
        users = User.objects.filter(default_instance=moi.default_instance, is_active=True)
        for user in users:
            carnet.append(user)

    # dans le cas d'un organisateur on prend les instructeurs de ses manifestations
    elif hasattr(moi, 'organisateur') and moi.organisateur:
        manifs = Manif.objects.filter(structure=moi.organisateur.structure)
        for manif in manifs:
            instruction = manif.get_instruction()
            if instruction:
                instructeurs = instruction.get_instructeurs()
                for instructeur in instructeurs:
                    carnet.append(instructeur.user) if instructeur.user not in carnet else ''

    # Pour un agent on va chercher d'abord tout les manifs dont il a accès
    elif hasattr(moi, 'agent') and moi.agent:
        role = UserHelper.get_role_name(moi)
        instance = moi.get_instance()
        filtre_specifique = request.GET.get('filtre_specifique', default=None)
        temporalite = "sans"

        # avoir la liste des instructions
        instructions = get_instructions(moi, temporalite, filtre_specifique, role, instance, request)

        # pour chaque instruction on va chercher les organisateurs
        for instru in instructions:
            user = instru.manif.get_organisateur().user
            carnet.append(user) if user not in carnet else ""

    # puis on rajoute les personnes qui ont envoyé un message à l'user
    messagerecus = Enveloppe.objects.filter(destinataire_id=moi).distinct('expediteur_id').all()
    for messagerecu in messagerecus:
        if messagerecu.expediteur_id and messagerecu.expediteur_id not in carnet:
            carnet.append(messagerecu.expediteur_id)

    return carnet
