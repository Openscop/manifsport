from django.forms import ModelForm
from django import forms
from ckeditor.widgets import CKEditorWidget

from ..models import Message


class MessageForm(ModelForm):
    corps = forms.CharField(widget=CKEditorWidget('minimal'), label='')

    class Meta:
        model = Message
        fields = ['corps']
