import threading


from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone

from django.db import models
from post_office import mail
from webpush import send_user_notification
from ckeditor.fields import RichTextField

from core.models.optionuser import OptionUser, User
from instructions.models import Avis, PreAvis, DocumentOfficiel
from evenements.models.manifestation import Manif
from configuration import settings


class PushThread(threading.Thread):
    def __init__(self, user):
        self.user = user
        threading.Thread.__init__(self)

    def run(self):
        payload = {
            "head": "Manifestation Sportive",
            "body": "Vous avez reçu un nouveau message",
        }
        send_user_notification(user=self.user, payload=payload, ttl=1000)


class MessageQuerySet(models.QuerySet):

    def creer_et_envoyer(self, type_msg, expediteur, destinataires, titre, contenu, manifestation_liee=None,
                         objet_lie_nature=None, objet_lie_pk=0, reponse_a_pk=None):
        """
        :param type_msg: type de l'enveloppe (conversation, news, action,  info_suivi) cf : Enveloppe.LISTE_TYPE
        :param expediteur: model user si Null = plateforme
        :param destinataires: tableau avec des User ou des parents d'User ou des structures (qui ont un email associé).
        :param titre: text
        :param contenu: text brut ou html
        :param manifestation_liee: model manifestation concerné par le message
        :param objet_lie_nature: type de l'objet concerné par le message(dossier, piece_jointe, avis, preavis, arrete, recepisse) cf : Enveloppe.LISTE_DOC_OBJET
        :param objet_lie_pk: pk de l'objet concerné par le message
        :param reponse_a_pk: pk du message original de la conversation (si ce message est une réponse)
        :return:
        """
        # creation du message (le corps et l'object (avis preavis ou doc associé)
        if objet_lie_pk:
            obj = None
            if objet_lie_nature == 'avis':
                obj = Avis.objects.get(pk=objet_lie_pk)
            elif objet_lie_nature == 'preavis':
                obj = PreAvis.objects.get(pk=objet_lie_pk)
            elif objet_lie_nature == 'piece_jointe':
                from evenements.models import DocumentComplementaire
                obj = DocumentComplementaire.objects.get(pk=objet_lie_pk)
            elif objet_lie_nature == 'arrete' or objet_lie_nature == 'recepisse':
                obj = DocumentOfficiel.objects.get(pk=objet_lie_pk)
            message = self.create(corps=contenu, content_object=obj)
        else:
            message = self.create(corps=contenu)

        # definition de l'expediteur en fonction de son existance ou non
        if expediteur:
            expediteur = getattr(expediteur, 'user', expediteur)
            if expediteur.__class__.__name__ == 'User':
                if expediteur.has_role('mairieagent'):
                    expediteur_txt = 'Mairie de ' + str(expediteur.get_service())
                else:
                    expediteur_txt = str(expediteur.get_service())
            else:
                expediteur_txt = str(expediteur)
                expediteur = None
        else:
            expediteur_txt = 'Manifestation sportive'

        # compte des destinataires
        nombre_destinataire = len(destinataires)

        # boucle pour créer les enveloppes
        for destinataire in destinataires:

            # verification si le destinataire est User
            destinataire = getattr(destinataire, 'user', destinataire)
            if destinataire.__class__.__name__ == "User":
                if destinataire.tableau_role:
                    destinataire_txt = str(
                        destinataire.get_service()) + '→' + destinataire.first_name + ' ' + destinataire.last_name
                else:
                    destinataire_txt = str(destinataire.first_name + '→' + destinataire.last_name)
            else:
                # sinon User non existant on envoi un mail d'office
                # et on créé un message blanc sans destinataire rataché
                if hasattr(destinataire, 'email') and destinataire.email:

                    enveloppe = Enveloppe(corps=message, expediteur_id=expediteur, expediteur_txt=expediteur_txt,
                                          destinataire_id=None, destinataire_txt=str(destinataire),
                                          objet=titre,
                                          type=type_msg, manifestation=manifestation_liee, lu_requis=False,
                                          action_traitee=False,
                                          affichage_messagerie=True, notif_bureau=False, doc_objet=objet_lie_nature,
                                          nombre_destinataire=nombre_destinataire)
                    enveloppe.save()
                    enveloppe.mail_nouveau_msg(email=destinataire.email)
                    msg = mail.Email.objects.latest('pk')
                    enveloppe.envoi_courriel = msg
                    enveloppe.save()

            if destinataire.__class__.__name__ == "User" and destinataire.is_active:
                option = OptionUser.objects.get(user=destinataire)

                lurequis = False
                affichage_messagerie = False
                envoi_courriel = False
                notif_bureau = False

                if objet_lie_pk :
                    action = False
                else:
                    action = None

                # Definition des notifications faites en fonction du type de message et des options du destinataire
                for obj, attrib in [('conversation', 'conversation'), ('news', 'nouveaute'), ('action', 'action'), ('info_suivi', 'notification'), ('tracabilite', 'tracabilite')]:
                    if type_msg == obj:
                        if getattr(option, attrib + '_mail'):
                            envoi_courriel = True
                        if getattr(option, attrib + '_push'):
                            notif_bureau = True
                        if getattr(option, attrib + '_msg'):
                            affichage_messagerie = True
                            lurequis = True

                enveloppe = Enveloppe(corps=message, expediteur_id=expediteur, expediteur_txt=expediteur_txt,
                                      destinataire_id=destinataire, destinataire_txt=destinataire_txt, objet=titre,
                                      type=type_msg, manifestation=manifestation_liee, reponse=reponse_a_pk, lu_requis=lurequis, action_traitee=action,
                                      affichage_messagerie=affichage_messagerie, notif_bureau=notif_bureau,
                                      doc_objet=objet_lie_nature, nombre_destinataire=nombre_destinataire)

                enveloppe.save()

                # on envoi une notification push si le destinataire le veut
                if notif_bureau:
                    PushThread(destinataire).start()
                # on envoie un message si le destinataire le veut
                if envoi_courriel:
                    enveloppe.mail_nouveau_msg()
                    msg = mail.Email.objects.latest('pk')
                    enveloppe.envoi_courriel = msg
                    enveloppe.save()

        return message


class Message(models.Model):
    corps = RichTextField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    objects = MessageQuerySet.as_manager()

    def __str__(self):
        return self.corps[:10]

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"
        ordering = ["pk"]


class Enveloppe(models.Model):
    """
    Model qui détient les détails du messages
    """
    LISTE_TYPE = [('conversation', 'Conversation'), ('news', 'News de la plateforme'),
                  ('info_suivi', 'Information de suivi'), ('action', "Demande d'action"), ('tracabilite', 'Traçabilité')]

    LISTE_DOC_OBJET = [('dossier', 'Dossier'), ('piece_jointe', 'Document joint'), ('avis', 'Avis'), ('preavis', 'Préavis'),
                       ('arrete', 'Arrêté'), ('recepisse', 'Récépissé')]

    expediteur_id = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE,
                                      related_name='message_expediteur')
    expediteur_txt = models.CharField(max_length=500)
    destinataire_id = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True,  on_delete=models.CASCADE,
                                        related_name='message_destinataire')
    destinataire_txt = models.CharField(max_length=500)
    nombre_destinataire = models.IntegerField(blank=True, null=True)
    objet = models.CharField(max_length=500)
    doc_objet = models.CharField(max_length=30, choices=LISTE_DOC_OBJET, blank=True, null=True)
    corps = models.ForeignKey(Message, on_delete=models.CASCADE, related_name='message_enveloppe')

    type = models.CharField(max_length=30, choices=LISTE_TYPE)
    date = models.DateTimeField(default=timezone.now, blank=True)
    manifestation = models.ForeignKey(Manif, on_delete=models.CASCADE, blank=True, null=True,
                                      related_name='message_manifestation')
    manif_terminee = models.BooleanField(default=False)
    reponse = models.IntegerField(blank=True, null=True)

    lu_requis = models.BooleanField()
    lu_datetime = models.DateTimeField(null=True, blank=True)
    action_traitee = models.BooleanField(null=True, blank=True)

    affichage_messagerie = models.BooleanField()
    envoi_courriel = models.OneToOneField(mail.Email, on_delete=models.CASCADE, null=True, blank=True)
    notif_bureau = models.BooleanField()

    def mail_nouveau_msg(self, email=None):
        """
        Envoi d'un email de notification
        """
        desti_mail = email if email else self.destinataire_id.email
        
        # l'adresse de l'expéditeur est définie au niveau de l'instance départementale (normalement)
        expe_mail = None
        if self.expediteur_id:
            expe_mail = self.expediteur_id.get_instance().get_email()
        if not expe_mail and self.destinataire_id:
            expe_mail = self.destinataire_id.get_instance().get_email()
        if not expe_mail:
            expe_mail = settings.DEFAULT_FROM_EMAIL
            
        titre = self.objet
        url = settings.MAIN_URL + "messagerie/message/?pk=" + str(self.pk) + "&fen=1"
        mail.send(
            desti_mail,
            expe_mail,
            subject=titre,
            html_message='Bonjour,<br><br>Vous avez un nouveau message sur la plateforme manifestation sportive.<br>'
                         'Veuillez cliquer ci dessous pour en prendre connaissance.<br>'
                         '<a href="' + url + '" target="_blank">' + url + '</a>',
        )

    def get_url_manif_associe(self, user):
        """
        Permet d'avoir l'url d'une manifestation selon l'utilisateur qui la consulte.
        """
        url = None
        if self.manifestation:
            if user.has_role('organisateur'):
                url = self.manifestation.get_cerfa().get_absolute_url()
            elif user.has_role('instructeur') or user.has_role('mairieagent'):
                url = self.manifestation.get_instruction().get_absolute_url()
            elif hasattr(user, 'agent'):
                url = self.manifestation.get_instruction().get_avis_user(user).get_absolute_url()
            elif hasattr(user, 'agentlocal'):
                url = self.manifestation.get_instruction().get_preavis_user(user).get_absolute_url()
        return url
