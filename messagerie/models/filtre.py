from django.db import models

from configuration import settings


class Filtre(models.Model):
    """
    filtre d'affichage custom des utilisateurs
    """
    titre = models.CharField(max_length=50)
    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='filtre_utilisateur')
    option = models.CharField(max_length=250)

    def __str__(self):
        return self.titre
