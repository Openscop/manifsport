from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe
from related_admin import RelatedFieldAdmin

from messagerie.models import *


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    readonly_fields = ["corps", "content_object", "content_type", "object_id"]


@admin.register(Filtre)
class FiltreAdmin(admin.ModelAdmin):
    list_display = ('titre', 'utilisateur',)


@admin.register(Enveloppe)
class EnveloppeAdmin(RelatedFieldAdmin):

    list_display = ('id', 'expediteur_txt', 'destinataire_txt', 'manifestation',
                    'date', 'type', "doc_objet",  'objet', 'corps_du_message')
    search_fields = ["manifestation__nom"]
    exclude = ['corps']
    list_filter = ("type","doc_objet",)
    list_per_page = 500
    readonly_fields = ['manifestation', "expediteur_id","expediteur_txt", "destinataire_id", "destinataire_txt",
                       "nombre_destinataire", "objet", "doc_objet","corps_du_message" ,  "type", "date", "manif_terminee",
                       "reponse", "lu_requis", "lu_datetime", "action_traitee", "affichage_messagerie",
                       "envoi_courriel", "notif_bureau"]

    fieldsets = (
        ("Utilisateurs", {'fields': ("expediteur_id","expediteur_txt", "destinataire_id", "destinataire_txt",
                       "nombre_destinataire")}),
        ("Corps",
         {'fields': ("objet", "doc_objet", "corps_du_message", "type", "date",
                     "reponse")}),
        ("Manifestation",
         {'fields': ('manifestation', "manif_terminee")}),
        ("Notification",
         {'fields': ( "lu_requis", "lu_datetime", "action_traitee", "affichage_messagerie",
                     "envoi_courriel", "notif_bureau")}),
    )

    def corps_du_message(self, obj):
        link = reverse("admin:messagerie_message_change", args=[obj.corps.id])  # model name has to be lowercase
        return mark_safe(u'<a href="%s">%s</a>' % (link, obj.objet))
    corps_du_message.short_description = "Voir le message"

    corps_du_message.allow_tags = True
