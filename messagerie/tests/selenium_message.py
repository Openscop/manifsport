from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from celery.contrib.testing.worker import start_worker
from configuration.celery import app
from django.contrib.auth.hashers import make_password as make
from allauth.account.models import EmailAddress
from django.test import tag
import re, time, os

from core.models import Instance

from core.factories import UserFactory
from evenements.factories import DcnmFactory
from organisateurs.factories import OrganisateurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory, CGDAgentFactory, DDSPAgentFactory,\
    CommissariatAgentFactory, MairieAgentFactory, CGAgentFactory, CGServiceAgentFactory, CGSuperieurFactory,\
    SDISAgentFactory, GroupementAgentFactory, CODISAgentFactory, CISAgentFactory, CGDFactory
from administration.factories import BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory, CISFactory
from organisateurs.factories import StructureFactory
from sports.factories import ActiviteFactory, FederationFactory


class SeleniumMessagerieTests(StaticLiveServerTestCase):
    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets sur le 42
            Création des utilisateurs
            Création de l'événement
        """
        print()
        print('meh')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(800, 800)

        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123),
                                              default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True,
                                    verified=True)
        structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        instructeur.is_superuser = True
        instructeur.save()
        EmailAddress.objects.create(user=instructeur, email='instructeur@example.com', primary=True, verified=True)
        InstructeurFactory.create(user=instructeur, prefecture=prefecture)
        agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_fede, email='agent_fede@example.com', primary=True, verified=True)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=agent_fede, federation=fede)
        agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_ggd, email='agent_ggd@example.com', primary=True, verified=True)
        GGDAgentFactory.create(user=agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_edsr, email='agent_edsr@example.com', primary=True, verified=True)
        EDSRAgentFactory.create(user=agent_edsr, edsr=dep.edsr)
        cls.cgd = CGDFactory.create(commune=cls.commune)
        agent_cgd = UserFactory.create(username='agent_cgd', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cgd, email='agent_cgd@example.com', primary=True, verified=True)
        CGDAgentFactory.create(user=agent_cgd, cgd=cls.cgd)
        cls.brigade = BrigadeFactory.create(kind='bta', cgd=cls.cgd, commune=cls.commune)
        agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_ddsp, email='agent_ddsp@example.com', primary=True, verified=True)
        DDSPAgentFactory.create(user=agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_commiss, email='agent_commiss@example.com', primary=True, verified=True)
        CommissariatAgentFactory.create(user=agent_commiss, commissariat=cls.commiss)
        agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_mairie, email='agent_mairie@example.com', primary=True, verified=True)
        MairieAgentFactory.create(user=agent_mairie, commune=cls.commune)
        agent_cg = UserFactory.create(username='agent_cg', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cg, email='agent_cg@example.com', primary=True, verified=True)
        CGAgentFactory.create(user=agent_cg, cg=dep.cg)
        cls.cgserv = CGServiceFactory.create(name='STD_test', cg=dep.cg, service_type='STD')
        agent_cgserv = UserFactory.create(username='agent_cgserv', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cgserv, email='agent_cgserv@example.com', primary=True, verified=True)
        CGServiceAgentFactory.create(user=agent_cgserv, cg_service=cls.cgserv)
        agent_cgsup = UserFactory.create(username='agent_cgsup', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cgsup, email='agent_cgsup@example.com', primary=True, verified=True)
        CGSuperieurFactory.create(user=agent_cgsup, cg=dep.cg)
        agent_sdis = UserFactory.create(username='agent_sdis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_sdis, email='agent_sdis@example.com', primary=True, verified=True)
        SDISAgentFactory.create(user=agent_sdis, sdis=dep.sdis)
        cls.group = CompagnieFactory.create(sdis=dep.sdis, number=98)
        agent_group = UserFactory.create(username='agent_group', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_group, email='agent_group@example.com', primary=True, verified=True)
        GroupementAgentFactory.create(user=agent_group, compagnie=cls.group)
        agent_codis = UserFactory.create(username='agent_codis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_codis, email='agent_codis@example.com', primary=True, verified=True)
        CODISAgentFactory.create(user=agent_codis, codis=dep.codis)
        cls.cis = CISFactory.create(name='CIS_test', compagnie=cls.group, commune=cls.commune)
        agent_cis = UserFactory.create(username='agent_cis', password=make(123), default_instance=dep.instance)
        EmailAddress.objects.create(user=agent_cis, email='agent_cis@example.com', primary=True, verified=True)
        CISAgentFactory.create(user=agent_cis, cis=cls.cis)

        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=structure, nom='Manifestation_Test',
                                               villes_traversees=(cls.autrecommune,), activite=activ)
        cls.manifestation.nb_participants = 1
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'un gros parcours'
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 6
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie'):
            mon_fichier = open(file + ".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

        cls.celery_worker = start_worker(app)
        cls.celery_worker.__enter__()
        super(SeleniumMessagerieTests, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Arrêt du driver sélénium
        """

        cls.selenium.quit()
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'itineraire_horaire',
                     'cartographie'):
            os.remove(file + ".txt")
        super(SeleniumMessagerieTests, cls).tearDownClass()
        cls.celery_worker.__exit__(None, None, None)


    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_messagerie(self):
        """
        Test de la messagerie
        """

        def connexion(username):
            """ Connexion de l'utilisateur 'username' """
            self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
            pseudo_input = self.selenium.find_element_by_name("login")
            pseudo_input.send_keys(username)
            time.sleep(self.DELAY)
            password_input = self.selenium.find_element_by_name("password")
            password_input.send_keys('123')
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
            time.sleep(self.DELAY * 4)
            self.assertIn('Connexion avec '+username+' réussie', self.selenium.page_source)


        def deconnexion():
            """ Deconnexion de l'utilisateur """
            self.selenium.find_element_by_class_name('navbar-toggler').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_class_name('deconnecter').click()
            time.sleep(self.DELAY)
            self.selenium.find_element_by_xpath('//button[@type="submit"]').click()

        print('***test de la messagerie***')
        connexion('instructeur')
        #affichage de la messagerie
        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//a[@title="Messagerie"]').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('menu_msg').click()
        time.sleep(self.DELAY)

        #envoi d'un msg
        self.selenium.find_element_by_id("show_write").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 5000)")
        time.sleep(self.DELAY)
        self.chosen_select('service_chosen', 'Organisateur inactif durant l\'année')
        time.sleep(self.DELAY)
        self.chosen_select('carnet_chosen', 'Grs0 - Bob Robert')
        time.sleep(self.DELAY)
        self.chosen_select('doc_objet_chosen', 'Dossier')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id("msg_objet").send_keys('premier')
        #ici on change pour ecrire dans l'iframe
        frame = self.selenium.find_element_by_class_name('cke_wysiwyg_frame')
        self.selenium.switch_to.frame(frame)
        body = self.selenium.find_element_by_tag_name('body')
        body.send_keys('tester')
        self.selenium.switch_to.default_content()
        self.selenium.find_element_by_id("envoi").click()
        time.sleep(self.DELAY)

        # on va verifier sa precense dans la liste des mails
        self.selenium.find_element_by_id('show_mail').click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_id('2').click()
        time.sleep(self.DELAY)
        self.selenium.find_elements_by_id("corps_msg_2")
        time.sleep(self.DELAY)

        # on va verifier les filtres
        self.selenium.execute_script("window.scroll(0, 800)")
        test = self.selenium.find_elements_by_id('filtre_mail')[0].click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_elements_by_id('filtre_save')[0].click()
        time.sleep(self.DELAY)
        self.selenium.find_elements_by_id('nom_filtre')[0].send_keys('aze')
        self.selenium.find_elements_by_id("form_filtre_submit")[0].click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('filtre_perso_list').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name("supprimer").click()
        time.sleep(self.DELAY)
        self.selenium.find_elements_by_id("supp_conf_1")[0].click()
        time.sleep(self.DELAY)
        self.selenium.find_elements_by_id("supp_1")[0].click()




