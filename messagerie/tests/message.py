from django.test import TestCase
from ..models.message import Message
from django.contrib.auth.hashers import make_password as make
from core.models import Instance
from core.factories import UserFactory
from organisateurs.factories import OrganisateurFactory
from evenements.factories import DcnmFactory
from instructions.models import Instruction, PreAvis
from organisateurs.factories import OrganisateurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, DDSPAgentFactory, CommissariatAgentFactory, MairieAgentFactory
from administration.factories import CommissariatFactory
from organisateurs.factories import StructureFactory
from sports.factories import ActiviteFactory, FederationFactory
from post_office.models import EmailTemplate
import re, os, time


class MessagerieTests(TestCase):

    @classmethod
    def setUpTestData(cls):

        print()
        print('========== Message ===========')

        # Création des objets sur le 78
        cls.dep = dep = DepartementFactory.create(name='78',
                                                  instance__name="instance de test",
                                                  instance__workflow_ggd=Instance.WF_GGD_EDSR,
                                                  instance__instruction_mode=Instance.IM_DEPARTEMENT)
        arrondissement = ArrondissementFactory.create(name='Versailles', code='99', departement=dep)
        cls.prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Plaisir', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        cls.organisateur = UserFactory.create(username='organisateur', password=make(123),
                                              default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)

        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=cls.agent_fede, federation=fede)
        cls.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance)
        DDSPAgentFactory.create(user=cls.agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        cls.agent_commiss = UserFactory.create(username='agent_commiss', password=make(123),
                                               default_instance=dep.instance)
        CommissariatAgentFactory.create(user=cls.agent_commiss, commissariat=cls.commiss)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123),
                                              default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)

        EmailTemplate.objects.create(name='new_msg')

        # Création de l'événement
        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=cls.structure, activite=activ,
                                               nom='Manifestation_Test', villes_traversees=(cls.autrecommune,))
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'parcours de 10Km'
        cls.manifestation.nb_participants = 1
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        # Création de l'événement
        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=cls.structure, activite=activ,
                                               nom='Manifestation_Testqsdqs', villes_traversees=(cls.autrecommune,))
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'parcours de 10Km'
        cls.manifestation.nb_participants = 1
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 3
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire'):
            mon_fichier = open(file + ".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire'):
            os.remove(file + ".txt")
        super(MessagerieTests, cls).tearDownClass()


    def test_affichagemessagerie(self):

        #Affichage sans login 403
        retour = self.client.get("/message/", HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 403)
        self.assertTrue(self.client.login(username="organisateur", password='123'))

        # affichage messagerie principale
        retour = self.client.get("/message/", HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, 'Messagerie')

        # affichage du body de la messagerie global
        retour = self.client.get("/message/mail/", HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Votre messagerie rassemble")

        # affichage du compteur
        retour = self.client.get('/message/cpt/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        # affichage de la liste des msg
        retour = self.client.get('/message/prendremsg/?limit=10&manif=&suivi=1&action=1&conv=1&nouv=1&dossier=1&docjoin=1&avis=1&preavis=1&arrete=1&recepisse=1&envoi=1&recu=1&nonlu=1', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "Destinataire/Expediteur")

        # affichage du filtre
        retour = self.client.get('/message/filtre/', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        # envoi d'un message
        retour =  self.client.get('/message/envoiunique/?manif=', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(retour, "<div id=\"formmail")

        # recuperation du carnet instructeur 403 normalement
        retour = self.client.get('/message/intrucarnet/?idservice=orga', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 403)

        # envoi du msg
        data = {
            "service": "orga",
            'destinataires': "1",
            "doc_objet": "dossier",
            "objet": "test",
            "corps": "<p>test</p>"
        }
        retour = self.client.post('/message/envoiunique/?manif=', data=data, HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)

        # delog pour log en instructeur
        self.client.logout()
        self.assertTrue(self.client.login(username="instructeur", password='123'))

        # recuperation du carnet instructeur
        retour = self.client.get('/message/intrucarnet/?idservice=orga', HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(retour.status_code, 200)


