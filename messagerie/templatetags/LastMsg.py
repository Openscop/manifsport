from django import template
from messagerie.models import Enveloppe

register = template.Library()


@register.filter()
def LastMsg(value):
    env = Enveloppe.objects.filter(manifestation=value).filter(type='action').last()
    if env:
        return env.date
    else:
        return None
