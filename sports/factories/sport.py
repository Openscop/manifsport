# coding: utf-8
import factory

from sports.factories import FederationFactory
from sports.models import Discipline, Activite


class DisciplineFactory(factory.django.DjangoModelFactory):
    """ Factory discipline sportive """

    # Champs
    name = factory.Sequence(lambda n: 'Discipline{0}'.format(n))

    # Meta
    class Meta:
        model = Discipline


class ActiviteFactory(factory.django.DjangoModelFactory):
    """ Factory activité sportive """

    # Champs
    name = factory.Sequence(lambda n: 'Activite{0}'.format(n))
    discipline = factory.SubFactory(DisciplineFactory)

    # Meta
    class Meta:
        model = Activite
