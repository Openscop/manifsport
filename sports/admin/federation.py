# coding: utf-8
from django.contrib import admin
from django.contrib.admin.filters import RelatedOnlyFieldListFilter
from django.db.models.aggregates import Count
from import_export.admin import ExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from sports.forms import FederationForm
from ..models import Federation


@admin.register(Federation)
class FederationAdmin(ExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['id', 'short_name', 'name', 'level', 'departement', 'email', 'multisports', 'disciplines_liées']
    list_editable = []
    list_filter = [('departement', RelatedOnlyFieldListFilter), 'region']
    search_fields = ['name__unaccent']
    form = FederationForm
    list_per_page = 50
    list_display_links = ['id', 'name']

    def disciplines_liées(self, obj):
        return " | ".join([d.name for d in obj.disciplines.all()])

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.is_superuser:
            queryset = queryset.filter(departement=request.user.get_departement())
        return queryset

    def get_list_filter(self, request):
        # Les utilisateurs équipe ne peuvent voir que leur département
        if not request.user.is_superuser:
            return []
        return self.list_filter

    def get_readonly_fields(self, request, obj=None):
        if request.user.has_group('Administrateurs techniques') or request.user.is_superuser:
            return ()
        elif request.user.has_group('Administrateurs d\'instance'):
            return ('short_name', 'name', 'multisports', 'level', "departement", 'region', 'disciplines')
        else:
            return ('short_name', 'name', 'email', 'multisports', 'level', "departement", 'region', 'disciplines')
