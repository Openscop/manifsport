# coding: utf-8
from django.apps import AppConfig


class SportsConfig(AppConfig):
    """ Configuration de l'application """

    name = 'sports'
    verbose_name = "Sports et fédérations"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        pass


default_app_config = 'sports.SportsConfig'
