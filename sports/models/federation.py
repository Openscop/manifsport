# coding: utf-8
from django.db import models

from core.util.admin import set_admin_info
from administrative_division.models.departement import LISTE_DEPARTEMENT, LISTE_REGION


class FederationQuerySet(models.QuerySet):
    """ Queryset Federation """

    # Overrides
    def get_by_natural_key(self, email):
        return self.get(email=email)

    # Getter
    def get_for_departement(self, discipline, departement):
        """
        Renvoyer une fédération pour une discipline et un département

        Si une fédération n'existe pas pour le département, chercher
        pour la région du département, puis pour le pays.
        """
        federations = self.filter(level=Federation.DEPARTEMENTAL, departement=departement, disciplines=discipline)
        if federations.exists():
            return federations.first()

        for dept in LISTE_DEPARTEMENT:
            if dept[0] == departement.name if departement else False:
                federations = self.filter(level=Federation.REGIONAL, region=dept[2], disciplines=discipline)
                if federations.exists():
                    return federations.first()

        federations = self.filter(level=Federation.NATIONAL, disciplines=discipline)
        if federations.exists():
            return federations.first()

        return self.get_fallback()  # Aucune fédération n'a été trouvée.

    def get_fallback(self):
        """
        Renvoyer une fédération par défaut

        La fédération par défaut est caractérisée par aucune discipline associée,
        un niveau national, l'attribut "multisports" à True, et un nom de code (short_name) à
        "ddcs_default".
        """
        short_name = Federation.DEFAULT_FEDERATION_NAME
        fallback_candidates = self.filter(level=Federation.NATIONAL, multisports=True, short_name=short_name)
        if fallback_candidates.exists():
            return fallback_candidates.first()
        else:
            result = self.create(level=Federation.NATIONAL, multisports=True, short_name=short_name, name="DDCS")
            return result


class Federation(models.Model):
    """ Fédération sportive """

    # Constantes
    LEVELS = [[0, "Départemental"], [1, "Régional"], [2, "National"]]
    DEPARTEMENTAL, REGIONAL, NATIONAL = 0, 1, 2
    DEFAULT_FEDERATION_NAME = 'ddcs_default'

    # Champs
    short_name = models.CharField("abréviation", max_length=200)
    name = models.CharField("nom", max_length=200)
    email = models.EmailField("e-mail", max_length=200)
    multisports = models.BooleanField(default=False, verbose_name="Multi-sports ?")
    level = models.SmallIntegerField(default=0, choices=LEVELS, verbose_name="Couverture territoriale")
    departement = models.ForeignKey('administrative_division.departement', null=True, blank=True, verbose_name="Département", on_delete=models.SET_NULL)
    region = models.CharField(max_length=3, blank=True, choices=LISTE_REGION, verbose_name="Région")
    disciplines = models.ManyToManyField('sports.discipline', blank=True, related_name='federations', verbose_name="Disciplines")
    objects = FederationQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name

    def natural_key(self):
        """ Clé naturelle """
        return self.email,

    def save(self, *args, **kwargs):
        """ Sauvegarder l'onjet """
        # Nettoyer les attributs de niveau administratif
        if not self.is_departemental():
            self.departement = None
        if not self.is_regional():
            self.region = ''
        # Définir multisports à True si plusieurs disciplines
        if self.get_discipline_count() > 1:
            self.multisports = True
        super().save(*args, **kwargs)

    # Getter
    def get_agents(self):
        """ Renvoyer les agents d'une fédération """
        return self.federationagents.all()

    @set_admin_info(short_description="Agents")
    def get_agent_count(self):
        """ Renvoyer le nombre d'agents pour la fédération """
        return self.get_agents().count()

    def is_departemental(self):
        """ Renvoyer True si la fédération est départementale """
        return self.level == Federation.DEPARTEMENTAL

    def is_regional(self):
        """ Renvoyer True si la fédération est régionale """
        return self.level == Federation.REGIONAL

    def is_national(self):
        """ Renvoyer True si la fédération est nationale """
        return self.level == Federation.NATIONAL

    @set_admin_info(short_description="Disciplines", admin_order_field='discipline_count')
    def get_discipline_count(self):
        """ Renvoyer le nombre de disciplines associées à la féération """
        return self.disciplines.count() if self.pk else -1

    # Meta
    class Meta:
        verbose_name = "fédération"
        default_related_name = "federations"
        app_label = "sports"
        ordering = ['pk']
        unique_together = [['short_name', 'level', 'departement', 'email']]

