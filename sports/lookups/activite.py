# coding: utf-8
from ajax_select import register, LookupChannel
from django.db.models import Q
from unidecode import unidecode

from sports.models.sport import Activite


@register('activite')
class ActiviteLookup(LookupChannel):
    """ Lookup AJAX des activités sportives """

    # Configuration
    model = Activite
    min_length = 2

    # Overrides
    def get_query(self, q, request):
        return self.model.objects.filter(Q(name__icontains=q) | Q(discipline__name__icontains=q)).order_by('name')[:20]

    def format_item_display(self, item):
        return "<span>{item}</span><small style=\"float: right; margin-left: 15px;\">{item.discipline}</small>".format(item=item)

    def format_match(self, item):
        return "<span>{item}</span><small style=\"float: right; margin-left: 15px;\">{item.discipline}</small>".format(item=item)

    def check_auth(self, request):
        return True


@register('activite_nm')
class ActiviteNMLookup(ActiviteLookup):
    """ Lookup AJAX des activités sportives non motorisées """

    # Overrides
    def get_query(self, q, request):
        return self.model.objects.non_motorise().filter(Q(name__icontains=q) | Q(discipline__name__icontains=q)).order_by('name')[:20]


@register('activite_m')
class ActiviteMLookup(ActiviteLookup):
    """ Lookup AJAX des activités sportives motorisées """

    # Overrides
    def get_query(self, q, request):
        return self.model.objects.motorise().filter(Q(name__icontains=q) | Q(discipline__name__icontains=q)).order_by('name')[:20]
