# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-02-20 19:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('nouveautes', '0004_nouveaute_departements'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nouveaute',
            name='creation',
            field=models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='Date de création'),
        ),
    ]
