# coding: utf-8
from django.contrib import admin
from import_export.admin import ExportActionModelAdmin

from nouveautes.forms import NouveauteForm
from nouveautes.models.nouveaute import Nouveaute


@admin.register(Nouveaute)
class NouveauteAdmin(ExportActionModelAdmin):
    """ Configuration admin des nouveautés """

    list_display = ['pk', 'title', 'creation', 'public', 'get_departements', 'get_role_names_shorter']
    list_filter = ['creation', 'public', 'departements']
    search_fields = ['title__unaccent', 'body__unaccent']
    form = NouveauteForm
    list_per_page = 25
