# coding: utf-8
from django.urls import path, re_path
from nouveautes import views


app_name = 'nouveautes'
urlpatterns = [
    path('', views.list_nouveautes, name='list'),
    re_path('detail/(?P<slug>.+)', views.view_nouveautes, name='detail')
]
