# coding: utf-8
from bs4 import BeautifulSoup
from django.urls import reverse
from django.db import models
from django.db.models import Q
from django.utils import timezone
from django.utils.safestring import mark_safe
from django_extensions.db.fields import AutoSlugField
from markdown import markdown

from core.util.admin import set_admin_info
from core.util.user import UserHelper


class NouveauteManager(models.Manager):
    """ Manager des nouveautés """

    # Getter
    def has_news(self, user):
        """
        Renvoie si une nouveauté n'a jamais été lue par l'utilisateur

        :param user: utilisateur à considérer
        :returns: True si une nouveauté est non-lue par l'utilisateur, False sinon
        :rtype: bool
        """
        if user and not user.is_anonymous and UserHelper.get_role_name(user) is not None:
            return self.news_for_user(user).exists()
        return False

    def for_user(self, user):
        """
        Renvoie les nouveautés qui peuvent être lues par l'utilisateur

        :returns: queryset des Nouveautés accessibles pour user
        """
        if not user.is_anonymous:
            departement = user.get_instance().get_departement()
            return self.filter(Q(departements__isnull=True) | Q(departements=departement), Q(role__icontains=UserHelper.get_role_name(user) or 'undefined') | Q(role=''), public=True).order_by('-creation')
        return self.none()

    def news_for_user(self, user):
        """
        Renvoie les nouveautés qui n'ont pas été lues par l'utilisateur

        :returns: queryset des Nouveautés accessibles pour user
        """
        return self.for_user(user).filter(creation__gt=user.lecturenouveaute.date).order_by('-creation')


class Nouveaute(models.Model):
    """
    Nouveauté à afficher aux utilisateurs de groupes différents

    Supporte le format Markdown (rétrocompatibilité) si un texte au format Markdown ne contient pas de HTML
    """

    # Champs
    title = models.CharField(max_length=128, blank=False, verbose_name="Titre")
    slug = AutoSlugField(max_length=128, populate_from='title', allow_duplicates=False, verbose_name="Slug")
    body = models.TextField(blank=False, help_text="Contenu au format Markdown", verbose_name="Contenu de la nouveauté")
    creation = models.DateTimeField(default=timezone.now, verbose_name="Date de création")
    public = models.BooleanField(default=True, db_index=True, verbose_name="Publié")
    role = models.TextField(blank=True, help_text="Rôles séparés par une virgule", verbose_name="Rôles autorisés")
    departements = models.ManyToManyField('administrative_division.departement', blank=True, verbose_name="Départements d'affichage")
    envoye = models.BooleanField(default=False, verbose_name="Envoyé")
    objects = NouveauteManager()

    # Getter
    def get_absolute_url(self):
        """ Renvoie l'URL de l'objet """
        return reverse('nouveautes:detail', args=None, kwargs={'slug': self.slug})

    @set_admin_info(short_description="Rôles")
    def get_role_names(self):
        """ Renvoie la liste des rôles avec leur nom lisible """
        roles = self.role.split(',')
        role_dict = dict(UserHelper.ROLE_CHOICES)
        names = filter(None, [role_dict.get(role, None) for role in roles if role])
        return ', '.join(names)

    @set_admin_info(short_description="Rôles")
    def get_role_names_shorter(self):
        """ Renvoie la liste des rôles avec leur nom lisible """
        roles = self.role.split(',')
        count = len(roles)
        role_dict = dict(UserHelper.ROLE_CHOICES)
        names = filter(None, [role_dict.get(role, None) for role in roles if role])
        names = list(names)
        return "({0}) : {1}...".format(count, ', '.join(names[:4]))

    @set_admin_info(admin_order_field='departement', short_description="Département")
    def get_departements(self):
        """
        Renvoie les département de la nouveauté

        """
        return " - ".join([dep.name for dep in self.departements.all()])

    def get_html(self):
        """
        Renvoyer le texte HTML de la nouveauté

        Si le texte ne contient pas de HTML, on considère que le texte est formaté en Markdown.
        """
        has_html = BeautifulSoup(self.body, 'html.parser').find()
        return mark_safe(self.body if has_html else markdown(self.body))

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.role:
            if 'compagnieagent' in self.role and 'compagnieagentlocal' not in self.role:
                self.role.replace('compagnieagent', 'compagnieagentlocal')
                self.save()

    # Métadonnées
    class Meta:
        verbose_name = u"Nouveauté de la plateforme"
        verbose_name_plural = u"Nouveautés de la plateforme"
        app_label = 'nouveautes'
