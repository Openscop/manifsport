# coding: utf-8
from django.urls import reverse

from agreements.factories import EDSRAvisFactory
from sub_agreements.tests.base import PreavisTestsBase
from ..factories import PreAvisCGDFactory


class PreAvisCGDTests(PreavisTestsBase):
    """ Test des préavis CGD """

    # Configuration
    def setUp(self):
        super().setUp()
        self.avis = EDSRAvisFactory.create(authorization=self.authorization)

    # Tests
    def test_str(self):
        """ Tester le nom du préavis """
        preavis = PreAvisCGDFactory.create(avis=self.avis, cgd=self.cgd)
        self.assertEqual(str(preavis), ' - '.join([str(self.manifestation), str(preavis.cgd)]))

    def test_access_url(self):
        """ Tester les URLs """
        preavis = PreAvisCGDFactory.create(avis=self.avis, cgd=self.cgd)
        self.assertEqual(preavis.get_absolute_url(), reverse('sub_agreements:cgd_subagreement_detail', kwargs={'pk': preavis.pk}))

    def test_workflow_edsr(self):
        """ Tester la création automatique de préavis lorsqu'on assigne des CGD à un avis """
        self.assertEqual(self.avis.get_preavis_count(), 0)  # L'avis ne possède pas de préavis, il faut sélectionner des CGD avant
        self.avis.concerned_cgd.add(self.cgd)  # Sélectionner un CGD dans l'avis EDSR. Crée normalement un préavis CGD
        self.assertEqual(self.avis.get_preavis_count(), 1)
