# coding: utf-8
from django.urls import path, re_path

from sub_agreements.views import PreAvisEDSRDetail, PreAvisEDSRAcknowledge
from .views import Dashboard, Archives
from .views import NotifyBrigadesView
from .views import NotifyCISView
from .views import PreAvisCGDAcknowledge
from .views import PreAvisCGDDetail
from .views import PreAvisCommissariatAcknowledge
from .views import PreAvisCommissariatDetail
from .views import PreAvisCompagnieAcknowledge
from .views import PreAvisCompagnieDetail
from .views import PreAvisServiceCGAcknowledge
from .views import PreAvisServiceCGDetail


app_name = 'sub_agreements'
urlpatterns = [

    path('dashboard/', Dashboard.as_view(), name='dashboard'),
    path('archives/', Archives.as_view(), name='archives'),

    # CGD SubAgreements
    re_path('cgd/(?P<pk>\d+)/acknowledge/', PreAvisCGDAcknowledge.as_view(), name='cgd_subagreement_acknowledge'),
    re_path('cgd/(?P<pk>\d+)/notify_brigades/', NotifyBrigadesView.as_view(), name='notify_brigades'),
    re_path('cgd/(?P<pk>\d+)/', PreAvisCGDDetail.as_view(), name='cgd_subagreement_detail'),

    # EDSR SubAgreements
    re_path('edsr/(?P<pk>\d+)/acknowledge/', PreAvisEDSRAcknowledge.as_view(), name='edsr_subagreement_acknowledge'),
    re_path('edsr/(?P<pk>\d+)/', PreAvisEDSRDetail.as_view(), name='edsr_subagreement_detail'),

    # Company SubAgreements
    re_path('company/(?P<pk>\d+)/acknowledge/', PreAvisCompagnieAcknowledge.as_view(), name='company_subagreement_acknowledge'),
    re_path('company/(?P<pk>\d+)/notify_cis/', NotifyCISView.as_view(), name='notify_cis'),
    re_path('company/(?P<pk>\d+)/', PreAvisCompagnieDetail.as_view(), name='company_subagreement_detail'),

    # Commissariat SubAgreements
    re_path('commissariat/(?P<pk>\d+)/acknowledge/', PreAvisCommissariatAcknowledge.as_view(), name='commissariat_subagreement_acknowledge'),
    re_path('commissariat/(?P<pk>\d+)/', PreAvisCommissariatDetail.as_view(), name='commissariat_subagreement_detail'),

    # CG Service SubAgreements
    re_path('cgservice/(?P<pk>\d+)/acknowledge/', PreAvisServiceCGAcknowledge.as_view(), name='cgservice_subagreement_acknowledge'),
    re_path('cgservice/(?P<pk>\d+)/', PreAvisServiceCGDetail.as_view(), name='cgservice_subagreement_detail'),
]
