# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView

from agreements.views import BaseAcknowledgeView

from core.util.permissions import require_role
from ..forms import *
from ..models import *


class PreAvisCGDDetail(DetailView):
    """ Détail d'un préavis CG """

    # Configuration
    model = PreAvisCGD

    # Overrides
    @method_decorator(require_role('cgdagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisCGDDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.avis.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class PreAvisCGDAcknowledge(BaseAcknowledgeView):
    """ Rendu préavis CG """

    # Configuration
    model = PreAvisCGD
    form_class = PreAvisForm

    @method_decorator(require_role('cgdagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisCGDAcknowledge, self).dispatch(*args, **kwargs)


class NotifyBrigadesView(UpdateView):
    """ Notification des brigades """

    # Configuration
    model = PreAvisCGD
    form_class = NotifyBrigadesForm
    template_name_suffix = '_notify_form'

    # Overrides
    @method_decorator(require_role('cgdagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(NotifyBrigadesView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        response = super(NotifyBrigadesView, self).form_valid(form)
        form.instance.notify_brigades()
        form.instance.save()
        return response
