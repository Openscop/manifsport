# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView

from agreements.views import BaseAcknowledgeView
from core.util.permissions import require_role
from ..forms import *
from ..models import *


class PreAvisCompagnieDetail(DetailView):
    """ Détail d'un prévis compagnie """

    # Configuration
    model = PreAvisCompagnie

    # Overrides
    @method_decorator(require_role('compagnieagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisCompagnieDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.avis.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class PreAvisCompagnieAcknowledge(BaseAcknowledgeView):
    """ Rendu d'un préavis compagnie """

    # Configuration
    model = PreAvisCompagnie
    form_class = PreAvisForm

    # Overrides
    @method_decorator(require_role('compagnieagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(PreAvisCompagnieAcknowledge, self).dispatch(*args, **kwargs)


class NotifyCISView(UpdateView):
    """ Notification des agents CIS """

    # Configuration
    model = PreAvisCompagnie
    form_class = NotifyCISForm
    template_name_suffix = '_notify_form'

    # Overrides
    @method_decorator(require_role('compagnieagentlocal'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser l'accès à la vue aux agents locaux autorisés """
        return super(NotifyCISView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        response = super(NotifyCISView, self).form_valid(form)
        form.instance.notify_cis()
        form.instance.save()
        return response
