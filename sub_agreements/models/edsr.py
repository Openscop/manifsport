# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from notifications.models.action import Action
from notifications.models.notification import Notification
from sub_agreements.models.abstract import PreAvis, PreAvisQuerySet


class PreAvisEDSR(PreAvis):
    """ Préavis EDSR """

    # Champs
    preavis_ptr = models.OneToOneField("sub_agreements.preavis", parent_link=True, related_name='preavisedsr', on_delete=models.CASCADE)
    edsr = models.ForeignKey("administration.edsr", verbose_name='EDSR', on_delete=models.CASCADE)
    objects = PreAvisQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        manifestation = self.avis.get_manifestation()
        edsr = self.edsr
        return ' - '.join([str(manifestation), str(edsr)])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'objet """
        url = 'sub_agreements:edsr_subagreement_detail'
        return reverse(url, kwargs={'pk': self.pk})

    def get_edsr(self):
        """ Renvoyer l'EDSR pour ce préavis """
        return self.edsr

    def get_ggd(self):
        """ Renvoyer le GGD pour ce préavis """
        return self.edsr.departement.ggd

    def get_agents(self):
        """ Renvoyer les agents concernés par le préavis """
        return self.edsr.edsragents.all()

    # Action
    @transition(field='state', source=['created', 'notified'], target='notified')
    def notify_edsr_agents(self):
        """ Envoyer une notification aux agents EDSR """
        recipients = self.get_agents()
        Notification.objects.notify_and_mail(recipients, "Prenez connaissance des informations", self.edsr, self.avis.get_manifestation())
        Action.objects.log(self.get_agents(), "EDSR informé", self.avis.get_manifestation())

    @transition(field='state', source=['created', 'notified'], target='acknowledged')
    def acknowledge(self):
        """ Rendre le préavis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(agents=self.get_ggd().ggdagents.all(), content_object=self.edsr)
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = 'pré-avis EDSR'
        verbose_name_plural = 'pré-avis EDSR'
        default_related_name = "preavisedsr_set"
        app_label = "sub_agreements"
