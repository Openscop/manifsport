# coding: utf-8
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout

from core.forms.base import GenericForm
from ..models import PreAvisCGD
from ..models import PreAvisCompagnie


class NotifyCISForm(GenericForm):
    """ Notification CIS """

    def __init__(self, *args, **kwargs):
        super(NotifyCISForm, self).__init__(*args, **kwargs)
        self.fields['concerned_cis'].queryset = self.instance.compagnie.ciss.all()  # noqa pylint: disable=E1101
        self.fields['concerned_cis'].widget.attrs = {'data-placeholder': "Choisissez un ou plusieurs CIS concernés"}
        self.helper.layout = Layout(
            'concerned_cis',
            FormActions(Submit('save', "Informer les CIS concernés"))
        )

    class Meta:
        model = PreAvisCompagnie
        fields = ['concerned_cis']


class NotifyBrigadesForm(GenericForm):
    """ Botification brigade """

    def __init__(self, *args, **kwargs):
        super(NotifyBrigadesForm, self).__init__(*args, **kwargs)
        self.fields['concerned_brigades'].queryset = self.instance.cgd.brigades.all()  # noqa pylint: disable=E1101
        self.fields['concerned_brigades'].widget.attrs = {'data-placeholder': "Choisissez une ou plusieurs brigades concernées"}
        self.helper.layout = Layout(
            'concerned_brigades',
            FormActions(
                Submit('save', "Informer les brigades concernées")
            )
        )

    class Meta:
        model = PreAvisCGD
        fields = ['concerned_brigades']
