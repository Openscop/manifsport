# coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save, m2m_changed
from django.dispatch import receiver

from administration.models import CGService
from agreements.models import CGAvis
from sub_agreements.models.servicecg import PreAvisServiceCG


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=PreAvisServiceCG)
    def notify_preavis_service_cg(created, instance, **kwargs):
        """ Notifier de la création du préavis """
        if created:
            instance.notify_creation(agents=instance.get_agents(), content_object=instance.cg_service.cg)


    @receiver(m2m_changed, sender=CGAvis.concerned_services.through)
    def create_preavis_service_cgs(instance, action, pk_set, **kwargs):
        """ Créer le préavis de service CG lors de l'ajout d'un service à l'avis CG """
        if action == 'post_add':
            for pk in pk_set:
                PreAvisServiceCG.objects.get_or_create(avis=instance, cg_service=CGService.objects.get(pk=pk))
