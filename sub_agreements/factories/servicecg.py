# coding: utf-8
import factory

from administration.factories import CGServiceFactory
from agreements.factories import CGAvisFactory
from ..models import PreAvisServiceCG


class PreAvisServiceCGFactory(factory.django.DjangoModelFactory):
    """ Factory pour les préavis de service de Conseil départemental """

    avis = factory.SubFactory(CGAvisFactory)
    cg_service = factory.SubFactory(CGServiceFactory)

    class Meta:
        model = PreAvisServiceCG
