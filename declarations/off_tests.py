# coding: utf-8
from django.test import TestCase

from notifications.models import Action
from .factories import EventDeclarationFactory


class EventDeclarationMethodTests(TestCase):
    """ Tests des déclarations """

    # Tests
    def test_log_declaration_creation(self):
        declaration = EventDeclarationFactory.create()
        # Une action est créée (voir declarations.listeners), vérifier son état
        action = Action.objects.last()
        self.assertEqual(action.user, declaration.manifestation.structure.organisateur.user)
        self.assertEqual(action.manifestation, declaration.manifestation.manifestation_ptr)
        self.assertEqual(action.action, "déclaration envoyée")

        # Vérifier qu'une déclaration resauvegardée ne provoque pas la création d'une autre action
        declaration.save()
        action2 = Action.objects.last()
        self.assertEqual(action, action2)
