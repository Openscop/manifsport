# coding: utf-8
from django.urls import path, re_path

from .views import DeclarationCreateView, DeclarationDetailView, DeclarationPublishReceiptView
from .views import Dashboard, Archives


app_name = 'declarations'
urlpatterns = [
    path('dashboard/', Dashboard.as_view(), name="dashboard"),
    path('archives/', Archives.as_view(), name="archives"),

    re_path('(?P<pk>\d+)/publish/', DeclarationPublishReceiptView.as_view(), name='declaration_publish'),
    re_path('add/(?P<manifestation_pk>\d+)', DeclarationCreateView.as_view(), name='declaration_add'),
    re_path('(?P<pk>\d+)/', DeclarationDetailView.as_view(), name='declaration_detail')
]
