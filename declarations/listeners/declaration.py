# coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from declarations.models import ManifestationDeclaration


if not settings.DISABLE_SIGNALS:
    @receiver(post_save, sender=ManifestationDeclaration)
    def log_declaration_creation(sender, instance, raw, created, **kwargs):
        """ Lors de la sauveegarde d'une déclaration """
        if raw is True:
            return
        if created:
            instance.log_creation()
            instance.notify_creation()
