# coding: utf-8

from uuid import uuid4

import requests
from django.test import TestCase
from furl.furl import furl

from carto.consumer.openrunner import openrunner_api
from configuration import OPENRUNNER_FRONTEND_URL


class OpenRunnerAPITests(TestCase):
    """ Tests de l'interfaçage aux API OpenRunner """

    # Configuration
    def setUp(self):
        """ Configurer chaque test """
        username = '__testuser_repeated'
        openrunner_api.create_user(username, '999', 'organizer')

    def test_user_routes(self):
        """ Tester la récupération des parcours """
        data = openrunner_api.get_user_routes('__not_existing_username__', return_data_on_failure=True)
        self.assertEqual(data['success'], False)
        self.assertEqual(data['code'], 404)

    def test_user_creation_success(self):
        """ Tester la création d'utilisateurs """
        username = '__testuser_{0}'.format(str(uuid4()))
        data = openrunner_api.create_user(username, '999', 'organizer')
        self.assertTrue(data)

    def test_user_creation_already_exists(self):
        """
        Tester la création d'utilisateurs

        L'utilisateur est connu, create_user renvoie directement True
        """
        username = '__testuser_repeated'
        data = openrunner_api.create_user(username, '999', 'organizer', return_data_on_failure=True)
        self.assertTrue(data)

    def test_user_creation_name_too_short(self):
        """ Tester la création d'utilisateurs """
        username = '_1'
        data = openrunner_api.create_user(username, '999', 'organizer', return_data_on_failure=True)
        self.assertEqual(data['code'], 400)
        self.assertFalse(data['success'])
        self.assertTrue(data['message'].startswith('The username must be at least'))

    def test_user_token(self):
        """ Tester la récupération de token et le login avec token """
        username = '__testuser_repeated'
        token = openrunner_api.get_user_login_token(username)
        login_url = openrunner_api.get_url_with_login(None, username)  # Page par défaut de login à l'interface
        error_url = furl(OPENRUNNER_FRONTEND_URL).set({'token': 'invalid-token-on-purpose'}).url

        self.assertTrue(len(token) > 16)
        response = requests.get(login_url, verify=False)
        self.assertEquals(response.status_code, 200)
        response = requests.get(error_url, verify=False)
        # Ici, l'assert est normalemebt un notEqual
        self.assertEqual(response.status_code, 200, "Openrunner : La page devrait idéalement avoir un code de statut > 399. (mail du 20/02/2017 15:53)")
