# coding: utf-8
"""
Paramètres de l'application
- settings Django
"""
from __future__ import absolute_import, unicode_literals
from .settings import *

from .celery import app as celery_app

__all__ = ('celery_app',)