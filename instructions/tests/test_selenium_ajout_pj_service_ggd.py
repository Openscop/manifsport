import re, time

from django.test import tag
from django.test.utils import override_settings
from django.conf import settings
from post_office.models import Email
from messagerie.models import Message

from selenium.common.exceptions import NoSuchElementException

from .test_base_selenium import SeleniumCommunClass
from instructions.models import PreAvis, Avis


class InstructionAjoutPjTests(SeleniumCommunClass):
    """
    Test d'ajout de PJ du circuit d'instance EDSR avec sélénium pour une Dcnm
        workflow_GGD : Avis EDSR
        instruction par arrondissement
        openrunner false par défaut
    Test sur les services complexes avec transfert des PJ du préavis dans l'avis et
    transfert des PJ d'avis en document officiel
    """

    DELAY = 0.35

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
        """
        print()
        print('============ Pièces Jointes (Sel) =============')
        SeleniumCommunClass.init_setup(cls)
        cls.avis_nb = 1
        super().setUpClass()

    @tag('selenium')
    @override_settings(DEBUG=True)
    def test_Ajout_PJ(self):
        """
        Test d'ajout de PJ pour les différentes étapes du circuit EDSR pour une autorisationNM
        """
        assert settings.DEBUG

        print('**** test 1 finalisation de la manifestation ****')
        # Ajout des fichiers et envoie de la demande
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        # Appel de la vue pour joindre les fichiers
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        envoi = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        if hasattr(envoi, 'group'):
            self.client.post(envoi.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test 2 distribution avis ****')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes d\'avis', self.selenium.page_source)
        # Distribuer les demandes d'avis de l'événement
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes d\'avis').click()
        time.sleep(self.DELAY)
        self.assertIn('Choix des avis', self.selenium.page_source)
        self.selenium.execute_script("window.scroll(0, 1000)")
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis EDSR requis')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.execute_script("window.scroll(0, 1500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_id('submit-id-save').click()
        time.sleep(self.DELAY * 8)

        # Annuler la demande d'avis fédération
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']")
        avis.click()
        self.selenium.execute_script("window.scroll(0, 400)")
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Fédération')][@data-toggle='collapse']/following::*")
        self.assertIn('Annuler cette demande', carte.text)
        carte.find_element_by_partial_link_text('Annuler cette demande').click()
        carte.find_element_by_partial_link_text('Confirmer').click()

        # Vérifier le passage en encours et le nombre d'avis manquants
        self.selenium.execute_script("window.scroll(0, 1200)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('instructeur', 'encours')
        self.assertIn(str(self.avis_nb) + '&nbsp; avis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 3 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Envoyer les demandes', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Envoyer les préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Envoyer les demandes').click()
        time.sleep(self.DELAY)
        self.assertIn('Envoyer les demandes de pré-avis', self.selenium.page_source)
        self.chosen_select('id_cgd_concerne_chosen', self.cgd.__str__())
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Test d ela partie pièces jointes
        self.assertIn('Aucune', self.selenium.page_source)
        self.assertIn('Ajouter une pièce jointe', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en encours
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertIn('1 préavis manquants', self.selenium.page_source)
        self.deconnexion()

        print('**** test 4 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.connexion('agent_cgd')
        self.presence_avis('agent_cgd', 'nouveau')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Informer les brigades', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # informer les brigades de l'événement
        self.selenium.find_element_by_partial_link_text('Informer les brigades').click()
        time.sleep(self.DELAY)
        self.assertIn('Choisissez les brigades concernées', self.selenium.page_source)
        self.chosen_select('id_brigades_concernees_chosen', 'BTA - Bard')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Vérification du passage en warning
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'encours')
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre le pré-avis', self.selenium.page_source)
        self.assertIn('pourrez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        # Rendre le préavis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre le pré-avis').click()
        time.sleep(self.DELAY)
        self.assertIn('pouvez ajouter une ou plusieurs pièces jointes', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        self.assertIn('Aucune', self.selenium.page_source)  # Pièces jointes
        self.selenium.execute_script("window.scroll(0, 500)")
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_1.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        self.selenium.execute_script("window.scroll(0, 800)")
        time.sleep(self.DELAY)
        # Tester la présence de la pièce jointe
        pan = self.selenium.find_element_by_xpath("//div[@id='subagreement']")
        self.assertIn('pj_1', pan.text)
        # Tester aucune action disponible
        self.aucune_action()
        # Vérification du passage en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_cgd', 'rendu')
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[1].to, [self.edsr.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un préavis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, PreAvis.objects.get(service_concerne='cgd').id)          # Pré-avis CGD
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")
        Email.objects.all().delete()
        Message.objects.all().delete()

        print('**** test 5 avis edsr ****')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        self.connexion('agent_edsr')
        self.presence_avis('agent_edsr', 'encours')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.assertNotIn('préavis manquants', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Mettre en forme l\'avis', self.selenium.page_source)
        # Vérifier affichage
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('Aucune', pan.text)  # Pièces jointes
        self.assertIn('Ajouter une pièce jointe', pan.text)
        # Vérifier les icônes du préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        icones = preavis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            preavis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester la présence de la PJ dans le préavis et du bouton de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien1.text)
        # Transférer la PJ sur l'avis
        lien1.click()
        # Tester aucune notifications
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 0)
        # Tester la présence de la PJ sur l'avis
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertNotIn('Aucune', pan.text)
        self.assertIn('pj_1', pan.text)
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester l'absence du bouton de transfert pour PJ_1
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_3.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence ds PJs
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_3', pan.text)

        # Formater l'avis de l'événement
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Mettre en forme l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//span[contains(text(), 'Avis favorable')]/..").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)
        # Tester aucune action disponible
        self.aucune_action()
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 600)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_edsr', 'rendu')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        self.deconnexion()

        print('**** test 6 preavis cgd ****')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        self.connexion('agent_cgd')
        self.presence_avis('agent_cgd', 'rendu')

        Email.objects.all().delete()
        Message.objects.all().delete()

        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Ajouter une pièce jointe
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_2.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence des pièces jointes
        pan = self.selenium.find_element_by_xpath("//div[@id='subagreement']")
        time.sleep(self.DELAY)
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        self.deconnexion()
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 4)
        self.assertEqual(outbox[0].to, [self.agent_edsr.email])
        self.assertEqual(outbox[1].to, [self.agent_ggd.email])
        self.assertEqual(outbox[2].to, [self.edsr.email])
        self.assertEqual(outbox[3].to, [self.dep.ggd.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un préavis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, PreAvis.objects.get(service_concerne='cgd').id)          # Pré-avis CGD
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "preavis")
        Email.objects.all().delete()
        Message.objects.all().delete()

        print('**** test 7 avis ggd ****')
        # Instruction de l'avis par le ggd, vérification de la présence de l'événement en nouveau
        self.connexion('agent_ggd')
        self.presence_avis('agent_ggd', 'nouveau')
        self.assertIn('Délai : 21 jours', self.selenium.page_source)
        # Appel de la vue de détail et test présence manifestation
        self.vue_detail()
        # Vérifier l'action disponible
        self.assertIn('Rendre l\'avis', self.selenium.page_source)
        # Vérifier affichage des PJs
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_3', pan.text)
        self.assertIn('Ajouter une pièce jointe', pan.text)
        # Vérifier les icônes du préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        icones = preavis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            preavis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        self.assertIn('pj_1', carte.text)
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer le document sur', lien2.text)
        # Transférer la PJ sur l'avis
        lien2.click()
        # Tester aucune notifications
        outbox = Email.objects.order_by('pk')
        self.assertEqual(len(outbox), 0)
        # Tester la présence des PJs sur l'avis
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        self.assertIn('pj_3', pan.text)
        self.selenium.execute_script("window.scroll(0, 800)")
        # Déplier le préavis
        preavis = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']")
        preavis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester l'absence du bouton de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'Cgd')][@data-toggle='collapse']/following::*")
        item1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/parent::*")
        self.assertNotIn('Transférer le document sur', item1.text)
        item2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/parent::*")
        self.assertNotIn('Transférer le document sur', item2.text)
        self.selenium.execute_script("window.scroll(0, 200)")
        # Rendre l'avis de l'événement
        self.selenium.find_element_by_partial_link_text('Rendre l\'avis').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)
        self.assertIn('Détail de la manifestation', self.selenium.page_source)

        Email.objects.all().delete()
        Message.objects.all().delete()

        # Ajouter une pièce jointe
        self.selenium.execute_script("window.scroll(0, 400)")
        self.selenium.find_element_by_class_name('btn_doc').click()
        fichier = self.selenium.find_element_by_xpath('//input[@name="fichier"]')
        fichier.send_keys('/tmp/pj_4.txt')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # Tester la présence ds PJs
        pan = self.selenium.find_element_by_xpath("//div[@id='agreement']")
        self.assertIn('pj_1', pan.text)
        self.assertIn('pj_2', pan.text)
        self.assertIn('pj_3', pan.text)
        self.assertIn('pj_4', pan.text)
        # Tester les notifications
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0].to, [self.instructeur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(messages[0].corps, 'Pièce jointe ajoutée à un avis au sujet de la manifestation Manifestation_Test')
        self.assertEqual(messages[0].object_id, Avis.objects.get(service_concerne='edsr').id)          # Avis EDSR
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "avis")
        # Tester aucune action disponible
        self.aucune_action()
        self.avis_nb -= 1
        # vérification de la présence de l'événement en rendu
        self.selenium.execute_script("window.scroll(0, 500)")
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY)
        self.presence_avis('agent_ggd', 'rendu')
        self.deconnexion()

        print('**** test 8 instructeur transfert pj ****')
        # Vérifier le nombre d'avis manquants dans la vue instructeur
        self.connexion('instructeur')
        self.presence_avis('instructeur', 'encours')

        Email.objects.all().delete()
        Message.objects.all().delete()

        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.vue_detail()
        # Avis EDSR
        avis = self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']")
        icones = avis.find_elements_by_css_selector('i')
        self.assertEqual(3, len(icones))
        # Tester icône Pièce jointe
        try:
            avis.find_element_by_class_name('doc')
        except NoSuchElementException:
            self.assertTrue(False, msg="pas d'icône pièce jointe")
        # Déplier l'avis
        avis.click()
        self.selenium.execute_script("window.scroll(0, 1000)")
        # Tester la présence des PJs dans le préavis et des boutons de transfert
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']/following::*")
        time.sleep(self.DELAY*2)
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        self.assertIn('pj_3', carte.text)
        lien3 = carte.find_element_by_xpath("//a[contains(text(), 'pj_3')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien3.text)
        self.assertIn('pj_4', carte.text)
        lien4 = carte.find_element_by_xpath("//a[contains(text(), 'pj_4')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien4.text)
        pj = self.selenium.find_element_by_partial_link_text('pj_4')
        # Transférer la PJ en document officiel
        lien4.click()
        nom_pj = pj.get_attribute('text')
        self.selenium.execute_script("window.scroll(0, 1500)")
        self.selenium.find_element_by_xpath("//select[@name='nature']/option[text()='Récépissé de déclaration']").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY)
        # Reprendre l'avis et tester plus de bouton de transfert
        self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']").click()
        carte = self.selenium.find_element_by_xpath("//div[contains(string(),'EDSR')][@data-toggle='collapse']/following::*")
        time.sleep(self.DELAY*2)
        self.assertIn('pj_1', carte.text)
        lien1 = carte.find_element_by_xpath("//a[contains(text(), 'pj_1')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien1.text)
        self.assertIn('pj_2', carte.text)
        lien2 = carte.find_element_by_xpath("//a[contains(text(), 'pj_2')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien2.text)
        self.assertIn('pj_3', carte.text)
        lien3 = carte.find_element_by_xpath("//a[contains(text(), 'pj_3')]/following-sibling::a")
        self.assertIn('Transférer en document officiel', lien3.text)
        self.assertIn('pj_4', carte.text)
        item = carte.find_element_by_xpath("//a[contains(text(), 'pj_4')]/parent::*")
        self.assertNotIn('Transférer le document sur', item.text)
        self.selenium.execute_script("window.scroll(0, 0)")
        self.assertIn('Documents officiels (1)', self.selenium.page_source)
        self.selenium.find_element_by_partial_link_text('Documents officiels (1)').click()
        pan = self.selenium.find_element_by_xpath("//div[@id='officialdata']")
        self.assertIn('Récépissé de déclaration', pan.text)
        self.assertIn('Déposé le', pan.text)
        self.assertIn(nom_pj, pan.text)
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk')
        messages = Message.objects.all()
        # for out in outbox:
        #     print(out.to)
        # for mess in messages:
        #     print(mess.corps)
        self.assertEqual(len(outbox), 8)
        self.assertEqual(outbox[0].to, [self.organisateur.email])
        self.assertEqual(outbox[1].to, [self.prefecture.email])
        self.assertEqual(outbox[2].to, [self.agent_cgd.email])
        self.assertEqual(outbox[3].to, [self.cgd.email])
        self.assertEqual(outbox[4].to, [self.agent_edsr.email])
        self.assertEqual(outbox[5].to, [self.agent_ggd.email])
        self.assertEqual(outbox[6].to, [self.dep.ggd.email])
        self.assertEqual(outbox[7].to, [self.edsr.email])
        self.assertEqual(messages[0].corps, 'Récépissé de déclaration publié pour la manifestation Manifestation_Test')
        self.assertEqual(messages[0].message_enveloppe.first().doc_objet, "recepisse")
        # Vérifier le passage en autorise
        self.selenium.execute_script("window.scroll(0, 800)")
        self.selenium.find_element_by_partial_link_text('Revenir au tableau').click()
        time.sleep(self.DELAY * 4)
        self.presence_avis('instructeur', 'autorise')
        self.assertNotIn('avis manquants', self.selenium.page_source)
        self.deconnexion()

