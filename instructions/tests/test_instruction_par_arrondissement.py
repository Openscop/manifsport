from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
import re

from core.models import Instance
from evenements.factories import DcnmFactory
from instructions.factories import InstructionFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory
from core.factories import UserFactory
from administration.factories import InstructeurFactory, MairieAgentFactory
from sports.factories import ActiviteFactory


class InstructionArrondissement(TestCase):
    """
    Test du circuit d'instruction par arrondissement
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('========= (Clt) ==========')
        # Création des objets sur le 42
        dep = DepartementFactory.create(name='42',
                                        instance__name="instance de test",
                                        instance__workflow_ggd=Instance.WF_EDSR,
                                        instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrond_1 = ArrondissementFactory.create(name='Montbrison', code='421', departement=dep)
        arrond_2 = ArrondissementFactory.create(name='Roanne', code='422', departement=dep)
        cls.instance = dep.instance
        cls.prefecture1 = arrond_1.prefecture
        cls.prefecture2 = arrond_2.prefecture
        cls.commune1 = CommuneFactory(name='Bard', arrondissement=arrond_1)
        cls.autrecommune1 = CommuneFactory(name='Roche', arrondissement=arrond_1)
        cls.commune2 = CommuneFactory(name='Bully', arrondissement=arrond_2)
        cls.autrecommune2 = CommuneFactory(name='Régny', arrondissement=arrond_2)
        structure1 = StructureFactory(commune=cls.commune1)
        structure2 = StructureFactory(commune=cls.commune2)
        activ = ActiviteFactory.create()

        # Création des utilisateurs
        cls.instructeur1 = UserFactory.create(username='instructeur1', password=make(123),
                                              default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur1, prefecture=cls.prefecture1)
        cls.agent_mairie1 = UserFactory.create(username='agent_mairie1', password=make(123),
                                               default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie1, commune=cls.commune1)
        cls.instructeur2 = UserFactory.create(username='instructeur2', password=make(123),
                                              default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur2, prefecture=cls.prefecture2)
        cls.agent_mairie2 = UserFactory.create(username='agent_mairie2', password=make(123),
                                               default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie2, commune=cls.commune2)

        # Création des événements
        cls.manifestation1 = DcnmFactory.create(ville_depart=cls.commune1, structure=structure1, activite=activ,
                                                nom='Manifestation_Test_1', villes_traversees=(cls.autrecommune1,))
        cls.manifestation2 = DcnmFactory.create(ville_depart=cls.commune2, structure=structure2, activite=activ,
                                                nom='Manifestation_Test_2', villes_traversees=(cls.autrecommune2,))

    def test_Instruction_Arrond(self):
        print('**** test 1 creation manifs ****')
        self.instruction1 = InstructionFactory.create(manif=self.manifestation1)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation1.ville_depart, end=" ; ")
        print(self.manifestation1.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation1.structure, end=" ; ")
        print(self.manifestation1.activite, end=" ; ")
        print(self.manifestation1.activite.discipline)
        self.assertEqual(str(self.instruction1.manif), str(self.manifestation1))

        self.instruction2 = InstructionFactory.create(manif=self.manifestation2)

        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation2.ville_depart, end=" ; ")
        print(self.manifestation2.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation2.structure, end=" ; ")
        print(self.manifestation2.activite, end=" ; ")
        print(self.manifestation2.activite.discipline)
        self.assertEqual(str(self.instruction2.manif), str(self.manifestation2))

        print('**** test 2 instruction préfecture 1 ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur1', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Montbrison')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)

        print('**** test 3 instruction préfecture 2 ****')

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur2', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Roanne')
        # Une instruction dans le TdB instructeur
        nb_bloc1 = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc2 = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc3 = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        nb_bloc4 = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc1, 'group'))
        self.assertTrue(hasattr(nb_bloc2, 'group'))
        self.assertTrue(hasattr(nb_bloc3, 'group'))
        self.assertTrue(hasattr(nb_bloc4, 'group'))
        resultat = int(nb_bloc2.group('nb')) + int(nb_bloc3.group('nb')) + int(nb_bloc4.group('nb'))
        self.assertEqual(0, resultat)
        self.assertEqual('1', nb_bloc1.group('nb'))
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction1.manif)

        print('**** test 4 instruction préfecture 1 et 2 en lecture ****')

        self.instance.acces_arrondissement = 1
        self.instance.save()

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur1', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Montbrison')
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Montbrison', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_421')
            self.assertTrue('test_ecriture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Montbrison trouvé")
        self.assertContains(reponse, 'Roanne')
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Roanne', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_422')
            self.assertTrue('test_lecture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Roanne trouvé")

        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)
        # Appel de la vue de détail et test présence manifestation et action possible
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_1')
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=2)

        # Changement d'onglet => Roanne
        url = "/instructions/tableaudebord/list/?filtre_etat=atraiter&arron=" + classe_i.group('id')[6:]
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction1.manif)

        # Appel de la vue de détail et test présence manifestation et lecture seule
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_2')
        self.assertContains(reponse, 'La configuration de votre département ne permet')

        print('**** test 5 instruction préfecture 2 et 1 ****')

        self.instance.acces_arrondissement = 2
        self.instance.save()

        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username='instructeur2', password='123'))
        # Appel de la page
        url = '/instructions/tableaudebord/'
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Roanne')
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Roanne', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_422')
            self.assertTrue('test_ecriture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Roanne trouvé")
        self.assertContains(reponse, 'Montbrison')
        classe_i = re.search('<label.+class="(?P<class>([^"]+)).+id="(?P<id>([^"]+)).+Montbrison', reponse.content.decode('utf-8'), re.DOTALL)
        if hasattr(classe_i, 'group'):
            self.assertEqual(classe_i.group('id'), 'arron_421')
            self.assertTrue('test_ecriture' in classe_i.group('class'))
        else:
            self.assertTrue(False, msg="pas d\'onglet Montbrison trouvé")

        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction2.manif)
        self.assertNotContains(reponse, self.instruction1.manif)
        # Appel de la vue de détail et test présence manifestation et action possible
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_2')
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=2)

        # Changement d'onglet => Montbrison
        url = "/instructions/tableaudebord/list/?filtre_etat=atraiter&arron=" + classe_i.group('id')[6:]
        reponse = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction1.manif)
        self.assertNotContains(reponse, self.instruction2.manif)

        # Appel de la vue de détail et test présence manifestation et lecture seule
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        if hasattr(detail, 'group'):
            reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test_1')
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=2)

        # print(reponse.content.decode('utf-8'))
