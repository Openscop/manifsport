import re, os

from django.test import TestCase
from django.contrib.auth.hashers import make_password as make

from core.models import Instance
from core.factories import UserFactory, GroupFactory
from evenements.factories import DcnmFactory
from instructions.models import Instruction
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from administration.factories import InstructeurFactory,\
    FederationAgentFactory, GGDAgentFactory, EDSRAgentFactory, CGDAgentFactory, DDSPAgentFactory,\
    CommissariatAgentFactory, MairieAgentFactory, CGAgentFactory, CGServiceAgentFactory, CGSuperieurFactory,\
    SDISAgentFactory, GroupementAgentFactory, CODISAgentFactory, CISAgentFactory, CGDFactory, BrigadeAgentFactory
from administration.factories import BrigadeFactory, CommissariatFactory, CGServiceFactory, CompagnieFactory, CISFactory
from sports.factories import ActiviteFactory, FederationFactory


class Acces_MediaTests(TestCase):
    """
    Test d"accès aux fichiers média déposés dans le dossier
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
        """
        print()
        print('============ Accès Média (Clt) =============')
        # Création des objets sur le 42
        cls.dep = dep = DepartementFactory.create(name='42',
                                                  instance__name="instance de test",
                                                  instance__workflow_ggd=Instance.WF_EDSR,
                                                  instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        autredep = DepartementFactory.create(name='69',
                                                  instance__name="autre instance de test",
                                                  instance__workflow_ggd=Instance.WF_GGD_EDSR,
                                                  instance__instruction_mode=Instance.IM_DEPARTEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        autrearrondissement = ArrondissementFactory.create(name='Roanne', code='97', departement=dep)
        autrearrondissementautredep = ArrondissementFactory.create(name='Lyon', code='95', departement=autredep)
        cls.prefecture = arrondissement.prefecture
        autreprefecture = autrearrondissement.prefecture
        autreprefectureautredep = autrearrondissementautredep.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        # Création des utilisateurs
        grp1 = GroupFactory.create(name='Administrateurs techniques')
        grp2 = GroupFactory.create(name='Administrateurs d\'instance')
        cls.admintech = UserFactory.create(username='admintech', password=make(123), default_instance=dep.instance)
        cls.admintech.groups.add(grp1)
        cls.admindinst = UserFactory.create(username='admindinst', password=make(123), default_instance=dep.instance)
        cls.admindinstautre = UserFactory.create(username='admindinstautre', password=make(123), default_instance=autredep.instance)
        cls.admindinst.groups.add(grp2)
        cls.admindinstautre.groups.add(grp2)

        cls.organisateur = UserFactory.create(username='organisateur', password=make(123), default_instance=dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.autreorganisateur = UserFactory.create(username='autreorganisateur', password=make(123), default_instance=dep.instance)
        autreorganisateur = OrganisateurFactory.create(user=cls.autreorganisateur)
        StructureFactory(commune=cls.autrecommune, organisateur=autreorganisateur)

        cls.instructeur = UserFactory.create(username='instructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=cls.prefecture)
        cls.autreinstructeur = UserFactory.create(username='autreinstructeur', password=make(123), default_instance=dep.instance)
        InstructeurFactory.create(user=cls.autreinstructeur, prefecture=autreprefecture)
        cls.autreinstructeurautredep = UserFactory.create(username='autreinstructeurautredep', password=make(123), default_instance=autredep.instance)
        InstructeurFactory.create(user=cls.autreinstructeurautredep, prefecture=autreprefectureautredep)
        cls.agent_fede = UserFactory.create(username='agent_fede', password=make(123), default_instance=dep.instance)
        activ = ActiviteFactory.create()
        fede = FederationFactory.create()
        FederationAgentFactory.create(user=cls.agent_fede, federation=fede)
        cls.agent_ggd = UserFactory.create(username='agent_ggd', password=make(123), default_instance=dep.instance)
        GGDAgentFactory.create(user=cls.agent_ggd, ggd=dep.ggd)
        cls.edsr = dep.edsr
        cls.agent_edsr = UserFactory.create(username='agent_edsr', password=make(123), default_instance=dep.instance)
        EDSRAgentFactory.create(user=cls.agent_edsr, edsr=dep.edsr)
        cls.cgd = CGDFactory.create(commune=cls.commune)
        cls.agent_cgd = UserFactory.create(username='agent_cgd', password=make(123), default_instance=dep.instance)
        CGDAgentFactory.create(user=cls.agent_cgd, cgd=cls.cgd)
        cls.brigade = BrigadeFactory.create(kind='bta', cgd=cls.cgd, commune=cls.commune)
        cls.agent_brg = UserFactory.create(username='agent_brg', password=make(123), default_instance=dep.instance)
        BrigadeAgentFactory.create(user=cls.agent_brg, brigade=cls.brigade)
        cls.agent_ddsp = UserFactory.create(username='agent_ddsp', password=make(123), default_instance=dep.instance)
        DDSPAgentFactory.create(user=cls.agent_ddsp, ddsp=dep.ddsp)
        cls.commiss = CommissariatFactory.create(commune=cls.commune)
        cls.agent_commiss = UserFactory.create(username='agent_commiss', password=make(123), default_instance=dep.instance)
        CommissariatAgentFactory.create(user=cls.agent_commiss, commissariat=cls.commiss)
        cls.agent_mairie = UserFactory.create(username='agent_mairie', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie, commune=cls.commune)
        cls.agent_mairie_autre = UserFactory.create(username='agent_mairie_autre', password=make(123), default_instance=dep.instance)
        MairieAgentFactory.create(user=cls.agent_mairie_autre, commune=cls.autrecommune)
        cls.agent_cg = UserFactory.create(username='agent_cg', password=make(123), default_instance=dep.instance)
        CGAgentFactory.create(user=cls.agent_cg, cg=dep.cg)
        cls.cgserv = CGServiceFactory.create(name='STD_test', cg=dep.cg, service_type='STD')
        cls.agent_cgserv = UserFactory.create(username='agent_cgserv', password=make(123), default_instance=dep.instance)
        CGServiceAgentFactory.create(user=cls.agent_cgserv, cg_service=cls.cgserv)
        cls.agent_cgsup = UserFactory.create(username='agent_cgsup', password=make(123), default_instance=dep.instance)
        CGSuperieurFactory.create(user=cls.agent_cgsup, cg=dep.cg)
        cls.agent_sdis = UserFactory.create(username='agent_sdis', password=make(123), default_instance=dep.instance)
        SDISAgentFactory.create(user=cls.agent_sdis, sdis=dep.sdis)
        cls.group = CompagnieFactory.create(sdis=dep.sdis, number=98)
        cls.agent_group = UserFactory.create(username='agent_group', password=make(123), default_instance=dep.instance)
        GroupementAgentFactory.create(user=cls.agent_group, compagnie=cls.group)
        cls.agent_codis = UserFactory.create(username='agent_codis', password=make(123), default_instance=dep.instance)
        CODISAgentFactory.create(user=cls.agent_codis, codis=dep.codis)
        cls.cis = CISFactory.create(name='CIS_test', compagnie=cls.group, commune=cls.commune)
        cls.agent_cis = UserFactory.create(username='agent_cis', password=make(123), default_instance=dep.instance)
        CISAgentFactory.create(user=cls.agent_cis, cis=cls.cis)

        # Création de l'événement
        cls.manifestation = DcnmFactory.create(ville_depart=cls.commune, structure=cls.structure, activite=activ,
                                               nom='Manifestation_Test', villes_traversees=(cls.autrecommune,))
        cls.manifestation.description = 'une course qui fait courir'
        cls.manifestation.descriptions_parcours = 'parcours de 10Km'
        cls.manifestation.nb_participants = 1
        cls.manifestation.nb_organisateurs = 10
        cls.manifestation.nb_spectateurs = 100
        cls.manifestation.nb_signaleurs = 1
        cls.manifestation.nb_vehicules_accompagnement = 0
        cls.manifestation.nom_contact = 'durand'
        cls.manifestation.prenom_contact = 'joseph'
        cls.manifestation.tel_contact = '0605555555'
        cls.manifestation.save()

        cls.avis_nb = 6
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire', 'recepisse_declaration'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'itineraire_horaire', 'recepisse_declaration'):
            os.remove(file+".txt")
        super().tearDownClass()

    def test_Acces_Media(self):
        """
        Test des accès aux fichiers déposés avec les différents comptes du circuit
        """
        """
        def print(string="", end=None):
            # Supprimer les prints hors debug
            pass
        """
        def acces_fichier(url, user, acces, show=False):
            """
            Appel de l'url du fichier pour vérifier l'accès
            """
            # Connexion avec l'utilisateur
            if user:
                self.assertTrue(self.client.login(username=user, password='123'))
            # Appel de la page
            retour = self.client.get(url, HTTP_HOST='127.0.0.1:8000')
            if show:
                print("Accès fichier : " + retour.content.decode('utf-8'))
            if acces == "ok":
                self.assertIn('ok pour voir', retour.content.decode('utf-8'))
            elif acces == "nok":
                self.assertIn('pas les droits pour voir', retour.content.decode('utf-8'))
            else:
                self.assertTrue(False, "paramètre incorrect")

        def presence_avis(username, state):
            """
            Appel de la dashboard de l'utilisateur pour tester la présence et l'état de l'événement
            :param username: agent considéré
            :param state: couleur de l'événement
            :return: retour: la réponse http
            """
            # Connexion avec l'utilisateur
            self.assertTrue(self.client.login(username=username, password='123'))
            # Appel de la page
            retour = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000')
            # print(retour.content.decode('utf-8'))
            # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
            # f.write(str(retour.content.decode('utf-8')).replace('\\n',""))
            # f.close()
            # Test du contenu
            if state == 'none':
                if username in ["agent_cis", "agent_codis", "agent_brg1", "agent_brg2"]:
                    nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                else:
                    # Recherche sur "avis demandés", on sous-entend que c'est un TdB agent
                    nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('0', nb_bloc.group('nb'))
            # Test du chiffre dans la bonne case
            elif state == 'nouveau':
                nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'encours':
                nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            elif state == 'rendu':
                nb_bloc = re.search('test_nb_rendu">(?P<nb>(\d))</', retour.content.decode('utf-8'), re.DOTALL)
                self.assertTrue(hasattr(nb_bloc, 'group'))
                self.assertEqual('1', nb_bloc.group('nb'))
            else:
                self.assertEqual('0', '1', 'paramètre inconnu')
            return retour

        def vue_detail(page):
            """
            Appel de la vue de détail de la manifestation
            :param page: réponse précédente
            :return: reponse suivante
            """
            detail = re.search('data-href=\'(?P<url>(/[^"]+))', page.content.decode('utf-8'))
            if hasattr(detail, 'group'):
                page = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
            # self.assertContains(retour, 'Détail de la manifestation<', count=1)
            # print(page.content.decode('utf-8'))
            self.assertContains(page, 'Manifestation_Test')
            return page

        print('--- creation manif ---')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')

        # Récupération de l'url du fichier déposé
        reponse = self.client.get(url_script.group('url'), HTTP_HOST='127.0.0.1:8000')
        url_fichier = re.search('href="(?P<url>(/[^"]+)).+> Déclaration et engagement', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_fichier, 'group'))
        print(url_fichier.group('url'))

        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file4:
            self.client.post(url_script.group('url'), {'cartographie': file4}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/itineraire_horaire.txt') as file5:
            reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file5}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        # Affichage des divers objets
        # Vérification de la création de l'événement
        print(self.manifestation.ville_depart, end=" ; ")
        print(self.manifestation.ville_depart.get_departement(), end=" ; ")
        print(self.manifestation.ville_depart.get_departement().get_instance())
        print(self.manifestation.structure, end=" ; ")
        print(self.manifestation.activite, end=" ; ")
        print(self.manifestation.activite.discipline, end=" ; ")
        print(self.manifestation.activite.discipline.get_federations().first().name)

        print('**** test accès instructeur avant envoi ****')
        acces_fichier(url_fichier.group('url'), 'instructeur', 'nok')

        print('**** test accès agent avant envoi : fédération ****')
        acces_fichier(url_fichier.group('url'), 'agent_fede', 'nok')

        # Connexion avec l'utilisateur
        print('--- envoi manif ---')
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la page tableau de bord organisateur
        self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = self.client.get('/tableau-de-bord-organisateur/liste?&filtre_etat=attente', HTTP_HOST='127.0.0.1:8000')
        detail = re.search('data-href=\'(?P<url>(/[^"]+))', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(detail, 'group'))
        reponse = self.client.get(detail.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Manifestation_Test')
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        # Soumettre la déclaration
        self.assertTrue(hasattr(declar, 'group'))
        self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')

        print('**** test accès instructeur après envoi ****')
        acces_fichier(url_fichier.group('url'), 'instructeur', 'ok')

        print('**** test accès agent après envoi : fédération ****')
        acces_fichier(url_fichier.group('url'), 'agent_fede', 'ok')

        self.instruction = Instruction.objects.get(manif=self.manifestation)
        self.assertEqual(str(self.instruction), str(self.manifestation))

        self.client.logout()
        print('**** test accès anonymous ****')
        acces_fichier(url_fichier.group('url'), '', 'nok')

        print('**** test accès grp technique ****')
        acces_fichier(url_fichier.group('url'), 'admintech', 'ok')

        print('**** test accès grp instance ****')
        acces_fichier(url_fichier.group('url'), 'admindinst', 'ok')

        print('**** test accès autre grp instance ****')
        acces_fichier(url_fichier.group('url'), 'admindinstautre', 'nok')

        print('**** test accès autre organisateur ****')
        acces_fichier(url_fichier.group('url'), 'autreorganisateur', 'nok')

        print('**** test accès organisateur ****')
        acces_fichier(url_fichier.group('url'), 'organisateur', 'ok')

        print('**** test accès autre instructeur d\'arrondissement ****')
        acces_fichier(url_fichier.group('url'), 'autreinstructeur', 'nok')

        print('**** test accès autre instructeur de département ****')
        acces_fichier(url_fichier.group('url'), 'autreinstructeurautredep', 'nok')

        print('**** test accès agent avant distribution : edsr ****')
        acces_fichier(url_fichier.group('url'), 'agent_edsr', 'nok')

        print('**** test accès agent avant distribution : ddsp ****')
        acces_fichier(url_fichier.group('url'), 'agent_ddsp', 'nok')

        print('**** test accès agent avant distribution : mairie ****')
        acces_fichier(url_fichier.group('url'), 'agent_mairie', 'nok')

        print('**** test accès agent avant distribution : cg ****')
        acces_fichier(url_fichier.group('url'), 'agent_cg', 'nok')

        print('**** test accès agent avant distribution : sdis ****')
        acces_fichier(url_fichier.group('url'), 'agent_sdis', 'nok')

        print('--- distribution des avis ---')
        # Instruction de l'avis par la préfecture, vérification de la présence de l'événement en nouveau
        presence_avis('instructeur', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes d\'avis', count=2)
        # Distribuer les demandes d'avis de l'événement avec l'url fournie et tester la redirection
        edit_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(edit_form, 'group'):
            reponse = self.client.post(edit_form.group('url'),
                                       {'edsr_concerne': True,
                                        'ddsp_concerne': True,
                                        'sdis_concerne': True,
                                        'cg_concerne': True,
                                        'villes_concernees': [self.commune.pk]}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, self.instruction)
        # Vérifier le passage en info et le nombre d'avis manquants
        presence_avis('instructeur', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, str(self.avis_nb) + '&nbsp; avis manquants')

        print('**** test accès agent après distribution : edsr ****')
        acces_fichier(url_fichier.group('url'), 'agent_edsr', 'ok')

        print('**** test accès agent après distribution : ddsp ****')
        acces_fichier(url_fichier.group('url'), 'agent_ddsp', 'ok')

        print('**** test accès agent après distribution : mairie ****')
        acces_fichier(url_fichier.group('url'), 'agent_mairie', 'ok')

        print('**** test accès agent après distribution : cg ****')
        acces_fichier(url_fichier.group('url'), 'agent_cg', 'ok')

        print('**** test accès agent après distribution : sdis ****')
        acces_fichier(url_fichier.group('url'), 'agent_sdis', 'ok')

        print('**** test accès agent local avant distribution : cgd ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgd', 'nok')

        print('**** test accès agent avant notification : brg ****')
        acces_fichier(url_fichier.group('url'), 'agent_brg', 'nok')

        print('--- distribution préavis edsr ---')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en nouveau
        presence_avis('agent_edsr', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'cgd_concerne': [self.cgd.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérification du passage en encours
        presence_avis('agent_edsr', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')

        print('**** test accès agent local après distribution : cgd ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgd', 'ok')

        print('--- preavis cgd ---')
        # Instruction du préavis par le cgd, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgd', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        print('\t >>> Informer brigade')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les brigades', count=1)
        # informer les brigades de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les brigades', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'brigades_concernees': [self.brigade.pk]}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation', count=1)

        print('**** test accès agent après notification : brg ****')
        acces_fichier(url_fichier.group('url'), 'agent_brg', 'ok')

        print('\t >>> Rendre préavis')
        # Vérification du passage en encours
        presence_avis('agent_cgd', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)

        print('**** test accès agent avant formattage : ggd ****')
        acces_fichier(url_fichier.group('url'), 'agent_ggd', 'nok')

        print('--- formattage avis edsr ---')
        # Instruction de l'avis par l'edsr, vérification de la présence de l'événement en encours
        presence_avis('agent_edsr', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Formater l'avis de l'événement avec l'url fournie et tester la redirection
        form_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(form_form, 'group'):
            reponse = self.client.post(form_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # vérification de la présence de l'événement en rendu
        presence_avis('agent_edsr', 'rendu')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')

        print('**** test accès agent après formattage : ggd ****')
        acces_fichier(url_fichier.group('url'), 'agent_ggd', 'ok')

        print('**** test accès agent local avant distribution : commissariat ****')
        acces_fichier(url_fichier.group('url'), 'agent_commiss', 'nok')

        print('--- distribution préavis ddsp ---')
        # Instruction de l'avis par le ddsp, vérification de la présence de l'événement en nouveau
        presence_avis('agent_ddsp', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'commissariats_concernes': self.commiss.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérifier le passage en encours
        presence_avis('agent_ddsp', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')

        print('**** test accès agent local après distribution : commissariat ****')
        acces_fichier(url_fichier.group('url'), 'agent_commiss', 'ok')

        print('**** test accès agent local avant distribution : CGService ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgserv', 'nok')

        print('--- distribution préavis cg ---')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cg', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'services_concernes': self.cgserv.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérifier le passage en encours
        presence_avis('agent_cg', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')

        print('**** test accès agent local après distribution : CGService ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgserv', 'ok')

        # Instruction du préavis par le cgservice, vérification de la présence de l'événement en nouveau
        presence_avis('agent_cgserv', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)

        print('**** test accès agent avant formattage : CGSupérieur ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgsup', 'nok')

        print('--- formattage avis cg ---')
        # Instruction de l'avis par le cg, vérification de la présence de l'événement en encours
        presence_avis('agent_cg', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Mettre en forme l\'avis', count=1)
        # Mettre en forme l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Mettre en forme l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # vérification de la présence de l'événement en rendu
        presence_avis('agent_cg', 'rendu')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')

        print('**** test accès agent après formattage : CGSupérieur ****')
        acces_fichier(url_fichier.group('url'), 'agent_cgsup', 'ok')

        print('**** test accès agent local avant distribution : groupement ****')
        acces_fichier(url_fichier.group('url'), 'agent_group', 'nok')

        print('**** test accès agent avant rendu : codis ****')
        acces_fichier(url_fichier.group('url'), 'agent_codis', 'nok')

        print('**** test accès agent avant notification : cis ****')
        acces_fichier(url_fichier.group('url'), 'agent_cis', 'nok')

        print('--- distribution préavis sdis ---')
        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en nouveau
        presence_avis('agent_sdis', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Envoyer les demandes', count=1)
        # Envoyer les préavis de l'événement avec l'url fournie et tester la redirection
        disp_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer les demandes', reponse.content.decode('utf-8'))
        if hasattr(disp_form, 'group'):
            reponse = self.client.post(disp_form.group('url'), {'compagnies_concernees': self.group.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)
        # Vérification du passage en encours
        presence_avis('agent_sdis', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, '1 préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')

        print('**** test accès agent local après distribution : groupement ****')
        acces_fichier(url_fichier.group('url'), 'agent_group', 'ok')

        # Instruction du préavis par le groupement, vérification de la présence de l'événement en nouveau
        presence_avis('agent_group', 'nouveau')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre le pré-avis', count=1)
        # Rendre le préavis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre le pré-avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)

        # Instruction de l'avis par le sdis, vérification de la présence de l'événement en encours
        presence_avis('agent_sdis', 'encours')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=encours', HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'préavis manquants')
        self.assertContains(reponse, 'Délai : 21 jours')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Rendre l\'avis', count=1)
        # Rendre l'avis de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Rendre l\'avis', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'favorable': True}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)

        print('**** test accès agent après rendu : codis ****')
        acces_fichier(url_fichier.group('url'), 'agent_codis', 'ok')

        print('**** test 26 notification cis du groupement ****')
        # Instruction du préavis par le groupement, vérification de la présence de l'événement en rendu
        presence_avis('agent_group', 'rendu')
        # appel ajax pour avoir la liste
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=rendu', HTTP_HOST='127.0.0.1:8000')
        # Appel de la vue de détail et test présence manifestation
        reponse = vue_detail(reponse)
        # Vérifier l'action disponible
        self.assertContains(reponse, 'Informer les CIS', count=1)
        # Informer les CIS de l'événement avec l'url fournie et tester la redirection
        ack_form = re.search('href="(?P<url>(/[^"]+)).+\\n.+Informer les CIS', reponse.content.decode('utf-8'))
        if hasattr(ack_form, 'group'):
            reponse = self.client.post(ack_form.group('url'), {'cis_concernes': self.cis.pk}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Détail de la manifestation<', count=1)

        print('**** test accès agent après notification : cis ****')
        acces_fichier(url_fichier.group('url'), 'agent_cis', 'ok')
