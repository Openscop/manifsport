# coding: utf-8
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.contrib.contenttypes.models import ContentType
from django.utils import timezone
from django.db.models import Q
from django.shortcuts import render

from instructions.models import Instruction
from core.util.permissions import require_role
from core.models.instance import Instance
from core.util.user import UserHelper
from administrative_division.models import Arrondissement


def get_instructions(user, temporalite, filtre_specifique, role, instance, request):
    manifacceslibre = Instruction.objects.all().prefetch_related('manif__ville_depart__arrondissement__departement')
    if temporalite == "a_traiter":  # cas général
        manifacceslibre = manifacceslibre.filter(manif__date_fin__gte=timezone.now())
    elif temporalite == "en_retard":  # todo : à supprimer ou à développer
        manifacceslibre = manifacceslibre
    elif temporalite == "archive":
        manifacceslibre = manifacceslibre.filter(manif__date_fin__lt=timezone.now())

    # Filtrer les manifs en fonctions du rôle de l'utilisateur
    if not filtre_specifique and role in ['federationagent', 'serviceagent', 'ddspagent', 'cgagent',
                                          'mairieagent', 'cgsuperieuragent', 'sdisagent'] \
            and UserHelper.get_role_name(user) in ['federationagent', 'serviceagent', 'ddspagent', 'cgagent',
                                                   'mairieagent', 'cgsuperieuragent', 'sdisagent']:
        service = user.get_service()
        ct_service = ContentType.objects.get_for_model(service)
        manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
        if user.has_role('cgsuperieuragent') and role == 'cgsuperieuragent':
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                     avis__etat__in=['formaté', 'rendu']).distinct()
    elif user.has_role('codisagent') and role == 'codisagent':
        service = user.get_departement().sdis
        ct_service = ContentType.objects.get_for_model(service)
        manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                 avis__etat='rendu')
    elif user.has_role('ggdagent') and role == 'ggdagent':
        if instance.get_workflow_ggd() == Instance.WF_EDSR:
            service = user.get_departement().edsr
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                     avis__etat__in=['formaté', 'rendu']).distinct()
        else:
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
    elif user.has_role('edsragent') and role == 'edsragent':
        if instance.get_workflow_ggd() == Instance.WF_GGD_EDSR:
            service = user.get_departement().ggd
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id,
                                                     avis__etat__in=['transmis', 'distribué', 'formaté', 'rendu']).distinct()
        else:
            service = user.get_service()
            ct_service = ContentType.objects.get_for_model(service)
            manifacceslibre = manifacceslibre.filter(avis__content_type=ct_service, avis__object_id=service.id)
    elif user.has_role('cisagent') and role == 'cisagent':
        service = user.get_service()
        ct_service = ContentType.objects.get_for_model(service)
        manifacceslibre = manifacceslibre.filter(avis__acces__isnull=False,
                                                 avis__acces__content_type=ct_service,
                                                 avis__acces__object_id=service.id,
                                                 avis__etat='rendu').distinct()
    elif user.has_role('brigadeagent') and role == 'brigadeagent':
        service = user.get_service()
        ct_service = ContentType.objects.get_for_model(service)
        manifacceslibre = manifacceslibre.filter(avis__acces__isnull=False,
                                                 avis__acces__content_type=ct_service,
                                                 avis__acces__object_id=service.id).distinct()

    elif role in ['compagnieagentlocal', 'cgdagentlocal', 'commissariatagentlocal',
                  'cgserviceagentlocal', 'edsragentlocal'] \
            and UserHelper.get_role_name(user) in ['compagnieagentlocal', 'cgdagentlocal', 'commissariatagentlocal',
                                                   'cgserviceagentlocal', 'edsragentlocal']:
        service = user.get_service()
        ct_service = ContentType.objects.get_for_model(service)
        manifacceslibre = manifacceslibre.filter(avis__preavis__content_type=ct_service,
                                                 avis__preavis__object_id=service.id)

    elif user.has_role('instructeur') and role == 'instructeur':
        config_instructeur = user.get_instance().get_instruction_mode()

        if request.GET.get('arron'):
            if config_instructeur == Instance.IM_ARRONDISSEMENT and user.get_instance().acces_arrondissement:
                manifacceslibre = manifacceslibre.filter(manif__ville_depart__arrondissement__code=request.GET.get('arron'))
        elif config_instructeur == Instance.IM_ARRONDISSEMENT:
            manifacceslibre = manifacceslibre.filter(
                manif__ville_depart__arrondissement=user.instructeur.get_prefecture().arrondissement)
        else:  # Département
            manifacceslibre = manifacceslibre.filter(
                manif__ville_depart__arrondissement__departement=user.instructeur.get_prefecture().arrondissement.departement)
        if filtre_specifique == 'instructionmairie':  # Instruction en mairie
            manifacceslibre = manifacceslibre.filter(Q(manif__villes_traversees__isnull=True) &
                                                     Q(manif__dvtm__isnull=True) & Q(manif__avtm__isnull=True) & Q(
                manif__avtmcir__isnull=True))
        else:  # Instruction en préfecture
            result1 = manifacceslibre.filter(manif__villes_traversees__isnull=False)
            result2 = manifacceslibre.filter(Q(manif__villes_traversees__isnull=True) &
                                             Q(manif__dvtm__isnull=False) | Q(manif__avtm__isnull=False) | Q(
                manif__avtmcir__isnull=False))
            manifacceslibre = (result1 | result2).distinct()

    # agent mairie en tant qu'instructeur
    elif user.has_role('mairieagent') and role == 'mairieagent' and filtre_specifique == 'instructionmairie':
        commune = user.agent.mairieagent.commune
        manifacceslibre = manifacceslibre.filter(
            Q(manif__ville_depart=commune) & Q(manif__villes_traversees__isnull=True) &
            Q(manif__dvtm__isnull=True) & Q(manif__avtm__isnull=True) & Q(manif__avtmcir__isnull=True))
    else:
        return Instruction.objects.none()

    return manifacceslibre


class TableauBody(View):

    @method_decorator(require_role(['agent', 'agentlocal', 'instructeur']))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, role=None):

        user = request.user
        instance = user.get_instance()

        temporalite = self.request.GET.get('temporalite', default='a_traiter')
        filtre_specifique = self.request.GET.get('filtre_specifique', default=None)

        # Premier role de l'utilisateur ou paramètre de l'url
        if not role:
            role = UserHelper.get_role_name(user)

        # context pour l'instruction d'arrondissement
        if user.has_role('instructeur') and temporalite == "a_traiter":
            onglet = bool(
                user.get_instance().acces_arrondissement and not
                user.get_instance().instruction_mode and not
                self.request.GET.get('temporalite', default=None) == 'archive')
            user_arrondis = user.instructeur.get_prefecture().arrondissement.code
            liste_arrondis = Arrondissement.objects.filter(departement=user.get_instance().departement)

            if self.request.GET.get('arron'):
                actual_arrondis = self.request.GET.get('arron')
            else:
                actual_arrondis = user_arrondis
        else:
            onglet = False
            actual_arrondis = None
            user_arrondis = None
            liste_arrondis = None

        # avoir la liste des instructions
        instructions = get_instructions(user, temporalite, filtre_specifique, role, instance, request)

        # context pour l'instruction departement des services simples
        if user.has_role('serviceagent') and role != "instructeur":
            service_dept = user.get_service().departements.all()
            onglet_service = bool(len(service_dept) > 1)
            liste_dept = service_dept

            dept = self.request.GET.get('dept', default=user.get_departement().name)
            actual_dept = dept
            if dept:
                instructions = instructions.filter(manif__ville_depart__arrondissement__departement__name=dept)
        else:
            onglet_service = False
            liste_dept = None
            actual_dept = None

        user_is_instructeur = bool(user.has_role('instructeur') and
                                   role == 'instructeur' or user.has_role('mairieagent') and
                                   self.request.GET.get('filtre_specifique', default=None) == 'instructionmairie')
        archive = bool(self.request.GET.get('temporalite', default=None) == 'archive')
        instruction_mairie = bool(self.request.GET.get('filtre_specifique', default=None) == 'instructionmairie')

        # Contexte général
        context = {
            "user_is_instructeur": user_is_instructeur,
            "archive": archive,
            "instruction_mairie": instruction_mairie,
            "role": role,
        }

        # Détermination du cas en présence
        usertype = ""
        if user.has_role('instructeur') and role != "serviceagent" or user.has_role('mairieagent') and filtre_specifique:
            usertype = "instructeur"
        elif user.has_role('agent') or user.has_role('mairieagent') and not filtre_specifique:
            usertype = "avis"
        elif user.has_role('agentlocal'):
            usertype = "preavis"

        # Définition des dicts de chaque bloc
        indic, rendu_tab, autorise_tab, annule_tab, interdit_tab, atraiter_tab, encours_tab = {}, {}, {}, {}, {}, {}, {}
        indic['rendu'], indic['autorise'], indic['annule'], indic['interdit'], indic['atraiter'], indic['encours'] = 0, 0, 0, 0, 0, 0
        # Initialisation des dicts
        for dic in [rendu_tab, autorise_tab, annule_tab, interdit_tab, atraiter_tab, encours_tab]:
            for cle in ['autorisation', 'declaration', 'competition', 'noncompetition', 'nonmotorise', 'motorise', 'circuit', 'velo']:
                dic[cle] = 0
        # Regroupement des dicts
        dic_gen = {"atraiter": atraiter_tab, "encours": encours_tab, "rendu": rendu_tab,
                   "autorise": autorise_tab, "annule": annule_tab, "interdit": interdit_tab}

        if usertype == "instructeur":
            # Total de chaque bloc
            indic['atraiter'] = instructions.filter(etat="demandée").count()
            indic['encours'] = instructions.filter(etat="distribuée").count()
            indic['autorise'] = instructions.filter(etat='autorisée').count()
            indic['annule'] = instructions.filter(etat='annulée').count()
            indic['interdit'] = instructions.filter(etat='interdite').count()

            # Remplissage des données par bloc
            for instru in instructions:
                # Distribution des instructions par blocs
                etat = instru.etat
                if etat == "demandée":
                    cle = "atraiter"
                elif etat == "distribuée":
                    cle = "encours"
                elif etat == "interdite":
                    cle = "interdit"
                elif etat == "annulée":
                    cle = "annule"
                elif etat == "autorisée":
                    cle = "autorise"
                # Sélection du dictionnaire du bloc
                dic = dic_gen[cle]
                # Incrémentation des catégories du tooltip du bloc
                if hasattr(instru.manif, "avtm") or hasattr(instru.manif, "avtmcir"):
                    dic['autorisation'] += 1
                else:
                    dic['declaration'] += 1
                if hasattr(instru.manif, "dcnm") or hasattr(instru.manif, "dcnmc"):
                    dic['competition'] += 1
                else:
                    dic['noncompetition'] += 1
                if hasattr(instru.manif, "avtm") or hasattr(instru.manif, "avtmcir") or hasattr(instru.manif, "dvtm"):
                    dic['motorise'] += 1
                else:
                    dic['nonmotorise'] += 1
                if hasattr(instru.manif, 'avtmcir'):
                    dic['circuit'] += 1
                if hasattr(instru.manif, 'dcnmc') or hasattr(instru.manif, 'dnmc'):
                    dic['velo'] += 1

        elif usertype == "avis":

            # Remplissage des données par bloc
            for instru in instructions:
                # Distribution des instructions par blocs
                etat = instru.get_avis_user(user).etat
                if instru.etat == "interdite":
                    cle = "interdit"
                elif instru.etat == "annulée":
                    cle = "annule"
                elif instru.etat == "autorisée":
                    cle = "autorise"
                else:
                    if etat == "demandé":
                        if not user.has_role('cgsuperieuragent') and \
                                not (user.has_role("edsragent") and user.get_instance().workflow_ggd == Instance.WF_GGD_EDSR) and \
                                not (user.has_role("ggdagent") and user.get_instance().workflow_ggd == Instance.WF_EDSR):
                            cle = "atraiter"
                    elif etat == "transmis":
                        if user.has_role('edsragent') and user.get_instance().workflow_ggd == Instance.WF_GGD_EDSR:
                            cle = "atraiter"
                        if user.has_role('ggdagent') and user.get_instance().workflow_ggd == Instance.WF_GGD_EDSR:
                            cle = "encours"
                    elif etat == "formaté":
                        if user.has_role('cgsuperieuragent') or user.has_role("ggdagent"):
                            cle = "atraiter"
                        if user.has_role('brigadeagent'):
                            cle = "encours"
                        if user.has_role('cgagent') or user.has_role('edsragent'):
                                cle = "rendu"
                    elif etat == "distribué":
                        if not user.has_role('cgsuperieuragent') and \
                                not (user.has_role("ggdagent") and user.get_instance().workflow_ggd == Instance.WF_EDSR):
                            cle = "encours"
                    elif etat == "rendu":
                            cle = "rendu"
                # Sélection du dictionnaire du bloc
                dic = dic_gen[cle]
                # Incrémentation du total du bloc
                indic[cle] += 1
                # Incrémentation des catégories du tooltip du bloc
                if hasattr(instru.manif, "avtm") or hasattr(instru.manif, "avtmcir"):
                    dic['autorisation'] += 1
                else:
                    dic['declaration'] += 1
                if hasattr(instru.manif, "dcnm") or hasattr(instru.manif, "dcnmc"):
                    dic['competition'] += 1
                else:
                    dic['noncompetition'] += 1
                if hasattr(instru.manif, "avtm") or hasattr(instru.manif, "avtmcir") or hasattr(instru.manif, "dvtm"):
                    dic['motorise'] += 1
                else:
                    dic['nonmotorise'] += 1
                if hasattr(instru.manif, 'avtmcir'):
                    dic['circuit'] += 1
                if hasattr(instru.manif, 'dcnmc') or hasattr(instru.manif, 'dnmc'):
                    dic['velo'] += 1

        elif usertype == "preavis":

            # Remplissage des données par bloc
            for instru in instructions:
                # Distribution des instructions par blocs
                etat = instru.get_preavis_user(user).etat
                if instru.etat == "interdite":
                    cle = "interdit"
                elif instru.etat == "annulée":
                    cle = "annule"
                elif instru.etat == "autorisée":
                    cle = "autorise"
                else:
                    if etat == "demandé":
                        cle = "atraiter"
                    elif etat == "notifié":
                        cle = "encours"
                    elif etat == "rendu":
                        cle = "rendu"
                # Sélection du dictionnaire du bloc
                dic = dic_gen[cle]
                # Incrémentation du total du bloc
                indic[cle] += 1
                # Incrémentation des catégories du tooltip du bloc
                if hasattr(instru.manif, "avtm") or hasattr(instru.manif, "avtmcir"):
                    dic['autorisation'] += 1
                else:
                    dic['declaration'] += 1
                if hasattr(instru.manif, "dcnm") or hasattr(instru.manif, "dcnmc"):
                    dic['competition'] += 1
                else:
                    dic['noncompetition'] += 1
                if hasattr(instru.manif, "avtm") or hasattr(instru.manif, "avtmcir") or hasattr(instru.manif, "dvtm"):
                    dic['motorise'] += 1
                else:
                    dic['nonmotorise'] += 1
                if hasattr(instru.manif, 'avtmcir'):
                    dic['circuit'] += 1
                if hasattr(instru.manif, 'dcnmc') or hasattr(instru.manif, 'dnmc'):
                    dic['velo'] += 1

        afficher_encours = bool(not (user.has_role('agentlocal') and not user.has_role('cgdagentlocal')) and
                                not user.has_role('cgsuperieuragent') and
                                not user.has_role('serviceagent') and
                                not user.has_role('federationagent') and
                                not (user.has_role('mairieagent') and not usertype == "instructeur") and
                                not (user.has_role("ggdagent") and user.get_instance().workflow_ggd == Instance.WF_EDSR))
        afficher_atraiter = bool(not user.has_role('cisagent') and
                                not user.has_role('codisagent') and
                                not user.has_role('brigadeagent'))

        context = { **context,
                    "atraiter": indic['atraiter'],
                    "atraiter_tab": atraiter_tab,
                    "encours": indic['encours'],
                    "encours_tab": encours_tab,
                    "rendu": indic['rendu'],
                    "rendu_tab": rendu_tab,
                    "autorise": indic['autorise'],
                    "autorise_tab": autorise_tab,
                    "interdit": indic['interdit'],
                    "interdit_tab": interdit_tab,
                    "annule": indic['annule'],
                    "annule_tab": annule_tab,
                    "onglet": onglet,
                    "user_arrondis": user_arrondis,
                    "liste_arrondis": liste_arrondis,
                    "actual_arrondis": actual_arrondis,
                    "onglet_service": onglet_service,
                    "liste_dept": liste_dept,
                    "actual_dept": actual_dept,
                    "afficher_encours": afficher_encours,
                    "afficher_atraiter": afficher_atraiter,
                    }

        return render(request, "instructions/tableaudebord_body.html", context)


class TableauListe(View):

    @method_decorator(require_role(['agent', 'agentlocal', 'instructeur']))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get(self, request, role=None):
        user = request.user
        instance = user.get_instance()

        temporalite = self.request.GET.get('temporalite', default='a_traiter')
        filtre_specifique = self.request.GET.get('filtre_specifique', default=None)
        filtre_etat = self.request.GET.get('filtre_etat', default=None)

        if not role:
            role = UserHelper.get_role_name(user)

        user_is_instructeur = bool(user.has_role('instructeur') and
                                   role == 'instructeur' or user.has_role('mairieagent') and
                                   self.request.GET.get('filtre_specifique', default=None) == 'instructionmairie')
        archive = bool(self.request.GET.get('temporalite', default=None) == 'archive')
        instruction_mairie = bool(self.request.GET.get('filtre_specifique', default=None) == 'instructionmairie')

        # avoir la liste des instructions
        instructions = get_instructions(user, temporalite, filtre_specifique, role, instance, request)

        dept = self.request.GET.get('dept', default=user.get_departement().name)
        if user.has_role('serviceagent'):
            if dept:
                instructions = instructions.filter(manif__ville_depart__arrondissement__departement__name=dept)

        instructions_liste = []
        titre = ""

        if user_is_instructeur:
            # Pas de marquage spécial, les lignes seront colorées selon l'état
            if filtre_etat:
                # Instructions du bloc
                if filtre_etat == "atraiter":
                    titre  ="Liste des manisfestations à traiter"
                    instructions_liste = instructions.filter(etat="demandée")
                elif filtre_etat == "encours":
                    titre  ="Liste des manisfestations en cours d'instruction"
                    instructions_liste = instructions.filter(etat="distribuée")
                elif filtre_etat == "autorise":
                    titre  ="Liste des manisfestations autorisées"
                    instructions_liste = instructions.filter(etat="autorisée")
                elif filtre_etat == "interdit":
                    titre  ="Liste des manisfestations interdites"
                    instructions_liste = instructions.filter(etat="interdite")
                elif filtre_etat == "annule":
                    titre  ="Liste des manisfestations annulées"
                    instructions_liste = instructions.filter(etat="annulée")
            else:
                # Tout voir
                titre = "Liste des manisfestations à venir"
                instructions_liste = instructions

        elif user.has_role('agent'):
            # Marquer chaque instruction avec l'attribut "css" pour colorer la ligne
            if not filtre_etat:
                # Tout voir
                filtre_etat = ["atraiter", "encours", "rendu", "autorise", "interdit", "annule"]
            for instruction in instructions:
                if instruction.etat not in ["autorisée", "annulée", "interdite"]:
                    if "atraiter" in filtre_etat:
                        titre = "Liste des avis à traiter"
                        if not user.has_role('cgsuperieuragent') and \
                                not (user.has_role("edsragent") and user.get_instance().workflow_ggd == Instance.WF_GGD_EDSR) and \
                                not (user.has_role("ggdagent") and user.get_instance().workflow_ggd == Instance.WF_EDSR):
                            if instruction.get_avis_user(user).etat == "demandé":
                                instruction.css = "table_afaire"
                                instructions_liste.append(instruction)
                        if user.has_role("edsragent") and user.get_instance().workflow_ggd == Instance.WF_GGD_EDSR:
                            if instruction.get_avis_user(user).etat == "transmis":
                                instruction.css = "table_afaire"
                                instructions_liste.append(instruction)
                        if user.has_role('cgsuperieuragent') or user.has_role("ggdagent"):
                            if instruction.get_avis_user(user).etat == "formaté":
                                instruction.css = "table_afaire"
                                instructions_liste.append(instruction)
                    if "encours" in filtre_etat:
                        titre = "Liste des avis en cours"
                        if not user.has_role('cgsuperieuragent') and not user.has_role('brigadeagent') and \
                                not (user.has_role("ggdagent") and user.get_instance().workflow_ggd == Instance.WF_EDSR):
                            if instruction.get_avis_user(user).etat == "distribué":
                                instruction.css = "table_encours"
                                instructions_liste.append(instruction)
                        if user.has_role('ggdagent') and user.get_instance().workflow_ggd == Instance.WF_GGD_EDSR:
                            if instruction.get_avis_user(user).etat == "transmis":
                                instruction.css = "table_encours"
                                instructions_liste.append(instruction)
                        if user.has_role('brigadeagent'):
                            if instruction.get_avis_user(user).etat in ["distribué", "formaté"]:
                                instruction.css = "table_encours"
                                instructions_liste.append(instruction)
                    if "rendu" in filtre_etat:
                        titre = "Liste des avis rendus pour les manifestations en cours d'instruction"
                        if instruction.get_avis_user(user).etat == "rendu":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                        if user.has_role('cgagent') or user.has_role('edsragent'):
                            if instruction.get_avis_user(user).etat == "formaté":
                                instruction.css = "table_termine"
                                instructions_liste.append(instruction)
                else:
                    if "autorise" in filtre_etat:
                        titre = "Liste des avis des manifestations autorisées"
                        if instruction.etat == "autorisée":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    if "interdit" in filtre_etat:
                        titre = "Liste des avis des manifestations interdites"
                        if instruction.etat == "interdite":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    if "annule" in filtre_etat:
                        titre = "Liste des avis des manifestations annulées"
                        if instruction.etat == "annulée":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
            if type(filtre_etat) is list:
                titre = "Liste des avis des manifestations à venir"

        elif user.has_role('agentlocal'):
            # Marquer chaque instruction avec l'attribut "css" pour colorer la ligne
            if filtre_etat:
                # Pré-avis du bloc
                for instruction in instructions:
                    if filtre_etat == "atraiter":
                        titre = "Liste des pré-avis à traiter"
                        if instruction.get_preavis_user(user).etat == "demandé" and instruction.etat not in ["autorisée", "annulée", "interdite"]:
                            instruction.css = "table_afaire"
                            instructions_liste.append(instruction)
                    elif filtre_etat == "encours":
                        titre = "Liste des pré-avis en cours"
                        if instruction.get_preavis_user(user).etat == "notifié" and instruction.etat not in ["autorisée", "annulée", "interdite"]:
                            instruction.css = "table_encours"
                            instructions_liste.append(instruction)
                    elif filtre_etat == "rendu":
                        titre = "Liste des pré-avis rendus pour les manifestations en cours d'instruction"
                        if instruction.get_preavis_user(user).etat == "rendu" and instruction.etat not in ["autorisée", "annulée", "interdite"]:
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    elif filtre_etat == "autorise":
                        titre = "Liste des pré-avis des manifestations autorisées"
                        if instruction.etat == "autorisée":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    elif filtre_etat == "interdit":
                        titre = "Liste des pré-avis des manifestations interdites"
                        if instruction.etat == "interdite":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    elif filtre_etat == "annule":
                        titre = "Liste des pré-avis des manifestations annulées"
                        if instruction.etat == "annulée":
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
            else:
                # Tout voir
                titre = "Liste des pré-avis des manifestations à venir"
                for instruction in instructions:
                    if instruction.etat not in ["autorisée", "annulée", "interdite"]:
                        if instruction.get_preavis_user(user).etat == "demandé":
                            instruction.css = "table_afaire"
                            instructions_liste.append(instruction)
                        elif instruction.get_preavis_user(user).etat == "notifié":
                            instruction.css = "table_encours"
                            instructions_liste.append(instruction)
                        else:
                            instruction.css = "table_termine"
                            instructions_liste.append(instruction)
                    else:
                        instruction.css = "table_termine"
                        instructions_liste.append(instruction)

        context = {
            "user_is_instructeur": user_is_instructeur,
            "archive": archive,
            "instruction_mairie": instruction_mairie,
            "role": role,
            "instructions": instructions_liste,
            "titre": titre,
        }

        return render(request, "instructions/tableaudebord_liste.html", context)
