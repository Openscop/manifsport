from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class AutorisationAcces(models.Model):
    """
    Classe qui permet de donner l'accès aux avis pour les CIS et les Brigades notifiées et
    plus généralement, à un service qui n'a pas à rendre d'avis

    """

    # User qui créer l'autorisation d'accès
    referent = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='instructeur référent', on_delete=models.CASCADE)
    # Service autorisé
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    service_object = GenericForeignKey()

    # Accès sur la manifestation seule ou avec un avis
    manif = models.ForeignKey("evenements.manif", verbose_name="manif", on_delete=models.CASCADE)
    avis = models.ForeignKey("instructions.avis", verbose_name="avis", blank=True, null=True, on_delete=models.CASCADE)

    # Overrides
    def __str__(self):
        des = "Accès à " + str(self.manif)
        if self.avis:
            des = des + " , avis : " + str(self.avis)
        return des + " pour " + str(self.service_object)

    # Meta
    class Meta:
        verbose_name = 'autorisation d\'accès'
        verbose_name_plural = 'autorisations d\'accès'
        default_related_name = "acces"
        app_label = "instructions"
