from django.core.exceptions import ObjectDoesNotExist
from django.contrib.contenttypes.models import ContentType
from django.template.loader import render_to_string
from django.db import models
from django.db.models import Q
from django_fsm import FSMField, transition
from django.conf import settings
from django.utils import timezone
from django.urls import reverse

from evenements.models.manifestation import ManifRelatedModel
from configuration.directory import UploadPath
from administration.models import Agent, AgentLocal
from administration.models.service import Brigade, CIS
from core.models.instance import Instance
from ..models import Avis, PreAvis
from core.tasks import creation_thumbail_doc_officiel
from core.FileTypevalidator import file_type_validator


class InstructionQuerySet(models.QuerySet):
    """ Queryset des instructions d'événements sportifs"""

    def par_instructeur(self, instructeur):
        """ Renvoyer les instructions pour l'instructeur """
        from core.models import Instance
        config_instructeur = instructeur.user.get_instance().get_instruction_mode()
        if config_instructeur == Instance.IM_ARRONDISSEMENT:
            return self.filter(manif__ville_depart__arrondissement=instructeur.get_prefecture().arrondissement)
        else:  # Département
            return self.filter(manif__ville_depart__arrondissement__departement=instructeur.get_prefecture().arrondissement.departement)

    def termine(self):
        """ Renvoyer les instructions pour lesquelles l'événement prévu est terminé """
        return self.filter(manif__date_fin__lt=timezone.now())

    def closest_first(self):
        """ Renvoyer les instructions, triées par date de l'événement croissante """
        return self.order_by('manif__date_debut')

    def last_first(self):
        """ Renvoyer les instructions, triées par date de l'événement décroissante """
        return self.order_by('-manif__date_debut')

    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(manif__instance=request.user.get_instance())
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(manif__instance__in=instances)


class Instruction(ManifRelatedModel):
    """
    Dossier d'instruction d'une demande de manifestation sportive
    """
    # Champs
    etat = FSMField(default='demandée', verbose_name="état de l'instruction")  # Voir liste des états dans : documentation/Version-4_description.md
    date_creation = models.DateField("date de création", auto_now_add=True)
    date_consult = models.DateField("date d'envoi des demandes d'avis", blank=True, null=True)
    date_derniere_action = models.DateTimeField('Date de dernière action', blank=True, null=True)
    manif = models.OneToOneField('evenements.manif', related_name="instruction", verbose_name="manifestation associée", on_delete=models.CASCADE)
    referent = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, related_name="referent", verbose_name='instructeur référent', on_delete=models.SET_NULL)

    services_concernes = models.ManyToManyField('administration.service', verbose_name="services concernés", blank=True)
    villes_concernees = models.ManyToManyField('administrative_division.commune', verbose_name="villes concernées", blank=True)
    ddsp_concerne = models.BooleanField("avis DDSP requis", default=False)
    edsr_concerne = models.BooleanField("avis EDSR requis", default=False)
    ggd_concerne = models.BooleanField("avis GGD requis", default=False)
    sdis_concerne = models.BooleanField("avis SDIS requis", default=False)
    cg_concerne = models.BooleanField("avis Conseil Départemental requis", default=False)
    doc_verif = models.BooleanField("Documents en annexe vérifiés", default=False)

    objects = InstructionQuerySet.as_manager()

    def __str__(self):
        return self.manif.nom

    def get_absolute_url(self):
        """ Renvoyer l'URL de l'instruction """
        return reverse('instructions:instruction_detail', kwargs={'pk': self.pk})

    def get_tous_avis(self):
        """ Renvoyer les avis pour l'instruction """
        return self.avis.all()

    def get_avis_user(self, user):
        """
        Renvoyer l'avis pour l'instruction et l'utilisateur
        L'instruction contient bien un avis concernant l'utilisateur
        """
        instance = user.get_instance()
        if user.agent:
            filtre_spe = Q()
            service = user.get_service()
            if user.has_role('cgsuperieuragent'):
                filtre_spe = Q(etat__in=['rendu', 'formaté'])
            elif user.has_role('codisagent'):
                service = user.get_departement().sdis
                filtre_spe = Q(etat='rendu')
            elif user.has_role('ggdagent'):
                if instance.get_workflow_ggd() == Instance.WF_EDSR:
                    service = user.get_departement().edsr
                    filtre_spe = Q(etat__in=['rendu', 'formaté'])
            elif user.has_role('edsragent'):
                if instance.get_workflow_ggd() == Instance.WF_GGD_EDSR:
                    service = user.get_departement().ggd
                    filtre_spe = Q(etat__in=['transmis', 'distribué', 'formaté', 'rendu'])
            elif user.has_role('brigadeagent'):
                ct_brg = ContentType.objects.get_for_model(Brigade)
                ct_id = user.get_service().id
                if instance.get_workflow_ggd() == Instance.WF_EDSR:
                    service = user.get_departement().edsr
                else:
                    service = user.get_departement().ggd
                filtre_spe = Q(acces__content_type=ct_brg) & Q(acces__object_id=ct_id)
            elif user.has_role('cisagent'):
                ct_cis = ContentType.objects.get_for_model(CIS)
                ct_id = user.get_service().id
                service = user.get_departement().sdis
                filtre_spe = Q(etat='rendu') & Q(acces__content_type=ct_cis) & Q(acces__object_id=ct_id)
            ct_service = ContentType.objects.get_for_model(service)
            if self.avis.filter(filtre_spe).filter(content_type=ct_service, object_id=service.id).exists():
                return self.avis.filter(filtre_spe).distinct().get(content_type=ct_service, object_id=service.id)
        return None

    def get_preavis_user(self, user):
        """
        Renvoyer le pré-avis pour l'instruction et l'utilisateur
        L'instruction contient bien un avis concernant l'utilisateur
        """
        if user.agentlocal:
            if user.has_role('compagnieagentlocal'):
                service = user.get_departement().sdis
            elif user.has_role('cgserviceagentlocal'):
                service = user.get_departement().cg
            elif user.has_role('commissariatagentlocal'):
                service = user.get_departement().ddsp
            elif user.has_role('edsragentlocal'):
                service = user.get_departement().ggd
            elif user.has_role('cgdagentlocal'):
                # Si le WF GGD a changé, il va cohabiter des avis GGD et des avis EDSR dans le TdB suivant l'instruction
                if self.avis.filter(service_concerne='edsr').exists():
                    service = user.get_departement().edsr
                else:
                    service = user.get_departement().ggd
            ct_service = ContentType.objects.get_for_model(service)
            if self.avis.filter(content_type=ct_service, object_id=service.id).exists():
                avis = self.avis.get(content_type=ct_service, object_id=service.id)
                return avis.get_preavis_user(user)
        return None

    def get_prefecture_concernee(self):
        """ Renvoyer la préfecture de la manif """
        try:
            return self.manif.ville_depart.get_prefecture()
        except (AttributeError, ObjectDoesNotExist):
            return None

    def get_all_agents(self):
        """  Renvoyer tous les agents et agents locaux concernés par l'instruction """
        # Ajout des agents locaux concernés
        recipients = []
        for preavis in PreAvis.objects.filter(avis__instruction=self).order_by('pk'):
            recipients += AgentLocal.users_from_agents(preavis.get_agents())
            recipients.append(preavis.destination_object)
            # Ajout des brigades notifiées
            if preavis.service_concerne == 'cgd':
                from messagerie.models import Enveloppe
                for notif_brg in Enveloppe.objects.filter(doc_objet='preavis',
                                                          manifestation=self.manif,
                                                          corps__corps__contains="Prenez connaissance",
                                                          corps__object_id=preavis.id):
                    if notif_brg.destinataire_id:
                        recipients.append(notif_brg.destinataire_id)
                        recipients.append(notif_brg.destinataire_id.get_service())

        # Ajout des agents concernés
        for avis in Avis.objects.filter(instruction=self).order_by('pk'):
            recipients += Agent.users_from_agents(avis.get_agents())
            service = avis.get_service()
            # Ajout des agents n+1
            if avis.service_concerne == 'cg' and avis.etat in ['formaté', 'rendu']:
                recipients += service.get_cgsuperieurs()
            if avis.service_concerne == 'edsr' and avis.etat in ['formaté', 'rendu']:
                if hasattr(service.departement, 'ggd'):
                    recipients += service.departement.ggd.get_ggdagents()
                    recipients.append(service.departement.ggd)
            # Ajout des agents n-1
            if avis.service_concerne == 'ggd' and self.get_instance().get_workflow_ggd() == Instance.WF_GGD_EDSR:
                if hasattr(service.departement, 'edsr') and avis.etat != 'demandé':
                    recipients += service.departement.edsr.get_edsr_agents()
                    recipients.append(service.departement.edsr)
            # Ajout des agents codis et sis
            if avis.service_concerne == 'sdis':
                if hasattr(service.departement, 'codis') and avis.etat == 'rendu':
                    recipients += service.departement.codis.get_codisagents()
                    recipients.append(service.departement.codis)
                for acces in avis.acces.all():
                    recipients += acces.service_object.cisagents.all()
                    recipients.append(acces.service_object)
            # Ajout des "non_agent_recipient" de l'avis
            recipients += [service]
        return recipients

    def get_instructeurs(self):
        """ Renvoyer les instructeurs de l'instruction """
        # TODO : A supprimer avec appli Notification
        prefecture = self.get_prefecture_concernee()
        if prefecture is not None:
            return prefecture.get_instructeurs()
        return []

    def get_nb_avis_rendus(self):
        """ Renvoyer le nombre d'AVIS rendus """
        return self.get_tous_avis().filter(etat='rendu').count()

    def get_nb_avis(self):
        """ Renvoyer le nombre d'avis """
        return self.get_tous_avis().count()

    def avis_tous_rendus(self):
        """ Renvoyer si tous les avis sont rendus """
        return not self.get_tous_avis().exclude(etat='rendu').exists()

    def get_nb_avis_non_rendus(self):
        """ Renvoie le nombre d'avis non encore rendus """
        return self.get_nb_avis() - self.get_nb_avis_rendus()

    def get_instance(self):
        """
        Renvoyer l'instance de la manifestation

        (la méthode est aussi utilisée de façon générique pour l'upload de fichiers)
        """
        return self.manif.get_instance()

    def notifier_creation(self):
        """ Notifier les instructeurs et la préfecture quand l'instruction est créée """
        destinataires = self.manif.liste_instructeurs()
        from messagerie.models.message import Message
        titre = "Nouveau dossier à instruire"
        data = {
            "manif": self.manif,
            "url": reverse("evenements:manif_url", kwargs={'pk': self.manif.pk}),
            "structure": self.manif.structure,
            "user": self.manif.structure.organisateur.user,
        }
        contenu = render_to_string('notifications/mail/message_organisateur_creation_instruction.txt', data)
        Message.objects.creer_et_envoyer('action', self.manif.structure.organisateur.user, destinataires, titre,
                                         contenu, manifestation_liee=self.manif, objet_lie_nature="dossier")

    def notifier_consultation(self, service=None):
        """ Notifier lorsque les premières demandes d'avis sont envoyées """
        destinataires = [self.manif.structure.organisateur]
        from messagerie.models.message import Message
        titre = "Démarrage de l'instruction"
        contenu = 'Ce message vous informe que le service instructeur a débuté le traitement de votre dossier pour la manifestation ' + self.manif.nom + \
            '.<br>Vous pouvez suivre l\'avancement dans le bandeau de résumé du dossier.'
        Message.objects.creer_et_envoyer('info_suivi', self.referent, destinataires, titre,
                                         contenu, manifestation_liee=self.manif, objet_lie_nature="dossier")

    def notifier_publication(self, service, prefecture):
        """ Notifier lorsque un document officiel est publié """
        destinataires = [self.manif.structure.organisateur] + [prefecture]
        destinataires += self.get_all_agents()
        natureDocument = self.documents.last().nature
        if natureDocument == 4:
            titre = "déclaration d'annulation publiée"
        elif natureDocument == 3:
            titre = "Récépissé de déclaration publié"
        elif natureDocument == 2:
            titre = "Arrêté de circulation publié"
        elif natureDocument == 1:
            titre = "Arrêté d'autorisation publié"
        else:  # cas 0
            titre = "Arrêté d'interdiction publié"

        from messagerie.models.message import Message
        contenu = titre + ' pour la manifestation ' + self.manif.nom
        if natureDocument == 3:
            Message.objects.creer_et_envoyer('info_suivi', self.referent, destinataires, titre,
                                             contenu, manifestation_liee=self.manif, objet_lie_nature="recepisse",
                                             objet_lie_pk=self.documents.last().pk)
        else:
            Message.objects.creer_et_envoyer('info_suivi', self.referent, destinataires, titre,
                                             contenu, manifestation_liee=self.manif, objet_lie_nature="arrete",
                                             objet_lie_pk=self.documents.last().pk)

    @transition(field=etat, source=['demandée', 'distribuée', 'interdite'], target='distribuée')
    def envoyerDemandeAvis(self):
        """ envoi de la première demande(s) d'avis """
        service = self.referent.get_service()
        if service is not None:
            if self.etat == 'demandée':
                self.notifier_consultation(service)
                self.date_consult = timezone.now()
                self.save()

    @transition(field=etat, source='*', target='autorisée')
    def publier_autorisation(self):
        """ Changer l'état de l'instruction avant l'appel de la méthode générique"""
        self.publier_document()

    @transition(field=etat, source='*', target='interdite')
    def publier_interdiction(self):
        """ Changer l'état de l'instruction avant l'appel de la méthode générique"""
        self.publier_document()

    @transition(field=etat, source='*', target='annulée')
    def publier_annulation(self):
        """ Changer l'état de l'instruction avant l'appel de la méthode générique"""
        self.publier_document()

    def publier_document(self):
        """ Publier le document officiel """
        service = self.referent.get_service()
        prefecture = self.get_prefecture_concernee()
        if prefecture is not None:
            self.notifier_publication(service, prefecture)

    # Meta
    class Meta:
        verbose_name = "instruction de manifestation sportive"
        verbose_name_plural = "instructions de manifestations sportives"
        default_related_name = 'instruction'
        app_label = 'instructions'


class DocumentOfficiel(models.Model):
    """
    Documents officiels liés à l'instruction
    """
    NATURE_ICON = (
        (0, 'interdite text-danger iconex2'),
        (1, 'arrete text-sucess iconex2'),
        (2, 'arrete-circul text-warning iconex2'),
        (3, 'recepisse text-sucess iconex2'),
        (4, 'annulee text-secondary iconex2'),
    )

    NATURE_CHOICE = (
        (9, '--------'),
        (0, 'Arrêté d\'interdiction'),
        (1, 'Arrêté d\'autorisation'),
        (2, 'Arrêté de circulation'),
        (3, 'Récépissé de déclaration'),
        (4, 'Déclaration d\'annulation'),
    )

    fichier = models.FileField(upload_to=UploadPath('officiels'), blank=True, null=True, verbose_name="document officiel", max_length=512, validators=[file_type_validator])
    date_depot = models.DateTimeField("date de dépot", auto_now_add=True)
    utilisateur = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='utilisateur', null=True, on_delete=models.SET_NULL)
    instruction = models.ForeignKey("Instruction", verbose_name="instruction", on_delete=models.CASCADE)
    nature = models.SmallIntegerField("nature du document déposé", choices=NATURE_CHOICE)

    def __str__(self):
        return self.fichier.name.split('/')[-1]

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        # creation de la vignette du fichier
        creation_thumbail_doc_officiel.delay(self.pk)

    def get_nature_icon(self):
        return dict(self.NATURE_ICON)[self.nature]

    def get_nature_nom(self):
        return dict(self.NATURE_CHOICE)[self.nature]

    def get_instance(self):
        """
        Renvoyer l'instance du documents

        (la méthode est aussi utilisée de façon générique pour l'upload de fichiers)
        """
        return self.instruction.manif.get_instance()

    # Meta
    class Meta:
        verbose_name = "document officiel de l'instruction"
        verbose_name_plural = "documents officiels de l'instruction"
        default_related_name = 'documents'
        app_label = 'instructions'
