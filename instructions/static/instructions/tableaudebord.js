$(document).ready(function () {

    $('.btn_etat_1, .btn_etat_2, #btn_etat_all').mouseenter(function (e) {
        // Afficher le tooltip
        let id ="#div"+$(this).attr('id').substr(3);
        $(id).fadeIn()
    }).mouseleave(function (e) {
        // Ne plus afficher le tooltip
        let id ="#div"+$(this).attr('id').substr(3);
        $(id).fadeOut()
    }).click(function (e) {
        e.preventDefault();
        let url = window.location.pathname + window.location.search;
        // Reprendre l'url de base et insérer "list"
        let list_url = url.split('/');
        list_url.splice(3,0,"list");
        url = list_url.join('/');

        let btn_etat = $(this).attr('data');
        // Ajouter le filtre du bloc
        if (typeof btn_etat!=="undefined"){
            if (url.indexOf('?')=== -1) {
                // Pas de query, on crée
                url += "?filtre_etat="+btn_etat;
            } else {
                // Une query existe, on ajoute
                url += "&filtre_etat="+btn_etat;
            }
        }
        else {
            $(this).after(' <i class="spinner attente_chargement"></i>')
            AfficherIcones()
        }
        $.get(url, function (data) {
            // Recupérer la liste à afficher et insérer
            $('#block_tableaubord_liste').html(data);
            $('.attente_chargement').remove()
            // Lier la ligne vers la vue de détail
            $('tr.clickligne').click(function () {
                window.location=$(this).data("href");
            });
            AfficherIcones();
        })


    });

    $('.arron').on("click", function () {
        let arron = $(this).attr('id').split('_')[1];
        let parametres = window.location.search;
        if (parametres.search('arron') !== -1) {
            const regex = /arron=\d+/gi;
            parametres = parametres.replace(regex, "arron=" + arron);
        } else {
            if (parametres.indexOf('?') !== -1) {
                parametres += "&arron=" + arron;
            } else {
                parametres += "?arron=" + arron;
            }
        }
        document.location.href = window.location.origin + window.location.pathname + parametres;
    });

    $('.dept').on('click', function (e) {
        let dept = $(this).attr('id').split('_')[1];
        let parametres = window.location.search;
        if (parametres.search('dept') !== -1) {
            const regex = /dept=\d+/gi;
            parametres = parametres.replace(regex, "dept=" + dept);
        } else {
            if (parametres.indexOf('?') !== -1) {
                parametres += "&dept=" + dept;
            } else {
                parametres += "?dept=" + dept;
            }
        }
        document.location.href = window.location.origin + window.location.pathname + parametres;
    })

});