# coding: utf-8
from django.shortcuts import get_object_or_404, render
from functools import wraps

from core.util.permissions import require_role
from instructions.models.instruction import Instruction
from instructions.models.avis import Avis
from instructions.models.preavis import PreAvis


def verifier_secteur_instruction(function=None, arr_wrt=False):
    """
    Décorateur : limite l'accès des instructions en fonction du secteur
    :param function: La fonction décorée
    :param arr_wrt: déclenche le contrôle en instruction de la délégation d'arrondissement
    :return:
    """
    def decorator(view_func):
        @require_role(['instructeur', 'mairieagent'])
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            # Dans le cas d'une demande de renvoi ou de suppression d'avis, le pk fourni est celui de l'avis
            if (request.path.split('/')[-2] == 'resend' or request.path.split('/')[-2] == 'del') and request.path.split('/')[-4] == 'avis':
                instruction = get_object_or_404(Instruction, avis__pk=kwargs['pk'])
            # Dans le cas d'une demande d'export pdf de la manif, le pk fourni est celui de la manif
            elif request.path.split('/')[-2] == 'instructeur' and request.path.split('/')[-3] == 'export':
                instruction = get_object_or_404(Instruction, manif__pk=kwargs['pk'])
            # Dans le cas d'une modification de l'affichage dans le calendrier, le pk fourni est celui de la manif
            elif request.path.split('/')[-2] == 'instruction' and request.path.split('/')[-4] == 'evenement':
                instruction = get_object_or_404(Instruction, manif__pk=kwargs['pk'])
            # Dans le cas d'une demande de complémentd'information, le pk fourni est celui de la manif
            elif request.path.split('/')[-3] == 'doc_complementaire' and request.path.split('/')[-5] == 'evenement':
                instruction = get_object_or_404(Instruction, manif__pk=kwargs['manif_pk'])
            # Cas général ou le pk est celui de l'instruction
            else:
                instruction = get_object_or_404(Instruction, pk=kwargs['pk'])
            if request.user.has_role('instructeur'):
                # L'instructeur ne peut accéder qu'aux instructions de son département ou de son arrondisement
                from core.models import Instance
                config_instructeur = request.user.get_instance().get_instruction_mode()
                user_place = request.user.get_instance().get_departement()
                object_place = instruction.get_instance().get_departement()
                if config_instructeur == Instance.IM_ARRONDISSEMENT:
                    if arr_wrt:
                        if not request.user.get_instance().acces_arrondissement == 2:
                            user_place = request.user.instructeur.get_prefecture().arrondissement
                            object_place = instruction.manif.ville_depart.arrondissement
                    else:
                        if not request.user.get_instance().acces_arrondissement:
                            user_place = request.user.instructeur.get_prefecture().arrondissement
                            object_place = instruction.manif.ville_depart.arrondissement
                if user_place == object_place:
                    return view_func(request, *args, **kwargs)
            elif request.user.has_role('mairieagent'):
                # L'agent de mairie ne peut accéder qu'aux instructions sur une commune et non motorisées
                if not instruction.manif.villes_traversees.all() and not hasattr(instruction.manif.get_cerfa(), 'vehicules'):
                    # L'agent de mairie ne peut accéder qu'aux instructions de sa commune
                    user_commune = request.user.get_service()
                    object_commune = instruction.manif.ville_depart
                    if user_commune == object_commune:
                        return view_func(request, *args, **kwargs)
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_service_avis(function=None):
    """ Décorateur : limite l'accès des avis en fonction du service """

    def decorator(view_func):
        @require_role('agent')
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            # Dans le cas d'une demande de renvoi et de suppression de préavis, le pk fourni est celui du préavis
            if request.path.split('/')[-2] in ['resend', 'del'] and request.path.split('/')[-4] == 'preavis':
                avis = get_object_or_404(Avis, preavis__pk=kwargs['pk'])
            elif request.path.split('/')[-2] == 'addfile' and request.GET and request.GET['doc']:
                avis = get_object_or_404(Avis, preavis__pk=kwargs['pk'])
            # Cas général ou le pk est celui de l'instruction
            else:
                avis = get_object_or_404(Avis, pk=kwargs['pk'])
            # L'agent ne peut accéder qu'aux avis concernant son service sauf ...
            instruction = avis.instruction
            if instruction.get_avis_user(request.user).id == avis.id:
                return view_func(request, *args, **kwargs)
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator


def verifier_service_preavis(function=None):
    """ Décorateur : limite l'accès des avis en fonction du service """

    def decorator(view_func):
        @require_role('agentlocal')
        @wraps(view_func)
        def _wrapped_view(request, *args, **kwargs):
            preavis = get_object_or_404(PreAvis, pk=kwargs['pk'])
            # L'agent ne peut accéder qu'aux preavis concernant son service
            user_service = request.user.get_service()
            liste_service = [preavis.destination_object]
            if user_service in liste_service:
                return view_func(request, *args, **kwargs)
            return render(request, "core/access_restricted.html",
                          {'message': "Vous n'avez pas les droits d'accès à cette page"}, status=403)

        return _wrapped_view

    if function:
        return decorator(function)
    return decorator
