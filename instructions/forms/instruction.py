# coding: utf-8
from django import forms
from django.urls import reverse_lazy
from django.forms.models import ModelMultipleChoiceField

from crispy_forms.helper import FormHelper
from crispy_forms.bootstrap import FormActions
from crispy_forms.layout import Submit, Layout, Fieldset, HTML

from administration.models.service import Service
from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelMultipleChoiceField
from clever_selects.forms import ChainedChoicesModelForm
from core.forms.base import GenericForm
from instructions.models.instruction import Instruction, DocumentOfficiel


class InstructionForm(GenericForm):
    """ Formulaire de demande d'autorisation """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML('<div class="alert alert-success" role="alert">' +
                                         '<p><strong><em>Je valide mon dossier et je souhaite le transmettre au service instructeur.</em></strong></p>' +
                                         '<p>Un accusé de réception va vous être envoyé par courriel. Si vous ne le recevez pas dans la prochaine heure, veuillez consulter votre tableau de bord puis éventuellement prendre contact avec votre service instructeur.<br>' +
                                         'Une fois votre dossier transmis vous serez redirigé sur une page qui affichera l\'état d\'avancement de votre dossier. L\'étape "Demande envoyée" doit apparaître en vert.' +
                                         '</p></div>' +
                                         '<div><p>Une demande d\'avis va être automatiquement envoyée à votre fédération délégataire :<br></p></div>'),
                                    FormActions(Submit('save', "Ok, envoyer maintenant...")))

    # Méta
    class Meta:
        model = Instruction
        fields = ()


class InstructionDispatchForm(GenericForm, ChainedChoicesModelForm):
    """ Formulaire de distribution des demandes d'avis """

    # Champs
    departements_concernes = ModelMultipleChoiceField(required=False, queryset=Departement.objects.configured(), label="Départements concernés")
    villes_concernees = ChainedModelMultipleChoiceField('departements_concernes', reverse_lazy('administrative_division:commune_widget'),
                                                       Commune, label="Mairies concernées", required=False)
    services_concernes = ChainedModelMultipleChoiceField('departements_concernes', reverse_lazy('administration:service_widget'),
                                                       Service, label="Services concernés", required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.label_class = 'col-sm-12'
        self.helper.field_class = 'col-sm-12'
        self.helper.form_id = 'envoyeravis'
        self.helper.layout = Layout(
            Fieldset("Choix des avis",
                     'ddsp_concerne', 'edsr_concerne', 'ggd_concerne', 'sdis_concerne', 'cg_concerne', 'departements_concernes', 'villes_concernees',
                     'services_concernes'),
            FormActions(Submit('save', "Envoyer"))
        )
        self.fields['departements_concernes'].widget.attrs = {'data-placeholder': "Sélectionnez un ou plusieurs départements", 'class': 'selectchosen'}
        self.fields['villes_concernees'].widget.attrs = {'data-placeholder': "Sélectionnez une ou plusieurs communes", 'class': 'selectchosen'}
        self.fields['services_concernes'].widget.attrs = {'data-placeholder': "Sélectionnez un ou plusieurs services", 'class': 'selectchosen'}

    def clean_villes_concernees(self):
        data = self.cleaned_data['villes_concernees']
        return data

    # Méta
    class Meta:
        model = Instruction
        exclude = ['manif', 'date_creation', 'date_consult', 'etat', 'documents', 'referent', 'doc_verif']


class InstructionPublishForm(GenericForm):
    """ Formulaire de publication d'arrêté """

    fichier = forms.FileField()
    nature = forms.ChoiceField(label="Nature du document déposé", choices=DocumentOfficiel.NATURE_CHOICE)

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(
            Fieldset("Choisissez le fichier du document officiel", 'nature', 'fichier'),
            FormActions(Submit('save', "Publier le document officiel"))
        )

    # Validation des champs
    def clean_fichier(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['fichier']
        if not data:
            raise forms.ValidationError("Vous devez spécifier un fichier contenant le document officiel")
        return data

    def clean_nature(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['nature']
        if not data or data == '9':
            raise forms.ValidationError("Vous devez spécifier la nature du document officiel")
        return data

    # Meta
    class Meta:
        model = Instruction
        fields = ['fichier', 'nature']


class InstructionPublierForm(forms.ModelForm):
    """ Formulaire de publication d'arrêté """

    nature = forms.ChoiceField(label="nature du document", choices=DocumentOfficiel.NATURE_CHOICE)

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-8'
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset("", 'nature'),
        )

    # Validation des champs
    def clean_nature(self):
        """ Vérifier le champ de fichier d'arrêté """
        data = self.cleaned_data['nature']
        if not data or data == '9':
            raise forms.ValidationError("Vous devez spécifier la nature du document officiel")
        return data

    # Meta
    class Meta:
        model = Instruction
        fields = ['nature', ]
