from django.apps import AppConfig


class InstructionsConfig(AppConfig):
    name = 'instructions'
    verbose_name = "Instructions"


default_app_config = 'instructions.InstructionsConfig'
