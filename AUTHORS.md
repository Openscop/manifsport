Manifestation Sportive is created by Openscop <contact@openscop.fr> with contributions from the following authors:

## The core team

- Christian Lopez
- David Réchatin
- Simon Panay
- Steve Kossouho

## Other contributors

- Aurélien Touzet
- Michael Mazurczak
- Thomas Bronner
