# coding: utf-8
from django import forms
from django.urls import reverse
from django.shortcuts import get_object_or_404, render
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView

from administrative_division.models.departement import Departement
from core.models.instance import Instance
from core.util.permissions import require_role
from core.util.user import UserHelper
from declarations.models import ManifestationDeclaration
from events.models import Manifestation
from .forms import AuthorizationDispatchForm
from .forms import AuthorizationForm
from .forms import AuthorizationPublishBylawForm
from .models import ManifestationAutorisation


class Dashboard(ListView):
    """ Tableau de bord instructeur """

    # Configuration
    model = ManifestationAutorisation
    template_name = 'authorizations/dashboard_instructeur.html'

    # Overrides
    @method_decorator(require_role('instructeur'))
    def dispatch(self, *args, **kwargs):
        return super(Dashboard, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        instructeur = self.request.user.instructeur
        return ManifestationAutorisation.objects.to_process().display_pref(instructeur).closest_first()

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        instructeur = self.request.user.instructeur
        context['declarations'] = ManifestationDeclaration.objects.to_process().display_pref(instructeur).closest_first()
        return context


class Archives(ListView):
    """ Tableau de bord instructeur pour les manifestations passées"""

    # Configuration
    model = ManifestationAutorisation
    template_name = 'authorizations/archives_instructeur.html'

    # Overrides
    @method_decorator(require_role('instructeur'))
    def dispatch(self, *args, **kwargs):
        return super(Archives, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        instructeur = self.request.user.instructeur
        return ManifestationAutorisation.objects.finished().display_pref(instructeur).last_first()

    def get_context_data(self, **kwargs):
        context = super(Archives, self).get_context_data(**kwargs)
        instructeur = self.request.user.instructeur
        context['declarations'] = ManifestationDeclaration.objects.finished().display_pref(instructeur).last_first()
        return context


class AuthorizationCreateView(CreateView):

    # Configuration
    model = ManifestationAutorisation
    form_class = AuthorizationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super(AuthorizationCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        return super(AuthorizationCreateView, self).form_valid(form)

    def get_success_url(self):
        """ Renvoyer l'URL de retour lorsque le formulaire est valide """

        type_name = self.object.manifestation.get_manifestation_type_name()

        if type_name == 'autorisationnm':
            return reverse('events:autorisationnm_detail', kwargs={'pk': self.object.manifestation.pk})
        elif type_name == 'motorizedconcentration':
            return reverse('events:motorizedconcentration_detail', kwargs={'pk': self.object.manifestation.pk})
        elif type_name == 'motorizedevent':
            return reverse('events:motorizedevent_detail', kwargs={'pk': self.object.manifestation.pk})
        elif type_name == 'motorizedrace':
            return reverse('events:motorizedrace_detail', kwargs={'pk': self.object.manifestation.pk})
        else:
            return reverse('portail:home_page')


class AuthorizationDetailView(DetailView):

    # Configuration
    model = ManifestationAutorisation

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        """ L'instructeur ne peut accéder qu'aux autorisations de son département """
        user_departement = self.request.user.get_instance().get_departement()
        object_departement = self.get_object().get_instance().get_departement()
        if user_departement == object_departement:
            return super().dispatch(*args, **kwargs)
        return render(self.request, 'core/access_restricted.html', status=403)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class AuthorizationUpdateView(UpdateView):
    # Configuration

    model = ManifestationAutorisation
    form_class = AuthorizationDispatchForm

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        user_departement = self.request.user.get_instance().get_departement()
        object_departement = self.get_object().get_instance().get_departement()
        if user_departement == object_departement:
            return super().dispatch(*args, **kwargs)
        return render(self.request, 'core/access_restricted.html', status=403)

    def form_valid(self, form):
        form.instance.dispatch()
        return super(AuthorizationUpdateView, self).form_valid(form)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # Modifier l'état des champs selon la configuration actuelle
        if self.object.get_instance().get_workflow_ggd() in [Instance.WF_GGD_EDSR, Instance.WF_GGD_SUBEDSR]:
            form.fields['ggd_concerned'].initial = True
            form.fields['edsr_concerned'].initial = False
            form.fields['edsr_concerned'].widget = forms.HiddenInput()
        elif self.object.get_instance().get_workflow_ggd() == Instance.WF_EDSR:
            form.fields['edsr_concerned'].initial = True
            form.fields['ggd_concerned'].initial = False
            form.fields['ggd_concerned'].widget = forms.HiddenInput()
        return form

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['initial'].update({'concerned_departments': Departement.objects.by_cities(self.object, as_initial=True)})
        return kwargs

    def get_success_url(self):
        """ Retourne l'URL de destination lorsque le formulaire est validé """
        pk = self.object.id
        return reverse('authorizations:authorization_detail', kwargs={'pk': pk})


class AuthorizationPublishBylawView(UpdateView):
    # Configuration
    model = ManifestationAutorisation
    form_class = AuthorizationPublishBylawForm
    template_name_suffix = '_publish_form'

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        user_departement = self.request.user.get_instance().get_departement()
        object_departement = self.get_object().get_instance().get_departement()
        if user_departement == object_departement:
            return super().dispatch(*args, **kwargs)
        return render(self.request, 'core/access_restricted.html', status=403)

    def form_valid(self, form):
        form.instance.publish()
        return super(AuthorizationPublishBylawView, self).form_valid(form)

    def get_success_url(self):
        """ Retourne l'URL de destination lorsque le formulaire est validé """
        if UserHelper.has_role(self.request.user, 'instructeur'):
            return reverse('authorizations:dashboard')
        else:
            return reverse('declarations:dashboard')
