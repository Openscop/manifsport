# coding: utf-8
from datetime import date

from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.db import models
from django.utils import timezone
from django_fsm import FSMField, transition

from administration.models import Instructeur
from authorizations.signals import authorization_published
from configuration.directory import UploadPath
from events.models.manifestation import ManifestationRelatedModel
from notifications.models.action import Action
from notifications.models.notification import Notification


class ManifestationAutorisationQuerySet(models.QuerySet):
    """ Queryset des autorisations d'événements sportifs """

    def by_instructeur(self, instructeur):
        """ Renvoyer les autorisations en instruction pour l'instructeur """
        from core.models import Instance
        config_instructeur = instructeur.user.get_instance().get_instruction_mode()
        if config_instructeur == Instance.IM_ARRONDISSEMENT:
            return self.filter(manifestation__departure_city__arrondissement=instructeur.get_prefecture().arrondissement)
        else:  # Département
            return self.filter(manifestation__departure_city__arrondissement__departement=instructeur.get_prefecture().arrondissement.departement)

    def display_pref(self, instructeur):
        """ Renvoyer les autorisations qui traversent une autre commune et qui sont motorisées """
        result1 = self.by_instructeur(instructeur).filter(manifestation__crossed_cities__isnull=False)
        result2 = self.by_instructeur(instructeur).filter(manifestation__crossed_cities__isnull=True).filter(manifestation__autorisationnm__isnull=True)
        return (result1 | result2).distinct()

    def display_mairie(self, instructeur):
        """ Renvoyer les autorisations qui ne traversent pas une autre commune et qui ne sont pas motorisées """
        if type(instructeur) == Instructeur:
            result = self.by_instructeur(instructeur)
        else:
            commune = instructeur.agent.mairieagent.commune
            result = self.filter(manifestation__departure_city=commune)
        return result.filter(manifestation__crossed_cities__isnull=True).filter(manifestation__autorisationnm__isnull=False).distinct()

    def to_process(self):
        """ Renvoyer les autorisations pour lesquelles l'événement prévu n'est pas terminé """
        return self.filter(manifestation__end_date__gte=timezone.now())

    def finished(self):
        """ Renvoyer les autorisations pour lesquelles l'événement prévu est terminé """
        return self.filter(manifestation__end_date__lt=timezone.now())

    def closest_first(self):
        """ Renvoyer les autorisations, triées par date de l'événement croissante """
        return self.order_by('manifestation__begin_date')

    def last_first(self):
        """ Renvoyer les autorisations, triées par date de l'événement décroissante """
        return self.order_by('-manifestation__begin_date')

    def by_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(manifestation__instance=request.user.get_instance())
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(manifestation__instance__in=instances)


class ManifestationAutorisation(ManifestationRelatedModel):
    """
    Dossier de demande d'Autorisation pour manifestation sportive

    Si l'événement est une compétition (avec classement), L'événement doit faire
    l'objet d'une demande d'autorisation complémentaire auprès des instances
    compétentes de la fédération sportive concernée. Les procédures et les délais
    à respecter sont indiqués par la fédération sportive concernée.

    Si l'événement n'est pas une compétition (sans classement)
    en cas de circuit ou de parcours (course à pied, roller skating, cyclotourisme, etc.),
    une demande d'autorisation à l'aide du formulaire Cerfa n°13391*03,au
    moins 3 mois avant la manifestation
    """

    # Champs
    state = FSMField(default='created', verbose_name="Statut de l'autorisation")
    creation_date = models.DateField("date de création", default=date.today, editable=False)
    dispatch_date = models.DateField("date d'envoi des demandes d'avis", blank=True, null=True)
    manifestation = models.OneToOneField('events.manifestation', related_name="manifestationautorisation", verbose_name="manifestation associée", on_delete=models.CASCADE)

    concerned_services = models.ManyToManyField('administration.service', verbose_name="services concernés", blank=True)
    concerned_cities = models.ManyToManyField('administrative_division.commune', verbose_name="villes concernées", blank=True)
    ddsp_concerned = models.BooleanField("avis DDSP requis", default=False)
    edsr_concerned = models.BooleanField("avis EDSR requis", default=False)
    ggd_concerned = models.BooleanField("avis GGD requis", default=False)
    sdis_concerned = models.BooleanField("avis SDIS requis", default=False)
    cg_concerned = models.BooleanField("avis Conseil Départemental requis", default=False)
    bylaw = models.FileField(upload_to=UploadPath('arretes'), blank=True, null=True, verbose_name="arrêté d'autorisation", max_length=512)

    objects = ManifestationAutorisationQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return self.manifestation.name

    @staticmethod
    def get_absolute_url():
        """ Renvoyer l'URL des autorisations """
        return reverse('authorizations:dashboard')

    # Getter
    def get_avis(self):
        """ Renvoyer les avis pour l'autorisation """
        return self.avis.all()

    def get_concerned_prefecture(self):
        """ Renvoyer la préfecture de la manif """
        try:
            return self.manifestation.departure_city.get_prefecture()
        except (AttributeError, ObjectDoesNotExist):
            return None

    def get_instructeurs(self):
        """ Renvoyer les instructeurs de l'autorisation """
        prefecture = self.get_concerned_prefecture()
        if prefecture is not None:
            return prefecture.get_instructeurs()
        return []

    def get_validated_avis_count(self):
        """ Renvoyer le nombre d'AVIS rendus """
        return self.get_avis().filter(state='acknowledged').count()

    def get_avis_count(self):
        """ Renvoyer le nombre d'avis """
        return self.get_avis().count()

    def are_avis_validated(self):
        """ Renvoyer si tous les avis sont rendus """
        return not self.get_avis().exclude(state='acknowledged').exists()

    def get_pending_avis_count(self):
        """ Renvoie le nombre d'avis non encore rendus """
        return self.get_avis_count() - self.get_validated_avis_count()

    # Actions
    def log_creation(self):
        """ Notifier de la création """
        recipients = self.manifestation.structure.organisateur
        Action.objects.log(recipients, "demande d'autorisation envoyée", self.manifestation)

    def log_dispatch(self, prefecture):
        """ Notifier du dispatch """
        Action.objects.log(prefecture.get_instructeurs(), "demandes d'avis envoyées", self.manifestation)

    def log_publish(self, prefecture):
        """ Notifier la publication de l'arrêté préfectoral """
        Action.objects.log(prefecture.get_instructeurs(), "arrêté d'autorisation publié", self.manifestation)

    def notify_creation(self):
        """ Notifier et envoyer un mail aux instructeurs et à la préfecture quand l'autorisation est créée """
        prefecture = self.get_concerned_prefecture()
        if prefecture is not None:
            if not self.manifestation.crossed_cities.all() and hasattr(self.manifestation,'autorisationnm'):
                # Instruction par la mairie
                commune = self.manifestation.departure_city
                recipients = list(commune.get_mairieagents()) + [prefecture]
            else:
                # Instruction par la préfecture
                recipients = list(prefecture.get_instructeurs()) + [prefecture]
            Notification.objects.notify_and_mail(recipients, "demande d'autorisation envoyée", self.manifestation.structure, self.manifestation)

    def notify_dispatch(self, prefecture):
        """ Notifier et envoyer un mail lorsque les demandes d'avis sont envoyées """
        recipients = [self.manifestation.structure.organisateur]
        Notification.objects.notify_and_mail(recipients, "début de l'instruction", prefecture, self.manifestation)

    def notify_publish(self, prefecture):
        """ Notifier et envoyer un mail lorsque l'arrêté est publié """
        recipients = [self.manifestation.structure.organisateur]
        Notification.objects.notify_and_mail(recipients, "arrêté d'autorisation publié", prefecture, self.manifestation)

    @transition(field=state, source=['created', 'dispatched'], target='dispatched')
    def dispatch(self):
        """ Dispenser les demandes d'avis """
        prefecture = self.get_concerned_prefecture()
        if prefecture is not None:
            if self.state == 'created':
                self.notify_dispatch(prefecture)
                self.log_dispatch(prefecture)
                self.dispatch_date = timezone.now()
                self.save()
            elif self.state == 'dispatched':
                self.concerned_cities.clear()
                self.concerned_services.clear()
                self.save()

    @transition(field=state, source='dispatched', target='published')
    def publish(self):
        """ Publier l'arrêté """
        prefecture = self.get_concerned_prefecture()
        if prefecture is not None:
            self.notify_publish(prefecture)
            self.log_publish(prefecture)
            authorization_published.send(sender=self.__class__, instance=self)

    # Meta
    class Meta:
        verbose_name = "autorisation de manifestation sportive"
        verbose_name_plural = "autorisations de manifestations sportives"
        default_related_name = 'manifestationautorisations'
        app_label = 'authorizations'
