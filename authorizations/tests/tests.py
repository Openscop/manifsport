# coding: utf-8
import html

from django.test import TestCase

from administration.factories import InstructeurFactory
from administration.factories.service import CGServiceFactory, CompagnieFactory, CGDFactory
from notifications.models import Action
from notifications.models import Notification
from authorizations.factories import ManifestationAuthorizationFactory
from authorizations.models import ManifestationAutorisation


class ManifestationAutorisationTests(TestCase):
    """ Tests des autorisations """

    # Configuration
    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        self.authorization = ManifestationAuthorizationFactory.create()
        self.manifestation = self.authorization.get_manifestation()
        self.commune = self.manifestation.departure_city
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.commune.get_prefecture()
        self.instructeur = InstructeurFactory.create(prefecture=self.prefecture)
        self.cg = self.departement.cg
        self.edsr = self.departement.edsr
        self.ddsp = self.departement.ddsp
        self.sdis = self.departement.sdis
        self.ggd = self.departement.ggd
        self.activite = self.manifestation.activite
        self.federation = self.manifestation.get_federation()
        self.compagnie = CompagnieFactory.create(sdis=self.sdis)
        self.cgservice = CGServiceFactory.create(cg=self.cg)
        self.cgd = CGDFactory.create(commune=self.commune)

    # Tests
    def test_by_instructeur(self):
        """ Vérifier que l'instructeur est bien en charge de la délivrance de l'autorisation """
        self.assertEqual(ManifestationAutorisation.objects.by_instructeur(self.instructeur)[0].pk, self.authorization.pk)

    def test_str(self):
        """ Tester la représentation texte de l'autorisation """
        self.assertEqual(str(self.authorization), str(self.manifestation))

    def test_prefecture(self):
        """ Tester que la préfecture de l'autorisation est correcte """
        self.assertEqual(self.authorization.get_concerned_prefecture(), self.prefecture)

    def test_avis(self):
        """ Tester la validation des avis """
        self.assertEqual(self.authorization.get_avis_count(), 1)
        self.assertFalse(self.authorization.are_avis_validated())  # L'avis fédération (tjs créé par défaut) n'est pas validé
        avis = self.authorization.get_avis().first()  # on sait que c'est un avis federation
        avis.state = 'acknowledged'
        avis.save()
        self.assertEqual(avis.state, 'acknowledged')
        self.assertEqual(self.authorization.get_avis_count(), 1)
        self.assertTrue(self.authorization.are_avis_validated())  # L'avis fédération est désormais validé

    def test_action_log(self):
        """ Tester les logs d'actions """
        action = Action.objects.last()  # action loggée lorsque la manifestation est créée (voir authorizations.listeners)
        action_count = Action.objects.count()
        self.assertEqual(action.user, self.manifestation.structure.organisateur.user)
        self.assertEqual(action.manifestation, self.manifestation.manifestation_ptr)
        self.assertTrue("demande d'autorisation envoyée" in html.unescape(action.action))  # s'assurer que l'action contient cette chaîne

        self.authorization.save()  # on va vérifier qu'aucune nouvelle action n'est loggée
        self.assertEqual(action_count, Action.objects.count())

    def test_notification_log(self):
        """ Tester les notifications """
        notification = Notification.objects.first()  # autorisation + avis fédération
        # pas de notification instructeur, ni de notification fédération : les agents sont créés après l'autorisation et l'autorisation
        self.assertIsNone(notification)

    def test_notify_authorization_creation_with_no_prefecture(self):
        authorization = ManifestationAuthorizationFactory.create()
        self.assertEqual(Notification.objects.filter(manifestation=authorization.manifestation).count(), 0)
