/**
 * Created by knt on 22/01/17.
 */
$(document).ready(function () {

    $('#id_departements_traverses').trigger("change");  // Provoquer la mise à jour du champ Communes traversées
    $('#id_departements_traverses').trigger("chosen:updated");  // Obligatoire avec jQuery.chosen
    $("#manifestation-form").liveValidate('/validate/umanif/', {event: 'update blur'});

    $("textarea").autogrow({flickering: false, horizontal: false});

});
