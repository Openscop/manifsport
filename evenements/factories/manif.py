# coding: utf-8
import factory
from django.utils.timezone import timedelta, now
from factory.fuzzy import FuzzyDateTime

from administrative_division.factories import CommuneFactory
from organisateurs.factories import StructureFactory
from evenements.models import *
from sports.factories import ActiviteFactory


class DnmFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation non motorisée """

    # Champs
    nom = factory.Sequence(lambda n: 'Dnm{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    # date de début aléatoire entre aujourd'hui plus 100jours et dans 2 ans plus 100jours
    date_debut = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    # TODO: Fuzzy sera supprimé dans la release 3.x de factory-boy, A remplacer par l'équivalent Faker ( voir https://github.com/FactoryBoy/factory_boy/issues/271/)
    # dat de fin même jours 8 heures plus tard
    date_fin = factory.LazyAttribute(lambda o: o.date_debut + timedelta(hours=8))
    ville_depart = factory.SubFactory(CommuneFactory)
    nb_participants = 666
    activite = factory.SubFactory(ActiviteFactory)
    nb_vehicules_accompagnement = 30
    nb_spectateurs = 5000
    nb_organisateurs = 40

    # Meta
    class Meta:
        model = Dnm

    @factory.post_generation
    def villes_traversees(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for city in extracted:
                self.villes_traversees.add(city)


class DcnmFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation non motorisée """

    # Champs
    nom = factory.Sequence(lambda n: 'Dcnm{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    date_debut = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    date_fin = factory.LazyAttribute(lambda o: o.date_debut + timedelta(hours=8))
    ville_depart = factory.SubFactory(CommuneFactory)
    nb_participants = 666
    activite = factory.SubFactory(ActiviteFactory)
    nb_vehicules_accompagnement = 30
    nb_spectateurs = 5000
    nb_organisateurs = 40

    # Meta
    class Meta:
        model = Dcnm

    @factory.post_generation
    def villes_traversees(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for city in extracted:
                self.villes_traversees.add(city)


class DvtmFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation motorisée """

    # Champs
    nom = factory.Sequence(lambda n: 'Dvtm{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    date_debut = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    date_fin = factory.LazyAttribute(lambda o: o.date_debut + timedelta(hours=8))
    ville_depart = factory.SubFactory(CommuneFactory)
    nb_participants = 666
    activite = factory.SubFactory(ActiviteFactory)
    vehicules = 30
    nb_spectateurs = 5000
    nb_organisateurs = 40

    # Meta
    class Meta:
        model = Dvtm


class AvtmFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation motorisée """

    # Champs
    nom = factory.Sequence(lambda n: 'Avtm{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    date_debut = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    date_fin = factory.LazyAttribute(lambda o: o.date_debut + timedelta(hours=8))
    ville_depart = factory.SubFactory(CommuneFactory)
    nb_participants = 666
    activite = factory.SubFactory(ActiviteFactory)
    vehicules = 30
    nb_spectateurs = 5000
    nb_organisateurs = 40

    # Meta
    class Meta:
        model = Avtm


class AvtmcirFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation motorisée """

    # Champs
    nom = factory.Sequence(lambda n: 'Avtmcir{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    date_debut = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    date_fin = factory.LazyAttribute(lambda o: o.date_debut + timedelta(hours=8))
    ville_depart = factory.SubFactory(CommuneFactory)
    nb_participants = 666
    vehicules = 30
    activite = factory.SubFactory(ActiviteFactory)
    nb_spectateurs = 5000
    nb_organisateurs = 40

    # Meta
    class Meta:
        model = Avtmcir
