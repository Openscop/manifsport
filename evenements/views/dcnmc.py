# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.dcnm import DcnmcForm, DcnmcFilesForm
from ..models.dnm import Dcnmc


class DcnmcDetail(ManifDetail):
    """ Détail de manifestation """

    # Configuration
    model = Dcnmc


class DcnmcCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Dcnmc
    form_class = DcnmcForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnmc'
        return context


class DcnmcUpdate(ManifUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = Dcnmc
    form_class = DcnmcForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnmc'
        return context


class DcnmcFilesUpdate(ManifFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = Dcnmc
    form_class = DcnmcFilesForm


class DcnmcDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dcnmc