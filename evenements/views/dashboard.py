# coding: utf-8
from django.utils.decorators import method_decorator
from django.views.generic import View
from django.shortcuts import render

from core.util.permissions import require_role
from evenements.models import Manif
from organisateurs.models.structure import Structure


class TableauOrganisateurBody(View):
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour un accès par les organisateurs uniquement """
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        try:
            user_structure = self.request.user.organisateur.structure
        except Structure.DoesNotExist:
            user_structure = None
        manifs = Manif.objects.filter(structure=user_structure).order_by('-pk')
        non_envoye = 0
        en_cours = 0
        autorise = 0
        interdite = 0
        annule = 0
        for manif in manifs:
            if hasattr(manif.get_cerfa(), 'instruction'):
                if manif.get_cerfa().instruction.etat == "interdite":
                    interdite+=1
                elif manif.get_cerfa().instruction.etat == "autorisée":
                    autorise += 1
                elif manif.get_cerfa().instruction.etat == "annulée":
                    annule += 1
                else:
                    en_cours+=1
            else:
                non_envoye+=1

        context = {
            "non_envoye": non_envoye,
            "en_cours": en_cours,
            "autorise": autorise,
            "interdite": interdite,
            "annule": annule,
        }
        return render(request, 'evenements/dashboard_organisateur_body.html', context)


class TableauOrganisateurListe(View):

    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ Protéger la vue pour un accès par les organisateurs uniquement """
        return super().dispatch(*args, **kwargs)

    def get(self, request):
        try:
            user_structure = self.request.user.organisateur.structure
        except Structure.DoesNotExist:
            user_structure = None
        manifs = Manif.objects.filter(structure=user_structure).order_by('-pk')
        context = {
            "manifs": manifs,
        }
        if request.GET.get('filtre_etat', None):
            trimanif= []
            for manif in manifs:
                if request.GET['filtre_etat']=="attente":
                    if not hasattr(manif.get_cerfa(), 'instruction'):
                        trimanif.append(manif)
                elif request.GET['filtre_etat']=="demande":
                    if hasattr(manif.get_cerfa(), 'instruction') and manif.get_cerfa().instruction.etat in ["demandée","distribuée"]:
                            trimanif.append(manif)
                elif request.GET['filtre_etat']=="autorise":
                    if hasattr(manif.get_cerfa(), 'instruction') and manif.get_cerfa().instruction.etat == "autorisée":
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "interdite":
                    if hasattr(manif.get_cerfa(), 'instruction') and manif.get_cerfa().instruction.etat == "interdite":
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "annule":
                    if hasattr(manif.get_cerfa(), 'instruction') and manif.get_cerfa().instruction.etat == "annulée":
                        trimanif.append(manif)
                elif request.GET['filtre_etat'] == "all":
                    trimanif.append(manif)
            context = {
                'manifs': trimanif
            }

        return render(request, "evenements/dashboard_organisateur_liste.html", context)
