# coding: utf-8
from sports.models.federation import Federation
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.dcnm import DcnmForm, DcnmFilesForm
from ..models.dnm import Dcnm


class DcnmDetail(ManifDetail):
    """ Détail de manifestation """

    # Configuration
    model = Dcnm


class DcnmCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Dcnm
    form_class = DcnmForm

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        # Remplacer le champ federation_multisport si le département est passé dans l"URL
        if request_departement:
            form.fields['federation_multisport'].queryset = Federation.objects.filter(multisports=True, departement__name=request_departement)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnm'
        return context


class DcnmUpdate(ManifUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = Dcnm
    form_class = DcnmForm

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        # Remplacer le champ federation_multisport si le département est passé dans l"URL
        if request_departement:
            form.fields['federation_multisport'].queryset = Federation.objects.filter(multisports=True, departement__name=request_departement)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dcnm'
        return context


class DcnmFilesUpdate(ManifFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = Dcnm
    form_class = DcnmFilesForm


class DcnmDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dcnm