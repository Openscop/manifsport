# coding: utf-8
from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.dnm import DnmcForm, DnmcFilesForm
from ..models.dnm import Dnmc


class DnmcDetail(ManifDetail):
    """ Détail de manifestation """

    # Configuration
    model = Dnmc


class DnmcCreate(ManifCreate):
    """ Création de manifestation """

    # Configuration
    model = Dnmc
    form_class = DnmcForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnmc'
        return context


class DnmcUpdate(ManifUpdate):
    """ Modifier une manifestation """

    # Configuration
    model = Dnmc
    form_class = DnmcForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'dnmc'
        return context


class DnmcFilesUpdate(ManifFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = Dnmc
    form_class = DnmcFilesForm


class DnmcDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Dnmc