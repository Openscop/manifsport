# coding: utf-8
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView
from django.forms.models import ModelChoiceField
from django.forms.fields import IntegerField
from django.forms.widgets import HiddenInput

from administrative_division.models.departement import Departement
from administrative_division.models.commune import Commune
from evenements.forms.sansinstance import SansInstanceManifForm
from organisateurs.decorators import *
from ..models.sansinstance import SansInstanceManif


class SansInstanceManifCreate(CreateView):
    """ Création de manifestation dans un département pas encore configuré """

    # Configuration
    template_name = 'evenements/evenement_sansinstance_form.html'
    model = SansInstanceManif
    form_class = SansInstanceManifForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        if 'activite' not in self.request.session or not self.request.GET.get('dept'):
            return render(self.request, "core/access_restricted.html",
                          {'message': "Demande incorrecte. Veuillez utiliser l'outil de <a href=\"/evenement/creation/?dept=\">recherche de formulaire</a>."}, status=403)
        return super().dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        """ Renvoyer le formulaire initialisé """
        form = super().get_form(form_class)

        request_departement = self.request.GET.get('dept')
        form.fields['activite'].initial = self.request.session['activite']
        form.fields['nb_participants'].initial = self.request.session['nb_participants']
        form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
        form.fields['ville_depart'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                         label="Commune de départ", disabled=True)
        form.fields['ville_depart'].initial = self.request.session['commune']
        form.fields['departement_depart'] = IntegerField(widget=HiddenInput())
        form.fields['departement_depart'].initial = Departement.objects.get_by_name(request_departement).pk
        del form.fields['sites_natura2000']
        del form.fields['lieux_pdesi']
        return form

    def form_valid(self, form):
        """ Méthode appelée lorsque le formulaire est valide, renvoie une réponse HTTP """
        self.object = form.save(commit=False)
        self.object.structure = self.request.user.organisateur.structure
        self.object.instance = None
        self.object = form.save()
        self.request.session.pop('activite')
        return HttpResponseRedirect(self.get_success_url())
