# coding: utf-8
from django import forms

from .manif import ManifDetail, ManifCreate, ManifUpdate, ManifFilesUpdate, ManifDelete
from ..forms.avtmcir import AvtmcirForm, AvtmcirFilesForm
from ..models.vtm import Avtmcir


class AvtmcirDetail(ManifDetail):
    """ Détail de la manifestation """

    # Configiratiuon
    model = Avtmcir


class AvtmcirCreate(ManifCreate):
    """ Création de la manifestation """

    # Configuration
    model = Avtmcir
    form_class = AvtmcirForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtmcir'
        return context

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['circuit_homologue'].widget = forms.HiddenInput()
        return form


class AvtmcirUpdate(ManifUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = Avtmcir
    form_class = AvtmcirForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'avtmcir'
        return context

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['circuit_homologue'].widget = forms.HiddenInput()
        return form


class AvtmcirFilesUpdate(ManifFilesUpdate):
    """ Modification de la manifestation """

    # Configuration
    model = Avtmcir
    form_class = AvtmcirFilesForm


class AvtmcirDelete(ManifDelete):
    """ Supprimer une manifestation """

    # Configuration
    model = Avtmcir