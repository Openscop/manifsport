import os

from django import template

from preview_generator.manager import PreviewManager
from func_timeout import func_timeout, FunctionTimedOut

from configuration.settings import *


register = template.Library()


def createthumbnail(value):
    """
        Fonction pour créer des miniature.
        :param value: .ulr du fichier media voulu
        :return: url de l'image generée
        """
    try:
        url_fichier_source = value.path
        if not os.path.exists(url_fichier_source):
            return '/static/portail/img/vignette.png'
        cache_path = value.file.name[:-len(os.path.basename(value.file.name))] + "thumbnail/"
        manager = PreviewManager(cache_path, create_folder=True)
        image_reduite = manager.get_jpeg_preview(url_fichier_source, width=1000, height=500)
        return MEDIA_URL + image_reduite[len(MEDIA_ROOT):]
    except Exception as e:
        return '/static/portail/img/vignette.png'


@register.filter(name='thumbnail')
def thumbnail(value):
    """
    Filtre pour envoyer la miniature.
    Comme preview manager ne prend pas en compte le timeout on  utilite func_timeout pour empecher un blocage
    on attend la valeur une seconde si rien est envoyé on retourne none
    Il existe également :
    - Future ne voulait pas arreter le processus
    - Subprocess trop lourd.
    """
    try:
        result = func_timeout(10, createthumbnail, args=({value}))
        return result
    except FunctionTimedOut:
        return '/static/portail/img/vignette.png'
    except Exception as e:
        return '/static/portail/img/vignette.png'


