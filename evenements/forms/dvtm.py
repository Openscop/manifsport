# coding: utf-8
from crispy_forms.layout import Layout, Fieldset, HTML
from django.template.loader import render_to_string

from core.forms.base import GenericForm
from ..models.vtm import Dvtm
from .manif import ManifForm, FIELDS_INITIALS, FIELDS_MANIFESTATION, FIELDS_FILES, FIELDS_MARKUP, FIELDS_NATURA2000, FORM_WIDGETS, FIELDS_CONTACT


class DvtmForm(ManifForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.</p><hr>"),
                                    Fieldset(*(FIELDS_INITIALS)),
                                    Fieldset("Organisateur Technique", 'technique_nom', 'technique_prenom', 'technique_tel', 'technique_email'),
                                    Fieldset(*(FIELDS_MANIFESTATION[:-1] + ['nb_personnes_pts_rassemblement', 'nb_spectateurs', 'vehicules', 'nb_vehicules_accompagnement'])),
                                    Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*(FIELDS_NATURA2000[:-1])),
                                    Fieldset(*FIELDS_CONTACT))
        self.ajouter_css_class_requis_aux_champs_etape_0()

    # Meta
    class Meta:
        model = Dvtm
        exclude = ('descriptions_parcours', 'parcours_openrunner', 'structure', 'demande_homologation', 'instance')
        help_texts = {'engagement_organisateur': render_to_string('evenements/forms/help/mc_engagement_organisateur.txt')}
        widgets = FORM_WIDGETS


class DvtmFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES + ['itineraire_horaire'])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = Dvtm
        fields = FIELDS_FILES[1:] + ['itineraire_horaire']
