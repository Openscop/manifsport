# coding: utf-8
from crispy_forms.layout import Submit, Layout, HTML

from core.forms.base import GenericForm
from evenements.models import DocumentComplementaire


class DocumentComplementaireRequestForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(DocumentComplementaireRequestForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Envoyer la Demande d'Information"))

    # Meta
    class Meta:
        model = DocumentComplementaire
        fields = ['information_requise']
