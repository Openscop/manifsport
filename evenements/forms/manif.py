# coding: utf-8
import ast
import logging
from datetime import timedelta

from bootstrap_datepicker_plus import DateTimePickerInput
from django import forms
from django.core.exceptions import ValidationError
from django.urls import reverse_lazy
from django.forms import ModelChoiceField, IntegerField, BooleanField
from django.forms.models import ModelMultipleChoiceField
from django.forms.widgets import SelectMultiple
from django.utils import timezone
from crispy_forms.layout import HTML, Layout, Fieldset

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelMultipleChoiceField
from clever_selects.forms import ChainedChoicesModelForm
from core.forms.base import GenericForm
from ..models.manifestation import Manif
from sports.models.sport import Activite
from evaluation_incidence.models import N2kSite, PdesiLieu, RnrZone


logger = logging.getLogger('django.request')

# Contenus de Fieldsets standards
# Modifiés à la volée dans les helpers des formulaires hérités
FIELDS_INITIALS = ["Données initiales", 'activite', 'departement_depart', 'ville_depart', 'nb_participants']
FIELDS_MANIFESTATION = ["Détail de la manifestation", 'nom', 'date_debut', 'date_fin', 'description', 'observation',
                        'public', 'afficher_adresse_structure', 'dans_calendrier', 'departements_traverses',
                        'villes_traversees', 'nb_organisateurs', 'nb_spectateurs']
FIELDS_FILES = ["Pièces jointes", 'engagement_organisateur', 'reglement_manifestation', 'disposition_securite', 'charte_dispense_site_n2k',
                'topo_securite', 'presence_docteur', 'certificat_assurance', 'docs_additionels', 'cartographie']
FIELDS_FILES_optionals = ['cartographie', 'charte_dispense_site_n2k']
FIELDS_MARKUP = ["Balisage", 'convention_balisage', 'balisage']
FIELDS_NATURA2000 = ["Zones protégées", 'gros_budget', 'lucratif', 'delivrance_titre', 'vtm_hors_circulation',
                     'sites_natura2000', 'signature_charte_dispense_site_n2k', 'lieux_pdesi', 'zones_rnr',
                     'demande_homologation']
FIELDS_CONTACT = ["Personne à contacter sur place", 'nom_contact', 'prenom_contact', 'tel_contact']

# Dates par défaut de début et fin de manifestation
DEFAULT_START = timezone.now() + timedelta(days=150)
# Inversion jour et mois pour datepicker4
DEFAULT_START_STR = DEFAULT_START.strftime('%m/%d/%Y 8:00')
DEFAULT_END_STR = DEFAULT_START.strftime('%m/%d/%Y 20:00')

# Widgets
FORM_WIDGETS = {
    'date_debut': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_START_STR, 'locale': 'fr'}),
    'date_fin': DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'defaultDate': DEFAULT_END_STR, 'locale': 'fr'}),
    'description': forms.Textarea(attrs={'rows': 1}),
    'observation': forms.Textarea(attrs={'rows': 1}),
    'balisage': forms.Textarea(attrs={'rows': 1}),
    'tel_contact': forms.TextInput(attrs={'pattern': '^0[0-9]{9}'},)
}


class ManifForm(GenericForm, ChainedChoicesModelForm):
    """ Création de manifestation """

    # Champs
    activite = ModelChoiceField(queryset=Activite.objects.all(), disabled=True)
    nb_participants = IntegerField(disabled=True)
    departement_depart = ModelChoiceField(required=False, queryset=Departement.objects.configured(), label="Département de départ", disabled=True)
    ville_depart = ModelChoiceField(queryset=Commune.objects.all(), disabled=True)
    departements_traverses = ModelMultipleChoiceField(required=False, queryset=Departement.objects.all(), label="Autres départements traversés",
                                                      help_text="attention : si votre manifestation se déroule sur plusieurs départements, alors le délai d'instruction est de 3 mois !")
    villes_traversees = ChainedModelMultipleChoiceField('departements_traverses',
                                                     reverse_lazy('administrative_division:commune_widget'),
                                                     Commune, label="Autres communes traversées", required=False,
                                                     help_text="Ne pas sélectionner la commune de départ.")
    sites_natura2000 = ChainedModelMultipleChoiceField('departements_traverses', reverse_lazy('evaluation_incidence:sitesn2k_widget'),
                                                     N2kSite, required=False, help_text=Manif.AIDE_N2K, label="Sites Natura 2000")
    lieux_pdesi = ChainedModelMultipleChoiceField('departements_traverses', reverse_lazy('evaluation_incidence:lieuxpdesi_widget'),
                                                  PdesiLieu, required=False, help_text=Manif.AIDE_PDESI, label="Lieux PDESI")
    zones_rnr = ChainedModelMultipleChoiceField('departements_traverses', reverse_lazy('evaluation_incidence:zonesrnr_widget'),
                                                  RnrZone, required=False, help_text=Manif.AIDE_RNR, label="Zones RNR")
    # Créer un champ à substituer au champ "privé"
    public = forms.BooleanField(initial=True, label="Publier la manifestation dans le calendrier", required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        self.routes = kwargs.pop('routes', None)
        self.extradata = kwargs.pop('extradata', None)

        # Prendre la valeur du champ "privé" pour positionner le champ de remplacement
        if 'instance' in kwargs:
            if hasattr(kwargs['instance'], 'prive') and kwargs['instance'].prive:
                if 'initial' in kwargs:
                    kwargs['initial']['public'] = False

        super().__init__(*args, **kwargs)
        if hasattr(self, 'helper'):
            self.helper.form_tag = False
        self.fields['departements_traverses'].widget.attrs = {'data-placeholder': "1. Sélectionnez les départements traversés"}
        self.fields['villes_traversees'].widget.attrs = {'data-placeholder': "2. Sélectionnez les autres communes traversées"}
        self.fields['sites_natura2000'].widget.attrs = {'data-placeholder': "Faites votre sélection"}
        self.fields['lieux_pdesi'].widget.attrs = {'data-placeholder': "Faites votre sélection"}
        self.fields['activite'].widget.attrs = {'data-placeholder': "Choisissez une discipline"}
        if 'initial' in kwargs and 'departement_depart' in kwargs['initial']:
            dept = Departement.objects.get(pk=kwargs['initial']['departement_depart'])
            instance = dept.get_instance()
            if not instance.is_master() and hasattr(instance, 'natura2kdepartementconfig'):
                if not instance.natura2kdepartementconfig.pdesi:
                    del self.fields['lieux_pdesi']
    # Validation
    def clean_date_debut(self):
        """ Valider la date de départ """
        begin_date = self.cleaned_data['date_debut']
        if not begin_date:
            raise ValidationError("La date de départ de la manifestation ne peut pas être laissée vide")
        if not hasattr(self, 'instance') or not self.instance or not self.instance.id:
            if begin_date < timezone.now():
                raise ValidationError("La date de départ de la manifestation ne peut pas être dans le passé")
        return begin_date

    def clean_nom(self):
        """ Valider le nom de la manif """
        nom = self.cleaned_data['nom']
        if not nom:
            raise ValidationError("Le nom de la manifestation ne peut pas être laissée vide")
        return nom

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('nom', None):
            cleaned_data['nom'] = cleaned_data['nom'].replace('"', '＂')
        begin_date = cleaned_data.get('date_debut')
        end_date = cleaned_data.get('date_fin')
        if begin_date and end_date and begin_date >= end_date:
            message = "La date de fin de la manifestation doit être supérieure à la date de début."
            self.add_error("date_fin", self.error_class([message]))
        if begin_date and end_date and end_date >= begin_date + timedelta(days=90):
            message = "La manifestation ne peut excéder 90 jours."
            self.add_error("date_fin", self.error_class([message]))

        # Vérifier que "departure_city" n'est pas dans la liste de "crossed_cities"
        depart = cleaned_data.get('ville_depart')
        liste = list(cleaned_data.get('villes_traversees'))
        if liste:
            cleaned_data['villes_traversees'] = [x for x in liste if x != depart]

        # Convertir la valeur du champ "public" au champ "prive"
        public = cleaned_data.get('public')
        if public:
            cleaned_data['prive'] = False
        else:
            cleaned_data['prive'] = True
        return cleaned_data

    def ajouter_css_class_requis_aux_champs_etape_0(self):
        """ Ajoute une classe css nommée 'requis' à tous les champs obligatoire à l'étape 0 de l'instruction, c'est à dire pour être envoyé aux instructeurs """
        for cle in self.instance.get_liste_champs_etape_0(self.initial['departement_depart']):
            if cle in self.fields:
                field = self.fields[cle]
                css = field.widget.attrs.get('class', '')
                field.widget.attrs['class'] = 'requis ' + css

    class Meta:
        model = Manif
        exclude = []


class ManifInstructeurUpdateForm(GenericForm):
    """ Formulaire destiné aux instructeurs """

    class Meta:
        model = Manif
        fields = ['afficher_adresse_structure', 'cache']

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if hasattr(self, 'helper'):
            self.helper.form_tag = False


class ManifFilesForm(GenericForm):
    """ Formulaire d'upload des fichiers d'une manifestation """

    class Meta:
        model = Manif
        widgets = FORM_WIDGETS
        exclude = []


class ManifCartoForm(GenericForm):
    """
    formulaire pour la modif de la carto
    """

    def __init__(self, *args, **kwargs):
        self.routes = kwargs.pop('routes', None)
        super().__init__(*args, **kwargs)
        if hasattr(self, 'helper'):
            self.helper.layout = Layout(
                Fieldset("Cartographie de parcours", 'parcours_openrunner', 'descriptions_parcours'))
            self.helper.form_tag = False
        if self.routes and 'parcours_openrunner' in self.fields:
            if 'erreur' in self.routes:
                self.fields['parcours_openrunner'].widget.choices = list()
                self.fields['parcours_openrunner'].widget.attrs = {'data-placeholder': 'erreur'}
                self.fields['parcours_openrunner'].help_text = self.routes['erreur'].upper()
            else:
                self.fields['parcours_openrunner'].widget.choices = list(self.routes.items())
                self.fields['parcours_openrunner'].widget.attrs = {
                    'data-placeholder': "Sélectionnez les parcours pour cette manifestation"}
            #  ajouter css class "requis" aux deux champs
            field = self.fields['parcours_openrunner']
            css = field.widget.attrs.get('class', '')
            field.widget.attrs['class'] = 'requis ' + css
        field = self.fields['descriptions_parcours']
        css = field.widget.attrs.get('class', '')
        field.widget.attrs['class'] = 'requis ' + css

    # Validation
    def clean_parcours_openrunner(self):
        """ Nettoyer et valider le contenu de openrunner_route """
        # Les données sont une chaîne au format "[a,b,c,d...]"
        openrunner_route = self.cleaned_data['parcours_openrunner']
        try:
            # Convertir cette chaîne en type primitif python (liste d'entiers)
            # Renvoyer les routes dans un format compatible avec CommaSeparatedField
            openrunner_route = ast.literal_eval(openrunner_route)
            return ','.join(openrunner_route)
        except (SyntaxError, TypeError):
            return ''

    class Meta:
        model = Manif
        fields = ('parcours_openrunner', 'descriptions_parcours')
        widgets = {
            'parcours_openrunner': SelectMultiple(attrs={"size": 10}),
            'descriptions_parcours': forms.Textarea(attrs={'rows': 1}), }
