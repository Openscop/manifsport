from configuration.directory import UploadPath
from django.db import models

from . import Manif
from .instructionconfig import AbstractInstructionConfig
from core.FileTypevalidator import file_type_validator


class AbstractVtm(AbstractInstructionConfig):
    """ Manifestation avec véhicule motorisé """

    # Appel en deuxième position -> mixin : pas de surcharge des méthodes !!!!
    # Déplacement de la méthode "afficher_panneau_eval_n2000" dans les classes filles.

    # Champs
    vehicules = models.PositiveIntegerField("nombre de véhicules", blank=True, null=True)
    # Ajout de default dans les CharField et TextField pour la DB existante
    technique_nom = models.CharField("nom de famille", max_length=200, blank=True, default='')
    technique_prenom = models.CharField("prénom", max_length=200, blank=True, default='')
    technique_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    technique_email = models.EmailField("adresse email", max_length=200, blank=True, default='')

    # Meta
    class Meta:
        abstract = True


class Dvtm(AbstractVtm, Manif):
    """ Manifestation avec véhicule motorisé hors compétition """

    # Constantes
    refCerfa = "cerfa_15848-01"
    consultFederation = False
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    # delaiDepot = defaut = 60
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_CHAMPS_ETAPE_0 = AbstractInstructionConfig.LISTE_CHAMPS_ETAPE_0 + ['nb_vehicules_accompagnement', 'nb_personnes_pts_rassemblement', 'vehicules']
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['itineraire_horaire']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.manif', parent_link=True, related_name='dvtm', on_delete=models.CASCADE)
    nb_vehicules_accompagnement = models.PositiveIntegerField("nombre de véhicules d'accompagnement", blank=True, null=True, help_text="nombre de véhicules d'accompagnement")
    nb_personnes_pts_rassemblement = models.PositiveSmallIntegerField("nombre de personnes aux points de rassemblement", blank=True, null=True)
    itineraire_horaire = models.FileField(upload_to=UploadPath('itineraires_horaires'), blank=True, null=True,
                                          verbose_name="itinéraire horaire", max_length=512, validators=[file_type_validator])

    def get_delai_legal(self):
        """ Renvoyer le délai légal pour l'obtention d'une autorisation """
        if self.departements_traverses.count() < 20:
            return self.delaiDepot
        else:
            return self.delaiDepot_ndept

    # Meta
    class Meta:
        verbose_name = "manifestation avec véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations avec véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "dvtm"
        app_label = "evenements"


class Avtm(AbstractVtm, Manif):
    """ Manifestation avec véhicule motorisé sousmise à autorisation """

    # Constantes
    refCerfa = "cerfa_15847-01"
    consultFederation = True
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation obligatoire']
    delaiDepot = 90
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_CHAMPS_ETAPE_0 = AbstractInstructionConfig.LISTE_CHAMPS_ETAPE_0 + ['nb_vehicules_accompagnement', 'vehicules']
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['itineraire_horaire', 'commissaires', 'certificat_organisateur_tech']
    LISTE_FICHIERS_ETAPE_1 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_1 + ['avis_federation_delegataire', 'carte_zone_public']
    LISTE_FICHIERS_ETAPE_2 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_2 + ['participants']

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.manif', parent_link=True, related_name='avtm', on_delete=models.CASCADE)
    nb_vehicules_accompagnement = models.PositiveIntegerField("nombre de véhicules d'accompagnement", blank=True, null=True, help_text="nombre de véhicules d'accompagnement")
    avis_federation_delegataire = models.FileField(upload_to=UploadPath('avis_federation_delegataire'), blank=True, null=True,
                                                   verbose_name="avis fédération délégataire", max_length=512, validators=[file_type_validator])
    carte_zone_public = models.FileField(upload_to=UploadPath('carte_zone_publique'), blank=True, null=True,
                                         verbose_name="carte zone publique", max_length=512, validators=[file_type_validator])
    commissaires = models.FileField(upload_to=UploadPath('postes_commissaires'), blank=True, null=True,
                                    verbose_name="liste des postes commissaires", max_length=512, validators=[file_type_validator])
    certificat_organisateur_tech = models.FileField(upload_to=UploadPath('attestations_orga_tech'), blank=True, null=True,
                                                    verbose_name="attestation organisateur technique", max_length=512, validators=[file_type_validator])
    itineraire_horaire = models.FileField(upload_to=UploadPath('itineraires_horaires'), blank=True, null=True,
                                          verbose_name="itinéraire horaire", max_length=512, validators=[file_type_validator])
    participants = models.FileField(upload_to=UploadPath('liste_participants'), blank=True, null=True,
                                    verbose_name="liste complète des participants (voir a331-21)", max_length=512,
                                    help_text="nom, prénom, date et lieu de naissance, numéro de permis de conduire, nationalité et adresse de domicile ainsi que le numéro d'inscription de leur véhicule délivré par l'organisateur", validators=[file_type_validator])
    plan_masse = models.FileField(upload_to=UploadPath('plan_masse'), blank=True, null=True,
                                  verbose_name="plan de masse", max_length=512, validators=[file_type_validator])

    # Meta
    class Meta:
        verbose_name = "manifestation avec véhicules terrestres à moteur soumise à autorisation"
        verbose_name_plural = "manifestations avec véhicules terrestres à moteur soumises à autorisation"
        default_related_name = "avtm"
        app_label = "evenements"


class Avtmcir(AbstractVtm, Manif):
    """ Manifestation avec véhicule motorisé hors voie publique ou ouverte à la circulation publique """
    # Modèle non utilisé pour le moment avec les décrets 2017

    # Constantes
    refCerfa = "Pas de cerfa"
    consultFederation = True
    consultServices = AbstractInstructionConfig.LISTE_MODE_CONSULT['Consultation optionnelle']
    # ldelaiDepot = 60
    # delaiDepot_ndept = defaut = 90
    # delaiDocComplement1 = defaut = 21
    # delaiDocComplement2 = defaut = 6
    LISTE_CHAMPS_ETAPE_0 = AbstractInstructionConfig.LISTE_CHAMPS_ETAPE_0 + ['type_support', 'vehicules']
    LISTE_FICHIERS_ETAPE_0 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_0 + ['plan_masse', 'certificat_organisateur_tech']
    LISTE_FICHIERS_ETAPE_2 = AbstractInstructionConfig.LISTE_FICHIERS_ETAPE_2 + ['participants']

    SUPPORT_CHOICES = (('s', "circuit"), ('g', "terrain"), ('r', "parcours"))

    # Champs
    manifestation_ptr = models.OneToOneField('evenements.manif', parent_link=True, related_name='avtmcir', on_delete=models.CASCADE)
    type_support = models.CharField("type de support", blank=True, null=True, max_length=1, choices=SUPPORT_CHOICES)
    # TODO : à supprimer si ce formulaire est dédié au circuit homologué (décision ministère)
    plan_masse = models.FileField(upload_to=UploadPath('plan_masse'), blank=True, null=True,
                                  verbose_name="plan de masse", max_length=512, validators=[file_type_validator])
    certificat_organisateur_tech = models.FileField(upload_to=UploadPath('attestations_orga_tech'), blank=True, null=True,
                                                    verbose_name="attestation organisateur technique", max_length=512, validators=[file_type_validator])
    participants = models.FileField(upload_to=UploadPath('liste_participants'), blank=True, null=True,
                                    verbose_name="liste complète des participants (voir a331-21)", max_length=512,
                                    help_text="nom, prénom, date et lieu de naissance, numéro de permis de conduire, nationalité et adresse de domicile ainsi que le numéro d'inscription de leur véhicule délivré par l'organisateur", validators=[file_type_validator])

    # Getter
    def get_delai_legal(self):
        """ Renvoyer le délai légal d'instruction """
        return self.get_instance().get_manifestation_avtm_homolog_delay()

    # Méta
    class Meta:
        verbose_name = "manifestation sur circuit, terrain ou parcours, soumise à autorisations"
        verbose_name_plural = "courses avec véhicules terrestres à moteur"
        default_related_name = "avtmcir"
        app_label = "evenements"
