import sys, datetime

from django.apps import apps
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.core.validators import validate_comma_separated_integer_list
from django.core.exceptions import ObjectDoesNotExist
from django.urls import reverse
from django.utils import timezone
from django.template.loader import render_to_string
from model_utils.managers import InheritanceQuerySet

from administration.models import Prefecture
from carto.consumer.openrunner import openrunner_api
from configuration.directory import UploadPath
from sports.models.federation import Federation
from core.models.instance import Instance
from core.models import User
from core.FileTypevalidator import file_type_validator
from configuration import settings
from core.tasks import creation_thumbail_manif


class ManifQuerySet(InheritanceQuerySet):
    """ Queryset des manifestations """

    # Getter
    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les manifestations pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les manifestations pour l'utilisateur s'il est renseigné
            return self.filter(instance=request.user.get_instance())
        elif instances:
            # Renvoyer les manifestations pour les instances si elles sont renseignées
            return self.filter(instance__in=instances)
        else:
            return self

    def par_organisateur(self, organisateur):
        """ Renvoyer les manifestations créées par l'organisateur """
        return self.filter(structure__organisateur=organisateur)

    def prisesencharge(self):
        """ Renvoyer les manifestations prises en charge """
        return self.filter(instance__departement__isnull=False)

    def nonprisesencharge(self):
        """ Renvoyer les manifestations non prises en charge """
        return self.filter(instance__departement__isnull=True)


class Manif(models.Model):
    """ Demande de manifestation sportive """

    CHOIX_VTM = [["dvtm", "dvtm"], ["avtm", "avtm"]]
    CHOIX_NM = (("dnm", "dnm"), ("dnmc", "dnmc"), ("dcnm", "dcnm"), ("dcnmc", "dcnmc"))
    CHOIX_MANIF = [["dnm", "dnm"], ["dnmc", "dnmc"], ["dcnm", "dcnm"], ["dcnmc", "dcnmc"], ["dvtm", "dvtm"], ["avtm", "avtm"], ["avtmcir", "avtmcir"]]
    CHOIX_EMPRISE = [(0, "--------"),
                    (1, "Hors voies publiques ou ouvertes à la circulation"),
                    (2, "Partiellement sur voies publiques ou ouvertes à la circulation"),
                    (3, "Totalement sur voies publiques ou ouvertes à la circulation")]
    EMPRISE = {"null": 0, "hors": 1, "partiel": 2, "total": 3}  # Inverse de CHOIX_EMPRISE pour faciliter la lecture du code

    # Texte d'aide et descriptions
    AIDE_DESCRIPTION = "précisez les modalités d'organisation et les caractéristiques de la manifestation"
    AIDE_OBSERVATION = "précisez les observations sur la manifestation, destinées aux services d'instruction"
    AIDE_PARCOURS_DESCRIPTION = "précisez pour chaque parcours : nb km, nb participants, heure de départ, départ groupé ou échelonné..."
    DESC_DANS_CALENDRIER = "la manifestation est inscrite sur le calendrier de la fédération délégataire de la discipline"
    DESC_DEPARTEMENTS = "autres départements traversés par la manifestation"
    AIDE_MULTISELECT = "maintenez appuyé « Ctrl », ou « Commande (touche pomme) » sur un Mac, pour en sélectionner plusieurs."
    AIDE_NB_PARTICIPANTS = "renseigner le nombre de participants de la précedente édition ou le nombre de participants estimé si première édition"
    AIDE_NB_ORGANISATEURS = "renseigner le nombre d'organisateurs présents pour la manifestation"
    DESC_CONVENTION_BALISAGE = "vous êtes signataire de la charte du balisage temporaire"
    AIDE_CONVENTION_BALISAGE = "Voir : <a href='/aide/charteecob/' target='_blank'>Charte Ecobalisage</a>"
    AIDE_SENS_UNIQUE = "veuillez lister les voies publiques à emprunter en sens unique lors de la manifestation"
    AIDE_VOIE_FERMEE = "veuillez lister les voies publiques à fermer lors de la manifestation"
    DESC_GROS_BUDGET = "le budget de la manifestation excède 100 000 €"
    DESC_GROS_TITRE = "manifestation donnant lieu à la délivrance d'un titre national ou international"
    DESC_LUCRATIF = "manifestation présentant un caractère lucratif et regroupant sur un même site plus de 1500 personnes"
    DESC_VTM_HORS_CIRC = "manifestation avec engagement de véhicules terrestres à moteur se déroulant en dehors des voies ouvertes à la circulation"
    DESC_CHARTE_DISPENSE = "une charte de dispense en cours de validité pour le site Natura 2000 a été signée"
    DESC_VTM_N2K = "manifestation avec engagement de véhicules terrestres à moteur se déroulant sur des voies ouvertes à la circulation et à l'intérieur " \
                   "d'un périmètre Natura 2000"
    AIDE_N2K = "sélectionnez les sites Natura 2000 traversés par la manifestation"
    AIDE_RNR = "sélectionnez les zones Réserve Naturelle Régionale traversées par la manifestation"
    AIDE_PDESI = "sélectionner les lieux inscrits au plan PDESI traversés par la manifestation"
    DESC_HOMOLOGATION = "manifestation avec demande d'homologation d'un circuit de sports motorisés"
    AIDE_ENGAGEMENT = "un modèle de cette déclaration est disponible <a target='_blank' href='/aide/engagement_organisateur/autorisation'>ici</a>"
    DESC_ENGAGEMENT = "déclaration et engagement de l'organisateur"
    AIDE_ASSURANCE = "attestation de la police d'assurance souscrite par l'organisateur et couvrant sa responsabilité civile ainsi que celle des " \
                     "participants à la manifestation et de toute personne, nommément désignée par l'organisateur, prêtant son concours à l'organisation" \
                     "de la manifestation"
    DESC_SECURITE = "dispositions prises pour la sécurité"
    AIDE_DISPO_SECURITE = "recenser l'ensemble des dispositions assurant la sécurité et la protection des pratiquants et des tiers ainsi que les mesures " \
                          "prises par l'organisateur pour garantir la tranquilité publique pendant toute la durée de la manifestation (attestation " \
                          "secouristes, ambulance, extincteurs, dépanneuse...)"
    AIDE_TOPO_SECURITE = "recenser l'ensemble des dispositions assurant la sécurité et la protection des pratiquants et des tiers ainsi que les mesures " \
                         "prises par l'organisateur pour garantir la tranquilité publique pendant toute la durée de la manifestation (carte détaillée " \
                         "et photos des zones de protection du public)"
    DESC_PRESENCE_MED = "attestation de présence de médecin(s)"

    # Chemins d'upload
    UP_CARTO = UploadPath('cartographie')
    UP_REGLE = UploadPath('reglements')
    UP_ENGA = UploadPath('engagements')
    UP_ASSU = UploadPath('attestation_assurance')
    UP_DISPO_SEC = UploadPath('securite')
    UP_DOCT = UploadPath('attestations_presence_medecin')
    UP_TOPO_SEC = UploadPath('securisation_epreuves')
    UP_EXTRA = UploadPath('doc_complementaires')
    UP_CHARTE = UploadPath('charte_dispense_n2k')
    UP_CONV = UploadPath('convention_police')

    # Attributs généraux
    instance = models.ForeignKey('core.instance', null=True, verbose_name="Configuration d'instance", on_delete=models.SET_NULL)
    structure = models.ForeignKey('organisateurs.structure', verbose_name="structure", on_delete=models.CASCADE)
    nom = models.CharField(verbose_name="nom de la manifestation", max_length=255, null=True, blank=True, unique=True)
    date_creation = models.DateField("date de création", auto_now_add=True)
    date_debut = models.DateTimeField("date de début", null=True, blank=True)
    date_fin = models.DateTimeField("date de fin", null=True, blank=True)
    description = models.TextField("description", null=True, blank=True, help_text=AIDE_DESCRIPTION)
    observation = models.TextField("observation", default='', blank=True, help_text=AIDE_OBSERVATION)
    activite = models.ForeignKey('sports.activite', verbose_name="activité", on_delete=models.CASCADE)
    dans_calendrier = models.BooleanField(verbose_name=DESC_DANS_CALENDRIER, default=False)
    ville_depart = models.ForeignKey('administrative_division.commune', verbose_name="commune de départ", on_delete=models.CASCADE)
    parcours_openrunner = models.CharField("cartographie de parcours", validators=[validate_comma_separated_integer_list], max_length=255, blank=True)
    descriptions_parcours = models.TextField("description des parcours", default='', blank=True, help_text=AIDE_PARCOURS_DESCRIPTION)
    nb_organisateurs = models.PositiveIntegerField("nombre d'organisateurs", null=True, blank=True, help_text=AIDE_NB_ORGANISATEURS)
    nb_participants = models.PositiveIntegerField("nombre de participants", null=True, blank=True, help_text=AIDE_NB_PARTICIPANTS)
    nb_spectateurs = models.PositiveIntegerField("nombre max. de spectateurs", null=True, blank=True)
    villes_traversees = models.ManyToManyField('administrative_division.commune', related_name="%(app_label)s_%(class)s_crossed_by", verbose_name="communes traversées", blank=True)
    departements_traverses = models.ManyToManyField('administrative_division.departement', verbose_name=DESC_DEPARTEMENTS, blank=True)
    prenom_contact = models.CharField("prénom", max_length=200, null=True, blank=True)
    nom_contact = models.CharField("nom de famille", max_length=200, null=True, blank=True)
    tel_contact = models.CharField("numéro de téléphone", max_length=14, null=True, blank=True)

    # delai
    delai_cours = models.CharField('Delai en cours', max_length=30, null=True, blank=True)

    # Informations de publication
    prive = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Organisateurs
    cache = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Instructeurs
    afficher_adresse_structure = models.BooleanField(default=True, verbose_name="Afficher les coordonnées de la structure dans le calendrier")  # Organisateur

    # Informations de balisage
    convention_balisage = models.BooleanField(verbose_name=DESC_CONVENTION_BALISAGE, help_text=AIDE_CONVENTION_BALISAGE, default=False)
    balisage = models.TextField("Type et mode de balisage utilisé", null=True, blank=True)

    # Questions aiguillage cerfa, les libellés sont affinés dans form.create
    emprise = models.SmallIntegerField("Emprise sur voie publique", choices=CHOIX_EMPRISE, default=EMPRISE['null'], blank=True)
    competition = models.BooleanField(verbose_name="Compétition", default=False)
    circuit_non_permanent = models.BooleanField(verbose_name="Circuit non permanent", default=False)
    circuit_homologue = models.BooleanField(verbose_name="Circuit homologué dans la discipline", default=False)

    # Questions natura 2000
    gros_budget = models.BooleanField(verbose_name=DESC_GROS_BUDGET, default=False)
    delivrance_titre = models.BooleanField(verbose_name=DESC_GROS_TITRE, default=False)
    lucratif = models.BooleanField(verbose_name=DESC_LUCRATIF, default=False)
    vtm_hors_circulation = models.BooleanField(verbose_name=DESC_VTM_HORS_CIRC, default=False)

    signature_charte_dispense_site_n2k = models.BooleanField(verbose_name=DESC_CHARTE_DISPENSE, default=False)
    sites_natura2000 = models.ManyToManyField('evaluation_incidence.n2ksite', blank=True, verbose_name="sites Natura 2000", help_text=AIDE_N2K)
    lieux_pdesi = models.ManyToManyField('evaluation_incidence.pdesilieu', blank=True, verbose_name="lieux inscrits au PDESI", help_text=AIDE_PDESI)
    demande_homologation = models.BooleanField(verbose_name=DESC_HOMOLOGATION, default=False)
    zones_rnr = models.ManyToManyField('evaluation_incidence.rnrzone', blank=True, verbose_name="zones RNR", help_text=AIDE_RNR)

    # Fichiers joints
    cartographie = models.FileField(upload_to=UP_CARTO, blank=True, null=True, verbose_name="cartographie de parcours", max_length=512, validators=[file_type_validator])
    reglement_manifestation = models.FileField(upload_to=UP_REGLE, blank=True, null=True, verbose_name="réglement de la manifestation", max_length=512, validators=[file_type_validator])
    disposition_securite = models.FileField(upload_to=UP_DISPO_SEC, blank=True, null=True, verbose_name=DESC_SECURITE, help_text=AIDE_DISPO_SECURITE, max_length=512, validators=[file_type_validator])
    presence_docteur = models.FileField(upload_to=UP_DOCT, blank=True, null=True, verbose_name=DESC_PRESENCE_MED, max_length=512, validators=[file_type_validator])
    docs_additionels = models.FileField(upload_to=UP_EXTRA, blank=True, null=True, verbose_name="documents complémentaires", max_length=512, validators=[file_type_validator])
    engagement_organisateur = models.FileField(upload_to=UP_ENGA, blank=True, null=True, verbose_name=DESC_ENGAGEMENT, help_text=AIDE_ENGAGEMENT, max_length=512, validators=[file_type_validator])
    certificat_assurance = models.FileField(upload_to=UP_ASSU, blank=True, null=True, verbose_name="attestation d'assurance", help_text=AIDE_ASSURANCE, max_length=512, validators=[file_type_validator])
    topo_securite = models.FileField(upload_to=UP_TOPO_SEC, blank=True, null=True, verbose_name="sécurisation des épreuves", help_text=AIDE_TOPO_SECURITE, max_length=512, validators=[file_type_validator])
    charte_dispense_site_n2k = models.FileField(upload_to=UP_CHARTE, blank=True, null=True, verbose_name="charte de dispense Natura 2000", max_length=512, validators=[file_type_validator])
    convention_police = models.FileField(upload_to=UP_CONV, blank=True, null=True, verbose_name="Convention avec la police nationale ou la gendarmerie", max_length=512, validators=[file_type_validator])

    objects = ManifQuerySet.as_manager()

    # Overrides
    def __str__(self):
        if self.nom:
            return self.nom.title()
        return ''

    def save(self, *args, **kwargs):
        # Définir l'instance à celle de la ville de départ
        if self.ville_depart and not self.instance and not self.ville_depart.get_instance().is_master():
            self.instance = self.ville_depart.get_departement().get_instance()
        if self.pk:
            delai = self.get_cerfa().delai_en_cours()
            if not delai == 21:
                self.delai_cours = str(delai)
            else:
                depot = self.get_cerfa().delaiDepot
                self.delai_cours = str(depot) + '-21'
        super().save(*args, **kwargs)

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de consultation """
        url = ''.join(['evenements:', ContentType.objects.get_for_model(self).model, '_detail'])
        return reverse(url, kwargs={'pk': self.pk})

    def get_instance(self):
        """
        Renvoyer l'instance associée à la manifestation

        :rtype: core.models.Instance
        """
        return self.instance

    def get_departement_depart_nom(self):
        """ Renvoyer le nom simple du département de départ """
        return self.ville_depart.get_departement().name

    def get_federation(self):
        """ Renvoyer la fédération pour le département et pour la discipline """
        return Federation.objects.get_for_departement(self.activite.discipline, self.ville_depart.get_departement())

    def get_organisateur(self):
        """ Renvoyer l'organisateur """
        return self.structure.organisateur

    def get_instruction(self):
        """ Renvoyer l'autorisation si disponible """
        try:
            return self.instruction
        except (AttributeError,):
            return None

    def get_departements_traverses(self):
        """ Renvoyer les départements traversés """
        return list(set(list(self.departements_traverses.all()) + [self.ville_depart.get_departement()]))

    def get_villes_traversees(self):
        """
        Renvoyer les communes traversées

        :returns: toutes les communes traversées (dont celle de départ)
        """
        return list(set(list(self.villes_traversees.all()) + [self.ville_depart]))

    def get_nom_departements_traverses(self):
        """ Renvoyer les noms (01, 42, 78 etc.) des départements traversés """
        return [departement.name for departement in self.get_departements_traverses()]

    def pas_supportee(self):
        """ Renvoyer si la manifestation n'est pas prise en charge """
        return self.instance is None or self.instance.departement is None

    def visible_dans_calendrier(self):
        """ Renvoyer l'état de visibilité dans le calendrier """
        return not (self.prive or self.cache)
    visible_dans_calendrier.short_description = "Dans le calendrier"
    visible_dans_calendrier.boolean = True

    def get_federation_defaut(self):
        """ Renvoyer la fédération disponible qui correspond le mieux à cette manif """
        return Federation.objects.get_for_departement(self.activite.discipline, self.ville_depart.get_departement())

    def soumise_natura2000(self):
        """ Renvoyer si la manifestation est soumise à une éval N2K """
        return hasattr(self, 'natura2000evaluations') or hasattr(self, 'n2kevaluation')
    soumise_natura2000.short_description = "est soumise à évaluation natura 2000"
    soumise_natura2000.boolean = True

    def soumise_rnr(self):
        """ Renvoyer si la manifestation est soumise à une éval RNR """
        return hasattr(self, 'rnrevaluations') or hasattr(self, 'rnrevaluation')
    soumise_rnr.short_description = "est soumise à évaluation RNR"
    soumise_rnr.boolean = True

    def get_delai_legal(self):
        """ Renvoie le délai légal en jours pour déclarer ou envoyer la demande d'autorisation """
        return self.get_cerfa().delaiDepot

    def get_date_limite(self):
        """ Retourne la date limite pour déclarer ou envoyer la demande d'autorisation """
        return (self.date_debut - datetime.timedelta(days=self.get_delai_legal())).replace(hour=23, minute=59)

    def get_date_etape_1(self):
        """ Retourne la première date limite pour déposer des documents manquants """
        return (self.date_debut - datetime.timedelta(days=self.get_cerfa().delaiDocComplement1)).replace(hour=23, minute=59)

    def get_date_etape_2(self):
        """ Retourne la deuxième date limite pour déposer des documents manquants """
        return (self.date_debut - datetime.timedelta(days=self.get_cerfa().delaiDocComplement2)).replace(hour=23, minute=59)

    def get_prefecture_concernee(self):
        """ Renvoyer la préfécture selon la ville de départ """
        return self.ville_depart.arrondissement.prefecture

    def deux_semaines_restantes(self):
        """ Renvoie true or false suivant le nombre de semaines restantes (sous 2 semaines ou plus de 2 semaines) """
        return self.get_date_limite().date() < (datetime.date.today() + datetime.timedelta(weeks=2))

    def delai_depasse(self):
        """ Si le délai officiel est dépassé, renvoie True, sinon, renvoie False """
        return datetime.date.today() > self.get_date_limite().date()

    def date_depassee(self):
        """ Si la manif est passée, renvoie True, sinon, renvoie False """
        return datetime.date.today() > self.date_debut.date()

    def get_nbr_ppl(self):
        """
        Renvoie un int contenant le nombre de personne actuellement sur le dossier
        :return:
        """
        last_time = timezone.now() - datetime.timedelta(minutes=settings.INACTIVE_TIME)
        count = User.objects.filter(last_manif=self.pk, last_visit__gte=last_time).count()
        return count

    def get_parcours_manif(self):
        """
        Renvoyer les parcours OpenRunner sélectionnés pour la manifestation

        Si l'on considère les ensembles get_manifestation_routes et get_available_routes,
        l'intersection entre les deux est égale à get_manifestation_routes. Pour faire clair,
        get_manifestation_routes renvoie toujours tout ou partie des parcours renvoyés
        par get_available_routes.
        :returns: un dictionnaire id parcours:nom parcours
        :rtype: dict
        """
        ids = [route for route in self.parcours_openrunner.strip(',').split(',') if route]
        parcours_dispo = self.get_parcours_dispo()
        if parcours_dispo.get('erreur', False):
            parcours = parcours_dispo
        else:
            parcours = {key: parcours_dispo[key] for key in ids if key in parcours_dispo}
        return parcours

    def get_parcours_dispo(self):
        """ Renvoyer les parcours Openrunner disponibles pour la manifestation """
        return openrunner_api.get_user_routes(self.get_organisateur().user.get_username())

    def get_manifs_meme_jour(self, include_self=False):
        """
        Renvoyer les manifestations qui ont lieu aux mêmes dates que cette manifestation
        (et qui bien entendu, ne sont pas cette même manifestation)

        Dans la pratique, si cette manifestation est nommée e1, et la manifestation à
        comparer est nommée e2, les deux manifestations coïncident si :
        - (e2.debut <= e1.debut ET e2.fin >= e1.debut) OU
        - (e2.debut <= e1.fin ET e2.fin >= e1.fin) OU
        - (e2.debut >= e1.debut ET e2.fin <= e1.fin)

        Et plus simplement, elles coïncident si :
        - NON (e2.debut > e1.fin OU e2.fin < e1.debut)
        """
        if self.nom and self.date_debut and self.date_fin:
            exclusion = {} if include_self else {'nom': self.nom}
            return Manif.objects.exclude(models.Q(date_debut__gt=self.date_fin) | models.Q(date_fin__lt=self.date_debut)).exclude(**exclusion).distinct()
        return Manif.objects.none()

    def get_manifs_meme_jour_meme_ville(self, include_self=False):
        """
        Renvoyer les manifestations qui passent dans les mêmes villes
        que la manifestation actuelle.
        """
        communes = self.get_villes_traversees()
        return self.get_manifs_meme_jour(include_self=include_self).filter(models.Q(ville_depart__in=communes) | models.Q(villes_traversees__in=communes)).distinct()

    def get_str_ids_parcours_en_conflit(self, include_self=True):
        """
        Renvoyer les ids des parcours probablement en conflit formatté en chaine avec séparateur "-"
        Les ids de parcours de la manifestation en cours sont inclus.

        :rtype: list
        """
        # Pour ne pas interroger OpenRunner lorsque l'on joue les Tests de Django avec manage.py
        if 'test' in sys.argv[1:]:
            return '1'

        conflits_possible = self.get_manifs_meme_jour_meme_ville(include_self=include_self)
        parcours_ids = []
        for manifestation in conflits_possible:
            parcours = manifestation.get_parcours_manif()
            for key in parcours.keys():  # Récupérer les ids
                parcours_ids.append(key)
        parcours_ids = list(set(parcours_ids))  # dédupliquer
        return '-'.join([str(parcours) for parcours in parcours_ids])

    def get_parcours_manif_str(self):
        parcours_ids = []
        parcours = self.get_parcours_manif()
        for key in parcours.keys():
            parcours_ids.append(key)
        parcours_ids = list(set(parcours_ids))
        return '-'.join([str(parcours) for parcours in parcours_ids])

    def get_manifs_en_conflit_avec_OR(self):
        """ Renvoyer les manifs du même jour/même ville qui ont des routes OpenRunner """
        result = []
        conflit_possible = self.get_manifs_meme_jour_meme_ville()
        for manifestation in conflit_possible:
            if manifestation.parcours_openrunner:
                result.append(manifestation)
        return result

    def get_manifs_en_conflit_sans_OR(self):
        """ Renvoyer les manifs du même jour/même ville qui n'ont pas de route OpenRunner """
        result = []
        conflit_possible = self.get_manifs_meme_jour_meme_ville()
        for manifestation in conflit_possible:
            if not manifestation.parcours_openrunner:
                result.append(manifestation)
        return result

    def afficher_panneau_eval_rnr(self):
        """ Renvoyer si l'encart d'évaluation RNR doit être affiché """
        from evaluation_incidence.models import EvaluationManif
        return EvaluationManif.evaluation_rnr_requise(self)

    def afficher_panneau_eval_n2000(self, cause=None):
        """ Renvoyer si l'encart d'évaluation Natura2000 doit être affiché """
        from evaluation_incidence.models import EvaluationManif
        return EvaluationManif.evaluation_n2k_requise(self, cause)

    def get_breadcrumbs(self):
        """ Renvoie les breadcrumbs indiquant l'avancement de l'instruction """
        # breadcrumbs est une liste d'étape d'instruction, chaque étape d'instruction est une liste composée ainsi :
        # ["Libellé de l'étape", boolean qui indique si l'étape est terminée ]
        breadcrumbs = [["<i class='detail'></i>Préparation du dossier",
                        False,
                        self.get_absolute_url()+"edit/?dept="+self.get_departement_depart_nom()]]
        if self.formulaire_complet():
            breadcrumbs = [["<i class='detail'></i>Préparation du dossier", True, ""]]
        if self.afficher_panneau_eval_rnr():
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée RNR",
                                False,
                                reverse('evaluation_incidence:rnreval_add', kwargs={'manif_pk': self.id})])
        if hasattr(self, 'rnrevaluation') and not self.rnrevaluation.formulaire_rnr_complet():
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée RNR",
                                False,
                                reverse('evaluation_incidence:rnreval_update', kwargs={'pk': self.rnrevaluation.id})])
        elif hasattr(self, 'rnrevaluations') or (hasattr(self, 'rnrevaluation') and self.rnrevaluation.formulaire_rnr_complet()):
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée RNR", True, ""])
        if self.afficher_panneau_eval_n2000():
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée Natura 2000",
                                False,
                                reverse('evaluation_incidence:n2keval_add', kwargs={'manif_pk': self.id})])
        if hasattr(self, 'n2kevaluation') and not self.n2kevaluation.formulaire_n2k_complet():
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée Natura 2000",
                                False,
                                reverse('evaluation_incidence:n2keval_update', kwargs={'pk': self.n2kevaluation.id})])
        elif hasattr(self, 'natura2000evaluations') or (hasattr(self, 'n2kevaluation') and self.n2kevaluation.formulaire_n2k_complet()):
            breadcrumbs.append(["<i class='feuille'></i>EI simplifiée Natura 2000", True, ""])
        try:
            self.instruction
        except (AttributeError, ObjectDoesNotExist):
            breadcrumbs.append(["<i class='envoyer'></i>Envoi de la demande", False, ""])
        else:
            breadcrumbs.append(["<i class='envoyer'></i>Envoi de la demande&nbsp;&nbsp;", True, ""])
            if not self.dossier_complet():
                breadcrumbs.append(["<i class='doc'></i>Pièce à joindre", False, "#pieceajoindre"])
        return breadcrumbs

    def etape_en_cours(self):
        """ Renvoyer la période de temps actuelle en fonction des différents delais """
        if datetime.date.today() < self.get_date_limite().date():
            return 'etape 0'
        elif datetime.date.today() < self.get_date_etape_1().date():
            return 'etape 1'
        elif datetime.date.today() < self.get_date_etape_2().date():
            return 'etape 2'
        return 'etape 3'

    def date_limite_etape_en_cours(self):
        """Renvoi la date limite de la prochaine étape"""
        if datetime.date.today() < self.get_date_limite().date():
            return self.get_date_limite()
        elif datetime.date.today() < self.get_date_etape_1().date():
            return self.get_date_etape_1()
        elif datetime.date.today() < self.get_date_etape_2().date():
            return self.get_date_etape_2()
        return self.date_debut.date()

    def delai_en_cours(self):
        """ Renvoyer le delai qui court à la date en cours """
        if datetime.date.today() < self.get_date_limite().date():
            return self.delaiDepot
        elif datetime.date.today() < self.get_date_etape_1().date():
            return self.delaiDocComplement1
        elif datetime.date.today() < self.get_date_etape_2().date():
            return self.delaiDocComplement2
        return 0

    def ecrire_delai(self, manif=None, all=False):
        if not manif:

            if all:
                manifs = Manif.objects.all()
            else:
                manifs = Manif.objects.filter(date_debut__gte=timezone.now())

            for manif in manifs:
                if hasattr(manif.get_cerfa(), "delaiDepot"):
                    delai = manif.get_cerfa().delai_en_cours()
                    if not delai == 21:
                        manif.delai_cours = str(delai)
                    else:
                        depot = manif.get_cerfa().delaiDepot
                        manif.delai_cours = str(depot)+'-21'
                    manif.save()
            oldmanifs = Manif.objects.filter(date_debut__lte=timezone.now()).filter(delai_cours__isnull=False)
            for manif in oldmanifs:
                manif.delai_cours = None
                manif.save()
        else:
            delai = manif.get_cerfa().delai_en_cours()
            if not delai == 21:
                manif.delai_cours = str(delai)
            else:
                depot = manif.get_cerfa().delaiDepot
                manif.delai_cours = str(depot) + '-21'
            manif.save()

    def formulaire_complet(self):
        """ Indiquer si le formulaire est complet """
        liste_champs = self.get_liste_champs_etape_0()
        try:
            liste_champs.remove('descriptions_parcours')
            liste_champs.remove('parcours_openrunner')
        except ValueError:
            pass
        for champ in liste_champs:
            if getattr(self, champ) == None:
                return False
            elif isinstance(getattr(self, champ), str) and getattr(self, champ) == '':
                return  False
        return True

    def carto_complet(self):
        """ Indiquer si la carto est complete """
        liste_champs = ['descriptions_parcours']
        if 'parcours_openrunner' in self.get_liste_champs_etape_0():
            liste_champs.append('parcours_openrunner')
        for champ in liste_champs:
            if getattr(self, champ) == None:
                return False
            elif isinstance(getattr(self, champ), str) and getattr(self, champ) == '':
                return  False
        return True

    def dossier_complet(self):
        """ Indiquer si le dossier est complet en fonction de la date courante et des pièces manquantes """
        if self.etape_en_cours() == 'etape 0':
            if hasattr(self, 'n2kevaluation'):
                if not self.n2kevaluation.formulaire_n2k_complet():
                    return False
            if hasattr(self, 'rnrevaluation'):
                if not self.rnrevaluation.formulaire_rnr_complet():
                    return False
            if not self.formulaire_complet():
                return False
            if not self.carto_complet():
                return False
            for fichier in self.get_liste_fichier_etape_0():
                if not getattr(self, fichier):
                    return False
        if self.etape_en_cours() == 'etape 1':
            for fichier in self.LISTE_FICHIERS_ETAPE_1:
                if not getattr(self, fichier):
                    return False
        if self.etape_en_cours() == 'etape 2' or self.etape_en_cours() == 'etape 3':
            liste = self.LISTE_FICHIERS_ETAPE_1 + self.LISTE_FICHIERS_ETAPE_2
            for fichier in liste:
                if not getattr(self, fichier):
                    return False
        return True

    def liste_manquants(self):
        """
        Donner la liste des documents manquants en fonction de la date courante
        On détermine le modèle de dossier puis on retrouve le champ dans le méta du modèle pour extraire le verbose_name
        """
        listemanquants = []
        if self.etape_en_cours() == 'etape 0':
            for fichier in self.get_liste_fichier_etape_0():
                if not getattr(self, fichier):
                    listemanquants.append(apps.get_model(app_label='evenements', model_name=self.get_type_manif())._meta.get_field(fichier).verbose_name)
        if self.etape_en_cours() == 'etape 1':
            for fichier in self.LISTE_FICHIERS_ETAPE_1:
                if not getattr(self, fichier):
                    listemanquants.append(apps.get_model(app_label='evenements', model_name=self.get_type_manif())._meta.get_field(fichier).verbose_name)
        if self.etape_en_cours() == 'etape 2' or self.etape_en_cours() == 'etape 3':
            liste = self.LISTE_FICHIERS_ETAPE_1 + self.LISTE_FICHIERS_ETAPE_2
            for fichier in liste:
                if not getattr(self, fichier):
                    listemanquants.append(apps.get_model(app_label='evenements', model_name=self.get_type_manif())._meta.get_field(fichier).verbose_name)
        return listemanquants

    def en_cours_instruction(self):
        """
        Renvoyer si une spécialisation de manifestation existe
        En d'autres mots, renvoyer si le formulaire principal est validé.
        """
        try:
            self.instruction
        except AttributeError:
            return False
        return True
    en_cours_instruction.short_description = "Formulaire validé"
    en_cours_instruction.boolean = True

    def get_docs_complementaires_manquants(self):
        """ Renvoyer les documents complémentaires dont le fichier n'est pas renseigné """
        return self.documentscomplementaires.filter(document_attache='')

    def get_type_manif(self):
        """
        Renvoyer l'attribut qui définit le type de manifestation

        :rtype: str
        """
        for name in ['dnm', 'dnmc', 'dcnm', 'dcnmc', 'dvtm', 'avtm', 'avtmcir']:
            try:
                getattr(self, name)
                return name
            except (AttributeError, ObjectDoesNotExist):
                pass
        return None
    get_type_manif.short_description = "Type"

    def get_cerfa(self):
        """ Renvoyer l'objet manifestation spécialisé  """
        name = self.get_type_manif()
        if name:
            return getattr(self, name)
        return self

    @property
    def cerfa(self):
        """ Renvoyer l'objet manifestation spécialisé  """
        name = self.get_type_manif()
        if name:
            return getattr(self, name)
        return self

    def liste_instructeurs(self):
        try:
            prefecture = self.get_prefecture_concernee()
            if hasattr(self, 'dvtm') or hasattr(self, 'avtm') or hasattr(self, 'avtmcir'):
                # Instruction par la préfecture
                recipients = [agent.user for agent in prefecture.get_instructeurs()]
                recipients.append(prefecture)
            elif not self.villes_traversees.all():
                # Instruction par la mairie
                commune = self.ville_depart
                recipients = [agent.user for agent in commune.get_mairieagents()]
                recipients.append(commune)
            else:
                # Instruction par la préfecture
                recipients = [agent.user for agent in prefecture.get_instructeurs()]
                recipients.append(prefecture)
            # Inclure le dernier instructeur intervenu sur le dossier (cas arrondissement différent)
            if hasattr(self, 'instruction') and self.instruction.referent:
                if self.instruction.referent not in recipients:
                    recipients.append(self.instruction.referent)
        except Prefecture.DoesNotExist:
            recipients = []
        return recipients

    def notifier_creation(self):
        """ Enregistrer l'action de création """
        from messagerie.models import Message
        data = {
            "manif": self,
            "url": reverse("evenements:manif_url", kwargs={'pk': self.pk}),
            "structure": self.structure,
            "user": self.structure.organisateur.user,
        }
        contenu = render_to_string('notifications/mail/message_organisateur_creation_manif.txt', data)
        Message.objects.creer_et_envoyer('tracabilite', None, [self.structure.organisateur.user],
                                         'Création du dossier de manifestation',
                                         contenu, manifestation_liee=self, objet_lie_nature="dossier")

    def ajouter_document(self, nom_piece_jointe):
        """ Avertir et enregistrer l'action """
        expediteur = self.get_organisateur()
        destinatairesaction = self.liste_instructeurs()
        destinataires = self.instruction.get_all_agents()
        from messagerie.models.message import Message
        titre = "Pièce jointe ajoutée"
        contenu = titre + ' pour la manifestation ' + self.nom + " : " + nom_piece_jointe
        Message.objects.creer_et_envoyer('info_suivi', expediteur, destinataires, titre, contenu, manifestation_liee=self, objet_lie_nature="dossier")
        Message.objects.creer_et_envoyer('action', expediteur, destinatairesaction, titre, contenu, manifestation_liee=self, objet_lie_nature="dossier")

    # Meta
    class Meta:
        verbose_name = "manifestation"
        verbose_name_plural = "manifestations"
        default_related_name = "manifs"
        app_label = "evenements"


class ManifRelatedModel(models.Model):
    """
    Tous les modèles liés à manifestation peuvent hériter de cette classe
    afin de bénéficier de méthodes liées aux informations de manifestation
    """

    # Getter
    def get_manif(self):
        """ Renvoyer la manifestation de l'objet """
        return self.manif

    def get_instance(self):
        """ Renvoyer l'instance liée à la manifestation liée à l'objet """
        return self.get_manif().get_instance() or Instance.objects.get_master()

    # Meta
    class Meta:
        abstract = True
