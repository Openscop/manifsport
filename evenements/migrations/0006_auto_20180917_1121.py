# Generated by Django 2.0.7 on 2018-09-17 09:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('evenements', '0005_auto_20180917_1106'),
    ]

    operations = [
        migrations.AlterField(
            model_name='avtm',
            name='nb_vehicules_accompagnement',
            field=models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement"),
        ),
        migrations.AlterField(
            model_name='avtm',
            name='vehicules',
            field=models.TextField(blank=True, null=True, verbose_name='type et nombre de véhicules'),
        ),
        migrations.AlterField(
            model_name='avtmcir',
            name='type_support',
            field=models.CharField(blank=True, choices=[('s', 'circuit'), ('g', 'terrain'), ('r', 'parcours')], max_length=1, null=True, verbose_name='type de support'),
        ),
        migrations.AlterField(
            model_name='avtmcir',
            name='vehicules',
            field=models.TextField(blank=True, null=True, verbose_name='type et nombre de véhicules'),
        ),
        migrations.AlterField(
            model_name='dcnm',
            name='nb_signaleurs',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs'),
        ),
        migrations.AlterField(
            model_name='dcnm',
            name='nb_vehicules_accompagnement',
            field=models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement"),
        ),
        migrations.AlterField(
            model_name='dcnmc',
            name='nb_signaleurs',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs'),
        ),
        migrations.AlterField(
            model_name='dcnmc',
            name='nb_vehicules_accompagnement',
            field=models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement"),
        ),
        migrations.AlterField(
            model_name='dnm',
            name='nb_signaleurs',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs'),
        ),
        migrations.AlterField(
            model_name='dnm',
            name='nb_vehicules_accompagnement',
            field=models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement"),
        ),
        migrations.AlterField(
            model_name='dnmc',
            name='nb_signaleurs',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de signaleurs'),
        ),
        migrations.AlterField(
            model_name='dnmc',
            name='nb_vehicules_accompagnement',
            field=models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement"),
        ),
        migrations.AlterField(
            model_name='dvtm',
            name='nb_personnes_pts_rassemblement',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='nombre de personnes aux points de rassemblement'),
        ),
        migrations.AlterField(
            model_name='dvtm',
            name='nb_vehicules_accompagnement',
            field=models.PositiveIntegerField(blank=True, help_text="nombre de véhicules d'accompagnement", null=True, verbose_name="nombre de véhicules d'accompagnement"),
        ),
        migrations.AlterField(
            model_name='dvtm',
            name='vehicules',
            field=models.TextField(blank=True, null=True, verbose_name='type et nombre de véhicules'),
        ),
    ]
