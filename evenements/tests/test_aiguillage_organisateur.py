import re, os

from django.test import TestCase
from django.contrib.auth.hashers import make_password as make
from datetime import timedelta, datetime

from allauth.account.models import EmailAddress
from post_office.models import EmailTemplate, Email

from core.factories import UserFactory
from core.models import User, Instance
from administration.factories import InstructeurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory, OrganisateurFactory
from sports.factories import ActiviteFactory
from messagerie.models import Message


class Aiguillage_OrganisateurTests(TestCase):
    """
    Test du circuit organisateur de la nouvelle version
        workflow_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========= Aiguillage organisateur =========')
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        cls.other_dep = DepartementFactory.create(name='43',
                                                  instance__name="instance de test",
                                                  instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                                  instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur',
                                              password=make(123),
                                              default_instance=cls.dep.instance,
                                              email='orga@test.fr')
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.instructeur = UserFactory.create(username='instructeur',
                                             password=make(123),
                                             default_instance=cls.dep.instance,
                                             email='inst@test.fr')
        InstructeurFactory.create(user=cls.instructeur, prefecture=prefecture)

        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.activ = ActiviteFactory.create()
        for user in User.objects.all():
            user.optionuser.notification_mail = True
            user.optionuser.action_mail = True
            user.optionuser.tracabilite_mail = True
            user.optionuser.save()

        EmailTemplate.objects.create(name='new_msg')

        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'liste_signaleurs',
                     'participants', 'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire',
                     'topo_securite', 'dossier_tech_cycl', 'plan_masse', 'cartographie'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test aiguillage Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'liste_signaleurs',
                     'participants', 'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire',
                     'topo_securite', 'dossier_tech_cycl', 'plan_masse', 'cartographie'):
            os.remove(file+".txt")
        super(Aiguillage_OrganisateurTests, cls).tearDownClass()

    def nouvelle_manif(self, reponse):
        """
        Appel de la vue d'aiguillage
        :param reponse: réponse précédente
        :return: réponse suivante
        """
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer ou demander ', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Recherche de formulaire', count=2)
        self.assertContains(reponse, 'Rechercher le formulaire')
        self.assertContains(reponse, 'Accéder au formulaire')

        return reponse

    def donnees_aiguillage(self, reponse, formulaire):
        """
        Remplissage des champs pour aiguiller sur la bonne manifestation spécialisée
        :param reponse: reponse précédente
        :param formulaire: cerfa demandé
        :return: data à poster
        """
        data = {}
        # Informations générales
        data['csrf_token'] = reponse.context['csrf_token']

        data['discipline'] = self.activ.discipline.pk
        data['activite'] = self.activ.pk
        data['nb_participants'] = 200
        data['ville_depart'] = self.commune.pk
        data['departement'] = self.commune.arrondissement.departement.pk
        data['emprise'] = 3
        data['formulaire'] = formulaire
        return data

    def donnees_formulaire(self, reponse):
        """
        Récupération des données du formulaire concernant le formset de contact qui a des données à restituer
        et remplissage des champs communs
        :param reponse: reponse précédente
        :return: data à poster
        """
        data = {}
        # Informations générales
        data['csrf_token'] = reponse.context['csrf_token']
        data['nom'] = 'La grosse course'
        data['description'] = 'une grosse course'
        data['descriptions_parcours'] = 'parcours long'
        data['nb_spectateurs'] = 15
        data['nb_organisateurs'] = 15
        data['villes_traversees'] = self.autrecommune.pk
        data['nom_contact'] = 'durand'
        data['prenom_contact'] = 'joseph'
        data['tel_contact'] = '0605555555'
        # Le champ n'est pas rempli avec le kwarg de la vue, alors on le passe
        data['departement_depart'] = self.dep.pk
        """
        print('#' * 30)
        for i in sorted(data.keys()):
            print(i, '\t:', data[i])
        print('#' * 30)
        """
        return data

    def joindre_fichiers(self, reponse, cas='d'):
        """
        Ajout des fichiers nécessaires à la manifestation
        :param reponse: reponse précédente
        :param cas: suivant cerfa considéré
        :return: reponse suivante
        """
        # appel de la vue pour joindre les fichiers
        joindre = re.search('id="(?P<id>([^"]+)).+\\n.+Pièces jointes', reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(joindre, 'group'))
        self.assertEqual(joindre.group('id'), 'pieceajoindreajax')
        url_script = re.search(joindre.group('id') + "'\)\.click.+\\n.+get\(\"(?P<url>(/[^\"]+))", reponse.content.decode('utf-8'))
        self.assertTrue(hasattr(url_script, 'group'))
        if hasattr(url_script, 'group'):
            print(url_script.group('url'))

        # Ajout des fichiers nécessaires
        with open('/tmp/reglement_manifestation.txt') as file1:
            self.client.post(url_script.group('url'), {'reglement_manifestation': file1}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/engagement_organisateur.txt') as file2:
            self.client.post(url_script.group('url'), {'engagement_organisateur': file2}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/disposition_securite.txt') as file3:
            self.client.post(url_script.group('url'), {'disposition_securite': file3}, follow=True,
                             HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/cartographie.txt') as file12:
            reponse = self.client.post(url_script.group('url'), {'cartographie': file12}, follow=True,
                                       HTTP_HOST='127.0.0.1:8000')
        if cas == 'd':
            with open('/tmp/itineraire_horaire.txt') as file7:
                reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file7}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        if cas == 'c':
            with open('/tmp/dossier_tech_cycl.txt') as file6:
                self.client.post(url_script.group('url'), {'dossier_tech_cycl': file6}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/itineraire_horaire.txt') as file7:
                reponse = self.client.post(url_script.group('url'), {'itineraire_horaire': file7}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        if cas == 'm':
            with open('/tmp/itineraire_horaire.txt') as file7:
                self.client.post(url_script.group('url'), {'itineraire_horaire': file7}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/certificat_organisateur_tech.txt') as file8:
                self.client.post(url_script.group('url'), {'certificat_organisateur_tech': file8}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/commissaires.txt') as file9:
                self.client.post(url_script.group('url'), {'commissaires': file9}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/participants.txt') as file10:
                reponse = self.client.post(url_script.group('url'), {'participants': file10}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        if cas == 's':
            with open('/tmp/certificat_organisateur_tech.txt') as file8:
                self.client.post(url_script.group('url'), {'certificat_organisateur_tech': file8}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/participants.txt') as file10:
                self.client.post(url_script.group('url'), {'participants': file10}, follow=True,
                                 HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/plan_masse.txt') as file11:
                reponse = self.client.post(url_script.group('url'), {'plan_masse': file11}, follow=True,
                                           HTTP_HOST='127.0.0.1:8000')
        return reponse

    def declarer_manif(self, reponse):
        """
        Soumettre la manifestation à l'instructeur
        :param reponse: reponse précédente
        :return: reponse suivante
        """
        # appel de la vue pour déclarer la manifestation
        declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Envoyer la demande', reponse.content.decode('utf-8'))
        if hasattr(declar, 'group'):
            print(declar.group('url'))
            reponse = self.client.get(declar.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Demander l\'instruction', count=2)
        # Soumettre la déclaration
        reponse = self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        return reponse

    def verification_instructeur(self):
        """
        Vérification dans la dashboard instructeur de l'affichage de la manifestation crée
        :return:
        """
        # Connexion avec l'instructeur
        self.assertTrue(self.client.login(username=self.instructeur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/instructions/tableaudebord/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        nb_bloc = re.search('test_nb_atraiter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('1', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_encours">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_auto">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        nb_bloc = re.search('test_nb_inter">(?P<nb>(\d))</', reponse.content.decode('utf-8'), re.DOTALL)
        self.assertTrue(hasattr(nb_bloc, 'group'))
        self.assertEqual('0', nb_bloc.group('nb'))
        reponse = self.client.get('/instructions/tableaudebord/list/?filtre_etat=atraiter', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'La grosse course')
        self.assertContains(reponse, 'table_afaire', count=1)
        # f = open('/var/log/manifsport/test_output.html', 'w')
        # f.write(reponse.content.decode('utf-8'))
        # f.close()

    def test_1_Inscription(self):
        """
        Test de l'inscription d'un organisateur
        """
        print('**** test Inscription ****')
        reponse = self.client.post('/inscription/organisateur',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': 'jandup',
                                    'email': 'ne-pas-repondre-42@manifestationsportive.fr',
                                    'password1': 'azeaz5646dfzdsf',
                                    'password2': 'azeaz5646dfzdsf',
                                    'cgu': True,
                                    'structure_name': 'MonAssoc',
                                    'type_of_structure': self.structure.pk,
                                    'address': '12 rue du repos éternel',
                                    'departement': self.dep.pk,
                                    'commune': self.commune.pk,
                                    'phone': '04 77 74 33 55',
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Vérifiez votre adresse email', count=2)
        email = EmailAddress.objects.last()
        email.verified = True
        email.save()
        user = User.objects.get(username='jandup')
        user.is_active = True
        user.save()
        reponse = self.client.post('/accounts/login/',
                                   {'login': 'jandup',
                                    'password': 'azeaz5646dfzdsf',
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Connexion avec jandup réussie', count=1)

    def test_2_Dnm(self):
        """
        Test d'une Dnm
        """
        print('**** test Dnm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue tableau de bord nouvelle apps
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dnm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=29)
        data['date_fin'] = datetime.now() + timedelta(days=29, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)
        # Vérifier les emails envoyés
        outbox = Email.objects.order_by('pk').values()
        messages = Message.objects.all()
        self.assertEqual(len(outbox), 2)
        self.assertEqual(outbox[0]['to'], [self.organisateur.email])
        self.assertIn("Connexion", messages[0].corps)
        self.assertIn("créer un nouveau dossier", messages[1].corps)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=31)
        data['date_fin'] = datetime.now() + timedelta(days=31, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'itinéraire horaire', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)



        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        print('bou')
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        # Test de l'export
        response = self.client.get("/evenement/"+action.group('url').split('/')[2]+"/export/organisateur/", HTTP_HOST='127.0.0.1:8000')
        self.assertEqual(response.status_code, 200)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def test_3_Dnmc(self):
        """
        Test d'une Dnmc
        """
        print('**** test Dnmc ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue tableau de bord nouvelle apps
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dnmc')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=29)
        data['date_fin'] = datetime.now() + timedelta(days=29, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=31)
        data['date_fin'] = datetime.now() + timedelta(days=31, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'itinéraire horaire', count=1)
        self.assertContains(reponse, 'dossier technique cyclisme', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)

        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        reponse = self.joindre_fichiers(reponse, 'c')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def test_4_Dcnm(self):
        """
        Test d'une Dcnm
        """
        print('**** test Dcnm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dcnm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content.decode('utf-8'))
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'itinéraire horaire', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)

        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def test_5_Dcnm_2dep(self):
        """
        Test d'une Dcnm qui traverse deux departements
        """
        print('**** test Dcnm sur deux départements ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dcnm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['departements_traverses'] = (self.other_dep.pk,)
        data['date_debut'] = datetime.now() + timedelta(days=89)
        data['date_fin'] = datetime.now() + timedelta(days=89, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=91)
        data['date_fin'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'itinéraire horaire', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)

        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def test_6_Dcnmc(self):
        """
        Test d'une Dcnmc
        """
        print('**** test Dcnmc ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dcnmc')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['nb_signaleurs'] = 3
        data['nb_signaleurs_fixes'] = 1
        data['nb_signaleurs_autos'] = 1
        data['nb_signaleurs_motos'] = 1
        data['nb_vehicules_accompagnement'] = 0
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        # print(reponse.content.decode('utf-8'))
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'itinéraire horaire', count=1)
        self.assertContains(reponse, 'dossier technique cyclisme', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)

        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        reponse = self.joindre_fichiers(reponse, 'c')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def test_7_Dvtm(self):
        """
        Test d'une Dvtm
        """
        print('**** test Dvtm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Dvtm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['vehicules'] = 2
        data['nb_vehicules_accompagnement'] = 1
        data['nb_personnes_pts_rassemblement'] = 2
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        self.assertContains(reponse, 'itinéraire horaire', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)

        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def test_8_Avtm(self):
        """
        Test d'une Avtm
        """
        print('**** test Avtm ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Avtm')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire de création de manifestation
        # avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['vehicules'] = 2
        data['nb_vehicules_accompagnement'] = 1
        data['date_debut'] = datetime.now() + timedelta(days=89)
        data['date_fin'] = datetime.now() + timedelta(days=89, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=91)
        data['date_fin'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)
        self.assertContains(reponse, 'liste des postes commissaires', count=1)
        self.assertContains(reponse, 'attestation organisateur technique', count=1)
        self.assertContains(reponse, 'itinéraire horaire', count=1)
        self.assertNotContains(reponse, 'liste complète des participants')

        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        reponse = self.joindre_fichiers(reponse, 'm')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def test_9_Avtmcir(self):
        """
        Test d'une Avtmcir
        """
        print('**** test Avtmcir ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/tableau-de-bord-organisateur/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (create)
        reponse = self.nouvelle_manif(reponse)

        # Remplissage du formulaire d'aiguillage
        data1 = self.donnees_aiguillage(reponse, 'Avtmcir')
        reponse = self.client.post('/evenement/creation/', data1, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')
        self.assertContains(reponse, 'Données initiales')
        self.assertContains(reponse, 'Détail de la manifestation')

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['type_support'] = 's'
        data['vehicules'] = 2
        data['date_debut'] = datetime.now() + timedelta(days=59)
        data['date_fin'] = datetime.now() + timedelta(days=59, hours=8)
        url = reponse.request['PATH_INFO'] + '?' + reponse.request['QUERY_STRING']
        print(url)
        reponse = self.client.post(url, data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive')

        # Remplissage du formulaire avec une date correcte
        data['date_debut'] = datetime.now() + timedelta(days=61)
        data['date_fin'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertNotContains(reponse, 'manifestation est incomplet')
        self.assertContains(reponse, 'Cartographie non renseignée', count=1)
        self.assertContains(reponse, 'réglement de la manifestation', count=1)
        self.assertContains(reponse, 'déclaration et engagement de l', count=1)
        self.assertContains(reponse, 'dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation et dans l'onglet carto : si manquant count = 3
        self.assertContains(reponse, 'cartographie de parcours', count=3)
        self.assertContains(reponse, 'attestation organisateur technique', count=1)
        self.assertNotContains(reponse, 'liste complète des participants')
        self.assertContains(reponse, 'plan de masse', count=1)

        url_carto = re.search('url_carto_update = "(?P<url>(/[^"]+))";', reponse.content.decode('utf-8'))
        print(url_carto.group('url'))
        reponse = self.client.post(url_carto.group('url'), {'descriptions_parcours': '10 km'}, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Cartographie non renseignée')

        reponse = self.joindre_fichiers(reponse, 's')

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

        # print(reponse.content.decode('utf-8'))
        # f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
        # f.write(str(reponse.content.decode('utf-8')).replace('\\n',""))
        # f.close()
