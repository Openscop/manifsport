# coding: utf-8

from instructions.factories import InstructionFactory
from .base import EvenementsTestsBase
from ..factories.manif import AvtmcirFactory


class AvtmcirTests(EvenementsTestsBase):
    """ Tests Avtmcir """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation sportive motorisée.
        """
        self.instruction = InstructionFactory.create(manif=AvtmcirFactory.create())
        super().setUp()

    # Tests
    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        print('Tests Avtmcir')
        manifestation = AvtmcirFactory.build(pk=5)
        self.assertEqual(manifestation.get_absolute_url(), '/Avtmcir/5/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.delivrance_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.delivrance_titre = False
        self.manifestation.vtm_hors_circulation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.vtm_hors_circulation = False
        self.manifestation.demande_homologation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 60)
