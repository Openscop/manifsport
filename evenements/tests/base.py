# coding: utf-8
from django.test import TestCase

from administration.factories.service import CompagnieFactory, CGServiceFactory, CGDFactory


class EvenementsTestsBase(TestCase):
    """ Mixin de tests des manifestations """

    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'instruction

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive compétitive non motorisée.
        """
        self.manifestation = self.instruction.manif
        self.commune = self.manifestation.ville_depart
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.commune.get_prefecture()
        self.cg = self.departement.cg
        self.edsr = self.departement.edsr
        self.ddsp = self.departement.ddsp
        self.sdis = self.departement.sdis
        self.ggd = self.departement.ggd
        self.activite = self.manifestation.activite
        self.compagnie = CompagnieFactory.create(sdis=self.sdis)
        self.cgservice = CGServiceFactory.create(cg=self.cg)
        self.cgd = CGDFactory.create(commune=self.commune)
