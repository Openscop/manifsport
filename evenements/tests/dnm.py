# coding: utf-8
from instructions.factories import InstructionFactory
from evaluation_incidence.factories import RNREvalFactory, N2kEvalFactory, N2kSiteConfigFactory, RnrZoneConfigFactory
from ..factories.manif import DnmFactory
from .base import EvenementsTestsBase
from ..models import Manif


class DnmTests(EvenementsTestsBase):
    """ Tests Dnm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation
        sportive non motorisée soumise à déclaration.
        """
        self.instruction = InstructionFactory.create(manif=DnmFactory.create())
        super().setUp()
        # Mettre en place la config N2k avec des critères tels que requise = False
        self.n2ksiteconfig = N2kSiteConfigFactory.create()
        self.n2ksite = self.n2ksiteconfig.n2ksite
        self.n2ksite.departements.add(self.departement)
        self.n2ksiteconfig.nm_seuil_participants = 700
        self.n2ksiteconfig.nm_seuil_total = 6000
        self.n2ksiteconfig.save()
        # Mettre en place la config RNR avec des critères tels que requise = True
        self.rnrzoneconfig = RnrZoneConfigFactory.create()
        self.rnrzone = self.rnrzoneconfig.rnrzone
        self.rnrzone.departements.add(self.departement)
        self.rnrzoneconfig.seuil_participants = 700
        self.rnrzoneconfig.seuil_total = 5000
        self.rnrzoneconfig.save()


    def test_str(self):
        print('Tests Dnm')
        self.assertEqual(str(self.manifestation), self.manifestation.nom.title())

    def test_display_rnr_eval_panel(self):
        """ Tester la méthode indiquant s'il faut afficher le panneau d'évaluation RNR """
        self.assertFalse(self.manifestation.afficher_panneau_eval_rnr())
        self.manifestation.zones_rnr.add(self.rnrzone)
        self.assertTrue(self.manifestation.afficher_panneau_eval_rnr())
        # formulaire non concerné
        self.rnrzoneconfig.formulaire = ['dcnm']
        self.rnrzoneconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_rnr())
        # formulaire concerné
        self.rnrzoneconfig.formulaire = ['dcnm', 'dnm']
        self.rnrzoneconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_rnr())
        self.rnrzoneconfig.seuil_total = 6000
        self.rnrzoneconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_rnr())
        self.rnrzoneconfig.seuil_total = 0
        self.rnrzoneconfig.seuil_participants = 200
        self.rnrzoneconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_rnr())
        self.rnrzoneconfig.delete()
        self.assertFalse(self.manifestation.afficher_panneau_eval_rnr())

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.manifestation.emprise = Manif.EMPRISE['total']
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Critères nationaux
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.delivrance_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.delivrance_titre = False
        self.manifestation.vtm_hors_circulation = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.vtm_hors_circulation = False
        # Critères de site sans formulaire ciblé
        # La manif passe sur le site n2k, les critères sont respectés
        self.manifestation.sites_natura2000.add(self.n2ksite)
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Critère nombre de participants
        self.n2ksiteconfig.nm_seuil_participants = 200
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Condition Hors voies ouvertes, la manif est totalement en voies ouvertes
        self.n2ksiteconfig.nm_hors_circulation = True
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # La manif est partiellement en voies ouvertes
        self.manifestation.emprise = Manif.EMPRISE['partiel']
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # La manif est totalement hors voies ouvertes
        self.manifestation.emprise = Manif.EMPRISE['hors']
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.nm_seuil_participants = 0
        # Critère nombre total
        self.n2ksiteconfig.nm_seuil_total = 5000
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères de site avec formulaire ciblé, condition sur seuil total
        # formulaire non concerné
        self.n2ksiteconfig.nm_formulaire = ['dcnm']
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # formulaire concerné
        self.n2ksiteconfig.nm_formulaire = ['dcnm', 'dnm']
        self.n2ksiteconfig.save()
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        # Critères de site avec charte de dispense, condition sur seuil total
        self.manifestation.signature_charte_dispense_site_n2k = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.n2ksiteconfig.charte_dispense_acceptee = True
        self.n2ksiteconfig.save()
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Pas de formulaire ciblé, dispense toujours active
        self.n2ksiteconfig.nm_formulaire = []
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        # Plus de dispense, critère nombre total appliqué
        self.manifestation.signature_charte_dispense_site_n2k = False
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 30)

    def test_get_breadcrumbs_not_sent(self):
        # Créer une manif sans instruction
        manifestation = DnmFactory.create()
        self.assertEqual(len(manifestation.get_breadcrumbs()), 2)
        self.assertEqual(manifestation.get_breadcrumbs()[1][1], 0)

    def test_get_breadcrumbs(self):
        self.assertEqual(len(self.manifestation.get_breadcrumbs()), 3)
        self.assertEqual(self.manifestation.get_breadcrumbs()[1][1], 1)

    def test_get_breadcrumbs_full(self):
        RNREvalFactory.create(manif=self.manifestation)
        N2kEvalFactory.create(manif=self.manifestation)
        self.assertEqual(len(self.manifestation.get_breadcrumbs()), 5)

    def test_get_breadcrumbs_full_2(self):
        self.manifestation.zones_rnr.add(self.rnrzone)
        self.manifestation.lucratif = True
        self.assertEqual(len(self.manifestation.get_breadcrumbs()), 5)
