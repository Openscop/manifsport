# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import Avis, AvisQuerySet


class CGAvis(Avis):
    """ Avis Conseil départemental """

    # Champs
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True, related_name='cgavis', on_delete=models.CASCADE)
    concerned_services = models.ManyToManyField('administration.cgservice', verbose_name="Services concernés")
    objects = AvisQuerySet.as_manager()

    # Override
    def __str__(self):
        manifestation = self.get_manifestation()
        departure_city = manifestation.departure_city
        departement = departure_city.get_departement()
        return ' - '.join([str(manifestation), ' '.join(['CG', departement.name])])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:cg_agreement_detail', kwargs={'pk': self.pk})

    def get_cg(self):
        """ Renvoyer le Conseil départemental """
        return self.get_manifestation().departure_city.get_departement().cg

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        return self.get_cg().get_cgagents()

    def are_preavis_validated(self):
        """ Renvoyer si tous les préavis ont été rendus """
        return super(CGAvis, self).are_preavis_validated()

    # Action
    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        """ Dispatch """
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='formatted', conditions=[are_preavis_validated])
    def format(self):
        """ Mettre en forme l'avis """
        cg = self.get_cg()
        self.notify_format(agents=cg.get_cgsuperieurs(), content_object=cg)
        self.log_format(agents=self.get_agents())

    @transition(field='state', source='formatted', target='acknowledged')
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        cg = self.get_cg()
        self.notify_ack(content_object=cg)
        self.log_ack(agents=cg.get_cgsuperieurs())

    # Meta
    class Meta:
        verbose_name = "Avis CD"
        verbose_name_plural = "Avis CD"
        app_label = 'agreements'
        default_related_name = 'cgaviss'
