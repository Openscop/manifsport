# coding: utf-8
import datetime

from django.urls import reverse
from django.db import models
from django_fsm import transition

from agreements.models.avis import Avis, AvisQuerySet
from core.util.admin import set_admin_info


class DDSPAvis(Avis):
    """ Avis DDSP """

    # Champs
    avis_ptr = models.OneToOneField("agreements.avis", parent_link=True, related_name='ddspavis', on_delete=models.CASCADE)
    commissariats_concernes = models.ManyToManyField('administration.commissariat', verbose_name="Commissariats concernés")
    objects = AvisQuerySet.as_manager()

    # Override
    def __str__(self):
        manifestation = self.get_manifestation()
        departement = manifestation.departure_city.get_departement()
        return ' - '.join([str(manifestation), ' '.join(['DDSP', departement.name])])

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de l'avis """
        return reverse('agreements:ddsp_agreement_detail', kwargs={'pk': self.pk})

    def get_agents(self):
        """ Renvoyer les agents concernés par l'avis """
        return self.get_ddsp().get_ddspagents()

    def get_ddsp(self):
        """ Renvoyer la DDSP (direction dept. de sécurité publique) """
        departement = self.get_manifestation().departure_city.get_departement()
        return departement.ddsp

    @set_admin_info(short_description="Commissariats", admin_order_field='commissariats_concernes')
    def get_formatted_commissariats(self):
        """ Renvoyer les commissariats concernés """
        commissariats = self.commissariats_concernes.all()
        return "<br>".join([str(commissariat) for commissariat in commissariats])

    def are_preavis_validated(self):
        """ Renvoyer si tous les préavis ont été rendus """
        return super(DDSPAvis, self).are_preavis_validated()

    @transition(field='state', source='created', target='dispatched')
    def dispatch(self):
        """ Dispatch """
        self.log_dispatch(agents=self.get_agents())

    @transition(field='state', source='dispatched', target='acknowledged', conditions=[are_preavis_validated])
    def acknowledge(self):
        """ Rendre l'avis """
        self.reply_date = datetime.date.today()
        self.save()
        self.notify_ack(content_object=self.get_ddsp())
        self.log_ack(agents=self.get_agents())

    # Meta
    class Meta:
        verbose_name = "Avis DDSP"
        verbose_name_plural = "Avis DDSP"
        app_label = 'agreements'
        default_related_name = 'ddsp_aviss'
