# coding: utf-8
import datetime
from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.utils import timezone

from administration.models.agent import Agent
from agreements.models.abstract import AvisBase
from configuration.directory import UploadPath
from notifications.models.action import Action
from notifications.models.notification import Notification


class AvisQuerySet(models.QuerySet):
    """ Queryset par défaut pour les avis """

    def by_instance(self, request=None, instances=None):
        """
        Renvoyer les avis pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les avis pour l'utilisateur s'il est renseigné
            return self.filter(authorization__manifestation__instance_id=request.user.get_instance().pk)
        elif instances:
            # Renvoyer les avis pour les instances si elles sont renseignées
            return self.filter(authorization__manifestation__instance__in=instances)
        else:
            return self

    def to_process(self):
        """ Renvoyer les avis dont la manifestation n'est pas terminée """
        return self.filter(authorization__manifestation__end_date__gte=timezone.now())

    def finished(self):
        """ Renvoyer les avis pour lesquels l'événement prévu est terminé """
        return self.filter(authorization__manifestation__end_date__lt=timezone.now()).order_by('-authorization__manifestation__begin_date')

    def acknowledged(self):
        """
        Renvoyer les avis rendus

        L'état acknowledged indique qu'un avis est rendu.
        """
        return self.filter(state='acknowledged')

    def dispatched(self):
        """ Renvoyer les avis pour lesquels les demandes de préavis ont été envoyées  """
        return self.filter(state='dispatched')

    def to_format(self):
        """
        Renvoyer les avis qui peuvent être formatés

        Avant d'être rendu, un agent autorisé peut formater un avis
        en ajoutant des informations sensibles pour l'agent en charge
        de valider l'avis ou de ne pas trancher positivement.
        """
        return self.filter(state__in=['created', 'dispatched'])

    def to_acknowledge(self):
        """
        Renvoyer les avis qui peuvent être rendus

        Les avis qui peuvent être (re)rendus sont des avis qui sont
        soit à l'état formaté (donc prêts à soumettre à l'agent probateur),
        soit à l'état rendu (dans ce cas, on peut à nouveau rendre l'avis.
        """
        return self.filter(state__in=['formatted', 'acknowledged'])

    def to_dispatch(self):
        """
        Renvoyer les avis qui peuvent être dispatchés

        On peut redispatcher un avis rendu, ou qui a déjà été dispatché (rappel aux agents)
        """
        return self.filter(state__in=['acknowledged', 'dispatched'])


class Avis(AvisBase):
    """
    Avis (classe parent commune aux autres types d'avis) (9 champs)

    Champs hérités : state, request_date, reply_date, favorable, prescriptions
    """

    # Champs
    authorization = models.ForeignKey('authorizations.manifestationautorisation', verbose_name="Autorisation", on_delete=models.CASCADE)
    attached_document = models.FileField(max_length=512, upload_to=UploadPath('avis'), blank=True, null=True, verbose_name="Pièce jointe")
    objects = AvisQuerySet.as_manager()

    # Overrides
    def __str__(self):
        return self.authorization.manifestation.name

    # Getter
    def get_deadline(self):
        """ Renvoyer la date maximum de rendu de l'avis selon le type d'avis """
        delays = {'federationavis': timedelta(weeks=4)}
        default_delay = timedelta(weeks=3)
        for related_field in delays:
            try:
                getattr(self, related_field)
                return self.request_date + delays[related_field]
            except ObjectDoesNotExist:
                pass
        return self.request_date + default_delay

    def get_days_left_before_deadline(self):
        """ Renvoyer le nombre de jours restant avant la deadline d'instruction """
        return self.get_deadline() - datetime.date.today()

    def delay_exceeded(self):
        """ Renvoyer si le délai légal d'instruction a expiré """
        return datetime.date.today() > self.get_deadline()

    def get_preavis(self):
        """ Renvoyer les préavis de l'avis """
        return self.preavis.all()

    def get_validated_preavis(self):
        """ Renvoyer les préavis validés/rendus """
        return self.preavis.filter(state='acknowledged')

    def are_preavis_validated(self):
        """ Renvoyer si tous les préavis en cours sont rendus """
        validated = self.get_validated_preavis().count()
        return validated == self.preavis.all().count()

    def get_preavis_count(self):
        """ Renvoyer le nombre de préavis générés """
        return self.preavis.all().count()

    def get_pending_preavis_count(self):
        """ Renvoyer le nombre de préavis non rendus/validés """
        return self.get_preavis_count() - self.get_validated_preavis().count()

    def get_manifestation(self):
        """ Renvoyer l'événement sportif pour cet avis """
        return self.authorization.manifestation

    def get_instance(self):
        """ Renvoyer l'instance du département de la manifestation de l'autorisation """
        return self.get_manifestation().get_instance()

    # Action
    def log_ack(self, agents):
        """
        Consigner l'événement lorsque l'avis est rendu

        :param agents: agents/utilisateurs qui recevront le log
        """
        recipients = Agent.users_from_agents(agents)
        Action.objects.log(recipients, "avis rendu", self.get_manifestation())

    def log_dispatch(self, agents):
        """
        Consigner l'événement dispatch : les demandes de préavis sont envoyées

        :param agents: agents/utilisateurs qui recevront le log
        """
        recipients = Agent.users_from_agents(agents)
        Action.objects.log(recipients, "demandes de pré-avis envoyées", self.get_manifestation())

    def log_format(self, agents):
        """
        Consigner l'événement lorsque l'avis est formaté/mis en forme

        :param agents: agents/utilisateurs qui recevront le log
        """
        recipients = Agent.users_from_agents(agents)
        Action.objects.log(recipients, "avis mis en forme", self.get_manifestation())

    def log_resend(self, recipient):
        """
        Consigner l'événement lorsque qu'une demande d'avis est relancée

        Les agents notifiés sont les instructeurs attachés à la préfecture de l'autorisation
        :param recipient: TODO: Documenter
        """
        prefecture = self.authorization.get_concerned_prefecture()
        recipients = [instructeur.user for instructeur in prefecture.get_instructeurs()]
        Action.objects.log(recipients, "demande d'avis relancée : {recipient}".format(recipient=recipient), self.get_manifestation())

    def notify_creation(self, agents, non_agent_recipient=None):
        """
        Consigner l'événement et envoyer un mail aux agents lorsque l'avis est créé

        :param agents: agents qui reçoivent le mail et sont notifiés
        :param non_agent_recipient: un objet avec un champ email, ex. Préfecture etc. qui sera notifié par mail
        """
        try:
            prefecture = self.authorization.get_concerned_prefecture()
            recipients = Agent.users_from_agents(agents) + [non_agent_recipient]
            Notification.objects.notify_and_mail(recipients, "avis requis", prefecture, self.get_manifestation())
        except ObjectDoesNotExist:
            raise

    def notify_ack(self, content_object):
        """
        Consigner l'événement et envoyer un mail aux instructeurs lorsqu'un avis est rendu

        Envoie également un mail à la préfecture (institutional)
        :param content_object: objet d'intérêt pour la notification
        """
        try:
            prefecture = self.authorization.get_concerned_prefecture()
            if not self.authorization.manifestation.crossed_cities.all() and hasattr(self.authorization.manifestation, 'autorisationnm'):
                # Instruction par la mairie
                commune = self.authorization.manifestation.departure_city
                recipients = list(commune.get_mairieagents()) + [prefecture]
            else:
                # Instruction par la préfecture
                recipients = Agent.users_from_agents(prefecture.get_instructeurs()) + [prefecture]
            Notification.objects.notify_and_mail(recipients, "avis rendu", content_object, self.get_manifestation())
        except ObjectDoesNotExist:
            pass

    def notify_format(self, agents, content_object):
        """ Consigner et envoyer un mail lorsque l'avis est formaté """
        recipients = Agent.users_from_agents(agents)
        Notification.objects.notify_and_mail(recipients, "avis mis en forme", content_object, self.get_manifestation())

    def notify_publication(self, agents, non_agent_recipient=None):
        """ Consigner et envoyer un mail lorsque l'arrêté (bylaw) est publié """
        prefecture = self.authorization.get_concerned_prefecture()
        recipients = Agent.users_from_agents(agents) + [non_agent_recipient]
        Notification.objects.notify_and_mail(recipients, "arrêté publié", prefecture, self.get_manifestation())

    # Meta
    class Meta:
        verbose_name = "Avis"
        verbose_name_plural = "Avis"
        ordering = ["authorization__manifestation__begin_date"]
        app_label = 'agreements'
        default_related_name = 'avis'
