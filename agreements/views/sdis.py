# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from administration.models.service import Compagnie
from agreements.views.base import BaseAcknowledgeView, BaseResendView, BaseDispatchView
from core.util.permissions import require_role
from ..forms import *
from ..models import *


class SDISAvisDetail(DetailView):
    """ Vue de détail des avis SDIS """

    # Configuration
    model = SDISAvis

    # Overrides
    @method_decorator(require_role(['sdisagent', 'codisagent', 'cisagent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue aux pompiers uniquement """
        return super(SDISAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class SDISAvisDispatch(BaseDispatchView):
    """ Vue d'envoi des demandes de préavis """

    # Configuration
    model = SDISAvis
    form_class = SDISAvisDispatchForm

    # Overrides
    @method_decorator(require_role('sdisagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue aux agents SDIS uniquement """
        return super(SDISAvisDispatch, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # Filtrer les services dispo à ceux des départements traversés
        form.fields['compagnies_concernees'].queryset = Compagnie.objects.filter(
            sdis__departement__in=self.object.get_manifestation().get_crossed_departements())
        return form


class SDISAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu des avis SDIS """

    # Configuration
    model = SDISAvis

    # Overrides
    @method_decorator(require_role('sdisagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue aux agents SDIS uniquement """
        return super(SDISAvisAcknowledge, self).dispatch(*args, **kwargs)


class SDISAvisResend(BaseResendView):
    """ Vue de renvoi des demandes """

    # Configuration
    model = SDISAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.authorization.manifestation.departure_city.get_departement().sdis.sdisagents.all())
        instance.log_resend(recipient=instance.get_sdis().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
