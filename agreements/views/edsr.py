# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from administration.models.service import CGD
from agreements.views.base import BaseAcknowledgeView, BaseResendView, BaseDispatchView, BaseFormatView
from core.util.permissions import require_role
from ..forms import *
from ..models import *


class EDSRAvisDetail(DetailView):
    """ Vue de détail d'un avis EDSR """

    # Configuration
    model = EDSRAvis

    # Overrides
    @method_decorator(require_role(['edsragent', 'ggdagent', 'brigadeagent']))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour l'agent EDSR uniquement """
        return super(EDSRAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class EDSRAvisDispatch(BaseDispatchView):
    """ Vue d'envoi des demandes pour l'avis EDSR """

    # Configuration
    model = EDSRAvis
    form_class = EDSRAvisDispatchForm

    # Overrides
    @method_decorator(require_role('edsragent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents EDSR uniquement """
        return super(EDSRAvisDispatch, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # Filtrer les services dispo à ceux des départements traversés
        form.fields['concerned_cgd'].queryset = CGD.objects.filter(
            commune__arrondissement__departement__in=self.object.get_manifestation().get_crossed_departements())
        return form


class EDSRAvisFormat(BaseFormatView):
    """ Vue de mise en forme pour les avis EDSR """

    # Configuration
    model = EDSRAvis

    # Overrides
    @method_decorator(require_role('edsragent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents EDSR uniquement """
        return super(EDSRAvisFormat, self).dispatch(*args, **kwargs)


class EDSRAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu d'avis EDSR """

    # Configuration
    model = EDSRAvis

    # Overrides
    @method_decorator(require_role('ggdagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour les agents EDSR uniquement """
        return super(EDSRAvisAcknowledge, self).dispatch(*args, **kwargs)


class EDSRAvisResend(BaseResendView):
    """ Vue de renvoi des avis """

    # Configuration
    model = EDSRAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.authorization.manifestation.departure_city.get_departement().edsr.edsragents.all())
        instance.log_resend(recipient=instance.get_edsr().__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
