# coding: utf-8
from django.contrib import messages
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView

from agreements.views.base import BaseAcknowledgeView, BaseResendView
from core.util.permissions import require_role
from ..models import *


class MairieAvisDetail(DetailView):
    """ Vue de détail de l'avis mairie """

    # Configuration
    model = MairieAvis

    # Overrides
    @method_decorator(require_role('mairieagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour l'agent mairie uniquement """
        return super(MairieAvisDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Affichage des pictos de la manifestation
        type_name = self.object.authorization.manifestation.get_manifestation_type_name()
        dict = {'autorisationnm': 'anm', 'declarationnm': 'dnm', 'motorizedconcentration': 'avtmc', 'motorizedevent': 'avtm', 'motorizedrace': 'avtmcir'}
        context['type'] = dict[type_name]
        return context


class MairieAvisAcknowledge(BaseAcknowledgeView):
    """ Vue de rendu d'avis mairie """

    # Configuration
    model = MairieAvis

    # Overrides
    @method_decorator(require_role('mairieagent'))
    def dispatch(self, *args, **kwargs):
        """ Autoriser la vue pour l'agent mairie uniquement """
        return super(MairieAvisAcknowledge, self).dispatch(*args, **kwargs)


class MairieAvisResend(BaseResendView):
    """ Vue de renvoi des avis """

    # Configuration
    model = MairieAvis

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Vue pour la méthode GET """
        instance = self.get_object()
        instance.notify_creation(agents=instance.commune.mairieagents.all())
        instance.log_resend(recipient=instance.commune.__str__())
        messages.success(request, "Demande d'avis relancée avec succès")
        return redirect(reverse('authorizations:authorization_detail', kwargs={'pk': instance.authorization.id}))
