# coding: utf-8

from django.urls import reverse

from agreements.tests.base import AvisTestsBase
from core.models.instance import Instance
from ..factories import *


class EDSRAvisTests(AvisTestsBase):
    """ Tests des avis EDSR """

    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        super().setUp()

    def test_access_url(self):
        """ Tester l'URL d'accès """
        avis = EDSRAvisFactory.create(authorization=self.authorization)
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:edsr_agreement_detail', kwargs={'pk': avis.pk}))

    def test_preavis(self):
        """ Tester l'état des préavis """
        avis = EDSRAvisFactory.create(authorization=self.authorization)
        self.assertTrue(avis.are_preavis_validated())  # Aucun préavis n'est émis

    def test_workflow_edsr(self):
        """
        Tester la création des avis EDSR pour l'autorisation

        Nécessite de configurer l'instance pour utiliser un workflow EDSR.
        """
        self.instance.workflow_ggd = Instance.WF_EDSR  # Utiliser le seul workflow GGD autorisant les avis EDSR
        self.instance.save()
        self.authorization.edsr_concerned = True  # Déclencher la création d'un avis EDSR
        self.authorization.save()
        self.assertEqual(self.authorization.get_avis_count(), 2)  # Avis Fédération (toujours) + avis EDSR

    def test_workflow_non_edsr(self):
        """ Tester que l'avis EDSR n'existe pas si le workflow ne le permet pas """
        self.instance.workflow_ggd = Instance.WF_GGD_EDSR  # Utiliser un workflow GGD sans avis EDSR
        self.instance.save()
        self.authorization.edsr_concerned = True  # Demander la création d'un avis EDSR, devrait échouer
        self.authorization.save()
        self.assertEqual(self.authorization.get_avis_count(), 1)  # Avis Fédération (toujours). Aucun avis EDSR
