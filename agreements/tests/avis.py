# coding: utf-8
from datetime import date

from django.test import TestCase

from core.models.instance import Instance
from sports.factories import FederationFactory
from ..factories import *


class AvisTests(TestCase):
    """ Tests de la base des avis """

    def setUp(self):
        """ Configuration """
        self.avis = AvisFactory.create()
        FederationFactory.create()

    def test_deadlines(self):
        """ Tester la date limite pour l'avis """
        expected_deadline = date.today() + Instance.get_avis_delay()
        self.assertEqual(self.avis.get_deadline(), expected_deadline)
        self.assertEqual(self.avis.delay_exceeded(), False)

    def test_exceeded_deadlines(self):
        expirable_date = date.today() - Instance.get_avis_delay(extra_days=2)
        self.avis.request_date = expirable_date
        self.assertEqual(self.avis.delay_exceeded(), True)

    def test_preavis(self):
        """ Tester les méthodes concernant les préavis """
        self.assertEqual(self.avis.get_preavis_count(), 0)  # Aucun préavis par défaut
        self.assertEqual(self.avis.get_pending_preavis_count(), 0)  # Donc aucun préavis en attente
        self.assertTrue(self.avis.are_preavis_validated())  # Et tous les préavis sont rendus
