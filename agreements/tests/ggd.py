# coding: utf-8

from django.urls import reverse

from agreements.tests.base import AvisTestsBase
from core.models.instance import Instance
from ..factories import *


class GGDAvisTests(AvisTestsBase):
    """ Tests des avis GGD (workflow 7) """

    # Configuration
    def setUp(self):
        super().setUp()

    # Tests
    def test_get_absolute_url(self):
        """ Tester les URLs d'accès """
        avis = GGDAvisFactory.create(authorization=self.authorization, concerned_edsr=self.departement.edsr)
        self.assertEqual(avis.get_absolute_url(), reverse('agreements:ggd_agreement_detail', kwargs={'pk': avis.pk}))

    def test_preavis_edsr(self):
        """ Tester les préavis EDSR (n'existent que dans le workflow GGD avec agent local EDSR) """
        self.instance.workflow_ggd = Instance.WF_GGD_SUBEDSR  # Choisir le workflow où des préavis EDSR existent
        self.instance.save()
        avis = GGDAvisFactory.create(authorization=self.authorization, concerned_edsr=self.departement.edsr)  # Un préavis EDSR sera créé automatiquement

        # Vérifier qu'il existe bien un préavis EDSR
        self.assertEqual(avis.get_preavis_count(), 1)  # préavis EDSR normalement
        self.assertIsNotNone(avis.preavis.first().preavisedsr)

    def test_preavis_other(self):
        """ Tester les préavis (normalement aucun par défaut dans les workflows non agent local EDSR) """
        self.instance.workflow_ggd = Instance.WF_GGD_EDSR  # Choisir le workflow GGD avec gestion par agent EDSR
        self.instance.save()
        avis = GGDAvisFactory.create(authorization=self.authorization, concerned_edsr=self.departement.edsr)  # Aucun préavis créé normalement

        # Vérifier qu'il n'existe pas un préavis EDSR
        self.assertEqual(avis.get_preavis_count(), 0)  # Aucun préavis par défaut en principe dans ce workflow

    def test_workflow_edsr(self):
        """ Tester l'état des avis du workflow (normalement autant d'avis dans chaque workflow) """
        self.instance.workflow_ggd = Instance.WF_GGD_EDSR  # Choisir le workflow GGD avec gestion par agent EDSR
        self.instance.save()
        self.authorization.ggd_concerned = True  # On teste la création automatique de l'avis GGD
        self.authorization.save()
        self.assertEqual(self.authorization.get_avis_count(), 2)  # avis fédération (tjs) + avis GGD

    def test_workflow_subedsr(self):
        """
        Tester l'état des avis du workflow (normalement autant d'avis dans chaque workflow)

        Note : le préavis EDSR n'existera pas ici, car il faudrait créer un Avis GGD avec un EDSR sélectionné
        """
        self.instance.workflow_ggd = Instance.WF_GGD_SUBEDSR  # Choisir le workflow GGD avec préavis EDSR.
        self.instance.save()
        self.authorization.ggd_concerned = True  # On teste la création automatique de l'avis GGD (configuré sans EDSR du coup)
        self.authorization.save()
        self.assertEqual(self.authorization.get_avis_count(), 2)  # avis fédération (tjs) + avis GGD
