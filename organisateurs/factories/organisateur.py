# coding: utf-8
import factory

from core.factories.user import UserFactory
from organisateurs.models import Organisateur


class OrganisateurFactory(factory.django.DjangoModelFactory):
    """ Factory Organisateur """

    # Champs
    user = factory.SubFactory(UserFactory)

    # Meta
    class Meta:
        model = Organisateur
