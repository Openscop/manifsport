# coding: utf-8
from clever_selects.form_fields import ChainedModelChoiceField
from crispy_forms.layout import Submit
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from core.forms.base import GenericForm
from core.util.champ import TelField
from organisateurs.models import Structure


class StructureForm(GenericForm):
    """ Formulaire des structures """

    # Champs
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'),
                                      Commune, label="Commune")
    phone = TelField()

    # Overrides
    def __init__(self, *args, **kwargs):
        super(StructureForm, self).__init__(*args, **kwargs)
        if self.instance.pk and self.instance.commune:
            self.fields['departement'].initial = self.instance.commune.get_departement()
            self.fields['commune'].queryset = Commune.objects.by_departement(self.instance.commune.get_departement())

        self.helper.add_input(Submit('submit', "Enregistrer"))

    # Meta
    class Meta:
        model = Structure
        fields = ['name', 'organisateur', 'type_of_structure', 'address', 'departement', 'commune', 'phone', 'website']
