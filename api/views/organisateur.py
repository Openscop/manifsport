import logging
from django_filters.rest_framework import FilterSet
from bootstrap_datepicker_plus import DateTimePickerInput
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend, ModelChoiceFilter, DateTimeFilter, NumberFilter
from api.serializer import ManifestationAutorisationSerializer, ManifestationAutorisationGroupApiSerializer,\
    ManifestationDeclarationSerializer, ManifestationDeclarationGroupApiSerializer, ManifestationSerializer,\
    StructureSerializer, ManifestationAutorisationInstructeurSerializer, ManifestationDeclarationInstructeurSerializer
from api.permissions import IsInGroupApi
from datetime import datetime
from administrative_division.models import Departement, Arrondissement, Commune
from events.models import Manifestation
from organisateurs.models import Structure
from authorizations.models import ManifestationAutorisation
from declarations.models import ManifestationDeclaration
from sports.models import Activite

api_logger = logging.getLogger('api')


class OrganisateurFilter(FilterSet):
    departement = ModelChoiceFilter(field_name="commune__arrondissement__departement",
                                    label='departement',
                                    queryset=Departement.objects.all())
    arrondissement = ModelChoiceFilter(field_name="commune__arrondissement",
                                       label='arrondissement',
                                       queryset=Arrondissement.objects.all())

    class Meta:
        model = Structure
        fields = ('departement', 'arrondissement', 'commune')


class OrganisateurViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vue des organisateurs basée sur le modèle Structure\n
    Recherche possible sur les champs 'organisateur' et 'structure' avec le bouton 'filtres'
    """
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    queryset = Structure.objects.all()
    serializer_class = StructureSerializer
    permission_classes = (IsInGroupApi, )
    filter_class = OrganisateurFilter
    search_fields = ('name', 'organisateur__user__username')
    ordering_fields = ('name', 'commune')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super().finalize_response(request, response, *args, **kwargs)
