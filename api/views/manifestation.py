import logging
from django_filters.rest_framework import FilterSet
from bootstrap_datepicker_plus import DateTimePickerInput
from rest_framework import viewsets, filters
from rest_framework.permissions import IsAuthenticated
from django_filters.rest_framework import DjangoFilterBackend, ModelChoiceFilter, DateTimeFilter, NumberFilter
from api.serializer import ManifestationAutorisationSerializer, ManifestationAutorisationGroupApiSerializer,\
    ManifestationDeclarationSerializer, ManifestationDeclarationGroupApiSerializer, ManifestationSerializer,\
    ManifestationAutorisationInstructeurSerializer, ManifestationDeclarationInstructeurSerializer
from datetime import datetime
from administrative_division.models import Departement, Arrondissement, Commune
from events.models import Manifestation
from authorizations.models import ManifestationAutorisation
from declarations.models import ManifestationDeclaration
from sports.models import Activite

api_logger = logging.getLogger('api')


class ManifestationFilter(FilterSet):
    departement = ModelChoiceFilter(field_name="manifestation__departure_city__arrondissement__departement",
                                    label='departement',
                                    queryset=Departement.objects.all())
    arrondissement = ModelChoiceFilter(field_name="manifestation__departure_city__arrondissement",
                                       label='arrondissement',
                                       queryset=Arrondissement.objects.all())
    commune = ModelChoiceFilter(field_name="manifestation__departure_city",
                                label='commune',
                                queryset=Commune.objects.all())
    activite = ModelChoiceFilter(field_name="manifestation__activite",
                                 label='activite',
                                 queryset=Activite.objects.all())
    begin_date = DateTimeFilter(field_name="manifestation__begin_date",
                                label='date de début',
                                widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                                lookup_expr='gte')
    end_date = DateTimeFilter(field_name="manifestation__end_date",
                              label='date de fin',
                              widget=DateTimePickerInput(options={"format": "DD/MM/YYYY HH:mm", 'locale': 'fr'}),
                              lookup_expr='lte')
    year = NumberFilter(field_name="manifestation__begin_date",
                        label='année',
                        initial=datetime.now().year,
                        lookup_expr='year')

    def __init__(self, data=None, *args, **kwargs):
        # filterset is bound, use initial values as defaults
        if data is not None:
            data = data.copy()

            # year, begin_date and end_date are either missing or empty, use initial as default for year
            if not data.get('year') and not data.get('begin_date') and not data.get('end_date'):
                data['year'] = self.base_filters['year'].extra['initial']

        super(ManifestationFilter, self).__init__(data, *args, **kwargs)


class ManifestationAutorisationFilter(ManifestationFilter):

    class Meta:
        model = ManifestationAutorisation
        fields = ('departement', 'arrondissement', 'commune', 'activite', 'begin_date', 'end_date', 'year')


class ManifestationDeclarationFilter(ManifestationFilter):

    class Meta:
        model = ManifestationDeclaration
        fields = ('departement', 'arrondissement', 'commune', 'activite', 'begin_date', 'end_date', 'year')


class ManifestationViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vue des manifestations
    """
    permission_classes = (IsAuthenticated,)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ('manifestation__name', 'manifestation__description',)
    ordering_fields = ('manifestation__begin_date', 'manifestation__activite', 'manifestation__departure_city')

    """
    Utilisation de la fonction finalize_response pour bénéficier des variables request et response
    dans lesquelles se trouvent les infos à logger
    """
    def finalize_response(self, request, response, *args, **kwargs):
        if 'count' in response.data:
            count = response.data['count']
        else:
            count = 'none'
        api_logger.info("Utilisateur %s | %s de %s | %s | réponse %s, %s | items : %s" %
                        (request.user, request.method, self.request.get_host(), self.request.get_full_path(),
                         response.status_code, response.status_text, count
                         ))
        return super(ManifestationViewSet, self).finalize_response(request, response, *args, **kwargs)


class ManifestationAllViewSet(ManifestationViewSet):
    """
    vue de toutes les manifestations ordonnées de la plus récente
    """
    queryset = Manifestation.objects.all().order_by('-begin_date')
    serializer_class = ManifestationSerializer
    filter_fields = ('activite', 'departure_city')
    search_fields = ('name', 'description',)
    ordering_fields = ('begin_date', 'activite', 'departure_city')


class ManifestationAutorisationViewSet(ManifestationViewSet):
    """
    Vue des manifestations autorisées avec le status Published\n
    Recherche possible sur les champs 'name' et 'description' avec le bouton 'filtres'
    """
    queryset = ManifestationAutorisation.objects.filter(state='published')
    filter_class = ManifestationAutorisationFilter

    def get_serializer_class(self):

        grouplist = self.request.user.groups.values_list('name', flat=True)
        if 'API' in grouplist:
            return ManifestationAutorisationGroupApiSerializer
        else:
            if hasattr(self.request.user, 'instructeur'):
                return ManifestationAutorisationInstructeurSerializer
            else:
                return ManifestationAutorisationSerializer


class ManifestationDeclarationViewSet(ManifestationViewSet):
    """
    Vue des manifestations déclarées avec le status Published\n
    Recherche possible sur les champs 'name' et 'description' avec le bouton 'filtres'
    """
    queryset = ManifestationDeclaration.objects.filter(state='published')
    filter_class = ManifestationDeclarationFilter

    def get_serializer_class(self):

        grouplist = self.request.user.groups.values_list('name', flat=True)
        if 'API' in grouplist:
            return ManifestationDeclarationGroupApiSerializer
        else:
            if hasattr(self.request.user, 'instructeur'):
                return ManifestationDeclarationInstructeurSerializer
            else:
                return ManifestationDeclarationSerializer
