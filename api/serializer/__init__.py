from .structure import StructureSerializer
from .manifestation import *
from .manif import InstructionSerializer, InstructionGroupApiSerializer,\
    InstructionInstructeurSerializer, ManifSerializer