from rest_framework import serializers
from evenements.models import Manif
from instructions.models import Instruction
from administrative_division.models import Commune


class CommuneField(serializers.RelatedField):
    """
    Modification de la représentation de l'objet
    """
    queryset = Commune.objects.all()

    def to_representation(self, value):
        return '%s, %s' % (value.code, value.name)


class ManifSerializer(serializers.ModelSerializer):

    # Utilisation de la classe StringRelatedField pour afficher
    # la représentation de l'objet défini par __str__
    # departure_city = serializers.StringRelatedField()
    #
    # Utilisation de la classe définie plus haut pour afficher
    # une représentation personnalisée de l'objet
    ville_depart = CommuneField()
    activite = serializers.StringRelatedField()

    class Meta:
        model = Manif
        fields = (
            'nom',
            'description',
            'activite',
            'date_debut',
            'date_fin',
            'ville_depart',
            'nb_participants'
        )
        # depth = 2 pour voir les attributs de l'objet valeur d'un champ


class ManifInstructeurSerializer(serializers.ModelSerializer):

    ville_depart = CommuneField()
    activite = serializers.StringRelatedField()
    villes_traversees = CommuneField(many=True)
    structure = serializers.StringRelatedField()

    class Meta:
        model = Manif
        fields = (
            'nom',
            'description',
            'activite',
            'structure',
            'date_debut',
            'date_fin',
            'ville_depart',
            'villes_traversees',
            'nb_participants'
        )


class ManifGroupApiSerializer(serializers.ModelSerializer):

    ville_depart = CommuneField()
    structure = serializers.StringRelatedField()
    activite = serializers.StringRelatedField()

    class Meta:
        model = Manif
        fields = (
            'nom',
            'description',
            'activite',
            'structure',
            'date_debut',
            'date_fin',
            'ville_depart',
            'nb_participants'
        )


class InstructionSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manif = ManifSerializer()

    class Meta:
        model = Instruction
        fields = ('manif',)


class InstructionGroupApiSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manif = ManifGroupApiSerializer()

    class Meta:
        model = Instruction
        fields = ('manif',)


class InstructionInstructeurSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manif = ManifInstructeurSerializer()

    class Meta:
        model = Instruction
        fields = ('manif',)
