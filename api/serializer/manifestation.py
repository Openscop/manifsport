from rest_framework import serializers
from events.models import Manifestation
from authorizations.models import ManifestationAutorisation
from declarations.models import ManifestationDeclaration
from administrative_division.models import Commune


class CommuneField(serializers.RelatedField):
    """
    Modification de la représentation de l'objet
    """
    queryset = Commune.objects.all()

    def to_representation(self, value):
        return '%s, %s' % (value.code, value.name)


class ManifestationSerializer(serializers.ModelSerializer):

    # Utilisation de la classe StringRelatedField pour afficher
    # la représentation de l'objet défini par __str__
    # departure_city = serializers.StringRelatedField()
    #
    # Utilisation de la classe définie plus haut pour afficher
    # une représentation personnalisée de l'objet
    departure_city = CommuneField()
    activite = serializers.StringRelatedField()

    class Meta:
        model = Manifestation
        fields = (
            'name',
            'description',
            'activite',
            'begin_date',
            'end_date',
            'departure_city',
            'number_of_entries'
        )
        # depth = 2 pour voir les attributs de l'objet valeur d'un champ


class ManifestationInstructeurSerializer(serializers.ModelSerializer):

    departure_city = CommuneField()
    activite = serializers.StringRelatedField()
    crossed_cities = CommuneField(many=True)
    structure = serializers.StringRelatedField()

    class Meta:
        model = Manifestation
        fields = (
            'name',
            'description',
            'activite',
            'structure',
            'begin_date',
            'end_date',
            'departure_city',
            'crossed_cities',
            'number_of_entries'
        )


class ManifestationGroupApiSerializer(serializers.ModelSerializer):

    departure_city = CommuneField()
    structure = serializers.StringRelatedField()
    activite = serializers.StringRelatedField()

    class Meta:
        model = Manifestation
        fields = (
            'name',
            'description',
            'activite',
            'structure',
            'begin_date',
            'end_date',
            'departure_city',
            'number_of_entries'
        )


class ManifestationAutorisationSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manifestation = ManifestationSerializer()

    class Meta:
        model = ManifestationAutorisation
        fields = ('manifestation',)


class ManifestationAutorisationGroupApiSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manifestation = ManifestationGroupApiSerializer()

    class Meta:
        model = ManifestationAutorisation
        fields = ('manifestation',)


class ManifestationAutorisationInstructeurSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manifestation = ManifestationInstructeurSerializer()

    class Meta:
        model = ManifestationAutorisation
        fields = ('manifestation',)


class ManifestationDeclarationSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manifestation = ManifestationSerializer()

    class Meta:
        model = ManifestationDeclaration
        fields = ('manifestation',)


class ManifestationDeclarationGroupApiSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manifestation = ManifestationGroupApiSerializer()

    class Meta:
        model = ManifestationDeclaration
        fields = ('manifestation',)


class ManifestationDeclarationInstructeurSerializer(serializers.ModelSerializer):

    # Utilisation du serializer de Manifestation
    manifestation = ManifestationInstructeurSerializer()

    class Meta:
        model = ManifestationDeclaration
        fields = ('manifestation',)
