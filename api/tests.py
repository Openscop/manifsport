from django.test import TestCase
from datetime import datetime
from django.contrib.auth import hashers
from django.contrib.auth.models import Group
from core.models import User
from organisateurs.factories import OrganisateurFactory, StructureFactory
from administration.factories import InstructeurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from core.factories import UserFactory
from sports.factories.sport import ActiviteFactory
from events.factories.manifestation import DeclarationNMFactory, AutorisationNMFactory
from declarations.factories import EventDeclarationFactory
from authorizations.factories import ManifestationAuthorizationFactory

class ApiTests(TestCase):

    @classmethod
    def setUpTestData(cls):
        # Création des lieux
        dep = DepartementFactory.create(name='42')
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=dep)
        prefecture = arrondissement.prefecture
        commune = CommuneFactory(name='Bard', arrondissement=arrondissement)

        # Création de trois utilisateurs
        UserFactory.create(username='simple_user', password=hashers.make_password(123), first_name='django')

        groupe = Group.objects.create(name='API')
        user = UserFactory.create(username='staff_user', password=hashers.make_password(321))
        user.groups.add(groupe)

        instructeur = UserFactory.create(username='instructeur', password=hashers.make_password(132))
        InstructeurFactory.create(user=instructeur, prefecture=prefecture)

        # Création d'une structure
        organ = OrganisateurFactory.create(user=user)
        struc = StructureFactory.create(name="structure_test", commune=commune, organisateur=organ)
        # Création d'une activite
        sport = ActiviteFactory.create(name='sport_test')
        # Création des manifestations
        # la date est fixée parce que, sans query, la route renvoie les manifestation de l'année
        manifD = DeclarationNMFactory.create(name='manifD_test',
                                            structure=struc,
                                            begin_date=datetime.now(),
                                            departure_city=commune,
                                            activite=sport)
        manifA = AutorisationNMFactory.create(name='manifA_test',
                                              structure=struc,
                                              departure_city=commune,
                                              begin_date=datetime.now(),
                                              activite=sport)
        # le status est spécifié parce que la route ne renvoie que les manifestations publiées
        EventDeclarationFactory.create(manifestation=manifD, state="published")
        ManifestationAuthorizationFactory(manifestation=manifA, state="published")

    def test1_user(self):
        """
        Tester si l'utilisateur est bien créé
        :return:
        """
        user = User.objects.get(username='simple_user')
        name = user.get_short_name()
        self.assertEqual(name, 'django')

    def test2_access_anonymous(self):
        """
        Verifie si l'accès est refusé sans login
        :return:
        """
        reponse = self.client.get('/api/')
        self.assertEqual(reponse.status_code, 401)
        self.assertContains(reponse, "authentification", status_code=401)

    def test3_access_simple_user(self):
        """
        Verifie si l'accès est ok avec login
        :return:
        """
        self.assertTrue(self.client.login(username='simple_user', password='123'))
        reponse = self.client.get('/api/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("ManifestationDeclaration", reponse.json())
        self.assertIn("ManifestationAutorisation", reponse.json())
        self.assertIn("Organisateur", reponse.json())

    def test4_ManifestationDeclaration_simple_user(self):
        """
        Verifie avec l'utilisateur simple l'accès à la route ManifestationDeclaration sans le champ "structure"
        :return:
        """
        self.assertTrue(self.client.login(username='simple_user', password='123'))
        reponse = self.client.get('/api/ManifestationDeclaration/')
        #print(reponse.json())
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("manifD_test", reponse.json()['results'][0]['manifestation']['name'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['manifestation']['activite'])
        self.assertNotIn("structure", reponse.json()['results'][0]['manifestation'])
        self.assertNotIn("crossed_cities", reponse.json()['results'][0]['manifestation'])

    def test5_ManifestationDeclaration_apigroup_user(self):
        """
        Verifie avec l'utilisateur staff l'accès à la route ManifestationDeclaration avec le champ "structure"
        :return:
        """
        self.assertTrue(self.client.login(username='staff_user', password='321'))
        reponse = self.client.get('/api/ManifestationDeclaration/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("manifD_test", reponse.json()['results'][0]['manifestation']['name'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['manifestation']['activite'])
        self.assertIn("Structure_Test", reponse.json()['results'][0]['manifestation']['structure'])
        self.assertNotIn("crossed_cities", reponse.json()['results'][0]['manifestation'])

    def test6_ManifestationDeclaration_instructeur_user(self):
        """
        Verifie avec l'utilisateur instructeur l'accès à la route ManifestationAutorisation avec le champ "structure" et "crossed_cities"
        :return:
        """
        self.assertTrue(self.client.login(username='instructeur', password='132'))
        reponse = self.client.get('/api/ManifestationDeclaration/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("manifD_test", reponse.json()['results'][0]['manifestation']['name'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['manifestation']['activite'])
        self.assertIn("Structure_Test", reponse.json()['results'][0]['manifestation']['structure'])
        self.assertIn("crossed_cities", reponse.json()['results'][0]['manifestation'])

    def test7_ManifestationAutorisation_simple_user(self):
        """
        Verifie avec l'utilisateur simple l'accès à la route ManifestationAutorisation sans le champ "structure"
        :return:
        """
        self.assertTrue(self.client.login(username='simple_user', password='123'))
        reponse = self.client.get('/api/ManifestationAutorisation/')
        #print(reponse.json())
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("manifA_test", reponse.json()['results'][0]['manifestation']['name'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['manifestation']['activite'])
        self.assertNotIn("structure", reponse.json()['results'][0]['manifestation'])
        self.assertNotIn("crossed_cities", reponse.json()['results'][0]['manifestation'])

    def test8_ManifestationAutorisation_apigroup_user(self):
        """
        Verifie avec l'utilisateur staff l'accès à la route ManifestationAutorisation avec le champ "structure"
        :return:
        """
        self.assertTrue(self.client.login(username='staff_user', password='321'))
        reponse = self.client.get('/api/ManifestationAutorisation/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("manifA_test", reponse.json()['results'][0]['manifestation']['name'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['manifestation']['activite'])
        self.assertIn("Structure_Test", reponse.json()['results'][0]['manifestation']['structure'])
        self.assertNotIn("crossed_cities", reponse.json()['results'][0]['manifestation'])

    def test9_ManifestationAutorisation_instructeur_user(self):
        """
        Verifie avec l'utilisateur instructeur l'accès à la route ManifestationAutorisation avec le champ "structure" et "crossed_cities"
        :return:
        """
        self.assertTrue(self.client.login(username='instructeur', password='132'))
        reponse = self.client.get('/api/ManifestationAutorisation/')
        self.assertEqual(reponse.status_code, 200)
        self.assertIn("manifA_test", reponse.json()['results'][0]['manifestation']['name'])
        self.assertIn("Sport_test", reponse.json()['results'][0]['manifestation']['activite'])
        self.assertIn("Structure_Test", reponse.json()['results'][0]['manifestation']['structure'])
        self.assertIn("crossed_cities", reponse.json()['results'][0]['manifestation'])

    def test10_Organisateur_denied(self):
        """
        Verifie la réponse à une recherche sur la route Organisateur
        L'utilisateur n'a pas accès à cette route
        :return:
        """
        self.assertTrue(self.client.login(username='simple_user', password='123'))
        reponse = self.client.get('/api/Organisateur/')
        self.assertEqual(reponse.status_code, 403)
        self.assertIn("pas accès à ces informations", reponse.json()['detail'])

    def test11_Organisateur_granted(self):
        """
        Verifie la réponse à une recherche sur la route Organisateur
        L'utilisateur a accès à cette route
        :return:
        """
        self.assertTrue(self.client.login(username='staff_user', password='321'))
        reponse = self.client.get('/api/Organisateur/')
        self.assertEqual(reponse.status_code, 200)
        self.assertEqual(1, reponse.json()['count'])
        self.assertIn("Bard", reponse.json()['results'][0]['commune'])
        self.assertIn("structure_test", reponse.json()['results'][0]['structure'])
