# coding: utf-8
from django.test import TestCase

from administration.factories.people import InstructeurFactory
from administration.factories.service import CompagnieFactory, CGServiceFactory, CGDFactory
from authorizations.factories import ManifestationAuthorizationFactory
from declarations.factories import EventDeclarationFactory
from evaluations.factories import Natura2000EvaluationFactory
from evaluations.factories import RNREvaluationFactory
from ..factories import DeclarationNMFactory


class DeclarationNMTests(TestCase):
    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        self.authorization = ManifestationAuthorizationFactory.create(manifestation=DeclarationNMFactory.create())
        self.manifestation = self.authorization.get_manifestation()
        self.commune = self.manifestation.departure_city
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.commune.get_prefecture()
        self.instructeur = InstructeurFactory.create(prefecture=self.prefecture)
        self.cg = self.departement.cg
        self.edsr = self.departement.edsr
        self.ddsp = self.departement.ddsp
        self.sdis = self.departement.sdis
        self.ggd = self.departement.ggd
        self.activite = self.manifestation.activite
        self.federation = self.manifestation.get_federation()
        self.compagnie = CompagnieFactory.create(sdis=self.sdis)
        self.cgservice = CGServiceFactory.create(cg=self.cg)
        self.cgd = CGDFactory.create(commune=self.commune)

    def test_str(self):
        self.assertEqual(str(self.manifestation), self.manifestation.name.title())

    def test_display_rnr_eval_panel(self):
        """ Tester la méthode indiquant s'il faut afficher le panneau d'évaluation RNR """
        manifestation = DeclarationNMFactory.build(is_on_rnr=True)
        self.assertTrue(manifestation.display_rnr_eval_panel())
        manifestation = DeclarationNMFactory.build(is_on_rnr=False)
        self.assertFalse(manifestation.display_rnr_eval_panel())

    def test_display_natura2000_eval_panel(self):
        manifestation = DeclarationNMFactory.build(lucrative=True)
        self.assertTrue(manifestation.display_natura2000_eval_panel())
        manifestation3 = DeclarationNMFactory.build(big_budget=True)
        self.assertTrue(manifestation3.display_natura2000_eval_panel())

    def test_not_display_natura2000_eval_panel(self):
        manifestation = DeclarationNMFactory.build()
        self.assertFalse(manifestation.display_natura2000_eval_panel())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.legal_delay(), 30)

    def test_get_final_breadcrumb_not_sent(self):
        manifestation = DeclarationNMFactory.build()
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 0)

    def test_get_final_breadcrumb_sent(self):
        manifestation = DeclarationNMFactory.create()
        _declaration = EventDeclarationFactory.create(manifestation=manifestation)
        self.assertEqual(manifestation.get_final_breadcrumb()[1], True)
        self.assertEqual(manifestation.get_final_breadcrumb()[0][1], 1)

    def test_get_breadcrumbs(self):
        manifestation = DeclarationNMFactory.build()
        self.assertEqual(len(manifestation.get_breadcrumbs()), 2)

    def test_get_breadcrumbs_full(self):
        manifestation = DeclarationNMFactory.create()
        RNREvaluationFactory.create(manifestation=manifestation)
        Natura2000EvaluationFactory.create(manifestation=manifestation)
        self.assertEqual(len(manifestation.get_breadcrumbs()), 4)

    def test_get_breadcrumbs_full_2(self):
        manifestation = DeclarationNMFactory.create(is_on_rnr=True, lucrative=True)
        self.assertEqual(len(manifestation.get_breadcrumbs()), 4)
