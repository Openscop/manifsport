# coding: utf-8
from django.test import TestCase

from administration.factories.service import CompagnieFactory, CGServiceFactory, CGDFactory
from authorizations.factories import ManifestationAuthorizationFactory


class EventsTestsBase(TestCase):
    """ Mixin de tests des manifestations """

    def setUp(self):
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        self.authorization = ManifestationAuthorizationFactory.create()
        self.manifestation = self.authorization.get_manifestation()
        self.commune = self.manifestation.departure_city
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.prefecture = self.commune.get_prefecture()
        self.cg = self.departement.cg
        self.edsr = self.departement.edsr
        self.ddsp = self.departement.ddsp
        self.sdis = self.departement.sdis
        self.ggd = self.departement.ggd
        self.activite = self.manifestation.activite
        self.compagnie = CompagnieFactory.create(sdis=self.sdis)
        self.cgservice = CGServiceFactory.create(cg=self.cg)
        self.cgd = CGDFactory.create(commune=self.commune)
