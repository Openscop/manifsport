# coding: utf-8
from ajax_select.fields import AutoCompleteField
from crispy_forms.layout import Layout, Fieldset, HTML
from django.forms import ModelChoiceField

from core.forms.base import GenericForm
from events.forms.manifestation import FORM_WIDGETS, HELP_EXTRA_ACTIVITE
from events.models import MotorizedRace
from sports.models import Discipline
from .manifestation import ManifestationForm, FIELDS_MANIFESTATION, FIELDS_ROADS, FIELDS_FILES, FIELDS_MARKUP, FIELDS_MARKUP_CONV, FIELDS_NATURA2000


class MotorizedRaceForm(ManifestationForm):
    """ Formulaire """

    # Champs
    discipline = ModelChoiceField(label="Disciplines", required=False, queryset=Discipline.objects.filter(motorise=True))
    extra_activite = AutoCompleteField('activite_m', label="Recherche d'activité sportive", required=False,
                                       help_text=HELP_EXTRA_ACTIVITE, show_help_text=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(HTML("<p>Remplissez ce formulaire puis vous pourrez joindre ensuite des fichiers à votre dossier.</p><hr>"),
                                    Fieldset("Organisateur Technique", 'tech_name', 'tech_firstname', 'tech_tel', 'tech_email'),
                                    Fieldset(*(FIELDS_MANIFESTATION + ['support', 'circuit_homologue'])),
                                    Fieldset(*FIELDS_ROADS),
                                    Fieldset(*FIELDS_MARKUP_CONV), Fieldset(*FIELDS_MARKUP),
                                    Fieldset(*[FIELDS_NATURA2000[i] for i in (0, 1, 2, 4)]))

    # Meta
    class Meta:
        model = MotorizedRace
        exclude = ('structure', 'approval_request', 'motor_on_natura2000', 'instance')
        widgets = FORM_WIDGETS


class MotorizedRaceFilesForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper.layout = Layout(Fieldset(*(FIELDS_FILES[:-1] + ['mass_map', 'cartography'])))
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = MotorizedRace
        fields = ['manifestation_rules', 'organisateur_commitment', 'insurance_certificate', 'safety_provisions', 'doctor_attendance', 'additional_docs',
                  'rounds_safety', 'mass_map', 'cartography']
