# coding: utf-8

from crispy_forms.layout import Submit
from django import forms

from core.forms.base import GenericForm
from events.models import DocumentComplementaire


class DocumentComplementaireRequestForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(DocumentComplementaireRequestForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Envoyer la Demande d'Information"))

    # Meta
    class Meta:
        model = DocumentComplementaire
        fields = ['desired_information']


class DocumentComplementaireProvideForm(GenericForm):
    """ Formulaire """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(DocumentComplementaireProvideForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Fournir le Document"))

    # Nettoyage des données validées
    def clean_attached_document(self):
        data = self.cleaned_data['attached_document']
        if data is None:
            raise forms.ValidationError("La sélection d'un document est obligatoire")
        return data

    # Meta
    class Meta:
        model = DocumentComplementaire
        fields = ['attached_document']
