# coding: utf-8

from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from events.models.manifestation import Manifestation


class DeclarationNM(Manifestation):
    """ Manifestation sans véhicule motorisé qui nécessite jsute une déclaration """

    # Constantes
    GROUPED_TRAFFIC_CHOICES = (
        ('p', "plus de 75 piétons"),
        ('c', "plus de 50 cycles ou autres véhicules ou engins non motorisés"),
        ('h', "plus de 25 chevaux ou autres animaux"),
    )
    HELP_GROUPED_TRAFFIC = "ne rien remplir dans ce champ car il ne concerne que les demandes déposés avant 14 décembre 2017"


    # Champs
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True, related_name='declarationnm', on_delete=models.CASCADE)
    grouped_traffic = models.CharField("circulation groupée", max_length=1, choices=GROUPED_TRAFFIC_CHOICES, blank=True, help_text=HELP_GROUPED_TRAFFIC)
    grouped_start = models.BooleanField("départ groupé des participants", default=False)
    grouped_run = models.BooleanField("circulation groupée des participants", default=False)
    support_vehicles_number = models.PositiveIntegerField("nombre de véhicules d'accompagnement", help_text="nombre de véhicules d'accompagnement")
    # Ajout de default dans les CharField et TextField pour la DB existante
    safety_name = models.CharField("nom de famille", max_length=200, blank=True, default='')
    safety_firstname = models.CharField("prénom", max_length=200, blank=True, default='')
    safety_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    safety_email = models.EmailField("adresse email", max_length=200, blank=True, default='')

    opening_vehicle = models.BooleanField('présence d\'un véhicule d\'ouverture (véhicule "pilote")', default=False)
    heading_vehicle = models.BooleanField('présence d\'un véhicule de début de course (véhicule "tête de course")', default=False)
    trailing_vehicle = models.BooleanField('présence d\'un véhicule de fin de course', default=False)
    staff_vehicle = models.BooleanField('présence d\'autres véhicules d\'organisation (auto ou moto)', default=False)
    signalers_number = models.PositiveSmallIntegerField("nombre de signaleurs", default=0)
    signalers_number_still = models.PositiveSmallIntegerField("en postes fixes", default=0)
    signalers_number_auto = models.PositiveSmallIntegerField("mobiles en voitures", default=0)
    signalers_number_moto = models.PositiveSmallIntegerField("mobiles en motos", default=0)
    city_police = models.BooleanField('Disposerez-vous d\'un encadrement de la police municipale ?', default=False)
    city_police_detail = models.TextField("Précisez les moyens affectés", blank=True, default='')
    state_police = models.BooleanField('Avez-vous passez une convention avec la police nationale ou la gendarmerie ?', default=False)
    state_police_detail = models.TextField("Précisez les moyens affectés", blank=True, default='', help_text="joignez, dans la mesure du possible la convention")

    # Getter
    def get_final_breadcrumb(self):
        """ Renvoyer le breadcrumb pour la manifestation """
        exists = True
        try:
            self.manifestationdeclaration
        except (AttributeError, ObjectDoesNotExist):
            breadcrumb = ["déclaration non envoyée", 0]
        else:
            breadcrumb = ["déclaration envoyée", 1]
        return breadcrumb, exists

    # Meta
    class Meta:
        verbose_name = "manifestation sans véhicules terrestres à moteur soumise à déclaration"
        verbose_name_plural = "manifestations sans véhicules terrestres à moteur soumises à déclaration"
        default_related_name = "declarationsnm"
        app_label = "events"
