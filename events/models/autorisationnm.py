# coding: utf-8

from django.core.exceptions import ObjectDoesNotExist
from django.db import models

from configuration.directory import UploadPath
from events.models.manifestation import Manifestation


class AutorisationNM(Manifestation):
    """ Manifestation non motorisée nécessitant une autorisation """

    # Champs
    manifestation_ptr = models.OneToOneField('events.manifestation', parent_link=True, related_name='autorisationnm', on_delete=models.CASCADE)
    multisport_federation = models.ForeignKey('sports.federation', verbose_name="fédération multi-sports",
                                              limit_choices_to={'multisports': True}, blank=True, null=True, on_delete=models.SET_NULL, help_text="Si votre manifestation est organisée sous la tutelle d'une fédération multisports, sélectionnez là ici.")
    affiliation = models.BooleanField("Affiliation à une fédération délégataire", default=False, help_text="Êtes-vous affilié à la fédération délégataire de la discipline pratiquée lors de la manifestation ?")
    signalers_list = models.FileField(upload_to=UploadPath('listes_signaleurs'), blank=True, null=True,
                                      verbose_name="liste des signaleurs", max_length=512)
    cycling_techdata = models.FileField(upload_to=UploadPath('dossier_technique'), blank=True, null=True,
                                        help_text="Le formulaire vierge est disponible <a href='https://www.ffc.fr/wp-content/uploads/2018/01/Dossier-technique-manifestation-cycliste-2018.pdf' target='_blank'>ici</a>",
                                        verbose_name="dossier technique cyclisme", max_length=512)
    # Ajout de default dans les CharField et TextField pour la DB existante
    safety_name = models.CharField("nom de famille", max_length=200, blank=True, default='')
    safety_firstname = models.CharField("prénom", max_length=200, blank=True, default='')
    safety_tel = models.CharField("numéro de téléphone", max_length=14, blank=True, default='')
    safety_email = models.EmailField("adresse email", max_length=200, blank=True, default='')

    opening_vehicle = models.BooleanField('présence d\'un véhicule d\'ouverture (véhicule "pilote")', default=False)
    heading_vehicle = models.BooleanField('présence d\'un véhicule de début de course (véhicule "tête de course")', default=False)
    trailing_vehicle = models.BooleanField('présence d\'un véhicule de fin de course', default=False)
    staff_vehicle = models.BooleanField('présence d\'autres véhicules d\'organisation (auto ou moto)', default=False)
    signalers_number = models.PositiveSmallIntegerField("nombre de signaleurs", default=0)
    signalers_number_still = models.PositiveSmallIntegerField("en postes fixes", default=0)
    signalers_number_auto = models.PositiveSmallIntegerField("mobiles en voitures", default=0)
    signalers_number_moto = models.PositiveSmallIntegerField("mobiles en motos", default=0)
    city_police = models.BooleanField('Disposerez-vous d\'un encadrement de la police municipale ?', default=False)
    city_police_detail = models.TextField("Précisez les moyens affectés", blank=True, default='')
    state_police = models.BooleanField('Avez-vous passez une convention avec la police nationale ou la gendarmerie ?', default=False)
    state_police_detail = models.TextField("Précisez les moyens affectés", blank=True, default='', help_text="joignez, dans la mesure du possible la convention")

    traffic_regulations_compliance = models.BooleanField('respect code de la route', default=True)
    right_of_way = models.BooleanField('priorité de passage', default=False)
    temporary_exclusive_roads = models.BooleanField('usage exclusif temporaire de la chaussée', default=False)
    private_roads = models.BooleanField('usage privatif de la chaussée', default=False)
    traffic_regulations_compliance_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")
    right_of_way_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")
    temporary_exclusive_roads_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")
    private_roads_detail = models.TextField("Précisez les voies empruntées", blank=True, default='', help_text="avec le créneau horaire")

    def legal_delay(self):
        """ Renvoyer le délai légal pour l'obtention d'une autorisation """
        if self.other_departments_crossed.count() == 0:
            return self.get_instance().get_manifestation_anm_ldept_delay()
        else:
            return self.get_instance().get_manifestation_anm_ndept_delay()

    def display_natura2000_eval_panel(self):
        """ Indiquer si le panneau pour l'évaluation N2K doit être affiché  """
        return not hasattr(self, 'natura2000evaluations') and (self.big_budget or self.lucrative or self.big_title)

    def get_final_breadcrumb(self):
        """
        Renvoyer le breadcrumb pour l'état de l'autorisation

        :returns: un breadcrumb de la forme d'une liste à deux entrées [libellé, statut] + un booléen 'exists'
        """
        try:
            self.manifestationautorisation
            breadcrumb = ["demande d'autorisation envoyée", 1]
        except (AttributeError, ObjectDoesNotExist):
            breadcrumb = ["demande d'autorisation non envoyée", 0]
        return breadcrumb, True

    def ready_for_instruction(self):
        """ Indiquer si l'instruction peut débuter """
        ready = super(AutorisationNM, self).ready_for_instruction()
        if not self.signalers_list:
            ready = False
        if not self.cycling_techdata and self.activite.discipline.name == 'Cyclisme':
            ready = False
        return ready

    # Méta
    class Meta:
        verbose_name = "manifestation sans véhicules terrestres à moteur soumise à autorisation"
        verbose_name_plural = "manifestations sans véhicules terrestres à moteur soumises à autorisation"
        default_related_name = "autorisationsnm"
        app_label = "events"
