# coding: utf-8

import factory
from django.utils.timezone import timedelta, now
from factory.fuzzy import FuzzyDateTime

from administrative_division.factories import CommuneFactory
from organisateurs.factories import StructureFactory
from events.models import *
from sports.factories import ActiviteFactory


class DeclarationNMFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation non motorisée + déclaration """

    # Champs
    name = factory.Sequence(lambda n: 'Non Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    # date de début aléatoire entre aujourd'hui plus 100jours et dans 2 ans plus 100jours
    begin_date = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    #TODO: Fuzzy sera supprimé dans la release 3.x de factory-boy, A remplacer par l'équivalent Faker ( voir https://github.com/FactoryBoy/factory_boy/issues/271/)
    # dat de fin même jours 8 heures plus tard
    end_date = factory.LazyAttribute(lambda o: o.begin_date + timedelta(hours=8))
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    support_vehicles_number = 30
    max_audience = 5000

    # Meta
    class Meta:
        model = DeclarationNM

    @factory.post_generation
    def crossed_cities(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for city in extracted:
                self.crossed_cities.add(city)



class AutorisationNMFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation non motorisée + autorisation """

    # Champs
    name = factory.Sequence(lambda n: 'Non Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    end_date = factory.LazyAttribute(lambda o: o.begin_date + timedelta(hours=8))
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000

    # Meta
    class Meta:
        model = AutorisationNM

    @factory.post_generation
    def crossed_cities(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of groups were passed in, use them
            for city in extracted:
                self.crossed_cities.add(city)


class MotorizedConcentrationFactory(factory.django.DjangoModelFactory):
    """ Factory concentration motorisée """

    # Champs
    name = factory.Sequence(lambda n: 'Motorized Concentration{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    end_date = factory.LazyAttribute(lambda o: o.begin_date + timedelta(hours=8))
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000

    # Meta
    class Meta:
        model = MotorizedConcentration


class MotorizedEventFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation motorisée """

    # Champs
    name = factory.Sequence(lambda n: 'Motorized Event{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    end_date = factory.LazyAttribute(lambda o: o.begin_date + timedelta(hours=8))
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000

    # Meta
    class Meta:
        model = MotorizedEvent


class MotorizedRaceFactory(factory.django.DjangoModelFactory):
    """ Factory compétition motorisée """

    # Champs
    name = factory.Sequence(lambda n: 'Motorized Race{0}'.format(n))
    structure = factory.SubFactory(StructureFactory)
    begin_date = FuzzyDateTime(now()+timedelta(days=100), now()+timedelta(weeks=118))
    end_date = factory.LazyAttribute(lambda o: o.begin_date + timedelta(hours=8))
    departure_city = factory.SubFactory(CommuneFactory)
    number_of_entries = 666
    activite = factory.SubFactory(ActiviteFactory)
    max_audience = 5000

    # Meta
    class Meta:
        model = MotorizedRace
