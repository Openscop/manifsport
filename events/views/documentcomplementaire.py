# coding: utf-8
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView

from core.util.permissions import require_role
from ..forms import *
from ..models import *


class DocumentComplementaireRequestView(SuccessMessageMixin, CreateView):
    """ Demande de documents complémentaires """

    # Configuration
    model = DocumentComplementaire
    form_class = DocumentComplementaireRequestForm
    success_message = "Demande de documents complémentaires envoyée avec succès"

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        return super(DocumentComplementaireRequestView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.manifestation = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        return super(DocumentComplementaireRequestView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(DocumentComplementaireRequestView, self).get_context_data(**kwargs)
        context['manifestation'] = get_object_or_404(Manifestation, pk=self.kwargs['manifestation_pk'])
        return context

    def get_success_url(self):
        try:
            declaration = self.object.manifestation.manifestationdeclaration
        except (ObjectDoesNotExist, AttributeError):
            authorization = self.object.manifestation.manifestationautorisation
            return reverse('authorizations:authorization_detail', kwargs={'pk': authorization.pk})
        else:
            return reverse('declarations:declaration_detail', kwargs={'pk': declaration.pk})


class DocumentComplementaireProvideView(SuccessMessageMixin, UpdateView):
    """ Renseignement de documents complémentaires """

    # Configuration
    model = DocumentComplementaire
    form_class = DocumentComplementaireProvideForm
    success_message = "Documents complémentaires envoyés avec succès"
    success_url = reverse_lazy('events:dashboard')

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super(DocumentComplementaireProvideView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        self.object.provide_data()
        return super(DocumentComplementaireProvideView, self).form_valid(form)
