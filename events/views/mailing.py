# coding: utf-8
from braces.views import LoginRequiredMixin, SuperuserRequiredMixin
from django.views.generic.base import TemplateView

from administration.models import MairieAgent


class MailToOrganisateursView(LoginRequiredMixin, SuperuserRequiredMixin, TemplateView):
    """ Envoi de mail aux organisateurs """

    # Configuration
    template_name = 'events/mail_to_promoters.html'

    # Overrides
    def get_context_data(self, **kwargs):
        context = super(MailToOrganisateursView, self).get_context_data(**kwargs)
        context['mails'] = ",".join([organisateur.user.email for organisateur in Organisateur.objects.all()])
        return context


class MailToTownHallsView(LoginRequiredMixin, SuperuserRequiredMixin, TemplateView):
    """ Envoi de mail aux mairies """

    # Configuration
    template_name = 'events/mail_to_townhalls.html'

    # Overrides
    def get_context_data(self, **kwargs):
        context = super(MailToTownHallsView, self).get_context_data(**kwargs)
        context['mails'] = ",".join([agent.user.email for agent in MairieAgent.objects.all()])
        return context
