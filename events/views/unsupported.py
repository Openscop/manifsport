# coding: utf-8
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView

from administrative_division.models.departement import Departement
from events.forms import UnsupportedManifestationForm
from organisateurs.decorators import *
from ..models import *


class UnsupportedManifestationCreate(CreateView):
    """ Création de manifestation dans un département pas encore configuré """

    # Configuration
    template_name = 'events/event_unsupported_form.html'
    model = UnsupportedManifestation
    form_class = UnsupportedManifestationForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ N'autoriser l'accès qu'aux organisateurs """
        return super(UnsupportedManifestationCreate, self).dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        """ Renvoyer le formulaire initialisé """
        form = super().get_form(form_class)
        form.fields['departure_departement'].queryset = Departement.objects.unconfigured()
        form.fields['other_departments_crossed'].label = "Départements traversés"
        return form

    def form_valid(self, form):
        """ Méthode appelée lorsque le formulaire est valide, renvoie une réponse HTTP """
        self.object = form.save(commit=False)
        self.object.structure = self.request.user.organisateur.structure
        self.object.instance = None
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        """ Renvoyer les kwargs utilisés pour initialiser le formulaire """
        kwargs = super(UnsupportedManifestationCreate, self).get_form_kwargs()
        self.request.session['extradata'] = ""
        return kwargs
