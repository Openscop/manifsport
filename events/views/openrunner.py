# coding: utf-8
from django.urls import reverse
from django.http.response import HttpResponseRedirect

from administrative_division.models.departement import Departement
from events.views.base import ChoiceView


class CreateRoute(ChoiceView):

    # Configuration
    template_name = 'events/create_route.html'

    # Overrides
    def get(self, request, *args, **kwargs):
        """ Passer le choix Carto/Manif si Openrunner est désactivé sur l'instance """
        request_departement = request.GET.get('dept')
        if request_departement:
            departement = Departement.objects.get_by_name(request_departement)
            instance = departement.get_instance()
            openrunner_used = instance.uses_openrunner()
            if not openrunner_used:
                return HttpResponseRedirect('{dest}?dept={dept}'.format(dest=reverse('events:manifestation_choose'), dept=request_departement))
        return super().get(request, *args, **kwargs)

