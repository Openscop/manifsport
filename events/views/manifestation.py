# coding: utf-8
from django.forms.fields import CharField, IntegerField
from django.forms.models import ModelChoiceField
from django.forms.widgets import HiddenInput
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import DetailView
from django.views.generic.edit import UpdateView
from extra_views import CreateWithInlinesView, UpdateWithInlinesView

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from contacts.forms import ContactInline
from core.util.user import UserHelper
from events.forms import ManifestationInstructeurUpdateForm
from organisateurs.decorators import *
from ..models import *


class ManifestationDetail(DetailView):
    """ D2tail d'une manifestation """

    # Overrides
    @method_decorator(require_owner)
    def dispatch(self, *args, **kwargs):
        return super(ManifestationDetail, self).dispatch(*args, **kwargs)


class ManifestationCreate(CreateWithInlinesView):
    """ Création de manifestation """

    # Configuration
    inlines = [ContactInline]
    template_name = 'events/event_form.html'

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super(ManifestationCreate, self).dispatch(*args, **kwargs)

    def forms_valid(self, form, inlines):
        self.object = form.save(commit=False)
        self.object.instance = self.object.departure_city.get_instance()
        self.object.structure = self.request.user.organisateur.structure
        self.object.save()
        form.save_m2m()
        for formset in inlines:
            formset.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super(ManifestationCreate, self).get_form_kwargs()
        kwargs['routes'] = openrunner_api.get_user_routes(self.request.user.username)
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        request_departement = self.request.GET.get('dept')
        if request_departement:
            kwargs['initial'].update({'departure_departement': Departement.objects.get_by_name(request_departement).pk})
        return kwargs

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super(ManifestationCreate, self).get_form(form_class)
        form.target = form.instance
        # Remplacer les champs commune de départ et département si le département est passé dans l"URL
        if request_departement:
            form.fields['other_departments_crossed'].queryset = Departement.objects.exclude(name=request_departement)
            form.fields['departure_city'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                             label="Commune de départ")
            form.fields['departure_departement'] = IntegerField(widget=HiddenInput())
            form._meta.exclude += ('departure_departement',)
            departement = Departement.objects.get_by_name(request_departement)
            instance = departement.get_instance()
            if not instance.uses_openrunner():
                del form.fields['openrunner_route']
            elif instance.is_openrunner_map_required():
                form.fields['openrunner_route'].required = True
        return form


class ManifestationUpdate(UpdateWithInlinesView):
    """ Modifier une manifestation """

    # Configuration
    inlines = [ContactInline]
    template_name = 'events/event_form.html'

    # Overrides
    @method_decorator(require_owner)
    def dispatch(self, *args, **kwargs):
        return super(ManifestationUpdate, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super(ManifestationUpdate, self).get_form_kwargs()
        kwargs['routes'] = openrunner_api.get_user_routes(self.request.user.username)
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        kwargs['initial'].update({'departure_departement': self.object.departure_city.get_departement().pk})
        # Pour afficher les communes traversées du département de départ, il faut ajouter le département de départ à la liste des département traversés
        # Cela doit venir de clever-select
        # le kwarg écrase la valeur initiale tirée de la DB d'où l'ajout à la liste
        dept_trav = list(self.object.other_departments_crossed.all())
        dept_trav.append(self.object.departure_city.get_departement())
        kwargs['initial'].update({'other_departments_crossed': dept_trav})
        # On transforme le texte des routes/parcours en liste
        if kwargs['instance'].__dict__['openrunner_route']:
            kwargs['instance'].__dict__['openrunner_route'] = [int(route) for route in kwargs['instance'].__dict__['openrunner_route'].strip(',').split(',')]
        return kwargs

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        form.target = form.instance
        form.request = self.request

        # Remplacer les champs commune de départ et département si le département est passé dans l"URL
        if request_departement:
            form.fields['other_departments_crossed'].queryset = Departement.objects.exclude(name=request_departement)
            form.fields['departure_city'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                             label="Commune de départ")
            form.fields['departure_departement'] = IntegerField(widget=HiddenInput())
            form._meta.exclude += ('departure_departement',)
            departement = Departement.objects.get_by_name(request_departement)
            instance = departement.get_instance()
            if not instance.uses_openrunner():
                del form.fields['openrunner_route']
            elif instance.is_openrunner_map_required():
                form.fields['openrunner_route'].required = True
        return form


class ManifestationFilesUpdate(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    template_name = 'events/event_form.html'

    # Overrides
    @method_decorator(require_owner)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        return form


class ManifestationInstructeurUpdate(UpdateWithInlinesView):
    """ Formulaire instructeur """

    model = Manifestation
    form_class = ManifestationInstructeurUpdateForm
    template_name = 'events/event_form.html'

    # Overrides
    @method_decorator(require_role(['instructeur', 'mairieagent']))
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        """ URL lors de la validation du formulaire """
        return reverse("authorizations:authorization_detail", kwargs={'pk': self.object.manifestationautorisation.id})

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['show_structure_address'].required = False
        return form


class ManifestationPublicDetail(DetailView):
    """ Détails pyblics d'une manifestation """

    # Configuration
    model = Manifestation
    template_name = 'events/eventpublic_detail.html'

    def dispatch(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance.is_visible_in_calendar():
            if not UserHelper.has_role(request.user, 'instructeur'):
                if instance.structure.organisateur.user != request.user:
                    return render(request, 'core/access_restricted.html', status=403)
        return super().dispatch(request, *args, **kwargs)
