# coding: utf-8
from events.views.manifestation import ManifestationDetail, ManifestationCreate, ManifestationUpdate, ManifestationFilesUpdate
from ..forms import *
from ..models import *


class AutorisationNMDetail(ManifestationDetail):
    """ Détail de manifestation """

    # Configuration
    model = AutorisationNM


class AutorisationNMCreate(ManifestationCreate):
    """ Création d'une manifestation """

    # Configuration
    model = AutorisationNM
    form_class = AutorisationNMForm

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        # Remplacer le champ multisport_federation si le département est passé dans l"URL
        if request_departement:
            form.fields['multisport_federation'].queryset = Federation.objects.filter(multisports=True, departement__name=request_departement)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'anm'
        return context


class AutorisationNMUpdate(ManifestationUpdate):
    """ Modification d'une manifestation """

    # Configuration
    model = AutorisationNM
    form_class = AutorisationNMForm

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        # Remplacer le champ multisport_federation si le département est passé dans l"URL
        if request_departement:
            form.fields['multisport_federation'].queryset = Federation.objects.filter(multisports=True, departement__name=request_departement)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = 'anm'
        return context


class AutorisationNMFilesUpdate(ManifestationFilesUpdate):
    """ Ajout de fichiers à une manif """

    # Configuration
    model = AutorisationNM
    form_class = AutorisationNMFilesForm
