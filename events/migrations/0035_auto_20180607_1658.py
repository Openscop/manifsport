# Generated by Django 2.0.3 on 2018-06-07 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0034_merge_20180607_0921'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manifestation',
            name='crossed_cities',
            field=models.ManyToManyField(blank=True, help_text='Ne pas sélectionner la commune de départ.', related_name='events_manifestation_crossed_by', to='administrative_division.Commune', verbose_name='communes traversées'),
        ),
    ]
