# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-02-22 20:24
from __future__ import unicode_literals

from django.core.management import call_command
from django.db import migrations, models


def forwards_func(apps, schema_editor):
    """ Corrige des données incorrectes de manifestations """
    print()
    print("Correction des valeurs du champ 'number_of_entries'")
    Manifestation = apps.get_model('events', 'Manifestation')
    manifestations = Manifestation.objects.filter(number_of_entries__lt=0)
    count = manifestations.count()
    for manifestation in manifestations:
        manifestation.number_of_entries = abs(manifestation.number_of_entries)
        manifestation.save(force_update=True, update_fields=['number_of_entries'])
    print("{0} manifestations ont été corrigées".format(count))


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0015_auto_20170216_1650'),
    ]

    operations = [
        migrations.RunPython(forwards_func),
        migrations.AlterField(
            model_name='manifestation',
            name='number_of_entries',
            field=models.PositiveIntegerField(help_text='renseigner le nombre de participants de la précedente édition ou le nombre de participants estimé si première édition', verbose_name='nombre de participants'),
        ),
    ]
