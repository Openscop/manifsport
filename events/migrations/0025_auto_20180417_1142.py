# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-04-17 09:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0024_auto_20180417_1109'),
    ]

    operations = [
        migrations.AddField(
            model_name='autorisationnm',
            name='city_police',
            field=models.BooleanField(default=False, verbose_name="Disposerez-vous d'un encadrement de la police municipale ?"),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='city_police_detail',
            field=models.TextField(blank=True, default='', verbose_name='Précisez les moyens affectés'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='heading_vehicle',
            field=models.BooleanField(default=False, verbose_name='présence d\'un véhicule de début de course (véhicule "tête de course")'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='opening_vehicle',
            field=models.BooleanField(default=False, verbose_name='présence d\'un véhicule d\'ouverture (véhicule "pilote")'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='signalers_number',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='nombre de signaleurs'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='signalers_number_auto',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en voitures'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='signalers_number_moto',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='mobiles en motos'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='signalers_number_still',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='en postes fixes'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='staff_vehicle',
            field=models.BooleanField(default=False, verbose_name="présence d'autres véhicules d'organisation (auto ou moto)"),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='state_police',
            field=models.BooleanField(default=False, verbose_name='Avez-vous passez une convention avec la police nationale ou la gendarmerie ?'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='state_police_detail',
            field=models.TextField(blank=True, default='', help_text='joignez, dans la mesure du possible la convention', verbose_name='Précisez les moyens affectés'),
        ),
        migrations.AddField(
            model_name='autorisationnm',
            name='trailing_vehicle',
            field=models.BooleanField(default=False, verbose_name="présence d'un véhicule de fin de course"),
        ),
    ]
