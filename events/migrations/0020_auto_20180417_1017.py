# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-04-17 08:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0019_motorizedevent_participants'),
    ]

    operations = [
        migrations.AddField(
            model_name='motorizedconcentration',
            name='following',
            field=models.PositiveSmallIntegerField(default=0, verbose_name="nombre de véhicules d'accompagnement"),
        ),
        migrations.AddField(
            model_name='motorizedconcentration',
            name='meeting',
            field=models.PositiveSmallIntegerField(default=0, verbose_name='nombre de personnes aux points de rassemblement'),
        ),
        migrations.AddField(
            model_name='motorizedevent',
            name='following',
            field=models.PositiveSmallIntegerField(default=0, verbose_name="nombre de véhicules d'accompagnement"),
        ),
    ]
