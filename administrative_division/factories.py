# coding: utf-8
import factory
from factory.fuzzy import FuzzyDecimal

from .models import *


class DepartementFactory(factory.django.DjangoModelFactory):
    """ Factory pour les départements """

    name = factory.Sequence(lambda n: "{0}".format(n))
    instance = factory.RelatedFactory('core.factories.InstanceFactory', 'departement')
    cg = factory.RelatedFactory('administration.factories.CGFactory', 'departement')
    ddsp = factory.RelatedFactory('administration.factories.DDSPFactory', 'departement')
    edsr = factory.RelatedFactory('administration.factories.EDSRFactory', 'departement')
    ggd = factory.RelatedFactory('administration.factories.GGDFactory', 'departement')
    sdis = factory.RelatedFactory('administration.factories.SDISFactory', 'departement')
    codis = factory.RelatedFactory('administration.factories.CODISFactory', 'departement')

    class Meta:
        model = Departement


class ArrondissementFactory(factory.django.DjangoModelFactory):
    """ Factory pour les arrondissements """

    departement = factory.SubFactory('administrative_division.factories.DepartementFactory')
    code = factory.Sequence(lambda n: '{0}'.format(n))
    name = factory.Sequence(lambda n: 'Arrondissement{0}'.format(n))
    prefecture = factory.RelatedFactory('administration.factories.PrefectureFactory', 'arrondissement')

    class Meta:
        model = Arrondissement


class CommuneFactory(factory.django.DjangoModelFactory):
    """ Factory pour les communes """

    arrondissement = factory.SubFactory('administrative_division.factories.ArrondissementFactory')  # créer un arrondissement avec la commune
    name = factory.Sequence(lambda n: 'Commune{0}'.format(n))
    code = factory.Sequence(lambda n: '{0}'.format(n))
    zip_code = '42300'
    latitude = FuzzyDecimal(low=-90.0, high=90.0)
    longitude = FuzzyDecimal(low=-180.0, high=180.0)
    email = factory.Sequence(lambda n: 'commune{0}@example.com'.format(n))

    class Meta:
        model = Commune
