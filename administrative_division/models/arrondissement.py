# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import ManyPerDepartementMixin


class ArrondissementManager(models.Manager):
    """ Queryset des arrondissements """

    # Getter
    def by_departement(self, departement):
        """ Renvoyer les arrondissements pour le département """
        return self.filter(departement=departement)

    def get_by_natural_key(self, code):
        return self.get(code=code)


class Arrondissement(ManyPerDepartementMixin):
    """ Division de niveau 3 (arrondissement, 330+ en France Métropolitaine """

    # Champs
    code = models.CharField("Code", unique=True, max_length=4)
    name = models.CharField("Nom", max_length=255)
    objects = ArrondissementManager()

    # Overrides
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name

    def natural_key(self):
        return self.code,

    # Méta
    class Meta:
        verbose_name = "Arrondissement"
        verbose_name_plural = "Arrondissements"
        ordering = ['name']
        app_label = 'administrative_division'
        default_related_name = 'arrondissements'
