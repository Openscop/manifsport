# coding: utf-8
from django.db import models

from administrative_division.mixins.divisions import DepartementableMixin


class SecoursPublics(DepartementableMixin):
    """ Secours publics """

    # Champs
    name = models.CharField("nom", max_length=200)

    # Overrides
    def __str__(self):
        return self.name

    # Méta
    class Meta:
        verbose_name = "secours publics"
        verbose_name_plural = "secours publics"
        app_label = 'emergencies'
