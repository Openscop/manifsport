# coding: utf-8
from django.core.management.base import BaseCommand
from django.db.models import Q
from django.db import transaction

from evenements.models import Manif


class Command(BaseCommand):
    """
    Remplir les champs de sélection ajoutés pour les manifestations existantes
    """
    args = ''
    help = "Remplir leschamps de sélection pour les manifestations existantes'"

    def add_arguments(self, parser):
        parser.add_argument('--no-input', action='store_false', dest='interactive', default=True,
                            help="Ne pas demander de validation de l'utilisateur")

    def handle(self, *args, **options):
        """ Exécuter la commande """
        interactive = options.get('interactive')
        reply = ''

        if interactive is True:
            reply = input("Cette commande va remplir les champs de sélection pour les manifestations existantes. Continuer ? [oui/*]\n")

        if reply.lower() in ["oui", "o"] or not interactive:
            with transaction.atomic():
                for manif in Manif.objects.filter(Q(dnm__isnull=False) | Q(dnmc__isnull=False) | Q(dvtm__isnull=False)):
                    manif.voie_publique = True
                    manif.save()
                for manif in Manif.objects.filter(Q(dcnm__isnull=False) | Q(dcnmc__isnull=False)):
                    manif.voie_publique = True
                    manif.competition = True
                    manif.save()
                for manif in Manif.objects.filter(avtmcir__isnull=False):
                    manif.circuit_homologue = True
                    manif.vtm_hors_circulation = True
                    manif.save()
                # Concernant les manifs Avtm, on ne peut pas savoir quelles critères ont conduit au choix du formulaire
                # Voie_publique ou pas, circuit_non_permanent ou pas et donc le bool vtm_hors_circulation
        else:
            print("Commande abandonnée.")
