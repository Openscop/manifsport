# coding: utf-8
import datetime
import factory

from factory.fuzzy import FuzzyDate

from evaluation_incidence.models import (EvaluationRnr, EvaluationN2K, Natura2kDepartementConfig, Natura2kSiteConfig,
                                         N2kSite, RnrZoneConfig, RnrZone)
from evenements.factories import DnmFactory
from core.factories import InstanceFactory


class RNREvalFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation RNR """

    # Champs
    date_contact_administrateur = FuzzyDate(datetime.date(2010, 1, 1))
    participants = 1
    spectateurs = 1
    Longueur_totale_parcours = 1
    manif = factory.SubFactory(DnmFactory)

    # Meta
    class Meta:
        model = EvaluationRnr


class RnrZoneFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation motorisée """

    # Champs
    nom = factory.Sequence(lambda n: 'RNR_{0}'.format(n))
    code = '123456789'

    # Meta
    class Meta:
        model = RnrZone


class RnrZoneConfigFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation Natura2000 """

    # Champs
    formulaire = []
    rnrzone = factory.SubFactory(RnrZoneFactory)

    # Meta
    class Meta:
        model = RnrZoneConfig


class N2kEvalFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation Natura2000 """

    # Champs
    distance_site = 5
    Longueur_totale_parcours = 1
    manif = factory.SubFactory(DnmFactory)

    # Meta
    class Meta:
        model = EvaluationN2K


class N2kSiteFactory(factory.django.DjangoModelFactory):
    """ Factory manifestation motorisée """

    # Champs
    nom = factory.Sequence(lambda n: 'Natura2000_{0}'.format(n))
    site_type = 's'
    index = '123456789'

    # Meta
    class Meta:
        model = N2kSite


class N2kDepConfigFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation Natura2000 """

    # Champs
    vtm_formulaire = []
    nm_formulaire = []
    instance = factory.SubFactory(InstanceFactory)

    # Meta
    class Meta:
        model = Natura2kDepartementConfig


class N2kSiteConfigFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation Natura2000 """

    # Champs
    vtm_formulaire = []
    nm_formulaire = []
    n2ksite = factory.SubFactory(N2kSiteFactory)

    # Meta
    class Meta:
        model = Natura2kSiteConfig
