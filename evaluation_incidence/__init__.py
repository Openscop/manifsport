from django.apps import AppConfig


class EvaluationIncidenceConfig(AppConfig):
    name = 'evaluation_incidence'
    verbose_name = 'Evaluations d \'incidence'

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from evaluation_incidence import listeners


default_app_config = 'evaluation_incidence.EvaluationIncidenceConfig'
