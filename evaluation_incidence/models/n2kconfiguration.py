from django.db import models

from multiselectfield import MultiSelectField

from ..models import N2kSite
from evenements.models import Manif


class Natura2kDepartementConfig(models.Model):
    """
    Paramètres de configuration Natura 2000
    """
    instance = models.OneToOneField('core.instance', on_delete=models.CASCADE)
    pdesi = models.BooleanField(default=False, verbose_name="Département signataire PDESI")

    vtm_formulaire = MultiSelectField(blank=True, null=True, max_length=200, choices=Manif.CHOIX_VTM, verbose_name="Type de formulaires concernés")
    vtm_sur_siten2k = models.BooleanField(default=False, verbose_name="Manifestation sur un site Natura 2000")
    vtm_seuil_participants = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants")
    vtm_seuil_vehicules = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de véhicules pratiquants")
    vtm_seuil_total = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants + spectateurs + organisateurs")

    nm_formulaire = MultiSelectField(null=True, blank=True, max_length=200, choices=Manif.CHOIX_NM, verbose_name="Type de formulaires concernés")
    nm_sur_siten2k = models.BooleanField(default=False, verbose_name="Manifestation sur un site Natura 2000")
    nm_seuil_participants = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants")
    nm_seuil_total = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants + spectateurs + organisateurs")
    nm_hors_circulation = models.BooleanField(default=False, verbose_name="Hors voie ouverte à la circulation publique")

    def __str__(self):
        return "Configuration Natura2000 du département " + self.instance.get_departement().name

    class Meta:
        verbose_name = "configuration départementale Natura 2000"
        verbose_name_plural = "configurations départementales Natura 2000"
        app_label = 'evaluation_incidence'


class Natura2kSiteConfig(models.Model):
    """
    Paramètres de configuration Natura 2000
    """
    n2ksite = models.OneToOneField(N2kSite, on_delete=models.CASCADE)
    charte_dispense_acceptee = models.BooleanField(default=False, verbose_name="Signature de la charte de dispense")

    vtm_formulaire = MultiSelectField(max_length=200, null=True, blank=True, choices=Manif.CHOIX_VTM, verbose_name="Type de formulaires concernés")
    vtm_sur_siten2k = models.BooleanField(default=False, verbose_name="Manifestation sur un site Natura 2000")
    vtm_seuil_participants = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants")
    vtm_seuil_vehicules = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de véhicules pratiquants")
    vtm_seuil_total = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants + spectateurs + organisateurs")

    nm_formulaire = MultiSelectField(max_length=200, null=True, blank=True, choices=Manif.CHOIX_NM, verbose_name="Type de formulaires concernés")
    nm_sur_siten2k = models.BooleanField(default=False, verbose_name="Manifestation sur un site Natura 2000")
    nm_seuil_participants = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants")
    nm_seuil_total = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants + spectateurs + organisateurs")
    nm_hors_circulation = models.BooleanField(default=False, verbose_name="Hors voie ouverte à la circulation publique")

    def __str__(self):
        return "Configuration Natura2000 du site " + self.n2ksite.nom

    class Meta:
        verbose_name = "configuration site Natura 2000"
        verbose_name_plural = "configurations sites Natura 2000"
        app_label = 'evaluation_incidence'
