from django.db import models

from multiselectfield import MultiSelectField

from ..models import RnrZone
from evenements.models import Manif


class RnrZoneConfig(models.Model):
    """
    Paramètres de configuration pour une zone RNR
    """
    rnrzone = models.OneToOneField(RnrZone, on_delete=models.CASCADE)
    formulaire = MultiSelectField(max_length=200, null=True, blank=True, choices=Manif.CHOIX_MANIF, default='', verbose_name="Type de formulaires concernés")
    seuil_participants = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants")
    seuil_total = models.PositiveIntegerField(default=0, verbose_name="Seuil du nombre de participants + spectateurs + organisateurs")

    def __str__(self):
        return "Configuration RNR de la zone " + self.rnrzone.nom

    class Meta:
        verbose_name = "configuration de zone RNR"
        verbose_name_plural = "configurations de zone RNR"
        app_label = 'evaluation_incidence'
