# coding: utf-8
from django.views.generic import ListView, CreateView, UpdateView
from django.shortcuts import get_object_or_404, redirect
from django.utils.decorators import method_decorator

from evaluation_incidence.models import N2kSite, EvaluationN2K
from evaluation_incidence.forms import EvaluationN2kForm
from administrative_division.models.departement import Departement
from organisateurs.decorators import verifier_proprietaire
from evenements.models import Manif
from core.models import Instance


class SiteN2KListe(ListView):
    """ Afficher la liste des sites Natura 2000 """

    # Configuration
    model = N2kSite

    def get_queryset(self):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            return N2kSite.objects.filter(departements__name=departement).order_by('nom')
        return N2kSite.objects.all().order_by('nom')

    def get_context_data(self, **kwargs):
        instance = Instance.objects.get_for_request(self.request)
        departement = instance.get_departement().name if instance.get_departement() else ''
        context = super().get_context_data(**kwargs)
        param = self.kwargs.get('dept')
        if param and param.isdecimal() and param != '0':
            departement = param
        if departement and not param == '0':
            context['current_departement'] = departement
        context['departements'] = Departement.objects.all()
        return context


class N2kEvalCreate(CreateView):
    """ Création d'évaluation N2K """

    # Configuration
    model = EvaluationN2K
    form_class = EvaluationN2kForm

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        self.manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if EvaluationN2K.objects.filter(manif=self.manif).exists():
            return redirect(self.manif.get_absolute_url())
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        milieu = self.manif.activite.discipline.get_milieu_display()
        if 'diurne' not in kwargs['initial']:
            kwargs['initial'].update({'diurne': True})
        if milieu == 'Terrestre':
            kwargs['initial'].update({'terrestre': True})
        elif milieu == 'Nautique':
            kwargs['initial'].update({'nautique': True})
        elif milieu == 'Aérien':
            kwargs['initial'].update({'aerien': True})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        impact = 0
        if self.manif.get_type_manif() == 'avtm':
            if self.manif.cerfa.vehicules:
                if self.manif.cerfa.vehicules > 250:
                    impact = 2
                elif self.manif.cerfa.vehicules >= 100:
                    impact = 1
        context['impact'] = impact
        context['dept'] = self.manif.ville_depart.get_departement().name
        context['sites'] = self.manif.sites_natura2000.all()
        return context

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manif = self.manif
        return super().form_valid(form)


@method_decorator(verifier_proprietaire(), name='dispatch')
class N2kEvalUpdate(UpdateView):
    """ Modification de l'évaluation N2K """

    # Configuration
    model = EvaluationN2K
    form_class = EvaluationN2kForm

    # Overrides
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        manif = self.get_object().manif
        milieu = manif.activite.discipline.get_milieu_display()
        if milieu == 'Terrestre':
            kwargs['initial'].update({'terrestre': True})
        elif milieu == 'Nautique':
            kwargs['initial'].update({'nautique': True})
        elif milieu == 'Aérien':
            kwargs['initial'].update({'aerien': True})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        manif = self.get_object().manif
        impact = 0
        if manif.get_type_manif() == 'avtm':
            if manif.cerfa.vehicules:
                if manif.cerfa.vehicules > 250:
                    impact = 2
                elif manif.cerfa.vehicules >= 100:
                    impact = 1
        context['impact'] = impact
        context['dept'] = manif.ville_depart.get_departement().name
        context['sites'] = manif.sites_natura2000.all()
        return context
