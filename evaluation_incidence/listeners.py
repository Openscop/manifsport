# coding: utf-8
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.db import models
from django.core.exceptions import FieldDoesNotExist

from instructions.models import Instruction


if not settings.DISABLE_SIGNALS:

    @receiver(post_save, sender=Instruction)
    def Purge_description_fields(sender, instance, raw, created, **kwargs):
        """ Effacer les champs description pour lesquels le booléen associé est False """
        if created:
            if hasattr(instance.manif, 'n2kevaluation'):
                # Une évaluation N2K existe pour cette manifestation
                n2keval = instance.manif.n2kevaluation
                for field in n2keval._meta.get_fields():
                    if type(field) is models.BooleanField:
                        # Chercher les champs booléens
                        champ = getattr(n2keval, field.name)
                        if not champ:
                            # Si False, le champ description associé doit être vidé
                            champ_desc_name = "description_" + field.name
                            try:
                                champ_desc = n2keval._meta.get_field(champ_desc_name)
                                # Le champ description associé existe
                                if champ_desc:
                                    # Le champ est rempli
                                    setattr(n2keval, champ_desc.name, '')
                                    n2keval.save()
                            except FieldDoesNotExist:
                                pass
