from django.contrib import admin


class ConfigN2kFilter(admin.SimpleListFilter):
    title = 'Configuration de site'
    parameter_name = 'Configuration'

    def lookups(self, request, model_admin):
        return ('True', 'Avec configuration'), ('False', 'Sans configuration')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(natura2ksiteconfig__isnull=False)
        if self.value() == 'False':
            return queryset.filter(natura2ksiteconfig__isnull=True)


class OperateurN2kFilter(admin.SimpleListFilter):
    title = 'Opérateur de site'
    parameter_name = 'Opérateur'

    def lookups(self, request, model_admin):
        return ('True', 'Avec opérateur'), ('False', 'Sans opérateur')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(operateursiten2k__isnull=False)
        if self.value() == 'False':
            return queryset.filter(operateursiten2k__isnull=True)


class ConfigRnrFilter(admin.SimpleListFilter):
    title = 'Configuration de site'
    parameter_name = 'Configuration'

    def lookups(self, request, model_admin):
        return ('True', 'Avec configuration'), ('False', 'Sans configuration')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(rnrzoneconfig__isnull=False)
        if self.value() == 'False':
            return queryset.filter(rnrzoneconfig__isnull=True)


class OperateurRnrFilter(admin.SimpleListFilter):
    title = 'Opérateur de site'
    parameter_name = 'Opérateur'

    def lookups(self, request, model_admin):
        return ('True', 'Avec opérateur'), ('False', 'Sans opérateur')

    def queryset(self, request, queryset):
        if self.value() == 'True':
            return queryset.filter(administrateurrnr__isnull=False)
        if self.value() == 'False':
            return queryset.filter(administrateurrnr__isnull=True)
