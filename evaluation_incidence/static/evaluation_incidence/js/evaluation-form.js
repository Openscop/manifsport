/**
 * Created by knt on 23/02/17.
 */
$(document).ready(function () {
    $("textarea").autogrow({flickering: false, horizontal: false});

    // Mise en forme du formulaire
    $('div[id*="mesures"]').addClass('ml-5');
    $('div[id*="description"]').addClass('ml-5');
    $('.offset_remove .form-check').each(function () {
        $(this).parent().removeClass().addClass('col-sm-9');
    });

    // BOF Modification pour l'édition de l'évaluation
    // Marquer les champs de classe "requis"
    $('.requis').each(function () {
        $(this).parents('.form-group').append('<div class="col-sm-2 text-right completer"></div>');
        if ($(this).val() == '') {
            $(this).parents('.form-group').children('.completer').append('<i class="a-completer"></i> à completer</div>');
        }
    });
    AfficherIcones();
    // Gérer le marquage en fonction du remplissage
    $('body').on('change', '.requis', function () {
        if ($(this).val() != '') {
            $(this).parents('.form-group').children('.completer').empty();
        } else {
            $(this).parents('.form-group').children('.completer').append('<i class="a-completer"></i> à completer');
        }
        AfficherIcones();
    });
    // Gérer le marquage en fonction du remplissage pour les champs 'date'
    $('.requis').on('dp.change', function () {
        if ($(this).val() != '') {
            $(this).parents('.form-group').children('.completer').empty();
        } else {
            $(this).parents('.form-group').children('.completer').append('<i class="a-completer"></i> à completer');
        }
        AfficherIcones();
    });
    // EOF

    // BOF Gérer l'affichage des champs de description
    $('[name^="mesures"]').parents('.form-group').hide();
    $('[class^="mesures"][class*="ctx-help"]').hide();
    $('[type|="checkbox"]').each(function () {
        if ($(this).is(':checked')) {
            let desc_assoc = "mesures_" + $(this).attr("name");
            $('[name^="' + desc_assoc + '"]').parents('.form-group').show();
            $('[class^="' + desc_assoc + '_ctx-help"]').show();
        }
    });
    // Suivant selection
    $('[type|="checkbox"]').on('change', function () {
        let desc_assoc = "mesures_" + $(this).attr("name");
        let desc_div = $('[name^="' + desc_assoc + '"]').parents('.form-group');
        desc_div.toggle("fast", "swing");
        $('[class^="' + desc_assoc + '_ctx-help"]').toggle("slow");
        if (desc_div.is(":visible")) {
            $('[name^="' + desc_assoc + '"]').addClass('requis');
            if (desc_div.children('.completer').length == 0) {
                desc_div.append('<div class="col-sm-2 text-right completer"></div>');
            }
            desc_div.children('.completer').empty();
            if ($('[name^="' + desc_assoc + '"]').val() == '') {
                desc_div.children('.completer').append('<i class="a-completer"></i> à completer</div>');
            }
        }
        AfficherIcones();
    });
    // Gérer les champs visible avec milieu "Terrestre"
    if ($('[name="terrestre"]').length != 0 && !$('[name="terrestre"]').is(':checked')) {
        $('[name="lieu"]').parents('.form-group').hide();
        $('[name*="pietinement"]').parents('.form-group').hide();
        $('[class*="pietinement"]').hide();
    }
    $('body').on('change', '[name="terrestre"]', function () {
        $('[name="lieu"]').parents('.form-group').toggle("slow");
        $('[name="pietinement"]').parents('.form-group').toggle("slow");
        $('[class^="pietinement"]').toggle("slow");
        if ($('[name="pietinement"]').is(':checked')) {
            $('[name="mesures_pietinement"]').parents('.form-group').toggle("slow");
            $('[class*="mesures_pietinement"]').toggle("slow");
        }
    });
    // Affichage de l'impact
    function aff_impact(indice) {
        if (indice == 2) {
            $('#id_impact_calcule').val('Impact FORT');
        } else if (indice == 1) {
            $('#id_impact_calcule').val('Impact MOYEN');
        } else {
            $('#id_impact_calcule').val('Impact FAIBLE');
        }
    }
    // Calcul de l'impact
    // Variable "impact" déjà positionnée par la vue et transmise par le template
    // 2 pour avtm.vehicules > 250, 1 pour avtm.vehicules entre 100 et 250 et 0 pour avtm.vehicules < 100
    if ($('[name="sensibilite"]').is(':checked')) {
        if ($('.sitesn2k ul').children().length != 0) {
            impact = 2;
        }
    } else {
        if (impact != 2) {
            if ($('.sitesn2k ul').children().length != 0) {
                impact = 1;
            }
        }
    }
    $('body').on('change', '[name="sensibilite"]', function () {
        if ($('[name="sensibilite"]').is(':checked')) {
            if ($('.sitesn2k ul').children().length != 0) {
                impact = 2;
            }
        } else {
            if (impact != 2) {
                if ($('.sitesn2k ul').children().length != 0) {
                    impact = 1;
                }
            }
        }
        aff_impact(impact);
    });
    aff_impact(impact);

    // Gérer les champs dépendants. Pas de changement possible sur la page, les sites n2k ne sont pas modifiable içi
    if ($('#id_impact_calcule').val() == 'Impact FAIBLE') {
        $('[name*="emprise_amenagement"]').parents('.form-group').hide();
        $('[class*="emprise_amenagement"]').hide();
        $('[name*="public"]').parents('.form-group').hide();
        $('[class*="public"]').hide();
        $('[name*="parking"]').parents('.form-group').hide();
        $('[class*="parking"]').hide();
        $('[name*="engins_aeriens"]').parents('.form-group').hide();
        $('[class*="engins_aeriens"]').hide();
    }
});
