# coding: utf-8
from django.urls import path, re_path

from .views import ActionsListView, NotificationsListView


app_name = 'notifications'
urlpatterns = [
    re_path('(?P<pk>\d+)/(?P<path>\w+)/(?P<pko>\d+)/actions', ActionsListView.as_view(), name="actions_list"),
    re_path('(?P<pk>\d+)/(?P<path>\w+)/(?P<pko>\d+)/notifications', NotificationsListView.as_view(), name="notifications_list"),
]
