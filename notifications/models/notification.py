# coding: utf-8
import html
from smtplib import SMTPRecipientsRefused, SMTPException

import logging
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.core.mail import send_mail, get_connection
from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone

from core.util.types import one_line, make_iterable
from notifications.models.base import LogQuerySet
from core.models import User


mail_logger = logging.getLogger('smtp')


class NotificationQuerySet(LogQuerySet):
    """ Queryset des notifications """

    # Setter
    def notify_and_mail(self, recipients, subject, target, manifestation, template=None):
        """
        Ajouter une notification pour les destinataires et envoyer un mail

        Les destinataires sont des instances d'objet avec un champ email.
        Cela inclut les utilisateurs, mais aussi les CG, DDSP, Préfectures etc.
        Tous les destinataires peuvent recevoir un email, mais seuls les destinataires
        étant des Utilisateurs peuvent recevoir une notification via le site.

        :param recipients: destinataires
        :type recipients: [User] ou [tout type avec un attribut user]
        :param subject: Titre à intégrer dans le sujet du mail
        :param target: objet cible du mail, ou objet important (au choix)
        :param manifestation: manifestation ciblée par la notification
        :param template: chemin du template utilisé pour rendre le contenu du mail. à utiliser surtout pour les notifications spéciales
        """
        recipients = make_iterable(recipients)
        data = {'subject': subject, 'manifestation': manifestation, 'url': settings.MAIN_URL}
        gabarit = 'notifications/mail/message.txt'
        if hasattr(manifestation, 'instruction'):
            if manifestation.structure.organisateur in recipients:
                ref = manifestation.instruction.referent
                if ref.last_name:
                    data['nom'] = ref.last_name
                    data['prenom'] = ref.first_name
                    data['email'] = ref.email
                else:
                    data['nom'] = ref.username
                    data['email'] = ref.email
                if hasattr(ref, 'instructeur'):
                    data['lieu'] = 'en préfecture'
                else:
                    data['lieu'] = 'en mairie'
                gabarit = 'notifications/mail/message_organisateur.txt'
        if hasattr(manifestation, 'begin_date'):
            data['date'] = manifestation.begin_date
        else:
            data['date'] = manifestation.date_debut
        data['organisateur'] = manifestation.structure.organisateur
        message = render_to_string(template or gabarit, data)
        subject = one_line(render_to_string('notifications/mail/subject.txt', data))

        for recipient in recipients:
            recipient = getattr(recipient, 'user', recipient)
            # Envoyer un email si le champ email est disponible sur le destinataire
            if hasattr(recipient, 'email') and (type(recipient) is not User or (type(recipient) is User and recipient.is_active)):
                try:
                    sender_email = manifestation.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
                    email_config = manifestation.get_instance().get_email_settings()
                    connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
                    send_mail(subject=html.unescape(subject), message=html.unescape(message), from_email=sender_email,
                              recipient_list=[recipient.email], connection=connection)

                    # Créer la notification si le destinataire est un utilisateur
                    if isinstance(recipient, get_user_model()):
                        if type(manifestation)._meta.object_name != 'Manif':
                            self.create(user=recipient, manifestation=manifestation, subject=subject, content_object=target)
                        else:
                            self.create(user=recipient, manif=manifestation, subject=subject, content_object=target)

                except SMTPRecipientsRefused:
                    # En cas d'échec où l'adresse n'existe pas (ou similaire), notifier les instructeurs du dossier
                    # À noter que l'échec SMTP ne peut se produire qu'en situation de production
                    # donc à surveiller.
                    mail_logger.exception("notifications.notification.notify_and_mail: SMTP Recipients refused, trying to send to instructors")
                    if hasattr(recipient, 'has_role') and not recipient.has_role('instructeur'):  # évite d'éventuels problèmes de récursivité infinie
                        if hasattr(manifestation, 'instruction'):
                            instructeurs = manifestation.instruction.get_instructeurs()
                        else:
                            instructeurs = manifestation.get_autorisation().get_instructeurs()
                        self.notify_and_mail(instructeurs, "problème d'email", recipient, manifestation,
                                             template='notifications/mail/message-mailerror.txt')
                except SMTPException:
                    # Si le serveur SMTP pose un autre type de problème, logger l'erreur
                    exception_text = """
                    notifications.notification.notify_and_mail: Une exception SMTP non gérée vient de se produire
                    instance: {instance}
                    destinataires: {recipients}
                    manifestation: {manifestation}
                    cible: {target}
                    """
                    mail_logger.exception(exception_text.format(instance=manifestation.get_instance(), recipients=recipients,
                                                                manifestation=manifestation, target=target))


class Notification(models.Model):
    """ Notification à un agent concernant une modification sur une manifestation """

    # Champs
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='notifications', verbose_name="utilisateur", on_delete=models.CASCADE)
    creation_date = models.DateTimeField("date", default=timezone.now)
    subject = models.CharField("sujet", max_length=255)
    manifestation = models.ForeignKey("events.manifestation", verbose_name="manifestation", on_delete=models.CASCADE, null=True)
    manif = models.ForeignKey("evenements.manif", verbose_name="manif", on_delete=models.CASCADE, null=True)
    content_type = models.ForeignKey('contenttypes.contenttype', null=True, on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(null=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    read = models.BooleanField("lu", default=False)
    objects = NotificationQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        if self.manifestation:
            param = self.manifestation.name
        else:
            param = self.manif.nom
        return ' - '.join([self.user.username, param, self.subject])

    # Meta
    class Meta:
        verbose_name = "notification"
        verbose_name_plural = "notifications"
        default_related_name = "notifications"
        app_label = "notifications"
