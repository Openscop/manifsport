from django.core.exceptions import ValidationError
import os

"""
 Fonction pour verifier les types de fichiers envoyer dans la plateforme.
 Le premier est un validateur via form à mettre dans le model du fichier
 Le second étant un validateur quand on utilise pas de formulaire
"""


def file_type_validator(value):
    ext = value.name.split('.')[-1]
    valid_extensions = ['bmp', 'csv', 'doc', 'docx', 'dotx', 'eml', 'gbd', 'gif', 'htm', 'html', 'jpeg', 'jpg', 'kml',
                        'kmz', 'msg', 'odg', 'odp', 'ods', 'odt','ott', 'pdf', 'png', 'pps', 'ppsm', 'ppsx', 'ppt',
                        'pptm', 'pptx', 'rtf', 'tif', 'tiff', 'trk', 'txt', 'webp', 'xls', 'xlsm', 'xlsx', 'xml',]

    if not ext in valid_extensions:
        # ici on verifie si le fichier existe dans le server, dans ce cas où c'est un fichier déposé avant la création du validateur
        if not os.path.exists(value.path):
            raise ValidationError(u'Ce type de fichier n\'est pas pris en charge')


def file_type_valide_non_form(value):
    ext = value.name.split('.')[-1]
    valid_extensions = ['bmp', 'csv', 'doc', 'docx', 'dotx', 'eml', 'gbd', 'gif', 'htm', 'html', 'jpeg', 'jpg', 'kml',
                        'kmz', 'msg', 'odg', 'odp', 'ods', 'odt','ott', 'pdf', 'png', 'pps', 'ppsm', 'ppsx', 'ppt',
                        'pptm', 'pptx', 'rtf', 'tif', 'tiff', 'trk', 'txt', 'webp', 'xls', 'xlsm', 'xlsx', 'xml',]
    return True if ext in valid_extensions or os.path.exists(value.path) else False