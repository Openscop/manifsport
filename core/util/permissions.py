# coding: utf-8
from functools import wraps

from django.shortcuts import render
from django.http import HttpResponseForbidden

from core.util.types import make_iterable
from core.util.user import UserHelper


def require_role(rolename=None):
    """
    Décorateur de protection d'une vue

    Autorise l'accès à la vue pour les types d'agents décrits dans rolename

    :param rolename: une chaîne qui peut être ex. agent, edsragent, etc.
    :type rolename: str | list
    :returns: soit la réponse de la vue décorée, soit une page 403
    :rtype: django.http.response.HttpResponse
    """

    def renderer(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            user = request.user
            has_role = False
            rolenames = [i for i in make_iterable(rolename) if i]
            for name in rolenames:
                has_role |= UserHelper.has_role(user, name)
            if has_role:
                return function(request, *args, **kwargs)
            else:
                return render(request, 'core/access_restricted.html', status=403)

        return wrapper

    return renderer


def login_requis(alert=False):
    """
    Décorateur alternatif à login_required qui lui, redirige vers la page de login

    :returns: soit la réponse de la vue décorée, soit une page 403
    :rtype: django.http.response.HttpResponse
    """

    def renderer(function):
        @wraps(function)
        def wrapper(request, *args, **kwargs):
            user = request.user
            if user.is_authenticated:
                return function(request, *args, **kwargs)
            else:
                if not alert:
                    return HttpResponseForbidden(403)
                else:
                    return render(request, "core/access_restricted.html",
                                  {'message': "Vous n'êtes pas connecté !"}, status=403)

        return wrapper

    return renderer
