# coding: utf-8
import re
import logging
import pytz

from django.contrib.sites.shortcuts import get_current_site
from smtplib import SMTPRecipientsRefused, SMTPException
from django.core.mail import mail_admins, send_mail, get_connection
from django.conf import settings
from django import forms
from django.urls import reverse
from django.utils import timezone
from allauth.account.adapter import DefaultAccountAdapter
from allauth.account.models import EmailConfirmationHMAC

from core.util.user import UserHelper
from core.models import User


mail_logger = logging.getLogger('smtp')


class ManifsportAccountAdapter(DefaultAccountAdapter):
    """ Account adapter (Django Allauth) """

    def get_login_redirect_url(self, request):
        """ Renvoyer l'URL de redirection après connexion """

        # Utiliser le mécanisme par défaut pour la validation mais ne pas renvoyer l'URL
        base_home = super().get_login_redirect_url(request)
        user = request.user

        # bof Campagne de modification des mots de passe (définie manuellement depuis settings)
        if settings.DATE_CHANGE_PASSWORD:
            date_change = pytz.UTC.localize(timezone.datetime.strptime(settings.DATE_CHANGE_PASSWORD, settings.DATE_INPUT_FORMATS[0]))
            date_init = pytz.UTC.localize(timezone.datetime.strptime(settings.DATE_INIT_PASSWORD, settings.DATE_INPUT_FORMATS[0]))
            if timezone.now() > date_change:
                if not user.last_change_password or user.last_change_password < date_change:
                    if timezone.now() > date_init:
                        return reverse("password_expired")
                    request.session['date_limite_password'] = settings.DATE_INIT_PASSWORD
                    return reverse("account_change_password")
        # eof Campagne de modification des mots de passe (définie manuellement depuis settings)

        # diriger l'utilisateur vers la page adéquate pour démarrer sa navigation
        if UserHelper.has_role(user, 'organisateur'):
            return reverse('evenements:tableau-de-bord-organisateur')
        elif UserHelper.has_role(user, 'instructeur'):
            return reverse('instructions:tableaudebord')
        elif UserHelper.has_role(user, 'agent'):
            return reverse('instructions:tableaudebord')
        elif UserHelper.has_role(user, 'agentlocal'):
            return reverse('instructions:tableaudebord')
        else:
            return base_home

    def get_email_confirmation_redirect_url(self, request):
        """
        Orienter la vue suivante (EmailConfirmedView) pour changer le message affichée
        en fonction du role de l'utilisateur.
        S'il est organisateur, le compte est validé. Pour les autres, l'admin d'instance doit les valider
        """
        url = super().get_email_confirmation_redirect_url(request)
        try:
            # récupération de la clé founie pour valider l'adresse
            key = request.META['HTTP_REFERER'].split('/')[-2]
            # construire l'objet pour récupérer l'adresse puis l'id de l'utilisateur
            emailconfirmation = EmailConfirmationHMAC.from_key(key)
            user = User.objects.get(id=emailconfirmation.email_address.user_id)
            if user.has_role('organisateur'):
                url += "?valid=true"
            else:
                url += "?valid=false"
        except:
            pass
        return url

    def clean_username(self, username):
        """ Renvoyer un nom d'utilisateur valide depuis un nom d'utilisateur passé """
        if not re.match(r'^[a-zA-Z0-9]+$', username):
            raise forms.ValidationError("Le nom d'utilisateur ne peut contenir que des lettres non-accentuées et des chiffres")
        if len(username) < 3:  # Minimum 3 caractères (correspond aux limites de l'API Openrunner)
            raise forms.ValidationError("Le nom d'utilisateur doit contenir au moins 3 (trois) caractères")
        return super(ManifsportAccountAdapter, self).clean_username(username)

    def send_confirmation_mail(self, request, emailconfirmation, signup):
        """ Méthode de l'adapter par défaut, surchargée afin de gérer les exceptions dûes à send() """
        current_site = get_current_site(request)
        user = emailconfirmation.email_address.user
        activate_url = self.get_email_confirmation_url(
            request,
            emailconfirmation)
        ctx = {
            "user": user,
            "activate_url": activate_url,
            "current_site": current_site,
            "key": emailconfirmation.key,
        }
        if signup:
            email_template = 'account/email/email_confirmation_signup'
        else:
            email_template = 'account/email/email_confirmation'
        msg = self.render_mail(email_template, emailconfirmation.email_address.email, ctx)
        try:
            msg.send()
        except SMTPRecipientsRefused:
            mail_logger.exception("Allauth account adapter: SMTP Recipients refused")
            sender_email = user.get_instance().get_email() or settings.DEFAULT_FROM_EMAIL
            email_config = user.get_instance().get_email_settings()
            connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
            message = "Problème d'envoi d'un email de confirmation." + chr(10) + \
                      "Vérifier l'adresse mail de l'utilisateur " + user.username
            destinataires = User.objects.filter(groups__name__contains="Administrateurs d'instance").filter(
                default_instance__departement=user.get_instance().departement)
            if not destinataires:
                destinataires = User.objects.filter(groups__name__contains="Administrateurs techniques")
            try:
                send_mail(subject="SMTPRecipientsRefused", message=message, from_email=sender_email,
                          recipient_list=[destinataire.email for destinataire in destinataires], connection=connection)
            except:
                mail_admins("SMTPRecipientsRefused", "Allauth account adapter: SMTP Recipients refused")
        except SMTPException:
            # Si le serveur SMTP pose un autre type de problème, logger l'erreur
            mail_logger.exception("Allauth account adapter: Une exception SMTP non gérée vient de se produire")
            mail_admins("SMTPException", "Allauth account adapter: Une exception SMTP non gérée vient de se produire")
