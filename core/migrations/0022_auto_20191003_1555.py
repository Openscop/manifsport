# Generated by Django 2.2.1 on 2019-10-03 13:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0021_auto_20190911_1131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='last_name',
            field=models.CharField(default='à renseigner', max_length=50),
        ),
    ]
