# coding: utf-8
from django.contrib.auth.models import AbstractUser, UserManager as BaseUserManager
from django.db import models
from django.utils.html import format_html, mark_safe
from django.utils import timezone
from core.models.instance import Instance
from core.util.admin import set_admin_info
from core.util.user import UserHelper


class UserManager(BaseUserManager):
    """ Manager des utilisateurs """

    # Getter
    def active(self):
        """ Renvoyer les utilisateurs actifs """
        return self.filter(is_active=True)

    def agents(self, name):
        """
        Renvoyer des agents départementaux

        :param name: type d'agent à renvoyer, ex. 'ggdagent'
        """
        query = {'agent__{0}__isnull'.format(name): False}
        return self.filter(**query)

    def agentslocaux(self, name):
        """
        Renvoyer des agents locaux

        :param name: type d'agent à renvoyer, ex. 'cgdagentlocal'
        """
        query = {'agentlocal__{0}__isnull'.format(name): False}
        return self.filter(**query)

    def by_role(self, name):
        """
        Renvoyer des utilisateurs qui ont un rôle particulier

        :param name: ex. 'instructeur'
        :type name: str
        """
        query = {'{0}__isnull'.format(name): False}
        return self.filter(**query)


class User(AbstractUser):
    """ Modèle utilisateur """

    # Constantes
    STATUS = [[0, "Normal"], [1, "Nom d'utilisateur modifié"]]
    NORMAL, USERNAME_CHANGED = 0, 1

    # Champs
    default_instance = models.ForeignKey('core.instance', null=True, related_name='users', verbose_name="Instance de configuration", on_delete=models.SET_NULL)
    status = models.SmallIntegerField(choices=STATUS, default=NORMAL, verbose_name="Statut")
    last_name = models.CharField(default="à renseigner", null=False, blank=False, max_length=50)
    first_name = models.CharField(default="à renseigner", null=False, blank=False, max_length=50)
    last_visit = models.DateTimeField(blank=True, null=True)
    last_manif = models.IntegerField(blank=True, null=True)
    last_change_password = models.DateTimeField(blank=True, null=True, verbose_name="Date du dernier changement du mot de passe")
    tableau_role = models.CharField(max_length=250, verbose_name="Tableau des roles", blank=True, null=True)
    recap = models.BooleanField(default=True, verbose_name="Envoi du récapitulatif des messages")
    objects = UserManager()

    def save(self, *args, **kwargs):
        # Première sauvegarde, pk est None
        created = self.pk
        ancien_mdp = ""
        if created:
            ancien_mdp = User.objects.get(pk=self.pk).password
        super(User, self).save(*args, **kwargs)
        nouveau_mdp = self.password

        if created:
            # Mettre à jour last_change_password si le mot de passe a été changé et tracer
            if not ancien_mdp == nouveau_mdp:
                from messagerie.models import Message
                Message.objects.creer_et_envoyer('tracabilite', None, [self], "Mot de passe modifié", "Mot de passe modifié")
                self.last_change_password = timezone.now()
                self.save()
        else:
            #  Créer les options et le compteur de message avec l'utilisateur
            from core.models.optionuser import OptionUser
            option = OptionUser(user=self)
            option.save()
            from messagerie.models import CptMsg
            cpt = CptMsg(utilisateur=self)
            cpt.save()

    # Getter
    def get_instance(self):
        """ Renvoyer l'instance de configuration de workflow """
        return self.default_instance or Instance.objects.get_master()

    def get_departement(self):
        """ Renvoyer le département de l'instance de l'utilisateur """
        return self.get_instance().get_departement()

    @set_admin_info(short_description="Service")
    def get_service(self):
        """ Renvoyer le service de l'utilisateur """
        return UserHelper.get_service_instance(self)

    @set_admin_info(short_description="Rôles")
    def get_role(self):
        """ Renvoyer l'objet role de l'utilisateur """
        roles = UserHelper.get_role_names(self)
        if len(roles):
            list = []
            for role in roles:
                if role:
                    role_instance = UserHelper.get_role_instance(self, role)
                    if role_instance:
                        display = type(role_instance)._meta.object_name
                        url = role_instance.get_url()
                        list.append("<a href=" + url + ">" + display + "</a>")
            return format_html(' , '.join(list))
        return None

    def has_role(self, role):
        """ Renvoyer si l'utilisateur possède un rôle, donné par son nom par UserHelper.ROLE_TYPES """
        return UserHelper.has_role(self, role)

    def has_group(self, group_name):
        """ Renvoyer si l'utilisateur appartient à un groupe """
        return self.groups.filter(name=group_name).exists()

    @set_admin_info(short_description="Rôles")
    def get_friendly_role_names(self):
        """ Renvoyer le nom des rôles pour l'utilisateur """
        roles = UserHelper.get_role_names(self)
        return [UserHelper.ROLE_CHOICES_DICT[name] if name != '' else None for name in roles]

    @set_admin_info(short_description="Nom complet")
    def get_full_name(self):
        """ Renvoyer le nom complet de l'utilisateur """
        full_name = "{0} {1}".format(self.first_name.title(), self.last_name.upper()).strip()
        return full_name or self.username

    @set_admin_info(short_description="Nom complet avec pseudo")
    def get_full_name_and_username(self):
        """ Renvoyer le nom complet de l'utilisateur """
        full_name = "{0} {1} <spam class='pseudo-light'>{2}</span>".format(self.first_name.title(), self.last_name.upper(), self.username).strip()
        return mark_safe(full_name)

    # Setter
    def set_instance(self, instance):
        """
        Définir l'instance par défaut de l'utilisateur

        :param instance: nom du département ou instance (ex. '42') ou instance
        :type instance: core.models.Instance | str
        :return: True si l'opération a correctement été effectuée
        :rtype: bool
        """
        if isinstance(instance, str):
            instances = Instance.objects.filter(departement__name=instance)
            if instances.exists():
                self.default_instance = instances.first()
                return True
            return False
        else:
            self.default_instance = instance
            return True

    # Méta
    class Meta:
        verbose_name = "Utilisateur"
        verbose_name_plural = "Utilisateurs"
        app_label = 'core'


class OneToUserModelMixin:
    """ Mixin pour les clés naturelles des objets liés en 1:1 aux utilisateurs """

    # Overrides
    def natural_key(self):
        return self.user.username,


class OneToUserQuerySetMixin:
    """ Mixin pour les clés naturelles des objets liés en 1:1 aux utilisateurs """

    # Overrides
    def get_by_natural_key(self, username):
        return self.get(user__username=username)
