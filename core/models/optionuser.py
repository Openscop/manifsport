from django.db import models
from core.models import User


class OptionUser(models.Model):
    """
    Model pour les options des utilisateurs
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # Affichage messagerie
    conversation_msg = models.BooleanField(default=True)
    nouveaute_msg = models.BooleanField(default=True)
    action_msg = models.BooleanField(default=True)
    notification_msg = models.BooleanField(default=True)
    tracabilite_msg = models.BooleanField(default=False)
    # Envoi de mail
    conversation_mail = models.BooleanField(default=False)
    nouveaute_mail = models.BooleanField(default=False)
    action_mail = models.BooleanField(default=False)
    notification_mail = models.BooleanField(default=False)
    tracabilite_mail = models.BooleanField(default=False)
    # Envoi de notification push
    conversation_push = models.BooleanField(default=True)
    nouveaute_push = models.BooleanField(default=True)
    action_push = models.BooleanField(default=True)
    notification_push = models.BooleanField(default=True)
    tracabilite_push = models.BooleanField(default=False)

    def __str__(self):
        return "Option de " + self.user.username + " (" + self.user.first_name + " " + self.user.last_name + ")"