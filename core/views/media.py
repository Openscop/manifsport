import urllib

from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.conf import settings

from evenements.models import Manif


def media_access(request, path):
    """
    Fonction pour verifier le droit d'acces au fichier media
    - Fichier ckeditor ou tmp tout le monde
    - admin techniques : tout les droits
    - admin instance & instructeur & mairie agent : tous ceux de son instance
    - organisateur ceux de ses manif
    - autres selon le droit d'acces à la manif en question

    Nessecite de configurer Nginx avec le code suivant

    location /protected/
        {
            internal;
            alias /var/www/html/;
        }

    """
    # cas de fichiers publics d'aide
    if path.split('/')[0] == "uploads" or path.split('/')[0] == "exportation":
        response = HttpResponse()
        path = urllib.parse.quote(path)
        # Content-type will be detected by nginx
        del response['Content-Type']
        response['X-Accel-Redirect'] = '/protected/media/' + path
        return response

    # cas utilisateur non auth
    if not request.user.is_authenticated:
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")

    # cas de fichiers de manifestation
    manif_id = path.split("/")[1]
    if not manif_id.isdigit():
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")

    manif = get_object_or_404(Manif, pk=manif_id)
    user = request.user
    ok = False
    if user.has_group('Administrateurs techniques'):
        ok = True
    elif user.has_group("Administrateurs d'instance"):
        if manif.get_instance() == user.get_instance():
            ok = True
    elif hasattr(user, 'organisateur') and user.organisateur == manif.get_organisateur():
        ok = True
    elif hasattr(manif, "instruction"):
        if hasattr(user, 'instructeur'):
            from core.models import Instance
            config_instructeur = user.get_instance().get_instruction_mode()
            if config_instructeur == Instance.IM_ARRONDISSEMENT:
                if not user.get_instance().acces_arrondissement:
                    if user.instructeur.get_prefecture().arrondissement == manif.ville_depart.arrondissement:
                        ok = True
                else:
                    if manif.get_instance() == user.get_instance():
                        ok = True
            else:
                if manif.get_instance() == user.get_instance():
                    ok = True
        elif user.has_role('agent') and manif.instruction.get_avis_user(user):
            ok = True
        elif user.has_role('mairieagent') and manif.ville_depart == user.get_service():
            if not manif.villes_traversees.all() and not hasattr(manif.get_cerfa(), 'vehicules'):
                ok = True
        elif user.has_role('agentlocal') and manif.instruction.get_preavis_user(user):
            ok = True

    if ok:
        response = HttpResponse()
        # Content-type will be detected by nginx
        del response['Content-Type']
        # gunicorn et nginx ne semble pas vouloir se comprendre cette ligne permet de mettre le bon encodage
        path = urllib.parse.quote(path)
        response['X-Accel-Redirect'] = '/protected/media/' + path
        if settings.TESTS_IN_PROGRESS:
            response.content = 'ok pour voir le fichier'
        return response
    else:
        return HttpResponseForbidden("Vous n'avez pas les droits pour voir ce fichier")
