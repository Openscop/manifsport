from __future__ import absolute_import, unicode_literals
from configuration.celery import app
from evenements.templatetags.thumbail_filter import createthumbnail


@app.task(bind=True)
def creation_thumbail_manif(self, pk):
    from evenements.models import Manif
    manif = Manif.objects.get(pk=pk)
    for field in manif.cerfa._meta.get_fields():
        if field.__class__.__name__ == "FileField":
            if getattr(manif.cerfa, field.attname):
                createthumbnail(getattr(manif.cerfa, field.attname)) if getattr(manif.cerfa,
                                                                                       field.attname).file else ''


@app.task(bind=True)
def creation_thumbail_doc_complementaire(self, pk):
    from evenements.models import DocumentComplementaire
    doc = DocumentComplementaire.objects.get(pk=pk)
    if doc.document_attache:
        createthumbnail(doc.document_attache)


@app.task(bind=True)
def creation_thumbail_doc_officiel(self, pk):
    from instructions.models import DocumentOfficiel
    doc = DocumentOfficiel.objects.get(pk=pk)
    if doc.fichier:
        createthumbnail(doc.fichier)


@app.task(bind=True)
def creation_thumbail_pj_avis(self, pk):
    from instructions.models import PieceJointeAvis
    pj = PieceJointeAvis.objects.get(pk=pk)
    if pj.fichier:
        createthumbnail(pj.fichier)