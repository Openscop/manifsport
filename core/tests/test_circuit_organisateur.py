from django.test import TestCase
from core.models import Instance
from django.contrib.auth.hashers import make_password as make
from django.utils.timezone import timedelta, datetime
from allauth.account.models import EmailAddress
import re, os

from core.factories import UserFactory
from core.models import User
from administration.factories import InstructeurFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory, OrganisateurFactory
from sports.factories import ActiviteFactory
from contacts.factories import ContactFactory


class Circuit_OrganisateurTests(TestCase):
    """
    Test du circuit organisateur
        workflow_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut
    """
    @classmethod
    def setUpTestData(cls):
        """
        Préparation du test
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========= Circuit organisateur =========')
        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        cls.other_dep = DepartementFactory.create(name='43',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        prefecture = arrondissement.prefecture
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur',
                                              password=make(123),
                                              default_instance=cls.dep.instance)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.instructeur = UserFactory.create(username='instructeur',
                                             password=make(123),
                                             default_instance=cls.dep.instance)
        InstructeurFactory.create(user=cls.instructeur, prefecture=prefecture)

        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.activ = ActiviteFactory.create()
        cls.contact = ContactFactory.create()

        os.chdir("/tmp")
        for file in ('manifestation_rules', 'organisateur_commitment', 'safety_provisions', 'signalers_list', 'participants',
                     'tech_organisateur_certificate', 'commissioners', 'hourly_itinerary', 'rounds_safety'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

    @classmethod
    def tearDownClass(cls):
        """
        Suppression des fichiers créés dans /tmp
        """
        os.chdir("/tmp")
        for file in ('manifestation_rules', 'organisateur_commitment', 'safety_provisions', 'signalers_list', 'participants',
                     'tech_organisateur_certificate', 'commissioners', 'hourly_itinerary', 'rounds_safety'):
            os.remove(file+".txt")
        super(Circuit_OrganisateurTests, cls).tearDownClass()

    def donnees_formulaire(self, reponse):
        """
        Récupération des données du formulaire concernant le formset de contact qui a des données à restituer
        et remplissage des champs communs
        :param reponse: reponse précédente
        :return: data à poster
        """
        data = {}
        # Informations générales
        data['csrf_token'] = reponse.context['csrf_token']

        # information du management_form , nécessaire à cause du formset
        management_form = reponse.context['formset'].management_form
        for i in 'TOTAL_FORMS', 'INITIAL_FORMS', 'MIN_NUM_FORMS', 'MAX_NUM_FORMS':
            data['%s-%s' % (management_form.prefix, i)] = management_form[i].value()

        for i in range(reponse.context['formset'].total_form_count()):
            # récupérer l'index 'i' du formulaire du formset
            current_form = reponse.context['formset'].forms[i]

            # remplir tous les champs du formulaire
            for field_name in current_form.fields:
                value = current_form[field_name].value()
                if value is not None:
                    data['%s-%s' % (current_form.prefix, field_name)] = value
                else:
                    data['%s-%s' % (current_form.prefix, field_name)] = getattr(self.contact, field_name)

        data['name'] = 'La grosse course'
        data['description'] = 'une grosse course'
        data['activite'] = self.activ.pk
        data['number_of_entries'] = 100
        data['max_audience'] = 15
        data['route_descriptions'] = 'parcours long'
        data['departure_city'] = self.commune.pk
        data['crossed_cities'] = self.autrecommune.pk
        data['departure_departement'] = self.commune.arrondissement.departement.pk
        """
        print('#' * 30)
        for i in sorted(data.keys()):
            print(i, '\t:', data[i])
        print('#' * 30)
        """
        return data

    def joindre_fichiers(self, reponse, type='a'):
        """
        Ajout des fichiers nécessaires à la manifestation
        :param reponse: reponse précédente
        :param type: autorisation (a ou m) ou declaration
        :return: reponse suivante
        """
        #appel de la vue pour joindre les fichiers
        joindre = re.search('href="(?P<url>(/[^"]+)).+\\n.+Joindre des documents', reponse.content.decode('utf-8'))
        if hasattr(joindre, 'group'):
            print(joindre.group('url'))
            reponse = self.client.get(joindre.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Fichiers attachés', count=1)

        # Ajout des fichiers nécessaires
        with open('/tmp/manifestation_rules.txt') as file1:
            self.client.post(joindre.group('url'), {'manifestation_rules': file1}, follow=True, HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/organisateur_commitment.txt') as file2:
            self.client.post(joindre.group('url'), {'organisateur_commitment': file2}, follow=True, HTTP_HOST='127.0.0.1:8000')
        with open('/tmp/safety_provisions.txt') as file3:
            reponse = self.client.post(joindre.group('url'), {'safety_provisions': file3}, follow=True, HTTP_HOST='127.0.0.1:8000')
        if type == 'a':
            with open('/tmp/signalers_list.txt') as file4:
                self.client.post(joindre.group('url'), {'signalers_list': file4}, follow=True, HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/rounds_safety.txt') as file8:
                reponse = self.client.post(joindre.group('url'), {'rounds_safety': file8}, follow=True, HTTP_HOST='127.0.0.1:8000')
        if type == 'm':
            with open('/tmp/hourly_itinerary.txt') as file5:
                self.client.post(joindre.group('url'), {'hourly_itinerary': file5}, follow=True, HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/tech_organisateur_certificate.txt') as file6:
                self.client.post(joindre.group('url'), {'tech_organisateur_certificate': file6}, follow=True, HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/commissioners.txt') as file7:
                self.client.post(joindre.group('url'), {'commissioners': file7}, follow=True, HTTP_HOST='127.0.0.1:8000')
            with open('/tmp/participants.txt') as file9:
                reponse = self.client.post(joindre.group('url'), {'participants': file9}, follow=True, HTTP_HOST='127.0.0.1:8000')
        return reponse

    def declarer_manif(self, reponse, type='a'):
        """
        Soumettre la manifestation à l'instructeur
        :param reponse: reponse précédente
        :param type: autorisation ou declaration
        :return: reponse suivante
        """
        #appel de la vue pour déclarer la manifestation
        if type == 'd':
            declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer la manifestation', reponse.content.decode('utf-8'))
        else:
            declar = re.search('href="(?P<url>(/[^"]+)).+\\n.+Demander l\'autorisation', reponse.content.decode('utf-8'))
        if hasattr(declar, 'group'):
            print(declar.group('url'))
            reponse = self.client.get(declar.group('url'), HTTP_HOST='127.0.0.1:8000')
        if type == 'd':
            self.assertContains(reponse, 'Déclaration de manifestation', count=2)
        else:
            self.assertContains(reponse, 'Autorisation de manifestation sportive', count=2)
        #Soumettre la déclaration
        reponse = self.client.post(declar.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        return reponse

    def verification_instructeur(self, type='a'):
        """
        Vérification dans la dashborad instructeur de l'affichage de la manifestation crée
        :param type: autorisation ou declaration
        :return:
        """
        # Connexion avec l'instructeur
        self.assertTrue(self.client.login(username=self.instructeur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/authorizations/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)
        self.assertContains(reponse, 'La Grosse Course')
        self.assertContains(reponse, 'list-group-item-danger', count=1)
        self.assertNotContains(reponse, 'list-group-item-warning')
        self.assertNotContains(reponse, 'list-group-item-success')
        #print(reponse.content.decode('utf-8'))
        #f = open('/var/log/manifsport/test_output.html', 'w')
        #f.write(reponse.content.decode('utf-8'))
        #f.close()
        if type == "d":
            self.assertContains(reponse, 'déclaration reçue')
        else:
            self.assertContains(reponse, 'autorisation envoyée')

    def skip_test_1_Inscription(self):
        """
        Test de l'inscription d'un organisateur
        """
        print('**** test Inscription ****')
        reponse = self.client.post('/inscription/organisateur',
                                   {'first_name': 'Jean',
                                    'last_name': 'Dupont',
                                    'username': 'jandup',
                                    'email': 'jdupont@exemple.com',
                                    'password1': 'azeaz5646dfzdsf',
                                    'password2': 'azeaz5646dfzdsf',
                                    'cgu': True,
                                    'structure_name': 'MonAssoc',
                                    'type_of_structure': self.structure.pk,
                                    'address': '12 rue du repos éternel',
                                    'departement': self.dep.pk,
                                    'commune': self.commune.pk,
                                    'phone': '04 77 74 33 55',
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Vérifiez votre adresse email', count=2)
        email = EmailAddress.objects.last()
        email.verified = True
        email.save()
        user = User.objects.get(username='jandup')
        user.is_active = True
        user.save()
        reponse = self.client.post('/accounts/login/',
                                   {'login': 'jandup',
                                    'password': 'azeaz5646dfzdsf',
                                    },
                                   follow=True,
                                   HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Connexion avec jandup réussie', count=1)

    def skip_test_2_DeclarationNM(self):
        """
        Test d'une déclarationNM
        """
        print('**** test DéclarationNM ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle ', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation sans VTM (nonmotorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive sans', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Vous organisez une compétition', count=1)
        self.assertContains(reponse, ' Oui</h3>', count=1)
        self.assertContains(reponse, ' Non</h3>', count=1)

        # Appel de la vue  liée à Non (DeclarationNM/choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Non</h3>', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Nombre de participants', count=1)
        self.assertContains(reponse, ' Oui</h3>', count=1)
        self.assertContains(reponse, ' Non</h3>', count=1)

        # Appel de la vue  liée à OUI (DeclarationNM/add)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Oui</h3>', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['signalers_number'] = 3
        data['signalers_number_still'] = 1
        data['signalers_number_auto'] = 1
        data['signalers_number_moto'] = 1
        data['grouped_traffic'] = 'p'
        data['support_vehicles_number'] = 0
        data['begin_date'] = datetime.now() + timedelta(days=29)
        data['end_date'] = datetime.now() + timedelta(days=29, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=31)
        data['end_date'] = datetime.now() + timedelta(days=31, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)

        reponse = self.joindre_fichiers(reponse, 'd')

        reponse = self.declarer_manif(reponse, 'd')

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur('d')

    def skip_test_3_DeclarationVTM(self):
        """
        Test d'une déclarationVTM
        """
        print('**** test DéclarationVTM ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle ', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation avec VTM (motorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive avec', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive sur voie', count=1)
        self.assertContains(reponse, 'Manifestation hors voie', count=1)
        # Appel de la vue  manifestation sur voie publique (publicroadmotorizedevents/choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive sur voie publique',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Concentration de véhicules', count=1)
        self.assertContains(reponse, 'manifestation avec véhicules', count=1)

        # Appel de la vue  concentration de véhicules (motorisedconcentrations/add)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Concentration de véhicules',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['vehicles'] = 'deux rolls'
        data['following'] = 1
        data['meeting'] = 2
        data['begin_date'] = datetime.now() + timedelta(days=59)
        data['end_date'] = datetime.now() + timedelta(days=59, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=61)
        data['end_date'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)

        reponse = self.joindre_fichiers(reponse, 'd')

        reponse = self.declarer_manif(reponse, 'd')

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur('d')

    def skip_test_4_AutorisationNM(self):
        """
        Test d'une autorisationNM
        """
        print('**** test AutorisationNM ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle manifestation',
                            reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation sans VTM (nonmotorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive sans participation',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Vous organisez une compétition', count=1)
        self.assertContains(reponse, ' Oui</h3>', count=1)
        self.assertContains(reponse, ' Non</h3>', count=1)

        # Appel de la vue  liée à Oui (DeclarationNM/choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Oui</h3>', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['signalers_number'] = 3
        data['signalers_number_still'] = 1
        data['signalers_number_auto'] = 1
        data['signalers_number_moto'] = 1
        data['begin_date'] = datetime.now() + timedelta(days=59)
        data['end_date'] = datetime.now() + timedelta(days=59, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=61)
        data['end_date'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def skip_test_5_AutorisationNM_2dep(self):
        """
        Test d'une autorisationNM qui traverse deux departements
        """
        print('**** test AutorisationNM sur deux départements ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle manifestation',
                            reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation sans VTM (nonmotorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive sans participation',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Vous organisez une compétition', count=1)
        self.assertContains(reponse, ' Oui</h3>', count=1)
        self.assertContains(reponse, ' Non</h3>', count=1)

        # Appel de la vue  liée à Oui (DeclarationNM/choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Oui</h3>', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['signalers_number'] = 3
        data['signalers_number_still'] = 1
        data['signalers_number_auto'] = 1
        data['signalers_number_moto'] = 1
        data['other_departments_crossed'] = (self.other_dep.pk,)
        data['begin_date'] = datetime.now() + timedelta(days=89)
        data['end_date'] = datetime.now() + timedelta(days=89, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=91)
        data['end_date'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def skip_test_6_AutorisationVTM(self):
        """
        Test d'une autorisationVTM
        """
        print('**** test AutorisationVTM ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle manifestation',
                            reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation avec VTM (motorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive avec participation',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive sur voie', count=1)
        self.assertContains(reponse, 'Manifestation hors voie', count=1)

        # Appel de la vue  manifestation sur voie publique (publicroadmotorizedevents/choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive sur voie publique',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Concentration de véhicules', count=1)
        self.assertContains(reponse, 'manifestation avec véhicules', count=1)

        # Appel de la vue  manifestation avec véhicules (motorizedevents/add)
        action = re.search('href="(?P<url>(/mot[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Une manifestation avec véhicules',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['vehicles'] = 'deux rolls'
        data['following'] = 1
        data['begin_date'] = datetime.now() + timedelta(days=89)
        data['end_date'] = datetime.now() + timedelta(days=89, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=91)
        data['end_date'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)
        self.assertContains(reponse, 'Liste des postes commissaires', count=1)
        self.assertContains(reponse, 'Attestation organisateur technique', count=1)
        self.assertContains(reponse, 'Itinéraire horaire', count=1)
        self.assertContains(reponse, 'Liste Complète des participants', count=1)

        reponse = self.joindre_fichiers(reponse, 'm')
        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def skip_test_7_AutorisationVTMCIR(self):
        """
        Test d'une autorisationVTMCIR
        """
        print('**** test AutorisationVTMCIR ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle manifestation',
                            reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation avec VTM (motorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive avec participation',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive sur voie', count=1)
        self.assertContains(reponse, 'Manifestation hors voie', count=1)

        # Appel de la vue  manifestation sur circuit (motorizedraces/add)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation hors voie publique',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['support'] = 's'
        data['begin_date'] = datetime.now() + timedelta(days=89)
        data['end_date'] = datetime.now() + timedelta(days=89, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=91)
        data['end_date'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def skip_test_8_AutorisationVTMC(self):
        """
        Test d'une autorisationVTMC
        """
        print('**** test autorisationVTMC ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle ', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation avec VTM (motorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive avec', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive sur voie', count=1)
        self.assertContains(reponse, 'Manifestation hors voie', count=1)

        # Appel de la vue  manifestation sur voie publique (publicroadmotorizedevents/choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive sur voie publique',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Concentration de véhicules', count=1)
        self.assertContains(reponse, 'manifestation avec véhicules', count=1)

        # Appel de la vue  concentration de véhicules (motorisedconcentrations/add)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Concentration de véhicules',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['vehicles'] = 'deux rolls'
        data['big_concentration'] = True
        data['following'] = 1
        data['meeting'] = 2
        data['begin_date'] = datetime.now() + timedelta(days=89)
        data['end_date'] = datetime.now() + timedelta(days=89, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=91)
        data['end_date'] = datetime.now() + timedelta(days=91, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

    def skip_test_9_AutorisationVTMCIR_Circuit_Homologue(self):
        """
        Test d'une autorisationVTMCIR
        """
        print('**** test AutorisationVTMCIR sur circuit homologue ****')
        # Connexion avec l'utilisateur
        self.assertTrue(self.client.login(username=self.organisateur, password='123'))
        # Appel de la vue dashboard
        reponse = self.client.get('/dashboard/', HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Tableau de bord', count=3)

        # Appel de la vue nouvelle manifestation (choose_start)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Déclarer une nouvelle manifestation',
                            reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Sélectionnez le département')
        self.assertContains(reponse, '42 - Loire', count=1)
        self.assertContains(reponse, 'Autre département', count=1)

        # Appel de la vue après sélection du département (create_route redirigée sur choose)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+42 - Loire', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive avec', count=1)
        self.assertContains(reponse, 'Manifestation sportive sans', count=1)

        # Appel de la vue manifestation avec VTM (motorizedevents)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation sportive avec participation',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'type de manifestation sportive')
        self.assertContains(reponse, 'Manifestation sportive sur voie', count=1)
        self.assertContains(reponse, 'Manifestation hors voie', count=1)

        # Appel de la vue  manifestation sur circuit (motorizedraces/add)
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+\\n.+\\n.+\\n.+\\n.+\\n.+Manifestation hors voie publique',
                           reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date trop rapprochée
        data = self.donnees_formulaire(reponse)
        data['support'] = 's'
        data['circuit_homologue'] = True
        data['begin_date'] = datetime.now() + timedelta(days=59)
        data['end_date'] = datetime.now() + timedelta(days=59, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertContains(reponse, 'Il est maintenant trop tard', count=1)

        # Rappel du formulaire
        action = re.search('href="(?P<url>(/[^"]+)).+\\n.+Modifier', reponse.content.decode('utf-8'))
        if hasattr(action, 'group'):
            print(action.group('url'))
            reponse = self.client.get(action.group('url'), follow=True, HTTP_HOST='127.0.0.1:8000')
        # Formulaire principal de création de manifestation
        self.assertContains(reponse, 'Organisation d\'une manifestation sportive', count=2)

        # Remplissage du formulaire avec une date correcte
        data['begin_date'] = datetime.now() + timedelta(days=61)
        data['end_date'] = datetime.now() + timedelta(days=61, hours=8)
        reponse = self.client.post(action.group('url'), data, follow=True, HTTP_HOST='127.0.0.1:8000')
        self.assertNotContains(reponse, 'Il est maintenant trop tard')

        # Test de la couleur des boutons 'etape' après redirection sur la vue de détail
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'danger', msg='pas de bouton rouge Déclaration')
        # Fichiers manquants
        self.assertContains(reponse, 'Réglement de la manifestation', count=1)
        self.assertContains(reponse, 'Déclaration et engagement de l\'organisateur', count=1)
        self.assertContains(reponse, 'Dispositions prises pour la sécurité', count=1)
        # Apparait dans la détail de la manifestation : si manquant count = 2
        self.assertContains(reponse, 'Cartographie de parcours', count=1)

        reponse = self.joindre_fichiers(reponse)

        reponse = self.declarer_manif(reponse)

        # retour à la vue de détail et test
        etape1 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 1', reponse.content.decode('utf-8'))
        if hasattr(etape1, 'group'):
            self.assertEqual(etape1.group('etat'), 'success', msg='pas de bouton vert Description')
        etape2 = re.search('class="btn btn-(?P<etat>([a-z]+)).+Étape 2', reponse.content.decode('utf-8'))
        if hasattr(etape2, 'group'):
            self.assertEqual(etape2.group('etat'), 'success', msg='pas de bouton vert Déclaration')

        self.verification_instructeur()

        #print(reponse.content.decode('utf-8'))
        #f = open('/var/log/manifsport/test_output.html', 'w', encoding='utf-8')
        #f.write(str(reponse.content.decode('utf-8')).replace('\\n',""))
        #f.close()
