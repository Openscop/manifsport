# coding: utf-8
from django.contrib import admin

from core.models.instance import Instance


@admin.register(Instance)
class InstanceAdmin(admin.ModelAdmin):
    """ Administration des instances de configuration """

    # Configuration
    list_display = ['pk', 'name', 'departement', 'workflow_ggd', 'workflow_ddsp', 'workflow_sdis', 'workflow_cg', 'instruction_mode']
    list_filter = ['workflow_ggd', 'workflow_ddsp', 'workflow_sdis', 'workflow_cg', 'instruction_mode']
    search_fields = ['name__unaccent']
    list_per_page = 25
