from django.template.defaulttags import register
...
@register.filter
def get_item_from_dict(dictionary, key):
    return dictionary.get(key)