# coding: utf-8
"""
Gestion des utilisateurs et des "instances" de configuration.
Les "instances de configuration" définissent comment se comporte l'application
et comment doit se dérouler l'instruction des dossiers.
"""
from django.apps import AppConfig
from django.utils.translation import activate


class CoreConfig(AppConfig):
    """ Configuration de l'application """

    name = 'core'
    verbose_name = "Cœur de l'application"

    def ready(self):
        """ Installer les récepteurs de signaux (listeners) """
        from core import listeners
        activate('fr')


default_app_config = 'core.CoreConfig'
