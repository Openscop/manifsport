# coding: utf-8
"""
Remplacer tous les nom prenom vide par : à renseigner _
"""
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction

from core.util.security import protect_code


class Command(BaseCommand):
    """ Nettoyer et charger les données par défaut de la base de données """
    args = ''
    help = 'Verifie les noms prenoms et les remplace par à renseigner _ si vide'



    def handle(self, *args, **options):
        """ Exécuter la commande """



        with transaction.atomic():
            nom = "à renseigner"
            prenom = "à renseigner"
            for user in get_user_model().objects.all():
                if not user.last_name:
                    user.last_name = nom
                if not user.first_name:
                    user.first_name = prenom
                user.save()
            print("Great success !")