# coding: utf-8
"""
Supprimer les manifestations anciennes
Utilisable uniquement en mode DEBUG.
"""
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone
from datetime import timedelta

from core.util.security import protect_code
from events.models import Manifestation


class Command(BaseCommand):
    """ Nettoyer et charger les données par défaut de la base de données """
    args = ''
    help = 'Supprimer les manifestations vieilles de x semaines. x : paramètre facultatif; defaut 104 semaines, 2 ans'

    def add_arguments(self, parser):
        parser.add_argument('weeks_nb', nargs='?', type=int, default=104)

    def handle(self, *args, **options):
        """ Exécuter la commande """
        weeks = options['weeks_nb']
        protect_code()
        date = timezone.now() - timedelta(weeks=weeks)
        print('Vous avez demandez la suppression des manifestations vieilles de plus de ' + str(weeks) + ' semaines.')
        print('Entrez Y pour continuer.')
        if (input() == 'Y'):
            if settings.DEBUG:
                with transaction.atomic():
                    count = Manifestation.objects.filter(end_date__lte=date).count()
                    Manifestation.objects.filter(end_date__lte=date).delete()
                    nb = Manifestation.objects.all().count()
                    print("Les manifestations vieilles de plus de {1} semaines ont été supprimées : {0} manifestations supprimées.".format(count, weeks))
                    print("Il reste {0} manifestations en base de données.".format(nb))
            else:
                print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
        else:
            print('Abandon')