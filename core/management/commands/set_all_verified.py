# coding: utf-8
from allauth.account.models import EmailAddress
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from core.util.security import protect_code


class Command(BaseCommand):
    """ Valide tous les comptes utilisateur, ne demandant ainsi pas de validation par email """
    args = ''
    help = 'Valider tous les comptes utilisateur'

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        if settings.DEBUG:
            count = get_user_model().objects.count()
            # Supprimer les anciens objets EmailAddress
            # Supprime logiquement les objets EmailConfirmation liés
            EmailAddress.objects.all().delete()
            failed = 0
            for user in get_user_model().objects.all().distinct():
                try:
                    EmailAddress.objects.create(user=user, email=user.email, verified=True, primary=False)
                except:
                    failed += 1
            print("{0} emails d'utilisateurs ont été validés, avec {1} erreurs.".format(count, failed))
        else:
            print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
