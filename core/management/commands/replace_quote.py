# coding: utf-8
"""
Remplacer toutes les quotes " par ＂
"""
from django.core.management.base import BaseCommand
from django.db import transaction
from evenements.models import Manif


class Command(BaseCommand):

    def handle(self, *args, **options):
        with transaction.atomic():
            for manif in Manif.objects.all():
                manif.nom=manif.nom.replace('"', '＂')
                manif.cerfa.nom=manif.cerfa.nom.replace('"', '＂')
                manif.cerfa.save()
                manif.save()


            print("Great success !")