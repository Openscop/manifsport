# coding: utf-8
import factory
from django.contrib.auth import get_user_model
from ..models import OptionUser
from django.contrib.auth.models import Group


class OptionUserFactory(factory.django.DjangoModelFactory):
    """ Factory option utilisateur """

    # Champs
    user = None
    conversation_msg = False
    nouveaute_msg = False
    action_msg = False
    notification_msg = False
    tracabilite_msg = False
    # Envoi de mail
    conversation_mail = False
    nouveaute_mail = False
    action_mail = False
    notification_mail = False
    tracabilite_mail = False
    # Envoi de notification push
    conversation_push = False
    nouveaute_push = False
    action_push = False
    notification_push = False
    tracabilite_push = False
    # Meta
    class Meta:
        model = OptionUser


class UserFactory(factory.django.DjangoModelFactory):
    """ Factory utilisateur """

    # Champs
    username = factory.Sequence(lambda n: 'jdoe{0}'.format(n))
    email = factory.Sequence(lambda n: 'ne-pas-repondre-42{0}@manifestationsportive.fr'.format(n))
    last_name = "Robert"
    first_name = "Bob"
    # optionuser = factory.RelatedFactory(OptionUserFactory, 'user')

    # Meta
    class Meta:
        model = get_user_model()


class GroupFactory(factory.django.DjangoModelFactory):
    """ Factory group """

    # Champs
    name = factory.Sequence(lambda n: 'grp{0}'.format(n))

    # Meta
    class Meta:
        model = Group
