# coding: utf-8
from django.apps.config import AppConfig


class EvaluationsConfig(AppConfig):
    """ Configuration de l'application d'évaluations """

    name = 'evaluations'
    verbose_name = 'Évaluations Naturelles'


default_app_config = 'evaluations.EvaluationsConfig'
