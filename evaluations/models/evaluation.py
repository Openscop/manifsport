# coding: utf-8
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models

from events.models import Manifestation
from evenements.models import Manif


class EvaluationManifestation(models.Model):
    """ Évaluation de l'impact environnemental d'une manifestation """

    # Constantes
    COST_CHOICES = (('0', "moins de 5 000 €"), ('1', "entre 5 000 et 20 000 €"), ('2', "entre 20 000 et 100 000 €"), ('3', "plus de 100 000 €"))
    IMPACT_CHOICES = (('0', '0'), ('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5'))

    # Champs
    content_type = models.ForeignKey(ContentType, null=True, blank=True, on_delete=models.SET_NULL)
    object_id = models.PositiveIntegerField(null=True, blank=True)
    content_object = GenericForeignKey()
    manifestation = models.OneToOneField(Manifestation, verbose_name='manifestation', on_delete=models.CASCADE, null=True)
    manif = models.OneToOneField(Manif, verbose_name='manif', on_delete=models.CASCADE, null=True)
    # Description
    place = models.CharField("lieu-dit", max_length=255, blank=True)
    # Fréquence de la manifestation
    every_year = models.BooleanField("la manifestation a lieu chaque année", default=False)
    first_edition = models.BooleanField("première édition de la manifestation", default=False)
    other_frequency = models.CharField("autre fréquence", max_length=255, blank=True)
    # Budget
    cost = models.CharField("coût de la manifestation", max_length=1, choices=COST_CHOICES)
    # Localization
    track_total_length = models.PositiveIntegerField("longueur totale des parcours (km)")
    # Éuipements et installations liés
    night_lighting = models.BooleanField("éclairage nocturne", default=False)
    night_lighting_desc = models.TextField("description", blank=True)
    trails_marks = models.BooleanField(
        "balisage des sentiers", default=False,
        help_text="Seuls les balisages temporaires retirés sous 24h sont autorisés dans le "
                  "cadre de manifestations sportives. Par conséquent ne seront pas autorisés "
                  "l'usage de peinture, confettis ou tout autre type de balisage laissant une "
                  "empreinte sur les milieux",
    )
    trails_marks_desc = models.TextField('description', blank=True)
    other_facilities = models.BooleanField("autres types d'aménagements", default=False)
    other_facilities_desc = models.TextField('description', blank=True)
    # Potential implications
    noise_emissions = models.BooleanField(
        "les émissions sonores sont susceptibles de déranger les espèces présentes "
        "sur le site (puissance des émissions et proximité du site)",
        default=False,
        help_text="Le bruit peut entraîner le dérangement de certaines espèces animales, "
                  "notamment en période de nidification ou d’hivernage pour les oiseaux.",
    )
    waste_management = models.BooleanField(
        "vous avez prévu des dispositifs particuliers pour la gestion des déchets "
        "pendant et après la manifestation [À décrire dans le champ réservé]",
        default=False,
        help_text="Les déchets polluent le milieu naturel et représentent un danger pour les "
                  "espèces animales (risque d’ingestion notamment).",
    )
    waste_management_desc = models.TextField('description', blank=True)
    awareness = models.BooleanField(
        "vous avez prévu la mise en place d'actions de communication ou de "
        "sensibilisation concernant les enjeux environnementaux",
        default=False,
        help_text="La sensibilisation et la communication permettent de réduire les impacts sur "
                  "l’environnement. Contacter l’animateur du site pour la réalisation ou la "
                  "mise à disposition de documents de communication.",
    )
    impact_reduction = models.BooleanField(
        "vous avez pris des mesures pour réduire l'impact de la manifestation sur le "
        "milieu naturel (contacts structures, choix du lieu en fonction des enjeux, "
        "délimitation de zones interdites au public...) [À décrire dans le champ "
        "réservé]",
        default=False,
        help_text="réduction des impacts",
    )
    impact_reduction_desc = models.TextField('description', blank=True)
    # Conclusions
    estimated_impact = models.CharField(
        "impact estimé de la manifestation",
        max_length=1,
        choices=IMPACT_CHOICES,
        help_text="D'après les éléments mis en évidence dans ce formulaire, évaluer l'impact de "
                  "la manifestation en donnant un chiffre de 0 à 5 (0 correspondant à une "
                  "absence d'incidences et 5 à une incidence significative sur le ou les sites "
                  "concernés)",
    )

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de consultation du contenu """
        if self.manifestation:
            return Manifestation.objects.get_subclass(id=self.manifestation.id).get_absolute_url()
        return Manif.objects.get_subclass(id=self.manif.id).get_absolute_url()

    # Méta
    class Meta:
        abstract = True
        verbose_name = "évaluation d'une manifestation"
        verbose_name_plural = "évaluations de manifestations"
        app_label = 'evaluations'
