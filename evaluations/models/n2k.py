# coding: utf-8
from django.db import models
from django.utils.encoding import smart_text

from evaluations.models.evaluation import EvaluationManifestation
from protected_areas.models import SiteN2K


class N2KEvaluation(EvaluationManifestation):
    """ Évaluation Natura 2000 """

    # Description
    diurnal = models.BooleanField("manifestation diurne", default=False)
    nocturnal = models.BooleanField("manifestation nocturne", default=False)

    # Localisation
    sites = models.ManyToManyField(SiteN2K, verbose_name="sur site Natura 2000")
    range_from_site = models.PositiveIntegerField("distance par rapport au site NATURA 2000")

    # Zone d'influence
    rnn = models.BooleanField("Réserve Naturelle Nationale", default=False)
    rnr = models.BooleanField("Réserve Naturelle Régionale", default=False)
    biotope_area = models.BooleanField("Arrêté de Protection de Biotope", default=False)
    classified_site = models.BooleanField("Site Classé", default=False)
    registered_site = models.BooleanField("Site Inscrit", default=False)
    pnr = models.BooleanField("Parc Naturel Régional", default=False)
    znieff = models.BooleanField("ZNIEFF", default=False)

    # Complexes liés
    parking_lot = models.BooleanField("parking", default=False)
    parking_lot_desc = models.TextField('description', blank=True)
    reception_area = models.BooleanField("zone d'accueil du public/des spéctateurs", default=False)
    reception_area_desc = models.TextField('description', blank=True)
    supplying_area = models.BooleanField("zones de ravitaillement", default=False)
    supplying_area_desc = models.TextField('description', blank=True)
    storage_area = models.BooleanField("zone de stockage (matériel, véhicules...)", default=False)
    storage_area_desc = models.TextField('description', blank=True)
    awards_stage = models.BooleanField("scène de remise des prix", default=False)
    awards_stage_desc = models.TextField('description', blank=True)
    bivouac_area = models.BooleanField("zone de bivouac", default=False)
    bivouac_area_desc = models.TextField('description', blank=True)
    noise = models.BooleanField("émissions sonores (sono, concert...)", default=False)
    noise_desc = models.TextField('description', blank=True)

    # Impact sur les habitats naturels
    lawn = models.BooleanField("pelouse", default=False)
    lawn_comments = models.CharField("commentaires", blank=True, max_length=255)
    semi_wooded_lawn = models.BooleanField("pelouse semi-boisée", default=False)
    semi_wooded_lawn_comments = models.CharField("commentaires", blank=True, max_length=255)
    moor = models.BooleanField("lande", default=False)
    moor_comments = models.CharField("commentaires", blank=True, max_length=255)
    scrubland_maquis = models.BooleanField("garrigue/maquis", default=False)
    scrubland_maquis_comments = models.CharField("commentaires", blank=True, max_length=255)

    coniferous_forest = models.BooleanField("forêt de résineux", default=False)
    coniferous_forest_comments = models.CharField("commentaires", blank=True, max_length=255)
    deciduous_forest = models.BooleanField("forêt de feuillus", default=False)
    deciduous_forest_comments = models.CharField("commentaires", blank=True, max_length=255)
    mixed_forest = models.BooleanField("forêt mixte", default=False)
    mixed_forest_comments = models.CharField("commentaires", blank=True, max_length=255)
    plantation = models.BooleanField("plantation", default=False)
    plantation_comments = models.CharField("commentaires", blank=True, max_length=255)

    cliff = models.BooleanField("falaise", default=False)
    cliff_comments = models.CharField("commentaires", blank=True, max_length=255)
    outcrop = models.BooleanField("affleurement rocheux", default=False)
    outcrop_comments = models.CharField("commentaires", blank=True, max_length=255)
    scree = models.BooleanField("éboulis", default=False)
    scree_comments = models.CharField("commentaires", blank=True, max_length=255)
    blocks = models.BooleanField("blocs", default=False)
    blocks_comments = models.CharField("commentaires", blank=True, max_length=255)

    ditch = models.BooleanField("fossé", default=False)
    ditch_comments = models.CharField("commentaires", blank=True, max_length=255)
    watercourse = models.BooleanField("cours d'eau", default=False)
    watercourse_comments = models.CharField("commentaires", blank=True, max_length=255)
    pound = models.BooleanField("étang", default=False)
    pound_comments = models.CharField("commentaires", blank=True, max_length=255)
    bog = models.BooleanField("tourbière", default=False)
    bog_comments = models.CharField("commentaires", blank=True, max_length=255)
    gravel = models.BooleanField("gravière", default=False)
    gravel_comments = models.CharField("commentaires", blank=True, max_length=255)
    wet_meadow = models.BooleanField("prairie humide", default=False)
    wet_meadow_comments = models.CharField("commentaires", blank=True, max_length=255)

    # Impact sur les espèces
    amphibia = models.BooleanField("amphibiens", default=False)
    amphibia_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    reptiles = models.BooleanField("réptiles", default=False)
    reptiles_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    crustaceans = models.BooleanField("crustacés", default=False)
    crustaceans_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    insects = models.BooleanField("insectes", default=False)
    insects_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    terrestrial_mammals = models.BooleanField("mammifères terrestres", default=False)
    terrestrial_mammals_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    birds = models.BooleanField("oiseaux", default=False)
    birds_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    plants = models.BooleanField("plantes", default=False)
    plants_species = models.CharField("nom de l'espèce", blank=True, max_length=255)
    fish = models.BooleanField("poissons", default=False)
    fish_species = models.CharField("nom de l'espèce", blank=True, max_length=255)

    # Implications potentielles
    water_crossing = models.BooleanField(
        "franchissement d'un cours d'eau hors d'un passage aménagé",
        default=False,
        help_text="la traversée des cours d’eau par des engins motorisés, des VTT ou des "
                  "personnes à pied entraîne une dégradation du lit et des berges, la "
                  "destruction des habitats et le dérangement des espèces",
    )
    off_trail = models.BooleanField(
        "passage des participants en dehors des sentiers (à localiser sur la carte)",
        default=False,
        help_text="le passage d’individus en dehors des sentiers entraîne le piétinement et/ou "
                  "la destruction des habitats et des espèces végétales (prairies naturelles, "
                  "zones humides, tourbières, etc.). Le passage entraîne également le "
                  "dérangement des espèces animales telles que les oiseaux en période de "
                  "nidification et d’hivernage.",
    )
    bio_implications = models.BooleanField(
        "la présence du public est susceptible d'avoir une incidence sur les habitats "
        "naturels et/ou les espèces animales et végétales",
        default=False,
        help_text="En fonction de sa localisation et du nombre de personnes attendues, la "
                  "présence du public peut déranger les espèces animales via le bruit ou la "
                  "présence visuelle. Un rassemblement peut également entraîner un piétinement "
                  "important et endommager les espèces végétales.",
    )
    related_facilities = models.BooleanField(
        "présence d'aménagements connexes",
        default=False,
        help_text="Les aménagements (zones de ravitaillement, d’accueil, de restauration, etc.) "
                  "sont à prendre en compte dans l'organisation de la manifestation car ils "
                  "peuvent avoir une incidence à la fois sur les milieux et sur les espèces.",
    )
    light_emissions = models.BooleanField(
        "les émissions lumineuses nocturnes sont-elles susceptibles de déranger les "
        "espèces présentes sur le site",
        default=False,
        help_text="Les sources lumineuses sont susceptibles de perturber les espèces animales "
                  "telles que les chauves-souris. Ces perturbations sont à prendre en compte "
                  "uniquement de nuit.",
    )

    # Conclusion
    natura_2000_conclusion = models.BooleanField(
        "ce formulaire permet de conclure à l'absence d'incidence significative sur "
        "le ou les sites NATURA 2000 concernés par la manifestation",
        default=False,
        help_text="Si laissé décoché, ce formulaire ne permet pas de conclure à l'absence "
                  "d'incidences significatives sur le ou les sites NATURA 2000 concernés par la "
                  "manifestation. L'évaluation d'incidences doit se poursuivre. Contactez la "
                  "DDT au plus vite.",
    )

    # Overrides
    def __str__(self):
        if self.manifestation:
            return smart_text(self.manifestation.__str__())
        return smart_text(self.manif.__str__())

    # Meta
    class Meta(EvaluationManifestation.Meta):
        verbose_name = "évaluation Natura 2000"
        verbose_name_plural = "évaluations Natura 2000"
        default_related_name = "natura2000evaluations"
        app_label = "evaluations"
