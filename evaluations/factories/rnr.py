# coding: utf-8
import datetime

import factory
from factory.fuzzy import FuzzyDate

from evaluations.models import RNREvaluation
from events.factories import DeclarationNMFactory
from evenements.factories import DnmFactory


class RNREvaluationFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation RNR """

    # Champs
    administrator_contact_date = FuzzyDate(datetime.date(2010, 1, 1))
    rnr_entries = 1
    rnr_audience = 1
    track_total_length = 1
    manifestation = factory.SubFactory(DeclarationNMFactory)

    # Meta
    class Meta:
        model = RNREvaluation


class RNREvalManifFactory(factory.django.DjangoModelFactory):
    """ Factory Evaluation RNR """

    # Champs
    administrator_contact_date = FuzzyDate(datetime.date(2010, 1, 1))
    rnr_entries = 1
    rnr_audience = 1
    track_total_length = 1
    manif = factory.SubFactory(DnmFactory)

    # Meta
    class Meta:
        model = RNREvaluation
