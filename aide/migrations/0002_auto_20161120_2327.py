# -*- coding: utf-8 -*-
# Generated by Django 1.9.11 on 2016-11-20 22:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('aide', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='helpnote',
            name='departement',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='administrative_division.Departement', verbose_name='Département'),
        ),
    ]
