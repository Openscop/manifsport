# Generated by Django 2.2.1 on 2019-05-27 12:17

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('aide', '0024_auto_20190322_1328'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='helpaccordionpage',
            options={'verbose_name': "Page d'aide complexe", 'verbose_name_plural': "Pages d'aide complexes"},
        ),
        migrations.AlterModelOptions(
            name='helpaccordionpanel',
            options={'ordering': ['my_order'], 'verbose_name': "Panneau de page d'aide complexe", 'verbose_name_plural': "Panneaux de pages d'aide complexe"},
        ),
        migrations.AlterModelOptions(
            name='helpnote',
            options={'verbose_name': 'Note de bas de page', 'verbose_name_plural': 'Notes de bas de pages'},
        ),
    ]
