# Generated by Django 2.2.1 on 2020-04-27 17:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aide', '0033_auto_20200124_0954'),
    ]

    operations = [
        migrations.AddField(
            model_name='contexthelp',
            name='nbr_vues',
            field=models.IntegerField(default=0, verbose_name='Nombre de fois où cette aide a été vue'),
        ),
    ]
