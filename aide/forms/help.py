# coding: utf-8
from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField
from django.forms.widgets import CheckboxSelectMultiple, SelectMultiple
from django.contrib.auth.models import Group
from aide.models.help import HelpPage, HelpNote
from core.util.user import UserHelper


class HelpPageForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = HelpPage

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôles")
    groupe = TypedMultipleChoiceField(choices=[], required=False, label="Groupes")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'].widget.choices = list(Group.objects.all().values_list('id', 'name'))
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}
        labels = {
            'get_content_html': 'Aperçu',
        }


class HelpNoteForm(models.ModelForm):
    """ Formulaire admin des nouveautés """
    model = HelpNote

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")
    groupe = TypedMultipleChoiceField(choices=[], required=False, label="Groupes")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['groupe'].widget.choices = list(Group.objects.all().values_list('id', 'name'))
        self.fields['departements'].help_text = None
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')
        if 'groupe' in self.initial and self.initial['groupe']:
            self.initial['groupe'] = self.initial['groupe'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        self.instance.groupe = ','.join(sorted(self.cleaned_data['groupe']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}
