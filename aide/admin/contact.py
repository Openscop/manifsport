from django.contrib import admin

from aide.models.contact import Demande
from aide.forms import DemandeAdminForm

@admin.register(Demande)
class DemandeAdmin(admin.ModelAdmin):
    """ Administration des demandes envoyées """

    # Configuration
    list_display = ['pk', 'date', 'user', 'email', 'status']
    list_filter = ['status']
    search_fields = ['email', 'user__username', 'user__last_name']
    actions = []
    exclude = []
    actions_on_top = True
    order_by = ['status']
    list_per_page = 50
    form = DemandeAdminForm
