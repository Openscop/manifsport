# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from import_export.admin import ExportActionModelAdmin

from administrative_division.models.departement import Departement
from aide.forms import HelpPageForm, HelpNoteForm
from aide.models.help import HelpPage, HelpNote
from core.util.admin import RelationOnlyFieldListFilter
from .filter import GroupeFilter


class HelpNoteInline(admin.StackedInline):
    """ Inline pour ajouter des notes aux pages d'aide """

    # Configuration
    model = HelpNote
    fields = (('title', 'active'), 'role', 'groupe', 'departements', 'content')
    extra = 1
    form = HelpNoteForm

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field


@admin.register(HelpPage)
class HelpPageAdmin(ExportActionModelAdmin):
    """ Administration des pages d'aide """

    # Configuration
    list_select_related = True
    list_display = ['pk', 'title', 'path', 'active', 'get_note_count', 'updated']
    list_display_links = []
    list_filter = ['active', 'role', GroupeFilter, ('departements', RelationOnlyFieldListFilter)]
    list_editable = ['active']
    readonly_fields = ['get_content_html']
    search_fields = ['title__unaccent', 'content__unaccent', 'path', 'slug']
    actions = []
    exclude = []
    form = HelpPageForm
    inlines = [HelpNoteInline]
    actions_on_top = True
    order_by = ['pk']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            if departement:
                field.queryset = Departement.objects.filter(id=departement.pk)
            return field

    def get_fieldsets(self, request, obj=None):
        if [group in ['Page d\'aide', 'Administrateurs techniques'] for group in request.user.groups.values_list('name', flat=True)].count(True) > 0:
            return [(None, {'fields': ['title', 'path', 'active', 'content', 'role', 'groupe', 'departements',
                                       'authenticated_only']}), ]
        return [(None, {'fields': ['title', 'path', 'active', 'get_content_html', 'role', 'groupe', 'departements',
                                   'authenticated_only']}), ]
