# coding: utf-8
from django.urls import reverse
from django.db import models
from django.http.response import Http404
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django_extensions.db.fields import AutoSlugField

from core.util.admin import set_admin_info
from core.util.user import UserHelper


class HelpPage(models.Model):
    """ Page d'aide """

    # Champs
    title = models.CharField(max_length=192, blank=False, verbose_name="Titre")
    slug = AutoSlugField(max_length=192, populate_from='title', unique_for_date=True, verbose_name="Slug")
    path = models.CharField(max_length=80, blank=True, verbose_name="Chemin URL")
    active = models.BooleanField(default=True, verbose_name="Active")
    content = models.TextField(blank=False, verbose_name="Contenu")
    role = models.TextField(blank=True, help_text="Rôles d'utilisateurs autorisés à voir la page", verbose_name="Rôles")
    groupe = models.TextField(blank=True, help_text="Groupes d'utilisateurs autorisés à voir la page", verbose_name="Groupes")
    departements = models.ManyToManyField('administrative_division.departement', blank=True, verbose_name="Départements")
    authenticated_only = models.BooleanField(default=False, verbose_name="Accès authentifié uniquement")
    updated = models.DateTimeField(auto_now=True, verbose_name="Mise à jour")

    def __str__(self):
        return self.title

    # Getter
    def get_content_html(self):
        """ Effectuer le rendu HTML de la réponse """
        return mark_safe(self.content)

    # Getter
    @set_admin_info(short_description="Notes")
    def get_note_count(self):
        """ Renvoyer le nombre total de notes pour la page """
        return self.notes.count()

    def get_notes(self):
        """ Renvoyer les notes pour la page d'aide """
        return self.notes.filter(active=True)

    def render(self, request):
        """ Rendre la page """
        template_path = 'aide/help-page.html'
        response = render(request, template_path, {'page': self})
        return response

    # Overrides
    def get_absolute_url(self):
        """ Renvoyer l'URL de la page d'aide """
        # Corriger l'URL si démarre par un '/'
        if str(self.path).startswith('/'):
            self.path = str(self.path).replace('/', '', 1)  # Remplacer le premier slash
            self.save()
        return reverse('aide:help-page', kwargs={'path': '{0}'.format(self.path)})

    # Meta
    class Meta:
        verbose_name = "Page d'aide"
        verbose_name_plural = "Pages d'aide"
        app_label = 'aide'


class HelpNote(models.Model):
    """ Note jointe à une page d'aide """

    # Champs
    page = models.ForeignKey('HelpPage', related_name='notes', verbose_name="Page", on_delete=models.CASCADE)
    title = models.CharField(max_length=192, blank=True, verbose_name="Titre")
    active = models.BooleanField(default=True, verbose_name="Active")
    content = models.TextField(blank=False, verbose_name="Contenu")
    role = models.TextField(blank=True, help_text="Rôles autorisés à voir la note", verbose_name="Rôles")
    groupe = models.TextField(blank=True, help_text="Groupes d'utilisateurs autorisés à voir la page", verbose_name="Groupes")
    departements = models.ManyToManyField('administrative_division.departement', blank=True, help_text="Départements autorisés à voir la note", verbose_name="Départements")
    updated = models.DateTimeField(auto_now=True, verbose_name="Mise à jour")

    # Getter
    def get_content_html(self):
        """ Effectuer le rendu HTML de la réponse """
        return mark_safe(self.content)

    # Meta
    class Meta:
        verbose_name = "Note de bas de page"
        verbose_name_plural = "Notes de bas de pages"
        app_label = 'aide'
