# coding: utf-8
"""
Préprocesseurs de requête
"""

from aide.models.context import ContextHelp

def contexthelps(request):
    '''
    Charge les aides contextuelles dynamiques.
    Elles sont ensuite ajoutées à la page HTML (dans un script JS) lors du traitement du template.
    :param request:
    :return: liste d'aides contextuelles
    '''

    contexthelps = []

    # récupère les aides contextuelles définies pour la page demandée
    if hasattr(request.resolver_match, 'url_name'):
        contexthelps = [ contexthelp for contexthelp in ContextHelp.objects.filter(page_names__contains=request.resolver_match.url_name) if contexthelp.is_visible(request)  ]
    # récupère et ajoute les aides contextuelles définies sans page spécifique
    # contexthelps += [contexthelp for contexthelp in
    #                 ContextHelp.objects.filter(page_names__exact='').exclude(positions__exact='') if
    #                 contexthelp.is_visible(request)]

    for contexthelp in ContextHelp.objects.filter(page_names__exact=''):
        contexthelps.append(contexthelp) if contexthelp.is_visible(request) else ""


    return {'contexthelps': contexthelps}