from django.forms import Form, DateField
from bootstrap_datepicker_plus import DatePickerInput


class CalendrierForm(Form):
    # Formulaire qui permet de générer un input date avec bootstrap_datepicker_plus
    date = DateField(widget=DatePickerInput(format='%d/%m/%Y', options={'locale': 'fr'}))
