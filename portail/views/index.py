# coding: utf-8
from django.views.generic.base import TemplateView
from django.shortcuts import render

from core.models.instance import Instance


class HomePage(TemplateView):
    """ Page d'accueil """

    # Configuration
    template_name = 'portail/index.html'

    # Méthodes
    def get_context_data(self, **kwargs):
        """ Définir le contexte d'affichage de la page """
        context = super(HomePage, self).get_context_data(**kwargs)
        instance = Instance.objects.get_for_request(self.request)
        context['INSTANCES'] = Instance.objects.configured()
        context['IS_MASTER'] = instance.is_master()
        # Domaine sans instance : ne pas afficher le calendrier
        context['HIDE_CALENDAR'] = instance.is_master()
        return context


def erreur400view(request, exception):
    return render(request,'400.html', {'message': exception}, status=400)


def erreur403view(request, exception):
    return render(request,'403.html', {'message': exception}, status=403)


def erreur404view(request, exception):
    return render(request,'404.html', {'message': exception}, status=404)


def erreur500view(request):
    return render(request,'500.html', status=500)
