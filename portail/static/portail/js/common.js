/**
 * Created by david on 11/05/16.
 */


function dateSorter(date1, date2) {
    var date12 = date1.split('/').reverse().join('');
    var date22 = date2.split('/').reverse().join('');
    if (date12 > date22) return 1;
    if (date12 < date22) return -1;
    return 0;
};

function scrollTo(selector) {
    var target_top = $("#searchform").offset().top - 50;
    $('html, body').animate({scrollTop: target_top}, 1500);
}