/**
 * Created by knt on 17/03/17.
 */

// jQuery plugin to prevent double submission of forms
jQuery.fn.preventDoubleSubmission = function() {
  $(this).on('submit',function(e){
    if ($(this).find('input[type="submit"]').attr("value")!=="Action en cours. Veuillez patienter..."){
      $(this).find('input[type="submit"]').attr('disabled', 'true')
      var text= $(this).find('input[type="submit"]').attr("value")
      $(this).find('input[type="submit"]').attr("value","Action en cours. Veuillez patienter...")
      $(this).find('input[type="submit"]').after("<i id='prevent_sub_spinner' class=\"prevent_sub_spinner far fa-spinner fa-2x fa-spin\" style=\"position: relative; top: 6px; margin-left: 9px; color: #015A70; \"></i>");


      setTimeout(function () {
        console.log('timeout')
        $(this).find('input[type="submit"]').attr('disabled', 'false')
        $(this).find('input[type="submit"]').attr("value", text)
        $(this).find(".prevent_sub_spinner").remove()
      }, 10000)
    }
  });

  // Keep chainability
  return this;
};
