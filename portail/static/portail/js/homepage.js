/**
 * Created by david on 19/10/2017.
 */


    // bof Gestion de la carte de France
    try {
        map = new jvm.Map({
            container: $('#carte-fr'),
            map: 'fr_merc',
            backgroundColor: '#015a70',
            regionsSelectable: true,
            regionStyle: {

                hover: {
                    fill: '#9aa400'
                },
                selected: {
                    fill: '#9aa400'
                },
            },
            onRegionClick: function (e, code) {
                if (typeof instances[code] !== 'undefined') {
                    window.location = instances[code];
                }
                else {
                    var departement = map.mapData.paths[code].name;
                    $('#myModal .modal-title').text(departement);
                    $('#myModal .modal-body').html("Ce département ne propose pas la dématérialisation des demandes sur cette plateforme.<br>" +
                        "Nous vous invitons à vous inscrire pour nous permettre de vous tenir informé dès que la situation évolue...");
                    $('#myModal').modal();
                }
            }
        });
        if (dep_number != "NATIONALE") {
            map.setSelectedRegions("FR-" + dep_number);
        }

    } catch(e) {

    }
    // eof Gestion de la carte de France