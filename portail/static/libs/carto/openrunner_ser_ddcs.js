var POI_LAYER_NAME = "POI";
var ORPOI_ACTIVATION = 0;
var ORPOI_PARTNER_ACTIVATION = 1;
var POI_EXPORTABLE = "POI Exportable";
var POI_LOCATION = "Position";
var POI_MODIFY = "Modifier la description";
var POI_REMOVE = "Supprimer ce poi";
var LATITUDE = "Latitude";
var LONGITUDE = "Longitude";
var ORConstants = {
    POI_COUNT: 1,
    POI_PREFIX_ID: "poi",
    POI_MODE_POPUP: 0,
    POI_MODE_LABEL_RIGHT: 1,
    POI_MODE_LABEL_LEFT: 2,
    POI_MODE_IMG: 3,
    DEG2RAD: 0.01745329252,
    EARTH_RAD_KM: 6371.598,
    EARTH_RAD_MILES: 3959.127,
    OFFSET_GMAPS: 268435456,
    RADIUS_GMAPS: 85445659.4471,
    POI_OFFSET_VERT: 34,
    POI_OFFSET_HORI: 28
};

function getParamString (obj, existingUrl, uppercase) {
    var params = [];
    for (var i in obj) {
        params.push(encodeURIComponent(uppercase ? i.toUpperCase() : i) + '=' + encodeURIComponent(obj[i]));
    }
    return ((!existingUrl || existingUrl.indexOf('?') === -1) ? '?' : '&') + params.join('&');
}

// le but ici est de remplir un tableau de layer
var layer = []

function Chargeleslayer(map){
    map.data.forEach(function(feature) {
        // If you want, check here for some constraints.
        map.data.remove(feature);
    });
    layer.forEach(function (layers) {
        getGeoJsonLayer(ORMainSer.map, {}, '',
            {name : 'OSM',minZoom:12,alt: 'OSM'},
            [
                {
                    pictureUrl:'',
                    url:'https://www.openstreetmap.org/',
                    textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                }
            ],
            layers,
            'localhost',
        );
    })



}

function getGeoJsonLayer(map, layer, options, originators, wmshost, type) {



    // Calcul BBOX
    var gmapbbox = map.getBounds();
    var sw = gmapbbox.getSouthWest();
    var ne = gmapbbox.getNorthEast();
    var bbox = [sw.lng(), sw.lat(), ne.lng(), ne.lat()].join()+",EPSG:4326";
    var url = ("https://carto2.manifestationsportive.fr/geoserver/cite/ows")

    var parameters = {
        service: 'WFS',
        version: '2.0.0',
        request: 'GetFeature',
        typeName: 'cite:'+type,
        maxFeatures: 30,
        outputFormat: 'application/json',
        bbox: bbox,
        srsName:'EPSG:4326'

    };
    // Récupération des data
    var URL = url + getParamString(parameters);

    let couleur = ""




    map.data.loadGeoJson(URL,null, function(features) {
        map.data.setStyle(function(feature) {
            if (feature.o.split('.')[0]==="natura_protection_speciale"){
                couleur = "#0ab944"
            }
            else if (feature.o.split('.')[0]==="natura_directive_habitat"){
                couleur = "#b947aa"
            }
            else if (feature.o.split('.')[0]==="parc_naturel_regional"){
                couleur = "#b95757"
            }
            else if (feature.o.split('.')[0]==="reserve_naturelle_regionale"){
                couleur = "#5946b9"
            }
            else if (feature.o.split('.')[0]==="communespsql"){
                couleur = "#b9b41e"
            }
            else if (feature.o.split('.')[0]==="parc_national"){
                couleur = "#b9282b"
            }
            else if (feature.o.split('.')[0]==="reserve_integrale"){
                couleur = "#2b6bb9"
            }
            else if (feature.o.split('.')[0]==="reserve_nationale"){
                couleur = "#2bb99d"
            }
            else if (feature.o.split('.')[0]==="protection_biotope0"){
                couleur = "#9e004f"
            }
            else if (feature.o.split('.')[0]==="parcelles_forestieres_publique0"){
                couleur = "#b07500"
            }
            else{
                couleur = "black"
            }



            return /** @type {google.maps.Data.StyleOptions} */({
                fillColor: couleur,
                strokeWeight: 1
            });
        });
        map.data.addListener('mouseup', function(event) {
            var feature = event.feature;
            // contruction du tooltip,
            let fprop = "<div style='width:300px;'><ul>";

            if (type==="communespsql"){
                fprop+= "<li>Nom : "+feature.j.nom+"</li>"
                fprop+= "<li>Code Insee : "+feature.j.insee+"</li>"
            }
            else if (type==="natura_directive_habitat" || type==="natura_protection_speciale"){
                fprop+="<li>Nom : "+feature.j.nom_site+"</li>"
                fprop+="<li>Code : "+feature.j.sitecode+"</li>"
                fprop+="<li>Fiche : <a href='https://inpn.mnhn.fr/site/natura2000/"+feature.j.sitecode+"' target='_blank'>https://inpn.mnhn.fr/site/natura2000/"+feature.j.sitecode+"</a></li>"
            }
            else if (type==="parc_national"){
                fprop+="<li>Nom : "+feature.j.gest_site+"</li>"
                fprop+="<li>Gestionnaire : "+feature.j.operateur+"</li>"
                fprop+="<li>Fiche : <a href='"+feature.j.url_fiche+"' target='_blank'>"+feature.j.url_fiche+"</a></li>"
            }
            else if (type==="reserve_nationale"){
                fprop+="<li>Nom : "+feature.j.nom_site+"</li>"
                fprop+="<li>Gestionnaire : "+feature.j.operateur+"</li>"
                fprop+="<li>Fiche : <a href='"+feature.j.url_fiche+"' target='_blank'>"+feature.j.url_fiche+"</a></li>"
            }
            else if (type==="parcelles_forestieres_publique0"){
                fprop+="<li>Nom : "+feature.j.llib_frt+"</li>"
                fprop+="<li>Code : "+feature.j.iidtn_frt+"</li>"
            }
            else if (type==="protection_biotope0"){
                fprop+="<li>Nom : "+feature.j.nom_site+"</li>"
                fprop+="<li>Gestionnaire : "+feature.j.operateur+"</li>"
                fprop+="<li>Fiche : <a href='"+feature.j.url_fiche+"' target='_blank'>"+feature.j.url_fiche+"</a></li>"
            }
            else if (type==="gend_police0") {
                fprop += "<li>Nom : " + feature.j.service + "</li>"
                fprop += "<li>Téléphone : " + feature.j.telephone + "</li>"
            }
            else if (type==="liste_passage_niveau"){
                fprop+="<li>Nom : "+feature.j.libelle_if+"</li>"
                fprop+="<li>Code_ligne : "+feature.j.code_ligne+"</li>"
            }
            else if (type==="bornage_routier0"){
                fprop+="<li>Route : "+feature.j.route+"</li>"
                fprop+="<li>Gestionnaire : "+feature.j.gestionnai+"</li>"
            }
            else{
                fprop+="<li>Nom : "+feature.j.nom_site+"</li>"
                fprop+="<li>Gestionnaire : "+feature.j.gest_site+"</li>"
                fprop+="<li>Fiche : <a href='"+feature.j.url_fiche+"' target='_blank'>"+feature.j.url_fiche+"</a></li>"
            }


            fprop+="</ul></div>"

            if (infowindow != null){
                infowindow.close()
                infowindow = null
            }
            infowindow = new google.maps.InfoWindow()
            infowindow.setContent(fprop);
            // position the infowindow on the marker
            infowindow.setPosition(event.latLng);
            // anchor the infowindow on the marker
            infowindow.setOptions({pixelOffset: new google.maps.Size(0,0)});
            infowindow.open(map);
        });
    });

}







var ORLatLng = Class.create({
    initialize: function (lat, lng, alt, rot, ortype) {
        this.latitude = lat || 0;
        this.longitude = lng || 0;
        this.alt = alt || 0;
        this.rot = rot || 0;
        this.ortype = ortype || "A"
    }, getLat: function () {
        return this.latitude
    }, getLng: function () {
        return this.longitude
    }, lat: function () {
        return this.latitude
    }, lng: function () {
        return this.longitude
    }, getAlt: function () {
        return this.alt
    }, getRot: function () {
        return this.rot
    }, latRadians: function () {
        return this.latitude * 0.01745329252
    }, lngRadians: function () {
        return this.longitude * 0.01745329252
    }, getORType: function () {
        return this.ortype
    }, setORType: function (ortype) {
        this.ortype = ortype
    }, setLat: function (lat) {
        this.latitude = lat
    }, setLng: function (lng) {
        this.longitude = lng
    }, equals: function (pt) {
        return (pt.getLat() == this.latitude && pt.getLng() == this.longitude && pt.getORType() == this.ortype)
    }, toString: function () {
        return "(" + this.latitude + ", " + this.longitude + ")"
    }
});
var ORRoute = Class.create({
    initialize: function (id, data) {
        this.id = id || -1;
        this.poiUrl = null;
        this.poiExtraUrl = null;
        this.data = data || null
    }, getId: function () {
        return this.id
    }, setId: function (id) {
        this.id = id
    }, getPoiUrl: function () {
        return this.poiUrl
    }, setPoiUrl: function (poiUrl) {
        this.poiUrl = poiUrl
    }, getPoiExtraUrl: function () {
        return this.poiExtraUrl
    }, setPoiExtraUrl: function (poiExtraUrl) {
        this.poiExtraUrl = poiExtraUrl
    }, getData: function () {
        return this.data
    }
});
var ORGMapRouteData = Class.create({
    initialize: function () {
        this.hPoi = new Hash();
        this.poiSave = false
    }, addPoi: function (poi) {
        this.hPoi.set(poi.getPoiId(), poi)
    }, getHPoi: function () {
        return this.hPoi
    }, getPoiById: function (poiid) {
        return this.hPoi.get(poiid)
    }, setHPoi: function (hPoi) {
        this.hPoi = hPoi
    }, hasPoi2Save: function () {
        return this.poiSave
    }, setPoi2Save: function (a) {
        this.poiSave = a
    }
});
var ORIconCategory = Class.create({
    initialize: function (id, title, iconList) {
        this.id = id;
        this.title = title;
        this.iconList = []
    }, getId: function () {
        return this.id
    }, getTitle: function () {
        return this.title
    }, getIconList: function () {
        return this.iconList
    }, addIcon: function (a) {
        this.iconList.push(a)
    }
});
var ORIcon = Class.create({
    initialize: function (id, sizeW, sizeH, anchorX, anchorY, iconUrlA, iconUrlI, desc, descriptable) {
        this.id = id || -1;
        this.sizeW = sizeW || 0;
        this.sizeH = sizeH || 0;
        this.anchorX = anchorX || 0;
        this.anchorY = anchorY || 0;
        this.iconUrlA = iconUrlA || "";
        this.iconUrlI = iconUrlI || "";
        this.desc = desc || "";
        this.descriptable = descriptable || 1
    }, getId: function () {
        return this.id
    }, getDomId: function () {
        return "ico" + this.id
    }, getSizeW: function () {
        return this.sizeW
    }, getSizeH: function () {
        return this.sizeH
    }, getAnchorX: function () {
        return this.anchorX
    }, getAnchorY: function () {
        return this.anchorY
    }, getIconUrlA: function () {
        return this.iconUrlA
    }, getIconUrlI: function () {
        return this.iconUrlI
    }, getDesc: function () {
        return this.desc
    }, setDescriptable: function (descriptable) {
        this.descriptable = descriptable
    }, getDescriptable: function () {
        return this.descriptable
    }
});
var ORMarker = Class.create({
    initialize: function (oricon, orlatlng) {
        this.oricon = oricon || new ORIcon();
        this.orlatlng = orlatlng || new ORLatLng();
        this.apimarker
    }, getApiMarker: function () {
        return this.apimarker
    }, setApiMarker: function (apimarker) {
        this.apimarker = apimarker
    }, setORIcon: function (oricon) {
        this.oricon = oricon
    }, getORIcon: function () {
        return this.oricon
    }, setORLatLng: function (orlatlng) {
        this.orlatlng = orlatlng
    }, getORLatLng: function () {
        return this.orlatlng
    }
});
var ORPoi = Class.create({
    initialize: function (poiid, posGg, marker, userShortDescription, descriptable) {
        this.poiid = ORConstants.POI_COUNT++;
        this.ormarker = marker;
        this.posGg = posGg;
        this.posKm = 0;
        this.userDescription = "";
        this.userShortDescription = userShortDescription;
        this.exportable_ser = "1";
        this.exportable_files = "1";
        this.elevation_display_mode = "-1";
        this.elevation_display_hori = "0";
        this.display_mode = "0";
        this.poi_global = "0";
        this.category = "";
        this.ormarker.setORLatLng(this.posGg)
    }, getPosKm: function () {
        return this.posKm
    }, setPosKm: function (posKm) {
        this.posKm = posKm
    }, setORMarker: function (ormarker) {
        this.ormarker = ormarker
    }, getORMarker: function () {
        return this.ormarker
    }, getPoiId: function () {
        return this.poiid
    }, setGlobal: function (poi_global) {
        this.poi_global = poi_global
    }, isGlobal: function () {
        return (this.poi_global == "1")
    }, setExportableSer: function (exportable_ser) {
        this.exportable_ser = exportable_ser
    }, isExportableSer: function () {
        return (this.exportable_ser == "1")
    }, setExportableFiles: function (exportable_files) {
        this.exportable_files = exportable_files
    }, isExportableFiles: function () {
        return (this.exportable_files == "1")
    }, getElevationDisplayMode: function () {
        return this.elevation_display_mode
    }, setElevationDisplayMode: function (elevation_display_mode) {
        this.elevation_display_mode = elevation_display_mode
    }, isElevationDisplayHori: function () {
        return (this.elevation_display_hori == "1")
    }, setElevationDisplayHori: function (elevation_display_hori) {
        this.elevation_display_hori = elevation_display_hori
    }, getDisplayMode: function () {
        return this.display_mode
    }, setDisplayMode: function (display_mode) {
        this.display_mode = display_mode
    }, getUserDescription: function () {
        return this.userDescription
    }, setUserDescription: function (userDescription) {
        this.userDescription = userDescription
    }, getUserShortDescription: function () {
        return this.userShortDescription
    }, setUserShortDescription: function (userShortDescription) {
        this.userShortDescription = userShortDescription
    }, getPoiAsUrl: function () {
        return {
            poi_id: this.ormarker.getORIcon().getId(),
            poi_lat: this.ormarker.getORLatLng().lat(),
            poi_lng: this.ormarker.getORLatLng().lng(),
            poi_pos: this.posKm,
            poi_img_i: this.ormarker.getORIcon().getIconUrlI(),
            poi_img_a: this.ormarker.getORIcon().getIconUrlA(),
            poi_exp_ser: this.exportable_ser,
            poi_exp_files: this.exportable_files,
            poi_title: this.userShortDescription,
            poi_desc: this.userDescription,
            poi_global: this.poi_global,
            display_mode: this.display_mode,
            elevation_display_mode: this.elevation_display_mode,
            elevation_display_hori: this.elevation_display_hori
        }
    }
});
var ORGMapDecoratorManager = (function () {
    function _createPOIUserMarker(poi) {
        if (poi != null) {
            var icon = new google.maps.MarkerImage();
            var orm = poi.getORMarker();
            icon.url = orm.getORIcon().getIconUrlI();
            icon.size = new google.maps.Size(orm.getORIcon().getSizeW(), orm.getORIcon().getSizeH());
            icon.anchor = new google.maps.Point(orm.getORIcon().getAnchorX(), orm.getORIcon().getAnchorY());
            icon.infoWindowAnchor = new google.maps.Point(orm.getORIcon().getAnchorX(), orm.getORIcon().getAnchorY());
            var marker = new google.maps.Marker({
                raiseOnDrag: false,
                draggable: false,
                optimized: false,
                position: orm.getORLatLng(),
                icon: icon
            });
            google.maps.event.addListener(marker, "mouseover", function () {
                ORMainSer.iwinpoiedddcs.close();
                ORMainSer.iwinpoiroddcs.close();
                if (ORPOIUserManager.isEditorOpen()) {
                    if (ORPOIUserManager.getActivePOIID() == poi.getPoiId()) {
                    } else {
                        _changePoiUserActive(poi.getPoiId())
                    }
                } else {
                    if (poi.getDisplayMode() == ORConstants.POI_MODE_POPUP) {
                        orm.getApiMarker().getIcon().url = orm.getORIcon().getIconUrlA();
                        orm.getApiMarker().setIcon(orm.getApiMarker().getIcon());
                        var c = "poi-r-popup-desc";
                        if (poi.getUserDescription() == "") {
                            c = "poi-r-popup-nodesc"
                        }
                        ORMainSer.iwinpoiroddcs.setContent("<div id='poipopup_roddcs' class='" + c + "'>" + poi.getPoiId() + "</div>");
                        ORMainSer.iwinpoiroddcs.open(ORMainSer.map, marker)
                    } else {
                        orm.getApiMarker().getIcon().url = orm.getORIcon().getIconUrlA();
                        orm.getApiMarker().setIcon(orm.getApiMarker().getIcon())
                    }
                }
            });
            google.maps.event.addListener(marker, "click", function () {
                ORMainSer.iwinpoiedddcs.close();
                ORMainSer.iwinpoiroddcs.close();
                if (ORPOIUserManager.isEditorOpen()) {
                    if (ORPOIUserManager.getActivePOIID() == poi.getPoiId()) {
                        ORGMapDecoratorManager.displayPoiUserEdPopUp(ORPOIUserManager.getActivePOIID())
                    }
                }
            });
            google.maps.event.addListener(marker, "mouseout", function () {
                if (!ORPOIUserManager.isEditorOpen()) {
                    orm.getApiMarker().getIcon().url = orm.getORIcon().getIconUrlI();
                    orm.getApiMarker().setIcon(orm.getApiMarker().getIcon());
                    ORMainSer.iwinpoiroddcs.close()
                } else {
                    ORMainSer.iwinpoiroddcs.close()
                }
            });
            google.maps.event.addListener(marker, "drag", function (event) {
                var poicoord = event.latLng;
                $("poi_udet_lng").innerHTML = poicoord.lng().toFixed(5);
                $("poi_udet_lat").innerHTML = poicoord.lat().toFixed(5)
            });
            google.maps.event.addListener(marker, "dragend", function (event) {
                var poicoord = event.latLng;
                poi.getORMarker().setORLatLng(new google.maps.LatLng(poicoord.lat(), poicoord.lng()));
                document.fire("or:poiuser-modif")
            });
            return marker
        } else {
            return null
        }
    }

    function _changePoiUserActive(poiid) {
        _freezePoiUser();
        var poi = ORMainSer.sdis_poi[poiid];
        var marker = poi.getORMarker();
        marker.getApiMarker().getIcon().url = marker.getORIcon().getIconUrlA();
        marker.getApiMarker().setDraggable(true);
        ORPOIUserManager.setActivePOIID(poi.getPoiId(), marker.getApiMarker().getPosition(), marker.getORIcon().getIconUrlI(), null)
    }

    function _freezePoiUser() {
        var lid = ORPOIUserManager.getActivePOIID();
        var poilist = ORMainSer.sdis_poi;
        poilist.forEach(function (lpoi) {
            var lmarker = lpoi.getORMarker();
            lmarker.getApiMarker().setMap(null);
            lmarker.getApiMarker().getIcon().url = lmarker.getORIcon().getIconUrlI();
            lmarker.getApiMarker().setDraggable(false);
            lmarker.getApiMarker().setMap(ORMainSer.map)
        });
        ORPOIUserManager.setActivePOIID(null, null, null, null)
    }

    function _removePoiUser(poiid) {
        if (window.confirm("Supprimer ce POI?")) {
            console.log(ORMainSer.sdis_poi);
            var rem = ORMainSer.sdis_poi.splice(poiid, 1);
            console.log(ORMainSer.sdis_poi);
            if (poiid != null && rem.length > 0) {
                rem[0].getORMarker().getApiMarker().setMap(null)
            }
            ORPOIUserManager.setActivePOIID(null, null, null, null);
            document.fire("or:poiuser-modif")
        }
    }

    function _removeAllPoiUser() {
        var a = ORMainSer.activeRoute.getData();
        var b = a.getHPoi().values();
        var lb = b.length;
        if (lb > 0) {
            for (var i = 0; i < lb; i++) {
                b[i].getORMarker().getApiMarker().setMap(null)
            }
        }
        var c = a.getTLabel();
        while (c.length > 0) {
            var e = c.pop();
            e.setVisible(false);
            e.setContent("")
        }
    }

    function _showPoiUser() {
        var poilist = ORMainSer.sdis_poi;
        poilist.forEach(function (poi) {
            var marker = _createPOIUserMarker(poi);
            poi.getORMarker().setApiMarker(marker);
            marker.setMap(ORMainSer.map)
        })
    }

    function _displayPoiUserEdPopUp(poiid) {
        ORMainSer.iwinpoiedddcs.close();
        ORMainSer.iwinpoiroddcs.close();
        var poi = ORRouteManager.findPOIUserInRoutes(poiid);
        if (ORPOIUserManager.isEditorOpen()) {
            if (ORPOIUserManager.getActivePOIID() == poi.getPoiId()) {
                var cpoi = "<div class='poi-w-popup or-form poi-container'>";
                cpoi += "	<div id='poipopup_edddcs'>" + poi.getPoiId() + "</div>";
                cpoi += "	</div>";
                ORMainSer.iwinpoiedddcs.setContent(cpoi);
                ORMainSer.iwinpoiedddcs.open(ORMainSer.map, poi.getORMarker().getApiMarker())
            }
        }
    }

    return {
        getPoiFromId: function (poiid) {
            return _getPoiFromId(poiid)
        }, addPoiUserMarker: function (point) {
            var ico = ORPOIUserManager.getSelectedIcon().ico;
            if (ico != null) {
                var orm = new ORMarker(ico);
                var poi = new ORPoi(ico.getId(), point, orm, ico.getDesc(), ico.getDescriptable());
                var marker = _createPOIUserMarker(poi);
                orm.setApiMarker(marker);
                ORMainSer.sdis_poi[poi.getPoiId()] = poi;
                _changePoiUserActive(poi.getPoiId());
                marker.setMap(ORMainSer.map);
                document.fire("or:poiuser-modif")
            }
        }, showPoiUser: function () {
            _showPoiUser()
        }, removeAllPoiUser: function () {
            _removeAllPoiUser()
        }, removePoiUser: function (poiid) {
            _removePoiUser(poiid)
        }, changePoiUserActive: function (poiid) {
            _changePoiUserActive(poiid)
        }, freezePoiUser: function () {
            _freezePoiUser()
        }, displayPoiUserEdPopUp: function (poiid) {
            _displayPoiUserEdPopUp(poiid)
        }, activatePoiUserSaveImg: function () {
            $("savepoiuserdata_img").src = "../img/v3/lt/bt_OR_updatedata.png";
            ORRouteManager.activatePoiUserChange(true)
        }, deactivatePoiUserSaveImg: function () {
            $("savepoiuserdata_img").src = "../img/v3/lt/bt_OR_updatedata-i.png";
            ORRouteManager.activatePoiUserChange(false)
        }, setPoiUserSaveImg: function () {
            var val = ORMainSer.activeRoute.getData().hasPoi2Save();
            if (val) {
                $("savepoiuserdata_img").src = "../img/v3/lt/bt_OR_updatedata.png"
            } else {
                $("savepoiuserdata_img").src = "../img/v3/lt/bt_OR_updatedata-i.png"
            }
        }
    }
})();
var ORIconCatalog = {
    iconCatalog: new Hash(), loaded: false, load: function () {
        if (!ORIconCatalog.loaded) {
            new Ajax.Request("../orpoi-user/loadIcon.php", {
                method: "post",
                asynchronous: false,
                parameters: {typ: "DDCS_SEC"},
                onSuccess: function (transport) {
                    if (transport.responseText == "ko") {
                    } else {
                        var a = transport.responseText.evalJSON(true);
                        for (var tic = 0; tic < a.length; tic++) {
                            var lc = new Hash();
                            var cat = a[tic];
                            var ica = new ORIconCategory(cat.category_cod, cat.category_title);
                            var icl = cat.iconlist;
                            for (var i = 0, j = icl.length; i < j; i++) {
                                var cati = icl[i];
                                var ic = new ORIcon(cati.idicon, cati.sizeW, cati.sizeH, cati.anchorX, cati.anchorY, "../" + cati.iconUrlA, "../" + cati.iconUrlI, cati.desc, cati.descriptable);
                                ica.addIcon(ic)
                            }
                            ORIconCatalog.iconCatalog.set(cat.category_cod, ica)
                        }
                        ORIconCatalog.loaded = true
                    }
                }
            })
        }
    }, getIconById: function (id) {
        var catlist = ORIconCatalog.iconCatalog.values();
        for (var i = 0; i < catlist.length; i++) {
            var iconlist = catlist[i].getIconList();
            for (var j = 0; j < iconlist.length; j++) {
                var ico = iconlist[j];
                if (ico.getId() == id) {
                    return ico
                }
            }
        }
    }, getIconCatalogAsArray: function () {
        return ORIconCatalog.iconCatalog.keys()
    }, getIconCatalog: function () {
        return ORIconCatalog.iconCatalog
    }
};
var ORPOIUserManager = {
    htmlDivManager: "layer_poiuser",
    htmlDivPOICat: "layer_poiuser_category",
    htmlDivPOIList: "layer_poiuser_list",
    htmlDivPOIDet: "layer_poiuser_details",
    suffixeIMG: "_img",
    bindFreezePOI: null,
    bindNameRemovePOI: null,
    bindNameDisPopUpPOI: null,
    selectedIcon: null,
    activePOIID: null,
    flgOc: false,
    hashIcon: new Hash(),
    countid: 0,
    catalogLoaded: false,
    initialize: function (bf, br, bp) {
        ORPOIUserManager.bindFreezePOI = bf;
        ORPOIUserManager.bindNameRemovePOI = br;
        ORPOIUserManager.bindNameDisPopUpPOI = bp
    },
    createPOICatalog: function () {
        if (!ORPOIUserManager.catalogLoaded) {
            ORPOIUserManager.catalogLoaded = true;
            var catlist = ORIconCatalog.getIconCatalogAsArray();
            var ca = new Element("div", {"class": "or-form poi-cat-container"});
            var se = new Element("select");
            $(se).observe("click", function (evt) {
                Event.stop(evt);
                ORPOIUserManager.changePoiCategoryList(this.options[this.selectedIndex].value)
            });
            for (var i = 0; i < catlist.length; i++) {
                var cat = ORIconCatalog.getIconCatalog().get(catlist[i]);
                var op = new Element("option", {value: cat.getId()}).update(cat.getTitle());
                se.appendChild(op)
            }
            ca.appendChild(se);
            $(ORPOIUserManager.htmlDivPOICat).appendChild(ca);
            ORPOIUserManager.changePoiCategoryList(catlist[0])
        }
    },
    changePoiCategoryList: function (idcat) {
        ORPOIUserManager.setSelectedIcon(null);
        $(ORPOIUserManager.htmlDivPOIList).innerHTML = "";
        var cat = ORIconCatalog.getIconCatalog().get(idcat);
        var iconlist = cat.getIconList();
        for (var i = 0; i < iconlist.length; i++) {
            var ic = iconlist[i];
            var dragid = ic.getDomId();
            var imgscrI = ic.getIconUrlI();
            var imgscrA = ic.getIconUrlA();
            var idesc = ic.getDesc();
            var a = new Element("div", {id: dragid, "class": "poiuser-entity"});
            var idm = dragid + "_img";
            a.appendChild(new Element("img", {id: idm, src: imgscrI, alt: idesc, title: idesc}));
            ORPOIUserManager.hashIcon.set(dragid, {idm: idm, ico: ic});
            $(ORPOIUserManager.htmlDivPOIList).appendChild(a);
            $(dragid).observe("click", function (evt) {
                Event.stop(evt);
                ORPOIUserManager.setSelectedIcon(this.id)
            });
            if (i == 0) {
                ORPOIUserManager.setSelectedIcon(dragid)
            }
        }
    },
    setBindFreezePOI: function (bf) {
        ORPOIUserManager.bindFreezePOI = bf
    },
    setBindNameRemovePOI: function (br) {
        ORPOIUserManager.bindNameRemovePOI = br
    },
    setBindNameDisPopUpPOI: function (bp) {
        ORPOIUserManager.bindNameDisPopUpPOI = bp
    },
    setActivePOIID: function (idpoi, pos, imgsrc, descriptable) {
        ORPOIUserManager.activePOIID = idpoi;
        if (idpoi != null) {
            $(ORPOIUserManager.htmlDivPOIDet).innerHTML = "<div style='margin-bottom:3px;'><img style='float:left' src='" + imgsrc + "'/><div style='float:left'><div class='lib'>" + LONGITUDE + ":</div><div class='val' id='poi_udet_lng'>" + pos.lng().toFixed(5) + "</div><span>°</span><br/><div class='lib'>" + LATITUDE + ":</div><div id='poi_udet_lat' class='val'>" + pos.lat().toFixed(5) + "</div><span>°</span><br class='clear'/></div><br class='clear'></div>";
            $(ORPOIUserManager.htmlDivPOIDet).innerHTML += "<div class='action'>";
            if (descriptable == 1) {
                $(ORPOIUserManager.htmlDivPOIDet).innerHTML += "<a href='' onclick='javascript:" + ORPOIUserManager.bindNameDisPopUpPOI + "(" + idpoi + ");return false;'>" + POI_MODIFY + "</a><br/>"
            }
            $(ORPOIUserManager.htmlDivPOIDet).innerHTML += "<a href='' onclick='javascript:" + ORPOIUserManager.bindNameRemovePOI + "(" + idpoi + ");return false;'>Supprimer ce POI</a></div>";
            $(ORPOIUserManager.htmlDivPOIDet).show()
        } else {
            $(ORPOIUserManager.htmlDivPOIDet).innerHTML = "";
            $(ORPOIUserManager.htmlDivPOIDet).hide()
        }
    },
    getActivePOIID: function () {
        return ORPOIUserManager.activePOIID
    },
    setSelectedIcon: function (idpoi) {
        if (ORPOIUserManager.selectedIcon != null) {
            var oldpoi = ORPOIUserManager.hashIcon.get(ORPOIUserManager.selectedIcon);
            $(oldpoi.idm).src = oldpoi.ico.getIconUrlI()
        }
        ORPOIUserManager.selectedIcon = idpoi;
        if (idpoi != null) {
            var newpoi = ORPOIUserManager.hashIcon.get(ORPOIUserManager.selectedIcon);
            $(newpoi.idm).src = newpoi.ico.getIconUrlA()
        }
    },
    getSelectedIcon: function () {
        if (ORPOIUserManager.selectedIcon != null) {
            return ORPOIUserManager.hashIcon.get(ORPOIUserManager.selectedIcon)
        } else {
            return null
        }
    },
    showHideEditor: function (close) {
        if (ORPOIUserManager.flgOc || close) {
            ORPOIUserManager.flgOc = false;
            $(ORPOIUserManager.htmlDivManager).hide();
            ORPOIUserManager.bindFreezePOI()
        } else {
            ORPOIUserManager.createPOICatalog();
            ORPOIUserManager.flgOc = true;
            var idjq = "#" + ORPOIUserManager.htmlDivManager;
            $j(idjq).draggable({handle: idjq + "_handle", containment: "document"});
            $(ORPOIUserManager.htmlDivManager).show()
        }
        return ORPOIUserManager.flgOc
    },
    isEditorOpen: function () {
        return ORPOIUserManager.flgOc
    },
    getNextPOIID: function () {
        return ++ORPOIUserManager.countid
    }
};
var ORInfoPopUpFactory = function () {
    return {
        createEditableContent: function (poiid) {
            var poi = ORRouteManager.findPOIUserInRoutes(poiid);
            var idin = "poi_in";
            var idta = "poi_ta";
            var imgsrc = poi.getORMarker().getORIcon().getIconUrlI();
            var tdes = '<div class="poi-w-header"><img src="' + imgsrc + '"><span class="poi-popup-shortdesc"><input id="' + idin + '" maxsize="25" value="' + (poi.getUserShortDescription().replace(/"/g, "&quot;") || poi.getORMarker().getORIcon().getDesc()) + '"></span><br class="clear"/></div>';
            tdes += '<div class="poi-popup-longdesc"><textarea id="' + idta + '" cols="33">' + poi.getUserDescription() + "</textarea></div>";
            $("poipopup_edddcs").update(tdes);
            $(idin).observe("keyup", function (evt) {
                poi.setUserShortDescription($(evt.target.id).value);
                document.fire("or:poiuser-modif")
            });
            $(idta).observe("keyup", function (evt) {
                poi.setUserDescription($(evt.target.id).value);
                document.fire("or:poiuser-modif")
            })
        }, createReadonlyContent: function (poiid) {
            var poi = ORRouteManager.findPOIUserInRoutes(poiid);
            var base = new Element("div");
            var a = new Element("div", {"class": "poi-r-header"});
            var imgsrc = poi.getORMarker().getORIcon().getIconUrlI();
            a.appendChild(new Element("img", {src: imgsrc}));
            var b = new Element("span");
            var idin = poi.getPoiId() + "_in";
            b.update(poi.getUserShortDescription());
            a.appendChild(b);
            var aBr = new Element("br", {"class": "clear"});
            a.appendChild(aBr);
            var c = new Element("div", {"class": "poi-r-content"});
            if (poi.getUserDescription() != "") {
                var idta = poi.getPoiId() + "_ta";
                c.update(poi.getUserDescription())
            }
            base.appendChild(a);
            base.appendChild(c);
            $("poipopup_roddcs").update(base.innerHTML)
        }
    }
}();
var ORRouteManager = (function () {
    function _saveUserPoi() {
        if (ORMainSer.sdis_poi.length > 0) {
            var poilist = ORMainSer.sdis_poi;
            var out = [];
            poilist.forEach(function (poi) {
                out.push(poi.getPoiAsUrl())
            });
            var idr = btoa(ORMainSer.route_ids);
            new Ajax.Request("../orpoi-user/saveDDCSPoi.php", {
                method: "post",
                parameters: {idr: idr, poiurl_ddcs: $j.toJSON(out)},
                asynchronous: false,
                onSuccess: function (transport) {
                    document.fire("or:poiuser-nomodif")
                }
            })
        } else {
            new Ajax.Request("../orpoi-user/saveDDCSPoi.php", {
                method: "post",
                parameters: {idr: idr, poiurl_ddcs: ""},
                asynchronous: false,
                onSuccess: function (transport) {
                    document.fire("or:poiuser-nomodif")
                }
            })
        }
    }

    return {
        findPOIUserInRoutes: function (poiid) {
            var poi = null;
            var d = ORMainSer.sdis_poi;
            poi = d[poiid];
            return poi
        }, saveUserPoi: function () {
            _saveUserPoi()
        }, activatePoiUserChange: function (val) {
            if (ORMainSer.activeRoute != null) {
                ORMainSer.activeRoute.getData().setPoi2Save(val)
            }
        }
    }
})();
ORIconCatalog.load();
ORPOIUserManager.initialize(ORGMapDecoratorManager.freezePoiUser, "ORGMapDecoratorManager.removePoiUser", "ORGMapDecoratorManager.displayPoiUserEdPopUp");