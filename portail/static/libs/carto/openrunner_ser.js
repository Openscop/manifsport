var legendecouche = "Couches :\n" +
"<label title=\"NATURA 2000 - ZPS\" for=\"chk_zps\">ZPS</label><input title=\"NATURA 2000 - ZPS\" type=\"checkbox\" id=\"chk_zps\" name=\"chk_zps\">\n" +
"<label title=\"NATURA 2000 - SIC\" for=\"chk_zps\">SIC</label><input title=\"NATURA 2000 - SIC\" type=\"checkbox\" id=\"chk_sic\" name=\"chk_sic\">\n" +
"<label title=\"PNR\" for=\"chk_pnr\">PNR</label><input title=\"PNR\" type=\"checkbox\" id=\"chk_pnr\" name=\"chk_pnr\">\n" +
"<label title=\"RNR\" for=\"chk_rnr\">RNR</label><input title=\"RNR\" type=\"checkbox\" id=\"chk_rnr\" name=\"chk_rnr\">\n" +
"<label title=\"Limites Administratives\" for=\"chk_adm\">ADM</label><input title=\"Limites Administratives\" type=\"checkbox\" id=\"chk_adm\" name=\"chk_adm\">\n" +
"<label title=\"Parc National\" for=\"chk_pn\">PN</label><input title=\"Parc National\" type=\"checkbox\" id=\"chk_pn\" name=\"chk_pn\">\n" +
"<label title=\"Réserve biologique\" for=\"chk_ri\">RB</label><input title=\"Réserve biologique\" type=\"checkbox\" id=\"chk_ri\" name=\"chk_ri\">\n" +
"<label title=\"Reserve Nationale\" for=\"chk_rn\">RN</label><input title=\"Reserve Nationale\" type=\"checkbox\" id=\"chk_rn\" name=\"chk_rn\">\n" +
"<label title=\"Arrêtés de protection de biotope\" for=\"chk_pb\">APB</label><input title=\"Arrêtés de protection de biotope\" type=\"checkbox\" id=\"chk_pb\" name=\"chk_pb\">\n" +
"<label title=\"Parcelles forestières publiques\" for=\"chk_pfp\">PFP</label><input title=\"Parcelles forestières publiques\" type=\"checkbox\" id=\"chk_pfp\" name=\"chk_pfp\">\n" +
"<label title=\"Gendarmerie Police\" for=\"chk_gp\">GP</label><input title=\"Gendarmerie Police\" type=\"checkbox\" id=\"chk_gp\" name=\"chk_gp\">\n" +
"<label title=\"Liste des passages à niveau\" for=\"chk_lpn\">LPN</label><input title=\"Liste des passages à niveau\" type=\"checkbox\" id=\"chk_lpn\" name=\"chk_lpn\">\n" +
"<label title=\"Bornage routier\" for=\"chk_br\">BN</label><input title=\"Bornage routier\" type=\"checkbox\" id=\"chk_br\" name=\"chk_br\">\n" +
"<label title=\"Cadastre\" for=\"chk_cad\">CAD</label><input title=\"Cadastre\" type=\"checkbox\" id=\"chk_cad\" name=\"chk_cad\">"


$(function () {
    let divs=document.getElementsByClassName("ex-nat")

    for (var i = 0; i < divs.length; i++) {
        divs[i].innerHTML = legendecouche;
    }

})


var ORConstants = {
    POI_MODE_POPUP: 0,
    POI_MODE_LABEL_RIGHT: 1,
    POI_MODE_LABEL_LEFT: 2,
    POI_MODE_IMG: 3,
    RADIUS_GMAPS: 85445659.4471,
    POI_OFFSET_VERT: 34,
    POI_OFFSET_HORI: 28,
    DEG2RAD: 0.01745329252,
    EARTH_RAD_KM: 6371.598,
    EARTH_RAD_MILES: 3959.127,
    OFFSET_GMAPS: 268435456,
    RADIUS_GMAPS: 85445659.4471
};

var infowindow = new google.maps.InfoWindow()
var ORMainSer = {
    DEFAULT_ZOOM: 15,
    map: null,
    mp: null,
    maploaded: false,
    polyOver: null,
    actroute: null,
    nbroute: 0,
    listkm: null,
    iwinpoiro: null,
    listpoiextra: null,
    sdis_poi: [],
    route_ids: [],
    startKmIcon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 14,
        fillColor: "green",
        fillOpacity: 0.6,
        strokeColor: "green",
        strokeOpacity: 0.8,
        strokeWeight: 2
    },
    kmIcon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 6,
        fillColor: "orange",
        fillOpacity: 0.8,
        strokeColor: "white",
        strokeWeight: 1
    },
    lastKmIcon: {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 10,
        fillColor: "red",
        fillOpacity: 0.6,
        strokeColor: "red",
        strokeOpacity: 0.8,
        strokeWeight: 2
    },
    initService: function() {
        ORMainSer.showGMapRoute();
        if (ORMainSer.sec) {
            $("tools_poiuser").show();
            $("poiuserdata").observe("click", function(evt) {
                Event.stop(evt);
                ORPOIUserManager.showHideEditor(false)
            });
            $("savepoiuserdata").observe("click", function(evt) {
                Event.stop(evt);
                ORRouteManager.saveUserPoi()
            })
        }
        $j("#map").width(window.innerWidth - 250);
        $j("#map").height(window.innerHeight - 80);
        $j("#pa").css("top", window.innerHeight - 180);
        $j(window).resize(function() {
            $j("#map").width(window.innerWidth - 250);
            $j("#map").height(window.innerHeight - 80);
            $j("#pa").css("top", window.innerHeight - 180)
        })
    },
    setParams: function(options, s) {
        ORMainSer.mp = options;
        ORMainSer.sec = (s == 1)
    },
    showGMapRoute: function() {
        if (!ORMainSer.maploaded) {
            ORMainSer.maploaded = true;
            ORMainSer.initGMap()
        }
        var b = new google.maps.LatLngBounds();
        for (i = 0, j = ORMainSer.mp.length; i < j; i++) {
            var br = new google.maps.LatLngBounds();
            var r = ORMainSer.mp[i];
            ORMainSer.route_ids.push(r.idroute);
            var p = r.points;
            if (r.roupoints != "") {
                p = r.roupoints
            }
            var t = decodeLine(p);
            var m = [];
            var mor = [];
            for (var k = 0, l = t.length; k < l; k++) {
                var pt = new google.maps.LatLng(t[k][0], t[k][1]);
                m.push(pt);
                mor.push(new google.maps.LatLng(t[k][0], t[k][1]));
                b.extend(pt);
                br.extend(pt);
                r.mor = mor
            }
            r.bounds = br;
            var str = 2;
            if (ORMainSer.sw == "-1") {
                str = ORMainSer.rsw
            }
            var f = 0.5 * Math.floor(str / 3) + 1;
            var p = "M " + (-2 * f) + "," + (3 * f) + " 0," + (-3 * f) + " " + (2 * f) + "," + (3 * f) + " 0," + (2.25 * f) + " z";
            var lineSymbol = {
                path: p,
                fillColor: "#000",
                strokeColor: "#000",
                fillOpacity: 1,
                strokeOpacity: 1,
                scale: 2
            };
            var poly = new google.maps.Polyline({
                path: m,
                strokeColor: "#000",
                strokeOpacity: 1,
                strokeWeight: str,
                icons: [{
                    icon: lineSymbol,
                    offset: "50px",
                    repeat: "150px"
                }]
            });
            poly.setMap(ORMainSer.map);
            var idr = r.idroute;
            $(idr).innerHTML += "<div class='ex-com-ddcs'>	" + r.routename + "  <br/> <span class='ex-dis-aut'>Distance : </span><span class='ex-dis-value'>" + r.distance + "</span><span class='ex-dis-unit'>km</span>	 <br/>  <span class='ex-dis-aut'>Auteur : </span><span class='ex-dis-value'>" + r.libuser + "</span>	<br/>  <span class='ex-dis-aut'>ID du parcours : </span><span class='ex-dis-value'>" + r.idroute + "</span></div>";
            (function(route, path) {
                var id = route.idroute;
                Event.observe(id, "click", function(event) {
                    ORMainSer.showRoute(route, path)
                })
            })(r, m);
            if (i == 0) {
                ORMainSer.route = r;
                ORMainSer.showRoute(r, m)
            }
        }
        ORMainSer.map.fitBounds(b);
        copyrights.IGN = '<a style="color:#222" target="_blank" href="https://www.geoportail.fr/"><img src="../img/v3/wms/logo_gp.gif" alt="Geoprotail"></a>';
        copyrights.IGN += '&nbsp;<a style="color:#222" target="_blank" href="https://www.ign.fr/"><img src="../img/v3/wms/logo_ign.gif" alt="IGN"></a>';
        copyrights.IGN += '&nbsp;<a style="color:#FFF;background-color:#666" target="_blank" href="https://www.ign.fr/partage/api/cgu/licAPI_CGUF.pdf">Conditions Générales d\'Utilisation API-Géoportail</a>';
        copyrights["IGN-SCAN"] = '<a style="color:#222" target="_blank" href="https://www.geoportail.fr/"><img src="../img/v3/wms/logo_gp.gif" alt="Geoprotail"></a>';
        copyrights["IGN-SCAN"] += '&nbsp;<a style="color:#222" target="_blank" href="https://www.ign.fr/"><img src="../img/v3/wms/logo_ign.gif" alt="IGN"></a>';
        copyrights["IGN-SCAN"] += '&nbsp;<a style="color:#FFF;background-color:#666" target="_blank" href="https://www.ign.fr/partage/api/cgu/licAPI_CGUF.pdf">Conditions Générales d\'Utilisation API-Géoportail</a>';
        ORMainSer.map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(copyrightDiv);


        var ign = ORExtMap.getIGNBaseMap(ORMainSer.map, "GEOGRAPHICALGRIDSYSTEMS.MAPS", {
            name: "IGN"
        });
        var ignExp = ORExtMap.getIGNBaseMap(ORMainSer.map, "GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.CLASSIQUE", {
            name: "IGN-SCAN"
        });
        var ignAdm = ORExtMap.getIGNBaseMap(ORMainSer.map, "ADMINISTRATIVEUNITS.BOUNDARIES", {
            name: "IGN-ADM",
            isPng: true
        });
        var ignCad = ORExtMap.getIGNBaseMap(ORMainSer.map, "CADASTRALPARCELS.PARCELS", {
            name: "IGN-CAD",
            isPng: true,
            style: "bdparcellaire"
        });
        ORMainSer.map.mapTypes.set("IGN", ign.mapType);
        ORMainSer.map.mapTypes.set("IGN-SCAN", ignExp.mapType);
        ORMainSer.map.setOptions({
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID, "IGN", "IGN-SCAN"],
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
                position: google.maps.ControlPosition.TOP_RIGHT
            },
            zoomControlOptions: {
                position: google.maps.ControlPosition.TOP_LEFT
            },
            streetViewControlOptions: {
                position: google.maps.ControlPosition.LEFT_TOP
            }
        });
        google.maps.event.addListener(ORMainSer.map, "maptypeid_changed", onMapTypeIdChanged);
        ORMainSer.iwinpoiro = new google.maps.InfoWindow({
            disableAutoPan: true
        });
        ORMainSer.iwinpoiroddcs = new google.maps.InfoWindow({
            disableAutoPan: true
        });
        ORMainSer.iwinpoiedddcs = new google.maps.InfoWindow();
        if (ORMainSer.sec) {
            google.maps.event.addListener(ORMainSer.iwinpoiedddcs, "domready", function(evt) {
                ORInfoPopUpFactory.createEditableContent($("poipopup_edddcs").innerHTML)
            });
            document.observe("or:poiuser-modif", ORGMapDecoratorManager.activatePoiUserSaveImg);
            document.observe("or:poiuser-nomodif", ORGMapDecoratorManager.deactivatePoiUserSaveImg)
        }
            google.maps.event.addListener(ORMainSer.iwinpoiroddcs, "domready", function(evt) {
                ORInfoPopUpFactory.createReadonlyContent($("poipopup_roddcs").innerHTML)
            });
        ORMainSer.map.addListener("idle", function (event) {
            Chargeleslayer(ORMainSer.map)
        })
        jQuery('body').on("click", "#chk_zps",  function(event) {
            if ($("chk_zps").checked) {
                layer.push('natura_protection_speciale')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('natura_protection_speciale'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_sic",  function(event) {
            if ($("chk_sic").checked) {
                layer.push('natura_directive_habitat')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('natura_directive_habitat'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on( "click", "#chk_pnr", function(event) {
            if ($("chk_pnr").checked) {
                layer.push('parc_naturel_regional')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('parc_naturel_regional'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_rnr",  function(event) {
            if ($("chk_rnr").checked) {
                layer.push('reserve_naturelle_regionale')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('reserve_naturelle_regionale'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click","#chk_adm",  function(event) {
            if ($("chk_adm").checked) {
                layer.push('communespsql')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('communespsql'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_pn",  function(event) {
            if ($("chk_pn").checked) {
                layer.push('parc_national')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('parc_national'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_ri",  function(event) {
            if ($("chk_ri").checked) {
                layer.push('reserve_integrale')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('reserve_integrale'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_rn",  function(event) {
            if ($("chk_rn").checked) {
                layer.push('reserve_nationale')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('reserve_nationale'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_pb",  function(event) {
            if ($("chk_pb").checked) {
                layer.push('protection_biotope0')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('protection_biotope0'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_pfp",  function(event) {
            if ($("chk_pfp").checked) {
                layer.push('parcelles_forestieres_publique0')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('parcelles_forestieres_publique0'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_gp",  function(event) {
            if ($("chk_gp").checked) {
                layer.push('gend_police0')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('gend_police0'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_lpn",  function(event) {
            if ($("chk_lpn").checked) {
                layer.push('liste_passage_niveau')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('liste_passage_niveau'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });
        jQuery('body').on("click", "#chk_br",  function(event) {
            if ($("chk_br").checked) {
                layer.push('bornage_routier0')
                Chargeleslayer(ORMainSer.map)
            } else {
                layer.splice( layer.indexOf('bornage_routier0'), 1 )
                Chargeleslayer(ORMainSer.map)
            }
        });

        jQuery('body').on("click", "#chk_cad",  function(event) {
            if ($("chk_cad").checked) {
                ORMainSer.map.overlayMapTypes.setAt(5, ignCad.mapType)
            } else {
                ORMainSer.map.overlayMapTypes.setAt(5, null)
            }
        });
        var data = new ORGMapRouteData();
        if (ORMainSer.route.poiurl_extra != undefined && ORMainSer.route.poiurl_extra != "") {
            var poitab = $j.parseJSON(ORExtUtils.urldecode(ORMainSer.route.poiurl_extra));
            ORMainSer.poiurl_extra = poitab;
            for (var i = 0; i < poitab.length; i++) {
                var tmppoi = poitab[i];
                var poishortdesc = tmppoi.poi_title;
                var poidesc = tmppoi.poi_desc;
                var ico = ORIconCatalog.getIconById(tmppoi.poi_id);
                var orm = new ORMarker(ico);
                var orlatlng = new google.maps.LatLng(parseFloat(tmppoi.poi_lat), parseFloat(tmppoi.poi_lng));
                var poi = new ORPoi(ico.getId(), orlatlng, orm, poishortdesc, 1);
                poi.setUserDescription(poidesc);
                ORMainSer.sdis_poi[poi.getPoiId()] = poi
            }
            if (poitab.length > 0) {
                ORGMapDecoratorManager.showPoiUser()
            }
            google.maps.event.addListener(ORMainSer.map, "click", function(event) {
                if (event.latLng) {
                    if (ORPOIUserManager.isEditorOpen()) {
                        ORGMapDecoratorManager.addPoiUserMarker(event.latLng)
                    }
                }
            });
            if (ORMainSer.sec) {
                Event.observe($("layer_poiuser_close"), "click", function() {
                    ORPOIUserManager.showHideEditor(true)
                })
            }
        }
    },
    showRoute: function(route, path) {
        if (ORMainSer.actroute != null) {
            $(ORMainSer.actroute).removeClassName("active")
        }
        ORMainSer.actroute = route.idroute;
        ORMainSer.route = route;
        $(route.idroute).addClassName("active");
        if (ORMainSer.polyOver != null) {
            ORMainSer.polyOver.setMap(null)
        }
        var str_hl = 3;
        var f = 0.5 * Math.floor(str_hl / 3) + 1;
        var p = "M " + (-2 * f) + "," + (3 * f) + " 0," + (-3 * f) + " " + (2 * f) + "," + (3 * f) + " 0," + (2.25 * f) + " z";
        var lineSymbol = {
            path: p,
            fillColor: "#FF0000",
            strokeColor: "#FF0000",
            fillOpacity: 0.6,
            strokeOpacity: 0.6,
            scale: 2
        };
        ORMainSer.polyOver = new google.maps.Polyline({
            path: path,
            strokeColor: "#FF0000",
            strokeOpacity: 0.6,
            strokeWeight: 6,
            zIndex: -1000000,
            icons: [{
                icon: lineSymbol,
                offset: "50px",
                repeat: "150px"
            }]
        });
        ORMainSer.polyOver.setMap(ORMainSer.map);
        var kmp = 5;
        if (route.distance > 0) {
            var kmp = Math.round(30 / (600 / route.distance))
        }
        if (kmp < 5) {
            kmp = 5
        } else {
            if (kmp > 5 && kmp < 10) {
                kmp = 10
            } else {
                if (kmp > 10 && kmp < 15) {
                    kmp = 15
                } else {
                    if (kmp > 15 && kmp < 20) {
                        kmp = 20
                    } else {
                        if (kmp > 20 && kmp < 25) {
                            kmp = 25
                        } else {
                            if (kmp > 25 && kmp < 30) {
                                kmp = 30
                            } else {
                                if (kmp > 30) {
                                    kmp = 50
                                }
                            }
                        }
                    }
                }
            }
        }
        $j('#dl').attr('href', '/kml/exportImportGPX.php?rttype=0&id=' + ORMainSer.actroute);
        $j("#pa").load("/elevation/findORElevation4ser.php?id=" + route.idroute + "&h=200&w=700&pa=1&k=" + kmp + "&c=0&nl=2");
        if (ORMainSer.listkm != null) {
            var tabsize = ORMainSer.listkm.length;
            for (var i = 0; i < tabsize; i++) {
                var stone = ORMainSer.listkm[i];
                if (stone != null) {
                    stone.setMap(null)
                }
            }
        }
        if (!route.listkm) {
            if (route.distance < 10) {
                ORMainSer.createStonesMarker(route, path, 1)
            } else {
                if (route.distance < 40) {
                    ORMainSer.createStonesMarker(route, path, 5)
                } else {
                    if (route.distance < 50) {
                        ORMainSer.createStonesMarker(route, path, 10)
                    } else {
                        if (route.distance < 100) {
                            ORMainSer.createStonesMarker(route, path, 20)
                        } else {
                            ORMainSer.createStonesMarker(route, path, 25)
                        }
                    }
                }
            }
        }
        ORMainSer.listkm = route.listkm;
        if (ORMainSer.listkm != null) {
            var tabsize = ORMainSer.listkm.length;
            for (var i = 0; i < tabsize; i++) {
                var stone = ORMainSer.listkm[i];
                if (stone != null) {
                    stone.setMap(ORMainSer.map)
                }
            }
        }
        if (ORMainSer.tab_poi != null) {
            var tabsize = ORMainSer.tab_poi.length;
            for (var i = 0; i < tabsize; i++) {
                var poi = ORMainSer.tab_poi[i];
                if (poi != null) {
                    poi.marker.setMap(null)
                }
            }
        }
        if (ORMainSer.route.poiurl != "") {
            ORMainSer.route.tab_poi = [];
            var pois = $j.parseJSON(ORExtUtils.urldecode(ORMainSer.route.poiurl));
            for (var i = 0; i < pois.length; i++) {
                var poi = pois[i];
                var marker = ORMainSer.createGMapsPOIUserMarker(poi);
                ORMainSer.poiUserOnGrid(poi, marker);
                ORMainSer.poiDisplayLabel(poi, marker);
                marker.setMap(ORMainSer.map);
                route.tab_poi.push({
                    poi: poi,
                    marker: marker
                })
            }
        }
        ORMainSer.tab_poi = route.tab_poi;
        if (ORMainSer.tab_poi != null) {
            var tabsize = ORMainSer.tab_poi.length;
            for (var i = 0; i < tabsize; i++) {
                var poi = ORMainSer.tab_poi[i];
                if (poi != null) {
                    poi.marker.setMap(ORMainSer.map)
                }
            }
        }
    },
    createGMapsPOIUserMarker: function(poi) {
        var icon = new google.maps.MarkerImage();
        icon.url = "../" + poi.poi_img_i;
        icon.size = new google.maps.Size(30, 37);
        icon.anchor = new google.maps.Point(15, 37);
        icon.infoWindowAnchor = new google.maps.Point(15, 37);
        var marker = new google.maps.Marker({
            raiseOnDrag: false,
            draggable: false,
            position: new google.maps.LatLng(poi.poi_lat, poi.poi_lng),
            icon: icon
        });
        google.maps.event.addListener(marker, "mouseover", function() {
            ORMainSer.iwinpoiro.close();
            marker.getIcon().url = "../" + poi.poi_img_a;
            marker.setIcon(marker.getIcon());
            if (poi.display_mode == ORConstants.POI_MODE_POPUP) {
                var c = "poi-r-popup-desc";
                if (poi.poi_desc == "") {
                    c = "poi-r-popup-nodesc"
                }
                ORMainSer.iwinpoiro.setContent("<div id='poipopup_ro' class='" + c + " or-form poi-container'><div class='poi-r-header'><img src='../" + poi.poi_img_a + "'><span>" + poi.poi_title + "</span><br class='clear'></div><div class='poi-r-content'>" + poi.poi_desc + "</div>");
                ORMainSer.iwinpoiro.open(ORMainSer.map, marker)
            }
        });
        google.maps.event.addListener(marker, "mouseout", function() {
            marker.getIcon().url = "../" + poi.poi_img_i;
            marker.setIcon(marker.getIcon());
            ORMainSer.iwinpoiro.close()
        });
        return marker
    },
    poiUserOnGrid: function(poi, marker) {
        if (poi.grid_position && poi.grid_position != "0000") {
            var off_x = 0;
            var off_y = 0;
            if (poi.grid_position.charAt(0) == "1") {
                off_x = -ORConstants.POI_OFFSET_HORI
            }
            if (poi.grid_position.charAt(1) == "1") {
                off_y = ORConstants.POI_OFFSET_VERT
            }
            if (poi.grid_position.charAt(2) == "1") {
                off_x = ORConstants.POI_OFFSET_HORI
            }
            if (poi.grid_position.charAt(3) == "1") {
                off_y = -ORConstants.POI_OFFSET_VERT
            }
            var pos_poi_new = ORExtUtils.calculatePoiPositionInGrid(poi.poi_lat, poi.poi_lng, off_x, off_y, ORMainSer.map.getZoom());
            marker.setPosition(new google.maps.LatLng(pos_poi_new.lat, pos_poi_new.lng))
        }
    },
    poiDisplayLabel: function(poi, marker) {
        if (poi.display_mode == ORConstants.POI_MODE_LABEL_RIGHT || poi.display_mode == ORConstants.POI_MODE_LABEL_LEFT) {
            var offsetLab = new google.maps.Size(16, -35);
            if (poi.display_mode == ORConstants.POI_MODE_LABEL_LEFT) {
                offsetLab = new google.maps.Size(-114, -35)
            }
            var myOptions = {
                content: '<div class="poi-mode-label"><div class="poi-label-text">' + poi.poi_title + "</div></div>",
                disableAutoPan: true,
                pixelOffset: offsetLab,
                position: new google.maps.LatLng(poi.poi_lat, poi.poi_lng),
                closeBoxURL: "",
                isHidden: false,
                enableEventPropagation: false
            };
            var ibLabel = new InfoBox(myOptions);
            ibLabel.open(ORMainSer.map)
        }
    },
    initGMap: function() {
        $("map").show();
        var mapOptions = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggableCursor: "crosshair",
            keyboardShortcuts: true,
            scrollwheel: true,
            zoom: ORMainSer.DEFAULT_ZOOM
        };
        ORMainSer.map = new google.maps.Map(document.getElementById("map"), mapOptions);
        ORMainSer.map.setOptions({
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID],
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            }
        });
        google.maps.event.addListener(ORMainSer.map, "maptypeid_changed", onMapTypeIdChanged);
        ORMainSer.map.setCenter(new google.maps.LatLng(48.8, 2), ORMainSer.DEFAULT_ZOOM)
    },
    dPGMk: function(point) {
        if (point) {
            var pt = new google.maps.LatLng(point.lat(), point.lng());
            var im = "../img/pt-11-P.png";
            if (ORMainSer.mkp != null) {
                ORMainSer.mkp.setPosition(pt)
            } else {
                ORMainSer.mkp = new google.maps.Marker({
                    position: pt,
                    map: ORMainSer.map,
                    icon: ORMainSer.kmIcon
                })
            }
        }
    },
    animElevation: function(idx) {
        cen = ORExtUtils.findPoiGeographicPosition(ORMainSer.route.mor, idx, "km");
        ORMainSer.dPGMk(cen)
    },
    centerMap: function(evt, idx) {
        console.log(evt);
        if (evt) {
            if (evt.detail == "1") {
                cen = ORExtUtils.findPoiGeographicPosition(ORMainSer.route.mor, idx, "km");
                ORMainSer.map.setCenter(cen)
            }
            if (evt.detail == "2") {
                if ((ORMainSer.map.mapTypes[ORMainSer.map.getMapTypeId()]).maxZoom > ORMainSer.map.getZoom()) {
                    ORMainSer.map.setZoom((ORMainSer.map.getZoom() + 1))
                }
            }
        }
    },
    createStonesMarker: function(route, path, step) {
        var pathkm = ORExtUtils.calculateFixedPosition(path, route.distance, step, "km");
        var mileicon = null;
        var a = route;
        var listkm = [];
        var tabsize = pathkm.length;
        var svg_template = '<svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" width="25px" height="42px" viewBox="0 0 260 420"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><circle stroke="#F00" stroke-width="12" stroke-position="inside" fill="#FFF" cx="127" cy="127" r="122"></circle><path d="M127,260 L127,415" id="Line" stroke="#000" stroke-width="12" stroke-linecap="square"></path></g><g><text y="138" x="130" text-anchor="middle" alignment-baseline="middle" font-family="Arial" font-weight="bold" font-size="110">{{poskm}}</text></g></svg>';
        for (var i = 0; i < tabsize; i++) {
            if (i == 0) {
                mileicon = ORMainSer.startKmIcon
            }
            var poskm = i * step;
            svg_km = svg_template.replace("{{poskm}}", poskm.toString());
            var pos = pathkm[i];
            if (i == 0) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pos.lat(), pos.lng()),
                    icon: mileicon,
                    zIndex: 6
                })
            } else {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pos.lat(), pos.lng()),
                    optimized: false,
                    zIndex: 10,
                    icon: {
                        url: "data:image/svg+xml;charset=UTF-8;base64," + btoa(svg_km),
                        scaledSize: new google.maps.Size(25, 42)
                    }
                })
            }
            listkm.push(marker)
        }
        if (tabsize > 0) {
            mileicon = ORMainSer.lastKmIcon;
            var point = path[path.length - 1];
            marker = new google.maps.Marker({
                position: point,
                icon: mileicon,
                zIndex: 7
            });
            listkm.push(marker)
        }
        route.listkm = listkm
    }
};
var ORExtUtils = {
    calculateDistanceBetweenTwoORLatLng: function(pts, startIndex, endIndex, unit) {
        var s = 0;
        if (pts.length > 1 && startIndex >= 0 && endIndex > 0) {
            for (var i = startIndex; i < endIndex; i++) {
                u = Math.sin(pts[i].lat() * ORConstants.DEG2RAD) * Math.sin(pts[i + 1].lat() * ORConstants.DEG2RAD) + Math.cos(pts[i].lat() * ORConstants.DEG2RAD) * Math.cos(pts[i + 1].lat() * ORConstants.DEG2RAD) * Math.cos(pts[i + 1].lng() * ORConstants.DEG2RAD - pts[i].lng() * ORConstants.DEG2RAD);
                if (u >= -1 && u <= 1) {
                    if (unit == "km") {
                        s = s + ORConstants.EARTH_RAD_KM * Math.acos(u)
                    } else {
                        s = s + ORConstants.EARTH_RAD_MILES * Math.acos(u)
                    }
                }
            }
        }
        return s
    },
    lngToX: function(lng) {
        return Math.round(ORConstants.OFFSET_GMAPS + ORConstants.RADIUS_GMAPS * lng * Math.PI / 180)
    },
    XToLng: function(x) {
        return (180 / Math.PI) * (x - ORConstants.OFFSET_GMAPS) / ORConstants.RADIUS_GMAPS
    },
    latToY: function(lat) {
        return Math.round((ORConstants.OFFSET_GMAPS - ORConstants.RADIUS_GMAPS * Math.log((1 + Math.sin(lat * Math.PI / 180)) / (1 - Math.sin(lat * Math.PI / 180)))) / 2)
    },
    YToLat: function(y) {
        return (180 / Math.PI) * (2 * Math.atan(Math.exp((ORConstants.OFFSET_GMAPS - 2 * y) / (2 * ORConstants.RADIUS_GMAPS))) - Math.PI / 2)
    },
    calculatePoiPositionInGrid: function(lat1, lng1, off_x, off_y, zoom) {
        var x1 = this.lngToX(lng1);
        var y1 = this.latToY(lat1);
        var dx1x2 = Math.sqrt(Math.pow(off_x << (21 - zoom), 2));
        if (off_x > 0) {
            dx1x2 = -dx1x2
        }
        var lng2 = this.XToLng(x1 - dx1x2);
        var dy1y2 = Math.sqrt(Math.pow(off_y << (21 - zoom), 2));
        if (off_y < 0) {
            dy1y2 = -dy1y2
        }
        var lat2 = this.YToLat(y1 - dy1y2);
        return {
            lat: lat2,
            lng: lng2
        }
    },
    calculateFixedPosition: function(pts, distance, inc, unit) {
        if (inc == 0) {
            return []
        }
        var mDifDis = 0;
        var mDifDisN = 0;
        var mDisDone = 0;
        var mLastPoint = 0;
        var mPointRef = 1;
        var tmpDistance = 0;
        var compt = inc;
        var xe = 1;
        var tabout = [];
        if (pts.length > 0) {
            tabout.push(pts[0])
        }
        while (compt <= distance && compt <= 1200) {
            mDifDis = 0;
            mDifDisN = 0;
            for (var i = mLastPoint; i < pts.length - 1; i++) {
                mDifDis = this.calculateDistanceBetweenTwoORLatLng(pts, i, i + 1, unit);
                tmpDistance = tmpDistance + mDifDis;
                if (tmpDistance.toFixed(3) >= compt) {
                    mDifDisN = tmpDistance - compt;
                    mLastPoint = i;
                    tmpDistance = tmpDistance - mDifDis;
                    xe = mDifDis / mDifDisN;
                    var tlat = pts[i + 1].lat() - (pts[i + 1].lat() - pts[i].lat()) / xe;
                    var tlng = pts[i + 1].lng() - (pts[i + 1].lng() - pts[i].lng()) / xe;
                    tabout.push(new google.maps.LatLng(tlat, tlng));
                    break
                }
            }
            compt += inc
        }
        return tabout
    },
    findPoiGeographicPosition: function(pts, dis, unit) {
        var mDifDis = 0;
        var mDifDisN = 0;
        var mDisDone = 0;
        var mLastPoint = 0;
        var mPointRef = 1;
        var tmpDistance = 0;
        var compt = parseFloat(dis);
        var lat;
        var lng;
        var pos;
        for (var i = mLastPoint; i < pts.length - 1; i++) {
            mDifDis = this.calculateDistanceBetweenTwoORLatLng(pts, i, i + 1, unit);
            tmpDistance = tmpDistance + mDifDis;
            if (tmpDistance.toFixed(3) >= compt) {
                mDifDisN = tmpDistance - compt;
                mLastPoint = i;
                tmpDistance = tmpDistance - mDifDis;
                var xe = mDifDis / mDifDisN;
                lat = pts[i + 1].lat() - (pts[i + 1].lat() - pts[i].lat()) / xe;
                lng = pts[i + 1].lng() - (pts[i + 1].lng() - pts[i].lng()) / xe;
                pos = new google.maps.LatLng(lat, lng, 0, 0);
                break
            }
        }
        return pos
    },
    urldecode: function(str) {
        return decodeURIComponent((str + "").replace(/%(?![\da-f]{2})/gi, function() {
            return "%25"
        }).replace(/\+/g, "%20"))
    }
};
function decodeLine(encoded) {
    var len = encoded.length;
    var index = 0;
    var array = new Array();
    var lat = 0;
    var lng = 0;
    while (index < len) {
        var b;
        var shift = 0;
        var result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 31) << shift;
            shift += 5
        } while (b >= 32);
        var dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = encoded.charCodeAt(index++) - 63;
            result |= (b & 31) << shift;
            shift += 5
        } while (b >= 32);
        var dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        array.push([lat * 0.00001, lng * 0.00001])
    }
    return array
}
var copyrightDiv;
var copyrights = {};
copyrightDiv = document.createElement("div");
copyrightDiv.id = "map-copyright";
copyrightDiv.style.fontSize = "11px";
copyrightDiv.style.fontFamily = "Arial, sans-serif";
copyrightDiv.style.margin = "0 2px 2px 0";
copyrightDiv.style.whiteSpace = "nowrap";
function onMapTypeIdChanged() {
    var newMapType = ORMainSer.map.getMapTypeId();
    copyrightDiv = document.getElementById("map-copyright");
    if (newMapType in copyrights) {
        copyrightDiv.innerHTML = copyrights[newMapType]
    } else {
        copyrightDiv.innerHTML = ""
    }
}
var IGN_KEY = "8e68qrbqv9abwhnyiydw8zqo";
var ORExtMap = {};
ORExtMap.getIGNBaseMap = function(map, layer, options, originators) {
    var tileSize,
        copyright,
        territory = "FXX",
        listeners = {},
        scale0Geop = 559082264,
        options = options || {},
        googProj = new ORExtMap.Projection.google(),
        options = {
            alt: options.alt || "IGN " + layer,
            getTileUrl: getTileUrl,
            isPng: options.isPng || false,
            maxZoom: options.maxZoom || 18,
            minZoom: options.minZoom || 2,
            name: options.name || "IGN " + layer,
            tileSize: new google.maps.Size(256, 256),
            style: options.style || "normal"
        },
        params = {
            ANF: {
                bounds: [11.7, -64, 18.18, -59]
            },
            ASP: {
                bounds: [-40, 76, -36, 79]
            },
            CRZ: {
                bounds: [-48, 47, -44, 55]
            },
            FXX: {
                bounds: [27.33, -31.17, 80.83, 69.03]
            },
            GUF: {
                bounds: [-4.3, -62.1, 11.5, -46]
            },
            MYT: {
                Kx: 108886.89283435,
                bounds: [-17.5, 40, 3, 56]
            },
            NCL: {
                Kx: 103213.63456212,
                bounds: [-24.3, 160, -17.1, 170]
            },
            PYF: {
                Kx: 107526.37112657,
                bounds: [-28.2, -160, 11, -108]
            },
            REU: {
                Kx: 103925.69769224,
                bounds: [-26.2, 37.5, -17.75, 60]
            },
            SPM: {
                Kx: 75919.710164,
                bounds: [43.5, -60, 52, -50]
            },
            WLF: {
                Kx: 108012.82616793,
                bounds: [-14.6, -178.5, -12.8, -175.8]
            }
        },
        mapType = new google.maps.ImageMapType(options),
        locked = false,
        originators = originators;
    for (var t in params) {
        if (params.hasOwnProperty(t)) {
            params[t].bounds = new google.maps.LatLngBounds(new google.maps.LatLng(params[t].bounds[0], params[t].bounds[1]), new google.maps.LatLng(params[t].bounds[2], params[t].bounds[3]))
        }
    }
    function getTileUrl(point, zoom) {
        return "https://wxs.ign.fr/{key}/wmts?SERVICE=WMTS&REQUEST=GetTile&VERSION=1.0.0&LAYER={layer}&STYLE={style}&TILEMATRIXSET=PM&TILEMATRIX={tilematrix}&TILEROW={tilerow}&TILECOL={tilecol}&FORMAT=image/{imgFormat}".replace("{tilerow}", point.y).replace("{tilecol}", point.x).replace("{key}", IGN_KEY).replace("{tilematrix}", zoom).replace("{layer}", layer).replace("{style}", options.style).replace("{imgFormat}", (options.isPng) ? "png" : "jpeg")
    }
    function initProjection() {
        var scale,
            projFactory,
            oldTerritory = territory,
            zoom = map.getZoom();
        var bounds = map.getBounds();
        if (!params[territory] || (bounds && !params[territory].bounds.intersects(bounds))) {
            for (var t in params) {
                if (params.hasOwnProperty(t) && bounds && params[t].bounds.intersects(bounds)) {
                    territory = t;
                    break
                }
            }
        }
        scale = 0.00028 * scale0Geop;
        projFactory = function(territory) {
            return ORExtMap.Projection.geoportalV3()
        };
        if (!bounds || oldTerritory != territory) {
            var center = map.getCenter();
            googProj.proj = projFactory(territory);
            googProj.scale0 = scale;
            map.setCenter(center)
        }
    }
    initProjection();
    mapType.projection = googProj;
    return {
        mapType: mapType,
        bounds: (function() {
            var bounds = [];
            for (var t in params) {
                if (params.hasOwnProperty(t)) {
                    bounds.push(params[t].bounds)
                }
            }
            return bounds
        })()
    }
};
ORExtMap.Projection = (function() {
    function deg2rad(deg) {
        return deg * Math.PI / 180
    }
    function rad2deg(rad) {
        return rad / Math.PI * 180
    }
    function deg2secsex(angle) {
        var deg = parseInt(angle),
            min = parseInt((angle - deg) * 60),
            sec = (((angle - deg) * 60) - min) * 60;
        return sec + min * 60 + deg * 3600
    }
    var wgs84 = {
        A: 6378137,
        B: 6356752.314,
        IF: 298.257220143
    };
    function Projection(proj, scale0) {
        this.proj = proj;
        this.scale0 = scale0
    }
    Projection.prototype.fromLatLngToPoint = function(latLng) {
        var coord = this.proj.forward(latLng.lat(), latLng.lng());
        return new google.maps.Point((coord.x - this.proj.origin.x) / this.scale0, (this.proj.origin.y - coord.y) / this.scale0)
    };
    Projection.prototype.fromPointToLatLng = function(point, noWrap) {
        var coord = this.proj.inverse(this.proj.origin.x + point.x * this.scale0, this.proj.origin.y - point.y * this.scale0);
        return new google.maps.LatLng(coord.lat, coord.lng, noWrap)
    };
    return {
        geoportalV3: function() {
            return {
                name: "IGNF:GEOPORTAL{territory}",
                origin: {
                    x: -20037508,
                    y: 20037508
                },
                forward: function(lat, lng) {
                    return {
                        x: wgs84.A * deg2rad(lng),
                        y: wgs84.A * Math.log(Math.tan(deg2rad(lat / 2) + Math.PI / 4))
                    }
                },
                inverse: function(x, y) {
                    return {
                        lng: rad2deg(x) / wgs84.A,
                        lat: rad2deg(2 * (Math.atan(Math.exp(y / 6378137)) - Math.PI / 4))
                    }
                }
            }
        },
        google: function(proj, scale0) {
            return new Projection(proj, scale0)
        }
    }
})();


