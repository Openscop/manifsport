console.log('ok');
var infowindow = new google.maps.InfoWindow()

function showCGUAnchor() {
    return ORAccMain.callCGUForm("cookie"),
    !1
}
function LongPress(e, t) {
    this.length_ = t;
    var n = this;
    n.elem_ = e,
    n.timeoutId_ = null,
    google.maps.event.addListener(e, "mousedown", function(e) {
        n.onMouseDown_(e)
    }),
    google.maps.event.addListener(e, "mouseup", function(e) {
        n.onMouseUp_(e)
    }),
    google.maps.event.addListener(e, "drag", function(e) {
        n.onMapDrag_(e)
    })
}
function encodeNumber(e) {
    for (var t = ""; e >= 32; )
        t += String.fromCharCode((32 | 31 & e) + 63),
        e >>= 5;
    return t += String.fromCharCode(e + 63)
}
function encodeSignedNumber(e) {
    var t = e << 1;
    return 0 > e && (t = ~t),
    encodeNumber(t)
}
function createEncodings(e) {
    var t = 0
      , n = 0
      , a = 0
      , o = "";
    for (t = 0; t < e.length; ++t) {
        var i = e[t]
          , r = i.lat()
          , s = i.lng()
          , l = Math.round(1e5 * r)
          , c = Math.round(1e5 * s)
          , u = l - n
          , g = c - a;
        n = l,
        a = c,
        o += encodeSignedNumber(u) + encodeSignedNumber(g)
    }
    return o
}
function decodeLine(e) {
    for (var t = e.length, n = 0, a = new Array, o = 0, i = 0; t > n; ) {
        var r, s = 0, l = 0;
        do
            r = e.charCodeAt(n++) - 63,
            l |= (31 & r) << s,
            s += 5;
        while (r >= 32);var c = 1 & l ? ~(l >> 1) : l >> 1;
        o += c,
        s = 0,
        l = 0;
        do
            r = e.charCodeAt(n++) - 63,
            l |= (31 & r) << s,
            s += 5;
        while (r >= 32);var u = 1 & l ? ~(l >> 1) : l >> 1;
        i += u,
        a.push([1e-5 * o, 1e-5 * i])
    }
    return a
}
function findPoiCol(latmin, latmax, lngmin, lngmax, ale, type) {
    var tabpoi = {}
      , delta_lat = Math.abs(latmax - latmin)
      , delta_lng = Math.abs(lngmax - lngmin);
    if (delta_lat > COL_SEARCH_AREA_MAX || delta_lng > COL_SEARCH_AREA_MAX)
        ale && ORMain.showAlertInlineOR(ZOOM_LEVEL_PASS);
    else {
        $("tab_plan_main_message").innerHTML = "";
        var url = "poi-utils/showPoiCol.php";
        new Ajax.Request(url,{
            method: "post",
            parameters: {
                latmin: latmin,
                lngmin: lngmin,
                latmax: latmax,
                lngmax: lngmax,
                level: 0,
                type: type
            },
            asynchronous: !1,
            onSuccess: function(transport, json) {
                var out = transport.responseText.replace(C_SEP, "");
                out = decodeCol(out),
                tabpoi = eval(out)
            }
        })
    }
    return tabpoi
}
function findPoiPartner(e, t, n, a, o, i) {
    var r = {}
      , s = Math.abs(n - t)
      , l = Math.abs(o - a);
    if (s > GITE_SEARCH_AREA_MAX || l > GITE_SEARCH_AREA_MAX)
        i && ORMain.showAlertOR(ZOOM_LEVEL_GITES);
    else {
        var c = "poi-utils/showPoiPartner.php";
        new Ajax.Request(c,{
            method: "post",
            parameters: {
                latmin: t,
                lngmin: a,
                latmax: n,
                lngmax: o,
                level: 0,
                type: e
            },
            asynchronous: !1,
            onSuccess: function(e) {
                r = e.responseText.evalJSON(!0)
            }
        })
    }
    return r
}
function colinearite(e) {
    for (var t = [], n = 0, a = e.length - 2; a > n; n++) {
        var o = e[n]
          , i = e[n + 1]
          , r = e[n + 2];
        0 == n && t.push(o);
        var s = i[1] - o[1]
          , l = i[0] - o[0]
          , c = r[1] - i[1]
          , u = r[0] - i[0]
          , g = s * c + l * u
          , p = Math.sqrt(Math.pow(s, 2) + Math.pow(l, 2))
          , d = Math.sqrt(Math.pow(c, 2) + Math.pow(u, 2))
          , R = p * d
          , M = 0;
        M = 0 != R ? Math.abs(Math.acos(g / R)) : 0,
        M > angle_limit ? t.push(i) : totfiltre++,
        n == e.length - 3 && t.push(r)
    }
    return t
}
function dateTCXFormat(e) {
    var t = ""
      , n = e.getFullYear()
      , a = e.getMonth() + 1;
    a = 10 > a ? "0" + a : a;
    var o = e.getDate();
    o = 10 > o ? "0" + o : o;
    var i = e.getHours();
    i = 10 > i ? "0" + i : i;
    var r = e.getMinutes();
    r = 10 > r ? "0" + r : r;
    var s = e.getSeconds();
    return s = 10 > s ? "0" + s : s,
    t = n + "-" + a + "-" + o + "T" + i + ":" + r + ":" + s + "Z"
}
function getScrollOffset() {
    var e;
    return self.pageYOffset ? e = self.pageYOffset : document.documentElement && document.documentElement.scrollTop ? e = document.documentElement.scrollTop : document.body && (e = document.body.scrollTop),
    e
}
function decodeCol(e) {
    var t, n, a, o, i, r = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", s = "", l = "", c = "", u = 0;
    e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    do
        a = r.indexOf(e.charAt(u++)),
        o = r.indexOf(e.charAt(u++)),
        i = r.indexOf(e.charAt(u++)),
        c = r.indexOf(e.charAt(u++)),
        t = a << 2 | o >> 4,
        n = (15 & o) << 4 | i >> 2,
        l = (3 & i) << 6 | c,
        s += String.fromCharCode(t),
        64 != i && (s += String.fromCharCode(n)),
        64 != c && (s += String.fromCharCode(l)),
        t = n = l = "",
        a = o = i = c = "";
    while (u < e.length);return unescape(s)
}
function encodeNumber(e) {
    for (var t = ""; e >= 32; )
        t += String.fromCharCode((32 | 31 & e) + 63),
        e >>= 5;
    return t += String.fromCharCode(e + 63)
}
function encodeSignedNumber(e) {
    var t = e << 1;
    return 0 > e && (t = ~t),
    encodeNumber(t)
}
function createEncodings(e) {
    var t = 0
      , n = 0
      , a = 0
      , o = "";
    for (t = 0; t < e.length; ++t) {
        var i = e[t]
          , r = i.lat()
          , s = i.lng()
          , l = Math.round(1e5 * r)
          , c = Math.round(1e5 * s)
          , u = l - n
          , g = c - a;
        n = l,
        a = c,
        o += encodeSignedNumber(u) + encodeSignedNumber(g)
    }
    return o
}
function decodeLine(e) {
    for (var t = e.length, n = 0, a = new Array, o = 0, i = 0; t > n; ) {
        var r, s = 0, l = 0;
        do
            r = e.charCodeAt(n++) - 63,
            l |= (31 & r) << s,
            s += 5;
        while (r >= 32);var c = 1 & l ? ~(l >> 1) : l >> 1;
        o += c,
        s = 0,
        l = 0;
        do
            r = e.charCodeAt(n++) - 63,
            l |= (31 & r) << s,
            s += 5;
        while (r >= 32);var u = 1 & l ? ~(l >> 1) : l >> 1;
        i += u,
        a.push([1e-5 * o, 1e-5 * i])
    }
    return a
}
function encodeNumber(e) {
    for (var t = ""; e >= 32; )
        t += String.fromCharCode((32 | 31 & e) + 63),
        e >>= 5;
    return t += String.fromCharCode(e + 63)
}
function encodeSignedNumber(e) {
    var t = e << 1;
    return 0 > e && (t = ~t),
    encodeNumber(t)
}
function createEncodings(e) {
    var t = 0
      , n = 0
      , a = 0
      , o = "";
    for (t = 0; t < e.length; ++t) {
        var i = e[t]
          , r = i.lat()
          , s = i.lng()
          , l = Math.round(1e5 * r)
          , c = Math.round(1e5 * s)
          , u = l - n
          , g = c - a;
        n = l,
        a = c,
        o += encodeSignedNumber(u) + encodeSignedNumber(g)
    }
    return o
}
function decodeLine(e) {
    for (var t = e.length, n = 0, a = new Array, o = 0, i = 0; t > n; ) {
        var r, s = 0, l = 0;
        do
            r = e.charCodeAt(n++) - 63,
            l |= (31 & r) << s,
            s += 5;
        while (r >= 32);var c = 1 & l ? ~(l >> 1) : l >> 1;
        o += c,
        s = 0,
        l = 0;
        do
            r = e.charCodeAt(n++) - 63,
            l |= (31 & r) << s,
            s += 5;
        while (r >= 32);var u = 1 & l ? ~(l >> 1) : l >> 1;
        i += u,
        a.push([1e-5 * o, 1e-5 * i])
    }
    return a
}
function submitMail() {
    var e = document.forms.sendmailform.txtimg.value
      , t = document.forms.sendmailform.email.value;
    "" == t && alert(EMAIL_EMPTY),
    0 == checkemail(t) ? alert("Adresse mail non valide!") : "" == e ? alert(TXT_ABOVE_EMPTY) : document.forms.sendmailform.submit()
}
function submitMailContact() {
    var e = document.forms.sendmailform.txtimg.value
      , t = document.forms.sendmailform.mailcomment.value;
    "" == t ? alert(COMMENT_EMPTY) : "" == e ? alert(TXT_ABOVE_EMPTY) : document.forms.sendmailform.submit()
}
function checkemail(e) {
    var t = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9-]{2,}[.][a-zA-Z]{2,3}$/;
    return null == t.exec(e) ? !1 : !0
}
function loadSearchPage(e, t) {
    ORSearchMain.loaded ? void 0 != e && void 0 != t && launchSearch(e, t) : (_gaq.push(["_trackPageview", "/search/search.php"]),
    new Ajax.Updater("searchdiv","search/search.php",{
        method: "get",
        parameters: {
            u: ORMain.getUnit()
        },
        onFailure: function() {
            return !1
        },
        onComplete: function() {
            ORSearchMain.loadSearchPage(),
            launchSearch(e, t),
            $("pub-search").innerHTML = $("pub-google").innerHTML,
            $("pub-search").show()
        }
    }),
    ORSearchMain.loaded = !0)
}
function launchSearch(e, t) {
    switch (document.forms.orse.reset(),
    t) {
    case 0:
        void 0 != e && (isNaN(e) ? (document.forms.orse.kw.value = e,
        document.forms.orse.es.checked = !1) : document.forms.orse.idr.value = e,
        ORSearchMain.viewSimpleSearch(),
        ORSearchMain.searchSimpleRoutes());
        break;
    case 1:
        ORSearchMain.viewSimpleSearch();
        var n = ORRouteManager.getActiveRoute().getDetails();
        "" == e ? document.forms.orse.us.value = n.getLibUser() : document.forms.orse.us.value = e,
        ORSearchMain.searchSimpleRoutes();
        break;
    case 2:
        ORSearchMain.viewAdvancedSearch();
        var a = ORRouteManager.getActiveRoute()
          , n = a.getDetails()
          , o = ORRouteUtils.getFirstPoint(a)
          , r = o.split(";");
        for (ORSearchMain.mapstart.setCenter(new google.maps.LatLng(r[0],r[1])),
        ORSearchMain.mapstart.setZoom(10),
        i = 0,
        j = document.forms.orsead.sp.options.length; i < j; ++i)
            document.forms.orsead.sp.options[i].value == n.getSport() && (document.forms.orsead.sp.options[i].selected = !0);
        ORSearchMain.searchAdvancedRoutes();
        break;
    case 3:
        ORSearchMain.viewSimpleSearch(),
        document.forms.orse.us.value = e,
        ORSearchMain.searchSimpleRoutes();
        break;
    default:
        ORSearchMain.viewSimpleSearch()
    }
}
function initialize() {
    $("colForm").hide(),
    $("colUpForm").hide(),
    GBrowserIsCompatible() && (mappoi = new GMap2(document.getElementById("mappoi_canvas")),
    mappoi.setCenter(new GLatLng(45.87267,6.46403), 15),
    mappoi.addControl(new GLargeMapControl),
    mappoi.addMapType(G_SATELLITE_MAP),
    mappoi.addMapType(G_PHYSICAL_MAP),
    mappoi.setMapType(G_PHYSICAL_MAP),
    mappoi.addControl(new GMapTypeControl),
    geocoderpoi = new GClientGeocoder,
    GEvent.addListener(mappoi, "zoomend", function() {
        var e = mappoi.getCenter();
        moverelated || (moverelated = !moverelated,
        setIgnCenter(e.lat(), e.lng(), mappoi.getZoom() - 1),
        moverelated = !moverelated)
    }),
    GEvent.addListener(mappoi, "moveend", function() {
        var e = mappoi.getCenter();
        moverelated || (moverelated = !moverelated,
        setIgnCenter(e.lat(), e.lng(), mappoi.getZoom() - 1),
        moverelated = !moverelated)
    }))
}
function showAddress(address) {
    mappoi.clearOverlays(),
    $("colForm").hide(),
    $("colUpForm").hide(),
    $("result").innerHTML = "",
    geocoderpoi && geocoderpoi.getLocations(address, function(location) {
        var place = eval(location);
        if (location && 200 == place.Status.code) {
            var placemark = place.Placemark[0]
              , point = new GLatLng(placemark.Point.coordinates[1],placemark.Point.coordinates[0]);
            mappoi.setCenter(point, 15);
            var marker = new GMarker(point,{
                draggable: !0
            });
            GEvent.addListener(marker, "dragend", function(e) {
                displayCoord(e.lat(), e.lng()),
                removeIgnMarker(),
                addIgnMarker(e.lat(), e.lng()),
                moverelated || (moverelated = !moverelated,
                setIgnCenter(e.lat(), e.lng(), mappoi.getZoom() - 1),
                moverelated = !moverelated)
            }),
            fillColForm(placemark);
            var add = checkExistingPOI(point.lat(), point.lng(), $F("nom"));
            add && ($("colForm").show(),
            mappoi.addOverlay(marker),
            displayCoord(point.lat(), point.lng()),
            addIgnMarker(point.lat(), point.lng()),
            moverelated || (moverelated = !moverelated,
            setIgnCenter(point.lat(), point.lng(), mappoi.getZoom() - 1),
            moverelated = !moverelated))
        } else
            ORMain.showAlertOR(address + " not found")
    })
}
function checkExistingPOI(lat, lng, nom) {
    var url = "checkPoiCol.php?lat=" + lat + "&lng=" + lng + "&nom=" + escape(nom)
      , out = !1;
    return new Ajax.Request(url,{
        method: "get",
        asynchronous: !1,
        onSuccess: function(transport, json) {
            var r = eval(json);
            if ("0" != r.idpoi) {
                var point = new GLatLng(r.lat,r.lng)
                  , marker = new GMarker(point,{
                    draggable: !1
                });
                GEvent.addListener(marker, "click", function(e) {
                    marker.openInfoWindowHtml("<b> " + r.nom + " </b><br> ALTITUDE : " + r.alt + "m.")
                }),
                mappoi.addOverlay(marker),
                addIgnMarker(r.lat, r.lng),
                $("colUpForm").show(),
                $("idpoi").value = r.idpoi,
                $("result").innerHTML = "Existe dans la base."
            } else
                out = !0
        }
    }),
    out
}
function displayCoord(e, t) {
    $("lat").value = e.toFixed(5),
    $("lng").value = t.toFixed(5);
    var n = "../elevation/callORServiceElevation.php?lat=" + e.toFixed(5) + "&lng=" + t.toFixed(5);
    new Ajax.Request(n,{
        method: "get",
        onSuccess: function(e) {
            $("alt").value = e.responseText,
            $("altref").value = e.responseText
        }
    })
}
function fillColForm(e) {
    var t = e.AddressDetails.Country;
    if (void 0 != t) {
        $("cou").value = t.CountryNameCode;
        var n = t.AdministrativeArea;
        if (void 0 != n && ($("are").value = n.AdministrativeAreaName,
        void 0 != n.SubAdministrativeArea)) {
            $("dep").value = n.SubAdministrativeArea.SubAdministrativeAreaName;
            var a = n.SubAdministrativeArea.Locality;
            if (void 0 != a) {
                $("loc").value = a.LocalityName;
                var o = a.PostalCode;
                void 0 != o && ($("zip").value = a.PostalCode.PostalCodeNumber);
                var i = a.Premise;
                void 0 != i ? $("nom").value = i.PremiseName : $("nom").value = a.LocalityName
            }
        }
    } else
        $("nom").value = e.address
}
function subColForm() {
    var e = "managePOI.php?"
      , t = $F("nom")
      , n = $F("loc")
      , a = $F("zip")
      , o = $F("dep")
      , i = $F("are")
      , r = $F("cou")
      , s = $F("lat")
      , l = $F("lng")
      , c = $F("alt")
      , u = $F("altref");
    return e += "nom=" + t,
    e += "&loc=" + n,
    e += "&zip=" + a,
    e += "&dep=" + o,
    e += "&are=" + i,
    e += "&cou=" + r,
    e += "&lat=" + s,
    e += "&lng=" + l,
    e += "&alt=" + c,
    e += "&altref=" + u,
    e += "&typ=0",
    new Ajax.Request(e,{
        method: "get",
        onSuccess: function(e) {
            $("result").innerHTML = "insertion ok"
        }
    }),
    !1
}
function subColUpForm() {
    var e = "managePOI.php?"
      , t = $F("idpoi")
      , n = $F("descmodif");
    return e += "idpoi=" + t,
    e += "&desc=" + n,
    e += "&typ=1",
    new Ajax.Request(e,{
        method: "get",
        onSuccess: function(e) {
            $("result").innerHTML = "màj ok"
        }
    }),
    !1
}
function setGMapCenter(e, t, n) {
    null != mappoi && mappoi.setCenter(new GLatLng(e,t), n)
}
function initGeoportalMap() {
    geoportalLoadmapign("GeoportalMapDiv", "normal", "FXX"),
    mapign.allowedGeoportalLayers && (mapign.setCenterAtLonLat(6.46403, 45.87267, 14),
    initLayerOpacity());
    mapign.allowedDisplayProjections;
    mapign.getMap().events.register("moveend", mapign.getMap(), function(e) {
        var t = (new OpenLayers.Projection("IGNF:LAMB93"),
        mapign.getMap().getCenter())
          , n = new OpenLayers.LonLat(t.lon,t.lat).transform(mapign.getProjection(), OpenLayers.Projection.CRS84)
          , a = mapign.getMap().getZoom();
        moverelated || (moverelated = !moverelated,
        setGMapCenter(n.lat, n.lon, a + 1),
        moverelated = !moverelated),
        OpenLayers.Event.stop(e)
    })
}
function initLayerOpacity() {
    if (mapign.allowedGeoportalLayers) {
        for (var e = 0; e < mapign.allowedGeoportalLayers.length; e++)
            "GEOGRAPHICALGRIDSYSTEMS.MAPS" == mapign.allowedGeoportalLayers[e] && (coucheCarteIGN = mapign.allowedGeoportalLayers[e]),
            "ORTHOIMAGERY.ORTHOPHOTOS" == mapign.allowedGeoportalLayers[e] && (couchePhoto = mapign.allowedGeoportalLayers[e]);
        var t = null;
        t = {
            opacity: 1
        },
        mapign.addGeoportalLayer(coucheCarteIGN, t),
        t = {
            opacity: .1,
            visibility: !0
        },
        mapign.addGeoportalLayer(couchePhoto, t)
    }
}
function setIgnCenter(e, t, n) {
    mapign.setCenterAtLonLat(t, e, n)
}
function addIgnMarker(e, t) {
    coucheMarqueurs = new OpenLayers.Layer.Markers("MarkerLayer"),
    mapign.getMap().addLayer(coucheMarqueurs);
    var n = new OpenLayers.Size(12,20)
      , a = new OpenLayers.Pixel(-6,-20)
      , o = new OpenLayers.Icon("https://labs.google.com/ridefinder/images/mm_20_green.png",n,a)
      , i = new OpenLayers.LonLat(t,e);
    i.transform(OpenLayers.Projection.CRS84, mapign.getProjection());
    var r = new OpenLayers.Marker(i,o);
    coucheMarqueurs.addMarker(r)
}
function removeIgnMarker() {
    mapign.getMap().removeLayer(coucheMarqueurs)
}
var ORConstants = {
    DEG2RAD: .01745329252,
    EARTH_RAD_KM: 6371.598,
    EARTH_RAD_MILES: 3959.127,
    UNIT_CONVERSION: 1.609344,
    EARTH_SPHERE_COR: 1.09,
    POI_COUNT: 1,
    ICON_PREFIX_ID: "ico",
    POI_PREFIX_ID: "poi",
    ICON_DEFAULT_TYPE: "decorator",
    ICON_ORIENTATION_TYPE: "orientation",
    LIMIT_STONES_1KM: 110,
    TYPE_GMAP: "GMAP",
    TYPE_IGN: "IGN",
    TYPE_POINT_DEFAULT: "A",
    TYPE_POINT_NOROUTING: "M",
    NB_ROUTE_LIMIT: NB_ROUTE_LIMIT,
    NB_FAVOURITE_LIMIT: NB_FAVOURITE_LIMIT,
    UNIT_TEST: !1,
    CMADE_WALKING: "foot",
    CMADE_DRIVING: "car",
    CMADE_CYCLING: "bicycle",
    GMAP_ENGINE_ID: 1,
    CLOUDMADE_ENGINE_ID: 2,
    IGN_CTRLK_DRAW: "draw",
    IGN_CTRLK_DRAG: "drag",
    IGN_CTRLK_REMOVE: "remove",
    IGN_CTRLK_INSERT: "insert",
    IGN_CTRLK_MODIFY: "modify",
    IGN_CTRLK_DRAW_POI: "drawpoi",
    IGN_CTRLK_SELECT_POI: "selectpoi",
    TYPE_ROUTE_OR: "OR",
    TYPE_ROUTE_GPS: "GPS",
    STROKE_WIDTH_DEFAULT: STROKE_WIDTH_DEFAULT,
    STROKE_OPACITY_DEFAULT: STROKE_OPACITY_DEFAULT,
    STROKE_COLOR_DEFAULT: STROKE_COLOR_DEFAULT,
    STEP_STONES_DEFAULT: 2,
    WIN_WIDTH_MIN: 1024,
    WIN_HEIGHT_FIX: 600,
    WIN_HEIGHT_MIN: 550,
    WINFS_IGN_DIM1_MAX: 1750,
    WINFS_IGN_DIM2_MAX: 1230,
    MODAL_SMALL_WIDTH: 400,
    MODAL_MEDIUM_WIDTH: 718,
    MODAL_LARGE_WIDTH: 980,
    MODAL_XSMALL_HEIGHT: 300,
    MODAL_SMALL_HEIGHT: 500,
    MODAL_MEDIUM_HEIGHT: 560,
    MODAL_LARGE_HEIGHT: 660,
    MODAL_REF_XSMALL_HEIGHT: 300,
    MODAL_REF_SMALL_HEIGHT: 500,
    MODAL_REF_MEDIUM_HEIGHT: 620,
    MODAL_REF_LARGE_HEIGHT: 660,
    MODAL_BOTTOM_MARGIN: 20,
    MODAL_TITLE: "www.openrunner.com",
    CLASS_NORATE: "norate",
    CLASS_ONESTAR: "onestar",
    CLASS_TWOSTAR: "twostar",
    CLASS_THREESTAR: "threestar",
    CLASS_FOURSTAR: "fourstar",
    CLASS_FIVESTAR: "fivestar",
    TOPO_FRANCE: TOPO_FRANCE,
    TOPO_FRANCE_CLS: "Scan Express Classique",
    TOPO_FRANCE_STD: "Scan Express Standard",
    TOPO_SUISSE: TOPO_SUISSE,
    TOPO_NORVEGE: TOPO_NORVEGE,
    TOPO_ESPAGNE: TOPO_ESPAGNE,
    TOPO_USA: TOPO_USA,
    TOPO_CANADA: TOPO_CANADA,
    GMAPS_PLAN: GMAPS_PLAN,
    GMAPS_SATELLITE: GMAPS_SATELLITE,
    MAP_OSM_OCM: MAP_OSM_OCM,
    MAP_OSM: MAP_OSM,
    MAP_MAPQUEST: MAP_MAPQUEST,
    MAP_HBM: MAP_HBM,
    OL_SRTM3: OL_SRTM3,
    OL_PHOTOS: OL_PHOTOS,
    OL_TERRAIN: OL_TERRAIN,
    OL_LABELS: OL_LABELS,
    OL_BICYCLE: OL_BICYCLE,
    OL_ZPS: "N2000 Zone protection spéciale",
    OL_SIC: "N2000 Directive Habitats",
    OL_PNR: "PNR",
    OL_RNR: "RNR",
    OL_SA: "Sentiers Autorisés",
    OL_CIS: "CIS",
    OL_ADM: "COMMUNES",
    OL_PN: "Parc National",
    OL_RI: "Réserve Biologique",
    OL_RN: "Réserve Nationale",
    OL_PFP: "Parcelles Forestières Publiques",
    OL_PB: "Arrêtés de protection de biotope",
    OL_GP: "Gendarmerie/Police",
    OL_LPN: "Liste des passages à niveau",
    OL_BR: "Bornage Routier",
    OL_CAD: "Cadastre",
    CHECKED: 'checked="checked"',
    POI_MODE_POPUP: 0,
    POI_MODE_LABEL_RIGHT: 1,
    POI_MODE_LABEL_LEFT: 2,
    POI_MODE_IMG: 3,
    OFFSET_GMAPS: 268435456,
    RADIUS_GMAPS: 85445659.4471,
    POI_OFFSET_VERT: 34,
    POI_OFFSET_HORI: 28
}
  , ORExtMap = {
    LAYER_MAP: "MAP",
    LAYER_PHOTO: "PHOTO"
};
LongPress.prototype.onMouseUp_ = function(e) {
    clearTimeout(this.timeoutId_)
}
,
LongPress.prototype.onMouseDown_ = function(e) {
    clearTimeout(this.timeoutId_);
    var t = this.elem_
      , n = e;
    jQuery.browser.mobile && (this.timeoutId_ = setTimeout(function() {
        google.maps.event.trigger(t, "longpress", n)
    }, this.length_))
}
,
LongPress.prototype.onMapDrag_ = function(e) {
    clearTimeout(this.timeoutId_)
}
;
var Tab = Class.create({
    initialize: function(e, t, n, a, o) {
        this.container = e,
        this.bind = e.hash.substr(1, e.hash.length),
        this.hash = e.hash,
        this.loaded = !1,
        this.reload = !1,
        this.fire = a || "or:tab",
        this.container.observe("click", function(e) {
            t && !this.loaded ? (this.loaded = !0,
            o && o()) : n && o && o(),
            Event.stop(e),
            a && document.fire(a, {
                bind: this.hash.substr(1, this.hash.length)
            })
        })
    },
    activate: function() {
        this.container.addClassName("active"),
        $(this.bind).show()
    },
    deactivate: function() {
        this.container.removeClassName("active"),
        $(this.bind).hide()
    },
    fireTabEvent: function() {
        this.fire && document.fire(this.fire, {
            bind: this.hash.substr(1, this.hash.length)
        })
    }
})
  , TabManager = Class.create({
    initialize: function() {
        this.tabs = [],
        this.activeTab,
        this.setActiveTab()
    },
    addTab: function(e) {
        this.tabs.push(e)
    },
    setActiveTab: function(e) {
        for (i = 0,
        j = this.tabs.length; i < j; ++i)
            this.tabs[i].bind == e ? (this.tabs[i].activate(),
            this.activeTab = this.tabs[i]) : this.tabs[i].deactivate()
    },
    initTab: function(e) {
        this.setActiveTab(e)
    },
    reActivateTab: function() {
        this.activeTab && this.activeTab.fireTabEvent()
    },
    goToTab: function(e, t) {
        document.fire(e, {
            bind: t
        })
    }
})
  , ORElevationMain = function() {
    function e() {
        var e = []
          , t = ORRouteManager.getActiveRoute();
        if (null != t.getTPointDir() && t.getTPointDir().length > 0)
            e = ORUtils.calculateFixedPosition(t.getTPointDir(), t.getDistance(), t.getInterval(), t.getUnit());
        else {
            var n = t.getData();
            n.getType() == ORConstants.TYPE_GMAP && (e = ORUtils.calculateFixedPosition(n.getTPoint(), t.getDistance(), t.getInterval(), t.getUnit()))
        }
        return createEncodings(e)
    }
    function t() {
        $("elevation").show(),
        $("loadingel").show();
        var e = ORRouteManager.getActiveRoute()
          , t = 0;
        (ORGMapDecoratorManager.isShowColsR() || ORGMapDecoratorManager.isShowColsM()) && (t = 1);
        var n = new Date
          , a = "elevation/findORElevation4.php";
        $j("#profilecontent").load(a + "?id=" + e.getId() + "&c=" + t + "&u=" + ORMain.getUnit() + "&det=1&t=" + n.getTime()),
        ORElevationMain.imLoaded()
    }
    function n(e) {
        a = e,
        o = a[0][4]
    }
    var a = []
      , o = .1;
    return {
        shEl: function(n) {
            var a = ORRouteManager.getActiveRoute();
            if (n || a.getDistanceRef() != a.getDistance())
                t();
            else {
                a.setInterval(INTERVAL_MIN),
                a.getDistance() > LIMIT_INTERVAL && a.setInterval(INTERVAL_MAX);
                var o = e();
                new Ajax.Request("elevation/prOREle.php",{
                    method: "post",
                    asynchronous: !0,
                    parameters: {
                        fixpoints: escape(o),
                        id: a.getId(),
                        interval: a.getInterval()
                    },
                    onSuccess: function(e) {
                        "ko" == e.responseText || (t(),
                        a.getDetails().setProfProv(!0))
                    }
                })
            }
        },
        cKmPos: function() {
            return e()
        },
        imLoaded: function() {
            var e = ORRouteManager.getActiveRoute();
            new Ajax.Request("elevation/getEVDForAnimation.php",{
                method: "post",
                asynchronous: !1,
                parameters: {
                    id: e.getId()
                },
                onSuccess: function(e) {
                    if ("ko" == e.responseText)
                        ;
                    else {
                        n(e.responseText.evalJSON(!0)),
                        $("eleinfo").innerHTML = a[0][0],
                        $("loadingel").hide();
                        var t = 0
                          , o = 0
                          , i = null;
                        i = ORUtils.findPoiGeographicPosition(t, ORMain.getUnit(), !0);
                        var r = "<div class='title'>" + ELE_CURSOR_LOCATION + "</div><span class='lib'>" + ALTITUDE + ":</span><span id='eleinfo_alt' class='val'>" + a[o][0] + "</span><span class='unitsize'>m</span>";
                        r += "<span class='lib' title='" + ELE_POSITIVE_CHANGE_FROMSTART + "'>D+:</span><span id='eleinfo_dp' class='val'> " + a[o][2] + "</span><span class='unitsize'>m</span>",
                        r += "<span class='lib' title='" + ELE_NEGATIVE_CHANGE_FROMSTART + "'>D- :</span><span id='eleinfo_dn' class='val'>" + a[o][3] + "</span><span class='unitsize'>m</span>",
                        r += "<span class='lib'>" + DISTANCE_DEPARTURE + ":</span><span id='eleinfo_dis' class='val distance'>" + t.toFixed(3) + "</span><span class='unitlabel unitsize'>" + ORMain.getUnit() + "</span>",
                        r += "<br/><div class='title'>&nbsp;</div><span class='lib'>" + LONGITUDE + ":</span><span id='eleinfo_lng' class='val'>" + i.pos.lng().toFixed(5) + "</span><span>°</span><span class='lib'>" + LATITUDE + ":</span><span id='eleinfo_lat' class='val'>" + i.pos.lat().toFixed(5) + "</span><span>°</span>",
                        $("eleinfo").innerHTML = r
                    }
                }
            })
        },
        animElevation: function(e) {
            cen = ORUtils.findPoiGeographicPosition(e, ORMain.getUnit(), !0),
            null != ORMain.mkp ? ORMain.mkp.setPosition(new google.maps.LatLng(cen.pos.lat(),cen.pos.lng())) : ORMain.mkp = new google.maps.Marker({
                position: new google.maps.LatLng(cen.pos.lat(),cen.pos.lng()),
                map: ORMain.map,
                icon: ORMain.kmIcon
            }),
            ORGMapDecoratorManager.drawProfilePoly(cen.idx, cen.pos);
            var t = Math.ceil(e / o);
            a[t] && ($j("#eleinfo_alt").html(a[t][0]),
            $j("#eleinfo_dp").html(a[t][2]),
            $j("#eleinfo_dn").html(a[t][3]),
            $j("#eleinfo_dis").html(e.toFixed(3)),
            $j("#eleinfo_lat").html(cen.pos.lat().toFixed(5)),
            $j("#eleinfo_lng").html(cen.pos.lng().toFixed(5)))
        },
        centerMap: function(e, t) {
            e && ("1" == e.detail && (cen = ORUtils.findPoiGeographicPosition(t, ORMain.getUnit()),
            ORMain.map.setCenter(cen)),
            "2" == e.detail && ORMain.map.mapTypes[ORMain.map.getMapTypeId()].maxZoom > ORMain.map.getZoom() && ORMain.map.setZoom(ORMain.map.getZoom() + 1))
        }
    }
}()
  , totfiltre = 0
  , angle_limit = .025
  , ORParMain = function() {
    return {
        go2: function(e, t, n, a) {
            var o = "logger/logPubPartners.php";
            new Ajax.Request(o,{
                method: "post",
                parameters: {
                    idr: e,
                    ref: t,
                    cdp: n
                }
            })
        }
    }
}()
  , ORAdsMain = function() {
    return {
        printBanner: function(e, t, n, a) {
            ("" == $(e).innerHTML || a) && new Ajax.Updater(e,"adsor/getadsor.php",{
                method: "post",
                asynchronous: !1,
                parameters: {
                    ads_size: t,
                    ads_code: n
                }
            })
        }
    }
}()
  , ORTimtooRecords = function() {
    return {
        getByTID: function(e) {
            new Ajax.Request("timtoo/listRecords.php",{
                method: "post",
                asynchronous: !1,
                parameters: {
                    tid: e
                },
                onSuccess: function(e) {
                    e.responseText;
                    "ko" != e.responseText && ($("timtoo_content").innerHTML = e.responseText,
                    ORTimtooRecords.initTable())
                }
            })
        },
        initTable: function() {
            void 0 != $("timtoo_result") && (sortable = SortableTable.init("timtoo_result", {
                tableScroll: SortableTable.options.tableScroll,
                smallTable: !1
            })),
            Effect.ScrollTo("timtoo_content", {
                duration: "0"
            })
        }
    }
}()
  , ORStatsMain = function() {
    return {
        collect: function(e, t) {
            var n = "logger/logStats.php";
            new Ajax.Request(n,{
                method: "post",
                parameters: {
                    ref: e,
                    desc: t
                }
            })
        }
    }
}();
jQuery.b64 = function(e) {
    function t(e, t) {
        var n = r.indexOf(e.charAt(t));
        if (-1 === n)
            throw "Cannot decode base64";
        return n
    }
    function n(e) {
        var n, a, o = 0, r = e.length, s = [];
        if (e = String(e),
        0 === r)
            return e;
        if (r % 4 !== 0)
            throw "Cannot decode base64";
        for (e.charAt(r - 1) === i && (o = 1,
        e.charAt(r - 2) === i && (o = 2),
        r -= 4),
        n = 0; r > n; n += 4)
            a = t(e, n) << 18 | t(e, n + 1) << 12 | t(e, n + 2) << 6 | t(e, n + 3),
            s.push(String.fromCharCode(a >> 16, a >> 8 & 255, 255 & a));
        switch (o) {
        case 1:
            a = t(e, n) << 18 | t(e, n + 1) << 12 | t(e, n + 2) << 6,
            s.push(String.fromCharCode(a >> 16, a >> 8 & 255));
            break;
        case 2:
            a = t(e, n) << 18 | t(e, n + 1) << 12,
            s.push(String.fromCharCode(a >> 16))
        }
        return s.join("")
    }
    function a(e, t) {
        var n = e.charCodeAt(t);
        if (n > 255)
            throw "INVALID_CHARACTER_ERR: DOM Exception 5";
        return n
    }
    function o(e) {
        if (1 !== arguments.length)
            throw "SyntaxError: exactly one argument required";
        e = String(e);
        var t, n, o = [], s = e.length - e.length % 3;
        if (0 === e.length)
            return e;
        for (t = 0; s > t; t += 3)
            n = a(e, t) << 16 | a(e, t + 1) << 8 | a(e, t + 2),
            o.push(r.charAt(n >> 18)),
            o.push(r.charAt(n >> 12 & 63)),
            o.push(r.charAt(n >> 6 & 63)),
            o.push(r.charAt(63 & n));
        switch (e.length - s) {
        case 1:
            n = a(e, t) << 16,
            o.push(r.charAt(n >> 18) + r.charAt(n >> 12 & 63) + i + i);
            break;
        case 2:
            n = a(e, t) << 16 | a(e, t + 1) << 8,
            o.push(r.charAt(n >> 18) + r.charAt(n >> 12 & 63) + r.charAt(n >> 6 & 63) + i)
        }
        return o.join("")
    }
    var i = "="
      , r = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
      , s = "1.0";
    return {
        decode: n,
        encode: o,
        VERSION: s
    }
}(jQuery);
var ORRoutingEnginesMap = function() {
    var e = new Hash
      , t = new Hash;
    return t.set("LABEL", "GOOGLEMAPS"),
    t.set("MODE", [ROUTE_CALCULATION_MODE_CAR, ROUTE_CALCULATION_MODE_WALKING, ROUTE_CALCULATION_MODE_CYCLING]),
    e.set(ORConstants.GMAP_ENGINE_ID, t),
    {
        getLabel: function(t) {
            return e.get(t).get("LABEL")
        },
        getModes: function(t) {
            return e.get(t).get("MODE")
        }
    }
}()
  , ORRoute = Class.create({
    initialize: function(e, t) {
        this.id = e || -1,
        this.unit = ORMain.getUnit(),
        this.strokeColor = ORConstants.STROKE_COLOR_DEFAULT,
        this.strokeOpacity = ORConstants.STROKE_OPACITY_DEFAULT,
        this.strokeWidth = ORConstants.STROKE_WIDTH_DEFAULT,
        this.stepstones = ORConstants.STEP_STONES_DEFAULT,
        this.details = null,
        this.routingDetails = null,
        this.routing = !1,
        this.routingEngine = 0,
        this.routingMode = 0,
        this.updateable = !1,
        this.distance = 0,
        this.zerokm = 0,
        this.distanceRef = 0,
        this.interval = INTERVAL_MIN,
        this.poiUrl = null,
        this.tPointDir = [],
        this.tPointDirLevel = null,
        this.data = t || null,
        this.typeroute = ORConstants.TYPE_ROUTE_OR,
        this.datelmod = ""
    },
    getId: function() {
        return this.id
    },
    setId: function(e) {
        this.id = e
    },
    getUnit: function() {
        return this.unit
    },
    setUnit: function(e) {
        this.unit = e
    },
    getStrokeColor: function() {
        return this.strokeColor
    },
    setStrokeColor: function(e) {
        this.strokeColor = e
    },
    getStrokeOpacity: function() {
        return this.strokeOpacity
    },
    setStrokeOpacity: function(e) {
        this.strokeOpacity = e
    },
    getStrokeWidth: function() {
        return this.strokeWidth
    },
    setStrokeWidth: function(e) {
        this.strokeWidth = e
    },
    getDetails: function() {
        return this.details
    },
    setDetails: function(e) {
        this.details = e
    },
    setRoutingDetails: function(e) {
        this.routingDetails = e
    },
    getRoutingDetails: function() {
        return this.routingDetails
    },
    addRoutingDetails: function(e) {
        this.routingDetails.push(e)
    },
    setRoutingEngine: function(e) {
        this.routingEngine = e
    },
    getRoutingEngine: function() {
        return this.routingEngine
    },
    setRoutingMode: function(e) {
        this.routingMode = e
    },
    getRoutingMode: function() {
        return this.routingMode
    },
    isRouting: function() {
        return this.routing
    },
    setRouting: function(e) {
        this.routing = e
    },
    isUpdateable: function() {
        return this.updateable
    },
    setUpdateable: function(e) {
        this.updateable = e
    },
    getDistance: function() {
        return this.distance
    },
    setDistance: function(e) {
        this.distance = e
    },
    getDistanceRef: function() {
        return this.distanceRef
    },
    setDistanceRef: function(e) {
        this.distanceRef = e
    },
    getPoiUrl: function() {
        return this.poiUrl
    },
    setPoiUrl: function(e) {
        this.poiUrl = e
    },
    getTPointDir: function() {
        return this.tPointDir
    },
    setTPointDir: function(e) {
        this.tPointDir = e
    },
    addTPointDir: function(e) {
        this.tPointDir = this.tPointDir.concat(e)
    },
    addPointDir: function(e) {
        this.tPointDir.push(e)
    },
    getTPointDirLevel: function() {
        return this.tPointDirLevel
    },
    setTPointDirLevel: function(e) {
        this.tPointDirLevel = e
    },
    addPointDirLevel: function(e) {
        this.tPointDirLevel.push(e)
    },
    getData: function() {
        return this.data
    },
    setData: function(e) {
        this.data = e
    },
    getTypeRoute: function() {
        return this.typeroute
    },
    setTypeRoute: function(e) {
        this.typeroute = e || ORConstants.TYPE_ROUTE_OR
    },
    getInterval: function() {
        return this.interval
    },
    setInterval: function(e) {
        this.interval = e
    },
    getStepStones: function() {
        return this.stepstones
    },
    setStepStones: function(e) {
        this.stepstones = e
    },
    getDateLastMod: function() {
        return this.datelmod
    },
    setDateLastMod: function(e) {
        this.datelmod = e
    },
    destroy: function() {
        this.strokeColor = null,
        this.strokeOpacity = null,
        this.strokeWidth = null,
        this.details = null,
        this.routingDetails = null,
        this.routing = null,
        this.routingEngine = 0,
        this.routingMode = 0,
        this.updateable = null,
        this.distance = null,
        this.distanceRef = null,
        this.poiUrl = null,
        this.tPointDir = null,
        this.tPointDirLevel = null,
        this.data.destroy()
    },
    init: function() {
        this.details = null,
        this.routingDetails = null,
        this.routing = null,
        this.distance = 0,
        this.tPointDir = [],
        this.tPointDirLevel = null,
        this.data.init()
    }
})
  , ORRouteDetails = Class.create({
    initialize: function() {
        this.name = null,
        this.idUser = -1,
        this.libUser = null,
        this.publicUser = !1,
        this.cityStart = null,
        this.deptStart = null,
        this.areaStart = null,
        this.countryStart = null,
        this.cityEnd = null,
        this.deptEnd = null,
        this.areaEnd = null,
        this.countryEnd = null,
        this.difficulty = null,
        this.kindOfRoute = null,
        this.scenic = null,
        this.kindOfRoad = null,
        this.sport = null,
        this.comment = null,
        this.keywords = null,
        this.denplus = 0,
        this.denmoins = 0,
        this.elemax = 0,
        this.elemin = 0,
        this.libsport = null,
        this.libkor = null,
        this.libdiff = null,
        this.liboff = null,
        this.profprov = !1,
        this.priv = !1,
        this.libtripused = null,
        this.libtripwaymk = null,
        this.rating = 0,
        this.numRating = 0,
        this.usercomments = [],
        this.domrep = null,
        this.domrepheader = null,
        this.partner = null,
        this.routePartner = !1
    },
    getName: function() {
        return this.name
    },
    getIdUser: function() {
        return this.idUser
    },
    getLibUser: function() {
        return this.libUser
    },
    isPublicUser: function() {
        return this.publicUser
    },
    getCityStart: function() {
        return this.cityStart
    },
    getDeptStart: function() {
        return this.deptStart
    },
    getAreaStart: function() {
        return this.areaStart
    },
    getCountryStart: function() {
        return this.countryStart
    },
    getCityEnd: function() {
        return this.cityEnd
    },
    getDeptEnd: function() {
        return this.deptEnd
    },
    getAreaEnd: function() {
        return this.areaEnd
    },
    getCountryEnd: function() {
        return this.countryEnd
    },
    getDifficulty: function() {
        return this.difficulty
    },
    getKindOfRoute: function() {
        return this.kindOfRoute
    },
    getScenic: function() {
        return this.scenic
    },
    getKindOfRoad: function() {
        return this.kindOfRoad
    },
    getSport: function() {
        return this.sport
    },
    getComment: function() {
        return this.comment
    },
    getKeywords: function() {
        return this.keywords
    },
    getDenPlus: function() {
        return this.denplus
    },
    getDenMoins: function() {
        return this.denmoins
    },
    getEleMax: function() {
        return this.elemax
    },
    getEleMin: function() {
        return this.elemin
    },
    getDomRep: function() {
        return this.domrep
    },
    getLibSport: function() {
        return this.libsport
    },
    getLibKor: function() {
        return this.libkor
    },
    getLibDiff: function() {
        return this.libdiff
    },
    getLibOff: function() {
        return this.liboff
    },
    isProfProv: function() {
        return this.provprof
    },
    isPrivate: function() {
        return this.priv
    },
    getRating: function() {
        return this.rating
    },
    getNumRating: function() {
        return this.numRating
    },
    getUserComments: function() {
        return this.usercomments
    },
    getDomRepHeader: function() {
        return this.domrepheader
    },
    getLibTripUsed: function() {
        return this.libtripused
    },
    getLibTripWayMk: function() {
        return this.libtripwaymk
    },
    isRoutePartner: function() {
        return this.routePartner
    },
    getPartner: function() {
        return this.partner
    },
    setName: function(e) {
        this.name = e
    },
    setIdUser: function(e) {
        this.idUser = e
    },
    setLibUser: function(e) {
        this.libUser = e
    },
    setPublicUser: function(e) {
        this.publicUser = e
    },
    setCityStart: function(e) {
        this.cityStart = e
    },
    setDeptStart: function(e) {
        this.deptStart = e
    },
    setAreaStart: function(e) {
        this.areaStart = e
    },
    setCountryStart: function(e) {
        this.countryStart = e
    },
    setCityEnd: function(e) {
        this.cityEnd = e
    },
    setDeptEnd: function(e) {
        this.deptEnd = e
    },
    setAreaEnd: function(e) {
        this.areaEnd = e
    },
    setCountryEnd: function(e) {
        this.countryEnd = e
    },
    setDifficulty: function(e) {
        this.difficulty = e
    },
    setKindOfRoute: function(e) {
        this.kindOfRoute = e
    },
    setScenic: function(e) {
        this.scenic = e
    },
    setKindOfRoad: function(e) {
        this.kindOfRoad = e
    },
    setSport: function(e) {
        this.sport = e
    },
    setComment: function(e) {
        this.comment = e
    },
    setKeywords: function(e) {
        this.keywords = e
    },
    setDenPlus: function(e) {
        this.denplus = e
    },
    setDenMoins: function(e) {
        this.denmoins = e
    },
    setEleMax: function(e) {
        this.elemax = e
    },
    setEleMin: function(e) {
        this.elemin = e
    },
    setDomRep: function(e) {
        this.domrep = e
    },
    setLibSport: function(e) {
        this.libsport = e
    },
    setLibKor: function(e) {
        this.libkor = e
    },
    setLibDiff: function(e) {
        this.libdiff = e
    },
    setLibOff: function(e) {
        this.liboff = e
    },
    setProfProv: function(e) {
        this.provprof = e
    },
    setPrivate: function(e) {
        this.priv = "1" == e
    },
    setRating: function(e) {
        this.rating = e
    },
    setNumRating: function(e) {
        this.numRating = e
    },
    setUserComments: function(e) {
        this.usercomments = e
    },
    setDomRepHeader: function(e) {
        this.domrepheader = e
    },
    setLibTripUsed: function(e) {
        this.libtripused = e
    },
    setLibTripWayMk: function(e) {
        this.libtripwaymk = e
    },
    setRoutePartner: function(e) {
        this.routePartner = "1" == e
    },
    setPartner: function(e) {
        this.partner = e
    }
})
  , ORRoutingDetails = Class.create({
    initialize: function() {
        this.dirResult = [],
        this.domrep = null,
        this.status = -1,
        this.copyrigths = "",
        this.warnings = "",
        this.statusMessage = ""
    },
    getDirResult: function() {
        return this.dirResult
    },
    getDomRep: function() {
        return this.domrep
    },
    setDirResult: function(e) {
        this.dirResult = e
    },
    addDirResult: function(e) {
        this.dirResult.push(e)
    },
    setDomRep: function(e) {
        this.domrep = e
    },
    setStatus: function(e) {
        this.status = e
    },
    getStatus: function() {
        return this.status
    },
    getCopyrights: function() {
        return this.copyrights
    },
    setCopyrights: function(e) {
        this.copyrights = e
    },
    getWarnings: function() {
        return this.warnings
    },
    setWarnings: function(e) {
        this.warnings = e
    },
    getStatusMessage: function() {
        return this.statusMessage
    },
    setStatusMessage: function(e) {
        this.statusMessage = e
    }
})
  , ORGMapRouteData = Class.create({
    initialize: function() {
        this.type = ORConstants.TYPE_GMAP,
        this.tPoint = [],
        this.hPoi = new Hash,
        this.poiSave = !1,
        this.poly = null,
        this.tMarkPoint = [],
        this.tMarkStone = [],
        this.pointInsert = null,
        this.markInsert = null,
        this.tPointFixed = [],
        this.markProfile = null,
        this.polyProfile = null,
        this.tMarkAcc = [],
        this.markerCluster = null,
        this.pointStartRemove = null,
        this.pointEndRemove = null,
        this.markStartRemove = null,
        this.markEndRemove = null,
        this.polyRemove = null,
        this.tLabel = []
    },
    getType: function() {
        return this.type
    },
    getTPoint: function() {
        return this.tPoint
    },
    addPoint: function(e) {
        this.tPoint.push(e)
    },
    setTPoint: function(e) {
        this.tPoint = e
    },
    addPoi: function(e) {
        this.hPoi.set(e.getPoiId(), e)
    },
    getHPoi: function() {
        return this.hPoi
    },
    getPoiById: function(e) {
        return this.hPoi.get(e)
    },
    setHPoi: function(e) {
        this.hPoi = e
    },
    hasPoi2Save: function() {
        return this.poiSave
    },
    setPoi2Save: function(e) {
        this.poiSave = e
    },
    getPoly: function() {
        return this.poly
    },
    setPoly: function(e) {
        this.poly = e
    },
    getMarkerCluster: function() {
        return this.markerCluster
    },
    setMarkerCluster: function(e) {
        this.markerCluster = e
    },
    getTMarkPoint: function() {
        return this.tMarkPoint
    },
    setTMarkPoint: function(e) {
        this.tMarkPoint = e
    },
    addMarkPoint: function(e) {
        this.tMarkPoint.push(e),
        this.markerCluster.addMarker(e),
        this.markerCluster.repaint()
    },
    insertMarkPoint: function(e, t) {
        this.tMarkPoint.splice(e, 0, t),
        this.markerCluster.clearMarkers(),
        this.markerCluster.addMarkers(this.tMarkPoint),
        this.markerCluster.repaint()
    },
    replaceMarkPoint: function(e, t) {
        this.tMarkPoint.splice(e, 1, t),
        this.markerCluster.clearMarkers(),
        this.markerCluster.addMarkers(this.tMarkPoint),
        this.markerCluster.repaint()
    },
    removeMarkPoint: function(e) {
        this.tMarkPoint.splice(e, 1),
        this.markerCluster.clearMarkers(),
        this.markerCluster.addMarkers(this.tMarkPoint),
        this.markerCluster.repaint()
    },
    removeNMarkPoint: function(e, t) {
        this.tMarkPoint.splice(e, t),
        this.markerCluster.clearMarkers(),
        this.markerCluster.addMarkers(this.tMarkPoint),
        this.markerCluster.repaint()
    },
    removeLastMarkPoint: function() {
        this.tMarkPoint.splice(this.tMarkPoint.length - 1, 1),
        this.markerCluster.clearMarkers(),
        this.markerCluster.addMarkers(this.tMarkPoint),
        this.markerCluster.repaint()
    },
    getTMarkStone: function() {
        return this.tMarkStone
    },
    addMarkStone: function(e) {
        this.tMarkStone.push(e)
    },
    setTMarkStone: function(e) {
        this.tMarkStone = e
    },
    getPointInsert: function() {
        return this.pointInsert
    },
    setPointInsert: function(e) {
        this.pointInsert = e
    },
    getPointStartRemove: function() {
        return this.pointStartRemove
    },
    setPointStartRemove: function(e) {
        this.pointStartRemove = e
    },
    getPointEndRemove: function() {
        return this.pointEndRemove
    },
    setPointEndRemove: function(e) {
        this.pointEndRemove = e
    },
    getMarkInsert: function() {
        return this.markInsert
    },
    setMarkInsert: function(e) {
        this.markInsert = e
    },
    getMarkStartRemove: function() {
        return this.markStartRemove
    },
    setMarkStartRemove: function(e) {
        this.markStartRemove = e
    },
    getMarkEndRemove: function() {
        return this.markEndRemove
    },
    setMarkEndRemove: function(e) {
        this.markEndRemove = e
    },
    getPolyRemove: function() {
        return this.polyRemove
    },
    setPolyRemove: function(e) {
        this.polyRemove = e
    },
    getTPointFixed: function() {
        return this.tPointFixed
    },
    setTPointFixed: function(e) {
        this.tPointFixed = e
    },
    getMarkProfile: function() {
        return this.markProfile
    },
    setMarkProfile: function(e) {
        this.markProfile = e
    },
    setPolyProfile: function(e) {
        this.polyProfile = e
    },
    getPolyProfile: function() {
        return this.polyProfile
    },
    getTMarkAcc: function() {
        return this.tMarkAcc
    },
    addMarkAcc: function(e) {
        this.tMarkAcc.push(e)
    },
    setTMarkAcc: function(e) {
        this.tMarkAcc = tMarkAcc
    },
    addLabel: function(e) {
        this.tLabel.push(e)
    },
    getTLabel: function() {
        return this.tLabel
    },
    destroy: function() {
        this.tPoint = null,
        this.hPoi = null,
        this.poly = null,
        this.tMarkPoint = null,
        this.tMarkStone = null,
        this.pointInsert = null,
        this.markInsert = null,
        this.tPointFixed = null,
        this.markProfile = null,
        this.tMarkAcc = null,
        this.tLabel = null
    },
    init: function() {
        this.tPoint = [],
        this.hPoi = new Hash,
        this.poly = null,
        this.tMarkPoint = [],
        this.tMarkStone = [],
        this.pointInsert = null,
        this.markInsert = null,
        this.tPointFixed = [],
        this.markProfile = null,
        this.tMarkAcc = [],
        this.tLabel = []
    }
});
google.maps.LatLng.prototype.getORType = function() {
    return this.ortype
}
,
google.maps.LatLng.prototype.setORType = function(e) {
    this.ortype = e
}
,
google.maps.LatLng.prototype.changeORType = function() {
    this.ortype == ORConstants.TYPE_POINT_DEFAULT ? this.ortype = ORConstants.TYPE_POINT_NOROUTING : this.ortype = ORConstants.TYPE_POINT_DEFAULT
}
,
google.maps.LatLng.prototype.latRadians = function() {
    return this.lat() * ORConstants.DEG2RAD
}
,
google.maps.LatLng.prototype.lngRadians = function() {
    return this.lng() * ORConstants.DEG2RAD
}
;
var ORLatLng = Class.create({
    initialize: function(e, t, n, a, o) {
        this.latitude = e || 0,
        this.longitude = t || 0,
        this.alt = n || 0,
        this.rot = a || 0,
        this.ortype = o || ORConstants.TYPE_POINT_DEFAULT
    },
    getLat: function() {
        return this.latitude
    },
    getLng: function() {
        return this.longitude
    },
    lat: function() {
        return this.latitude
    },
    lng: function() {
        return this.longitude
    },
    getAlt: function() {
        return this.alt
    },
    getRot: function() {
        return this.rot
    },
    latRadians: function() {
        return this.latitude * ORConstants.DEG2RAD
    },
    lngRadians: function() {
        return this.longitude * ORConstants.DEG2RAD
    },
    getORType: function() {
        return this.ortype
    },
    setORType: function(e) {
        this.ortype = e
    },
    changeORType: function() {
        this.ortype == ORConstants.TYPE_POINT_DEFAULT ? this.ortype = ORConstants.TYPE_POINT_NOROUTING : this.ortype = ORConstants.TYPE_POINT_DEFAULT
    },
    setLat: function(e) {
        this.latitude = e
    },
    setLng: function(e) {
        this.longitude = e
    },
    equals: function(e) {
        return e.getLat() == this.latitude && e.getLng() == this.longitude && e.getORType() == this.ortype
    },
    toString: function() {
        return "(" + this.latitude + ", " + this.longitude + ")"
    },
    toUrlValue: function() {
        return round(this.latitude, 6) + "," + round(this.longitude, 6)
    }
})
  , ORIconCategory = Class.create({
    initialize: function(e, t, n) {
        this.id = e,
        this.title = t,
        this.iconList = []
    },
    getId: function() {
        return this.id
    },
    getTitle: function() {
        return this.title
    },
    getIconList: function() {
        return this.iconList
    },
    addIcon: function(e) {
        this.iconList.push(e)
    }
})
  , ORIcon = Class.create({
    initialize: function(e, t, n, a, o, i, r, s, l) {
        this.id = e || -1,
        this.sizeW = t || 0,
        this.sizeH = n || 0,
        this.anchorX = a || 0,
        this.anchorY = o || 0,
        this.iconUrlA = i || "",
        this.iconUrlI = r || "",
        this.desc = s || "",
        this.descriptable = l || 1
    },
    getId: function() {
        return this.id
    },
    getDomId: function() {
        return ORConstants.ICON_PREFIX_ID + this.id
    },
    getSizeW: function() {
        return this.sizeW
    },
    getSizeH: function() {
        return this.sizeH
    },
    getAnchorX: function() {
        return this.anchorX
    },
    getAnchorY: function() {
        return this.anchorY
    },
    getIconUrlA: function() {
        return this.iconUrlA
    },
    getIconUrlI: function() {
        return this.iconUrlI
    },
    getDesc: function() {
        return this.desc
    },
    setDescriptable: function(e) {
        this.descriptable = e
    },
    getDescriptable: function() {
        return this.descriptable
    }
})
  , ORMarker = Class.create({
    initialize: function(e, t) {
        this.oricon = e || new ORIcon,
        this.orlatlng = t || new ORLatLng,
        this.apimarker
    },
    getApiMarker: function() {
        return this.apimarker
    },
    setApiMarker: function(e) {
        this.apimarker = e
    },
    setORIcon: function(e) {
        this.oricon = e
    },
    getORIcon: function() {
        return this.oricon
    },
    setORLatLng: function(e) {
        this.orlatlng = e
    },
    getORLatLng: function() {
        return this.orlatlng
    }
})
  , ORPoi = Class.create({
    initialize: function(e, t, n, a, o) {
        this.poiid = ORConstants.POI_COUNT++,
        this.ormarker = n,
        this.posGg = t,
        this.posKm = 0,
        this.userDescription = "",
        this.userShortDescription = a,
        this.exportable_ser = "1",
        this.exportable_files = "1",
        this.elevation_display_mode = "-1",
        this.elevation_display_hori = "0",
        this.display_mode = "0",
        this.poi_global = "0",
        this.category = "",
        this.ormarker.setORLatLng(this.posGg),
        this.gridPosition = "0000"
    },
    getPosKm: function() {
        return ORMain.getUnit() == UNIT_MILES ? (this.posKm / ORConstants.UNIT_CONVERSION).toFixed(3) : this.posKm
    },
    setPosKm: function(e) {
        ORMain.getUnit() == UNIT_MILES ? this.posKm = (e * ORConstants.UNIT_CONVERSION).toFixed(3) : this.posKm = e
    },
    setORMarker: function(e) {
        this.ormarker = e
    },
    getORMarker: function() {
        return this.ormarker
    },
    getPoiId: function() {
        return this.poiid
    },
    setGlobal: function(e) {
        this.poi_global = e
    },
    isGlobal: function() {
        return "1" == this.poi_global
    },
    setExportableSer: function(e) {
        this.exportable_ser = e
    },
    isExportableSer: function() {
        return "1" == this.exportable_ser
    },
    setExportableFiles: function(e) {
        this.exportable_files = e
    },
    isExportableFiles: function() {
        return "1" == this.exportable_files
    },
    getElevationDisplayMode: function() {
        return this.elevation_display_mode
    },
    setElevationDisplayMode: function(e) {
        this.elevation_display_mode = e
    },
    isElevationDisplayHori: function() {
        return "1" == this.elevation_display_hori
    },
    setElevationDisplayHori: function(e) {
        this.elevation_display_hori = e
    },
    getDisplayMode: function() {
        return this.display_mode
    },
    setDisplayMode: function(e) {
        this.display_mode = e
    },
    getUserDescription: function() {
        return this.userDescription
    },
    setUserDescription: function(e) {
        this.userDescription = e
    },
    getUserShortDescription: function() {
        return this.userShortDescription
    },
    setUserShortDescription: function(e) {
        this.userShortDescription = e
    },
    getGridPosition: function() {
        return this.gridPosition
    },
    setGridPosition: function(e) {
        this.gridPosition = e
    },
    getPoiAsUrl: function() {
        return {
            poi_id: this.ormarker.getORIcon().getId(),
            poi_lat: this.ormarker.getORLatLng().lat(),
            poi_lng: this.ormarker.getORLatLng().lng(),
            poi_pos: this.posKm,
            poi_img_i: this.ormarker.getORIcon().getIconUrlI(),
            poi_img_a: this.ormarker.getORIcon().getIconUrlA(),
            poi_exp_ser: this.exportable_ser,
            poi_exp_files: this.exportable_files,
            poi_title: this.userShortDescription,
            poi_desc: this.userDescription,
            poi_global: this.poi_global,
            display_mode: this.display_mode,
            elevation_display_mode: this.elevation_display_mode,
            elevation_display_hori: this.elevation_display_hori,
            grid_position: this.gridPosition
        }
    }
})
  , ORRouteManager = function() {
    function e(e, n, o) {
        if (a(),
        M.size() == ORConstants.NB_ROUTE_LIMIT || t(n))
            return !1;
        if (d = new ORRoute(n || --p),
        d.setTypeRoute(o),
        (e || ORConstants.TYPE_GMAP) == ORConstants.TYPE_GMAP && (d.setData(new ORGMapRouteData),
        null != ORMain.map)) {
            var i = new MarkerClusterer(ORMain.map);
            i.setMaxZoom(MC_MAX_ZOOM),
            i.setMinimumClusterSize(MC_MIN_CSIZE),
            i.setZoomOnClick(!1),
            d.getData().setMarkerCluster(i)
        }
        return M.set(d.getId(), d),
        R = d.getId(),
        !0
    }
    function t(e) {
        for (var t = M.values(), n = 0; n < t.length; n++)
            if (t[n].getId() == e && e > 0)
                return !0;
        return !1
    }
    function n(e) {
        M.get(e) && (d = M.get(e),
        R = e)
    }
    function a() {
        null != d && M.set(R, d)
    }
    function o(e) {
        M.unset(R),
        d.setId(e),
        R = e,
        M.set(R, d)
    }
    function i(n) {
        if (t(n))
            throw "ROUTE_ALREADY_LOADED";
        var a = n
          , o = null
          , i = "route-data/loadData.php";
        if (ORConstants.UNIT_TEST && (i = "../route-data/loadData.php"),
        new Ajax.Request(i,{
            method: "post",
            parameters: {
                id: n
            },
            asynchronous: !1,
            evalscript: !0,
            onSuccess: function(t) {
                if ("ko" == t.responseText)
                    o = ROUTE_NOT_VALID_OR_PRIVATE;
                else {
                    var n = t.responseText.evalJSON(!0);
                    if (0 > R && 0 == d.getDistance()) {
                        M.unset(R);
                        d = null
                    }
                    e(ORConstants.TYPE_GMAP, a) ? c(n) : o = MAXNB_OF_ROUTE_EXCEEDED
                }
            }
        }),
        null != o)
            throw o
    }
    function r(e) {
        var t = null;
        if (new Ajax.Request("route-data/loadData.php",{
            method: "post",
            parameters: {
                id: e
            },
            asynchronous: !1,
            onSuccess: function(e) {
                if ("ko" == e.responseText)
                    t = ROUTE_NOT_VALID_OR_PRIVATE,
                    dcsearch = 0;
                else {
                    var n = e.responseText.evalJSON(!0);
                    l(n),
                    n = null
                }
            }
        }),
        null != t)
            throw t
    }
    function s(t) {
        d.getDistance() > 0 && !e(ORConstants.TYPE_GMAP) ? status = "MAXNB_OF_ROUTE_EXCEEDED" : u(t)
    }
    function l(e) {
        var t = new ORRouteDetails;
        t.setName(e.routename),
        t.setIdUser(e.iduser),
        t.setLibUser(e.libuser),
        t.setCityStart(e.city),
        t.setDeptStart(e.dept),
        t.setAreaStart(e.area),
        t.setCountryStart(e.country),
        t.setCityEnd(e.cityend),
        t.setDeptEnd(e.deptend),
        t.setAreaEnd(e.areaend),
        t.setCountryEnd(e.countryend),
        t.setDifficulty(e.diff),
        t.setKindOfRoute(e.offrace),
        t.setScenic(!1),
        t.setKindOfRoad(e.kindroute),
        t.setSport(e.sport),
        t.setComment(e.routecomment),
        t.setKeywords(e.keyword),
        t.setDenPlus(e.denplus),
        t.setDenMoins(e.denmoins),
        t.setEleMax(e.maxaltprof),
        t.setEleMin(e.minaltprof),
        t.setLibSport(e.libsport),
        t.setLibKor(e.libkor),
        t.setLibDiff(e.libdifficulty),
        t.setLibOff(e.liboffrace),
        t.setProfProv(e.profprov),
        t.setPrivate(e.routepriv),
        t.setLibTripUsed(e.libtripused),
        t.setLibTripWayMk(e.libwaymk),
        t.setPublicUser(1 == e.flguserpublic),
        t.setRating(parseInt(e.rating)),
        t.setNumRating(parseInt(e.num_rating)),
        t.setUserComments(e.usercomments),
        t.setRoutePartner(e.flg_routepartner),
        t.setPartner(e.partner),
        d.setDetails(t),
        d.getDetails().setDomRep(e.mc),
        d.getDetails().setDomRepHeader(e.hc)
    }
    function c(e) {
        d.init(),
        d.setTypeRoute(e.typeroute);
        for (var t = decodeLine(e.points), n = (t.length,
        e.typepoints), a = 0; a < t.length; a++) {
            var o = null;
            null == n ? (o = new google.maps.LatLng(parseFloat(t[a][0]),parseFloat(t[a][1])),
            o.setORType(ORConstants.TYPE_POINT_DEFAULT)) : (o = new google.maps.LatLng(parseFloat(t[a][0]),parseFloat(t[a][1])),
            o.setORType(n.charAt(a))),
            d.getData().addPoint(o)
        }
        if (d.setTPointDirLevel(e.levels),
        null != e.roupoints && "" != e.roupoints)
            for (var i = decodeLine(e.roupoints), a = 0; a < i.length; a++) {
                var o = new google.maps.LatLng(parseFloat(i[a][0]),parseFloat(i[a][1]));
                o.setORType(ORConstants.TYPE_POINT_DEFAULT),
                d.addPointDir(o)
            }
        if (d.setRoutingEngine(parseInt(e.routingengine)),
        d.setRoutingMode(parseInt(e.routingmode)),
        d.setUpdateable(e.updateable),
        d.setStrokeColor(e.strokecolor),
        d.setStrokeOpacity(parseFloat(e.strokeopacity)),
        d.setStrokeWidth(parseInt(e.strokewidth, 10)),
        d.setStepStones("-1" != e.stepstones ? parseInt(e.stepstones, 10) : ORConstants.STEP_STONES_DEFAULT),
        d.getUnit() == UNIT_KM ? d.setDistance(e.distance) : d.setDistance((e.distance / ORConstants.UNIT_CONVERSION).toFixed(3)),
        d.setDateLastMod(e.datecreation),
        l(e),
        void 0 != e.poiurl && "" != e.poiurl || void 0 != e.partner && "" != e.partner.url_poiglobal) {
            var r = [];
            void 0 != e.poiurl && "" != e.poiurl && (r = r.concat($j.parseJSON(e.poiurl))),
            void 0 != e.partner && "" != e.partner.url_poiglobal && (r = r.concat($j.parseJSON(e.partner.url_poiglobal))),
            d.setPoiUrl(r);
            for (var a = 0; a < r.length; a++) {
                var s = r[a]
                  , c = s.poi_title
                  , u = s.poi_desc
                  , g = ORIconCatalog.getIconById(s.poi_id)
                  , p = new ORMarker(g)
                  , R = new google.maps.LatLng(parseFloat(s.poi_lat),parseFloat(s.poi_lng))
                  , M = new ORPoi(g.getId(),R,p,c,1);
                M.setUserDescription(u),
                s.poi_pos && M.setPosKm(s.poi_pos),
                s.poi_exp_ser && M.setExportableSer(s.poi_exp_ser),
                s.poi_exp_files && M.setExportableFiles(s.poi_exp_ser),
                s.display_mode && M.setDisplayMode(s.display_mode),
                s.elevation_display_mode && M.setElevationDisplayMode(s.elevation_display_mode),
                s.elevation_display_hori && M.setElevationDisplayHori(s.elevation_display_hori),
                s.poi_global && M.setGlobal(s.poi_global),
                s.grid_position && M.setGridPosition(s.grid_position),
                d.getData().addPoi(M)
            }
        }
    }
    function u(e) {
        if (e.garimport) {
            for (var t = [], n = e.garimport, a = 0, o = n.length; o > a; a++) {
                var i = new ORLatLng(parseFloat(n[a][0]),parseFloat(n[a][1]),0,0,ORConstants.TYPE_POINT_DEFAULT);
                t.push(i)
            }
            var r = createEncodings(t);
            new Ajax.Request("import/redtr.php",{
                method: "post",
                parameters: {
                    st: r
                },
                asynchronous: !1,
                onSuccess: function(e) {
                    if ("ko" != e.responseText) {
                        for (var t = decodeLine(e.responseText), n = 0; n < t.length; n++) {
                            var a = new google.maps.LatLng(parseFloat(t[n][0]),parseFloat(t[n][1]));
                            a.setORType(ORConstants.TYPE_POINT_DEFAULT),
                            d.getData().addPoint(a)
                        }
                        d.setTypeRoute(ORConstants.TYPE_ROUTE_GPS)
                    }
                }
            })
        } else if (null != e.roupoints && "" != e.roupoints) {
            for (var s = decodeLine(e.roupoints), a = 0; a < s.length; a++) {
                var i = new google.maps.LatLng(parseFloat(s[a][0]),parseFloat(s[a][1]));
                i.setORType(ORConstants.TYPE_POINT_DEFAULT),
                d.getData().addPoint(i)
            }
            d.setTypeRoute(ORConstants.TYPE_ROUTE_GPS)
        }
        if (void 0 != e.poiurl && "" != e.poiurl || void 0 != e.partner && "" != e.partner.url_poiglobal) {
            var l = e.poiurl;
            d.setPoiUrl(l);
            for (var a = 0; a < l.length; a++) {
                var c = l[a]
                  , u = c.poi_title
                  , g = c.poi_desc
                  , p = ORIconCatalog.getIconById(c.poi_id)
                  , R = new ORMarker(p)
                  , M = new google.maps.LatLng(parseFloat(c.poi_lat),parseFloat(c.poi_lng))
                  , O = new ORPoi(p.getId(),M,R,u,1);
                O.setUserDescription(g),
                c.poi_pos && O.setPosKm(c.poi_pos),
                c.poi_exp_ser && O.setExportableSer(c.poi_exp_ser),
                c.poi_exp_files && O.setExportableFiles(c.poi_exp_ser),
                c.display_mode && O.setDisplayMode(c.display_mode),
                c.elevation_display_mode && O.setElevationDisplayMode(c.elevation_display_mode),
                c.elevation_display_hori && O.setElevationDisplayHori(c.elevation_display_hori),
                c.poi_global && O.setGlobal(c.poi_global),
                d.getData().addPoi(O)
            }
        }
    }
    function g() {
        if (null == d || !d.getData().hasPoi2Save())
            return !1;
        var e = d.getData().getHPoi().values();
        if (e.length > 0) {
            for (var t = [], n = [], a = 0; a < e.length; a++)
                e[a].isGlobal() ? n.push(e[a].getPoiAsUrl()) : t.push(e[a].getPoiAsUrl());
            new Ajax.Request("orpoi-user/savePoi.php",{
                method: "post",
                parameters: {
                    idr: d.getId(),
                    poiurl: $j.toJSON(t),
                    poiurlg: $j.toJSON(n)
                },
                asynchronous: !1,
                onSuccess: function(e) {
                    document.fire("or:poiuser-nomodif")
                }
            })
        } else
            new Ajax.Request("orpoi-user/savePoi.php",{
                method: "post",
                parameters: {
                    idr: d.getId(),
                    poiurl: ""
                },
                asynchronous: !1,
                onSuccess: function(e) {
                    document.fire("or:poiuser-nomodif")
                }
            })
    }
    var p = 0
      , d = null
      , R = 0
      , M = new Hash;
    return {
        getActiveRoute: function() {
            return null == d && e(),
            d
        },
        changeActiveRouteId: function(e) {
            o(e)
        },
        getNbActiveRoute: function() {
            return M.size()
        },
        getAllActiveRoute: function() {
            return M
        },
        changeActiveRoute: function(e) {
            a(),
            n(e)
        },
        createNewActiveRoute: function(t, n) {
            return e(t, n)
        },
        destroyRoute: function(t) {
            var a = M.unset(R);
            if (a.destroy(),
            M.size() > 0) {
                var o = M.keys();
                return o.sort(function(e, t) {
                    return t - e
                }),
                n(o[o.length - 1]),
                !1
            }
            return d = null,
            e(t || ORConstants.TYPE_GMAP),
            !0
        },
        destroyAllRoutes: function() {
            for (var e = M.keys(), t = 0; t < e.length; t++) {
                var n = M.unset(e[t]);
                n.destroy()
            }
            d = null
        },
        initRoute: function() {
            d.init()
        },
        load: function(e) {
            i(e)
        },
        reLoadDetails: function(e) {
            r(e)
        },
        save: function() {
            if (0 == d.getDistance())
                return ORMain.showAlertOR(ALERT_NO_DATA_TO_SAVE),
                !1;
            if (d.getUnit() == UNIT_KM && d.getDistance() > DISTANCE_MAX_KM || d.getUnit() != UNIT_KM && d.getDistance() > DISTANCE_MAX_MILES)
                return ORMain.showAlertOR(ROUTING_DISTANCE_LIMIT),
                !1;
            var e = ORRouteUtils.getFirstPoint(d)
              , t = ORRouteUtils.getLastPoint(d);
            if (ORConstants.UNIT_TEST)
                return e + "," + t;
            if (ORAccMain.isUserConnected()) {
                if (ORAccMain.noads)
                    return ORMain.showAlertOR(ALERT_ADBLOCK_1 + "<br/><b>" + ALERT_ADBLOCK_2 + "</b>"),
                    !1;
                Modalbox.show("route-data/saveDataInput.php?fp=" + e + "&lp=" + t, {
                    slideDownDuration: "0",
                    title: ORConstants.MODAL_TITLE,
                    width: ORConstants.MODAL_LARGE_WIDTH,
                    height: ORConstants.MODAL_LARGE_HEIGHT,
                    overlayClose: !1
                })
            } else
                Modalbox.show("account/userNotAuthenticated.php", {
                    slideDownDuration: "0",
                    title: ORConstants.MODAL_TITLE,
                    width: ORConstants.MODAL_LARGE_WIDTH,
                    height: ORConstants.MODAL_LARGE_HEIGHT,
                    overlayClose: !1
                });
            return !1
        },
        update: function() {
            if (0 == d.getDistance())
                return ORMain.showAlertOR(ALERT_NO_DATA_TO_SAVE),
                !1;
            if (d.getUnit() == UNIT_KM && d.getDistance() > DISTANCE_MAX_KM || d.getUnit() != UNIT_KM && d.getDistance() > DISTANCE_MAX_MILES)
                return ORMain.showAlertOR(ROUTING_DISTANCE_LIMIT),
                !1;
            var e = ORRouteUtils.getFirstPoint(d)
              , t = ORRouteUtils.getLastPoint(d)
              , n = d.getId();
            return ORConstants.UNIT_TEST ? n + ":" + e + "," + t : ORAccMain.noads ? (ORMain.showAlertOR(ALERT_ADBLOCK_1 + "<br/><b>" + ALERT_ADBLOCK_2 + "</b>"),
            !1) : (Modalbox.show("route-data/updateDataInput.php?fp=" + e + "&lp=" + t + "&idroute=" + n, {
                title: ORConstants.MODAL_TITLE,
                width: ORConstants.MODAL_LARGE_WIDTH,
                height: ORConstants.MODAL_LARGE_HEIGHT,
                overlayClose: !1
            }),
            !1)
        },
        remove: function(e) {
            return -1 == e && (e = d.getId()),
            ORConstants.UNIT_TEST ? e : void Modalbox.show("route-data/removeDataInput.php?idroute=" + e, {
                width: ORConstants.MODAL_MEDIUM_WIDTH,
                height: ORConstants.MODAL_SMALL_HEIGHT,
                title: ORConstants.MODAL_TITLE,
                method: "post",
                overlayClose: !1
            })
        },
        loadGPSData: function(e) {
            s(e)
        },
        changeRouteColor: function(e) {
            d.setStrokeColor(e)
        },
        changeRouteOpacity: function(e) {
            d.setStrokeOpacity(parseFloat(e))
        },
        changeRouteWidth: function(e) {
            d.setStrokeWidth(parseInt(e, 10))
        },
        changeUnit: function(e) {
            for (var t = M.keys(), n = 0; n < t.length; n++) {
                var a = M.get(t[n]);
                a.setUnit(e)
            }
        },
        isAlreadyLoaded: function(e) {
            return t(e)
        },
        findPOIUserInRoutes: function(e) {
            for (var t = M.keys(), n = null, a = 0; a < t.length; a++) {
                var o = M.get(t[a])
                  , i = o.getData();
                if (n = i.getPoiById(e),
                null != n)
                    break
            }
            return n
        },
        saveUserPoi: function() {
            g()
        },
        activatePoiUserChange: function(e) {
            null != d && d.getData().setPoi2Save(e)
        }
    }
}()
  , ORDescRoute = Class.create({
    initialize: function(e, t, n, a, o) {
        this.desc = e,
        this.lat = t,
        this.lng = n,
        this.stepdis = a,
        this.maneuver = o
    },
    getDesc: function() {
        return this.desc
    },
    getLat: function() {
        return this.lat
    },
    getLng: function() {
        return this.lng
    },
    getStepDis: function() {
        return this.stepdis
    },
    getStepManeuver: function() {
        return this.maneuver
    },
    toString: function() {
        return this.desc + " : " + this.stepdis
    }
})
  , ORDirResult = Class.create({
    initialize: function() {
        this.tdesc = [],
        this.tpoly = [],
        this.tpoint = [],
        this.status = 0,
        this.copyrights = "",
        this.warnings = "",
        this.statusMessage = "",
        this.advice = ""
    },
    getTDesc: function() {
        return this.tdesc
    },
    setTDesc: function(e) {
        this.tdesc = e
    },
    addDesc: function(e) {
        this.tdesc.push(e)
    },
    getTPoly: function() {
        return this.tpoly
    },
    setTPoly: function(e) {
        this.tpoly = e
    },
    addPoly: function(e) {
        this.tpoly.push(e)
    },
    getTPoint: function() {
        return this.tpoint
    },
    setTPoint: function(e) {
        this.tpoint = e
    },
    addPoint: function(e) {
        this.tpoint.push(e)
    },
    getStatus: function() {
        return this.status
    },
    setStatus: function(e) {
        this.status = e
    },
    getCopyrights: function() {
        return this.copyrights
    },
    setCopyrights: function(e) {
        this.copyrights = e
    },
    getWarnings: function() {
        return this.warnings
    },
    setWarnings: function(e) {
        this.warnings = e
    },
    getStatusMessage: function() {
        return this.statusMessage
    },
    setStatusMessage: function(e) {
        this.statusMessage = e
    },
    toString: function() {
        return this.tdesc.join(",") + " : " + this.tpoint.join(",")
    }
})
  , ORGMapEngine = function() {
    function e(e, a) {
        0 == s && (l = new ORDirResult,
        c = e),
        u = a,
        r = c.length > 10 ? Math.ceil(c.length / 9) : 1;
        var g = t(c)
          , p = {
            origin: g[0],
            destination: g[g.length - 1],
            waypoints: n(g),
            travelMode: o,
            avoidHighways: !0,
            avoidTolls: !0
        };
        try {
            i.route(p, ORGMapEngine.callbackGMapRouting)
        } catch (d) {}
    }
    function t(e) {
        for (var t = new Array, n = 9 * s, a = 9 * (s + 1), o = e.length; a >= n && o > n; n++)
            t.push(e[n]);
        return s++,
        t
    }
    function n(e) {
        for (var t = new Array, n = 1; n < e.length - 1; n++) {
            var a = {
                location: e[n],
                stopover: !1
            };
            t.push(a)
        }
        return t
    }
    function a(t, n) {
        if (n != google.maps.DirectionsStatus.OK)
            return l.setStatus(-1),
            l.setStatusMessage(n),
            s = 0,
            r = 0,
            void document.fire("or:cbrouting", {
                result: l
            });
        var a = t.routes[0];
        l.setCopyrights(a.copyrights),
        l.setWarnings(a.warnings);
        for (var o = a.legs.length, i = 0; o > i; i++)
            for (var g = a.legs[i], p = 0, d = g.steps.length; d > p; p++) {
                var R = g.steps[p];
                dis = ORUtils.calculateTotalDistance(R.path, ORMain.unit),
                l.addDesc(new ORDescRoute(R.instructions,R.start_location.lat(),R.start_location.lng(),dis,R.maneuver)),
                nb_pts = R.path.length;
                for (var M = 0; M < nb_pts; M++) {
                    var O = R.path[M];
                    O.setORType(ORConstants.TYPE_POINT_NOROUTING),
                    l.addPoint(O)
                }
            }
        r > s ? setTimeout(function() {
            e(c, u)
        }, 700) : (s = 0,
        r = 0,
        document.fire("or:cbrouting", {
            result: l
        }))
    }
    var o = google.maps.TravelMode.DRIVING
      , i = null
      , r = 0
      , s = 0
      , l = null
      , c = null
      , u = LANG
      , g = UNIT_KM;
    return {
        calculateRoute: function(t, n) {
            e(t, n)
        },
        callbackGMapRouting: function(e, t) {
            a(e, t)
        },
        getGDir: function() {
            return i
        },
        setGDir: function(e) {
            i = e
        },
        setDrivingMode: function(e) {
            o = e
        },
        getDrivingMode: function() {
            return o
        },
        setUnit: function(e) {
            g = e
        }
    }
}()
  , ORSimpleEngine = function() {
    function e(e, n) {
        var a = new ORDirResult;
        a.setCopyrights("");
        var o = ORUtils.calculateTotalDistance(e, t).toFixed(3);
        a.addDesc(new ORDescRoute("or-indications",e[0].lat(),e[0].lng(),o,"or-maneuver")),
        a.setTPoint(e),
        document.fire("or:cbrouting", {
            result: a
        })
    }
    var t = UNIT_KM;
    return {
        calculateRoute: function(t, n, a) {
            e(t, n, a)
        },
        setUnit: function(e) {
            t = e
        }
    }
}()
  , ORDirUtils = function() {
    function e() {
        u = !0,
        p.getData().getType() == ORConstants.TYPE_GMAP && $("follow").addClassName("tools-follow-locked")
    }
    function t() {
        if (null != g && g.length > 0) {
            p.setTPointDir([]),
            p.setRoutingDetails(new ORRoutingDetails);
            for (var e = 0; e < g.length; e++)
                if (p.getRoutingDetails().addDirResult(g[e].getTDesc()),
                p.addTPointDir(g[e].getTPoint()),
                p.getRoutingDetails().setStatus(g[e].getStatus()),
                p.getRoutingDetails().setStatusMessage(g[e].getStatusMessage()),
                R || "" == g[e].getCopyrights() || (p.getRoutingDetails().setCopyrights(g[e].getCopyrights()),
                R = !0),
                !M && g[e].getWarnings().length > 0) {
                    for (var t = "", n = 0, a = g[e].getWarnings().length; a > n; n++)
                        t += g[e].getWarnings()[n];
                    p.getRoutingDetails().setWarnings(t),
                    M = !0
                }
        }
        o = [],
        i = [],
        r = 0,
        s = 0,
        l = 0,
        c = LANG,
        g = [],
        R = !1,
        M = !1,
        u = !1,
        p.getData().getType() == ORConstants.TYPE_GMAP && ($("follow").addClassName("tools-follow-a"),
        $("follow").removeClassName("tools-follow-locked")),
        p = null,
        null != d && d.call()
    }
    var n = ORGMapEngine
      , a = ORSimpleEngine
      , o = []
      , i = []
      , r = 0
      , s = 0
      , l = 0
      , c = LANG
      , u = !1
      , g = []
      , p = null
      , d = null
      , R = !1
      , M = !1;
    return {
        calculateRoute: function(t, r, l) {
            if (c = r,
            u)
                return !1;
            if (t.getData().getType() == ORConstants.TYPE_GMAP) {
                if (t.getData().getTPoint().length < LIMIT_ROUTING_PTS_MIN)
                    return;
                if (t.getData().getTPoint().length > LIMIT_ROUTING_PTS_MAX)
                    return void ORMain.showAlertOR(ROUTING_POINTS_LIMIT);
                if (t.getUnit() == UNIT_KM && t.getDistance() > LIMIT_ROUTING_KM || t.getUnit() != UNIT_KM && t.getDistance() > LIMIT_ROUTING_MILES)
                    return void ORMain.showAlertOR(ROUTING_DISTANCE_LIMIT)
            } else {
                if (null != t.getData().getLayerPoint() && null != t.getData().getLayerPoint().features && t.getData().getLayerPoint().features.length < LIMIT_ROUTING_PTS_MIN)
                    return;
                if (null != t.getData().getLayerPoint() && null != t.getData().getLayerPoint().features && t.getData().getLayerPoint().features.length > LIMIT_ROUTING_PTS_MAX)
                    return void ORMain.showAlertOR(ROUTING_POINTS_LIMIT);
                if (t.getUnit() == UNIT_KM && t.getDistance() > LIMIT_ROUTING_KM || t.getUnit() != UNIT_KM && t.getDistance() > LIMIT_ROUTING_MILES)
                    return void ORMain.showAlertOR(ROUTING_DISTANCE_LIMIT)
            }
            t.getRoutingEngine(),
            t.getRoutingMode();
            switch (t.getRoutingEngine()) {
            case 1:
                switch (n = ORGMapEngine,
                t.getRoutingMode()) {
                case 0:
                    ORGMapEngine.setDrivingMode(google.maps.TravelMode.DRIVING);
                    break;
                case 1:
                    ORGMapEngine.setDrivingMode(google.maps.TravelMode.WALKING);
                    break;
                case 2:
                    ORGMapEngine.setDrivingMode(google.maps.TravelMode.BICYCLING);
                    break;
                default:
                    ORGMapEngine.setDrivingMode(google.maps.TravelMode.DRIVING)
                }
                break;
            default:
                n = ORGMapEngine
            }
            a.setUnit(t.getUnit()),
            n.setUnit(t.getUnit()),
            p = t,
            e(),
            d = void 0 != l ? l : null,
            t.getData().getType() == ORConstants.TYPE_GMAP && (i = t.getData().getTPoint());
            var g = ORRouteUtils.getPointTypeAsUrl(i);
            o = ORRouteUtils.splitUrlType(g),
            s = o.length,
            document.fire("or:cbrouting", {
                result: null
            })
        },
        callbackRouting: function(e) {
            var u = e.memo.result;
            if (Event.stop(e),
            r > 0 && null != u) {
                if (l++,
                0 != u.getStatus())
                    return g = [],
                    g.push(u),
                    void t();
                g.push(u)
            }
            if (s > l) {
                var p = o[l]
                  , d = []
                  , R = r + p.length + 1;
                if (R > i.length && (R = r + p.length,
                1 == p.length))
                    return void t();
                d = i.slice(r, R),
                r = R - 1,
                p.charAt(0) == ORConstants.TYPE_POINT_DEFAULT ? n.calculateRoute(d, c) : a.calculateRoute(d, c)
            } else
                t()
        },
        setRoutingEngine: function(e) {
            n = e
        },
        isLock: function() {
            return u
        },
        unLock: function() {
            o = [],
            i = [],
            r = 0,
            s = 0,
            l = 0,
            c = LANG,
            g = [],
            u = !1,
            p.getData().getType() == ORConstants.TYPE_GMAP ? ($("calitgmap-calajax").hide(),
            $("unlockgmap").hide()) : ($("calitign-calajax").hide(),
            $("unlockign").hide()),
            p = null
        }
    }
}();
document.observe("or:cbrouting", ORDirUtils.callbackRouting);
var ORRouteUtils = function() {
    return {
        calculateDistance: function(e) {
            var t = 0;
            return null != e.getTPointDir() && e.getTPointDir().length > 0 ? t = ORUtils.calculateTotalDistance(e.getTPointDir(), e.getUnit()).toFixed(3) : e.getData().getType() == ORConstants.TYPE_GMAP && (t = ORUtils.calculateTotalDistance(e.getData().getTPoint(), e.getUnit()).toFixed(3)),
            t
        },
        calculateStonesPosition: function(e, t) {
            var n = [];
            return null != e.getTPointDir() && e.getTPointDir().length > 0 ? n = ORUtils.calculateFixedPosition(e.getTPointDir(), e.getDistance(), t, e.getUnit()) : e.getData().getType() == ORConstants.TYPE_GMAP && (n = ORUtils.calculateFixedPosition(e.getData().getTPoint(), e.getDistance(), t, e.getUnit())),
            n
        },
        calculatePolyline: function(e) {
            var t = .5 * Math.floor(e.getStrokeWidth() / 3) + 1
              , n = "M " + -2 * t + "," + 3 * t + " 0," + -3 * t + " " + 2 * t + "," + 3 * t + " 0," + 2.25 * t + " z"
              , a = {
                path: n,
                fillColor: e.getStrokeColor(),
                strokeColor: e.getStrokeColor(),
                fillOpacity: e.getStrokeOpacity(),
                strokeOpacity: e.getStrokeOpacity(),
                scale: 2
            }
              , o = null;
            return e.getData().getType() == ORConstants.TYPE_GMAP && (null != e.getTPointDir() && e.getTPointDir().length > 1 || e.getData().getTPoint().length > 1) && (o = new google.maps.Polyline({
                path: null != e.getTPointDir() && e.getTPointDir().length > 0 ? e.getTPointDir() : e.getData().getTPoint(),
                strokeColor: e.getStrokeColor(),
                strokeOpacity: e.getStrokeOpacity(),
                strokeWeight: e.getStrokeWidth(),
                clickable: !1,
                editable: !1,
                icons: [{
                    icon: a,
                    offset: "50px",
                    repeat: "150px",
                    zIndex: 1e3
                }]
            })),
            o
        },
        calculateProfilePolyline: function(e, t, n) {
            var a = null;
            if (null != e.getTPointDir() && e.getTPointDir().length > 1 || e.getData().getTPoint().length > 1) {
                var o = [];
                null != e.getTPointDir() && e.getTPointDir().length > 0 ? ptsi = e.getTPointDir().slice(0, t) : ptsi = e.getData().getTPoint().slice(0, t),
                o = o.concat(ptsi),
                o.push(n);
                var i = STROKE_COLOR_PROFILE_1;
                "#000000" == e.getStrokeColor() && (i = STROKE_COLOR_PROFILE_2),
                a = new google.maps.Polyline({
                    path: o,
                    strokeColor: i,
                    strokeOpacity: e.getStrokeOpacity(),
                    strokeWeight: e.getStrokeWidth() + 2,
                    clickable: !1,
                    editable: !1,
                    zIndex: -100
                })
            }
            return a
        },
        getPointTypeAsUrl: function(e) {
            for (var t = "", n = 0; n < e.length; n++)
                t += e[n].getORType();
            return t
        },
        splitUrlType: function(e) {
            for (var t = [], n = "", a = "", o = 0; o < e.length; o++)
                (0 == o || n != e.charAt(o)) && (o > 0 && t.push(a),
                a = "",
                n = e.charAt(o)),
                a += n;
            return t.push(a),
            t
        },
        getFirstPoint: function(e) {
            var t = "none";
            return e.getTPointDir().length > 0 ? t = e.getTPointDir()[0].lat() + ";" + e.getTPointDir()[0].lng() : e.getData().getType() == ORConstants.TYPE_GMAP && e.getData().getTPoint().length > 0 && (t = e.getData().getTPoint()[0].lat() + ";" + e.getData().getTPoint()[0].lng()),
            t
        },
        getFirstORLatLng: function(e) {
            var t = null;
            return e.getTPointDir().length > 0 ? t = e.getTPointDir()[0] : e.getData().getType() == ORConstants.TYPE_GMAP && e.getData().getTPoint().length > 0 && (t = e.getData().getTPoint()[0]),
            t
        },
        getLastPoint: function(e) {
            var t = "none";
            if (e.getTPointDir().length > 0) {
                var n = e.getTPointDir().length - 1;
                t = e.getTPointDir()[n].lat() + ";" + e.getTPointDir()[n].lng()
            } else if (e.getData().getType() == ORConstants.TYPE_GMAP && e.getData().getTPoint().length > 0) {
                var n = e.getData().getTPoint().length - 1;
                t = e.getData().getTPoint()[n].lat() + ";" + e.getData().getTPoint()[n].lng()
            }
            return t
        }
    }
}()
  , ORUtils = {
    extendOLPoint: function(e, t) {
        return OpenLayers.Util.extend(e, {
            ortype: t || ORConstants.TYPE_POINT_DEFAULT,
            setORType: function(e) {
                this.ortype = e
            },
            getORType: function() {
                return this.ortype
            },
            changeORType: function() {
                this.ortype == ORConstants.TYPE_POINT_DEFAULT ? this.ortype = ORConstants.TYPE_POINT_NOROUTING : this.ortype = ORConstants.TYPE_POINT_DEFAULT
            }
        })
    },
    calculateDistanceBetweenTwoORLatLng: function(e, t, n, a) {
        var o = 0;
        if (e.length > 1 && t >= 0 && n > 0)
            for (var i = t; n > i; i++)
                u = Math.sin(e[i].latRadians()) * Math.sin(e[i + 1].latRadians()) + Math.cos(e[i].latRadians()) * Math.cos(e[i + 1].latRadians()) * Math.cos(e[i + 1].lngRadians() - e[i].lngRadians()),
                u >= -1 && u <= 1 && (o += a == UNIT_KM ? ORConstants.EARTH_RAD_KM * Math.acos(u) : ORConstants.EARTH_RAD_MILES * Math.acos(u));
        return o
    },
    lngToX: function(e) {
        return Math.round(ORConstants.OFFSET_GMAPS + ORConstants.RADIUS_GMAPS * e * Math.PI / 180)
    },
    XToLng: function(e) {
        return 180 / Math.PI * (e - ORConstants.OFFSET_GMAPS) / ORConstants.RADIUS_GMAPS
    },
    latToY: function(e) {
        return Math.round((ORConstants.OFFSET_GMAPS - ORConstants.RADIUS_GMAPS * Math.log((1 + Math.sin(e * Math.PI / 180)) / (1 - Math.sin(e * Math.PI / 180)))) / 2)
    },
    YToLat: function(e) {
        return 180 / Math.PI * (2 * Math.atan(Math.exp((ORConstants.OFFSET_GMAPS - 2 * e) / (2 * ORConstants.RADIUS_GMAPS))) - Math.PI / 2)
    },
    calculatePoiPositionInGrid: function(e, t, n, a, o) {
        var i = this.lngToX(t)
          , r = this.latToY(e)
          , s = Math.sqrt(Math.pow(n << 21 - o, 2));
        n > 0 && (s = -s);
        var l = this.XToLng(i - s)
          , c = Math.sqrt(Math.pow(a << 21 - o, 2));
        0 > a && (c = -c);
        var u = this.YToLat(r - c);
        return {
            lat: u,
            lng: l
        }
    },
    calculateTotalDistance: function(e, t) {
        var n = 0;
        if (e.length > 1)
            for (var a = 0; a < e.length - 1; a++)
                u = Math.sin(e[a].latRadians()) * Math.sin(e[a + 1].latRadians()) + Math.cos(e[a].latRadians()) * Math.cos(e[a + 1].latRadians()) * Math.cos(e[a + 1].lngRadians() - e[a].lngRadians()),
                u >= -1 && u <= 1 && (n += t == UNIT_KM ? ORConstants.EARTH_RAD_KM * Math.acos(u) : ORConstants.EARTH_RAD_MILES * Math.acos(u));
        return n
    },
    calculateDistanceFromStart: function(e, t, n) {
        var a = 0;
        if (e.length > 1 && t > 0)
            for (var o = 1; t >= o; o++)
                u = Math.sin(e[o - 1].latRadians()) * Math.sin(e[o].latRadians()) + Math.cos(e[o - 1].latRadians()) * Math.cos(e[o].latRadians()) * Math.cos(e[o].lngRadians() - e[o - 1].lngRadians()),
                u >= -1 && u <= 1 && (a += n == UNIT_KM ? ORConstants.EARTH_RAD_KM * Math.acos(u) : ORConstants.EARTH_RAD_MILES * Math.acos(u));
        return a
    },
    calculateFixedPosition: function(e, t, n, a) {
        if (0 == n)
            return [];
        var o = 0
          , i = 0
          , r = 0
          , s = 0
          , l = n
          , c = 1
          , u = [];
        for (e.length > 0 && u.push(new ORLatLng(e[0].lat(),e[0].lng())); t >= l && l <= LIMIT_KM; ) {
            o = 0,
            i = 0;
            for (var g = r; g < e.length - 1; g++)
                if (o = this.calculateDistanceBetweenTwoORLatLng(e, g, g + 1, a),
                s += o,
                s.toFixed(3) >= l) {
                    i = s - l,
                    r = g,
                    s -= o,
                    c = o / i;
                    var p = e[g + 1].lat() - (e[g + 1].lat() - e[g].lat()) / c
                      , d = e[g + 1].lng() - (e[g + 1].lng() - e[g].lng()) / c;
                    u.push(new ORLatLng(p,d));
                    break
                }
            l += n
        }
        return u
    },
    convertGMapPointsToORLatLng: function(e) {
        for (var t = [], n = 0; n < e.length; n++)
            t.push(new ORLatLng(e[n].lat(),e[n].lng()));
        return t
    },
    convertORLatLngToGMapPoints: function(e) {
        for (var t = [], n = 0; n < e.length; n++)
            t.push(new GLatLng(e[n].lat(),e[n].lng()));
        return t
    },
    findPoiGeographicPosition: function(e, t, n) {
        var a, o, i, r = 0, s = 0, l = 0, c = 0, u = parseFloat(e), g = ORRouteManager.getActiveRoute(), p = g.getTPointDir();
        (null == p || 0 == p.length) && (g.getData().getType() == ORConstants.TYPE_GMAP ? p = g.getData().getTPoint() : null != g.getData().getORLatLngForPOI() && (p = g.getData().getORLatLngForPOI()));
        for (var d = l; d < p.length - 1; d++)
            if (r = this.calculateDistanceBetweenTwoORLatLng(p, d, d + 1, t),
            c += r,
            c.toFixed(3) >= u) {
                s = c - u,
                l = d,
                c -= r;
                var R = r / s;
                a = p[d + 1].lat() - (p[d + 1].lat() - p[d].lat()) / R,
                o = p[d + 1].lng() - (p[d + 1].lng() - p[d].lng()) / R;
                var M = 0
                  , O = p[d + 1].lat() - p[d].lat()
                  , h = p[d + 1].lng() - p[d].lng()
                  , m = 0;
                if (0 == h)
                    M = O > 0 ? 0 : Math.PI;
                else if (0 == O)
                    M = h > 0 ? -Math.PI / 2 : Math.PI / 2;
                else {
                    var v = O / h;
                    m = Math.atan(v),
                    v > 0 ? O > 0 ? (M = -(Math.PI / 2) + m,
                    M /= ORConstants.EARTH_SPHERE_COR) : (M = Math.PI / 2 + m,
                    M *= ORConstants.EARTH_SPHERE_COR) : O > 0 ? (M = Math.PI / 2 + m,
                    M /= ORConstants.EARTH_SPHERE_COR) : (M = -(Math.PI / 2) + m,
                    M *= ORConstants.EARTH_SPHERE_COR)
                }
                i = new google.maps.LatLng(a,o);
                break
            }
        return n ? {
            pos: i,
            idx: d + 1
        } : i
    },
    getRatingStarClassName: function(e) {
        switch (e) {
        case 1:
            return ORConstants.CLASS_ONESTAR;
        case 2:
            return ORConstants.CLASS_TWOSTAR;
        case 3:
            return ORConstants.CLASS_THREESTAR;
        case 4:
            return ORConstants.CLASS_FOURSTAR;
        case 5:
            return ORConstants.CLASS_FIVESTAR;
        default:
            return ORConstants.CLASS_NORATE;
        }
    },
    splitPartnerUrl: function(e) {
        for (var t = [], n = e.split(PARTNER_URL_SEPARATOR), a = 0, o = n.length; o > a; a++) {
            var i = n[a].split(PARTNER_URL_HREF_SEPARATOR)
              , r = i[1].split(PARTNER_URL_LOG_SEPARATOR);
            t.push({
                urlimg: i[0],
                urllink: r[0],
                urllog: r[1]
            })
        }
        return t
    }
}
  , ORAccMain = {
    validSignUpForm: null,
    validUpdateSignUpForm: null,
    validUpdateEmailForm: null,
    validSignInForm: null,
    validLostAccessForm: null,
    validMessageForm: null,
    validSaveDataForm: null,
    validUpdateDataForm: null,
    validContactForm: null,
    validCartForm: null,
    validShareForm: null,
    validUserRouteCommentForm: null,
    validSubSerForm: null,
    validContestForm: null,
    selectemmdrow: null,
    selectemmdrowBG: null,
    userConnected: !1,
    userOrganizer: !1,
    userPartner: !1,
    mapup: null,
    geocoder: null,
    userIDF: ",",
    userIDS: "",
    userIDR: ",",
    userIDPassTD: ",",
    userIDPassD: ",",
    countPassD2000: 0,
    countPassDone: 0,
    userLogin: "",
    userSub: "",
    isRemovedFromMyOr: !1,
    noads: !1,
    callSignInForm: function(e) {
        return Modalbox.show("account/loginInput.php?u=" + e, {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callSignUpForm: function() {
        return Modalbox.show("account/createAccountInput.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callUpdateSignUpForm: function() {
        return Modalbox.show("account/updateAccountInput.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callUpdateEmailForm: function() {
        return Modalbox.show("account/updateEmailInput.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callMLForm: function(e) {
        return Modalbox.show("account/or-ml.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callCGUForm: function(e) {
        return "cookie" != e ? Modalbox.show("account/or-cgu.php?cf=" + e, {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }) : Modalbox.show("account/or-cgu.php?cf=mp", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1,
            autoFocusing: !0
        }),
        !1
    },
    callContactForm: function() {
        return Modalbox.show("mail/contactInput.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callPartnersPage: function() {
        return Modalbox.show("partners.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callTeamPage: function() {
        return Modalbox.show("team.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callContestForm: function(e) {
        return Modalbox.show("contest/orcontest-in.php?cc=" + e, {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callCreditsPage: function() {
        return Modalbox.show("credits.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callSuscribePage: function() {
        return Modalbox.show("subscription/orSubList.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callCartPage: function() {
        return Modalbox.show("subscription/orSubCart.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callCartIncPage: function() {
        return Modalbox.show("subscription/orSubCartInc.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callCentsCols: function() {
        return Modalbox.show("centcols.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    callStatPage: function() {
        return Modalbox.show("stats/global-stats.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        }),
        !1
    },
    checkFreeLogin: function(e) {
        var t = new Date
          , n = 0;
        return e.length > 5 && new Ajax.Request("account/checkLogin.php?user=" + e + "&t=" + t.getTime(),{
            method: "get",
            asynchronous: !1,
            onSuccess: function(e) {
                "ok" == e.responseText && (n = 1)
            }
        }),
        1 == n ? !0 : !1
    },
    checkFreeMail: function(e) {
        var t = new Date
          , n = 0;
        return e.length > 5 && new Ajax.Request("account/checkMail.php?email=" + e + "&t=" + t.getTime(),{
            method: "get",
            asynchronous: !1,
            onSuccess: function(e) {
                "ok" == e.responseText && (n = 1)
            }
        }),
        1 == n ? !0 : !1
    },
    checkConfirmationCode: function(e) {
        var t = new Date
          , n = 0;
        return isNaN(parseInt(e)) || new Ajax.Request("account/checkCode.php?txtimg=" + e + "&t=" + t.getTime(),{
            method: "get",
            asynchronous: !1,
            onSuccess: function(e) {
                "ok" == e.responseText && (n = 1)
            }
        }),
        1 == n ? !0 : !1
    },
    checkCGU: function(e) {
        e ? $("validcgu").show() : $("validcgu").hide()
    },
    submitSignUpForm: function() {
        var e = ORAccMain.validSignUpForm.validate();
        return e && Modalbox.show("account/createAccount.php", {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            params: Form.serialize("createaccount"),
            overlayClose: !1
        }),
        !1
    },
    submitCGUForm: function() {
        return Modalbox.show("account/cguAccepted.php", {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            params: Form.serialize("acceptcgu"),
            overlayClose: !1
        }),
        !1
    },
    submitUpdateSignUpForm: function() {
        var e = ORAccMain.validUpdateSignUpForm.validate();
        return e && Modalbox.show("account/updateAccount.php", {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            params: Form.serialize("updateaccount"),
            overlayClose: !1
        }),
        !1
    },
    submitUpdateEmailForm: function() {
        var e = ORAccMain.validUpdateEmailForm.validate();
        return $("errmsg").hide(),
        e && new Ajax.Request("account/updateEmail.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("updateemail"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : Modalbox.show("account/updateEmailConfirm.php", {
                    width: ORConstants.MODAL_MEDIUM_WIDTH,
                    height: ORConstants.MODAL_SMALL_HEIGHT,
                    title: ORConstants.MODAL_TITLE,
                    overlayClose: !1
                })
            }
        }),
        !1
    },
    submitSignInForm: function() {
        var e = ORAccMain.validSignInForm.validate();
        return $("errmsg").hide(),
        e && new Ajax.Request("account/login.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("loginform"),
            onSuccess: function(e) {
                if ("ko" == e.responseText)
                    new Effect.Appear("errmsg");
                else {
                    var t = e.responseText.evalJSON(!0);
                    0 == t.flg_cgu ? Modalbox.show("account/or-cgu.php?cf=lo", {
                        width: ORConstants.MODAL_MEDIUM_WIDTH,
                        height: ORConstants.MODAL_SMALL_HEIGHT,
                        title: ORConstants.MODAL_TITLE,
                        overlayClose: !1
                    }) : Modalbox.show("account/loginConfirm.php", {
                        width: ORConstants.MODAL_MEDIUM_WIDTH,
                        height: ORConstants.MODAL_SMALL_HEIGHT,
                        title: ORConstants.MODAL_TITLE,
                        overlayClose: !1
                    })
                }
            }
        }),
        !1
    },
    setUserLogin: function(e) {
        ORAccMain.userLogin = e
    },
    setUserSub: function(e) {
        ORAccMain.userSub = e
    },
    activateDrmMaps: function() {
        if (ORAccMain.userSub) {
            for (var e = 0, t = ORAccMain.userSub.length; t > e; e++) {
                var n = ORAccMain.userSub[e];
                ORMain.USER_DRM[n.tt] = n.tk
            }
            ORMain.mapManager && ORMain.mapManager.forceEnableMaps()
        }
    },
    deActivateDrmMaps: function() {
        if (ORMain.USER_DRM) {
            for (var e in ORMain.USER_DRM)
                ORMain.USER_DRM[e] = "";
            ORMain.mapManager && ORMain.mapManager.forceEnableMaps()
        }
    },
    deActivateDrm: function(e) {
        ORMain.USER_DRM && ORMain.USER_DRM[e] && (ORMain.USER_DRM[e] = "",
        ORMain.mapManager && ORMain.mapManager.forceEnableMaps())
    },
    addCart: function(e) {
        $(e).hide();
        var t = "#cart_choice_form input:radio[name=item_" + e + "]:checked"
          , n = $j(t).val();
        new Ajax.Request("subscription/orSubCartAct.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                act: $j.b64.encode("ci:" + n + ";ca:add")
            },
            onSuccess: function(t) {
                var n = t.responseText.evalJSON(!0);
                if (0 != n.status)
                    console.log("Erreur"),
                    $(e).show();
                else {
                    $("cart_choice_link").show(),
                    $("cart_choice_nb_item").innerHTML = n.total_cart_items;
                    var a = "bloc_" + e;
                    $(a).hide()
                }
            }
        })
    },
    removeIC: function() {
        var e = ORAccMain.getCheckedBox("cartvalid");
        return "" == e ? !1 : void ORAccMain.remCart(e)
    },
    remCart: function(e) {
        console.log(e),
        new Ajax.Request("subscription/orSubCartAct.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                act: $j.b64.encode("ci:" + e + ";ca:rem")
            },
            onSuccess: function(e) {
                var t = e.responseText.evalJSON(!0);
                0 != t.status ? console.log("Erreur") : t.total_cart_items > 0 ? ORAccMain.callCartPage() : ORAccMain.callSuscribePage()
            }
        })
    },
    valCart: function() {
        console.log(),
        new Ajax.Request("subscription/orSubCartAct.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                act: $j.b64.encode("ci:NONE;ca:val")
            },
            onSuccess: function(e) {
                var t = e.responseText.evalJSON(!0);
                0 != t.status ? console.log("Erreur") : ORAccMain.callCartIncPage()
            }
        })
    },
    submitCartForm: function() {
        var e = ORAccMain.validCartForm.validate();
        return e && Modalbox.show("subscription/orSubCartIncOut.php", {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_LARGE_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            params: Form.serialize("orcartform"),
            overlayClose: !1
        }),
        !1
    },
    checkLostPwdMail: function(e) {
        var t = new Date
          , n = 0;
        return e.length > 5 && new Ajax.Request("account/checkMail.php?email=" + e + "&mode=1&t=" + t.getTime(),{
            method: "get",
            asynchronous: !1,
            onSuccess: function(e) {
                "ok" == e.responseText && (n = 1)
            }
        }),
        1 == n ? !0 : !1
    },
    submitLostAccessForm: function() {
        var e = ORAccMain.validLostAccessForm.validate();
        return e && new Ajax.Request("account/retrievePwdLost.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("lostaccessform"),
            onSuccess: function(e) {
                e.responseText;
                "ok" == e.responseText && Modalbox.show("account/retrievePwdLostConfirm.php", {
                    width: ORConstants.MODAL_MEDIUM_WIDTH,
                    height: ORConstants.MODAL_SMALL_HEIGHT,
                    title: ORConstants.MODAL_TITLE,
                    method: "post",
                    params: Form.serialize("lostaccessform"),
                    overlayClose: !1
                })
            }
        }),
        !1
    },
    submitLogout: function() {
        ORAccMain.setUserConnected(0),
        ORAccMain.userIDF = ",",
        ORAccMain.userIDS = "",
        ORAccMain.userIDR = ",",
        ORAccMain.userIDR = ",",
        ORAccMain.userIDPassTD = ",",
        ORAccMain.userIDPassD = ",",
        ORAccMain.countPassD2000 = 0,
        countPassDone = 0,
        new Ajax.Request("account/logout.php",{
            method: "post",
            asynchronous: !1
        }),
        ORMain.tabMainManager.setActiveTab("tab_home"),
        ORElementShowableManager.hideTabElements("tab-plan"),
        ORElementShowableManager.hideTabElements("tab-search"),
        ORMain.changeHeaderSize(!0),
        document.fire("or:connected")
    },
    submitContestForm: function() {
        var e = ORAccMain.validContestForm.validate();
        return $("errmsg").hide(),
        e && new Ajax.Request("contest/orcontest-out.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("orcontestform"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : $("orcontestcontent").innerHTML = e.responseText
            }
        }),
        !1
    },
    setUserPref: function(e, t, n, a, o, i, r, s) {
        0 != e && 0 != t && (ORMain.centerPoint.setLat(e),
        ORMain.centerPoint.setLng(t),
        ORMain.wheelmouse = n,
        ORMain.autocenter = a,
        ORMain.changeUnit(o),
        "" != i && (ORConstants.STROKE_COLOR_DEFAULT = i),
        "" != r && (ORConstants.STROKE_OPACITY_DEFAULT = r),
        "" != s && (ORConstants.STROKE_WIDTH_DEFAULT = s))
    },
    uP: function() {
        var e = ORAccMain.mapup.getCenter();
        $("userlat").value = e.lat(),
        $("userlng").value = e.lng(),
        $("err-up").hide(),
        $("suc-up").hide(),
        new Ajax.Request("account/userPreferencesUpdate.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("orup"),
            onSuccess: function(e) {
                if ("ko" == e.responseText)
                    new Effect.Appear("err-up");
                else {
                    new Effect.Appear("suc-up"),
                    setTimeout(function() {
                        $("suc-up").hide()
                    }, 3e3);
                    var t = e.responseText.evalJSON(!0);
                    ORAccMain.setUserPref(t.lat, t.lng, t.mo, t.ac, t.unit, t.stc, t.sto, t.stw),
                    ORMain.refreshUserPref()
                }
            }
        })
    },
    initPref: function() {
        var e = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggableCursor: "crosshair",
            keyboardShortcuts: !0,
            zoomControl: !0,
            mapTypeControl: !1,
            scaleControl: !0,
            streetViewControl: !1,
            scrollwheel: !1,
            center: new google.maps.LatLng(ORMain.centerPoint.getLat(),ORMain.centerPoint.getLng()),
            zoom: 10,
            mapTypeControlOptions: {
                position: google.maps.ControlPosition.RIGHT_BOTTOM,
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            }
        };
        ORAccMain.mapup = new google.maps.Map(document.getElementById("map-pref"),e);
        var t = document.getElementById("locu")
          , n = new google.maps.places.SearchBox(t);
        ORAccMain.mapup.controls[google.maps.ControlPosition.TOP_LEFT].push(t),
        n.addListener("places_changed", function() {
            var e = n.getPlaces();
            0 != e.length && (ORAccMain.mapup.setCenter(e[0].geometry.location),
            ORAccMain.mapup.setZoom(13))
        })
    },
    addFav: function(e) {
        ORAccMain.isUserConnected() ? (ORAccMain.userIDF += e + ",",
        new Ajax.Request("route-data/addFavorite.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                lr: ORAccMain.userIDF
            },
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("err-af") : (ORAccMain.hideShowFavB(!1),
                $("supfav").show())
            }
        })) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    removeMF: function() {
        var e = ORAccMain.getCheckedBox("myfavorites");
        return "" == e ? !1 : (e = "," + e + ",",
        void ORAccMain.remFav(e))
    },
    remFav: function(e) {
        new Ajax.Request("route-data/removeFavorite.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                lr: ORAccMain.userIDF,
                ltr: e
            },
            onSuccess: function(e) {
                "ko" == e.responseText || (ORAccMain.userIDF = e.responseText,
                ORAccMain.hideShowFavB(!0),
                ORSearchMain.searchMF())
            }
        })
    },
    activateFavLink: function() {
        if (ORAccMain.isUserConnected()) {
            var e = ORRouteManager.getActiveRoute();
            e.getId() > 0 && (ORAccMain.userIDF.indexOf("," + e.getId() + ",") > -1 ? $("supfav") && ($("supfav").show(),
            $("notconfav").hide()) : ORAccMain.getFavSize() < ORConstants.NB_FAVOURITE_LIMIT ? $("quofav") && ($("quofav").hide(),
            ORAccMain.hideShowFavB(!0),
            $("notconfav").hide()) : $("quofav") && ($("quofav").show(),
            $("notconfav").hide()))
        } else
            $("notconfav") && $("notconfav").show()
    },
    deActivateFavLink: function() {
        $("supfav") && $("supfav").hide(),
        $("quofav") && $("quofav").hide(),
        $("notconfav") && $("notconfav").show(),
        ORAccMain.hideShowFavB(!1)
    },
    hideShowFavB: function(e) {
        if ($$("a.addfav")) {
            var t = $$("a.addfav");
            if (t.length > 0)
                for (var n = 0, a = t.length; a > n; n++)
                    e ? t[n].show() : t[n].hide()
        }
    },
    addPass: function(e, t, n) {
        ORAccMain.isUserConnected() ? ("TD" == n ? (ORAccMain.userIDPassTD.indexOf("," + e + ",") > -1 ? ORAccMain.userIDPassTD = ORAccMain.userIDPassTD.replace("," + e + ",", ",") : ORAccMain.userIDPassTD += e + ",",
        ORAccMain.userIDPassD.indexOf("," + e + ",") > -1 && (ORAccMain.userIDPassD = ORAccMain.userIDPassD.replace("," + e + ",", ","),
        t >= 2e3 && ORAccMain.countPassD2000--,
        ORAccMain.countPassDone--)) : ORAccMain.userIDPassD.indexOf("," + e + ",") > -1 ? (ORAccMain.userIDPassD = ORAccMain.userIDPassD.replace("," + e + ",", ","),
        t >= 2e3 && ORAccMain.countPassD2000--,
        ORAccMain.countPassDone--) : (ORAccMain.userIDPassTD.indexOf("," + e + ",") > -1 && (ORAccMain.userIDPassTD = ORAccMain.userIDPassTD.replace("," + e + ",", ",")),
        ORAccMain.userIDPassD += e + ",",
        ORAccMain.countPassDone++,
        t >= 2e3 && ORAccMain.countPassD2000++),
        new Ajax.Request("route-data/addPass.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                lrd: ORAccMain.userIDPassD,
                lrtd: ORAccMain.userIDPassTD,
                nd: ORAccMain.countPassDone,
                nd2: ORAccMain.countPassD2000
            },
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("err-af") : (ORGMapDecoratorManager.isShowColsM() && ORGMapDecoratorManager.showColsM(!0, !0),
                ORGMapDecoratorManager.isShowColsR() && ORGMapDecoratorManager.showColsR(!0, !0))
            }
        })) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    hasPassInUserList: function(e, t) {
        return "TD" == t && ORAccMain.userIDPassTD.indexOf("," + e + ",") > -1 ? !0 : "D" == t && ORAccMain.userIDPassD.indexOf("," + e + ",") > -1 ? !0 : !1
    },
    setUserPIDS: function(e, t, n, a) {
        ORAccMain.userIDPassD = e,
        "" == ORAccMain.userIDPassD && (ORAccMain.userIDPassD = ","),
        ORAccMain.userIDPassTD = t,
        "" == ORAccMain.userIDPassTD && (ORAccMain.userIDPassTD = ","),
        ORAccMain.countPassD2000 = a,
        "" == ORAccMain.countPassD2000 && (ORAccMain.countPassD2000 = 0),
        ORAccMain.countPassDone = n,
        "" == ORAccMain.countPassDone && (ORAccMain.countPassDone = 0)
    },
    removeMP: function(e) {
        if (ORAccMain.isUserConnected()) {
            if ("D" == e) {
                var t = ORAccMain.getCheckedBox("mypass_done");
                if ("" != t)
                    for (var n = t.split(","), a = 0; a < n.length; a++) {
                        var o = n[a].split("_")
                          , i = o[0]
                          , r = o[1];
                        ORAccMain.userIDPassD.indexOf("," + i + ",") > -1 && (ORAccMain.userIDPassD = ORAccMain.userIDPassD.replace("," + i + ",", ","),
                        r >= 2e3 && ORAccMain.countPassD2000--,
                        ORAccMain.countPassDone--)
                    }
            } else {
                var t = ORAccMain.getCheckedBox("mypass_todo");
                if ("" != t)
                    for (var n = t.split(","), a = 0; a < n.length; a++) {
                        var o = n[a].split("_")
                          , i = o[0]
                          , r = o[1];
                        ORAccMain.userIDPassTD.indexOf("," + i + ",") > -1 && (ORAccMain.userIDPassTD = ORAccMain.userIDPassTD.replace("," + i + ",", ","))
                    }
            }
            if ("" == t)
                return !1;
            ORAccMain.upPass()
        }
    },
    changeMPTD: function() {
        if (ORAccMain.isUserConnected()) {
            var e = ORAccMain.getCheckedBox("mypass_todo");
            if ("" != e)
                for (var t = e.split(","), n = 0; n < t.length; n++) {
                    var a = t[n].split("_")
                      , o = a[0]
                      , i = a[1];
                    ORAccMain.userIDPassTD.indexOf("," + o + ",") > -1 && (ORAccMain.userIDPassTD = ORAccMain.userIDPassTD.replace("," + o + ",", ",")),
                    ORAccMain.userIDPassD += o + ",",
                    i >= 2e3 && ORAccMain.countPassD2000++,
                    ORAccMain.countPassDone++
                }
            if ("" == e)
                return !1;
            ORAccMain.upPass()
        }
    },
    upPass: function() {
        new Ajax.Request("route-data/upPass.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                lrd: ORAccMain.userIDPassD,
                lrtd: ORAccMain.userIDPassTD,
                nd: ORAccMain.countPassDone,
                nd2: ORAccMain.countPassD2000
            },
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("err-af") : ORSearchMain.searchMPS()
            }
        })
    },
    hidePassAccess: function() {
        ORGMapDecoratorManager.hideColsR(),
        ORGMapDecoratorManager.hideColsM()
    },
    share: function(e, t) {
        Modalbox.show("mail/shareRouteInput.php?rid=" + e + "&mode=" + t, {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "get",
            overlayClose: !1
        })
    },
    submitShareForm: function() {
        var e = ORAccMain.validShareForm.validate();
        return e && new Ajax.Request("mail/sendShare.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("createshareform"),
            onSuccess: function(e) {
                e.responseText;
                "ok" == e.responseText ? $("layerShareForm").innerHTML = CONTACT_MESSAGE_SENT : $("errmsg").show(),
                $("layerShareFormClose").show()
            }
        }),
        !1
    },
    setUserIDF: function(e) {
        ORAccMain.userIDF = e
    },
    getFavSize: function() {
        var e = ORAccMain.userIDF.split(",");
        return e.length - 2
    },
    setUserIDR: function(e) {
        ORAccMain.userIDR = e
    },
    sendMessageInput: function(e, t, n) {
        return ORAccMain.isUserConnected() ? Modalbox.show("message/createMessageInput.php?idm=" + e + "&userto=" + t + "&idr=" + n, {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "get",
            overlayClose: !1
        }) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            overlayClose: !1
        }),
        !1
    },
    readMessage: function(e, t) {
        Modalbox.show("message/readMessage.php?idmessage=" + e, {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "get",
            overlayClose: !1
        }),
        null != ORAccMain.selectemmdrow && (ORAccMain.selectemmdrow.parentNode.style.backgroundColor = ORAccMain.selectemmdrowBG),
        ORAccMain.selectemmdrow = t,
        ORAccMain.selectemmdrowBG = t.parentNode.style.backgroundColor,
        t.parentNode.style.backgroundColor = "#FC3",
        t.parentNode.style.fontWeight = "normal",
        t.parentNode.style.color = "#000"
    },
    removeMM: function() {
        var e = ORAccMain.getCheckedBox("mymessages");
        return "" == e ? !1 : void ORAccMain.removeMessages(e)
    },
    removeMessages: function(e) {
        return new Ajax.Request("message/removeMessages.php?listidmess=" + e,{
            method: "get",
            asynchronous: !1,
            onSuccess: function(e) {
                ORSearchMain.searchMM()
            }
        }),
        !1
    },
    submitMessageForm: function() {
        var e = ORAccMain.validMessageForm.validate();
        return e && new Ajax.Request("message/sendMessage.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("createmessageform"),
            onSuccess: function(e) {
                e.responseText;
                "ok" == e.responseText ? $("layerMessageForm").innerHTML = "<h2>" + CONTACT_MESSAGE_SENT + "</h2>" : $("errmsg").show(),
                $("layerMessageFormClose").show()
            }
        }),
        !1
    },
    activateMessageLink: function() {
        ORAccMain.isUserConnected() ? $("conaut") && ($("conaut").show(),
        $("notconaut").hide()) : ORAccMain.deActivateMessageLink()
    },
    deActivateMessageLink: function() {
        $("conaut") && ($("conaut").hide(),
        $("notconaut").show())
    },
    activateExportLink: function() {
        ORAccMain.isUserConnected() ? $("conexp") && ($("conexp").show(),
        $("notconexp") && $("notconexp").hide()) : ORAccMain.deActivateExportLink()
    },
    deActivateExportLink: function() {
        $("conexp") && ($("conexp").hide(),
        $("notconexp") && $("notconexp").show())
    },
    activateExportPartnerLink: function() {
        if (ORAccMain.isUserConnected()) {
            var e = ORRouteManager.getActiveRoute();
            ORAccMain.userIDS.indexOf("," + e.getId() + ",") > -1 && ORAccMain.isUserPartner() && $("exp_partner") && $("exp_partner").show()
        } else
            ORAccMain.deActivateExportPartnerLink()
    },
    deActivateExportPartnerLink: function() {
        $("exp_partner") && $("exp_partner").hide()
    },
    submitSaveDataForm: function() {
        $("savebutton").hide();
        var e = ORAccMain.validSaveDataForm.validate();
        if (e) {
            var t = ORRouteManager.getActiveRoute();
            document.forms.saveroute.routecomment;
            document.forms.saveroute.routecomment.value = CKEDITOR.instances.routecomment.getData(),
            t.getTPointDir().length > 0 && (document.forms.saveroute.roupoints.value = escape(createEncodings(t.getTPointDir()))),
            t.getData().getType() == ORConstants.TYPE_GMAP && (document.forms.saveroute.points.value = escape(createEncodings(t.getData().getTPoint())),
            document.forms.saveroute.type_points.value = ORRouteUtils.getPointTypeAsUrl(t.getData().getTPoint())),
            document.forms.saveroute.distance.value = t.getDistance(),
            t.setInterval(INTERVAL_MIN),
            t.getDistance() > LIMIT_INTERVAL && t.setInterval(INTERVAL_MAX),
            document.forms.saveroute.interval.value = t.getInterval();
            var n = ORElevationMain.cKmPos();
            document.forms.saveroute.fixpoints.value = escape(n),
            document.forms.saveroute.routing_engine.value = t.getRoutingEngine(),
            document.forms.saveroute.routing_mode.value = t.getRoutingMode(),
            document.forms.saveroute.strokecolor.value = t.getStrokeColor(),
            document.forms.saveroute.strokeopacity.value = t.getStrokeOpacity(),
            document.forms.saveroute.strokewidth.value = t.getStrokeWidth(),
            document.forms.saveroute.typeroute.value = t.getTypeRoute(),
            document.forms.saveroute.stepstones.value = t.getStepStones(),
            document.forms.saveroute.unit.value = 0,
            new Ajax.Request("route-data/saveData.php",{
                method: "post",
                asynchronous: !1,
                parameters: Form.serialize("saveroute"),
                onSuccess: function(e) {
                    var t = e.responseText;
                    if (isNaN(parseInt(t)))
                        ;
                    else {
                        document.fire("or:routesaved", {
                            routeId: t
                        });
                        var n = ORAccMain.getIDSNumber();
                        Modalbox.show("route-data/saveDataConfirm.php?ru=" + n + "&rid=" + t, {
                            slideDownDuration: "0",
                            title: ORConstants.MODAL_TITLE,
                            width: ORConstants.MODAL_LARGE_WIDTH,
                            height: ORConstants.MODAL_LARGE_HEIGHT,
                            overlayClose: !1
                        })
                    }
                }
            })
        } else
            $("savebutton").show();
        return !1
    },
    submitUpdateDataForm: function() {
        $("savebutton").hide();
        var e = ORAccMain.validUpdateDataForm.validate();
        if (e) {
            var t = ORRouteManager.getActiveRoute();
            document.forms.updateroute.routecomment.value = CKEDITOR.instances.routecomment.getData(),
            t.getTPointDir().length > 0 && (document.forms.updateroute.roupoints.value = escape(createEncodings(t.getTPointDir()))),
            t.getData().getType() == ORConstants.TYPE_GMAP && (document.forms.updateroute.points.value = escape(createEncodings(t.getData().getTPoint())),
            document.forms.updateroute.type_points.value = ORRouteUtils.getPointTypeAsUrl(t.getData().getTPoint())),
            document.forms.updateroute.distance.value = t.getDistance(),
            t.setInterval(INTERVAL_MIN),
            t.getDistance() > LIMIT_INTERVAL && t.setInterval(INTERVAL_MAX),
            document.forms.updateroute.interval.value = t.getInterval();
            var n = ORElevationMain.cKmPos();
            document.forms.updateroute.fixpoints.value = escape(n),
            document.forms.updateroute.routing_engine.value = t.getRoutingEngine(),
            document.forms.updateroute.routing_mode.value = t.getRoutingMode(),
            document.forms.updateroute.strokecolor.value = t.getStrokeColor(),
            document.forms.updateroute.strokeopacity.value = t.getStrokeOpacity(),
            document.forms.updateroute.strokewidth.value = t.getStrokeWidth(),
            document.forms.updateroute.typeroute.value = t.getTypeRoute(),
            document.forms.updateroute.stepstones.value = t.getStepStones(),
            document.forms.updateroute.unit.value = 0,
            new Ajax.Request("route-data/updateData.php",{
                method: "post",
                asynchronous: !1,
                parameters: Form.serialize("updateroute"),
                onSuccess: function(e) {
                    var t = e.responseText;
                    isNaN(parseInt(t)) || (document.fire("or:routeupdated", {
                        routeId: t
                    }),
                    Modalbox.show("route-data/updateDataConfirm.php?rid=" + t, {
                        slideDownDuration: "0",
                        title: ORConstants.MODAL_TITLE,
                        width: ORConstants.MODAL_LARGE_WIDTH,
                        height: ORConstants.MODAL_SMALL_HEIGHT,
                        overlayClose: !1
                    }))
                }
            })
        } else
            $("savebutton").show();
        return !1
    },
    removeData: function(e) {
        -1 == e && ORRouteManager.getActiveRoute().getId() > 0 && (e = ORRouteManager.getActiveRoute().getId() + "",
        ORAccMain.isRemovedFromMyOr = !1),
        e.length > 0 && Modalbox.show("route-data/removeDataInput.php?idroute=" + e, {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "get",
            overlayClose: !1
        })
    },
    removeMR: function() {
        var e = ORAccMain.getCheckedBox("myroutes");
        return "" == e ? !1 : (ORAccMain.isRemovedFromMyOr = !0,
        void ORAccMain.removeData(e))
    },
    removeRoutes: function() {
        var e = document.forms.removeroute.listidroute.value;
        return new Ajax.Request("route-data/removeData.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("removeroute"),
            onSuccess: function(t) {
                $("removeinputid").innerHTML = t.responseText,
                ORAccMain.removeIDS(e),
                ORAccMain.isRemovedFromMyOr && ORSearchMain.searchMR(),
                ORAccMain.checkRoutesRights()
            }
        }),
        !1
    },
    setLangue: function(e) {
        var t = document.URL;
        if (t.indexOf("#") > -1 && (t = t.substr(0, t.indexOf("#"))),
        -1 == t.indexOf(".php"))
            window.location = t + "index.php?lang=" + e;
        else if ("php" == t.substr(t.length - 3, t.length))
            window.location = t + "?lang=" + e;
        else {
            var n = t.indexOf("lang", 0);
            if (-1 != n) {
                var a = t.split("?")
                  , o = "";
                o = o + a[0] + "?";
                for (var i = a[1].split("&"), r = 0; r < i.length; r++)
                    -1 != i[r].indexOf("lang", 0) ? o = o + "lang=" + e : o += i[r],
                    r < i.length - 1 && (o += "&");
                window.location = o
            } else
                t = t + "&lang=" + e,
                window.location = t
        }
    },
    submitContactForm: function() {
        var e = ORAccMain.validContactForm.validate();
        return e && new Ajax.Request("mail/sendContact.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("createcontactform"),
            onSuccess: function(e) {
                e.responseText;
                "ok" == e.responseText ? $("layerContactForm").innerHTML = "<h2>" + CONTACT_MESSAGE_SENT + "&nbsp;" + CONTACT_ANSWER_ASAP + "</h2>" : $("errmsg").show(),
                $("layerContactFormClose").show()
            }
        }),
        !1
    },
    rateRoute: function(e, t) {
        var n = ORRouteManager.getActiveRoute();
        return ORAccMain.userIDR.indexOf("," + n.getId() + ",") > -1 ? (ORMain.showAlertInlineOR(RATING_ALREADY_DONE),
        !1) : ORAccMain.userIDS.indexOf("," + n.getId() + ",") > -1 ? (ORMain.showAlertInlineOR(RATING_NOT_POSSIBLE),
        !1) : (ORAccMain.userIDR += e + ",",
        void new Ajax.Request("route-data/routeRate.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                lr: ORAccMain.userIDR,
                rid: e,
                ra: t
            },
            onSuccess: function(e) {
                if ("ko" == e.responseText)
                    new Effect.Appear("err-af");
                else {
                    var t = e.responseText.split(";")
                      , n = "(0 " + RATING_NB_VOTES + "; 0)"
                      , a = 0;
                    0 != t[1] && (a = t[0] / t[1],
                    n = "(" + t[1] + " " + RATING_NB_VOTES + "; " + a.toFixed(1) + ")"),
                    ORAccMain.deActivateRatingLink(),
                    $("rate-summary") && ($("rate-summary").innerHTML = n);
                    var o = Math.round(a);
                    $("det-fix-rating").addClassName(ORUtils.getRatingStarClassName(Math.round(o)))
                }
            }
        }))
    },
    activateRatingLink: function() {
        if (ORAccMain.isUserConnected()) {
            var e = ORRouteManager.getActiveRoute();
            if (e.getId() > 0)
                if (ORAccMain.userIDS.indexOf("," + e.getId() + ",") > -1)
                    ORAccMain.deActivateRatingLink();
                else if (ORAccMain.userIDR.indexOf("," + e.getId() + ",") > -1)
                    ORAccMain.deActivateRatingLink();
                else if ($("det-fix-rating"))
                    for (var t = $w($("det-fix-rating").className), n = 0, a = t.length; a > n; n++)
                        if ("fix-rating" == t[n]) {
                            $("det-fix-rating").removeClassName(t[n]),
                            $("det-fix-rating").addClassName("rating");
                            break
                        }
        } else
            ORAccMain.deActivateRatingLink()
    },
    deActivateRatingLink: function() {
        if ($("det-fix-rating"))
            for (var e = $w($("det-fix-rating").className), t = 0, n = e.length; n > t; t++)
                if ("rating" == e[t]) {
                    $("det-fix-rating").removeClassName(e[t]),
                    $("det-fix-rating").addClassName("fix-rating");
                    break
                }
    },
    activateUserComLink: function() {
        ORAccMain.isUserConnected() ? $("conuscom") && ($("conuscom").show(),
        $("notconuscom").hide()) : $("notconuscom") && ($("notconuscom").show(),
        $("conuscom").hide())
    },
    deActivateUserComLink: function() {
        $("conuscom") && $("conuscom").hide(),
        $("notconuscom") && $("notconuscom").show()
    },
    userRouteCommentInput: function(e) {
        ORAccMain.isUserConnected() ? Modalbox.show("route-data/userRouteCommentInput.php?idr=" + e, {
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_XSMALL_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "get",
            overlayClose: !1
        }) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_XSMALL_HEIGHT,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    submitUserRouteCommentForm: function() {
        var e = ORAccMain.validUserRouteCommentForm.validate();
        if (e) {
            var t = ORRouteManager.getActiveRoute();
            new Ajax.Request("route-data/userRouteComment.php",{
                method: "post",
                asynchronous: !1,
                parameters: Form.serialize("createusroutecomform"),
                onSuccess: function(e) {
                    var n = e.responseText;
                    "ok" != n ? $("errmsg") && $("errmsg").show() : (ORRouteManager.getActiveRoute().getData().getType() == ORConstants.TYPE_GMAP ? ORGMapToolsManager.reloadRouteDetails(t.getId()) : ORIGNToolsManager.reloadRouteDetails(t.getId()),
                    $("layerCommentFormClose") && ($("layerCommentFormClose").show(),
                    $("layerCommentForm").innerHTML = "<h2>" + YOUR_COMMENT_PUBLISHED + "</h2>"))
                }
            })
        }
    },
    getCheckedBox: function(e) {
        var e = $(e)
          , t = ""
          , n = e.getInputs("checkbox");
        return n.each(function(e) {
            e.checked && "on" != e.value && (t += e.value,
            t += ",")
        }),
        "" != t && (t = t.substring(0, t.length - 1)),
        t
    },
    routeSavedListener: function(e) {
        var t = e.memo.routeId;
        Event.stop(e),
        ORRouteManager.changeActiveRouteId(t),
        ORMain.cgRouteManager.updateIDRoute(t),
        ORRouteManager.getActiveRoute().getData().getType() == ORConstants.TYPE_GMAP ? (ORGMapToolsManager.reloadRouteDetails(t),
        ORGMapToolsManager.isFullScreen() && ORGMapToolsManager.fullScreen()) : (ORIGNToolsManager.reloadRouteDetails(t),
        ORIGNToolsManager.isFullScreen() && ORIGNToolsManager.fullScreen())
    },
    routeUpdatedListener: function(e) {
        var t = e.memo.routeId;
        Event.stop(e),
        ORRouteManager.getActiveRoute().getData().getType() == ORConstants.TYPE_GMAP ? ORGMapToolsManager.reloadRouteDetails(t) : ORIGNToolsManager.reloadRouteDetails(t)
    },
    isUserConnected: function() {
        return ORAccMain.userConnected
    },
    setUserConnected: function(e) {
        ORAccMain.userConnected = e
    },
    isUserOrganizer: function() {
        return ORAccMain.userOrganizer
    },
    setUserOrganizer: function(e) {
        ORAccMain.userOrganizer = e
    },
    isUserPartner: function() {
        return ORAccMain.userPartner
    },
    setUserPartner: function(e) {
        ORAccMain.userPartner = e
    },
    manageConnected: function(e) {
        e && Event.stop(e),
        ORAccMain.isUserConnected() ? ($("myortab").show(),
        ORAccMain.activateFavLink(),
        ORAccMain.activateMessageLink(),
        ORAccMain.activateRatingLink(),
        ORAccMain.activateUserComLink(),
        ORAccMain.activateExportLink(),
        ORAccMain.activateExportPartnerLink(),
        LIMIT_ROUTING_PTS_MAX = LIMIT_ROUTING_PTS_MAX_PARTNER,
        ORMain.refreshUserPref(),
        ORAccMain.activateDrmMaps(),
        ORAccMain.isUserOrganizer() && ORMain.showDirectlyIGN(),
        void 0 == document.getElementById("tester") || (ORAccMain.noads = !1)) : ($("myortab").hide(),
        $("myorcontent") && $("myorcontent").update(),
        LIMIT_ROUTING_PTS_MAX = LIMIT_ROUTING_PTS_MAX_REF),
        ORAccMain.checkRoutesRights()
    },
    checkRoutesRights: function() {
        if (ORAccMain.isUserConnected()) {
            var e = ORRouteManager.getActiveRoute();
            ORAccMain.userIDS.indexOf("," + e.getId() + ",") > -1 ? ($("updatedata").show(),
            $("removedata").show(),
            (1 == ORPOI_ACTIVATION || 1 == ORPOI_PARTNER_ACTIVATION && ORAccMain.isUserPartner()) && $("tools_poiuser").show(),
            ORAccMain.isUserPartner() && $("exp_partner") && $("exp_partner").show()) : ($("updatedata").hide(),
            $("removedata").hide(),
            $("tools_poiuser").hide())
        } else
            $("updatedata").hide(),
            $("removedata").hide(),
            $("tools_poiuser").hide(),
            ORAccMain.deActivateFavLink(),
            ORAccMain.deActivateMessageLink(),
            ORAccMain.deActivateRatingLink(),
            ORAccMain.deActivateUserComLink(),
            ORAccMain.deActivateExportLink(),
            ORAccMain.deActivateExportPartnerLink(),
            ORAccMain.hidePassAccess(),
            ORAccMain.deActivateDrmMaps(),
            ORAccMain.userIDS = ""
    },
    setUserIDS: function(e) {
        ORAccMain.userIDS = e
    },
    removeIDS: function(e) {
        for (var t = e.split(","), n = 0, a = t.length; a > n; n++) {
            var o = "," + t[n] + ",";
            ORAccMain.userIDS = ORAccMain.userIDS.replace(o, ",")
        }
    },
    addIDS: function(e) {
        ORAccMain.userIDS += e + ","
    },
    getIDSNumber: function() {
        var e = ORAccMain.userIDS.split(",")
          , t = e.length - 1;
        return t
    },
    subscribeORS: function(e) {
        Modalbox.show("account/orserviceSubscribeInput.php?cors=" + e, {
            width: 600,
            height: 400,
            title: ORConstants.MODAL_TITLE,
            overlayClose: !1
        })
    },
    submitSubORSForm: function(e) {
        $("errmsg").hide();
        var t = ORAccMain.validSubSerForm.validate();
        return t && (document.forms.subserform.typp.value = e,
        new Ajax.Request("account/orserviceSubscribe.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("subserform"),
            onSuccess: function(e) {
                e.responseText;
                "pp" == e.responseText ? Modalbox.show("account/orserviceSubscribePayP.php", {
                    width: 600,
                    height: 300,
                    title: ORConstants.MODAL_TITLE,
                    method: "post",
                    params: Form.serialize("subserform"),
                    overlayClose: !1
                }) : "pc" == e.responseText ? Modalbox.show("account/orserviceSubscribePayCh.php", {
                    width: 600,
                    height: 300,
                    title: ORConstants.MODAL_TITLE,
                    method: "post",
                    params: Form.serialize("subserform"),
                    overlayClose: !1
                }) : ($("errmsg").innerHTML = e.responseText,
                $("errmsg").show())
            }
        })),
        !1
    },
    checkHostName: function(e) {
        var t = new RegExp("^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])\\.)+([a-zA-Z0-9]{2,5})$");
        return e.match(t) ? !0 : !1
    },
    checkCGA: function(e) {
        e ? $("validcga").disabled = "" : $("validcga").disabled = "disabled"
    },
    payCh: function(e) {
        $("errmsg").hide(),
        new Ajax.Request("account/orserviceSubscribePayCh.php",{
            method: "post",
            asynchronous: !1,
            onSuccess: function(e) {
                e.responseText;
                "ok" == e.responseText || ($("errmsg").innerHTML = e.responseText,
                $("errmsg").show())
            }
        })
    },
    manageDrmError: function(e) {
        if (e) {
            Event.stop(e);
            var t = e.memo.cr
              , n = e.memo.tt;
            switch (t) {
            case "-1":
            case "-2":
            case "-3":
            case "-5":
                ORAccMain.deActivateDrm(n);
                break;
            case "-4":
                ORAccMain.deActivateDrm(n),
                ORMain.showAlertOR(SUBSCRIPTION_QUOTA_REACHED)
            }
        }
    },
    activateDrmLicense: function(typ, id, tk) {
        $("galic_errmsg").hide();
        var lo = "loading_" + id;
        $(id).hide(),
        $(lo).show();
        var ta = "tt:" + typ + ",tk:" + tk;
        new Ajax.Request("account/orUserLac.php",{
            method: "post",
            asynchronous: !1,
            parameters: {
                tae: $j.b64.encode(ta)
            },
            onSuccess: function(transport) {
                var ret = $j.b64.decode(transport.responseText);
                0 > ret ? ($("galic_errmsg").innerHTML = ERROR_CODE + " " + ret,
                $("galic_errmsg").show()) : (ORAccMain.setUserSub(eval(ret)),
                ORAccMain.activateDrmMaps(),
                ORSearchMain.searchMA(),
                setTimeout(function() {
                    $("galic_success").show()
                }, 100))
            }
        })
    }
};
document.observe("or:routesaved", ORAccMain.routeSavedListener),
document.observe("or:routeupdated", ORAccMain.routeUpdatedListener),
document.observe("or:connected", ORAccMain.manageConnected),
document.observe("or:drmerror", ORAccMain.manageDrmError);
var RoutingGCOR = Class.create({
    initialize: function(e, t, n, a, o, i) {
        this.container_id = e,
        this.mode_binding = a,
        this.engine_binding = n,
        this.engineOR = new RoutingEngineOR(t),
        this.ele_cal = i,
        this.aFollowRoadFlg = !1;
        var r = new Element("label",{
            "for": "engine_" + this.container_id
        });
        r.update(ROUTE_CALCULATION_ENGINE),
        $(this.container_id).appendChild(r),
        this.selEngineElement = this.engineOR.renderEngine(this.container_id),
        $(this.container_id).appendChild(this.selEngineElement),
        this.selEngineElement.observe("change", this.onChangeEngine.bind(this));
        var s = new Element("label",{
            "for": "mode_" + this.container_id
        });
        s.update(ROUTE_CALCULATION_MODE),
        $(this.container_id).appendChild(s),
        this.selMode = this.engineOR.renderMode(this.container_id),
        $(this.container_id).appendChild(this.selMode),
        this.selMode.observe("change", this.onChangeMode.bind(this))
    },
    getEngineOR: function() {
        return this.engineOR
    },
    onChangeEngine: function(e) {
        e && Event.stop(e),
        this.engineOR.changeSelectedEngine(this.selEngineElement.value),
        this.selMode.update(this.engineOR.renderMode(this.container_id).innerHTML),
        this.engine_binding && this.engine_binding(parseInt(this.engineOR.getSelectedEngine()))
    },
    onChangeMode: function(e) {
        e && Event.stop(e),
        this.mode_binding && this.mode_binding(parseInt(this.selMode.value))
    },
    onChangeAuto: function(e) {
        return this.aFollowRoadFlg ? this.aFollowRoadFlg = !1 : this.aFollowRoadFlg = !0,
        this.auto_binding && this.auto_binding(this.aFollowRoadFlg),
        !1
    },
    setEngine: function(e) {
        this.engineOR.changeSelectedEngine(e),
        this.selMode.update(this.engineOR.renderMode(this.container_id).innerHTML)
    }
})
  , RoutingEngineOR = Class.create({
    initialize: function(e) {
        this.selectedEngine = -1,
        this.selectedMode = -1,
        this.authorizedEngine = e || [ORConstants.CLOUDMADE_ENGINE_ID],
        this.container_id = -1
    },
    renderEngine: function(e) {
        this.container_id = e;
        for (var t = new Element("select",{
            id: "engine_" + this.container_id,
            "class": "cg-routing-selection"
        }), n = 0; n < this.authorizedEngine.length; n++)
            t.appendChild(new Element("option",{
                "class": "cg-routing-selection",
                value: this.authorizedEngine[n]
            }).update(ORRoutingEnginesMap.getLabel(this.authorizedEngine[n]))),
            -1 == this.selectedEngine && (this.selectedEngine = this.authorizedEngine[n]);
        return t
    },
    renderMode: function(e) {
        for (var t = new Element("select",{
            id: "mode_" + this.container_id,
            "class": "cg-routing-selection"
        }), n = ORRoutingEnginesMap.getModes(this.selectedEngine), a = 0; a < n.length; a++)
            t.appendChild(new Element("option",{
                "class": "cg-routing-selection",
                value: a
            }).update(n[a])),
            -1 == this.selectedMode && (this.selectedMode = a);
        return t
    },
    changeSelectedEngine: function(e) {
        this.selectedEngine = e;
        var t = "engine_" + this.container_id;
        $(t).select("[value=" + e + "]");
        var n = $(t).getElementsByTagName("option")
          , a = 0;
        $A(n).each(function(t) {
            t.value == e ? t.selected = "selected" : a++
        })
    },
    changeSelectedMode: function(e) {
        this.selectedMode = e;
        var t = "mode_" + this.container_id;
        $(t).select("[value=" + e + "]");
        var n = $(t).getElementsByTagName("option")
          , a = 0;
        $A(n).each(function(t) {
            t.value == e ? t.selected = "selected" : a++
        })
    },
    getSelectedEngine: function() {
        return this.selectedEngine
    },
    getSelectedMode: function() {
        return this.selectedMode
    }
})
  , RouteManagerGC = Class.create({
    initialize: function(e, t, n, a, o) {
        this.container_id = e,
        this.manager_create_binding = t,
        this.manager_remove_binding = n,
        this.manager_change_binding = a,
        this.manager_change_settings = o,
        this.main_id = "routegc_main",
        this.routeMap = new Hash,
        this.selectId = null,
        this.a = new Element("div",{
            id: "route-wrapper"
        });
        var i = new Element("ul",{
            id: this.main_id
        });
        this.a.appendChild(i),
        $(this.container_id).appendChild(this.a);
        var r = new Element("div",{
            "class": "add-rem-route"
        })
          , s = new Element("div",{
            id: this.main_id + "_add",
            "class": "sprite-tools add-route",
            title: GCOR_ADD_ROUTE
        })
          , l = new Element("div",{
            "class": "sprite-tools rem-route",
            title: GCOR_REM_ROUTE
        });
        r.appendChild(s),
        r.appendChild(l);
        var c = new Element("br",{
            "class": "clear"
        });
        r.appendChild(c),
        $(this.container_id).appendChild(r),
        s.observe("click", this.onClickAdd.bind(this)),
        l.observe("click", this.onClickRemove.bind(this)),
        $("route-wrapper").addClassName("rgc-overflow-hide")
    },
    addRoute: function(e, t, n, a, o) {
        $("layer_props").hide();
        var i = new RouteGC(e,t,n,a,o);
        $(this.main_id).appendChild(i.renderRoute()),
        i.renderRouteProperties($("layer_props_content"));
        var r = (this.manager_change_settings,
        "routegc_" + e);
        this.routeMap.set(r, i),
        $(r).className = "icone cg-routemg-route-active",
        $(r).observe("click", this.onClickRoute.bind(this)),
        $(r).observe("dbclick", this.onRightClickRoute.bind(this)),
        this.changeActiveRoute(r),
        this.changeColor(t),
        this.routeMap.size() > 5 && ($("route-wrapper").addClassName("rgc-overflow-show"),
        $("route-wrapper").removeClassName("rgc-overflow-hide"))
    },
    initRoute: function(e, t, n, a, o) {
        $("layer_props").hide();
        var i = new RouteGC(e,t,n,a,o);
        $(this.main_id).appendChild(i.renderRoute()),
        i.renderRouteProperties($("layer_props_content"));
        var r = (this.manager_change_settings,
        "routegc_" + e);
        this.routeMap.set(r, i),
        $(r).className = "icone cg-routemg-route-active",
        $(r).observe("click", this.onClickRoute.bind(this)),
        $(r).observe("dbclick", this.onRightClickRoute.bind(this)),
        this.setActiveRoute(e),
        this.changeColor(t)
    },
    updateDistance: function(e, t) {
        var n = "routegc_" + e + "_dis";
        $(n).update(t)
    },
    updateIDRoute: function(e) {
        var t = "routegc_" + this.selectId
          , n = t + "_id"
          , a = t + "_dis"
          , o = t + "_col"
          , i = "routegc_" + e
          , r = i + "_id"
          , s = i + "_dis"
          , l = i + "_col";
        $(t).id = i,
        $(n).id = r,
        $(a).id = s,
        $(o).id = l,
        $(r).update("ID:" + e);
        var c = this.routeMap.unset(t);
        c.setId(e),
        this.routeMap.set(i, c),
        this.setActiveRoute(e)
    },
    removeRoute: function(e) {
        var t = "routegc_" + e;
        $(t).remove(),
        this.selectId = null,
        this.routeMap.unset(t),
        this.routeMap.size() <= 5 && ($("route-wrapper").removeClassName("rgc-overflow-show"),
        $("route-wrapper").addClassName("rgc-overflow-hide"),
        $j("#route-wrapper").scrollLeft(0))
    },
    onClickAdd: function(e) {
        e && Event.stop(e),
        this.manager_create_binding && this.manager_create_binding()
    },
    onClickRemove: function(e) {
        e && Event.stop(e),
        this.manager_remove_binding && this.manager_remove_binding()
    },
    onClickRoute: function(e) {
        var t = null;
        if (e && (e.currentTarget ? t = e.currentTarget.id : e.srcElement && (t = e.srcElement.id,
        t = t.replace("_id", ""),
        t = t.replace("_col", ""),
        t = t.replace("_dis", ""),
        t = t.replace("_uni", "")),
        Event.stop(e)),
        null != t) {
            var n = "routegc_" + this.selectId;
            if (t == n) {
                var a = this.routeMap.get(n);
                a.renderRouteProperties($("layer_props_content")),
                $("layer_props").show();
                var o = ($(n).select('[class="cg-routemg-route-bottom-rc"]'),
                this.manager_change_settings)
                  , i = this.routeMap.get(n);
                $j("#routegc_color_props").ColorPicker({
                    flat: !0,
                    onChange: function(e, t, n) {
                        o && o({
                            color: "#" + t
                        })
                    },
                    onHide: function() {}
                }),
                $j("#routegc_color_props").ColorPickerSetColor(i.getColor()),
                $j("#routegc_color_props").ColorPickerShow();
                var r = $j("#so_slider").slider({
                    min: .1,
                    max: 1,
                    step: .1,
                    value: i.getOpacity(),
                    slide: function(e, t) {
                        $j("#so").html(100 * t.value + "%"),
                        o({
                            opacity: t.value.toFixed(2)
                        })
                    },
                    change: function(e, t) {
                        i.setOpacity(parseFloat(t.value.toFixed(2)))
                    }
                });
                r.slider("option", "value", i.getOpacity()),
                $j("#so").html(100 * i.getOpacity() + "%");
                var s = $j("#sw_slider").slider({
                    min: 1,
                    max: 12,
                    step: 1,
                    value: i.getWidth(),
                    slide: function(e, t) {
                        $j("#sw").html(t.value.toFixed(0) + "px"),
                        o({
                            width: t.value.toFixed(0)
                        })
                    },
                    change: function(e, t) {
                        i.setWidth(parseInt(t.value.toFixed(0), 10))
                    }
                });
                s.slider("option", "value", i.getWidth()),
                $j("#sw").html(i.getWidth() + "px")
            } else
                this.changeActiveRoute(t)
        }
    },
    onRightClickRoute: function(e) {
        var t = null;
        if (e && (e.currentTarget ? t = e.currentTarget.id : e.srcElement && (t = e.srcElement.id,
        t = t.replace("_id", ""),
        t = t.replace("_col", ""),
        t = t.replace("_dis", ""),
        t = t.replace("_uni", "")),
        Event.stop(e)),
        null != t) {
            var n = "routegc_" + this.selectId;
            t == n ? $("layer_props").update(r.renderRoute()) : this.changeActiveRoute(t)
        }
    },
    changeColor: function(e) {
        var t = "routegc_" + this.selectId
          , n = this.routeMap.get(t)
          , a = t + "_col";
        n.setColor(e),
        $(a).setStyle({
            backgroundColor: e
        })
    },
    changeActiveRoute: function(e) {
        if ($("layer_props").hide(),
        $(e).className = "icone cg-routemg-route-active",
        null != this.selectId) {
            var t = "routegc_" + this.selectId;
            $(t).className = "icone cg-routemg-route"
        }
        this.selectId = this.routeMap.get(e).getId(),
        this.manager_change_binding && this.manager_change_binding(this.selectId)
    },
    setActiveRoute: function(e) {
        var t = "routegc_" + e;
        $(t).className = "icone cg-routemg-route-active",
        this.selectId = e
    },
    setManagerCreateBinding: function(e) {
        this.manager_create_binding = e
    },
    setManagerRemoveBinding: function(e) {
        this.manager_remove_binding = e
    },
    setManagerChangeBinding: function(e) {
        this.manager_change_binding = e
    },
    setManagerChangeSettings: function(e) {
        this.manager_change_settings = e
    },
    changeUnit: function(e) {
        for (var t = this.routeMap.values(), n = 0; n < t.length; n++)
            t[n].changeUnit(e)
    }
})
  , RouteGC = Class.create({
    initialize: function(e, t, n, a, o) {
        this.id = e || -1,
        this.color = t,
        this.opacity = n,
        this.width = a,
        this.unit = o
    },
    renderRoute: function() {
        var e = new Element("li",{
            id: "routegc_" + this.id,
            "class": "icone cg-routemg-route",
            title: GCOR_SHOW_PROPERTIES
        })
          , t = new Element("div",{
            "class": "cg-routemg-color-id"
        })
          , n = new Element("div",{
            id: "routegc_" + this.id + "_col",
            "class": "cg-routemg-color"
        }).update("&nbsp;")
          , a = new Element("div",{
            id: "routegc_" + this.id + "_id",
            "class": "cg-routemg-id"
        }).update("ID:" + this.id);
        t.appendChild(n),
        t.appendChild(a),
        e.appendChild(t);
        var o = new Element("div",{
            "class": "cg-routemg-disunit"
        })
          , i = new Element("div",{
            id: "routegc_" + this.id + "_dis",
            "class": "cg-routemg-distance"
        }).update("0.000")
          , r = new Element("div",{
            id: "routegc_" + this.id + "_uni",
            "class": "cg-routemg-unit"
        }).update(this.unit.substr(0, 2));
        o.appendChild(i),
        o.appendChild(r);
        var s = new Element("br",{
            "class": "clear"
        });
        return o.appendChild(s),
        e.appendChild(o),
        e
    },
    getId: function() {
        return this.id
    },
    setId: function(e) {
        this.id = e
    },
    getColor: function() {
        return this.color
    },
    setColor: function(e) {
        this.color = e
    },
    getOpacity: function() {
        return this.opacity
    },
    setOpacity: function(e) {
        this.opacity = e
    },
    getWidth: function() {
        return this.width
    },
    setWidth: function(e) {
        this.width = e
    },
    changeUnit: function(e) {
        this.unit = e;
        var t = "routegc_" + this.id + "_uni";
        $(t).update(this.unit.substr(0, 2))
    },
    renderRouteProperties: function(e) {
        e && e.update("");
        var t = new Element("div",{
            id: "routegc_color_props"
        });
        e.appendChild(t);
        var n = new Element("div",{
            "class": "or-slider slider-props"
        })
          , a = new Element("label",{
            "for": "so_slider"
        }).update(GCOR_PROPERTIES_OPACITY)
          , o = new Element("div",{
            id: "so_slider"
        })
          , i = new Element("div",{
            id: "so",
            "class": "slider-val"
        });
        n.appendChild(a),
        n.appendChild(o),
        n.appendChild(i),
        e.appendChild(n);
        var r = new Element("div",{
            "class": "or-slider slider-props"
        })
          , s = new Element("label").update(GCOR_PROPERTIES_WIDTH)
          , l = new Element("div",{
            id: "sw_slider"
        })
          , c = new Element("div",{
            id: "sw",
            "class": "slider-val"
        });
        r.appendChild(s),
        r.appendChild(l),
        r.appendChild(c),
        e.appendChild(r);
        var u = new Element("br",{
            "class": "clear"
        });
        e.appendChild(u)
    }
})
  , ORIconCatalog = {
    iconCatalog: new Hash,
    loaded: !1,
    load: function() {
        ORIconCatalog.loaded || new Ajax.Request("orpoi-user/loadIcon.php",{
            method: "post",
            asynchronous: !1,
            onSuccess: function(e) {
                if ("ko" == e.responseText)
                    ;
                else {
                    for (var t = e.responseText.evalJSON(!0), n = 0; n < t.length; n++) {
                        for (var a = (new Hash,
                        t[n]), o = new ORIconCategory(a.category_cod,a.category_title), i = a.iconlist, r = 0, s = i.length; s > r; r++) {
                            var l = i[r]
                              , c = new ORIcon(l.idicon,l.sizeW,l.sizeH,l.anchorX,l.anchorY,l.iconUrlA,l.iconUrlI,l.desc,l.descriptable);
                            o.addIcon(c)
                        }
                        ORIconCatalog.iconCatalog.set(a.category_cod, o)
                    }
                    ORIconCatalog.loaded = !0
                }
            }
        })
    },
    getIconById: function(e) {
        for (var t = ORIconCatalog.iconCatalog.values(), n = 0; n < t.length; n++)
            for (var a = t[n].getIconList(), o = 0; o < a.length; o++) {
                var i = a[o];
                if (i.getId() == e)
                    return i
            }
    },
    getIconCatalogAsArray: function() {
        return ORIconCatalog.iconCatalog.keys()
    },
    getIconCatalog: function() {
        return ORIconCatalog.iconCatalog
    }
}
  , ORPOIUserManager = {
    htmlDivManager: "layer_poiuser",
    htmlDivPOICat: "layer_poiuser_category",
    htmlDivPOIList: "layer_poiuser_list",
    htmlDivPOIDet: "layer_poiuser_details",
    suffixeIMG: "_img",
    bindFreezePOI: null,
    bindNameRemovePOI: null,
    bindNameDisPopUpPOI: null,
    selectedIcon: null,
    activePOIID: null,
    flgOc: !1,
    hashIcon: new Hash,
    countid: 0,
    catalogLoaded: !1,
    initialize: function(e, t, n) {
        ORPOIUserManager.bindFreezePOI = e,
        ORPOIUserManager.bindNameRemovePOI = t,
        ORPOIUserManager.bindNameDisPopUpPOI = n
    },
    createPOICatalog: function() {
        if (!ORPOIUserManager.catalogLoaded) {
            ORPOIUserManager.catalogLoaded = !0;
            var e = ORIconCatalog.getIconCatalogAsArray()
              , t = new Element("div",{
                "class": "or-form poi-cat-container"
            })
              , n = new Element("select");
            $(n).observe("change", function(e) {
                Event.stop(e),
                ORPOIUserManager.changePoiCategoryList(this.options[this.selectedIndex].value)
            });
            for (var a = 0; a < e.length; a++) {
                var o = ORIconCatalog.getIconCatalog().get(e[a])
                  , i = new Element("option",{
                    value: o.getId()
                }).update(o.getTitle());
                n.appendChild(i)
            }
            t.appendChild(n),
            $(ORPOIUserManager.htmlDivPOICat).appendChild(t),
            ORPOIUserManager.changePoiCategoryList(e[0])
        }
    },
    changePoiCategoryList: function(e) {
        ORPOIUserManager.setSelectedIcon(null),
        $(ORPOIUserManager.htmlDivPOIList).innerHTML = "";
        for (var t = ORIconCatalog.getIconCatalog().get(e), n = t.getIconList(), a = 0; a < n.length; a++) {
            var o = n[a]
              , i = o.getDomId()
              , r = o.getIconUrlI()
              , s = (o.getIconUrlA(),
            o.getDesc())
              , l = new Element("div",{
                id: i,
                "class": "poiuser-entity"
            })
              , c = i + "_img";
            l.appendChild(new Element("img",{
                id: c,
                src: r,
                alt: s,
                title: s
            })),
            ORPOIUserManager.hashIcon.set(i, {
                idm: c,
                ico: o
            }),
            $(ORPOIUserManager.htmlDivPOIList).appendChild(l),
            $(i).observe("click", function(e) {
                Event.stop(e),
                ORPOIUserManager.setSelectedIcon(this.id)
            }),
            0 == a && ORPOIUserManager.setSelectedIcon(i)
        }
    },
    setBindFreezePOI: function(e) {
        ORPOIUserManager.bindFreezePOI = e
    },
    setBindNameRemovePOI: function(e) {
        ORPOIUserManager.bindNameRemovePOI = e
    },
    setBindNameDisPopUpPOI: function(e) {
        ORPOIUserManager.bindNameDisPopUpPOI = e
    },
    setActivePOIID: function(e, t, n, a) {
        ORPOIUserManager.activePOIID = e,
        null != e ? ($(ORPOIUserManager.htmlDivPOIDet).innerHTML = "<div style='margin-bottom:3px;'><img onclick='javascript:ORGMapDecoratorManager.displayPoiUserEdPopUp(" + e + ");' style='float:left;cursor:pointer' src='" + n + "'/><div style='float:left'><div class='lib'>" + LONGITUDE + ":</div><div class='val' id='poi_udet_lng'>" + t.lng().toFixed(5) + "</div><span>°</span><br/><div class='lib'>" + LATITUDE + ":</div><div id='poi_udet_lat' class='val'>" + t.lat().toFixed(5) + "</div><span>°</span><br class='clear'/></div><br class='clear'></div>",
        $(ORPOIUserManager.htmlDivPOIDet).show()) : ($(ORPOIUserManager.htmlDivPOIDet).innerHTML = "",
        $(ORPOIUserManager.htmlDivPOIDet).hide())
    },
    getActivePOIID: function() {
        return ORPOIUserManager.activePOIID
    },
    setSelectedIcon: function(e) {
        if (null != ORPOIUserManager.selectedIcon) {
            var t = ORPOIUserManager.hashIcon.get(ORPOIUserManager.selectedIcon);
            $(t.idm).src = t.ico.getIconUrlI()
        }
        if (ORPOIUserManager.selectedIcon = e,
        null != e) {
            var n = ORPOIUserManager.hashIcon.get(ORPOIUserManager.selectedIcon);
            $(n.idm).src = n.ico.getIconUrlA()
        }
    },
    getSelectedIcon: function() {
        return null != ORPOIUserManager.selectedIcon ? ORPOIUserManager.hashIcon.get(ORPOIUserManager.selectedIcon) : null
    },
    showHideEditor: function(e) {
        if (ORPOIUserManager.flgOc || e)
            ORPOIUserManager.flgOc = !1,
            $(ORPOIUserManager.htmlDivManager).hide(),
            ORPOIUserManager.bindFreezePOI();
        else {
            ORPOIUserManager.createPOICatalog(),
            ORPOIUserManager.flgOc = !0;
            var t = "#" + ORPOIUserManager.htmlDivManager;
            $j(t).draggable({
                handle: t + "_handle",
                containment: "document"
            }),
            $(ORPOIUserManager.htmlDivManager).show()
        }
        return ORPOIUserManager.flgOc
    },
    isEditorOpen: function() {
        return ORPOIUserManager.flgOc
    },
    getNextPOIID: function() {
        return ++ORPOIUserManager.countid
    }
}
  , ORInfoPopUpFactory = function() {
    return {
        createEditableContent: function(e) {
            var t = ORRouteManager.findPOIUserInRoutes(e)
              , n = "poi_in"
              , a = "poi_ta"
              , o = t.getORMarker().getORIcon().getIconUrlI()
              , i = '<div class="poi-w-header"><img src="' + o + '"><span class="poi-popup-shortdesc"><input id="' + n + '" maxsize="25" value="' + (t.getUserShortDescription().replace(/"/g, "&quot;") || t.getORMarker().getORIcon().getDesc()) + '"></span><br class="clear"/></div>';
            i += '<div class="poi-popup-longdesc"><textarea id="' + a + '">' + t.getUserDescription() + "</textarea></div>",
            i += "<a style='font-weight:bold;font-family: Arial,Helvetica,sans-serif;font-size: 11px;' href='' onclick='javascript:" + ORPOIUserManager.bindNameRemovePOI + "(" + e + ");return false;'>Supprimer ce POI</a>",
            $("poipopup_ed").update(i);
            var r = "";
            if (ORAccMain.isUserPartner() && ORRouteManager.getActiveRoute().getDetails().isRoutePartner()) {
                var s = "poi_gl";
                r += '<label for="' + s + '">POI Global</label>',
                r += '	<input  id="' + s + '" type="checkbox" name="' + s + '" ' + (t.isGlobal() ? ORConstants.CHECKED : "") + "/>",
                r += '	<br class="clear"/>'
            }
            var l = "poi_ex_se";
            r += '<label for="' + l + '">' + POI_EXPORTABLE + " ser</label>",
            r += '	<input id="' + l + '" type="checkbox" name="' + l + '" ' + (t.isExportableSer() ? ORConstants.CHECKED : "") + "/>",
            r += '	<br class="clear"/>';
            var c = "poi_ex_fi";
            if (r += '<label for="' + c + '"">' + POI_EXPORTABLE + " files</label>",
            r += '	<input id="' + c + '" type="checkbox" id="' + c + '" ' + (t.isExportableFiles() ? ORConstants.CHECKED : "") + "/>",
            r += '	<br class="clear"/>',
            ORAccMain.isUserPartner() && ORRouteManager.getActiveRoute().getDetails().isRoutePartner()) {
                var u = "poi_vi_pr";
                r += "<b>Visualisation sur profil alimétrique</b><br/>",
                r += '	<input  type="radio" id="' + u + '_m1" name="' + u + '" ' + ("-1" == t.getElevationDisplayMode() ? ORConstants.CHECKED : "") + ' value="-1"/><span>Non</span>&nbsp;',
                r += '	<input  type="radio" id="' + u + '_0" name="' + u + '" ' + ("0" == t.getElevationDisplayMode() ? ORConstants.CHECKED : "") + ' value="0"/><span>POI + Texte</span>&nbsp;',
                r += '	<input  type="radio" id="' + u + '_1" name="' + u + '" ' + ("1" == t.getElevationDisplayMode() ? ORConstants.CHECKED : "") + ' value="1"/><span>POI seul</span>&nbsp;',
                r += '	<input  type="radio" id="' + u + '_2" name="' + u + '" ' + ("2" == t.getElevationDisplayMode() ? ORConstants.CHECKED : "") + ' value="2"/><span>Texte seul</span>',
                r += '	<br class="clear"/>';
                var g = "poi_or_pr";
                r += '<label for="' + g + '">Empilement/Orientation horizontal [défaut vertical]</label>',
                r += '	<input id="' + g + '" type="checkbox" name="' + g + '" ' + (t.isElevationDisplayHori() ? ORConstants.CHECKED : "") + "/>",
                r += '	<br class="clear"/>'
            }
            r += "<b>Positionnement sur le parcours</b><br/>";
            var p = "poi_pos_km";
            r += '<label for="' + p + '">' + POI_LOCATION + "</label>",
            r += '<input  style="width:50px" type="text" id="' + p + '" maxsize="7" size="5" name="' + p + '" value="' + t.getPosKm() + '"/><a class="or-button or-button-small" style="font-family:Arial; !important;font-size:11px; !important;font-weight:bold; !important" href="" onclick="javascript:ORInfoPopUpFactory.setPOILatLngFromPos(' + e + ",'" + p + "');return false;\">Positionner</a>",
            r += '	<br class="clear"/>';
            var d = "poi_pos_gr_l"
              , R = "poi_pos_gr_t"
              , M = "poi_pos_gr_r"
              , O = "poi_pos_gr_b"
              , h = t.getGridPosition();
            r += '<label for="' + d + '">Position sur la Grille</label>',
            r += '	gauche&nbsp;<input id="' + d + '" type="checkbox" id="' + d + '" ' + ("1" == h.charAt(0) ? ORConstants.CHECKED : "") + "/>",
            r += '	haut&nbsp;<input id="' + R + '" type="checkbox" id="' + R + '" ' + ("1" == h.charAt(1) ? ORConstants.CHECKED : "") + "/>",
            r += '	<br class="clear"/>',
            r += "<label>&nbsp;</label>",
            r += '	droite&nbsp;<input id="' + M + '" type="checkbox" id="' + M + '" ' + ("1" == h.charAt(2) ? ORConstants.CHECKED : "") + "/>",
            r += '	bas&nbsp;<input id="' + O + '" type="checkbox" id="' + O + '" ' + ("1" == h.charAt(3) ? ORConstants.CHECKED : "") + "/>",
            r += '	<br class="clear"/>';
            var m = "poi_dis_mode";
            r += '<label for="' + m + "\"><b>Type d'affichage</b></label><br/>",
            r += '	<input  type="radio" id="' + m + '_0" name="' + m + '" ' + ("0" == t.getDisplayMode() ? ORConstants.CHECKED : "") + ' value="0"/><span>Standard (Pop-up)</span><br/>',
            r += '	<input  type="radio" id="' + m + '_1" name="' + m + '" ' + ("1" == t.getDisplayMode() ? ORConstants.CHECKED : "") + ' value="1"/><span>Etiquette droite</span>&nbsp;<input  type="radio" id="' + m + '_2" name="' + m + '" ' + ("2" == t.getDisplayMode() ? ORConstants.CHECKED : "") + ' value="2"/><span>Etiquette gauche</span><br/>',
            r += '	<input  type="radio" id="' + m + '_3" name="' + m + '" ' + ("3" == t.getDisplayMode() ? ORConstants.CHECKED : "") + ' value="3"/><span>Icône seule</span>',
            r += '	<br class="clear"/>',
            $("poipopup_opt").update(r),
            $(n).stopObserving(),
            $(n).observe("keyup", function(e) {
                t.setUserShortDescription($(e.target.id).value),
                document.fire("or:poiuser-modif")
            });
            var v = "[id^=" + m + "_]";
            if ($$(v).invoke("stopObserving"),
            $$(v).invoke("on", "change", function(e) {
                t.setDisplayMode($(e.target.id).value),
                document.fire("or:poiuser-modif")
            }),
            $(l).stopObserving(),
            $(l).observe("click", function(e) {
                t.setExportableSer($(e.target.id).checked ? "1" : "0"),
                document.fire("or:poiuser-modif")
            }),
            $(c).stopObserving(),
            $(c).observe("click", function(e) {
                t.setExportableFiles($(e.target.id).checked ? "1" : "0"),
                document.fire("or:poiuser-modif")
            }),
            $(p).stopObserving(),
            $(p).observe("keyup", function(e) {
                var n = $(e.target.id).value;
                t.setPosKm(n),
                document.fire("or:poiuser-modif")
            }),
            $(d).stopObserving(),
            $(d).observe("click", function(e) {
                var n = $(d).checked ? "1" : "0";
                n += $(R).checked ? "1" : "0",
                n += $(M).checked ? "1" : "0",
                n += $(O).checked ? "1" : "0",
                t.setGridPosition(n),
                document.fire("or:poiuser-modif")
            }),
            $(R).stopObserving(),
            $(R).observe("click", function(e) {
                var n = $(d).checked ? "1" : "0";
                n += $(R).checked ? "1" : "0",
                n += $(M).checked ? "1" : "0",
                n += $(O).checked ? "1" : "0",
                t.setGridPosition(n),
                document.fire("or:poiuser-modif")
            }),
            $(M).stopObserving(),
            $(M).observe("click", function(e) {
                var n = $(d).checked ? "1" : "0";
                n += $(R).checked ? "1" : "0",
                n += $(M).checked ? "1" : "0",
                n += $(O).checked ? "1" : "0",
                t.setGridPosition(n),
                document.fire("or:poiuser-modif")
            }),
            $(O).stopObserving(),
            $(O).observe("click", function(e) {
                var n = $(d).checked ? "1" : "0";
                n += $(R).checked ? "1" : "0",
                n += $(M).checked ? "1" : "0",
                n += $(O).checked ? "1" : "0",
                t.setGridPosition(n),
                document.fire("or:poiuser-modif")
            }),
            ORAccMain.isUserPartner() && ORRouteManager.getActiveRoute().getDetails().isRoutePartner()) {
                $(s).stopObserving(),
                $(s).observe("click", function(e) {
                    t.setGlobal($(e.target.id).checked ? "1" : "0"),
                    document.fire("or:poiuser-modif")
                });
                var f = "[id^=" + u + "_]";
                $$(f).invoke("stopObserving"),
                $$(f).invoke("on", "change", function(e) {
                    t.setElevationDisplayMode($(e.target.id).value),
                    document.fire("or:poiuser-modif")
                }),
                $(g).stopObserving(),
                $(g).observe("click", function(e) {
                    t.setElevationDisplayHori($(e.target.id).checked ? "1" : "0"),
                    document.fire("or:poiuser-modif")
                })
            }
            setTimeout(function() {
                document.stopObserving("or:tab-poi-desc"),
                document.stopObserving("or:tab-poi-opt"),
                document.stopObserving("or:tab-poi-mod");
                var e = new TabManager
                  , n = $("tab_poi_editor").getElementsBySelector("li a");
                n.each(function(t) {
                    "#poipopup_ed" == t.hash ? e.addTab(new Tab(t,!1,!1,"or:tab-poi-desc",function() {}
                    )) : "#poipopup_opt" == t.hash ? e.addTab(new Tab(t,!1,!1,"or:tab-poi-opt",function() {}
                    )) : "#poipopup_mod" == t.hash && e.addTab(new Tab(t,!1,!1,"or:tab-poi-mod",function() {}
                    ))
                }),
                document.observe("or:tab-poi-desc", function(t) {
                    if (t) {
                        var n = t.memo.bind;
                        e.setActiveTab(n),
                        Event.stop(t)
                    }
                }),
                document.observe("or:tab-poi-opt", function(t) {
                    if (t) {
                        var n = t.memo.bind;
                        e.setActiveTab(n),
                        Event.stop(t)
                    }
                }),
                document.observe("or:tab-poi-mod", function(n) {
                    if (n) {
                        var a = n.memo.bind;
                        e.setActiveTab(a),
                        Event.stop(n),
                        ORInfoPopUpFactory.createPOIGrid(t, t.getORMarker().getORIcon().getId())
                    }
                }),
                e.setActiveTab("poipopup_ed"),
                CKEDITOR.instances[a] && CKEDITOR.remove(CKEDITOR.instances[a]),
                CKEDITOR.replace(a, {
                    language: LANG,
                    toolbar: "PartnerOR",
                    filebrowserUploadUrl: "../upload-img/uploadImgPoi.php",
                    width: 320,
                    height: 150
                }),
                CKEDITOR.instances[a].on("change", function(e) {
                    t.setUserDescription(CKEDITOR.instances[a].getData()),
                    document.fire("or:poiuser-modif")
                })
            }, 150)
        },
        createReadonlyContent: function(e) {
            var t = ORRouteManager.findPOIUserInRoutes(e)
              , n = new Element("div")
              , a = new Element("div",{
                "class": "poi-r-header"
            })
              , o = t.getORMarker().getORIcon().getIconUrlI();
            a.appendChild(new Element("img",{
                src: o
            }));
            var i = new Element("span");
            t.getPoiId() + "_in";
            i.update(t.getUserShortDescription()),
            a.appendChild(i);
            var r = new Element("br",{
                "class": "clear"
            });
            a.appendChild(r);
            var s = new Element("div",{
                "class": "poi-r-content"
            });
            if ("" != t.getUserDescription()) {
                t.getPoiId() + "_ta";
                s.update(t.getUserDescription())
            }
            n.appendChild(a),
            n.appendChild(s),
            $("poipopup_ro").update(n.innerHTML)
        },
        createPOIGrid: function(e, t) {
            $("poipopup_mod") && ($("poipopup_mod").innerHTML = "");
            for (var n = ORIconCatalog.getIconCatalog().values(), a = 0; a < n.length; a++)
                for (var o = n[a].getIconList(), i = 0; i < o.length; i++) {
                    var r = o[i]
                      , s = r.getId()
                      , l = r.getIconUrlI()
                      , c = r.getIconUrlA()
                      , u = r.getDesc()
                      , g = s + "_ch"
                      , p = new Element("div",{
                        id: g,
                        "class": "poiuser-entity"
                    })
                      , d = g + "_img"
                      , R = ORConstants.ICON_PREFIX_ID + t;
                    R == r.getDomId() ? p.appendChild(new Element("img",{
                        id: d,
                        src: c,
                        alt: u,
                        title: u
                    })) : p.appendChild(new Element("img",{
                        id: d,
                        src: l,
                        alt: u,
                        title: u
                    })),
                    $("poipopup_mod").appendChild(p),
                    $(g).stopObserving("click"),
                    $(g).observe("click", function(t) {
                        Event.stop(t);
                        var n = this.id.substr(0, this.id.length - 3)
                          , a = ORIconCatalog.getIconById(n)
                          , o = e.getORMarker().getORIcon().getDomId();
                        o = o.substr(3, o.length) + "_ch_img",
                        $(o).src = $(o).src.replace("-a.png", "-i.png"),
                        e.getORMarker().setORIcon(a);
                        var i = a.getDomId();
                        i = i.substr(3, i.length) + "_ch_img",
                        $(i).src = $(i).src.replace("-i.png", "-a.png"),
                        e.getORMarker().getApiMarker().setMap(null),
                        e.getORMarker().getApiMarker().getIcon().url = e.getORMarker().getORIcon().getIconUrlA(),
                        e.getORMarker().getApiMarker().setMap(ORMain.map),
                        document.fire("or:poiuser-modif")
                    })
                }
        },
        setPOILatLngFromPos: function(e, t) {
            var n = $(t).value
              , a = n.replace(",", ".");
            if (isNaN(a))
                return $(t).value = 0,
                void ORMain.showAlertOR(VALIDATE_NUMBER);
            if (parseFloat(a) < 0 || parseFloat(a) > ORRouteManager.getActiveRoute().getDistance())
                return void ORMain.showAlertOR(VALIDATE_POI_POSITION);
            var o = ORUtils.findPoiGeographicPosition(parseFloat(a), ORMain.unit)
              , i = ORRouteManager.findPOIUserInRoutes(e)
              , r = i.getORMarker();
            r.setORLatLng(o),
            r.getApiMarker().setPosition(o)
        }
    }
}()
  , ORExpMain = {
    validSer01Form: null,
    validSer02Form: null,
    validSer03Form: null,
    validSer04Form: null,
    validSer05Form: null,
    validSer08Form: null,
    validSer09Form: null,
    submitSer01Form: function() {
        var e = ORExpMain.validSer01Form.validate();
        return $("errmsg").hide(),
        e && ($("expresult").hide(),
        new Ajax.Request("orservice/generateHTMLCodeForSer.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("ser01form"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : (ORExpMain.splitAndShowResult(e.responseText),
                $("urltxt").focus,
                $("urltxt").select())
            }
        })),
        !1
    },
    getSer01Wizzard: function(e) {
        ORAccMain.isUserConnected() ? Modalbox.show("orservice/wizzardSer01.php?rid=" + e, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    submitSer02Form: function() {
        var e = ORExpMain.validSer02Form.validate();
        return $("errmsg").hide(),
        e && ($("expresult").hide(),
        new Ajax.Request("orservice/generateHTMLCodeForSer.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("ser02form"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : (ORExpMain.splitAndShowResult(e.responseText),
                $("urltxt").focus,
                $("urltxt").select())
            }
        })),
        !1
    },
    getSer02Wizzard: function(e) {
        var t = ORRouteManager.getActiveRoute().getDetails();
        ORAccMain.isUserConnected() ? t.isProfProv() ? Modalbox.show("orservice/wizzardSer02.php?rid=" + e, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : ORMain.showAlertOR(EXPORT_ELE_PROFILE_NOT_PROVIDED) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    submitSer03Form: function() {
        $("expresult").hide();
        var e = ORExpMain.validSer03Form.validate();
        return $("errmsg").hide(),
        e && ($("expresult").hide(),
        new Ajax.Request("orservice/generateHTMLCodeForSer.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("ser03form"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : (ORExpMain.splitAndShowResult(e.responseText),
                $("urltxt").focus,
                $("urltxt").select())
            }
        })),
        !1
    },
    getSer03Wizzard: function(e) {
        var t = ORRouteManager.getActiveRoute().getDetails();
        ORAccMain.isUserConnected() ? t.isProfProv() ? Modalbox.show("orservice/wizzardSer03.php?rid=" + e, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : ORMain.showAlertOR(EXPORT_ELE_PROFILE_NOT_PROVIDED) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    submitSer04Form: function() {
        var e = ORExpMain.validSer04Form.validate();
        return $("errmsg").hide(),
        e && ($("expresult").hide(),
        new Ajax.Request("orservice/generateHTMLCodeForSer.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("ser04form"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : (ORExpMain.splitAndShowResult(e.responseText),
                $("urltxt").focus,
                $("urltxt").select())
            }
        })),
        !1
    },
    getSer04Wizzard: function(e) {
        return ORExpMain.checkPublicRoute() ? void (ORAccMain.isUserConnected() ? Modalbox.show("orservice/wizzardSer04.php?rid=" + e, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })) : (ORMain.showAlertOR(ALERT_PRIVATE_ROUTE_SHARE),
        !1)
    },
    submitSer05Form: function() {
        var e = ORExpMain.validSer05Form.validate();
        return $("errmsg").hide(),
        e && ($("expresult").hide(),
        new Ajax.Request("orservice/generateHTMLCodeForSer.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("ser05form"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : (ORExpMain.splitAndShowResult(e.responseText),
                $("urltxt").focus,
                $("urltxt").select())
            }
        })),
        !1
    },
    getSer05Wizzard: function(e) {
        if (!ORExpMain.checkPublicRoute())
            return ORMain.showAlertOR(ALERT_PRIVATE_ROUTE_SHARE),
            !1;
        var t = ORRouteManager.getActiveRoute().getDetails();
        ORAccMain.isUserConnected() ? t.isProfProv() ? Modalbox.show("orservice/wizzardSer05.php?rid=" + e, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : ORMain.showAlertOR(EXPORT_ELE_PROFILE_NOT_PROVIDED) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    submitSer08Form: function() {
        var e = ORExpMain.validSer08Form.validate();
        return $("errmsg").hide(),
        e && ($("expresult").hide(),
        new Ajax.Request("orservice/generateHTMLCodeForSer.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("ser08form"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : (ORExpMain.splitAndShowResult(e.responseText),
                $("urltxt").focus,
                $("urltxt").select())
            }
        })),
        !1
    },
    getSer08Wizzard: function(e) {
        return ORExpMain.checkPublicRoute() ? void (ORAccMain.isUserConnected() ? Modalbox.show("orservice/wizzardSer08.php?rid=" + e, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })) : (ORMain.showAlertOR(ALERT_PRIVATE_ROUTE_SHARE),
        !1)
    },
    submitSer09Form: function() {
        $("expresult").hide();
        var e = ORExpMain.validSer09Form.validate();
        return $("errmsg").hide(),
        e && ($("expresult").hide(),
        new Ajax.Request("orservice/generateHTMLCodeForSer.php",{
            method: "post",
            asynchronous: !1,
            parameters: Form.serialize("ser09form"),
            onSuccess: function(e) {
                "ko" == e.responseText ? new Effect.Appear("errmsg") : (ORExpMain.splitAndShowResult(e.responseText),
                $("urltxt").focus,
                $("urltxt").select())
            }
        })),
        !1
    },
    getSer09Wizzard: function(e) {
        if (!ORExpMain.checkPublicRoute())
            return ORMain.showAlertOR(ALERT_PRIVATE_ROUTE_SHARE),
            !1;
        var t = ORRouteManager.getActiveRoute().getDetails();
        ORAccMain.isUserConnected() ? t.isProfProv() ? Modalbox.show("orservice/wizzardSer09.php?rid=" + e, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : ORMain.showAlertOR(EXPORT_ELE_PROFILE_NOT_PROVIDED) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    splitAndShowResult: function(e) {
        var t = e.split("|||");
        return $("urltxt").value = t[0],
        new Ajax.Request(t[1] + "&nc=1",{
            method: "get",
            asynchronous: !1,
            evalJS: !1,
            onSuccess: function(e) {
                var t = e.responseText
                  , n = t.split("document.write('")
                  , a = n[1].split("');");
                return a[0] = a[0].replace("'+top.location.hostname+'", top.location.hostname),
                $("expresult").show(),
                $("exporturl").innerHTML = a[0],
                !1
            }
        }),
        !1
    },
    initSlider: function() {
        $j("#oc_slider").slider({
            min: .1,
            max: 1,
            step: .1,
            value: 1,
            slide: function(e, t) {
                $j("#oc").html(100 * t.value + "%"),
                $("url_map_oc").value = t.value
            }
        });
        $j("#oc").html("100%"),
        $("url_map_oc").value = 1
    },
    getSerPartnerWizzard: function(e, t) {
        var n = ORRouteManager.getActiveRoute().getDetails();
        ORAccMain.isUserConnected() ? n.isProfProv() ? Modalbox.show("orservice/wizzardSerPartner.php?rid=" + e + "&idp=" + t, {
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            title: ORConstants.MODAL_TITLE,
            method: "post",
            overlayClose: !1
        }) : ORMain.showAlertOR(EXPORT_ELE_PROFILE_NOT_PROVIDED) : Modalbox.show("account/userNotAuthenticated.php", {
            slideDownDuration: "0",
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_LARGE_WIDTH,
            height: ORConstants.MODAL_MEDIUM_HEIGHT,
            overlayClose: !1
        })
    },
    checkPublicRoute: function() {
        var e = ORRouteManager.getActiveRoute().getDetails();
        return e.isPrivate() ? !1 : !0
    }
}
  , ORMainExt = {
    map: null,
    key: null,
    chost: null,
    urlpw: null,
    dbclicse: !1,
    dbcliclo: !1,
    maploaded: !1,
    selectedrow: null,
    selectedrowBG: null,
    poly: null,
    initService: function() {
        $("map").hide(),
        $("ext-ref-main").hide(),
        $("ext-dis-info").hide(),
        ORMainExt.urlpw = top.location.hostname,
        ORMainExt.urlpw != ORMainExt.chost && (ORMainExt.logBadUsage(),
        document.location.href = "bad-externalusage.php")
    },
    logBadUsage: function(e) {
        var t = "bad-externalusage-t.php";
        return new Ajax.Request(t,{
            method: "post",
            parameters: {
                h: ORMainExt.urlpw,
                ch: ORMainExt.chost
            }
        }),
        !1
    },
    searchCertifiedRoutes: function(e, t) {
        return ORMainExt.dbclic ? !1 : (ORMainExt.dbclicse = !0,
        ORMainExt.fillForm(e, t),
        $("loadingsiad").show(),
        new Ajax.Request("searchCertifiedRoutes.php",{
            method: "post",
            parameters: Form.serialize("extform"),
            onSuccess: function(e) {
                "empty" != e.responseText ? ($("ext-results").innerHTML = e.responseText,
                $("map").hide(),
                $("ext-ref-main").hide(),
                $("ext-dis-info").hide(),
                Effect.ScrollTo("ext-results", {
                    duration: "0"
                }),
                ORMainExt.initTable()) : $("ext-results").innerHTML = "Pb technique",
                $("loadingsiad").hide(),
                ORMainExt.dbclicse = !1
            },
            onCreate: function() {},
            onComplete: function() {
                ORMainExt.dbclicse = !1
            }
        }),
        void 0)
    },
    fillForm: function(e, t) {
        $("k").value = ORMainExt.key,
        $("u").value = ORMainExt.urlpw,
        $("c").value = e,
        $("lc").value = t
    },
    setKeys: function(e, t) {
        ORMainExt.key = e,
        ORMainExt.chost = t
    },
    initTable: function() {
        void 0 != $("tabSearchResult") && SortableTable.init("tabSearchResult", {
            tableScroll: SortableTable.options.tableScroll
        })
    },
    showRoute: function(e, t) {
        return ORMainExt.dbcliclo ? !1 : (ORMainExt.lockLoad(),
        void 0 != ORMainExt.selectedrow && (ORMainExt.selectedrow.parentNode.parentNode.style.backgroundColor = ORMainExt.selectedrowBG),
        ORMainExt.selectedrow = t,
        ORMainExt.selectedrowBG = t.parentNode.parentNode.style.backgroundColor,
        t.parentNode.parentNode.style.backgroundColor = "#FFCC33",
        ORMainExt.maploaded || (ORMainExt.maploaded = !0,
        ORMainExt.initGMap()),
        void setTimeout(function() {
            ORMainExt.loadRoute(e)
        }, 10))
    },
    unLockLoad: function() {
        ORMainExt.dbcliclo = !1,
        $("loadinglayer-se").hide()
    },
    lockLoad: function() {
        ORMainExt.dbcliclo = !0,
        $("loadinglayer-se").show()
    },
    loadRoute: function(e) {
        var t = "../route-data/loadData.php"
          , n = null;
        new Ajax.Request(t,{
            method: "post",
            parameters: {
                id: e
            },
            asynchronous: !1,
            onSuccess: function(e) {
                if ("ko" == e.responseText)
                    n = ROUTE_NOT_VALID_OR_PRIVATE;
                else {
                    null != ORMainExt.poly && ORMainExt.poly.setMap(null),
                    $("map").show();
                    var t = e.responseText.evalJSON(!0)
                      , a = t.points;
                    t.routingengine > 0 && (a = t.roupoints);
                    for (var o = decodeLine(a), i = [], r = new google.maps.LatLngBounds, s = 0, l = o.length; l > s; s++) {
                        var c = new google.maps.LatLng(o[s][0],o[s][1]);
                        i.push(c),
                        r.extend(c)
                    }
                    ORMainExt.poly = new google.maps.Polyline({
                        path: i,
                        strokeColor: t.strokecolor,
                        strokeOpacity: t.strokeopacity,
                        strokeWeight: t.strokewidth
                    }),
                    ORMainExt.poly.setMap(ORMainExt.map),
                    ORMainExt.map.fitBounds(r),
                    ORMainExt.map.setCenter(r.getCenter()),
                    ORMainExt.unLockLoad(),
                    $("ext-ref").href = "https://www.openrunner.com/index.php?id=" + t.idroute,
                    $("ext-ref").innerHTML = "www.openrunner.com",
                    $("ext-ref-main").show(),
                    $("distance").innerHTML = t.distance,
                    $("ext-dis-info").show(),
                    Effect.ScrollTo("ext-ref", {
                        duration: "0"
                    })
                }
            }
        }),
        null != n && alert(n)
    },
    initGMap: function() {
        $("map").show();
        var e = {
            zoom: 12,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggableCursor: "crosshair",
            keyboardShortcuts: !0,
            scrollwheel: !0
        };
        ORMainExt.map = new google.maps.Map(document.getElementById("map"),e),
        ORMainExt.map.setOptions({
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.TERRAIN, google.maps.MapTypeId.SATELLITE, google.maps.MapTypeId.HYBRID],
                style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
            }
        }),
        ORMainExt.map.setCenter(new google.maps.LatLng(48.8,2), 13)
    }
}
  , ORConstantsGar = {
    GORSTATUS: "gor-status-",
    GORSDEVICES: "gor-sdevices-",
    GORSACTIVITIES: "gor-sactivities-",
    GORBDEVICES: "gor-bdevices-",
    GORBACTIVITIES: "gor-bactivities-",
    GORBIMPORT: "gor-bimport-",
    GORBEXPORT: "gor-bexport-",
    gorstatus: null,
    gorsdevices: null,
    gorsactivities: null,
    gorbdevices: null,
    gorbactivities: null,
    gorbimport: null,
    gorbexport: null,
    gordatatype: null,
    initConstants: function(e) {
        ORConstantsGar.gorstatus = ORConstantsGar.GORSTATUS + e,
        ORConstantsGar.gorsdevices = ORConstantsGar.GORSDEVICES + e,
        ORConstantsGar.gorsactivities = ORConstantsGar.GORSACTIVITIES + e,
        ORConstantsGar.gorbdevices = ORConstantsGar.GORBDEVICES + e,
        ORConstantsGar.gorbactivities = ORConstantsGar.GORBACTIVITIES + e,
        ORConstantsGar.gorbimport = ORConstantsGar.GORBIMPORT + e,
        ORConstantsGar.gorbexport = ORConstantsGar.GORBEXPORT + e
    }
}
  , ORMainGar = {
    controller: null,
    listener: null,
    init: function(e) {
        var t = "im";
        return 0 != e && (t = "ex",
        $("expgarblo").show()),
        ORMainGar.initController(t, e) && ORMainGar.findDevices(),
        !1
    },
    initController: function(e, t) {
        ORConstantsGar.initConstants(e);
        try {
            ORMainGar.controller = new Garmin.DeviceControl
        } catch (n) {
            return $(ORConstantsGar.gorstatus).innerHTML = n,
            !1
        }
        return ORMainGar.listener = new ORGarListener(t),
        ORMainGar.controller.register(ORMainGar.listener),
        ORMainGar.controller.unlock(["https://www.openrunner.com", "6da47e5a424b6f69797050526f2577d5"]) ? $(ORConstantsGar.gorstatus).innerHTML = GARMIN_UNLOCK_SUCCESS : $(ORConstantsGar.gorstatus).innerHTML = GARMIN_UNLOCK_FAILED,
        !0
    },
    _showButton: function() {
        $(ORConstantsGar.gorbdevices).show()
    },
    findDevices: function() {
        ORMainGar._showButton(),
        ORMainGar.controller.findDevices()
    },
    findActivities: function() {
        var e = ORMainGar.controller.getDevices()[ORMainGar.controller.getDeviceNumber()];
        if (ORMainGar.gordatatype = Garmin.DeviceControl.FILE_TYPES.gpx,
        e.supportDeviceDataTypeRead(Garmin.DeviceControl.FILE_TYPES.tcx)) {
            try {
                ORMainGar.controller.readDataFromDevice(Garmin.DeviceControl.FILE_TYPES.tcx)
            } catch (t) {
                console.log(t)
            }
            ORMainGar.gordatatype = Garmin.DeviceControl.FILE_TYPES.tcx
        } else
            try {
                ORMainGar.controller.readDataFromDevice(Garmin.DeviceControl.FILE_TYPES.gpx)
            } catch (t) {
                try {
                    ORMainGar.controller.readDataFromDevice(Garmin.DeviceControl.FILE_TYPES.tcx)
                } catch (t) {
                    console.log(t)
                }
                ORMainGar.gordatatype = Garmin.DeviceControl.FILE_TYPES.tcx
            }
    },
    importActivity: function() {
        for (var e = ORMainGar.listener.getSelectedActivity(), t = [], n = 0, a = e.getSamplesLength(); a > n; n++)
            try {
                var o = e.findNearestValidLocationSample(n, 0);
                t.push([o.getLatitude(), o.getLongitude()])
            } catch (i) {}
        var r = {
            garimport: t
        };
        mainpts = t,
        t.length > 1 ? ORGMapToolsManager.loadGPSRoute(r) : ORMain.showAlertOR(IMPORT_GPS_NO_DATA)
    },
    exportActivity: function(e) {
        var t = null
          , n = e.getDetails().getName()
          , a = "OR-" + e.getId()
          , o = e.getPoiUrl();
        t = null != e.getTPointDir() && e.getTPointDir().length > 0 ? e.getTPointDir() : e.getData().getTPoint();
        var i = ""
          , r = [];
        new Ajax.Request("elevation/getE4TG.php",{
            method: "post",
            parameters: {
                idr: e.getId()
            },
            asynchronous: !1,
            onSuccess: function(s) {
                r = "ko" == s.responseText ? null : s.responseText.evalJSON(!0);
                var l = ORMainGar.controller.getDevices()[ORMainGar.controller.getDeviceNumber()].getDisplayName();
                if (l = l.toLowerCase(),
                !ORMainGar.controller.checkDeviceReadSupport(Garmin.DeviceControl.FILE_TYPES.tcx) && -1 == l.indexOf("edge 500") || -1 != l.indexOf("edge 800") || -1 != l.indexOf("edge 810") || -1 != l.indexOf("Oregon 600") || -1 != l.indexOf("edge 1000")) {
                    if (-1 == l.indexOf("etrex 10") && -1 == l.indexOf("etrex 20") && -1 == l.indexOf("etrex 30") && (-1 != l.indexOf("etrex") || -1 != l.indexOf("tochange") || -1 != l.indexOf("60csx")) && t.length > 499) {
                        if (!confirm(GARMIN_TRUNCATED_TRACK))
                            return !1;
                        $(ORConstantsGar.gorstatus).innerHTML = GARMIN_LARGE_TRACK,
                        n = "ACTIVE_LOG"
                    }
                    i = ORMainGar._produceGPXString(t, n, a, r, o);
                    try {
                        ORMainGar.controller.writeToDevice(i, n)
                    } catch (c) {
                        console.log(c)
                    }
                } else {
                    var u = prompt("Vitesse moyenne: km/h", 12);
                    if (null == u)
                        return;
                    if (isNaN(parseFloat(u)) || 0 > u || 0 == u)
                        return;
                    i = ORMainGar._produceTCXString(t, n, a, e.getDistance(), u, r);
                    try {
                        ORMainGar.controller.writeCoursesToFitnessDevice(i, n + ".crs")
                    } catch (c) {
                        console.log(c)
                    }
                }
            }
        })
    },
    _produceGPXString: function(e, t, n, a, o) {
        var i = ""
          , r = "openrunner.com";
        if (i += '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>',
        i += '\n<gpx xmlns="https://www.topografix.com/GPX/1/1" creator="' + r + '" version="1.1" xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://www.topografix.com/GPX/1/1 https://www.topografix.com/GPX/1/1/gpx.xsd https://www.garmin.com/xmlschemas/GpxExtensions/v3 https://www.garmin.com/xmlschemas/GpxExtensions/v3/GpxExtensionsv3.xsd">',
        null != o && "" != o && o.length > 0)
            for (var s = 0, l = o.length; l > s; s++) {
                var c = o[s]
                  , u = ""
                  , g = c.poi_img_i.split("/");
                g.length > 0 && (u = g[g.length - 1],
                u = u.substr(0, u.length - 4)),
                i += '\n		<wpt lat="' + c.poi_lat + '" lon="' + c.poi_lng + '">',
                i += "\n		 <ele>0</ele>",
                i += "\n		 <name><![CDATA[" + c.poi_title + "]]></name>",
                i += "\n		<sym>" + u + "</sym>",
                i += "\n		</wpt>"
            }
        i += "\n	<trk>",
        i += "\n	<name><![CDATA[" + t + "]]></name>",
        i += "\n		<trkseg>";
        for (var s = 0, l = e.length; l > s; s++)
            i += '\n		<trkpt lat="' + e[s].lat() + '" lon="' + e[s].lng() + '">',
            i += null != a && a[s] ? "\n			<ele>" + a[s] + "</ele>" : "\n			<ele>0</ele>",
            i += "\n		</trkpt>";
        return i += "\n		</trkseg>",
        i += "\n	</trk>",
        i += "\n</gpx>"
    },
    _produceTCXString: function(e, t, n, a, o, i) {
        var r = ""
          , s = e.length - 1;
        r += '<?xml version="1.0" encoding="UTF-8" standalone="no" ?>',
        r += '\n<TrainingCenterDatabase xmlns="https://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2" xmlns:xsi="https://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 https://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd">',
        r += "\n	<Folders>",
        r += "\n	   <Courses>",
        r += '\n	     	<CourseFolder Name="Courses">',
        r += "\n	     		<CourseNameRef>",
        r += "\n					<Id>" + n + "</Id>",
        r += "\n	     		</CourseNameRef>",
        r += "\n	     	</CourseFolder>",
        r += "\n	   </Courses>",
        r += "\n	</Folders>",
        r += "\n	<Courses>",
        r += "\n		<Course>",
        r += "\n			<Name>" + n + "</Name>",
        r += "\n			<Lap>";
        var l = 3600 * a / o;
        r += "\n				<TotalTimeSeconds>" + l + "</TotalTimeSeconds>",
        r += "\n				<DistanceMeters>" + a + "</DistanceMeters>",
        r += "\n				<BeginPosition>",
        r += "\n					<LatitudeDegrees>" + e[0].lat() + "</LatitudeDegrees>",
        r += "\n					<LongitudeDegrees>" + e[0].lng() + "</LongitudeDegrees>",
        r += "\n				</BeginPosition>",
        r += "\n				<EndPosition>",
        r += "\n					<LatitudeDegrees>" + e[s].lat() + "</LatitudeDegrees>",
        r += "\n					<LongitudeDegrees>" + e[s].lng() + "</LongitudeDegrees>",
        r += "\n				</EndPosition>",
        r += '\n				<AverageHeartRateBpm xsi:type="HeartRateInBeatsPerMinute_t">',
        r += "\n					<Value>60</Value>",
        r += "\n				</AverageHeartRateBpm>",
        r += '\n				<MaximumHeartRateBpm xsi:type="HeartRateInBeatsPerMinute_t">',
        r += "\n					<Value>60</Value>",
        r += "\n				</MaximumHeartRateBpm>",
        r += "\n				<Intensity>Active</Intensity>",
        r += "\n			</Lap>",
        r += "\n			<Track>";
        for (var c = 0, u = 0, g = e.length; g > u; u++) {
            r += "\n				<Trackpoint>",
            u > 0 && (c += ORUtils.calculateDistanceBetweenTwoORLatLng(e, u - 1, u, ORMain.getUnit()));
            var p = 36e5 * c / o
              , d = new Date;
            d.setTime(d.getTime() + p),
            r += "\n					<Time>" + dateTCXFormat(d) + "</Time>",
            r += "\n					<Position>",
            r += "\n						<LatitudeDegrees>" + e[u].lat() + "</LatitudeDegrees>",
            r += "\n						<LongitudeDegrees>" + e[u].lng() + "</LongitudeDegrees>",
            r += "\n					</Position>",
            r += null != i ? "\n					<AltitudeMeters>" + i[u] + "</AltitudeMeters>" : "\n					<AltitudeMeters>0</AltitudeMeters>",
            r += "\n					<DistanceMeters>" + c + "</DistanceMeters>",
            r += '\n					<HeartRateBpm xsi:type="HeartRateInBeatsPerMinute_t">',
            r += "\n						<Value>60</Value>",
            r += "\n					</HeartRateBpm>",
            r += "\n					<SensorState>Absent</SensorState>",
            r += "\n				</Trackpoint>"
        }
        return r += "\n			</Track>",
        r += "\n		</Course>",
        r += "\n	</Courses>",
        r += "\n	</TrainingCenterDatabase>"
    }
}
  , ORGarListener = Class.create();
ORGarListener.prototype = {
    initialize: function(e) {
        this.activities = [],
        this.selectedDevice = $(ORConstantsGar.gorsdevices),
        this.selectedActivity = $(ORConstantsGar.gorsactivities),
        this.statusZone = $(ORConstantsGar.gorstatus),
        this.buttonDevice = $(ORConstantsGar.gorbdevices),
        this.buttonActivity = $(ORConstantsGar.gorbactivities),
        this.buttonImport = $(ORConstantsGar.gorbimport),
        this.buttonExport = $(ORConstantsGar.gorbexport),
        this.mode = e
    },
    onStartFindDevices: function(e) {
        this.buttonDevice.disabled = !0,
        this.selectedDevice.innerHTML = "",
        this.selectedDevice.hide(),
        this.statusZone.innerHTML = GARMIN_LOOKING_FOR_DEVICES,
        0 == this.mode ? (this.activities = [],
        this.selectedActivity.innerHTML = "",
        this.buttonImport.hide(),
        this.buttonActivity.hide(),
        this.selectedActivity.hide()) : this.buttonExport.hide()
    },
    onFinishFindDevices: function(e) {
        this.buttonDevice.disabled = !1;
        var t = e.controller.getDevices();
        this.statusZone.innerHTML = GARMIN_FOUND + t.length + GARMIN_DEVICES,
        e.controller.setDeviceNumber(0),
        this._listDevices(e, t),
        0 == this.mode ? (this.buttonActivity.show(),
        this.selectedDevice.show()) : (this.selectedDevice.show(),
        this.buttonExport.show())
    },
    _listDevices: function(e, t) {
        this.selectedDevice.options.size = 0;
        for (var n = 0; n < t.length; n++)
            this.selectedDevice.options[n] = new Option(t[n].getDisplayName(),t[n].getNumber());
        this.selectedDevice.selectedIndex = 0,
        this.selectedDevice.onchange = function() {
            e.controller.getDevices()[this.selectedDevice.value];
            e.controller.setDeviceNumber(this.selectedDevice.value),
            0 == this.mode && (this.selectedActivity.hide(),
            this.buttonImport.hide())
        }
        .bind(this),
        this.selectedDevice.disabled = !1
    },
    onStartReadFromDevice: function(e) {
        this.activities = [],
        this.selectedActivity.innerHTML = "",
        this.selectedActivity.hide(),
        this.buttonImport.hide()
    },
    onFinishReadFromDevice: function(e) {
        var t = Garmin.GpxActivityFactory
          , n = ORMainGar.controller.getDevices()[ORMainGar.controller.getDeviceNumber()];
        if (n.supportDeviceDataTypeRead(Garmin.DeviceControl.FILE_TYPES.tcx) || ORMainGar.gordatatype == Garmin.DeviceControl.FILE_TYPES.tcx)
            var t = Garmin.TcxActivityFactory;
        for (var a = t.parseDocument(e.controller.gpsData), o = 0, i = 0, r = a.length; r > i; i++) {
            var s = a[i].getSeries();
            for (k = 0,
            l = s.length; k < l; k++)
                if (s[k].getSeriesType() == Garmin.Series.TYPES.route || s[k].getSeriesType() == Garmin.Series.TYPES.history || s[k].getSeriesType() == Garmin.Series.TYPES.course) {
                    this.activities.push(a[i]);
                    var c = a[i].getAttribute(Garmin.Activity.ATTRIBUTE_KEYS.activityName);
                    if (s[k].getSeriesType() == Garmin.Series.TYPES.history) {
                        var u = a[i].getSummaryValue(Garmin.Activity.SUMMARY_KEYS.startTime).getValue()
                          , g = a[i].getSummaryValue(Garmin.Activity.SUMMARY_KEYS.endTime).getValue();
                        c = u.getDateString() + " (" + GARMIN_DURATION + ": " + u.getDurationTo(g) + ")"
                    }
                    this.selectedActivity.options[this.selectedActivity.length] = new Option(c,o++ + "," + k)
                }
        }
        this.activities.length > 0 && (this.buttonImport.show(),
        this.selectedActivity.show())
    },
    onProgressReadFromDevice: function(e) {
        this.statusZone.innerHTML = e.progress.text[0] + " " + (null != e.progress.percentage ? e.progress.percentage + "%" : "")
    },
    getSelectedActivity: function() {
        var e = this.selectedActivity.options[this.selectedActivity.selectedIndex].value;
        e = e.split(",", 2);
        var t = this.activities[parseInt(e[0])]
          , n = t.getSeries()[parseInt(e[1])];
        return n
    },
    onWaitingWriteToDevice: function(e) {
        confirm(e.message.getText()) ? (this.statusZone.innerHTML = GARMIN_OVERWRITING_FILE,
        e.controller.respondToMessageBox(!0)) : (this.statusZone.innerHTML = GARMIN_NOT_OVERWRITING_FILE,
        e.controller.respondToMessageBox(!1))
    },
    onProgressWriteToDevice: function(e) {
        this.statusZone.innerHTML = e.progress + ""
    },
    onFinishWriteToDevice: function(e) {
        this.statusZone.innerHTML = e.progress + ""
    },
    onStartWriteToDevice: function(e) {
        this.statusZone.innerHTML = e.progress + ""
    }
};
var ORElementShowable = Class.create({
    initialize: function(e, t, n) {
        this.id = e,
        this.shown = t || !1,
        this.alwaysshown = n || !1
    },
    getId: function() {
        return this.id
    },
    isShown: function() {
        return this.shown
    },
    isAlwaysShown: function() {
        return this.alwaysshown
    },
    setShown: function(e) {
        this.shown = e
    },
    setAlwaysShown: function(e) {
        this.alwaysshown = e
    },
    show: function() {
        $(this.id).show()
    },
    hide: function() {
        $(this.id).hide()
    }
})
  , ORElementShowableManager = function() {
    var e = new Hash;
    return e.set("tab-plan", [new ORElementShowable("tools_deco",!0,!0), new ORElementShowable("tools_oc",!1,!0), new ORElementShowable("tools_draw",!0,!1), new ORElementShowable("tools_save",!0,!1), new ORElementShowable("tools_search",!0,!1), new ORElementShowable("tools_route",!1,!0), new ORElementShowable("layer_poiuser",!1,!1), new ORElementShowable("layer_helpor",!1,!1), new ORElementShowable("layer_props",!1,!1), new ORElementShowable("layer_follow",!1,!1)]),
    e.set("tab-search", []),
    e.set("tab-home", []),
    {
        showTabElements: function(t) {
            for (var n = e.get(t), a = 0, o = n.length; o > a; a++) {
                var i = n[a];
                i && (i.isAlwaysShown() || i.isShown() ? i.show() : i.hide())
            }
        },
        hideTabElements: function(t) {
            for (var n = e.get(t), a = 0, o = n.length; o > a; a++) {
                var i = n[a];
                i && i.hide()
            }
        },
        changeShownStatus: function(t, n, a) {
            for (var o = e.get(t), i = 0, r = o.length; r > i; i++) {
                var s = o[i];
                s && s.getId() == n && s.setShown(a)
            }
        }
    }
}()
  , ORMain = {
    startIcon: null,
    insertIcon: null,
    stonesIcon: null,
    removeIcon: null,
    colIcon: null,
    ptIcon: null,
    ptIconM: null,
    ptIconP: null,
    iwin: null,
    map: null,
    cgRouteManager: null,
    geocoder: null,
    gmap_step_slider: null,
    valKMMap: [0, 1, 2, 5, 10, 25, 50, 100],
    valRevKMMap: {
        0: 0,
        1: 1,
        2: 2,
        5: 3,
        10: 4,
        25: 5,
        50: 6,
        100: 7
    },
    mapSizeW: 922,
    mapSizeH: 600,
    winH: 600,
    winW: 800,
    winHFS: 600,
    winWFS: 800,
    winA4Min: 984,
    winA4Max: 1360,
    winA4Red: 270,
    winA4PRed: 150,
    prA4L: !1,
    pr4EleWidth: 390,
    allowedFS: !0,
    links: [],
    tabMainManager: new TabManager,
    initialization: !1,
    flgHelp: !1,
    unit: UNIT_KM,
    unitChanged: !1,
    reloadNeeded: !1,
    cgRoutingGMap: null,
    centerPoint: new ORLatLng(0,0),
    defaultZoom: 13,
    wheelmouse: 0,
    autocenter: 0,
    discLoaded: !1,
    directID: -1,
    directAd: "",
    directSe: "",
    uddcs: "",
    panoramioLayer: null,
    mkp: null,
    GLOBAL_DRM: {
        TYP0: "8e68qrbqv9abwhnyiydw8zqo",
        TYP1: ""
    },
    USER_DRM: {
        TYP0: "",
        TYP1: ""
    },
    sdloaded: !1,
    load: function() {
        if (jQuery.browser.mobile || 1 != ORBG_ACTIVATION || $j("body").click(function(e) {
            var t = e.target ? e.target : e.srcElement;
            $j("body").hasClass("boxed-layout") && ("fullcontent" != t.id || ORGMapToolsManager.isFullScreen() || window.open(ORBG_TARGET_URL))
        }),
        $("tn_news_wrap")) {
            var e = new Date;
            new Ajax.Request("i18n/orxtn.php?t=" + e.getTime(),{
                method: "get",
                asynchronous: !0,
                onSuccess: function(e) {
                    $("tn_news_wrap").innerHTML = e.responseText
                }
            })
        }
        if ($("or_news_wrap")) {
            var e = new Date;
            new Ajax.Request("i18n/orxnews.php?t=" + e.getTime(),{
                method: "post",
                asynchronous: !0,
                onSuccess: function(e) {
                    $("or_news_wrap").innerHTML = e.responseText
                }
            })
        }
        $j(window).resize(function(e) {
            e.stopPropagation(),
            $j.browser.msie || ORMain.setWSize()
        }),
        ORMain.cgRouteManager = new RouteManagerGC("tools_route",ORGMapToolsManager.createNewRoute,ORGMapToolsManager.destroyRoute,ORGMapToolsManager.changeActiveRoute,ORGMapToolsManager.changeRouteSettings),
        ORMain.cgRoutingGMap = new RoutingGCOR("layer_follow_content",[1],ORGMapToolsManager.changeRoutingEngine,ORGMapToolsManager.changeRoutingMode),
        ORIconCatalog.load(),
        ORPOIUserManager.initialize(ORGMapDecoratorManager.freezePoiUser, "ORGMapDecoratorManager.removePoiUser", "ORGMapDecoratorManager.displayPoiUserEdPopUp"),
        document.observe("or:poiuser-modif", ORMain.activatePoiUserSaveImg),
        document.observe("or:poiuser-nomodif", ORMain.deactivatePoiUserSaveImg),
        ORMain.initialize(),
        $("showutm").observe("click", function(e) {
            Event.stop(e),
            ORAccMain.isUserConnected() ? ORGMapDecoratorManager.showUTMGrid(!1) : Modalbox.show("account/userNotAuthenticated.php", {
                slideDownDuration: "0",
                title: ORConstants.MODAL_TITLE,
                width: ORConstants.MODAL_MEDIUM_WIDTH,
                height: ORConstants.MODAL_SMALL_HEIGHT,
                overlayClose: !1
            })
        }),
        $("showcol").observe("click", function(e) {
            Event.stop(e),
            ORAccMain.isUserConnected() ? ORGMapDecoratorManager.showColsR(!0) : Modalbox.show("account/userNotAuthenticated.php", {
                slideDownDuration: "0",
                title: ORConstants.MODAL_TITLE,
                width: ORConstants.MODAL_MEDIUM_WIDTH,
                height: ORConstants.MODAL_SMALL_HEIGHT,
                overlayClose: !1
            })
        }),
        $("showcolm").observe("click", function(e) {
            Event.stop(e),
            ORAccMain.isUserConnected() ? ORGMapDecoratorManager.showColsM(!0) : Modalbox.show("account/userNotAuthenticated.php", {
                slideDownDuration: "0",
                title: ORConstants.MODAL_TITLE,
                width: ORConstants.MODAL_MEDIUM_WIDTH,
                height: ORConstants.MODAL_SMALL_HEIGHT,
                overlayClose: !1
            })
        }),
        $("gps").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.loadGPSInput()
        }),
        $("show").observe("click", function(e) {
            Event.stop(e),
            ORGMapDecoratorManager.showMarkers(!1)
        }),
        ORGMapDecoratorManager.showMarkers(!0),
        $("draw").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.startDraw(!1)
        }),
        ORGMapToolsManager.startDraw(!0),
        $("insert").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.startInsert(!1)
        }),
        $("remove").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.startRemove(!1)
        }),
        $("remove_last").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.undoLast()
        }),
        $("clean").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.cleanRoute()
        }),
        $("reverse").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.reverseRoute()
        }),
        $("backhome").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.goBackToHome()
        }),
        $("joinstart").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.goDirectToHome()
        }),
        $("savedata").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.saveRoute()
        }),
        $("updatedata").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.updateRoute()
        }),
        $("removedata").observe("click", function(e) {
            Event.stop(e),
            ORAccMain.removeData(-1)
        }),
        $("poiuserdata").observe("click", function(e) {
            Event.stop(e),
            ORPOIUserManager.showHideEditor(!1)
        }),
        $("savepoiuserdata").observe("click", function(e) {
            Event.stop(e),
            ORRouteManager.saveUserPoi()
        }),
        $("follow").observe("click", function(e) {
            Event.stop(e),
            $("follow").hasClassName("tools-follow-locked") ? ORGMapToolsManager.unlockRouteAuto() : ORGMapToolsManager.changeRoutingAuto()
        }),
        $("follow_choice").observe("click", function(e) {
            Event.stop(e),
            $("layer_follow").show()
        }),
        $("layer_follow_close").observe("click", function(e) {
            Event.stop(e),
            $("layer_follow").hide()
        }),
        $("tools_openclose").observe("click", function(e) {
            Event.stop(e),
            ORGMapToolsManager.slide(["tools_draw", "tools_save", "routegc_main_add", "tools_search"], !1)
        }),
        $("fullscreen").observe("click", function(e) {
            Event.stop(e),
            ORMain.allowedFS ? ORGMapToolsManager.fullScreen() : ORMain.showAlertInlineOR(SCREEN_DIMENSIONS)
        }),
        $("printall").observe("click", function(e) {
            Event.stop(e),
            ORMain.prt(!0)
        }),
        $("helpor").observe("click", function(e) {
            Event.stop(e),
            ORMain.flgHelp ? ORMain.hideHelp() : ORMain.getHelp()
        }),
        0 == ORMain.centerPoint.getLat() && 0 == ORMain.centerPoint.getLng() && (window.geoip_city && "" != geoip_city() ? (ORMain.centerPoint.setLat(geoip_latitude()),
        ORMain.centerPoint.setLng(geoip_longitude())) : (ORMain.centerPoint.setLat(50.8019),
        ORMain.centerPoint.setLng(-7.3945),
        ORMain.defaultZoom = 6)),
        google.maps.visualRefresh = !0;
        var t = {
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            draggableCursor: "crosshair",
            keyboardShortcuts: !0,
            zoomControl: !0,
            panControl: !1,
            zoomControlOptions: {
                position: google.maps.ControlPosition.TOP_LEFT,
                style: google.maps.ZoomControlStyle.LARGE
            },
            mapTypeControl: !1,
            scaleControl: !0,
            streetViewControl: !0,
            scrollwheel: !1,
            center: new google.maps.LatLng(ORMain.centerPoint.getLat(),ORMain.centerPoint.getLng()),
            zoom: ORMain.defaultZoom
        };
        $j("#map").css("height", ORMain.mapSizeH + "px"),
        $j("#map").css("width", ORMain.mapSizeW + "px"),
        ORMain.map = new google.maps.Map(document.getElementById("map"),t);
        var n = document.getElementById("address")
          , a = new google.maps.places.SearchBox(n);
        if (ORMain.map.controls[google.maps.ControlPosition.TOP_LEFT].push(n),
        a.addListener("places_changed", function() {
            var e = a.getPlaces();
            0 != e.length && (ORMain.map.setCenter(e[0].geometry.location),
            ORMain.map.setZoom(13),
            document.fire("or:refreshDecoratorGMapEvent", {
                flgAlerte: !1
            }),
            0 == ORRouteManager.getActiveRoute().getData().getTPoint().length && ORGMapToolsManager.draw(e[0].geometry.location))
        }),
        ORMain.map.setTilt(45),
        ORMain.map.panBy(-ORMain.mapSizeW / 2, -ORMain.mapSizeH / 2),
        ORMain.customControl = ORCustomMapControl("or_layers_control", ORMain.map),
        ORMain.mapManager = new ORMapManager(ORMain.map,[new ORRegisteredMap(ORConstants.GMAPS_PLAN,ORRegisteredMapType.GMAPS,google.maps.MapTypeId.ROADMAP), new ORRegisteredMap(ORConstants.GMAPS_SATELLITE,ORRegisteredMapType.GMAPS,google.maps.MapTypeId.SATELLITE), new ORRegisteredMap(ORConstants.MAP_OSM,ORRegisteredMapType.EXTFIXMAPS,ORExtMap.getOSMBaseMap(ORMain.map, {}, "", {
            name: ORConstants.MAP_OSM,
            isPng: !0,
            maxZoom: 18,
            minZoom: 1,
            alt: ORConstants.MAP_OSM
        }, [{
            pictureUrl: "",
            url: "https://www.openstreetmap.org/",
            textUrl: "All maps &copy; CC-BY-SA OpenStreetMap contributors"
        }], "a.tile.openstreetmap.org")), new ORRegisteredMap(ORConstants.MAP_OSM_OCM,ORRegisteredMapType.EXTFIXMAPS,ORExtMap.getOSMBaseMap(ORMain.map, {}, "", {
            name: ORConstants.MAP_OSM_OCM,
            isPng: !0,
            maxZoom: 18,
            minZoom: 1,
            alt: ORConstants.MAP_OSM_OCM
        }, [{
            pictureUrl: "",
            url: "https://www.openstreetmap.org/",
            textUrl: "All maps &copy; CC-BY-SA OpenStreetMap contributors"
        }], "b.tile.thunderforest.com/cycle")), new ORRegisteredMap(ORConstants.TOPO_FRANCE,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getIGNBaseMap(ORMain.map, {
            hasDRM: !0,
            drmID: TYP0
        }, "GEOGRAPHICALGRIDSYSTEMS.MAPS", {
            name: ORConstants.TOPO_FRANCE
        }, [{
            pictureUrl: "./img/v3/wms/logo_ign.gif",
            url: "https://www.ign.fr",
            textUrl: "IGN"
        }, {
            pictureUrl: "./img/v3/wms/logo_gp.gif",
            url: "https://www.geoportail.gouv.fr/accueil",
            textUrl: "Géoportail"
        }])), new ORRegisteredMap(ORConstants.TOPO_SUISSE,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getSWTBaseMap(ORMain.map, {
            hasDRM: !0,
            drmID: TYP1
        }, ORExtMap.LAYER_MAP, {
            name: ORConstants.TOPO_SUISSE,
            maxZoom: 16,
            minZoom: 12
        }, [{
            pictureUrl: "",
            url: "https://www.swisstopo.ch/",
            textUrl: "&copy; SwissTopo"
        }])), new ORRegisteredMap(ORConstants.TOPO_USA,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getUSGSBaseMap(ORMain.map, {}, "DRG", {
            name: ORConstants.TOPO_USA,
            isPng: !1,
            maxZoom: 17,
            minZoom: 1,
            alt: "USGS"
        }, [{
            pictureUrl: "",
            url: "https://www.usgs.gov/",
            textUrl: "USGS"
        }])), new ORRegisteredMap(ORConstants.TOPO_CANADA,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getToporamaBaseMap(ORMain.map, {}, "WMS-Toporama", {
            name: ORConstants.TOPO_CANADA,
            isPng: !0,
            maxZoom: 17,
            minZoom: 1,
            alt: "WMS-Toporama"
        }, [{
            pictureUrl: "",
            url: "https://geogratis.cgdi.gc.ca/geogratis/fr/licence.jsp",
            textUrl: "&copy; Le ministère des Ressources naturelles Canada. Tous droits réservés."
        }])), new ORRegisteredMap(ORConstants.TOPO_NORVEGE,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getSKARTBaseMap(ORMain.map, {}, "", {
            name: ORConstants.TOPO_NORVEGE,
            isPng: !0,
            maxZoom: 17,
            minZoom: 8,
            alt: "Statkart"
        }, [{
            pictureUrl: "",
            url: "https://www.statkart.no/eng/Norwegian_Mapping_Authority/",
            textUrl: "&copy; Norwegian Mapping Authority"
        }])), new ORRegisteredMap(ORConstants.TOPO_ESPAGNE,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getIGNSpainBaseMap(ORMain.map, {}, ORExtMap.LAYER_MAP, {
            name: ORConstants.TOPO_ESPAGNE,
            isPng: !1,
            maxZoom: 16,
            minZoom: 6,
            alt: "IGNE"
        }, [{
            pictureUrl: "./img/IGN_logo_ministerio.png",
            url: "https://www.ign.es/",
            textUrl: "&copy; IGN"
        }])), new ORRegisteredMap(ORConstants.TOPO_FRANCE_CLS,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getIGNBaseMap(ORMain.map, {
            hasDRM: !0,
            drmID: TYP0
        }, "GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.CLASSIQUE", {
            name: ORConstants.TOPO_FRANCE_CLS
        }, [{
            pictureUrl: "./img/v3/wms/logo_ign.gif",
            url: "https://www.ign.fr",
            textUrl: "IGN"
        }, {
            pictureUrl: "./img/v3/wms/logo_gp.gif",
            url: "https://www.geoportail.gouv.fr/accueil",
            textUrl: "Géoportail"
        }])), new ORRegisteredMap(ORConstants.TOPO_FRANCE_STD,ORRegisteredMapType.EXTDYNMAPS,ORExtMap.getIGNBaseMap(ORMain.map, {
            hasDRM: !0,
            drmID: TYP0
        }, "GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD", {
            name: ORConstants.TOPO_FRANCE_STD
        }, [{
            pictureUrl: "./img/v3/wms/logo_ign.gif",
            url: "https://www.ign.fr",
            textUrl: "IGN"
        }, {
            pictureUrl: "./img/v3/wms/logo_gp.gif",
            url: "https://www.geoportail.gouv.fr/accueil",
            textUrl: "Géoportail"
        }])), new ORRegisteredMap(ORConstants.OL_TERRAIN,ORRegisteredMapType.GLOL,google.maps.MapTypeId.TERRAIN,[ORConstants.GMAPS_PLAN]), new ORRegisteredMap(ORConstants.OL_LABELS,ORRegisteredMapType.GLOL,google.maps.MapTypeId.HYBRID,[ORConstants.GMAPS_SATELLITE]), new ORRegisteredMap(ORConstants.OL_BICYCLE,ORRegisteredMapType.GOL,new google.maps.BicyclingLayer,[ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
             new ORRegisteredMap(ORConstants.OL_ZPS,ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                 {name : 'OSM',minZoom:12,alt: 'OSM'},
                 [
                     {
                         pictureUrl:'',
                         url:'https://www.openstreetmap.org/',
                         textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                     }
                 ],
                 "natura_protection_speciale",
                 'localhost',
             ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_SIC,ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "natura_directive_habitat",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_PNR,ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "parc_naturel_regional",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_RNR, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "reserve_naturelle_regionale",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_ADM, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "communespsql",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_PN, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "parc_national",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_RI, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "reserve_integrale",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_RN, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "reserve_nationale",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_PFP, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "parcelles_forestieres_publique0",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_PB, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "protection_biotope0",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),

             new ORRegisteredMap(ORConstants.OL_GP, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "gend_police0",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_LPN, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "liste_passage_niveau",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),
            new ORRegisteredMap(ORConstants.OL_BR, ORRegisteredMapType.EXTOL, ORExtMap.getGeoJsonLayer(ORMain.map, {}, '',
                {name : 'OSM',minZoom:12,alt: 'OSM'},
                [
                    {
                        pictureUrl:'',
                        url:'https://www.openstreetmap.org/',
                        textUrl:'All maps &copy; CC-BY-SA OpenStreetMap contributors'
                    }
                ],
                "bornage_routier0",
                'localhost',
            ), [ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE]),



            new ORRegisteredMap(ORConstants.OL_CAD,ORRegisteredMapType.EXTOL,ORExtMap.getIGNBaseMap(ORMain.map, {
            hasDRM: !0,
            drmID: TYP0
        }, "CADASTRALPARCELS.PARCELS", {
            name: "CAD France",
            isPng: !0,
            style: "bdparcellaire"
        }, []),[ORConstants.TOPO_FRANCE_CLS, ORConstants.TOPO_FRANCE_STD, ORConstants.MAP_OSM, ORConstants.MAP_OSM_OCM, ORConstants.TOPO_FRANCE, ORConstants.GMAPS_PLAN, ORConstants.GMAPS_SATELLITE])],ORMain.customControl),
        1 == ORMain.wheelmouse && ORMain.map.setOptions({
            scrollwheel: !0
        }),
        ORMain.cgRouteManager.initRoute(ORRouteManager.getActiveRoute().getId(), ORRouteManager.getActiveRoute().getStrokeColor(), ORRouteManager.getActiveRoute().getStrokeOpacity(), ORRouteManager.getActiveRoute().getStrokeWidth(), ORMain.getUnit()),
        ORGMapEngine.setGDir(new google.maps.DirectionsService),
        ORMain.iwin = new google.maps.InfoWindow({
            disableAutoPan: !0
        }),
        ORMain.iwinpoied = new google.maps.InfoWindow,
        google.maps.event.addListener(ORMain.iwinpoied, "domready", function(e) {
            ORInfoPopUpFactory.createEditableContent($("poipopup_ed").innerHTML)
        }),
        ORMain.iwinpoiro = new google.maps.InfoWindow({
            disableAutoPan: !0
        }),
        google.maps.event.addListener(ORMain.iwinpoiro, "domready", function(e) {
            ORInfoPopUpFactory.createReadonlyContent($("poipopup_ro").innerHTML)
        }),
        ORMain.startKmIcon = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 14,
            fillColor: "green",
            fillOpacity: .6,
            strokeColor: "green",
            strokeOpacity: .8,
            strokeWeight: 2
        },
        ORMain.startIcon = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 6,
            fillColor: "green",
            fillOpacity: .8,
            strokeColor: "white",
            strokeWeight: 1
        },
        ORMain.lastKmIcon = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 10,
            fillColor: "red",
            fillOpacity: .6,
            strokeColor: "red",
            strokeOpacity: .8,
            strokeWeight: 2
        },
        ORMain.kmIcon = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 6,
            fillColor: "orange",
            fillOpacity: .8,
            strokeColor: "white",
            strokeWeight: 1
        },
        ORMain.lastIcon = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 10,
            fillColor: "red",
            fillOpacity: .6,
            strokeColor: "red",
            strokeOpacity: .8,
            strokeWeight: 2
        },
        ORMain.ptIcon = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 6,
            fillColor: "red",
            fillOpacity: .8,
            strokeColor: "white",
            strokeWeight: 1
        },
        ORMain.ptIconM = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 6,
            fillColor: "yellow",
            fillOpacity: .8,
            strokeColor: "white",
            strokeWeight: 1
        },
        ORMain.ptIconP = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 8,
            fillColor: "orange",
            fillOpacity: .8,
            strokeColor: "white",
            strokeWeight: 1
        },
        ORMain.insertIcon = {
            path: google.maps.SymbolPath.CIRCLE,
            scale: 6,
            fillColor: "blue",
            fillOpacity: .8,
            strokeColor: "white",
            strokeWeight: 1
        },
        ORMain.stonesIcon = new google.maps.MarkerImage,
        ORMain.colRIcon = new google.maps.MarkerImage,
        ORMain.colRIcon.url = "img/v3/lt/sprite-mountains.png",
        ORMain.colRIcon.size = new google.maps.Size(32,37),
        ORMain.colRIcon.anchor = new google.maps.Point(16,37),
        ORMain.colRIcon.origin = new google.maps.Point(0,0),
        ORMain.colRIcon.infoWindowAnchor = new google.maps.Point(16,18),
        ORMain.colR2000Icon = new google.maps.MarkerImage,
        ORMain.colR2000Icon.url = "img/v3/lt/sprite-mountains.png",
        ORMain.colR2000Icon.size = new google.maps.Size(32,37),
        ORMain.colR2000Icon.anchor = new google.maps.Point(16,37),
        ORMain.colR2000Icon.origin = new google.maps.Point(96,0),
        ORMain.colR2000Icon.infoWindowAnchor = new google.maps.Point(16,18),
        ORMain.colMIcon = new google.maps.MarkerImage,
        ORMain.colMIcon.url = "img/v3/lt/sprite-mountains.png",
        ORMain.colMIcon.size = new google.maps.Size(32,37),
        ORMain.colMIcon.anchor = new google.maps.Point(16,37),
        ORMain.colMIcon.origin = new google.maps.Point(256,0),
        ORMain.colMIcon.infoWindowAnchor = new google.maps.Point(16,18),
        ORMain.colM2Icon = new google.maps.MarkerImage,
        ORMain.colM2Icon.url = "img/v3/lt/sprite-mountains.png",
        ORMain.colM2Icon.size = new google.maps.Size(32,37),
        ORMain.colM2Icon.anchor = new google.maps.Point(16,37),
        ORMain.colM2Icon.origin = new google.maps.Point(192,0),
        ORMain.colM2Icon.infoWindowAnchor = new google.maps.Point(16,18),
        ORMain.colM40Icon = new google.maps.MarkerImage,
        ORMain.colM40Icon.url = "img/v3/lt/sprite-mountains.png",
        ORMain.colM40Icon.size = new google.maps.Size(32,37),
        ORMain.colM40Icon.anchor = new google.maps.Point(16,37),
        ORMain.colM40Icon.origin = new google.maps.Point(224,0),
        ORMain.colM40Icon.infoWindowAnchor = new google.maps.Point(16,18),
        ORMain.accIcon = new google.maps.MarkerImage,
        ORMain.accIcon.size = new google.maps.Size(30,37),
        ORMain.accIcon.anchor = new google.maps.Point(15,37),
        ORMain.getORStats("orstats_globales", 1, "", ""),
        ORMain.getORStats("orstats_bestload", 2, "", ""),
        google.maps.event.addListener(ORMain.map, "mousemove", function(e) {
            if (e.latLng) {
                var t = e.latLng;
                $("coord_info_lng").innerHTML = t.lng().toFixed(5),
                $("coord_info_lat").innerHTML = t.lat().toFixed(5);
                var n = ORUTMConvertor.getUTMFromLatLon(t.lat(), t.lng());
                $("coord_info_utm_Z").innerHTML = n.Z,
                $("coord_info_utm_EV").innerHTML = n.E,
                $("coord_info_utm_NV").innerHTML = n.N
            }
        }),
        google.maps.event.addListener(ORMain.map, "click", function(e) {
            e.latLng && (ORPOIUserManager.isEditorOpen() ? ORGMapDecoratorManager.addPoiUserMarker(e.latLng) : (ORGMapToolsManager.draw(e.latLng),
            1 == ORMain.autocenter && ORMain.map.setCenter(e.latLng)))
        }),
            // google.maps.event.addListener(ORMain.map, "idle", function(e){
            //
            //     tempbounds = ORMain.map.getBounds()
            //     url ="https://217.70.190.13:8080/geoserver/cite/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=cite%3Acommunes&maxFeatures=50&outputFormat=application%2Fjson"
            //     typename = "cite:communes"
            //     url += "&bbox="+tempbounds.ga.j+","+tempbounds.na.j+","+tempbounds.ga.l+","+tempbounds.na.l+","+"EPSG%3A4326"
            //     console.log(url)
            //     ORMain.map.data.forEach(function(feature){
            //         ORMain.map.data.remove(feature)
            //     })
            //
            //
            //
            //     if (ORMain.map.zoom>=12){
            //
            //         jQuery.get(url, function(data){
            //
            //             let geo=ORMain.map.data.addGeoJson(data)
            //             console.log(geo)
            //         })
            //     }
            //
            //
            // }),
        google.maps.event.addListener(ORMain.map, "rightclick", function(e) {
            ORPOIUserManager.isEditorOpen() || e.latLng && ORGMapToolsManager.isRoutingAuto() && (ORGMapToolsManager.draw(e.latLng, ORConstants.TYPE_POINT_NOROUTING),
            1 == ORMain.autocenter && ORMain.map.setCenter(e.latLng))
        }),
        new LongPress(ORMain.map,2e3),
        google.maps.event.addListener(ORMain.map, "longpress", function(e) {
            ORPOIUserManager.isEditorOpen() || e.latLng && ORGMapToolsManager.isRoutingAuto() && (ORGMapToolsManager.draw(e.latLng, ORConstants.TYPE_POINT_NOROUTING),
            1 == ORMain.autocenter && ORMain.map.setCenter(e.latLng))
        }),
        google.maps.event.addListener(ORMain.map, "idle", function(e, t) {
            document.fire("or:refreshDecoratorGMapEvent", {
                flgAlerte: !0
            })
        }),
        $("stepstones").innerHTML = ORGMapDecoratorManager.getStepStones(),
        $("stepstones-unit").innerHTML = ORMain.getUnit(),
        ORMain.gmap_step_slider = $j("#step_slider").slider({
            min: 0,
            max: ORMain.valKMMap.length - 1,
            value: ORGMapDecoratorManager.getStepStones(),
            orientation: "vertical",
            slide: function(e, t) {
                $j("#stepstones").html(ORMain.valKMMap[t.value].toFixed(0))
            },
            change: function(e, t) {
                $j("#stepstones").html(ORMain.valKMMap[t.value].toFixed(0)),
                ORGMapDecoratorManager.setStepStones(ORMain.valKMMap[t.value]),
                ORRouteManager.getActiveRoute().setStepStones(ORMain.valKMMap[t.value]),
                document.fire("or:recalculateDistanceAndStonesGMapEvent")
            }
        }),
        -1 != ORMain.directID) {
            ORElementShowableManager.showTabElements("tab-plan"),
            ORElementShowableManager.hideTabElements("tab-search"),
            ORMain.changeHeaderSize(!1),
            ORMain.tabMainManager.setActiveTab("tab_plan"),
            ORSearchMain.lockLoad();
            for (var o = ORMain.directID.split(","), i = ",", r = 0, s = o.length; s > r && r < NB_ROUTE_LIMIT; r++)
                if (!isNaN(parseInt(o[r])) && -1 == i.indexOf("," + o[r] + ",")) {
                    var l = ORGMapToolsManager.loadRoute(o[r]);
                    l || ORGMapToolsManager.createNewRoute(),
                    i += o[r] + ","
                }
            ORGMapToolsManager.slide(["tools_draw", "tools_save", "routegc_main_add", "tools_search"], !0)
        } else
            "" != ORMain.directSe && "@" == ORMain.directSe.charAt(0) ? (ORElementShowableManager.hideTabElements("tab-plan"),
            loadSearchPage(ORMain.directSe, 3),
            ORMain.tabMainManager.setActiveTab("tab_search"),
            ORMain.changeHeaderSize(!1)) : (ORElementShowableManager.showTabElements("tab-plan"),
            ORElementShowableManager.hideTabElements("tab-search"),
            ORMain.tabMainManager.setActiveTab("tab_plan"),
            ORGMapToolsManager.slide(["tools_draw", "tools_save", "routegc_main_add", "tools_search"], !0));
        "" != ORMain.directAd && "0" == ORMain.directAd && (ORMain.tabMainManager.goToTab("or:tab-doc-doc", "tab_doc"),
        ORIDocMain.loaded = !0,
        ORIDocMain.setDocLesson("lessons/Guide_de_demarrage_rapide.html"));
        var c = ORRouteManager.getActiveRoute().getData();
        if (null == c.getMarkerCluster()) {
            var u = (ORRouteManager.getActiveRoute(),
            new MarkerClusterer(ORMain.map));
            u.setMaxZoom(MC_MAX_ZOOM),
            u.setMinimumClusterSize(MC_MIN_CSIZE),
            u.setZoomOnClick(!1),
            c.setMarkerCluster(u)
        }
        ORAccMain.manageConnected(),
        0 == ORMain.directIGN || ORAccMain.isUserOrganizer() || ORMain.showDirectlyIGN(),
        ORAccMain.isUserConnected() || "" == ORMain.uddcs || ORAccMain.callSignInForm(ORMain.uddcs),
        Event.observe($("layer_helpor_close"), "click", function() {
            ORMain.hideHelp()
        }),
        Event.observe($("layer_props_close"), "click", function() {
            $("layer_props").hide()
        }),
        Event.observe($("layer_alertor_close"), "click", function() {
            $("layer_alertor").hide()
        }),
        Event.observe($("layer_poiuser_close"), "click", function() {
            ORMain.showHidePoiEditor()
        })
    },
    activateDrmMaps: function() {},
    showHidePoiEditor: function() {
        ORPOIUserManager.showHideEditor(!0)
    },
    changeUnit: function(e) {
        if (ORMain.unit != e) {
            var t = ORConstants.UNIT_CONVERSION;
            e == UNIT_KM ? ($("sunitmi").removeClassName("button-active"),
            $("sunitkm").addClassName("button-active"),
            t = 1 / t) : ($("sunitkm").removeClassName("button-active"),
            $("sunitmi").addClassName("button-active")),
            ORMain.unit = e,
            ORMain.cgRouteManager && ($("stepstones-unit").innerHTML = ORGMapDecoratorManager.getStepStones() + " " + ORMain.getUnit(),
            ORMain.cgRouteManager.changeUnit(ORMain.unit),
            ORRouteManager.changeUnit(ORMain.unit),
            $$("span.distance").each(function(e) {
                e.innerHTML = (e.innerHTML / t).toFixed(3)
            }),
            $$("span.poi-distance").each(function(e) {
                e.innerHTML = (e.innerHTML / t).toFixed(3)
            }),
            $$("td.di").each(function(e) {
                e.innerHTML = (e.innerHTML / t).toFixed(3)
            }),
            $$("span.unitlabel").each(function(e) {
                e.innerHTML = ORMain.unit
            }),
            document.fire("or:redrawAllRoutesGMapEvent"))
        }
        return !1
    },
    initUnit: function() {
        var e = "sunit" + ORMain.unit;
        $(e).addClassName("button-active")
    },
    getUnit: function() {
        return ORMain.unit
    },
    setDirectId: function(e) {
        ORMain.directID = e
    },
    setDirectSe: function(e) {
        ORMain.directSe = e
    },
    setDirectAd: function(e) {
        ORMain.directAd = e
    },
    setDirectIGN: function(e) {
        ORMain.directIGN = e
    },
    setFDDCS: function(e) {
        ORMain.uddcs = e
    },
    proceedCleaning: function() {},
    logPartners: function(e) {
        var t = "logger/logPartners.php?" + e + "&map=GMAPS";
        return new Ajax.Request(t,{
            method: "get"
        }),
        !1
    },
    getORStats: function(e, t, n, a, o) {
        1 == t ? new Ajax.Updater(e,"stats/global-stats.php") : 2 == t && ($("sist") && $("sist").hide(),
        new Ajax.Updater(e,"stats/bestload-stats.php",{
            parameters: {
                fp: ORMain.centerPoint.getLat() + ";" + ORMain.centerPoint.getLng(),
                ca: n,
                cd: a,
                cac: o
            },
            onCreate: function() {
                $("loadingsist") && $("loadingsist").show()
            },
            onComplete: function(e) {
                if (200 == e.status) {
                    var t = ORConstants.UNIT_CONVERSION;
                    ORMain.getUnit() == UNIT_MILES && ($$("p.best-route-dis span.distance").each(function(e) {
                        e.innerHTML = (e.innerHTML / t).toFixed(3)
                    }),
                    $$("p.best-route-dis span.unitlabel unitsize").each(function(e) {
                        e.innerHTML = "&nbsp;(" + ORMain.unit + ")"
                    }))
                }
                $("sist") && $("sist").show()
            }
        }))
    },
    getAreaDept: function(e, t) {
        new Ajax.Updater(e,"utils/areaDeptAJX.php",{
            parameters: {
                ca: t
            },
            onComplete: function(e) {}
        })
    },
    getOREvents: function(e) {
        new Ajax.Updater(e,"event/orevents.php",{
            onComplete: function(e) {
                if (200 == e.status) {
                    var t = ORConstants.UNIT_CONVERSION;
                    ORMain.getUnit() == UNIT_MILES && ($$("div.pres-race span.distance").each(function(e) {
                        e.innerHTML = (e.innerHTML / t).toFixed(3)
                    }),
                    $$("div.pres-race span.unitlabel unitsize").each(function(e) {
                        e.innerHTML = "&nbsp;(" + ORMain.unit + ")"
                    }))
                }
            }
        })
    },
    getHelp: function() {
        new Ajax.Updater("layer_helpor_content","infodoc/helpv3.php"),
        $j("#layer_helpor").draggable({
            handle: "#layer_helpor_handle",
            containment: "document"
        }),
        $("layer_helpor").show(),
        ORMain.flgHelp = !0
    },
    hideHelp: function() {
        $("layer_helpor").hide(),
        ORMain.flgHelp = !1
    },
    getStartOR: function() {
        Modalbox.show("infodoc/orStartContent.php", {
            title: ORConstants.MODAL_TITLE,
            width: ORConstants.MODAL_MEDIUM_WIDTH,
            height: ORConstants.MODAL_SMALL_HEIGHT,
            overlayDuration: .1,
            overlayClose: !1
        })
    },
    closeStartOR: function(e) {
        $(e).hide()
    },
    showAlertInlineOR: function(e) {
        var t = '<div class="msg-calc"><span class="calc-warnings-title">Information</span><span class="calc-warnings">&nbsp;:&nbsp;' + e + "</span></div>";
        $("tab_plan_main_message").innerHTML = t,
        setTimeout(ORMain.clearAlertInlineOR, 1e4)
    },
    clearAlertInlineOR: function() {
        $("tab_plan_main_message").innerHTML = ""
    },
    showAlertOR: function(e) {
        var t = ORMain.winW / 2 - 250
          , n = ORMain.winH / 2 + getScrollOffset() - 125;
        e == MAXNB_OF_ROUTE_EXCEEDED ? e = $("maxroute").innerHTML : e == COD_PRINT_CHOICE && (e = $("print_choice").innerHTML),
        e += '<div style="width:100%;text-align:center;padding-top:5px;margin-top:10px;"><a href="#" onclick="javascript:ORMain.closeAlertOR(); return false;">' + CLOSE + "</a></div>",
        $("layer_alertor_content").innerHTML = e,
        $("layer_alertor").setStyle({
            left: t + "px",
            top: n + "px"
        }),
        $("layer_alertor").show()
    },
    closeAlertOR: function() {
        $("layer_alertor").hide()
    },
    changeHeaderSize: function(e) {},
    prt: function(e) {
        var t = ORRouteManager.getActiveRoute();
        if (0 == t.getDistance())
            return void ORMain.showAlertInlineOR(ALERT_NO_DATA_TO_PRINT);
        var n = ORRouteManager.getAllActiveRoute()
          , a = n.keys();
        if (e && 1 == a.length && t.getId() > 0)
            return void ORMain.showAlertOR(COD_PRINT_CHOICE);
        ORGMapToolsManager.isFullScreen() && ORGMapToolsManager.fullScreen(),
        ORMain.showHidePoiEditor(),
        $("orp-logo").src = "img/v3/homepage/logo-or-com2.png",
        $("top-zone").hide(),
        $("navigation").hide(),
        $("maintools").hide(),
        $("orp-rname").show(),
        $("page").addClassName("page-print"),
        $("fullcontent").removeClassName("fullcontent-nofs"),
        $("orp-legal").show(),
        $("but_pr").show(),
        $("footer").hide(),
        $("coord_maps").hide();
        var o = "orp-ele-gmap";
        if ($("detailsGMap").hide(),
        $("tab_plan_message").hide(),
        $("map-c").addClassName("map-cadre-prt"),
        $(o).show(),
        $(o).innerHTML = "",
        t.getId() < 0 && $("com_ms_gmaps").hide(),
        $("map").setStyle({
            width: ORMain.winA4Min + "px",
            height: ORMain.mapSizeH + "px"
        }),
        e) {
            var i = "<div class='orp-rname-bloc'>"
              , r = 0
              , s = 0;
            (ORGMapDecoratorManager.isShowColsR() || ORGMapDecoratorManager.isShowColsM()) && (s = 1);
            for (var l = 0, c = a.length; c > l; l++) {
                var u = n.get(a[l]);
                i += "<div class='orp-rname-details'>",
                i += "<div class='orp-color' style='background-color:" + u.getStrokeColor() + "'>&nbsp;</div>";
                var g = UNDEFINED;
                if (u.getId() > 0 && (g = u.getDetails().getName()),
                i += "<div class='orp-sum'>",
                i += "	<span class='orp-name'>" + g + "</span><br/>",
                u.getId() > 0 ? (i += "<span class='orp-rname-dis-value'>" + u.getDistance() + "</span><span class='unitsize'>" + ORMain.getUnit() + "</span><span>,&nbsp;" + ALTITUDE_POSITIVE_CHANGE + ":&nbsp;" + u.getDetails().getDenPlus() + "m</span>",
                i += "	<br/><span class='orp-id-lib'>[" + ID_ROUTE + ":&nbsp;" + u.getId() + "]</span>") : i += "<br/><span class='orp-rname-dis-value'>" + u.getDistance() + "</span><span class='unitsize'>" + ORMain.getUnit() + "</span>",
                i += "</div>",
                u.getId() > 0 && u.getDetails().isRoutePartner() && u.getDetails().partner.flg_activated ? i += "	<div class='orp-user-print'><img class='author-profile' src='img/v3/profile.png'><div style='float:right;'><a href='" + u.getDetails().getPartner().url_link_logo_map + "' onclick='javascript:ORParMain.go2(" + u.getId() + ',"LOGO-PRINT-ALL-GMAPS","' + u.getDetails().getPartner().codpartner + "\");window.open(this.href);return false;'><img class='orp-logo-partner-print' src='" + PARTNER_BASE_URL + u.getDetails().getPartner().url_logo_map + "'/></a></div></div>" : u.getId() > 0 && u.getDetails().isPublicUser() && (i += "	<div class='orp-user-print'><img class='author-profile' src='img/v3/profile.png'><div style='float:right;'><div class='orp-author'>" + u.getDetails().getLibUser() + "</div></div></div>"),
                i += "<br class='clear'/>",
                i += "</div>",
                (l + 1) % 2 == 0 && (i += ""),
                u.getId() > 0) {
                    r > 0 && ($(o).innerHTML += "<div class='orp-ele-separator'>&nbsp;</div>");
                    var s = 0;
                    (ORGMapDecoratorManager.isShowColsR() || ORGMapDecoratorManager.isShowColsM()) && (s = 1),
                    new Ajax.Request("elevation/sto.php",{
                        method: "post",
                        asynchronous: !1,
                        onSuccess: function(e) {
                            $(o).innerHTML += "<img src='elevation/findORElevation4.php?h=200&w=970&id=" + u.getId() + "&c=" + s + "&u=" + ORMain.getUnit() + "&det=1&t=" + e.responseText + "' alt='Profil Altimétrique openrunner.com'/>"
                        }
                    }),
                    r++
                }
            }
            i += "</div>",
            $("orp-rname").innerHTML = i
        } else {
            $("orp-rname").innerHTML = $("desc-title-4prt").innerHTML,
            $("content").addClassName("print-content-0"),
            $(o).innerHTML += $("desc-4prt").innerHTML,
            $("mynotes_pr").show(),
            $("mynotes_text_A4").innerHTML = $("desc-short-4prt").innerHTML;
            var s = 0;
            (ORGMapDecoratorManager.isShowColsR() || ORGMapDecoratorManager.isShowColsM()) && (s = 1),
            new Ajax.Request("elevation/sto.php",{
                method: "post",
                asynchronous: !1,
                onSuccess: function(e) {
                    $(o).innerHTML += "<img src='elevation/findORElevation4.php?h=200&w=970&id=" + t.getId() + "&c=" + s + "&u=" + ORMain.getUnit() + "&det=1&t=" + e.responseText + "' alt='Profil Altimétrique openrunner.com'/>"
                }
            });
            for (var l = 0, c = a.length; c > l; l++) {
                var u = n.get(a[l]);
                if (u.getId() != t.getId()) {
                    var p = u.getData();
                    null != p.getPoly() && p.getPoly().setMap(null);
                    for (var d = p.getTMarkStone().length, R = 0; d > R; R++)
                        p.getTMarkStone()[R].setMap(null)
                }
            }
        }
        var M = ORRouteManager.getNbActiveRoute()
          , O = 185;
        e && (M > 4 ? O = 205 : M > 2 && (O = 175)),
        $("header").setStyle({
            height: O + "px"
        }),
        google.maps.event.trigger(ORMain.map, "resize")
    },
    canPrt: function() {
        $("orp-logo").src = "img/v3/homepage/logo-or-240m.png",
        $("top-zone").show(),
        $("navigation").show(),
        $("maintools").show(),
        $("orp-rname").hide(),
        $("orp-legal").hide(),
        $("but_pr").hide(),
        $("page").removeClassName("page-print"),
        $("fullcontent").addClassName("fullcontent-nofs"),
        $("mynotes_pr") && $("mynotes_pr").hide(),
        $("content").removeClassName("print-content-0");
        var e = ORRouteManager.getActiveRoute();
        ORGMapToolsManager.showHideDetails(),
        $("tab_plan_message").show(),
        $("orp-ele-gmap").innerHTML = "",
        $("orp-ele-gmap").hide(),
        $("orp-desc-gmap").innerHTML = "",
        $("orp-desc-gmap").hide(),
        $("map-c").removeClassName("map-cadre-prt"),
        $("map").setStyle({
            width: ORMain.mapSizeW + "px",
            height: ORMain.mapSizeH + "px"
        });
        for (var t = ORRouteManager.getAllActiveRoute(), n = t.keys(), a = 0, o = n.length; o > a; a++) {
            var i = t.get(n[a]);
            if (i.getId() != e.getId()) {
                var r = i.getData();
                null != r.getPoly() && r.getPoly().setMap(ORMain.map);
                for (var s = r.getTMarkStone().length, l = 0; s > l; l++)
                    r.getTMarkStone()[l].setMap(ORMain.map)
            }
        }
        $("coord_maps").show(),
        $("footer").show(),
        $("header").setStyle({
            height: "155px"
        })
    },
    prtA4: function(e) {
        $("header").setStyle({
            height: "0px"
        }),
        $("header").hide();
        var t = ORRouteManager.getActiveRoute();
        ORGMapToolsManager.isFullScreen() && ORGMapToolsManager.fullScreen(),
        ORMain.showHidePoiEditor(),
        $("logozone").hide(),
        $("tab_plan_message").hide(),
        $("top-zone").hide(),
        $("navigation").hide(),
        $("maintools").hide(),
        $("page").addClassName("page-print-A4"),
        $("fullcontent").removeClassName("fullcontent-nofs"),
        $("orp-legal").show(),
        $("but_prA4").show(),
        $("footer").hide(),
        $("coord_maps").hide();
        var n = Math.ceil(30 / (ORMain.pr4EleWidth / t.getDistance()));
        5 > n ? n = 5 : n > 5 && 10 > n ? n = 10 : n > 10 && 15 > n ? n = 15 : n > 15 && 20 > n ? n = 20 : n > 20 && 25 > n ? n = 25 : n > 25 && 30 > n ? n = 30 : n > 30 && (n = 50);
        var a = t.getData().getPoly()
          , o = new google.maps.LatLngBounds;
        if (null != a) {
            var i = a.getPath();
            i.forEach(function(e, t) {
                o.extend(e)
            });
            var r = ORMain.map.getProjection().fromLatLngToPoint(o.getNorthEast())
              , s = ORMain.map.getProjection().fromLatLngToPoint(o.getSouthWest())
              , l = Math.abs(r.x - s.x)
              , c = Math.abs(r.y - s.y)
        }
        var u = ORMain.winA4Max;
        "P" == e ? ORMain.prA4L = !1 : "L" == e ? ORMain.prA4L = !0 : l > c ? ORMain.prA4L = !0 : ORMain.prA4L = !1,
        ORMain.prA4L ? ($("map").setStyle({
            width: ORMain.winA4Max - 220 + "px",
            height: ORMain.winA4Min - ORMain.winA4Red - 50 + "px"
        }),
        $("map-c").addClassName("map-cadre-prt-A4L"),
        $("logozone_prA4L").show(),
        $("orp_name_A4L").show(),
        $("orp_name_A4L").innerHTML = $("desc-title-4prtA4L").innerHTML,
        $("mynotes_text_A4L").innerHTML = $("desc-short-4prt").innerHTML,
        $("mynotes_prA4L").show(),
        new Ajax.Request("elevation/sto.php",{
            method: "post",
            asynchronous: !1,
            onSuccess: function(e) {
                $("ele_prA4L").show(),
                $("ele_prA4L").innerHTML = "<img src='elevation/findORElevation4.php?h=133&w=390&id=" + t.getId() + "&c=0&p=1&det=1&u=" + ORMain.getUnit() + "&k=" + n + "&t=" + e.responseText + "' alt='Profil Altimétrique openrunner.com'/>"
            }
        }),
        $("qrcode_A4L").innerHTML = "<img src='https://chart.googleapis.com/chart?chs=135x135&cht=qr&chl=https%3A%2F%2Fwww.openrunner.com%2Findex.php?id=" + t.getId() + "&choe=UTF-8' title='Link to openrunner.com' />") : ($("map").setStyle({
            width: ORMain.winA4Min + "px",
            height: ORMain.winA4Max - ORMain.winA4PRed + "px"
        }),
        $("map-c").addClassName("map-cadre-prt-A4"),
        $("logozone_prA4").show(),
        $("orp_name_A4").show(),
        $("orp_name_A4").innerHTML = $("desc-title-4prtA4").innerHTML,
        $("mynotes_prA4").show(),
        $("mynotes_text_A4").innerHTML = $("desc-short-4prt").innerHTML,
        new Ajax.Request("elevation/sto.php",{
            method: "post",
            asynchronous: !1,
            onSuccess: function(e) {
                $("ele_prA4").show(),
                $("ele_prA4").innerHTML = "<img src='elevation/findORElevation4.php?h=133&w=390&id=" + t.getId() + "&c=0&p=1&det1&u=" + ORMain.getUnit() + "&k=" + n + "&t=" + e.responseText + "' alt='Profil Altimétrique openrunner.com'/>"
            }
        }),
        $("qrcode_A4").innerHTML = "<img src='https://chart.googleapis.com/chart?chs=135x135&cht=qr&chl=https%3A%2F%2Fwww.openrunner.com%2Findex.php?id=" + t.getId() + "&choe=UTF-8' title='Link to openrunner.com' />",
        u = ORMain.winA4Min),
        $("page").setStyle({
            width: u + "px",
            marginLeft: "0px"
        }),
        $("fullcontent").setStyle({
            width: u + "px",
            marginLeft: "0px"
        }),
        $("detailsGMap").hide(),
        $("tab_plan_message").hide(),
        google.maps.event.trigger(ORMain.map, "resize")
    },
    canPrtA4: function() {
        $("header").setStyle({
            height: "155px"
        }),
        $("header").show(),
        $("logozone").show(),
        $("tab_plan_message").show(),
        $("top-zone").show(),
        $("navigation").show(),
        $("maintools").show(),
        $("page").removeClassName("page-print-A4"),
        $("orp-legal").hide(),
        $("but_prA4").hide(),
        $("footer").show(),
        $("mynotes_prA4").hide(),
        $("coord_maps").show(),
        ORMain.prA4L ? ($("logozone_prA4L").hide(),
        $("orp_name_A4L").hide(),
        $("orp_name_A4L").innerHTML = "",
        $("mynotes_prA4L").hide(),
        $("mynotes_text_A4L").innerHTML = "",
        $("ele_prA4L").hide(),
        $("map-c").removeClassName("map-cadre-prt-A4L")) : ($("logozone_prA4").hide(),
        $("orp-rname").hide(),
        $("orp-rname").innerHTML = "",
        $("mynotes_prA4").hide(),
        $("mynotes_text_A4").innerHTML = "",
        $("ele_prA4").hide(),
        $("map-c").removeClassName("map-cadre-prt-A4")),
        $("map").setStyle({
            width: ORMain.mapSizeW + "px",
            height: ORMain.mapSizeH + "px"
        }),
        $("detailsGMap").show(),
        $("tab_plan_message").show(),
        $("map-c").removeClassName("map-cadre-prt-A4"),
        $("page").setStyle({
            width: ORMain.winA4Min + "px",
            marginLeft: "auto",
            marginRight: "auto"
        }),
        $("fullcontent").setStyle({
            width: "100%",
            marginLeft: "auto",
            marginRight: "auto"
        }),
        $("fullcontent").addClassName("fullcontent-nofs"),
        google.maps.event.trigger(ORMain.map, "resize"),
        ORMain.prA4L = !1
    },
    refreshUserPref: function() {
        null != ORMain.map && (1 == ORMain.wheelmouse ? ORMain.map.setOptions({
            scrollwheel: !0
        }) : ORMain.map.setOptions({
            scrollwheel: !1
        }),
        0 == ORRouteManager.getActiveRoute().getDistance() && (ORMain.map.setCenter(new google.maps.LatLng(ORMain.centerPoint.getLat(),ORMain.centerPoint.getLng())),
        ORMain.map.setZoom(12)),
        ORMain.changeUnit(ORMain.unit))
    },
    showIdLink: function() {
        $("idlink_entry").value = "https://www.openrunner.com/index.php?id=" + ORRouteManager.getActiveRoute().getId(),
        $("layer_idlink").show(),
        $("idlink_entry").focus,
        $("idlink_entry").select()
    },
    setWSize: function() {
        "CSS1Compat" == document.compatMode && document.documentElement && document.documentElement.offsetWidth && (ORMain.winW = document.documentElement.offsetWidth,
        ORMain.winH = document.documentElement.offsetHeight),
        window.innerWidth && window.innerHeight && (ORMain.winW = window.innerWidth,
        ORMain.winH = window.innerHeight),
        ORMain.mapIgnSizeH = ORConstants.WIN_HEIGHT_FIX - 10,
        ORMain.mapSizeH = ORConstants.WIN_HEIGHT_FIX - 25,
        ORMain.winWFS = ORMain.winW - 45,
        ORMain.winHFS = ORMain.winH - 90,
        ORMain.winWFSIGN = ORMain.winW - 95,
        ORMain.winHFSIGN = ORMain.winH - 150,
        ORMain.winH < ORConstants.WIN_HEIGHT_MIN && (ORMain.winHFS = ORConstants.WIN_HEIGHT_MIN,
        ORMain.winHFSIGN = ORConstants.WIN_HEIGHT_MIN),
        ORMain.winHFSIGN * ORMain.winWFSIGN > ORConstants.WINFS_IGN_DIM1_MAX * ORConstants.WINFS_IGN_DIM2_MAX && (ORMain.winWFSIGN > ORMain.winHFSIGN ? (ORMain.winWFSIGN > ORConstants.WINFS_IGN_DIM1_MAX && (ORMain.winWFSIGN = ORConstants.WINFS_IGN_DIM1_MAX),
        ORMain.winHFSIGN > ORConstants.WINFS_IGN_DIM2_MAX && (ORMain.winHFSIGN = ORConstants.WINFS_IGN_DIM2_MAX)) : (ORMain.winHFSIGN > ORConstants.WINFS_IGN_DIM1_MAX && (ORMain.winHFSIGN = ORConstants.WINFS_IGN_DIM1_MAX),
        ORMain.winWFSIGN > ORConstants.WINFS_IGN_DIM2_MAX && (ORMain.winWFSIGN = ORConstants.WINFS_IGN_DIM2_MAX))),
        ORConstants.MODAL_XSMALL_HEIGHT = ORConstants.MODAL_REF_XSMALL_HEIGHT,
        ORConstants.MODAL_SMALL_HEIGHT = ORConstants.MODAL_REF_SMALL_HEIGHT,
        ORConstants.MODAL_MEDIUM_HEIGHT = ORConstants.MODAL_REF_MEDIUM_HEIGHT,
        ORConstants.MODAL_LARGE_HEIGHT = ORConstants.MODAL_REF_LARGE_HEIGHT,
        ORMain.winW <= ORConstants.WIN_WIDTH_MIN || ORMain.winH <= ORConstants.WIN_HEIGHT_MIN ? (ORMain.allowedFS = !1,
        ORGMapToolsManager.isFullScreen() && ORGMapToolsManager.fullScreen(),
        ORMain.winH <= ORConstants.MODAL_REF_XSMALL_HEIGHT && (ORConstants.MODAL_XSMALL_HEIGHT = ORMain.winH - ORConstants.MODAL_BOTTOM_MARGIN),
        ORMain.winH <= ORConstants.MODAL_REF_SMALL_HEIGHT && (ORConstants.MODAL_SMALL_HEIGHT = ORMain.winH - ORConstants.MODAL_BOTTOM_MARGIN),
        ORMain.winH <= ORConstants.MODAL_REF_MEDIUM_HEIGHT && (ORConstants.MODAL_MEDIUM_HEIGHT = ORMain.winH - ORConstants.MODAL_BOTTOM_MARGIN),
        ORMain.winH <= ORConstants.MODAL_REF_LARGE_HEIGHT && (ORConstants.MODAL_LARGE_HEIGHT = ORMain.winH - ORConstants.MODAL_BOTTOM_MARGIN)) : (ORMain.allowedFS = !0,
        ORGMapToolsManager.isFullScreen() && ORGMapToolsManager.fullScreen())
    },
    activatePoiUserSaveImg: function() {
        $("savepoiuserdata").addClassName("tools-savepoiuserdata-a"),
        $("savepoiuserdata").removeClassName("tools-savepoiuserdata-i"),
        ORRouteManager.activatePoiUserChange(!0)
    },
    deactivatePoiUserSaveImg: function() {
        $("savepoiuserdata").addClassName("tools-savepoiuserdata-i"),
        $("savepoiuserdata").removeClassName("tools-savepoiuserdata-a"),
        ORRouteManager.activatePoiUserChange(!1),
        ORGMapDecoratorManager.removeAllPoiUser(),
        ORGMapDecoratorManager.showPoiUser()
    },
    setPoiUserSaveImg: function() {
        var e = ORRouteManager.getActiveRoute().getData().hasPoi2Save();
        e ? ($("savepoiuserdata").addClassName("tools-savepoiuserdata-a"),
        $("savepoiuserdata").removeClassName("tools-savepoiuserdata-i")) : ($("savepoiuserdata").addClassName("tools-savepoiuserdata-i"),
        $("savepoiuserdata").removeClassName("tools-savepoiuserdata-a"))
    },
    initialize: function() {
        ORMain.initialization = !0,
        ORMain.setWSize(),
        document.observe("or:tab-plan", function(e) {
            if (e) {
                var t = e.memo.bind;
                Event.stop(e),
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                ORElementShowableManager.showTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search"),
                ORMain.map.panTo(ORMain.map.getCenter()),
                ORGMapToolsManager.showHideDetails(),
                google.maps.event.trigger(ORMain.map, "resize"),
                _gaq.push(["_trackPageview", "/plan/planGMaps.php"])
            }
        }),
        ORMain.links = $("tab_group_one").getElementsBySelector("li a"),
        ORMain.links.each(function(e) {
            if ("#tab_search" == e.hash)
                ORMain.tabMainManager.addTab(new Tab(e,!0,!1,"or:tab-search",loadSearchPage));
            else if ("#tab_plan" == e.hash)
                ORMain.tabMainManager.addTab(new Tab(e,!0,!1,"or:tab-plan",function() {}
                ));
            else if ("#tab_home" == e.hash) {
                var t = new Tab(e,!0,!1,"or:tab-home",function() {}
                );
                ORMain.tabMainManager.addTab(t)
            } else
                "#tab_doc" == e.hash ? ORMain.tabMainManager.addTab(new Tab(e,!0,!1,"or:tab-doc",function() {}
                )) : "#tab_partners" == e.hash ? ORMain.tabMainManager.addTab(new Tab(e,!0,!1,"or:tab-partners",function() {
                    new Ajax.Updater("tab_partners","i18n/partners.php",{
                        method: "get",
                        encoding: "UTF-8",
                        onFailure: function() {
                            ORMain.showAlertOR("Erreur!!!")
                        }
                    })
                }
                )) : "#tab_service" == e.hash ? ORMain.tabMainManager.addTab(new Tab(e,!0,!1,"or:tab-ser",function() {
                    new Ajax.Updater("tab_service","orservice/orservicetab.php",{
                        method: "get",
                        encoding: "UTF-8",
                        onFailure: function() {
                            ORMain.showAlertOR("Erreur!!!")
                        }
                    })
                }
                )) : "#tab_store" == e.hash ? ORMain.tabMainManager.addTab(new Tab(e,!0,!1,"or:tab-store",function() {
                    new Ajax.Updater("tab_store","store/store.php",{
                        method: "get",
                        encoding: "UTF-8",
                        onFailure: function() {
                            ORMain.showAlertOR("Erreur!!!")
                        }
                    })
                }
                )) : "#tab_myor" == e.hash && ORMain.tabMainManager.addTab(new Tab(e,!0,!1,"or:tab-myor",function() {}
                ))
        }),
        document.observe("or:tab", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e)
            }
        }),
        document.observe("or:tab-search", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORMain.changeHeaderSize(!1),
                ORMain.refreshTopBanner()
            }
        }),
        document.observe("or:tab-plan", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORElementShowableManager.hideTabElements("tab-search"),
                ORMain.changeHeaderSize(!1),
                ORMain.refreshTopBanner()
            }
        }),
        document.observe("or:tab-myor", function(e) {
            if (e) {
                ORSearchMain.myorloaded || new Ajax.Updater("myordiv","account/myORSpace.php",{
                    method: "get",
                    encoding: "UTF-8",
                    evalScripts: !0,
                    onFailure: function() {
                        ORMain.showAlertOR("Erreur!!!")
                    },
                    onSuccess: function() {
                        ORSearchMain.myorloaded = !0
                    }
                });
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search"),
                ORMain.refreshTopBanner()
            }
        }),
        document.observe("or:tab-doc", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORIDocMain.loaded || ORIDocMain.viewDoc(),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search"),
                ORMain.refreshTopBanner()
            }
        }),
        document.observe("or:tab-doc-news", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORIDocMain.viewNews(),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search")
            }
        }),
        document.observe("or:tab-doc-fea", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORIDocMain.viewFeatures(),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search")
            }
        }),
        document.observe("or:tab-doc-doc", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORIDocMain.viewDoc(),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search")
            }
        }),
        document.observe("or:tab-doc-faq", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORIDocMain.viewFaq(),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search")
            }
        }),
        document.observe("or:tab-partners", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search")
            }
        }),
        document.observe("or:tab-doc-ser", function(e) {
            if (e) {
                var t = e.memo.bind;
                ORMain.changeHeaderSize(!1),
                ORMain.tabMainManager.setActiveTab(t),
                Event.stop(e),
                ORIDocMain.viewServices(),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search")
            }
            document.observe("or:tab-store", function(e) {
                if (e) {
                    var t = e.memo.bind;
                    ORMain.changeHeaderSize(!1),
                    ORMain.tabMainManager.setActiveTab(t),
                    Event.stop(e),
                    ORElementShowableManager.hideTabElements("tab-plan"),
                    ORElementShowableManager.hideTabElements("tab-search");
                }
            })
        }),
        document.observe("or:tab-home", function(e) {
            if (e) {
                var t = e.memo.bind;
                Event.stop(e),
                ORElementShowableManager.hideTabElements("tab-plan"),
                ORElementShowableManager.hideTabElements("tab-search"),
                ORMain.tabMainManager.setActiveTab(t),
                ORMain.changeHeaderSize(!0),
                ORMain.refreshTopBanner()
            }
        })
    },
    showDirectlyIGN: function() {
        google.maps.event.trigger(ORMain.map, "maptypeid_changed", ORConstants.TOPO_FRANCE),
        ORMain.customControl.setDefaultMap(ORConstants.TOPO_FRANCE)
    },
    refreshTopBanner: function() {}
}
  , ORGMapToolsManager = function() {
    function e(e) {
        !C || e ? (C = !0,
        $("draw").addClassName("tools-draw-a"),
        $("draw").removeClassName("tools-draw-i"),
        u(),
        p()) : t()
    }
    function t() {
        C = !1,
        $("draw").addClassName("tools-draw-i"),
        $("draw").removeClassName("tools-draw-a")
    }
    function n(e) {
        for (var t = -1, n = ORRouteManager.getActiveRoute().getData().getTMarkPoint().length, a = 0; n > a; ++a)
            if (ORRouteManager.getActiveRoute().getData().getTMarkPoint()[a] == e) {
                t = a;
                break
            }
        return t
    }
    function a() {
        L ? (L = !1,
        $("follow").addClassName("tools-follow-i"),
        $("follow").removeClassName("tools-follow-a"),
        ORGMapToolsManager.removePolyAuto(!0)) : (L = !0,
        $("follow").addClassName("tools-follow-a"),
        $("follow").removeClassName("tools-follow-i")),
        i(L),
        L && o(LANG)
    }
    function o(e) {
        var t = ORRouteManager.getActiveRoute();
        t.setRouting(!0),
        t.setRoutingMode(y),
        t.setRoutingEngine(k),
        ORDirUtils.calculateRoute(t, e, ORGMapToolsManager.redraw)
    }
    function i(e) {
        L = e
    }
    function r(e) {
        y = e;
        var t = "tools-follow-choice-" + e;
        "1" == e ? ($("follow_choice").removeClassName("tools-follow-choice-0"),
        $("follow_choice").removeClassName("tools-follow-choice-2")) : "2" == e ? ($("follow_choice").removeClassName("tools-follow-choice-0"),
        $("follow_choice").removeClassName("tools-follow-choice-1")) : ($("follow_choice").removeClassName("tools-follow-choice-1"),
        $("follow_choice").removeClassName("tools-follow-choice-2")),
        $("follow_choice").addClassName(t),
        L && o(LANG)
    }
    function s(e) {
        k = e,
        $("follow_choice").addClassName("tools-follow-choice-0"),
        $("follow_choice").removeClassName("tools-follow-choice-1"),
        L && o(LANG)
    }
    function l(e) {
        y = e
    }
    function c(n) {
        !w || n ? 0 == ORRouteManager.getActiveRoute().getData().getTPoint().length ? (ORMain.showAlertInlineOR(ALERT_START_INSERT),
        u(),
        e(!0)) : (p(),
        t(),
        w = !0,
        $("insert").addClassName("tools-insert-a"),
        $("insert").removeClassName("tools-insert-i")) : u()
    }
    function u() {
        var t = ORRouteManager.getActiveRoute().getData();
        null != t.getPointInsert() && (t.getTMarkPoint()[t.getPointInsert() - 1].setVisible(!0),
        t.setPointInsert(null),
        null != t.getMarkInsert() && (t.getMarkInsert().setMap(null),
        t.setMarkInsert(null))),
        w = !1,
        $("insert").addClassName("tools-insert-i"),
        $("insert").removeClassName("tools-insert-a"),
        C || e(!1)
    }
    function g(n) {
        !P || n ? 0 == ORRouteManager.getActiveRoute().getData().getTPoint().length ? (ORMain.showAlertInlineOR(NO_DATA_TO_REMOVE),
        p(),
        e(!0)) : (u(),
        t(),
        P = !0,
        $("remove").addClassName("tools-remove-a"),
        $("remove").removeClassName("tools-remove-i")) : p()
    }
    function p() {
        P = !1,
        $("remove").addClassName("tools-remove-i"),
        $("remove").removeClassName("tools-remove-a");
        var t = ORRouteManager.getActiveRoute().getData();
        if (null != t.getPointStartRemove()) {
            var n = t.getTMarkPoint()[t.getPointStartRemove()];
            n.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? n.setIcon(ORMain.ptIcon) : n.setIcon(ORMain.ptIconM),
            n.setDraggable(!0),
            t.setPointStartRemove(null)
        }
        if (null != t.getPointEndRemove()) {
            var n = t.getTMarkPoint()[t.getPointEndRemove()];
            n.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? n.setIcon(ORMain.ptIcon) : n.setIcon(ORMain.ptIconM),
            n.setDraggable(!0),
            t.setPointEndRemove(null)
        }
        null != t.getPolyRemove() && (t.getPolyRemove().setMap(null),
        delete t.getPolyRemove(),
        t.setPolyRemove(null)),
        C || e(!1)
    }
    function d(e, t, a) {
        var o;
        return 0 == t ? o = 0 == ORRouteManager.getActiveRoute().getData().getTPoint().length ? new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.startIcon,
            zIndex: 10
        }) : e.getORType() == ORConstants.TYPE_POINT_DEFAULT ? new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.ptIcon,
            zIndex: 10
        }) : new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.ptIconM,
            zIndex: 10
        }) : 1 == t ? (o = new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.startIcon,
            zIndex: 10
        }),
        ORRouteManager.getActiveRoute().getData().replaceMarkPoint(0, o)) : 2 == t ? (o = new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.ptIcon,
            zIndex: 10
        }),
        ORRouteManager.getActiveRoute().getData().replaceMarkPoint(ORRouteManager.getActiveRoute().getData().getTPoint().length - 1, o)) : 3 == t ? (o = 0 == a ? new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.startIcon,
            zIndex: 10
        }) : e.getORType() == ORConstants.TYPE_POINT_DEFAULT ? new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.ptIcon,
            zIndex: 10
        }) : new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.ptIconM,
            zIndex: 10
        }),
        ORRouteManager.getActiveRoute().getData().replaceMarkPoint(a, o)) : 4 == t && (o = 0 == a ? new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.startIcon,
            zIndex: 10
        }) : e.getORType() == ORConstants.TYPE_POINT_DEFAULT ? new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.ptIcon,
            zIndex: 10
        }) : new google.maps.Marker({
            position: e,
            draggable: !0,
            icon: ORMain.ptIconM,
            zIndex: 10
        }),
        ORRouteManager.getActiveRoute().getData().getTMarkPoint().splice(a, 1, o)),
        0 == t && (w ? null != ORRouteManager.getActiveRoute().getData().getPointInsert() && (ORRouteManager.getActiveRoute().getData().getTPoint().splice(ORRouteManager.getActiveRoute().getData().getPointInsert(), 0, e),
        ORRouteManager.getActiveRoute().getData().insertMarkPoint(ORRouteManager.getActiveRoute().getData().getPointInsert(), o)) : (ORRouteManager.getActiveRoute().getData().getTPoint().push(e),
        ORRouteManager.getActiveRoute().getData().addMarkPoint(o))),
        google.maps.event.addListener(o, "dragstart", function(e) {
            S = n(o)
        }),
        google.maps.event.addListener(o, "drag", function(e) {
            var t = e.latLng
              , n = ORRouteManager.getActiveRoute().getData().getTPoint()[S]
              , a = new google.maps.LatLng(t.lat(),t.lng());
            a.setORType(n.getORType()),
            o.setPosition(a),
            ORRouteManager.getActiveRoute().getData().getTPoint()[S] = a,
            ORRouteManager.getActiveRoute().getData().getTMarkPoint()[S] = o,
            L || document.fire("or:redrawRouteGMapEvent")
        }),
        google.maps.event.addListener(o, "dragend", function(e) {
            L ? ORDirUtils.calculateRoute(ORRouteManager.getActiveRoute(), LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
        }),
        google.maps.event.addListener(o, "rightclick", function(e) {
            e.latLng && (P ? R(o) : ORGMapToolsManager.changePointType(o))
        }),
        new LongPress(o,2e3),
        google.maps.event.addListener(o, "longpress", function(e) {
            e.latLng && (P ? R(o) : ORGMapToolsManager.changePointType(o))
        }),
        google.maps.event.addListener(o, "click", function(e) {
            var t = n(o)
              , a = ORRouteManager.getActiveRoute();
            if (!P & !w)
                a.getTPointDir().length > 0 ? (ORMain.iwin.setContent(NO_DISTANCE_INFO),
                ORMain.iwin.open(ORMain.map, o)) : (tmpdis = ORUtils.calculateDistanceFromStart(a.getData().getTPoint(), t, ORMain.getUnit()).toFixed(3),
                ORMain.iwin.setContent(POINT + " #<b> " + t + " </b><br> " + DISTANCE_DEPARTURE + " : " + tmpdis + " " + ORMain.getUnit()),
                ORMain.iwin.open(ORMain.map, o));
            else if (P)
                a.getData().getTPoint().splice(t, 1),
                a.getData().removeMarkPoint(t),
                o.setMap(null),
                L ? ORDirUtils.calculateRoute(a, LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent");
            else if (w)
                if (null == a.getData().getPointInsert()) {
                    pt = o.getPosition();
                    for (var i = a.getData().getTPoint().length, r = 0; i > r; r++)
                        if (pt.equals(a.getData().getTPoint()[r])) {
                            a.getData().setPointInsert(r + 1),
                            a.getData().getTMarkPoint()[r].setVisible(!1);
                            break
                        }
                    a.getData().setMarkInsert(new google.maps.Marker({
                        position: o.getPosition(),
                        draggable: !1,
                        icon: ORMain.insertIcon,
                        zIndex: 10
                    })),
                    a.getData().getMarkInsert().setMap(ORMain.map),
                    L ? ORDirUtils.calculateRoute(a, LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
                } else
                    ORMain.showAlertInlineOR(ACTION_MAP_INSERT)
        }),
        o
    }
    function R(e) {
        var t = n(e)
          , a = ORRouteManager.getActiveRoute()
          , o = a.getData();
        if (null == o.getPointStartRemove())
            o.setPointStartRemove(t),
            e.setIcon(ORMain.insertIcon),
            e.setDraggable(!1);
        else if (null == o.getPointEndRemove()) {
            o.setPointEndRemove(t),
            e.setIcon(ORMain.insertIcon),
            e.setDraggable(!1);
            var i = o.getPointStartRemove()
              , r = null;
            o.getPointStartRemove() > o.getPointEndRemove() ? (i = o.getPointEndRemove(),
            r = o.getTPoint().slice(i, o.getPointStartRemove() + 1)) : r = o.getTPoint().slice(i, o.getPointEndRemove() + 1);
            var s = new google.maps.Polyline({
                path: r,
                strokeColor: "#000",
                strokeOpacity: .8,
                strokeWeight: 8,
                clickable: !0,
                editable: !1
            });
            s.setMap(ORMain.map),
            o.setPolyRemove(s),
            google.maps.event.addListener(s, "rightclick", function(e) {
                e.stop();
                var n = window.confirm(CONFIRM_REMOVE);
                if (n) {
                    var r = o.getTMarkPoint()[o.getPointStartRemove()];
                    r.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? r.setIcon(ORMain.ptIcon) : r.setIcon(ORMain.ptIconM),
                    r.setDraggable(!0),
                    r = o.getTMarkPoint()[o.getPointEndRemove()],
                    r.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? r.setIcon(ORMain.ptIcon) : r.setIcon(ORMain.ptIconM),
                    r.setDraggable(!0);
                    var s = Math.abs(o.getPointStartRemove() - o.getPointEndRemove()) - 1;
                    0 == s && (s = 1),
                    o.getTPoint().splice(i + 1, s),
                    o.removeNMarkPoint(i + 1, s),
                    o.setPointStartRemove(null),
                    o.setPointEndRemove(null),
                    L ? ORDirUtils.calculateRoute(a, LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
                } else {
                    var r = o.getTMarkPoint()[t];
                    r.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? r.setIcon(ORMain.ptIcon) : r.setIcon(ORMain.ptIconM),
                    r.setDraggable(!0),
                    o.setPointEndRemove(null)
                }
                o.getPolyRemove().setMap(null),
                delete o.getPolyRemove(),
                o.setPolyRemove(null)
            }),
            new LongPress(s,2e3),
            google.maps.event.addListener(s, "longpress", function(e) {
                e.stop();
                var n = window.confirm(CONFIRM_REMOVE);
                if (n) {
                    var r = o.getTMarkPoint()[o.getPointStartRemove()];
                    r.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? r.setIcon(ORMain.ptIcon) : r.setIcon(ORMain.ptIconM),
                    r.setDraggable(!0),
                    r = o.getTMarkPoint()[o.getPointEndRemove()],
                    r.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? r.setIcon(ORMain.ptIcon) : r.setIcon(ORMain.ptIconM),
                    r.setDraggable(!0);
                    var s = Math.abs(o.getPointStartRemove() - o.getPointEndRemove()) - 1;
                    0 == s && (s = 1),
                    o.getTPoint().splice(i + 1, s),
                    o.removeNMarkPoint(i + 1, s),
                    o.setPointStartRemove(null),
                    o.setPointEndRemove(null),
                    L ? ORDirUtils.calculateRoute(a, LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
                } else {
                    var r = o.getTMarkPoint()[t];
                    r.getPosition().getORType() == ORConstants.TYPE_POINT_DEFAULT ? r.setIcon(ORMain.ptIcon) : r.setIcon(ORMain.ptIconM),
                    r.setDraggable(!0),
                    o.setPointEndRemove(null)
                }
                o.getPolyRemove().setMap(null),
                delete o.getPolyRemove(),
                o.setPolyRemove(null)
            })
        } else
            ORMain.showAlertInlineOR(REMOVE_ZONE)
    }
    function M(e) {
        var t = n(e)
          , e = ORRouteManager.getActiveRoute().getData().getTMarkPoint()[t];
        e.setMap(null),
        ORRouteManager.getActiveRoute().getData().getTPoint()[t].changeORType(),
        d(ORRouteManager.getActiveRoute().getData().getTPoint()[t], 3, t),
        L && ORDirUtils.calculateRoute(ORRouteManager.getActiveRoute(), LANG, ORGMapToolsManager.redraw)
    }
    function O() {
        var e = []
          , t = ORRouteManager.getActiveRoute().getData();
        if (t.getTPoint().length > 1) {
            e = t.getTPoint().clone(),
            e.pop(),
            e.reverse();
            for (var n = 0; n < e.length; n++) {
                var a = new google.maps.LatLng(e[n].lat() + 1e-5,e[n].lng() + 1e-5);
                a.setORType(e[n].getORType()),
                t.addPoint(a);
                d(a, 2)
            }
            L ? ORDirUtils.calculateRoute(ORRouteManager.getActiveRoute(), LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
        }
    }
    function h() {
        var e;
        if (ORRouteManager.getActiveRoute().getData().getTPoint().length > 1) {
            e = ORRouteManager.getActiveRoute().getData().getTPoint()[0];
            var t = new google.maps.LatLng(e.lat() + 1e-5,e.lng() + 1e-5);
            t.setORType(e.getORType()),
            ORRouteManager.getActiveRoute().getData().addPoint(t),
            d(t, 2),
            L ? ORDirUtils.calculateRoute(ORRouteManager.getActiveRoute(), LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
        }
    }
    function m() {
        var e = ORRouteManager.getActiveRoute().getData()
          , t = e.getTPoint();
        t.length > 1 && (ORGMapDecoratorManager.removeExtremesMarkers(),
        t.reverse(),
        e.getTMarkPoint().reverse(),
        d(t[0], 1),
        d(t[t.length - 1], 2),
        ORGMapDecoratorManager.showExtremesMarkers(),
        L ? ORDirUtils.calculateRoute(ORRouteManager.getActiveRoute(), LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent"))
    }
    function v() {
        ORRouteManager.getActiveRoute().getData().getTPoint().pop(),
        null != ORRouteManager.getActiveRoute().getData().getPoly() && ORRouteManager.getActiveRoute().getData().getPoly().setMap(null),
        ORRouteManager.getActiveRoute().getData().removeLastMarkPoint(),
        L ? ORDirUtils.calculateRoute(ORRouteManager.getActiveRoute(), LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
    }
    function f(e) {
        null != ORRouteManager.getActiveRoute().getData().getPoly() && ORRouteManager.getActiveRoute().getData().getPoly().length > 0 && ORRouteManager.getActiveRoute().getData().getPoly().setMap(null),
        ORRouteManager.getActiveRoute().setTPointDir([]),
        ORRouteManager.getActiveRoute().setRouting(!1),
        ORRouteManager.getActiveRoute().setRoutingDetails(null),
        e && document.fire("or:redrawRouteGMapEvent")
    }
    function _() {
        return ORRouteManager.destroyRoute()
    }
    function T() {
        null != ORRouteManager.getActiveRoute().getData().getPoly() && ORRouteManager.getActiveRoute().getData().getPoly().setMap(null)
    }
    function A() {
        T(),
        f(!1)
    }
    function I(e, t) {
        if (x || t) {
            x = !1;
            for (var n = 0, a = e.length; a > n; n++)
                Effect.Fade(e[n], {
                    duration: .1
                }),
                ORElementShowableManager.changeShownStatus("tab-plan", e[n], x);
            $("address").hide(),
            $("tools_openclose").addClassName("tools-openclose-c"),
            $("tools_openclose").removeClassName("tools-openclose-o")
        } else {
            x = !0;
            for (var n = 0, a = e.length; a > n; n++)
                Effect.Appear(e[n], {
                    duration: .1
                }),
                ORElementShowableManager.changeShownStatus("tab-plan", e[n], x);
            $("address").show(),
            $("tools_openclose").addClassName("tools-openclose-o"),
            $("tools_openclose").removeClassName("tools-openclose-c")
        }
        return !1
    }
    function D() {
        var e = ORRouteManager.getActiveRoute();
        if (U)
            null != e.getRoutingDetails() && ("" != e.getRoutingDetails().getWarnings() ? ORMain.showAlertInlineOR(e.getRoutingDetails().getWarnings()) : "" != e.getRoutingDetails().getStatusMessage() && ("ZERO_RESULTS" == e.getRoutingDetails().getStatusMessage() ? ORMain.showAlertInlineOR(ROUTING_ZERO_RESULTS) : ORMain.showAlertInlineOR(e.getRoutingDetails().getStatusMessage())));
        else {
            if (null != e.getDetails()) {
                if ($("com_ms_gmaps").hide(),
                $("detgmap").innerHTML = e.getDetails().getDomRep(),
                $("tab_plan_message").innerHTML = e.getDetails().getDomRepHeader(),
                ORAccMain.activateExportLink(),
                $("detgmap").show(),
                console.log($("detgmap").innerHTML),
                $("detgmap").innerHTML.evalScripts(),
                $("tab_plan_message").innerHTML.evalScripts(),
                ORMain.unit == UNIT_MILES) {
                    var t = ORConstants.UNIT_CONVERSION;
                    $$("#tab_plan span.distance").each(function(e) {
                        e.innerHTML = (e.innerHTML / t).toFixed(3)
                    }),
                    $$("#tab_plan td.di").each(function(e) {
                        e.innerHTML = (e.innerHTML / t).toFixed(3)
                    }),
                    $$("#tab_plan span.unitlabel").each(function(e) {
                        e.innerHTML = ORMain.unit
                    })
                }
                if (e.getDetails().isRoutePartner()) {
                    for (var n = $$(".desc-border"), a = 0, o = n.length; o > a; a++)
                        n[a].setStyle({
                            borderLeftColor: e.getDetails().partner.bg_color
                        });
                    e.getDetails().partner.flg_accomodation && ORGMapDecoratorManager.showPoiPartner(e.getDetails().partner.codpartner, !1, !0)
                } else
                    for (var n = $$(".desc-border"), a = 0, o = n.length; o > a; a++)
                        n[a].setStyle({
                            borderLeftColor: "#003117"
                        });
                $("detailsGMap").show()
            } else {
                $("detailsGMap").hide(),
                $("tab_plan_message").innerHTML = $("tab_plan_message_gmaps").innerHTML;
                for (var n = $$(".desc-border"), a = 0, o = n.length; o > a; a++)
                    n[a].setStyle({
                        borderLeftColor: "#003117"
                    });
                $("tab_plan_message").show(),
                $("com_ms_gmaps").show()
            }
            null != e.getRoutingDetails() && ("" != e.getRoutingDetails().getWarnings() ? ORMain.showAlertInlineOR(e.getRoutingDetails().getWarnings()) : "" != e.getRoutingDetails().getStatusMessage() && ("ZERO_RESULTS" == e.getRoutingDetails().getStatusMessage() ? ORMain.showAlertInlineOR(ROUTING_ZERO_RESULTS) : ORMain.showAlertInlineOR(e.getRoutingDetails().getStatusMessage())))
        }
    }
    function E() {
        U ? ($("map-c").removeClassName("map-cadre-fs"),
        $("maintools").removeClassName("maintools-fs"),
        $("map").setStyle({
            width: ORMain.mapSizeW + "px",
            height: ORMain.mapSizeH + "px"
        }),
        $("tools_search").removeClassName("search-map-place-fs"),
        U = !1,
        $("fullscreen").addClassName("tools-fullscreen-i"),
        $("fullscreen").removeClassName("tools-fullscreen-a"),
        $("fullcontent").removeClassName("fullcontent-fs"),
        $("fullcontent").addClassName("fullcontent-nofs"),
        $("tab_plan_main_message").removeClassName("message-main-fs"),
        $("footer").show(),
        $("navigation").show(),
        $("com_fs_gmaps").hide(),
        D()) : (_gaq.push(["_trackPageview", "/plan/planGMAPS_FS.php"]),
        $("map-c").addClassName("map-cadre-fs"),
        $("maintools").addClassName("maintools-fs"),
        $("map").setStyle({
            width: ORMain.winWFS + "px",
            height: ORMain.winHFS + "px"
        }),
        $("tools_search").addClassName("search-map-place-fs"),
        $("fullscreen").addClassName("tools-fullscreen-a"),
        $("fullscreen").removeClassName("tools-fullscreen-i"),
        $("detailsGMap").hide(),
        $("navigation").hide(),
        $("fullcontent").removeClassName("fullcontent-nofs"),
        $("fullcontent").addClassName("fullcontent-fs"),
        $("tab_plan_main_message").addClassName("message-main-fs"),
        $("footer").hide(),
        $("com_fs_gmaps").setStyle({
            left: ORMain.winWFS + 48 + "px"
        }),
        $("com_fs_gmaps").show(),
        $("com_ms_gmaps").hide(),
        U = !0),
        google.maps.event.trigger(ORMain.map, "resize")
    }
    var C = !0
      , w = !1
      , P = !1
      , S = -1
      , L = !1
      , y = (ORConstants.TYPE_POINT_DEFAULT,
    0)
      , k = 1
      , x = !0
      , G = !1
      , U = !1;
    return {
        startDraw: function(t) {
            e(t)
        },
        stopDraw: function() {
            t()
        },
        isStartedDraw: function() {
            return C
        },
        startInsert: function(e) {
            c(e)
        },
        stopInsert: function() {
            u()
        },
        isStartedInsert: function() {
            return w
        },
        startRemove: function(e) {
            g(e)
        },
        stopRemove: function() {
            p()
        },
        isStartedRemove: function() {
            return P
        },
        changeRoutingAuto: function() {
            a()
        },
        setRoutingAuto: function(e) {
            i(e)
        },
        isRoutingAuto: function() {
            return L
        },
        changeRoutingMode: function(e) {
            r(e)
        },
        setRoutingMode: function(e) {
            l(e)
        },
        getRoutingMode: function() {
            return y
        },
        changeRoutingEngine: function(e) {
            s(e)
        },
        setRoutingEngine: function(e) {
            _setRoutingEngine(e)
        },
        getRoutingEngine: function() {
            return k
        },
        getFlgRoutingAuto: function() {
            return L
        },
        unlockRouteAuto: function() {
            ORDirUtils.unLock()
        },
        changePointType: function(e) {
            M(e)
        },
        getRouteAuto: function(e) {
            o(e)
        },
        callbackRouting: function() {
            _callbackRouting()
        },
        removePolyAuto: function(e) {
            f(e)
        },
        goBackToHome: function() {
            O()
        },
        goDirectToHome: function() {
            h()
        },
        reverseRoute: function() {
            m()
        },
        undoLast: function() {
            v()
        },
        createTPoint: function(e) {
            d(e, 0)
        },
        draw: function(e, t) {
            if (C || w) {
                if (w && null == ORRouteManager.getActiveRoute().getData().getPointInsert())
                    return ORMain.showAlertInlineOR(ALERT_INSERT_NOT_DEFINED),
                    !1;
                e.setORType(t || ORConstants.TYPE_POINT_DEFAULT);
                var n = d(e, 0)
                  , a = ORRouteManager.getActiveRoute();
                ORGMapDecoratorManager.isShowMarkers() && (null == a.getData().getMarkerCluster() && (a.getData().setMarkerCluster(new MarkerClusterer(ORMain.map,b.getTMarkPoint())),
                a.getData().getMarkerCluster().setMaxZoom(MC_MAX_ZOOM),
                a.getData().getMarkerCluster().setMinimumClusterSize(MC_MIN_CSIZE)),
                a.getData().getMarkerCluster().addMarker(n)),
                a.getTPointDir().length > 0 && a.setTPointDir([]),
                L ? (a.setRoutingMode(y),
                a.setRoutingEngine(k),
                ORDirUtils.calculateRoute(a, LANG, ORGMapToolsManager.redraw)) : document.fire("or:redrawRouteGMapEvent")
            }
        },
        redraw: function(e) {
            e && Event.stop(e);
            var t = ORRouteManager.getActiveRoute()
              , n = t.getData();
            n.getPoly() && null != n.getPoly() && n.getPoly().setMap(null);
            var a = ORRouteUtils.calculatePolyline(t);
            n.setPoly(a),
            null != a && a.setMap(ORMain.map);
            var o = ORRouteUtils.calculateDistance(t);
            if (t.setDistance(o),
            G) {
                if (G = !1,
                a) {
                    for (var i = a.getPath(), r = new google.maps.LatLngBounds, s = 0, l = i.length; l > s; s++)
                        r.extend(i.getAt(s));
                    ORMain.map.fitBounds(r)
                }
                t.setDistanceRef(o)
            }
            D(),
            ORMain.cgRouteManager.updateDistance(t.getId(), t.getDistance()),
            n.setTPointFixed(ORRouteUtils.calculateStonesPosition(t, ORGMapDecoratorManager.getStepStones())),
            ORSearchMain.unLockLoad(),
            document.fire("or:recalculateDistanceAndStonesGMapEvent")
        },
        redrawAllRoutes: function(e) {
            e && Event.stop(e);
            for (var t = ORRouteManager.getActiveRoute().getId(), n = ORRouteManager.getAllActiveRoute(), a = n.keys(), o = 0; o < a.length; o++) {
                var i = n.get(a[o]);
                null != i.getData().getPoly() && i.getData().getPoly().setMap(null);
                var r = ORRouteUtils.calculatePolyline(i);
                null != r && (i.getData().setPoly(r),
                i.getData().getPoly().setMap(ORMain.map)),
                i.setDistance(ORRouteUtils.calculateDistance(i)),
                i.getId() == t && (i.getId() < 0 && i.getTypeRoute() != ORConstants.TYPE_ROUTE_GPS && ORGMapDecoratorManager.showMarkers(!0),
                i.getData().getHPoi().values().length > 0 && ORGMapDecoratorManager.showPoiUser(),
                D()),
                ORMain.cgRouteManager.updateDistance(i.getId(), i.getDistance())
            }
            document.fire("or:recalculateAllRoutesDistanceAndStonesGMapEvent")
        },
        createDraggMarker: function(e, t, n) {
            d(e, t, n)
        },
        destroyRoute: function() {
            if (window.confirm(CONFIRM_DESTROY)) {
                if (null != ORMain.mkp && (ORMain.mkp.setMap(null),
                ORMain.mkp = null),
                ORPOIUserManager.isEditorOpen() && ORPOIUserManager.showHideEditor(!0),
                ORMain.cgRouteManager.removeRoute(ORRouteManager.getActiveRoute().getId()),
                ORGMapDecoratorManager.removeMarkers(),
                ORGMapDecoratorManager.removeStones(),
                ORGMapDecoratorManager.removePoiMarkers(),
                ORGMapDecoratorManager.removeAllPoiUser(),
                ORGMapDecoratorManager.clearProfilePoly(),
                A(),
                _())
                    ORMain.cgRouteManager.addRoute(ORRouteManager.getActiveRoute().getId(), ORRouteManager.getActiveRoute().getStrokeColor(), ORRouteManager.getActiveRoute().getStrokeOpacity(), ORRouteManager.getActiveRoute().getStrokeWidth(), ORMain.getUnit());
                else {
                    ORMain.cgRouteManager.setActiveRoute(ORRouteManager.getActiveRoute().getId());
                    var e = ORRouteUtils.getFirstORLatLng(ORRouteManager.getActiveRoute());
                    null != e && ORMain.map.panTo(e),
                    D()
                }
                ORAccMain.checkRoutesRights(),
                ORRouteManager.getActiveRoute().getId() < 0 && ORRouteManager.getActiveRoute().getTypeRoute() != ORConstants.TYPE_ROUTE_GPS ? ORGMapDecoratorManager.showMarkers(!0) : ORGMapDecoratorManager.hideMarkers()
            }
        },
        cleanRoute: function() {
            window.confirm(CONFIRM_CLEAN) && (null != ORMain.mkp && (ORMain.mkp.setMap(null),
            ORMain.mkp = null),
            A(),
            ORGMapDecoratorManager.cleanRoute(),
            ORRouteManager.initRoute(),
            ORMain.cgRouteManager.updateDistance(ORRouteManager.getActiveRoute().getId(), ORRouteManager.getActiveRoute().getDistance().toFixed(3)))
        },
        createNewRoute: function() {
            ORGMapDecoratorManager.hideMarkers(),
            ORGMapDecoratorManager.clearProfilePoly(),
            ORPOIUserManager.isEditorOpen() && ORPOIUserManager.showHideEditor(!0),
            ORRouteManager.createNewActiveRoute() && ORMain.cgRouteManager.addRoute(ORRouteManager.getActiveRoute().getId(), ORRouteManager.getActiveRoute().getStrokeColor(), ORRouteManager.getActiveRoute().getStrokeOpacity(), ORRouteManager.getActiveRoute().getStrokeWidth(), ORMain.getUnit()),
            ORRouteManager.getActiveRoute().getId() < 0 && ORRouteManager.getActiveRoute().getTypeRoute() != ORConstants.TYPE_ROUTE_GPS && (ORGMapDecoratorManager.showMarkers(!0),
            ORGMapToolsManager.startDraw(!0))
        },
        changeActiveRoute: function(e) {
            null != ORMain.mkp && (ORMain.mkp.setMap(null),
            ORMain.mkp = null),
            ORGMapDecoratorManager.hideMarkers(),
            ORGMapDecoratorManager.clearProfilePoly(),
            ORPOIUserManager.isEditorOpen() && ORPOIUserManager.showHideEditor(!0),
            ORRouteManager.changeActiveRoute(e),
            ORAccMain.checkRoutesRights(),
            ORRouteManager.getActiveRoute().getId() < 0 && ORRouteManager.getActiveRoute().getTypeRoute() != ORConstants.TYPE_ROUTE_GPS ? (ORGMapDecoratorManager.showMarkers(!0),
            ORGMapToolsManager.startDraw(!0)) : ORGMapToolsManager.stopDraw(),
            D();
            var t = ORRouteManager.getActiveRoute().getData();
            if (t.getPoly()) {
                for (var n = t.getPoly().getPath(), a = new google.maps.LatLngBounds, o = 0, i = n.length; i > o; o++)
                    a.extend(n.getAt(o));
                ORMain.map.fitBounds(a)
            }
        },
        loadRoute: function(e) {
            if (ORRouteManager.isAlreadyLoaded(e))
                ORRouteManager.getActiveRoute().getData().getType() == ORConstants.TYPE_IGN && ORMain.gmap_step_slider.slider("option", "value", ORMain.valRevKMMap[ORRouteManager.getActiveRoute().getStepStones()]),
                ORMain.tabMainManager.setActiveTab("tab_plan"),
                ORElementShowableManager.showTabElements("tab-plan"),
                google.maps.event.trigger(ORMain.map, "resize"),
                ORRouteManager.getActiveRoute().getId() != e && ORMain.cgRouteManager.changeActiveRoute("routegc_" + e),
                ORSearchMain.unLockLoad();
            else {
                t();
                var n = ORRouteManager.getActiveRoute();
                n.getId() < 0 && 0 == n.getDistance() ? (A(),
                ORGMapDecoratorManager.removeStones(),
                ORMain.cgRouteManager.removeRoute(n.getId())) : ORGMapDecoratorManager.clearProfilePoly(),
                ORGMapDecoratorManager.freezePoiUser(),
                ORMain.iwinpoied.close();
                try {
                    ORRouteManager.load(e)
                } catch (a) {
                    return ORMain.showAlertOR(a),
                    ORSearchMain.unLockLoad(),
                    !1
                }
                if (n = ORRouteManager.getActiveRoute(),
                ORMain.gmap_step_slider.slider("option", "value", ORMain.valRevKMMap[n.getStepStones()]),
                ORAccMain.checkRoutesRights(),
                ORMain.cgRouteManager.addRoute(n.getId(), n.getStrokeColor(), n.getStrokeOpacity(), n.getStrokeWidth(), ORMain.getUnit()),
                ORGMapDecoratorManager.hideMarkers(),
                ORMain.tabMainManager.setActiveTab("tab_plan"),
                ORElementShowableManager.showTabElements("tab-plan"),
                -1 == ORMain.directID && I(["tools_draw", "tools_save", "routegc_main_add", "tools_search"], !0),
                null != n.getData().getTPoint() && n.getData().getTPoint().length > 0) {
                    var o = n.getData().getTPoint()[0];
                    ORMain.map.panTo(new google.maps.LatLng(o.lat(),o.lng()))
                }
                n.getData().getHPoi().values().length > 0 && ORGMapDecoratorManager.showPoiUser(),
                G = !0,
                n.isRouting() ? (ORMain.cgRoutingGMap.setEngine(n.getRoutingEngine()),
                y = n.getRoutingMode(),
                k = n.getRoutingEngine(),
                ORMain.cgRoutingGMap.getEngineOR().changeSelectedMode(n.getRoutingMode()),
                ORDirUtils.calculateRoute(n, LANG, ORGMapToolsManager.redraw),
                google.maps.event.trigger(ORMain.map, "zoom_changed")) : (google.maps.event.trigger(map, "resize"),
                document.fire("or:redrawRouteGMapEvent"))
            }
            return !0
        },
        loadGPSInput: function() {
            ORRouteManager.getNbActiveRoute() < ORConstants.NB_ROUTE_LIMIT || 0 == ORRouteManager.getActiveRoute().getDistance() ? Modalbox.show("import/loadGPSInput.php", {
                title: "www.openrunner.com",
                width: ORConstants.MODAL_MEDIUM_WIDTH,
                height: ORConstants.MODAL_SMALL_HEIGHT,
                overlayClose: !1
            }) : ORMain.showAlertOR(MAXNB_OF_ROUTE_EXCEEDED)
        },
        loadGPSRoute: function(e) {
            ORGMapDecoratorManager.hideMarkers(),
            ORGMapDecoratorManager.freezePoiUser(),
            ORMain.iwinpoied.close();
            var t = !1;
            return 0 != ORRouteManager.getActiveRoute().getDistance() && (t = !0),
            ORRouteManager.getNbActiveRoute() < ORConstants.NB_ROUTE_LIMIT ? (ORRouteManager.loadGPSData(e),
            t && ORMain.cgRouteManager.addRoute(ORRouteManager.getActiveRoute().getId(), ORRouteManager.getActiveRoute().getStrokeColor(), ORRouteManager.getActiveRoute().getStrokeOpacity(), ORRouteManager.getActiveRoute().getStrokeWidth(), ORMain.getUnit()),
            G = !0,
            ORRouteManager.getActiveRoute().getData().getHPoi().values().length > 0 && ORGMapDecoratorManager.showPoiUser(),
            void document.fire("or:redrawRouteGMapEvent")) : (ORMain.showAlertOR(MAXNB_OF_ROUTE_EXCEEDED),
            !1)
        },
        saveRoute: function() {
            ORRouteManager.save()
        },
        updateRoute: function() {
            ORRouteManager.update()
        },
        reloadRoute: function(e) {
            ORRouteManager.getActiveRoute().getId() < 0 && (A(),
            ORGMapDecoratorManager.removeStones(),
            ORMain.cgRouteManager.removeRoute(ORRouteManager.getActiveRoute().getId())),
            ORGMapDecoratorManager.hideMarkers(),
            ORRouteManager.load(e),
            ORMain.cgRouteManager.addRoute(ORRouteManager.getActiveRoute().getId(), ORRouteManager.getActiveRoute().getStrokeColor(), ORRouteManager.getActiveRoute().getStrokeOpacity(), ORRouteManager.getActiveRoute().getStrokeWidth(), ORMain.getUnit()),
            ORGMapDecoratorManager.hideMarkers(),
            G = !0,
            ORRouteManager.getActiveRoute().isRouting() ? ORDirUtils.calculateRoute(ORRouteManager.getActiveRoute(), LANG, ORGMapToolsManager.redraw) : document.fire("or:redrawRouteGMapEvent")
        },
        reloadRouteDetails: function(e) {
            ORRouteManager.reLoadDetails(e),
            ORAccMain.addIDS(e),
            ORAccMain.checkRoutesRights(),
            D()
        },
        changeRouteSettings: function(e) {
            var t = ORRouteManager.getActiveRoute()
              , n = parseInt(e.width, 10)
              , a = .5 * Math.floor(t.getStrokeWidth() / 3) + 1
              , o = "M " + -2 * a + "," + 3 * a + " 0," + -3 * a + " " + 2 * a + "," + 3 * a + " 0," + 2.25 * a + " z"
              , i = {
                path: o,
                fillColor: t.getStrokeColor(),
                strokeColor: t.getStrokeColor(),
                fillOpacity: t.getStrokeOpacity(),
                strokeOpacity: t.getStrokeOpacity(),
                scale: 2
            };
            if (e.color && (i.fillColor = e.color,
            i.strokeColor = e.color,
            ORRouteManager.changeRouteColor(e.color),
            null != t.getData().getPoly() && t.getData().getPoly().setOptions({
                strokeColor: e.color,
                icons: [{
                    icon: i,
                    offset: "50px",
                    repeat: "150px"
                }]
            }),
            ORMain.cgRouteManager.changeColor(e.color)),
            e.opacity && (i.fillOpacity = e.opacity,
            i.strokeOpacity = e.opacity,
            ORRouteManager.changeRouteOpacity(e.opacity),
            null != t.getData().getPoly() && t.getData().getPoly().setOptions({
                strokeOpacity: parseFloat(e.opacity),
                icons: [{
                    icon: i,
                    offset: "50px",
                    repeat: "150px"
                }]
            })),
            e.width) {
                var n = parseInt(e.width, 10)
                  , a = .5 * Math.floor(n / 3) + 1;
                i.path = "M " + -2 * a + "," + 3 * a + " 0," + -3 * a + " " + 2 * a + "," + 3 * a + " 0," + 2.25 * a + " z",
                ORRouteManager.changeRouteWidth(e.width),
                null != t.getData().getPoly() && t.getData().getPoly().setOptions({
                    strokeWeight: n,
                    icons: [{
                        icon: i,
                        offset: "50px",
                        repeat: "150px"
                    }]
                })
            }
            ORGMapDecoratorManager.redrawStones(null)
        },
        slide: function(e, t) {
            return I(e, t)
        },
        fullScreen: function() {
            x ? (ORGMapToolsManager.slide(["tools_draw", "tools_search"], !1),
            setTimeout(function() {
                E(),
                ORGMapToolsManager.slide(["tools_draw", "tools_search"], !1)
            }, 100)) : E()
        },
        isFullScreen: function() {
            return U
        },
        showHideDetails: function() {
            D()
        }
    }
}()
  , ORGMapDecoratorManager = function() {
    function e() {
        var e = ORRouteManager.getActiveRoute();
        e.getData().getMarkerCluster() && e.getData().getMarkerCluster().clearMarkers(),
        x = !1,
        $("show").addClassName("tools-show-i"),
        $("show").removeClassName("tools-show-a")
    }
    function t() {
        var e = ORRouteManager.getActiveRoute();
        e.getData().getMarkerCluster().clearMarkers()
    }
    function n() {
        var e = ORRouteManager.getActiveRoute().getData().getTMarkPoint().length;
        if (e > 1) {
            var t = ORRouteManager.getActiveRoute().getData().getTMarkPoint()[0];
            null != t && t.setMap(null),
            t = ORRouteManager.getActiveRoute().getData().getTMarkPoint()[e - 1],
            null != t && t.setMap(null)
        }
    }
    function a() {
        if (x) {
            var e = ORRouteManager.getActiveRoute().getData().getTMarkPoint().length;
            e > 1 && (ORRouteManager.getActiveRoute().getData().getTMarkPoint()[0].setMap(ORMain.map),
            ORRouteManager.getActiveRoute().getData().getTMarkPoint()[e - 1].setMap(ORMain.map))
        }
    }
    function o(t) {
        var n = ORRouteManager.getActiveRoute();
        if (!x || t) {
            x = !0;
            var a = n.getData()
              , o = a.getTMarkPoint().length
              , i = a.getTPoint().length;
            if (0 == o && n.getDistance() > 0 || o != i) {
                a.setTMarkPoint([]);
                for (var r = 0; i > r; r++)
                    ORGMapToolsManager.createDraggMarker(a.getTPoint()[r], 4, r);
                n.getData().getMarkerCluster().clearMarkers(),
                n.getData().getMarkerCluster().addMarkers(n.getData().getTMarkPoint()),
                n.getData().getMarkerCluster().repaint(),
                o = a.getTMarkPoint().length
            } else
                n.getDistance() > 0 && (a.getMarkerCluster().addMarkers(a.getTMarkPoint()),
                a.getMarkerCluster().repaint());
            $("show").addClassName("tools-show-a"),
            $("show").removeClassName("tools-show-i")
        } else
            e()
    }
    function i() {
        if (F > 0) {
            G = !0,
            s(),
            c(F);
            for (var e = ORRouteManager.getActiveRoute().getData().getTMarkStone().length, t = 0; e > t; t++) {
                var n = ORRouteManager.getActiveRoute().getData().getTMarkStone()[t];
                null != n && n.setMap(null),
                n.setMap(ORMain.map)
            }
        } else
            r()
    }
    function r() {
        s(),
        G = !1
    }
    function s() {
        for (var e = ORRouteManager.getActiveRoute().getData().getTMarkStone().length, t = 0; e > t; t++) {
            var n = ORRouteManager.getActiveRoute().getData().getTMarkStone()[t];
            null != n && n.setMap(null)
        }
        ORRouteManager.getActiveRoute().getData().setTMarkStone([])
    }
    function l(e) {
        var t = new google.maps.Marker({
            position: e,
            draggable: !1,
            icon: ORMain.ptIconP,
            optimized: !1,
            visible: !0,
            zIndex: 5
        });
        return t
    }
    function c(e) {
        var t = null
          , n = ORRouteManager.getActiveRoute()
          , a = n.getData().getTPointFixed().length
          , o = '<svg version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink" width="25px" height="42px" viewBox="0 0 260 420"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><circle stroke="{{ color }}" stroke-width="12" stroke-position="inside" fill="#FFF" cx="127" cy="127" r="122"></circle><path d="M127,260 L127,415" id="Line" stroke="#000" stroke-width="12" stroke-linecap="square"></path></g><g><text y="138" x="130" text-anchor="middle" alignment-baseline="middle" font-family="Arial" font-weight="bold" font-size="110">{{ poskm }}</text></g></svg>';
        o = o.replace("{{ color }}", n.getStrokeColor());
        for (var i = 0; a > i; i++) {
            0 == i && (t = ORMain.startKmIcon);
            var r = i * e;
            svg_km = o.replace("{{ poskm }}", r.toString());
            var s = n.getData().getTPointFixed()[i];
            0 == i ? marker = new google.maps.Marker({
                position: new google.maps.LatLng(s.lat(),s.lng()),
                icon: t,
                zIndex: 6
            }) : marker = new google.maps.Marker({
                position: new google.maps.LatLng(s.lat(),s.lng()),
                optimized: !1,
                zIndex: 10,
                icon: {
                    url: "data:image/svg+xml;charset=UTF-8;base64," + btoa(svg_km),
                    scaledSize: new google.maps.Size(25,42)
                }
            }),
            n.getData().addMarkStone(marker);
        }
        if (a > 0) {
            t = ORMain.lastKmIcon;
            var l = null;
            l = null != n.getTPointDir() && n.getTPointDir().length > 0 ? n.getTPointDir()[n.getTPointDir().length - 1] : n.getData().getTPoint()[n.getData().getTPoint().length - 1],
            marker = new google.maps.Marker({
                position: l,
                icon: t,
                zIndex: 7
            }),
            ORRouteManager.getActiveRoute().getData().addMarkStone(marker)
        }
    }
    function u() {
        for (var e = ORRouteManager.getActiveRoute().getData().getTMarkAcc(), t = 0; t < e.length; t++)
            e[t].setMap(null)
    }
    function g(e) {
        !H || e ? ($("showutm").addClassName("tools-showutm-a"),
        $("showutm").removeClassName("tools-showutm-i"),
        H = !0,
        ORMainUTM.displayUTMGrid(ORMain.map) ? $("tab_plan_main_message").innerHTML = "" : ORMain.showAlertInlineOR(ZOOM_LEVEL_UTM)) : p()
    }
    function p() {
        ORMainUTM.hideUTMGrid(),
        $("tab_plan_main_message").innerHTML = "",
        $("showutm").addClassName("tools-showutm-i"),
        $("showutm").removeClassName("tools-showutm-a"),
        H = !1
    }
    function d(e, t) {
        if (!U || t) {
            var n = ORMain.map.getBounds()
              , a = n.getSouthWest()
              , o = n.getNorthEast()
              , i = findPoiCol(a.lat(), o.lat(), a.lng(), o.lng(), e, 0);
            if (null != i) {
                for (var r = 0, s = B.length; s > r; r++)
                    B[r].setMap(null);
                B = [];
                for (var s = i.length, r = 0; s > r; r++) {
                    var l = i[r];
                    point = new google.maps.LatLng(parseFloat(l.lat),parseFloat(l.lng));
                    var c = ""
                      , u = "";
                    ORAccMain.hasPassInUserList(l.idpoi, "D") ? u = "checked='checked'" : ORAccMain.hasPassInUserList(l.idpoi, "TD") && (c = "checked='checked'");
                    var g = "<div style='float:left;width:60px'><a href='https://www.centcols.org' target='_blank'><img src='img/par/logo_centcols_w100.png' style='width:50px'/></a></div><div style='float:left;width:145px;'><div style='font-weight:bold;'>" + unescape(l.nom) + "</div><div style='font-size:18px;padding-bottom:5px'>" + l.alt + "m</div></div><div><input type='checkbox' name='td' value='1' onchange=javascript:ORAccMain.addPass(\"" + l.idpoi + '",' + l.alt + ',"D"); ' + u + ">&nbsp;" + MY_PASSES_DONE + "</input><input style='margin-left:25px;' type='checkbox' name='td' onchange=javascript:ORAccMain.addPass(\"" + l.idpoi + '",' + l.alt + ',"TD"); ' + c + ">&nbsp;" + MY_PASSES_TODO + "</input></div><br style='clear:both'/>"
                      , p = "<div style='float:left;width:60px'><a href='https://www.centcols.org' target='_blank'><img src='img/par/logo_centcols_w100.png' style='width:50px'/></a></div><div style='float:left;width:145px;'><div style='font-weight:bold;'>" + unescape(l.nom) + "</div><div style='font-size:18px;padding-bottom:5px'>" + l.alt + "m</div></div><br style='clear:both'/>"
                      , d = "0";
                    l.alt > 2e3 && (d = "2000");
                    var M = h(point, g, p, d, l.idpoi);
                    B.push(M),
                    M.setMap(ORMain.map)
                }
            }
            U = !0,
            $("showcol").addClassName("mountains-showcol-r-a"),
            $("showcol").removeClassName("mountains-showcol-r-i")
        } else
            R()
    }
    function R() {
        for (var e = B.length, t = 0; e > t; t++)
            B[t].setMap(null);
        U = !1,
        $("showcol").addClassName("mountains-showcol-r-i"),
        $("showcol").removeClassName("mountains-showcol-r-a"),
        B = []
    }
    function M(e, t) {
        if (!N || t) {
            var n = ORMain.map.getBounds()
              , a = n.getSouthWest()
              , o = n.getNorthEast()
              , i = findPoiCol(a.lat(), o.lat(), a.lng(), o.lng(), e, 1);
            if (null != i) {
                for (var r = 0, s = j.length; s > r; r++)
                    j[r].setMap(null);
                j = [];
                for (var s = i.length, r = 0; s > r; r++) {
                    var l = i[r];
                    point = new google.maps.LatLng(parseFloat(l.lat),parseFloat(l.lng));
                    var c = ""
                      , u = "";
                    ORAccMain.hasPassInUserList(l.idpoi, "D") ? u = "checked='checked'" : ORAccMain.hasPassInUserList(l.idpoi, "TD") && (c = "checked='checked'");
                    var g = "<div style='float:left;width:60px'><a href='https://www.centcols.org' target='_blank'><img src='img/par/logo_centcols_w100.png' style='width:50px'/></a></div><div style='float:left;width:145px;'><div style='font-weight:bold;'>" + unescape(l.nom) + "</div><div style='font-size:18px;padding-bottom:5px'>" + l.alt + "m</div></div><div><input type='checkbox' name='td' value='1' onchange=javascript:ORAccMain.addPass(\"" + l.idpoi + '",' + l.alt + ',"D"); ' + u + ">" + MY_PASSES_DONE + "</input><input style='margin-left:25px;' type='checkbox' name='td' onchange=javascript:ORAccMain.addPass(\"" + l.idpoi + '",' + l.alt + ',"TD"); ' + c + ">" + MY_PASSES_TODO + "</input></div><br style='clear:both'/>"
                      , p = "<div style='float:left;width:60px'><a href='https://www.centcols.org' target='_blank'><img src='img/par/logo_centcols_w100.png' style='width:50px'/></a></div><div style='float:left;width:145px;'><div style='font-weight:bold;'>" + unescape(l.nom) + "</div><div style='font-size:18px;padding-bottom:5px'>" + l.alt + "m</div></div><br style='clear:both'/>"
                      , d = h(point, g, p, l.dif_cc, l.idpoi);
                    j.push(d),
                    d.setMap(ORMain.map)
                }
            }
            N = !0,
            $("showcolm").addClassName("mountains-showcol-m-a"),
            $("showcolm").removeClassName("mountains-showcol-m-i")
        } else
            O()
    }
    function O() {
        for (var e = j.length, t = 0; e > t; t++)
            j[t].setMap(null);
        N = !1,
        $("showcolm").addClassName("mountains-showcol-m-i"),
        $("showcolm").removeClassName("mountains-showcol-m-a"),
        j = []
    }
    function h(e, t, n, a, o) {
        var i = ORMain.colMIcon;
        switch (a) {
        case "0":
            i = ORMain.colRIcon;
            break;
        case "1":
            i = ORMain.colMIcon;
            break;
        case "2":
        case "3":
            i = ORMain.colM2Icon;
            break;
        case "35":
        case "40":
        case "50":
            i = ORMain.colM40Icon;
            break;
        case "2000":
            i = ORMain.colR2000Icon;
            break;
        default:
            i = ORMain.colMIcon
        }
        var r = "";
        ORAccMain.hasPassInUserList(o, "D") ? r = '<i class="map-icon-pass-done">•</i>' : ORAccMain.hasPassInUserList(o, "TD") && (r = '<i class="map-icon-pass-todo">•</i>');
        var s = new Marker({
            position: new google.maps.LatLng(e.lat(),e.lng()),
            icon: i,
            labelS: r,
            zIndex: 4
        });
        return google.maps.event.addListener(s, "mouseover", function() {
            ORMain.iwin.setContent(n),
            ORMain.iwin.open(ORMain.map, s)
        }),
        google.maps.event.addListener(s, "click", function() {
            ORMain.iwin.setContent(t),
            ORMain.iwin.open(ORMain.map, s)
        }),
        s
    }
    function m(e) {
        if (null != e) {
            var t = new google.maps.MarkerImage
              , n = e.getORMarker();
            t.url = n.getORIcon().getIconUrlI(),
            t.size = new google.maps.Size(n.getORIcon().getSizeW(),n.getORIcon().getSizeH()),
            t.anchor = new google.maps.Point(n.getORIcon().getAnchorX(),n.getORIcon().getAnchorY()),
            t.infoWindowAnchor = new google.maps.Point(n.getORIcon().getAnchorX(),n.getORIcon().getAnchorY());
            var a = new google.maps.Marker({
                raiseOnDrag: !1,
                draggable: !1,
                optimized: !1,
                position: n.getORLatLng(),
                icon: t,
                zIndex: 10
            });
            return google.maps.event.addListener(a, "mouseover", function() {
                ORMain.iwinpoied.close(),
                ORMain.iwinpoiro.close();
                var t = ORRouteManager.getActiveRoute().getData();
                if (ORPOIUserManager.isEditorOpen() && null != t.getPoiById(e.getPoiId()))
                    ORPOIUserManager.getActivePOIID() == e.getPoiId() || v(e.getPoiId());
                else if (e.getDisplayMode() == ORConstants.POI_MODE_POPUP) {
                    n.getApiMarker().getIcon().url = n.getORIcon().getIconUrlA(),
                    n.getApiMarker().setIcon(n.getApiMarker().getIcon());
                    var o = "poi-r-popup-desc";
                    "" == e.getUserDescription() && (o = "poi-r-popup-nodesc"),
                    ORMain.iwinpoiro.setContent("<div id='poipopup_ro' class='" + o + "'>" + e.getPoiId() + "</div>"),
                    ORMain.iwinpoiro.open(ORMain.map, a)
                } else
                    n.getApiMarker().getIcon().url = n.getORIcon().getIconUrlA(),
                    n.getApiMarker().setIcon(n.getApiMarker().getIcon())
            }),
            google.maps.event.addListener(a, "click", function() {
                ORMain.iwinpoied.close(),
                ORMain.iwinpoiro.close();
                var t = ORRouteManager.getActiveRoute().getData();
                ORPOIUserManager.isEditorOpen() && null != t.getPoiById(e.getPoiId()) && ORPOIUserManager.getActivePOIID() == e.getPoiId() && ORGMapDecoratorManager.displayPoiUserEdPopUp(ORPOIUserManager.getActivePOIID())
            }),
            google.maps.event.addListener(a, "mouseout", function() {
                ORPOIUserManager.isEditorOpen() || (n.getApiMarker().getIcon().url = n.getORIcon().getIconUrlI(),
                n.getApiMarker().setIcon(n.getApiMarker().getIcon()),
                ORMain.iwinpoiro.close())
            }),
            google.maps.event.addListener(a, "drag", function(e) {
                var t = e.latLng;
                $("poi_udet_lng").innerHTML = t.lng().toFixed(5),
                $("poi_udet_lat").innerHTML = t.lat().toFixed(5)
            }),
            google.maps.event.addListener(a, "dragend", function(t) {
                var n = t.latLng;
                e.getORMarker().setORLatLng(new google.maps.LatLng(n.lat(),n.lng())),
                document.fire("or:poiuser-modif")
            }),
            a
        }
        return null
    }
    function v(e) {
        f();
        var t = ORRouteManager.getActiveRoute().getData()
          , n = t.getPoiById(e)
          , a = n.getORMarker();
        a.getApiMarker().getIcon().url = a.getORIcon().getIconUrlA(),
        a.getApiMarker().setDraggable(!0),
        ORPOIUserManager.setActivePOIID(n.getPoiId(), a.getApiMarker().getPosition(), a.getORIcon().getIconUrlI(), null)
    }
    function f() {
        var e = ORPOIUserManager.getActivePOIID()
          , t = ORRouteManager.getActiveRoute().getData()
          , n = t.getHPoi().keys().length;
        if (null != e && n > 0) {
            var a = t.getPoiById(e)
              , o = a.getORMarker();
            o.getApiMarker().setMap(null),
            o.getApiMarker().getIcon().url = o.getORIcon().getIconUrlI(),
            o.getApiMarker().setDraggable(!1),
            o.getApiMarker().setMap(ORMain.map)
        }
        ORPOIUserManager.setActivePOIID(null, null, null, null)
    }
    function _(e) {
        if (window.confirm(CONFIRM_CLEAN_POI)) {
            var t = ORRouteManager.getActiveRoute().getData()
              , n = t.getHPoi().keys().length;
            if (null != e && n > 0) {
                var a = t.getHPoi().unset(e);
                a.getORMarker().getApiMarker().setMap(null)
            }
            ORPOIUserManager.setActivePOIID(null, null, null, null),
            document.fire("or:poiuser-modif")
        }
    }
    function T() {
        var e = ORRouteManager.getActiveRoute().getData()
          , t = e.getHPoi().values()
          , n = t.length;
        if (n > 0)
            for (var a = 0; n > a; a++)
                t[a].getORMarker().getApiMarker().setMap(null);
        A()
    }
    function A() {
        for (var e = ORRouteManager.getActiveRoute().getData(), t = e.getTLabel(); t.length > 0; ) {
            var n = t.pop();
            n.setVisible(!1),
            n.setContent("")
        }
    }
    function I() {
        for (var e = ORRouteManager.getActiveRoute().getData().getHPoi().values(), t = 0; t < e.length; t++) {
            var n = e[t]
              , a = m(n);
            n.getORMarker().setApiMarker(a),
            a.setMap(ORMain.map),
            E(n),
            C(n)
        }
    }
    function D(e) {
        ORMain.iwinpoied.close(),
        ORMain.iwinpoiro.close();
        var t = ORRouteManager.getActiveRoute().getData()
          , n = ORRouteManager.findPOIUserInRoutes(e);
        if (ORPOIUserManager.isEditorOpen() && null != t.getPoiById(n.getPoiId()) && ORPOIUserManager.getActivePOIID() == n.getPoiId()) {
            var a = "<div class='poi-w-popup or-form poi-container'>";
            a += "	<div class='navtab-details poi-tabs'>",
            a += "		<ul id='tab_poi_editor' class='subsection-tabs-details'>",
            a += "		    <li><a href='#poipopup_ed'>Description</a></li>",
            a += "		    <li><a href='#poipopup_opt'>Options</a></li>",
            a += "		    <li><a href='#poipopup_mod'>Modification du type</a></li>",
            a += "		</ul>",
            a += "	</div>",
            a += "	<div id='poipopup_ed'>" + n.getPoiId() + "</div>",
            a += "	<div id='poipopup_opt'>&nbsp;</div>",
            a += "	<div id='poipopup_mod' style='height:300px;overflow-x:hidden;overflow-y:auto'>&nbsp;</div>",
            a += "	</div>",
            ORMain.iwinpoied.setContent(a),
            ORMain.iwinpoied.open(ORMain.map, n.getORMarker().getApiMarker())
        }
    }
    function E(e) {
        if (e.gridPosition && "0000" != e.getGridPosition()) {
            var t = 0
              , n = 0
              , a = e.getORMarker().getORLatLng();
            "1" == e.getGridPosition().charAt(0) && (t = -ORConstants.POI_OFFSET_HORI),
            "1" == e.getGridPosition().charAt(1) && (n = ORConstants.POI_OFFSET_VERT),
            "1" == e.getGridPosition().charAt(2) && (t = ORConstants.POI_OFFSET_HORI),
            "1" == e.getGridPosition().charAt(3) && (n = -ORConstants.POI_OFFSET_VERT);
            var o = ORUtils.calculatePoiPositionInGrid(a.lat(), a.lng(), t, n, ORMain.map.getZoom());
            e.getORMarker().getApiMarker().setPosition(new google.maps.LatLng(o.lat,o.lng))
        }
    }
    function C(e) {
        if (e.getDisplayMode() == ORConstants.POI_MODE_LABEL_RIGHT || e.getDisplayMode() == ORConstants.POI_MODE_LABEL_LEFT) {
            var t = e.getORMarker()
              , n = new google.maps.Size(16,-35);
            e.getDisplayMode() == ORConstants.POI_MODE_LABEL_LEFT && (n = new google.maps.Size(-116,-35));
            var a = {
                content: '<div class="poi-mode-label"><div class="poi-label-text">' + e.getUserShortDescription() + "</div></div>",
                disableAutoPan: !0,
                pixelOffset: n,
                position: t.getApiMarker().getPosition(),
                closeBoxURL: "",
                isHidden: !1,
                enableEventPropagation: !1
            }
              , o = new InfoBox(a);
            o.open(ORMain.map);
            var i = ORRouteManager.getActiveRoute().getData();
            i.addLabel(o)
        }
    }
    function w(e) {
        for (var t = W.length, n = 0; t > n; n++)
            W[n].setMap(null);
        W = [],
        z = !1
    }
    function P(e, t, n) {
        S(e, t)
    }
    function S(e, t) {
        var n = []
          , a = ORRouteManager.getActiveRoute()
          , o = a.getDetails().partner;
        if (n = o.accpartner,
        null != n && !n.NONE) {
            var i = ORMain.accIcon;
            ORMain.accIcon.url = PARTNER_BASE_URL + o.url_img_acc,
            zindex = 1002;
            for (var r in n) {
                for (var s = n[r], l = null, c = [], u = [], g = 0; g < s.length; g++)
                    null == l && (l = new google.maps.LatLng(parseFloat(s[g].lat),parseFloat(s[g].lng))),
                    c.push(unescape(s[g].desc)),
                    u.push(s[g].idpoi);
                for (var p = "", r = 0, g = u.length; g > r; r++)
                    r > 0 && (p += ","),
                    p += u[r];
                var d = L(l, c, p, u[0], i, zindex, e);
                a.getData().addMarkAcc(d),
                d.setMap(ORMain.map)
            }
        }
    }
    function L(e, t, n, a, o, i, r) {
        var s = new google.maps.Marker({
            position: e,
            icon: o,
            zIndexProcess: function() {
                return 9e5
            }
        });
        return google.maps.event.addListener(s, "click", function() {
            ORMain.iwin.setContent("<div id='detpoi_gmaps'></div>"),
            ORMain.iwin.open(ORMain.map, s),
            google.maps.event.addListener(ORMain.iwin, "domready", function(e) {
                b(n, a, r)
            })
        }),
        s
    }
    function b(e, t, n) {
        var a = "poi-utils/showDetailPoiPartner.php";
        new Ajax.Request(a,{
            method: "post",
            parameters: {
                ids: e,
                id: t,
                type: n,
                map: "GMAPS"
            },
            asynchronous: !1,
            onSuccess: function(e) {
                if ("ko" == e.responseText)
                    $("detpoi_gmaps").innerHTML = "SERVICE NON DISPONIBLE";
                else {
                    $("detpoi_gmaps").innerHTML = e.responseText;
                    var t = $("poipartner_num").innerHTML;
                    $j(".photoslider-or").sliderkit({
                        auto: !1,
                        circular: !0,
                        shownavitems: t,
                        panelclick: !1,
                        panelfx: "sliding",
                        panelfxspeed: 1e3,
                        panelfxeasing: "easeInOutExpo"
                    })
                }
            }
        })
    }
    function y() {
        null != ORRouteManager.getActiveRoute().getData().getMarkProfile() && ORRouteManager.getActiveRoute().getData().getMarkProfile().setMap(null)
    }
    function k() {
        null != ORRouteManager.getActiveRoute().getData().getPolyProfile() && ORRouteManager.getActiveRoute().getData().getPolyProfile().setMap(null)
    }
    var x = !0
      , G = !1
      , U = !1
      , N = !1
      , H = !1
      , F = ORConstants.STEP_STONES_DEFAULT
      , z = !1
      , W = []
      , B = []
      , j = [];
    return {
        hideMarkers: function() {
            e()
        },
        showMarkers: function(e) {
            o(e)
        },
        showExtremesMarkers: function() {
            a()
        },
        isShowMarkers: function() {
            return x
        },
        isShowStones: function() {
            return G
        },
        isShowColsR: function() {
            return U
        },
        isShowColsM: function() {
            return N
        },
        isShowPoiPartner: function() {
            return z
        },
        removeMarkers: function() {
            t()
        },
        removeExtremesMarkers: function() {
            n()
        },
        hideStones: function() {
            r()
        },
        showStones: function() {
            i(force)
        },
        removeStones: function() {
            s()
        },
        createStonesMarker: function() {
            c()
        },
        hideColsR: function() {
            R()
        },
        hideColsM: function() {
            O()
        },
        showColsR: function(e, t) {
            d(e, t)
        },
        showColsM: function(e, t) {
            M(e, t)
        },
        hideUTMGrid: function() {
            p()
        },
        showUTMGrid: function(e) {
            g(e)
        },
        hidePoiPartner: function(e) {
            w(e)
        },
        showPoiPartner: function(e, t, n) {
            P(e, t, n)
        },
        showPoiPartnerDetail: function(e, t, n) {
            b(e, t, n)
        },
        showDistance: function() {
            _showDistance()
        },
        redrawStones: function(e) {
            i(),
            null != e && Event.stop(e)
        },
        setStepStones: function(e) {
            F = e
        },
        getStepStones: function(e) {
            return F
        },
        addPoiMarker: function(e) {
            _addPoiMarker(e)
        },
        removePoiMarker: function(e) {
            return _removePoiMarker(e)
        },
        removePoiMarkers: function() {
            return u()
        },
        createPoiMarker: function() {
            for (var e = ORRouteManager.getActiveRoute().getData().getHPoi().values(), t = 0; t < e.length; t++)
                _addPoiMarker(e[t])
        },
        getPoiFromId: function(e) {
            return _getPoiFromId(e)
        },
        addPoiUserMarker: function(e) {
            var t = ORPOIUserManager.getSelectedIcon().ico;
            if (null != t) {
                var n = new ORMarker(t)
                  , a = new ORPoi(t.getId(),e,n,t.getDesc(),t.getDescriptable())
                  , o = m(a);
                n.setApiMarker(o),
                ORRouteManager.getActiveRoute().getData().addPoi(a),
                v(a.getPoiId()),
                o.setMap(ORMain.map),
                document.fire("or:poiuser-modif")
            }
        },
        showPoiUser: function() {
            I()
        },
        removeAllPoiUser: function() {
            T()
        },
        removePoiUser: function(e) {
            _(e)
        },
        changePoiUserActive: function(e) {
            v(e)
        },
        freezePoiUser: function() {
            f()
        },
        displayPoiUserEdPopUp: function(e) {
            D(e)
        },
        drawProfileMarker: function(e) {
            var t = l(e);
            y(),
            ORRouteManager.getActiveRoute().getData().setMarkProfile(t),
            t.setMap(ORMain.map)
        },
        clearProfileMarker: function() {
            y()
        },
        drawProfilePoly: function(e, t) {
            var n = ORRouteManager.getActiveRoute()
              , a = ORRouteUtils.calculateProfilePolyline(n, e, t);
            k(),
            n.getData().setPolyProfile(a),
            a.setMap(ORMain.map)
        },
        clearProfilePoly: function() {
            k()
        },
        cleanRoute: function() {
            t(),
            s(),
            u()
        },
        redrawMarkersAndStones: function(e) {
            e && Event.stop(e),
            0 == F ? r() : ORRouteManager.getActiveRoute().getDistance() >= ORConstants.LIMIT_STONES_1KM && 5 > F ? ORMain.gmap_step_slider.slider("option", "value", ORMain.valRevKMMap[5]) : (ORRouteManager.getActiveRoute().getData().setTPointFixed(ORRouteUtils.calculateStonesPosition(ORRouteManager.getActiveRoute(), ORGMapDecoratorManager.getStepStones())),
            i())
        },
        redrawAllRoutesMarkersAndStones: function(e) {
            e && Event.stop(e);
            for (var t = ORRouteManager.getActiveRoute().getId(), n = ORRouteManager.getAllActiveRoute(), a = n.keys(), o = 0; o < a.length; o++) {
                ORRouteManager.changeActiveRoute(a[o]);
                var r = ORRouteManager.getActiveRoute();
                0 != F && (r.getDistance() >= ORConstants.LIMIT_STONES_1KM && 5 > F ? ORMain.gmap_step_slider.slider("option", "value", ORMain.valRevKMMap[5]) : (r.getData().setTPointFixed(ORRouteUtils.calculateStonesPosition(r, ORGMapDecoratorManager.getStepStones())),
                i()))
            }
            a.length > 1 && ORRouteManager.changeActiveRoute(t)
        },
        redraw: function(e) {
            U && d(e.memo.flgAlerte, U),
            N && M(e.memo.flgAlerte, N),
            z && P(POI_PARTNER_CODE_LOGIS, e.memo.flgAlerte, z),
            H && g(H);
            var t = ORRouteManager.getActiveRoute().getData()
              , n = t.getHPoi().values()
              , a = n.length;
            if (a > 0) {
                A();
                for (var o = 0; a > o; o++) {
                    var i = n[o];
                    E(i),
                    C(i)
                }
            }
            Event.stop(e)
        }
    }
}();
document.observe("or:redrawRouteGMapEvent", ORGMapToolsManager.redraw),
document.observe("or:redrawAllRoutesGMapEvent", ORGMapToolsManager.redrawAllRoutes),
document.observe("or:refreshDecoratorGMapEvent", ORGMapDecoratorManager.redraw),
document.observe("or:recalculateDistanceAndStonesGMapEvent", ORGMapDecoratorManager.redrawMarkersAndStones),
document.observe("or:recalculateAllRoutesDistanceAndStonesGMapEvent", ORGMapDecoratorManager.redrawAllRoutesMarkersAndStones),
ORMainUtils = {
    checkEnter: function(e, t) {
        var n;
        return n = ORMainUtils.getEvent(e),
        n && 13 == n ? ("loginform" == t ? ORAccMain.submitSignInForm() : "createaccount" == t ? ORAccMain.submitSignUpForm() : "updateaccount" == t ? ORAccMain.submitUpdateSignUpForm() : "lostaccessform" == t ? ORAccMain.submitLostAccessForm() : "createmessageform" == t ? ORAccMain.submitMessageForm() : "homesearchform" == t ? ORSearchMain.submitHomeSearchForm() : "saveroute" == t ? ORAccMain.submitSaveDataForm() : "updateroute" == t ? ORAccMain.submitUpdateDataForm() : "orse" == t ? ORSearchMain.searchRoute(0) : "selokw" == t ? ORSearchMain.searchSimpleRoutes() : "createcontactform" == t ? ORAccMain.submitContactForm() : "createshareform" == t ? ORAccMain.submitShareForm() : "ser01form" == t ? ORExpMain.submitSer01Form() : "ser02form" == t ? ORExpMain.submitSer02Form() : "ser03form" == t ? ORExpMain.submitSer03Form() : "ser04form" == t ? ORExpMain.submitSer04Form() : "ser05form" == t ? ORExpMain.submitSer05Form() : "ser08form" == t ? ORExpMain.submitSer08Form() : "ser09form" == t ? ORExpMain.submitSer09Form() : "subserform" == t ? ORAccMain.submitSubORSForm() : "orup" == t && ORAccMain.findUserLocation(document.getElementById("locu").value),
        !1) : !0
    },
    getEvent: function(e) {
        var t;
        return e && e.which ? t = e.which : (e = window.event,
        e && (t = e.keyCode)),
        t
    }
};
var mappoi = null, geocoderpoi = null, moverelated = !1, coucheCarteIGN, couchePhoto, coucheMarqueurs, ORSearchMain = {
    initializedMap: !1,
    mapstart: null,
    mapend: null,
    geocoder: null,
    cstart: null,
    cend: null,
    radstart: 5,
    radend: 5,
    gmap_step_slider: null,
    ign_step_slider: null,
    winH: 600,
    winW: 800,
    poly: null,
    selectedrow: null,
    selectedrowBG: null,
    validSimpleSearchForm: null,
    dcsearch: 0,
    mapqv: null,
    poly: null,
    loaded: !1,
    tabmf: "myormf",
    tabmm: "myormm",
    tabmp: "myormp",
    tabmps: "myormps",
    tabmr: "myormr",
    tabma: "myorma",
    tabsesi: "orsesi",
    tabsead: "orsead",
    pagese: "pagese",
    pagemy: "pagemy",
    mapW: 305,
    mapH: 280,
    mode: 0,
    qvopened: !1,
    qvid: -1,
    myorloaded: !1,
    initMaps: function() {
        if (!ORSearchMain.initializedMap) {
            ORSearchMain.initializedMap = !0;
            var e = {
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggableCursor: "crosshair",
                keyboardShortcuts: !0,
                zoomControl: !0,
                mapTypeControl: !1,
                scaleControl: !0,
                streetViewControl: !1,
                scrollwheel: !1,
                center: new google.maps.LatLng(ORMain.centerPoint.getLat(),ORMain.centerPoint.getLng()),
                zoom: 10,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM,
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                }
            };
            ORSearchMain.mapstart = new google.maps.Map(document.getElementById("mapsearch_start"),e);
            var t = {
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggableCursor: "crosshair",
                keyboardShortcuts: !0,
                zoomControl: !0,
                mapTypeControl: !1,
                scaleControl: !0,
                streetViewControl: !1,
                scrollwheel: !1,
                center: new google.maps.LatLng(ORMain.centerPoint.getLat(),ORMain.centerPoint.getLng()),
                zoom: 10,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM,
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                }
            };
            ORSearchMain.mapend = new google.maps.Map(document.getElementById("mapsearch_end"),t);
            var n = {
                strokeColor: "#000",
                strokeOpacity: .75,
                strokeWeight: 2,
                fillColor: "#D1E366",
                fillOpacity: .25,
                center: ORSearchMain.mapstart.getCenter(),
                map: ORSearchMain.mapstart,
                radius: 1e3 * ORSearchMain.radstart
            };
            ORSearchMain.cstart = new google.maps.Circle(n);
            var a = {
                strokeColor: "#000",
                strokeOpacity: .75,
                strokeWeight: 2,
                fillColor: "#D1E366",
                fillOpacity: .25,
                center: ORSearchMain.mapend.getCenter(),
                map: ORSearchMain.mapend,
                radius: 1e3 * ORSearchMain.radend
            }
              , o = document.getElementById("locs")
              , i = new google.maps.places.SearchBox(o);
            ORSearchMain.mapstart.controls[google.maps.ControlPosition.TOP_LEFT].push(o),
            i.addListener("places_changed", function() {
                var e = i.getPlaces();
                0 != e.length && (ORSearchMain.mapstart.setCenter(e[0].geometry.location),
                ORSearchMain.mapstart.setZoom(13))
            });
            var r = document.getElementById("loce")
              , s = new google.maps.places.SearchBox(r);
            ORSearchMain.mapend.controls[google.maps.ControlPosition.TOP_LEFT].push(r),
            s.addListener("places_changed", function() {
                var e = s.getPlaces();
                0 != e.length && (ORSearchMain.mapend.setCenter(e[0].geometry.location),
                ORSearchMain.mapend.setZoom(13))
            }),
            ORSearchMain.cend = new google.maps.Circle(a),
            google.maps.event.addListener(ORSearchMain.mapstart, "center_changed", function(e, t) {
                ORSearchMain.cstart.setRadius(1e3 * parseFloat($("rstart").innerHTML)),
                ORSearchMain.cstart.setCenter(ORSearchMain.mapstart.getCenter())
            }),
            google.maps.event.addListener(ORSearchMain.mapend, "center_changed", function(e, t) {
                ORSearchMain.cend.setRadius(1e3 * parseFloat($("rend").innerHTML)),
                ORSearchMain.cend.setCenter(ORSearchMain.mapend.getCenter())
            }),
            $("rstart").innerHTML = ORSearchMain.radstart.toFixed(1),
            $("rend").innerHTML = ORSearchMain.radend.toFixed(1),
            $("dismin").innerHTML = 0..toFixed(0),
            $("dismax").innerHTML = 175..toFixed(0);
            $j("#slider_dis").slider({
                range: !0,
                min: 0,
                max: 180,
                values: [0, 180],
                slide: function(e, t) {
                    $j("#dismin").html(t.values[0]),
                    $j("#dismax").html(t.values[1])
                }
            }),
            $j("#rstartSlider").slider({
                min: 1,
                max: 50,
                value: ORSearchMain.radstart,
                slide: function(e, t) {
                    ORSearchMain.cstart.setRadius(1e3 * parseFloat(t.value)),
                    ORSearchMain.cstart.setCenter(ORSearchMain.mapstart.getCenter()),
                    $j("#rstart").html(t.value.toFixed(1)),
                    ORSearchMain.radstart = t.value.toFixed(1)
                }
            }),
            $j("#rendSlider").slider({
                min: 1,
                max: 50,
                value: ORSearchMain.radend,
                slide: function(e, t) {
                    ORSearchMain.cend.setRadius(1e3 * parseFloat(t.value)),
                    ORSearchMain.cend.setCenter(ORSearchMain.mapend.getCenter()),
                    $j("#rend").html(t.value.toFixed(1)),
                    ORSearchMain.radend = t.value.toFixed(1)
                }
            })
        }
    },
    loadSearchPage: function() {
        $("loadingsiad").hide(),
        $("loadingsise").hide(),
        ORSearchMain.validSimpleSearchForm = new Validation("orse",{
            immediate: !0,
            onSubmit: !1
        }),
        Validation.add("validate-kw", VALIDATE_KW_SEARCH, function(e) {
            return ORSearchMain.validateKWSize(e)
        })
    },
    validateKWSize: function(e) {
        return "" == e || "" != e && e.length > 3
    },
    searchAdvancedRoutes: function() {
        $("siad").hide(),
        null != ORSearchMain.mapqv && (ORSearchMain.mapqv = null);
        var e = "search/searchRoutes.php";
        new Ajax.Request(e,{
            method: "post",
            parameters: ORSearchUtils.getSearchParams("a"),
            onSuccess: function(e) {
                $("loadingsiad").hide(),
                "empty" != e.responseText ? ($("searchcontent").innerHTML = e.responseText,
                $("searchcontent").innerHTML.evalScripts(),
                ORSearchUtils.initTable(0, !1)) : "ko" != e.responseText ? $("searchcontent").innerHTML = "Aucun parcours trouvé" : $("searchcontent").innerHTML = "Pb technique",
                $("siad").show()
            },
            onCreate: function() {
                $("loadingsiad").show()
            },
            onComplete: function() {
                $("loadingsiad").hide(),
                $("siad").show()
            }
        })
    },
    searchSimpleRoutes: function() {
        var e = ORSearchMain.validSimpleSearchForm.validate();
        if (!e)
            return !1;
        $("sise").hide();
        var t = "search/searchRoutes.php";
        new Ajax.Request(t,{
            method: "post",
            parameters: ORSearchUtils.getSearchParams("s"),
            onSuccess: function(e) {
                "empty" != e.responseText ? ($("searchcontent").innerHTML = e.responseText,
                $("searchcontent").innerHTML.evalScripts(),
                ORSearchUtils.initTable(0, !1)) : "ko" != e.responseText ? $("searchcontent").innerHTML = "Aucun parcours trouvé" : $("searchcontent").innerHTML = "Pb technique",
                $("sise").show()
            },
            onCreate: function() {
                $("loadingsise").show()
            },
            onComplete: function() {
                $("loadingsise").hide(),
                $("sise").show()
            }
        })
    },
    changeEndLocality: function() {
        var e = $F("cloce");
        switch (e) {
        case "0":
            $("actloce").show();
            break;
        case "1":
            $("actloce").show();
            break;
        case "2":
            $("actloce").hide();
            break;
        default:
            $("actloce").show()
        }
    },
    showRoute: function(e, t, n) {
        if (1 == ORSearchMain.dcsearch)
            return !1;
        if (ORSearchMain.lockLoad(e),
        2 != e && 4 != e) {
            var a = "#or_res_" + t;
            void 0 != ORSearchMain.selectedrow && ORSearchMain.selectedrow.children("td, th").removeClass("serCurRow"),
            ORSearchMain.selectedrow = $j(a),
            ORSearchMain.selectedrow.children("td, th").addClass("serCurRow")
        }
        if (0 == e || 3 == e) {
            var o = "or_res_" + t;
            if (ORSearchMain.qvopened && (ORSearchMain.closeQuickView(),
            ORSearchMain.unLockLoad()),
            ORSearchMain.qvid != t) {
                var i = '<tr class="or-qv">';
                i += '<td colspan="11">',
                i += '<div id="or_qv_map"></div>',
                i += '<div class="or-qv-desc-main"><div id="or_qv_prof"></div>',
                i += '<div id="or_qv_desc"></div>',
                i += '<div class="but-valid">',
                i += '<a class="or-button or-button-medium" href="" onclick="javascript:ORSearchMain.showRoute(1,' + t + ',this);return false;">' + DISPLAY_ROUTE + "</a>",
                i += '</div><br class="clear"/></div>',
                i += "</td></tr>",
                $(o).insert({
                    after: i
                }),
                setTimeout(function() {
                    ORSearchMain.quickview(t, n)
                }, 10)
            } else
                ORSearchMain.qvid = -1
        } else
            setTimeout(function() {
                ORMain.changeHeaderSize(!1),
                ORGMapToolsManager.loadRoute(t)
            }, 50)
    },
    unLockLoad: function() {
        ORSearchMain.dcsearch = 0,
        1 == ORSearchMain.mode ? $("loadinglayer-se").hide() : 2 == ORSearchMain.mode && $("orstats-bl").removeClassName("loadinglayer-st"),
        $("loadinglayer-mg").hide()
    },
    lockLoad: function(e) {
        ORSearchMain.mode = e,
        ORSearchMain.dcsearch = 0,
        1 == ORSearchMain.mode ? $("loadinglayer-se").show() : 2 == ORSearchMain.mode && $("orstats-bl").addClassName("loadinglayer-st"),
        $("loadinglayer-mg").show()
    },
    quickview: function(e, t) {
        var n = "search/displayRouteQV.php?id=" + e;
        new Ajax.Request(n,{
            method: "get",
            onSuccess: function(t) {
                var n = t.responseText.evalJSON(!0);
                ORSearchMain.initQVMap(),
                "" != n.points && ORSearchMain.loadTripForQV(n),
                $("or_qv_desc").innerHTML = n.routesdesc;
                var a = 5;
                n.distance > 0 && (a = Math.ceil(30 / (550 / n.distance))),
                5 > a ? a = 5 : a > 5 && 10 > a ? a = 10 : a > 10 && 15 > a ? a = 15 : a > 15 && 20 > a && (a = 20),
                $("or_qv_prof").innerHTML = '<img src="elevation/findORElevation3.php?id=' + e + "&k=" + a + "&h=150&w=550&dummy=" + (new Date).getTime() + '"/>',
                ORSearchMain.unLockLoad(),
                ORSearchMain.qvid = e,
                ORSearchMain.qvopened = !0
            },
            onCreate: function() {}
        })
    },
    closeQuickView: function() {
        var e = $$("tr.or-qv");
        e.length > 0 && (e[0].remove(),
        ORSearchMain.mapqv = void 0),
        ORSearchMain.qvopened = !1
    },
    initQVMap: function() {
        if (void 0 == ORSearchMain.mapqv) {
            var e = {
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                draggableCursor: "crosshair",
                keyboardShortcuts: !0,
                zoomControl: !0,
                mapTypeControl: !0,
                scaleControl: !0,
                streetViewControl: !0,
                scrollwheel: !1,
                center: new google.maps.LatLng(ORMain.centerPoint.getLat(),ORMain.centerPoint.getLng()),
                zoom: 10,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM,
                    style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
                }
            };
            ORSearchMain.mapqv = new google.maps.Map(document.getElementById("or_qv_map"),e)
        }
    },
    loadTripForQV: function(e) {
        if (void 0 != ORSearchMain.mapqv) {
            null != ORSearchMain.poly && (ORSearchMain.poly.setMap(null),
            ORSearchMain.poly = null);
            var t = e.points;
            e.routingengine > 0 && (t = e.roupoints);
            for (var n = decodeLine(t), a = (n.length,
            []), o = new google.maps.LatLngBounds, i = 0; i < n.length; i++) {
                var r = new google.maps.LatLng(parseFloat(n[i][0]),parseFloat(n[i][1]));
                o.extend(r),
                a.push(r)
            }
            ORSearchMain.poly = new google.maps.Polyline({
                path: a,
                strokeColor: "#FF0000",
                strokeOpacity: .85,
                strokeWeight: 2,
                clickable: !1,
                map: ORSearchMain.mapqv
            }),
            ORSearchMain.mapqv.fitBounds(o)
        }
    },
    searchMR: function() {
        ORSearchMain.activateTab(ORSearchMain.pagemy, ORSearchMain.tabmr),
        $(ORSearchMain.tabmr).hide();
        var e = "search/searchMyRoutes.php";
        new Ajax.Request(e,{
            method: "get",
            parameters: {
                u: ORMain.getUnit()
            },
            onSuccess: function(e) {
                "empty" != e.responseText ? ($("myorcontent").innerHTML = e.responseText,
                $("myorcontent").innerHTML.evalScripts(),
                ORSearchUtils.initTable(1, !1)) : "ko" != e.responseText ? $("myorcontent").innerHTML = "Aucun parcours trouvé" : $("myorcontent").innerHTML = "Pb technique"
            },
            onCreate: function() {
                $("loadingmyor").show()
            },
            onComplete: function() {
                $(ORSearchMain.tabmr).show(),
                _gaq.push(["_trackPageview", "/myorspace/mes_parcours.php"])
            }
        })
    },
    searchMF: function() {
        ORSearchMain.activateTab(ORSearchMain.pagemy, ORSearchMain.tabmf),
        $(ORSearchMain.tabmf).hide();
        var e = "search/searchMyFavorites.php";
        new Ajax.Request(e,{
            method: "get",
            parameters: {
                u: ORMain.getUnit()
            },
            onSuccess: function(e) {
                "empty" != e.responseText ? ($("myorcontent").innerHTML = e.responseText,
                $("myorcontent").innerHTML.evalScripts(),
                ORSearchUtils.initTable(1, !1)) : "ko" != e.responseText ? $("myorcontent").innerHTML = "Aucun parcours trouvé" : $("myorcontent").innerHTML = "Pb technique"
            },
            onCreate: function() {
                $("loadingmyor").show()
            },
            onComplete: function() {
                $(ORSearchMain.tabmf).show(),
                _gaq.push(["_trackPageview", "/myorspace/mes_favoris.php"])
            }
        })
    },
    searchMM: function() {
        ORSearchMain.activateTab(ORSearchMain.pagemy, ORSearchMain.tabmm),
        $(ORSearchMain.tabmm).hide();
        var e = "search/searchMyMessages.php";
        new Ajax.Request(e,{
            method: "get",
            onSuccess: function(e) {
                "empty" != e.responseText ? ($("myorcontent").innerHTML = e.responseText,
                $("myorcontent").innerHTML.evalScripts(),
                ORSearchUtils.initTable(1, !1)) : "ko" != e.responseText ? $("myorcontent").innerHTML = "Aucun message trouvé" : $("myorcontent").innerHTML = "Pb technique"
            },
            onCreate: function() {
                $("loadingmyor").show()
            },
            onComplete: function() {
                $(ORSearchMain.tabmm).show(),
                _gaq.push(["_trackPageview", "/myorspace/mes_messages.php"])
            }
        })
    },
    searchMP: function() {
        ORSearchMain.activateTab(ORSearchMain.pagemy, ORSearchMain.tabmp),
        $(ORSearchMain.tabmp).hide();
        var e = "account/userPreferences.php";
        new Ajax.Updater("myorcontent",e,{
            method: "get",
            encoding: "UTF-8",
            evalScripts: !0,
            onComplete: function() {
                $(ORSearchMain.tabmp).show(),
                _gaq.push(["_trackPageview", "/myorspace/mes_preferences.php"])
            }
        })
    },
    searchMPS: function() {
        ORSearchMain.activateTab(ORSearchMain.pagemy, ORSearchMain.tabmps),
        $(ORSearchMain.tabmps).hide();
        var e = "search/searchMyPasses.php";
        new Ajax.Request(e,{
            method: "get",
            onSuccess: function(e) {
                "empty" != e.responseText ? ($("myorcontent").innerHTML = e.responseText,
                $("myorcontent").innerHTML.evalScripts(),
                ORSearchUtils.initTable(1, !1)) : "ko" != e.responseText ? $("myorcontent").innerHTML = "Aucun col trouvé" : $("myorcontent").innerHTML = "Pb technique"
            },
            onCreate: function() {
                $("loadingmyor").show()
            },
            onComplete: function() {
                $(ORSearchMain.tabmps).show(),
                _gaq.push(["_trackPageview", "/myorspace/mes_cols.php"])
            }
        })
    },
    searchMA: function() {
        ORSearchMain.activateTab(ORSearchMain.pagemy, ORSearchMain.tabma),
        $(ORSearchMain.tabma).hide();
        var e = "search/searchMySubscriptions.php";
        new Ajax.Request(e,{
            method: "get",
            encoding: "UTF-8",
            evalScripts: !0,
            onSuccess: function(e) {
                "empty" != e.responseText ? ($("myorcontent").innerHTML = e.responseText,
                ORSearchUtils.initTable(1, !0)) : "ko" != e.responseText ? $("myorcontent").innerHTML = "Aucun abonnement trouvé" : $("myorcontent").innerHTML = "Pb technique"
            },
            onComplete: function() {
                $(ORSearchMain.tabma).show(),
                _gaq.push(["_trackPageview", "/myorspace/ma.php"])
            }
        })
    },
    viewSimpleSearch: function() {
        ORSearchMain.activateTab(ORSearchMain.pagese, ORSearchMain.tabsesi),
        $("tab-simple-search").show(),
        $("tab-advanced-search").hide(),
        $("searchcontent").innerHTML = ""
    },
    viewAdvancedSearch: function() {
        ORSearchMain.activateTab(ORSearchMain.pagese, ORSearchMain.tabsead),
        $("tab-simple-search").hide(),
        $("tab-advanced-search").show(),
        ORSearchMain.initMaps(),
        $("searchcontent").innerHTML = ""
    },
    activateTab: function(e, t) {
        e == ORSearchMain.pagemy ? ($(ORSearchMain.tabmp).removeClassName("button-active"),
        $(ORSearchMain.tabmr).removeClassName("button-active")) : e == ORSearchMain.pagese && ($(ORSearchMain.tabsesi).removeClassName("button-active"),
        $(ORSearchMain.tabsead).removeClassName("button-active")),
        $(t).addClassName("button-active")
    },
    submitHomeSearchForm: function() {
        ORMain.tabMainManager.setActiveTab("tab_search"),
        ORMain.changeHeaderSize(!1),
        ORElementShowableManager.hideTabElements("tab-plan"),
        loadSearchPage($("homesearch").value, 0)
    },
    submitDetailsSearchForm: function(e, t) {
        ORMain.tabMainManager.setActiveTab("tab_search"),
        ORMain.changeHeaderSize(!1),
        ORElementShowableManager.hideTabElements("tab-plan"),
        loadSearchPage(e, t)
    }
}, ORSearchUtils = function() {
    return {
        initTable: function(e, t) {
            var n = "700px";
            1 == e ? (n = "200px",
            void 0 != $j("#tabMyORResult") && $j("#tabMyORResult").tablesorter({
                theme: "or",
                widgets: ["scroller"],
                widgetOptions: {
                    scroller_height: 500,
                    scroller_barWidth: 0
                },
                widthFixed: !1,
                sortList: [[1, 1]],
                sortMultiSortKey: "shiftKey",
                dateFormat: "ddmmyyyy"
            }),
            void 0 != $j("#cc_result_d") && $j("#cc_result_d").tablesorter({
                theme: "or",
                widgets: ["scroller"],
                widgetOptions: {
                    scroller_height: 500,
                    scroller_barWidth: 0
                }
            }),
            void 0 != $j("#cc_result_td") && $j("#cc_result_td").tablesorter({
                theme: "or",
                widgets: ["scroller"],
                widgetOptions: {
                    scroller_height: 500,
                    scroller_barWidth: 0
                }
            }),
            $("loadingmyor").hide()) : void 0 != $j("#tabSearchResult") && $j("#tabSearchResult").tablesorter({
                theme: "or",
                widgets: ["scroller"],
                widgetOptions: {
                    scroller_height: 500,
                    scroller_barWidth: 0
                },
                sortList: [[0, 1]],
                sortMultiSortKey: "shiftKey",
                dateFormat: "ddmmyyyy"
            }),
            Effect.ScrollTo("searchcontent", {
                duration: "0"
            })
        },
        getSearchParams: function(e) {
            if ("a" == e) {
                var t = ORSearchMain.radend
                  , n = ORSearchMain.radstart
                  , a = ORSearchMain.mapstart.getCenter()
                  , o = a.lat()
                  , i = a.lng()
                  , r = ORSearchMain.mapend.getCenter()
                  , s = r.lat()
                  , l = r.lng()
                  , c = $F("cloce");
                "0" == c ? (s = 0,
                l = 0) : "1" == c && (s = o,
                l = i,
                n > t && (t = n));
                var u = document.forms.orsead.ra.options[document.forms.orsead.ra.options.selectedIndex].value
                  , g = document.forms.orsead.sp.options[document.forms.orsead.sp.options.selectedIndex].value
                  , p = document.forms.orsead.dif.options[document.forms.orsead.dif.options.selectedIndex].value
                  , d = $("dismin").innerHTML
                  , R = $("dismax").innerHTML;
                return {
                    st: e,
                    lats: o,
                    lngs: i,
                    late: s,
                    lnge: l,
                    dis: d,
                    die: R,
                    dif: p,
                    rs: n,
                    re: t,
                    ra: u,
                    ac: g,
                    lang: "fr",
                    es: v
                }
            }
            var M = document.forms.orse.kw.value
              , O = document.forms.orse.us.value
              , h = document.forms.orse.idr.value
              , m = document.forms.orse.ac.value;
            isNaN(h) && (h = 0);
            var v = 0;
            return document.forms.orse.es && document.forms.orse.es.checked && (v = 1),
            {
                st: e,
                kw: M,
                us: O,
                idr: h,
                es: v,
                ac: m
            }
        }
    }
}(), ORIDocMain = {
    tabdo: "oriddo",
    tabfa: "oridfa",
    tabne: "oridne",
    tabse: "oridse",
    tabfe: "oridfe",
    loaded: !1,
    newsloaded: !1,
    docloaded: !1,
    doclesson: "",
    viewDoc: function(e) {
        if (ORIDocMain.activateTab(ORIDocMain.tabdo),
        ORIDocMain.docloaded)
            $(ORIDocMain.tabdo).show(),
            $("orinfodoccontent").innerHTML = $("cache_doc").innerHTML,
            "" != ORIDocMain.doclesson && (ORIDocMain.loadDocLesson(ORIDocMain.doclesson),
            ORIDocMain.doclesson = "");
        else {
            $(ORIDocMain.tabdo).hide();
            var t = "infodoc/ordoc.php";
            new Ajax.Updater("orinfodoccontent",t,{
                method: "post",
                onCreate: function() {
                    $("loadingorid").show()
                },
                onComplete: function() {
                    $(ORIDocMain.tabdo).show(),
                    ORIDocMain.loaded = !0,
                    ORIDocMain.docloaded = !0,
                    $("cache_doc").innerHTML = $("orinfodoccontent").innerHTML,
                    $("loadingorid").hide(),
                    "" != ORIDocMain.doclesson && (ORIDocMain.loadDocLesson(ORIDocMain.doclesson),
                    ORIDocMain.doclesson = "")
                }
            }),
            _gaq.push(["_trackPageview", "/aide_info/doc.php"])
        }
    },
    viewFaq: function() {
        ORIDocMain.activateTab(ORIDocMain.tabfa);
        var e = "infodoc/orfaqtab.php";
        new Ajax.Updater("orinfodoccontent",e,{
            method: "post",
            evalScripts: !0,
            onCreate: function() {
                $("loadingorid").show()
            },
            onComplete: function() {
                $(ORIDocMain.tabfa).show(),
                ORIDocMain.loaded = !0,
                $("loadingorid").hide(),
                _gaq.push(["_trackPageview", "/aide_info/faq.php"])
            }
        })
    },
    viewFeatures: function() {
        ORIDocMain.activateTab(ORIDocMain.tabfe);
        var e = "infodoc/orfeatab.php";
        new Ajax.Updater("orinfodoccontent",e,{
            method: "post",
            evalScripts: !0,
            onCreate: function() {
                $("loadingorid").show()
            },
            onComplete: function() {
                $(ORIDocMain.tabfa).show(),
                ORIDocMain.loaded = !0,
                _gaq.push(["_trackPageview", "/aide_info/fonctionnalites.php"]),
                $("loadingorid").hide()
            }
        })
    },
    viewServices: function() {
        ORIDocMain.activateTab(ORIDocMain.tabse);
        var e = "infodoc/orsertab.php";
        new Ajax.Updater("orinfodoccontent",e,{
            method: "post",
            evalScripts: !0,
            onCreate: function() {
                $("loadingorid").show()
            },
            onComplete: function() {
                $(ORIDocMain.tabfa).show(),
                ORIDocMain.loaded = !0,
                _gaq.push(["_trackPageview", "/aide_info/services.php"]),
                $("loadingorid").hide()
            }
        })
    },
    viewNews: function() {
        if (ORIDocMain.activateTab(ORIDocMain.tabne),
        ORIDocMain.newsloaded)
            $("orinfodoccontent").innerHTML = $("cache_news").innerHTML;
        else {
            var e = "infodoc/ornews.php?lang=" + LANG;
            new Ajax.Updater("orinfodoccontent",e,{
                method: "post",
                evalScripts: !0,
                onCreate: function() {
                    $("loadingorid").show()
                },
                onComplete: function() {
                    ORIDocMain.loaded = !0,
                    ORIDocMain.newsloaded = !0,
                    $("cache_news").innerHTML = $("orinfodoccontent").innerHTML,
                    $("loadingorid").hide(),
                    _gaq.push(["_trackPageview", "/aide_info/news.php"])
                }
            })
        }
    },
    activateTab: function(e) {
        $(ORIDocMain.tabdo).removeClassName("button-active"),
        $(ORIDocMain.tabfa).removeClassName("button-active"),
        $(e).addClassName("button-active")
    },
    loadDocLesson: function(e) {
        var t = "i18n/doc/" + LANG + "/" + e;
        $j.ajax({
            url: t,
            beforeSend: function(e) {
                $("loadingorid").show()
            }
        }).done(function(e) {
            $("loadingorid").hide(),
            $j("#or-lesson-main").html(e)
        })
    },
    setDocLesson: function(e) {
        ORIDocMain.doclesson = e
    }
};
setTimeout(function(){
    jQuery('#tools_openclose').trigger('click');
}, 1000)